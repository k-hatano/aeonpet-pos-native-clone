# Orange Operation Pos

old README is [Here](./docs/development/en/README.old.md)

## Requirements for development
### MacOS(for iOS)
- MacOS X 10.12
- XCode 8.3.x
- NodeJS 6.9.x  NPM 3.x 

### Windows(for UWP)
- Windows10
- VisualStudio 2015 Community or Professional
- NodeJS 6.9.x  NPM 3.x 
- Git for Windows

## How to develop (MacOS for iOS)
### Installment
#### React Native CLI
```sh
npm i -g react-native-cli
```

#### Yarn
```sh
npm i -g yarn
```

### Build for simulator
```sh
git clone git@bitbucket.org:s-cubism/orange3-operation-pos-native.git
cd pos-native

# install npm libraries
yarn

# Setup XCode project
cd ios
carthage update --platform iOS

# XCodeを開く
# Open project by XCode.
open ios/pos_native.xcodeproj/

# react-nativeの開発様サーバを開始する。
# このサーバは、開発中のreact-nativeアプリがJavaScriptをロードするために使われる。
# Start react-native development server.
react-native start --reset-cache

# 最後に、XCodeでデバッグを開始する。
# 最初に赤い画面が表示される場合があるが、リロードすれば解決する場合がある。
# Start debugging on XCode.
```

## How to develop (Windows for UWP)
