# Table of contents
- [HomePage](#homepage)
- [SyncDataPage](#syncdatapage)
- [CartPage](#cartpage)
- [OrderCompletePage](#ordercompletepage)
- [SearchOrderPage](#searchorderpage)
- [SearchCustomerPage](#searchcustomerpage)
- [BalancePage](#balancepage)
- [OpenSalePage](#opensalepage)
- [CheckSalePage](#checksalepage)
- [CloseSalePage](#CloseSalePage)
- [AggregateCloseSalePage](#AggregateCloseSalePage)
- [CashRecordPage](#cashrecordpage)
- [LoginUserPage](#loginuserpage)
- [LoginSelectShopPage](#loginselectshoppage)
- [LoginSelectCashierPage](#loginselectcashierpage)
- [LoginConfirmPage](#loginconfirmpage)
- [TaxFreeInfoPage](#taxfreeinfopage)
- [TaxFreeSellerPage](#taxfreesellerpage)
- [TaxFreeConfirmPage](#taxfreeconfirmpage)


# Views

## HomePage
pages/HomePage.js

<img src="images/pages/HomePage.png" width="800" height="580"/>

### Contains

## SyncDataPage
pages/SyncDataPage.js

<img src="images/pages/SyncDataPage.png" width="800" height="580"/>

### Contains
- [SyncView](./modules/syncData#syncView)
- [SyncHistoryList](./modules/syncData#syncHistoryList)
- [SyncHisotryListItem](./modules/syncData#syncHitoryListItem)

## CartPage
pages/CartPage.js

<img src="images/pages/CartPage.png" width="800" height="580"/>

## OrderCompletePage
pages/OrderCompletePage.js

<img src="images/pages/OrderCompletePage.png" width="800" height="580"/>

### Contains
- [OrderCompleteView](#OrderCompleteView)
- [OrderCompleteActionsPanel](#OrderCompleteActionsPanel)

## SearchOrderPage
pages/SearchOrderPage.js

<img src="images/pages/SearchOrderPage.png" width="800" height="580"/>

### Contains
- [SearchOrderForm](./modules/order#searchorderform)
- [OrderListView](./modules/order#orderlistview)
- [OrderSearchResultDetailView](./modules/order#ordersearchresultdetailview)

## SearchCustomerPage
pages/SearchCustomerPage.js

<img src="images/pages/SearchCustomerPage.png" width="800" height="580"/>

## BalancePage
pages/BalancePage.js

<img src="images/pages/BalancePage.png" width="800" height="580"/>

### Contains
- [BalanceReasonView](./modules/balance#balancereasonview)
- [BalanceAmountForm](./modules/balance#balanceamountview)

## OpenSalePage
pages/OpenSalePage.js

<img src="images/pages/OpenSalePage.png" width="800" height="580"/>

## CheckSalePage
pages/CheckSalePage.js

<img src="images/pages/CheckSalePage.png" width="800" height="580"/>

## CloseSalePage
pages/CloseSalePage.js

<img src="images/pages/CloseSalePage.png" width="800" height="580"/>

## AggregateCloseSalePage
pages/AggregateCloseSalePage.js

<img src="images/pages/AggregateCloseSalePage.png" width="800" height="580"/>

## CashRecordPage
pages/CashRecordPage.js

<img src="images/pages/CashRecordPage.png" width="800" height="580"/>

### Contains
- [CashForm](CashForm)
- [CashView](CashView)

## LoginUserPage
pages/LoginUserPage.js

<img src="images/pages/LoginUserPage.png" width="800" height="580"/>

### Contains
- [LoginUserForm](./modules/login#LoginUserForm)

### Transitions
- Go to LoginSelectShopPage on LoginUserForm::onLogin

## LoginSelectShopPage
pages/LoginSelectShopPage.js

<img src="images/pages/LoginSelectShopPage.png" width="800" height="580"/>

### Contains
- [LoginUserForm](./modules/login#LoginSelectShopForm)

### Transitions
- Go to LoginSelectCashierPage on LoginSelectShopForm::onNext

## LoginSelectCashierPage
pages/LoginSelectCashierPage.js

<img src="images/pages/LoginSelectCashierPage.png" width="800" height="580"/>

### Contains
- [LoginUserForm](./modules/login#LoginSelectCashierForm)

### Transitions
- Go to LoginSelectShopPage on LoginSelectCashierForm::onBack
- Go to LoginConfirmPage on LoginSelectCashierForm::onNext

## LoginConfirmPage
pages/LoginConfirmPage.js

<img src="images/pages/LoginConfirmPage.png" width="800" height="580"/>

### Contains
- [LoginUserForm](./modules/login#LoginConfirmView)

### Transitions
- Go to LoginSelectCashierPage on LoginConfirmView::onCancel
- Go to Home on LoginConfirmView::onLogin

## TaxFreeInfoPage
pages/TaxFreeInfoPage.js

<img src="images/pages/TaxFreeInfoPage.png" width="800" height="580"/>

## TaxFreeSellerPage
pages/TaxFreeSellerPage.js

<img src="images/pages/TaxFreeSellerPage.png" width="800" height="580"/>

## TaxFreeConfirmPage
pages/TaxFreeConfirmPage.js

<img src="images/pages/TaxFreeConfirmPage.png" width="800" height="580"/>
