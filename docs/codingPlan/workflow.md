# pos_native コーディングルール Coding rule

## Repositoryについて
DB/APIへのアクセスは、リポジトリクラスを通して行う
Access DB/API via repository class.

DB/APIアクセスのメソッドは、必ず非同期(async)メソッドとする。
Methods for access DB/API must be async.

DB/APIアクセスメソッドの引数と返り値は、必ずJSONで表現可能な型とする。
Argument and return value of repository methods are simple js object (not a class).

リポジトリは、以下の構成とする。

- repositories/XxxxRepository
    - インタフェースとなるクラス。メソッドは全てstaticとする。
    - 状態に応じて実装を切り替えられるよう、このリポジトリ内では実装は持たず、実装用のリポジトリクラスに処理を移譲する。
    - 実装用のリポジトリクラスのインスタンスをstatic変数として持つ
- repositories/standard/StandardXxxxRepository
    - 通常利用するリポジトリ。実際にデータベースや
- repositories/sample/SampleXxxxRepository
    - 開発モードのときのみ、ログイン画面に表示されるSample Modeボタンをタップして画面に入ると、このリポジトリが利用される。
    - 固定値の配列を返す、変数内にデータを保存するなど、画面を動作させるのに最低限必要な機能のみをダミーで実装する。
- repositories/training/TrainingXxxxRepository
    - トレーニングモードのときのみ利用する
    - トレーニングモードの仕様に従い、適宜ダミーデータを返すなどの実装を行う。

ToDo : 実装の切替方法
ToDo : サンプルデータの書き方

### Example

```
// modules/xxxx/repositories/XxxxRepository.js
import SampleXxxxRepository from './sample/SampleXxxxRepository'
import StandardXxxxRepository from './standard/StandardXxxxRepository'

export class XxxxRepository {
  static _implement

  static async findAll() {
    return this._implement.findAll()
  }

  async save(xxxx) {
    return this._implement.save()
  }
}

// TODO
```

## ビジネスロジック
Componentにはロジックは書かない。
Don't put a logic in component.

ここで言うロジックは、計算処理の他、API/DBアクセス、設定保存などを含む。
The logic contains calculation, accessing to API/DB and Save settings.

ロジックは、models.js、あるいはmodelsディレクトリ以下のファイルに書く。
Put a logic in models.js or under models directory.

Containerにはシンプルなロジックのみ書いてもよい。
Can put only simple logic in Container.

また、カート、棚卸しなど状態を持つ複雑なビジネスロジックはimmutable.jsを利用して、
models以下に個別のクラスを作成する。
Use immutable.js for complex business logic. Put it in modules/{moduleName}/models directory.

## Componentについて
Componentは純粋なReactのUI部品である。
殆どの場合、Container経由で利用されますが、出来る限りReducerを意識しない作りにする。
ただし、操作状態の保持以外の目的でstateは利用しない。状態は必ずプロパティとして持つ。

ConponentからContainerはできる限り参照しないでください。

## Containerについて
ComponentにReducerの機能を付与したラッパーコンポーネントがContainerである。
mapDispatcherToPropsには、本来の役割であるdispatchを実行する以外にも、簡単なロジックを実装することができる。リポジトリやプリンタの利用もここに書く。

### PropTypes
必ずPropTypesを書く。
Should define propTypes

```
import PropTypes from 'prop-types'

export default class XxxxView extends Component {
    static propTypes = {
        xxxxs: PropTypes.array,
        onSelectXxxx: PropTypes.func,
        selectedXxxxType: PropTypes.number
    }

    ......
}
```

## 命名規約 Naming

### Component

- ページ Page
    - XxxxPage
- ボタンが配置されたComponent Component for buttons
    - XxxxPanel
- 入力のためのComponent Component for input
    - XxxxForm
- リスト
    - XxxxList
- リストの要素 Element of list
    - XxxxListItem
- その他の、moduleに配置されたComponent Other component in modules
    - XxxxView

### ComponentProperty

- 何かを実行するイベント Event for executing something
    - onXxxx (Xxxx is verb)
- 値を変更するイベント Event for change value
    - onXxxxChanged
    - onXxxxSelected

### Container
Componentと同じ名前
Same name as component

Containerサフィックスは使用しない。(admin_webと合わせるため)
Don't use "Container" suffix. (To match with admin_web)

### Repository
クラス名には、Repositoryサフィックスをつける。
Add "Repository" suffix

クラスファイルは /modules/{moduleName}/repositories/ に配置する。
Put the class in /modules/{moduleName}/repositories/

### RepositoryMethod

- DBを検索する Search for DB
    - findByXxxx (Xxxx is condition name)
    - findAll (Get all data without condition)
- DBに保存する Save into DB
    - save (Save single object)
    - bulkSave (Save array of object)
    - updateXxxxById(id, value) (Update specified property of existing data.)
- サーバから検索する Search for Server
    - fetchByXxxx (Xxxx is condition name)
- サーバに保存する。 Save to server
    - push (Save single object)
    - bulkPush (Save array of object)
- データ同期を行う Execute sync DB
    - syncRemoteToLocal

### Entity
エンティティ名には、Entityサフィックスをつける。
Add "Entity" suffix

クラスファイルは /modules/{moduleName}/repositories/entities/ に配置する。
Put the class in /modules/{moduleName}/repositories/entities/

### ActionCreator
名前の最初の単語は動詞にする
The first word of actionCreator name is a verb.

Example

- selectProduct
- updateAmount
- listProducts

# ワークフロー Work flow

## Gitの扱い
対象リポジトリ orange3/pos-native
ベースブランチ master
作業用ブランチ名 features/{BacklogTicketCode} (Ex. features/ENOTECA_POS_INNER-168)

ベースブランチから作業用ブランチを作成し、そちらにコミットを行う。
作業のあった日は最低１回はコミット、プッシュを行う。

## タスクの進め方
一つの機能の開発は、以下の５つのステップから成る。
最後以外の全てのステップにおいて、それぞれの実装計画を書く。

実装計画は下記リポジトリにマークダウン形式で書く
https://gitlab.private.s-cubism.com:8443/makoto.kume/orange-pos-ui-structure
こちらのwikiのブランチの扱いも、ソースコードと同様とする。

masterにマージされた実装計画は、適宜久米がwikiに反映する。
https://gitlab.private.s-cubism.com:8443/makoto.kume/orange-pos-ui-structure/wikis/home

入出金機能の画面を参考にすること。
Refer to balance page.
https://gitlab.private.s-cubism.com:8443/makoto.kume/orange-pos-ui-structure/wikis/modules/balance

一気に全ての計画を書いても良いし、ステップ毎に書いても良いが、
実装前に責任者（久米）が確認を行えるようにする。

実装後、マージリクエストを作成し、フィードバックがあれば修正を行い、マージリクエストの確認者がOKを出した時点でタスク完了とする。
マージリクエストについては、次の章を参照。

### 1. 画面実装 View
まずは、画面の見た目のみを作成する。
ただし、見た目が動的に変化する部分については、プロパティで外部から指定できるようにしておき、プロパティへの値のセットは定数値などで行う。

アプリ上で画面を確認するため、画面遷移は仮で実装する。
通常の画面遷移で表現が難しい場合や、部品のみの実装でページが無い場合などは、DebugPageに確認用のボタンを作る

#### 実装対象 Implementation target

- Components (modules/xxxx/components/* )
- styles (modules/xxxx/styles/* )
- Pages (pages/XxxxPage.js)
- Translations (ja.json, en.json)
- models
    - 値とラベルの変換(例: xxxTypeToLabel)と、MODULE_NAMEのみ
    - Only value to label conversion(Ex. xxxTypeToLabel) and MODULE_NAME.

#### 計画の書き方
画面をどの様な部品に分割するか、下記のフォーマットで書く

##### パス Path
```
#### Path
```

##### 画面画像 View Image

```
#### Image
![View Name](image path)
```

##### プロパティ Properties

```
#### Props
- onXxxxChange: func
- propertyName2: int
- propertyName3: Object
    - childPropertyName1: number
    - childPropertyName2: string
```

##### 包含するコンポーネント Contains components

包含するコンポーネントの一覧と、親から子へのプロパティのマッピングを書く

```
#### Contains
- [ChildView1](/modules/xxxx#childview1)
    - childViewProperty1: propertyName1
    - childViewProperty2: propertyName2
```

##### 画面遷移 View Transitions

```
#### Transitions
- Go to YyyyPage : onXxxxChanged
```

### 2. 画面動作 View State
#### 実装対象 Implementation target
- reducer (modules/xxxx/reducer.js)
- action (modules/xxxx/actions.js)
- Container (modules/xxxx/containers/* )

#### 計画の書き方 Planing
reducerで管理する状態を書く。
ステート名: 型 の形式で書く。型はentity一覧を参照する形で書く

```
#### States
- xxxx: entity_name[]
- selectedXxxx: entity_name
- selectedXxxxType: int
```

### 3. 帳票作成 Implement receipt
レシート出力のある機能の場合、レシート作成メソッドを作る。

#### 実装対象 Implementation target
- ReceipBuilder (modules/xxxx/models/XxxxReceiptBuilder)
- Unit test for receipt builder (modules/xxxx/models/__tests__/XxxxReceiptBuilder-test.js)

#### 計画の書き方 Planing
レシート情報作成の元になるサンプルデータと、出力されるレシート（テキスト表現）を書く
Write input sample data and output receipt text.

````
## Receipts
### BalanceReceipt
#### Sample1

sample data

```
// balances
[
  {
    balance_type: BALANCE_TYPE.DEPOSIT,
    amount: 12000,
    currency: 'jpy',
    payment_method_name: '現金',
    balance_reason_name: '小口入金'
  }
]

// createdAt
new Date("2017-07-07 09:30:00")
```

output

```
                       入金

店舗1
レジ1
2017-07-07 09:30:00

小口入金                                ￥12,000

             ----------------------

現金                                   ￥12,000

                                    担当:担当者1

```
````

### 4. ロジック実装 Implement logic
実際に画面が一通りの機能を果たすための機能を実装する。
ただし、APIアクセス/DBアクセスについては、サンプルリポジトリの実装とする。
5でStandardREpositoryを実装すると、全ての機能が実装される、という状態まで持っていく。

#### 実装対象 Implementation target
- models (modules/xxxx/models.js, modules/xxxx/models/* )
- page (pages/XxxxPage)
- container (modules/xxxx/containers/* )
- Repositories (modules/xxxx/repositories/* )
- And others.

#### 計画の書き方 Planing
リポジトリのメソッドの名前とインタフェース（引数と返り値の型）を書く。
型はエンティティを参照する。
Write repository's name and type of args and return value.
The type refers to entity.

```
### BalanceReasonRepository
- findByBalanceType
    - args
        - balanceType: int
    - return: cashier_balance[]

### BalanceRepository
- bulkPush
    - args
        - balances: cashier_balance[]
    - return: none
```

### 5 API/DB本接続
#### 実装対象 Implementation target
- StandardRepository (modules/xxxx/repositories/standard/StandardXxxxRepository.js )
- Entities (modules/xxxx/repositories/entities/* )

## マージリクエストについて Merge request
masterブランチに対してマージリクエストを行う。
Create merge request for master branch.

ワークフローは以下。

### オフショアメンバー HCMC member
- マージリクエストを作成し、Huyをアサイン Create merge request to Huy
- Huyはレビューをし、OKなら久米をアサイン Huy reviews it. If it is OK, assign it to makoto.kume.
- 久米が確認し、マージ。 指摘があれば実装担当者へ直接フィードバック。 makoto.kume review it and merge to master. If there is any problem, feedback and assign to developer.
- フィードバック後、修正したら久米にアサイン After fixed, assign to makoto.kume again.

### Huy
- 久米へ直接マージリクエスト Create merge request to makoto.kume.

### SCメンバー SC member
- 久米へ直接マージリクエスト Create merge request to makoto.kume or daiki.inoue.
- 場合によって井上に担当を振り分け

### 久米 makoto.kume
- 自身で直接masterにマージ Merge into master by myself.
- または、細田にレビュー依頼 Or, ask the review to kenji.hosoda.

### その他、パートナーなど Others
- 久米へ直接マージリクエスト
- 場合によって井上に担当を振り分け
