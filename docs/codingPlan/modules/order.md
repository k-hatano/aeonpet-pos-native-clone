# Table of contents
- [SearchOrderForm](#searchorderform)
- [OrderListView](#orderlistview)
- [OrderListItem](#orderlistitem)
- [OrderSearchResultDetailView](#ordersearchresultdetailview)
- [OrderView](#orderview)
- [OrderItemListItem](#orderitemlistitem)
- [OrderHistoryActionsPanel](#orderhistoryactionspanel)

# Views

## SearchOrderForm
modules/order/components/SearchOrderForm

![SearchOrderForm](../images/modules/order/components/SearchOrderForm.png)

## OrderListView
modules/order/components/OrderListView

![OrderListView](../images/modules/order/components/OrderListView.png)

### Props
- orders: Array<Object> (from reducer)

### Dependency
- [OrderListItem](#OrderListItem)
  - orders.map => order

## OrderListItem
modules/order/components/OrderListItem

![OrderListItem](../images/modules/order/components/OrderListItem.png)

### Props
- order: Object
  - payment_method_name
  - client_created_at
  - total_paid_taxless
  - total_paid_tax

## OrderSearchResultDetailView
modules/order/components/OrderSearchResultDetailView

![OrderSearchResultDetailView](../images/modules/order/components/OrderSearchResultDetailView.png)

### Props
- order: Object (from reducer)
  - order_items: array
- onRepringReceipt: func
- onPrintBill: func
- onCancelOrder: func
- onReturnOrder: func

### Dependency
- [OrderItemListItem](#OrderItemListItem)
    - order.order_items.map => items
- [OrderHistoryActionsPanel](#OrderHistoryActionsPanel)
    - onRepringReceipt => onRepringReceipt
    - onPrintBill => onPrintBill
    - onCancelOrder => onCancelOrder
    - onReturnOrder => onReturnOrder

## OrderView
modules/order/components/OrderView

![OrderView](../images/modules/order/components/OrderView.png)

Preparing...

### Props
- order: Object

## OrderItemListItem
modules/order/components/OrderItemListItem

![OrderItemListItem](../images/modules/order/components/OrderItemListItem.png)

### Patterns
- With item discount
  - ![OrderItemListItem2](../images/modules/order/components/OrderItemListItem-2.png)

### Props
- item: Object
  - product_name
  - quantity
  - product_variant_name
  - sales_total_taxless
  - sales_total_tax
  - list_price_taxless
  - list_price_tax
  - sales_discount
      - Dummy value for now
  - item_discount
      - Dummy value for now

## OrderHistoryActionsPanel
modules/order/components/OrderHistoryActionsPanel

![OrderHistoryActionsPanel](../images/modules/order/components/OrderHistoryActionsPanel.png)

### Props
- onReprintReceipt: func
- onPrintBill: func
- onCancelOrder: func
- onReturnOrder: func
