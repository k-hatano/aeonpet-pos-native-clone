# Balance Module

## Table of contents
- [BalanceReasonView](#BalanceReasonView)
- [BalanceReasonForm](#BalanceReasonForm)
- [PaymentFormView](#PaymentFormView)

## Views

### BalanceReasonView
#### Path
modules/balance/components/BalanceReasonView

#### Image
![BalanceReasonView](../images/modules/balance/components/BalanceReasonView.png)

#### Props
- onSelectBalanceReason: func
- onSelectBalanceType: func
- selectedBalanceType: number
- balanceReasons: array
- selectedDepositReason: object
- selectedWithdrawalReason: object

#### Contains
- BalanceReasonForm(for Deposit)
    - onSelectBalanceReason => onSelectBalanceReason
    - selectedBalanceType => balanceType
    - selectedDepositReason => selectedBalanceReason
    - balanceReasons => balanceReasons
- BalanceReasonForm(for Withdrawal)
    - onSelectBalanceReason => onSelectBalanceReason
    - selectedBalanceType => balanceType
    - selectedWithdrawalReason => selectedBalanceReason
    - balanceReasons => balanceReasons



### BalanceReasonForm
#### Path
modules/balance/components/BalanceReasonForm

#### Image
![BalanceReasonForm](../images/modules/balance/components/BalanceReasonForm.png)

#### Props
- onSelectBalanceReason: func
- balanceType: number
- selectedBalanceReason: object
- balanceReasons: array



### BalanceAmountForm
#### Path
modules/balance/components/BalanceAmountForm

#### Image
![PaymentFormView](../images/modules/balance/components/BalanceAmountForm.png)

#### Props
- balanceType: number
- amount: number
- onAmountChanged: func
- onComplete: func
- canComplete: bool

## States
- balanceReasons: cashier_balance_reason[]
- selectedBalance: cashier_balance
- selectedBalanceReason: cashier_balance_reason
- selectedDepositReason: cashier_balance_reason
- selectedWithdrawalReason: cashier_balance_reason
- selectedBalanceType: int
- balanceAmount: number
- balances: cashier_balance[]

## Repository
### BalanceReasonRepository
- findByBalanceType
    - args
        - balanceType: int
    - return: cashier_balance[]

### BalanceRepository
- bulkPush
    - args
        - balances: cashier_balance[]
    - return: none
