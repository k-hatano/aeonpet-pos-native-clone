# SearchCustomer Module

## Table of contents
- [CustomerSearchForm](#CustomerSearchForm)
- [CustomerList](#CustomerList)
- [CustomerDetail](#CustomerDetail)
- [SubstituteCustomerSearchForm](#SubstituteCustomerSearchForm)
- [CustomerOrderList](#CustomerOrderList)

## Views

### CustomerSearchForm
#### Path
modules/customer/components/CustomerSearchForm

![CustomerSearchForm](../images/modules/customer/components/CustomerSearchForm.png)

#### Props
- onFindCustomerDetail : func

### CustomerList
#### Path
modules/customer/components/CustomerList

#### Image
![CustomerList](../images/modules/customer/components/CustomerList.png)

#### Props
- onFindCustomerDetail : func
- customerList : array

### CustomerDetail
#### Path
modules/customer/components/CustomerDetail

#### Image
![CustomerDetail](../images/modules/customer/components/CustomerDetail.png)

#### Props
- customerDetail: object

### SubstituteCustomerSearchForm
#### Path
modules/customer/components/SubstituteCustomerSearchForm

#### Image
![SubstituteCustomerSearchForm](../images/modules/customer/components/SubstituteCustomerSearchForm.png)

#### Props
- onFindCustomerDetail : func

### CustomerOrderList
#### Path
modules/customer/components/CustomerOrderList

#### Image
![CustomerOrderList](../images/modules/customer/components/CustomerOrderList.png)

#### Props
- onFindOrderDetail : func
- customerOrderList : array

## States
- customerList: customer[]
- customerDetail: customer_detail
- customerOrderList: customer_order_detail[]

## Repository
### CustomerRepository
- findCustomers
    - args
        - firstNameKana: string
        - phoneNumber: number
    - return: customer[]

- findCustomerDetail
    - args
        - customerCode: number
    - return: customer_detail
