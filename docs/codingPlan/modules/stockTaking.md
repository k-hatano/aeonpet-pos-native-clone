# Stock Taking Module

## Table of contents
- [StockTakingList](#StockTakingList)
- [StockTakingView](#StockTakingView)
- [StockTakingTaskItemList](#StockTakingTaskItemList)
- [StockTakingTaskStartConfirmView](#StockTakingTaskStartConfirmView)
- [StockTakingCountView](#StockTakingCountView)
- [StockTakingSearchProductForm](#StockTakingSearchProductForm)

## Views

### StockTakingList(棚卸)
#### Props
- stockTakings: array
- onStartStockTaking: func
- onSelectStockTaking: func

### StockTakingView(棚卸詳細)
#### Props
- stockTaking: object
- stockTakingTasks: array
- stockTakingUnpushedTasks: array
- onPushUnpushedTasks: func
- onReadyStockTakingTask: func
- onSelectStockTakingTask: func

#### Contains
- StockTakingDetailView
	- stockTaking: object
- StockTakingTaskList
	- stocktakingTasks: array
	- onSelectStockTakingTask: func
- StockTakingTaskListItem
	- stockTakingTask: object
	- onSelectStockTakingTask: func

### StockTakingTaskItemList(棚卸タスク明細)
#### Props
- onBack: func
- stockTakingTaskItems: array

#### Contains
- StockTakingTaskItemListItem
	- stockTakingTaskItem: object


### StockTakingTaskStartConfirmView(棚卸タスク開始)
#### Props
- taskName: string
- staff: Objetct
- onChangeStaff: func
- onChangeTaskName: func
- onStartStockTakingTask: func


### StockTakingCountView(棚卸カウント:左)
#### Props
- itemCount: int
- stockTakingTaskItems: array
- onUpdateStock: func
- onClear: func
- onUpdateListItemQuantity: func

#### Contains
- StockTakingCountListItem
	- stockTakingTaskItem: object
	- onUpdateQuantity: func


### StockTakingSearchProductForm(棚卸カウント:右)
#### Props
- isAutoDisplayNumericKeypad: bool
- onProductSelected: func
- onToggleNumericKeypadAutoDisplay: func
- onComplete: func

#### Contains
- CategoryProductView(exist already)
	- onProductSelected: func

## States
- stockTakings: stock_tanking[]
- stockTakingTasks: stock\_taking_task[]
- stockTakingUnpushedTasks: stock\_taking_task[]
- currentStockTaking: stock_tanking
- currentStockTakingTask: stock_tanking
- itemCount: int
- isAutoDisplayNumericKeypad: bool
- staff: staff

## Repository
### StockTakingRepository
- fetchAll
    - args
        - none
    - return: stock_taking[]
- push
    - args	
        - stock_taking
    - return: none

### StockTakingTaskRepository
- fetchByStockTakingId
    - args
        - stock_taking.id: string
    - return: stock\_taking_task[]
- findUnpushedByStockTakingId
    - args
        - stock_taking.id: string
    - return: stock\_taking\_task[]\(nest\: stock\_taking\_task_item[])
- push
    - args
        - stock_taking.id
    - return: none
- save
    - args
        - stoc\_taking_task
    - return: none

### StockTakingTaskItemRepository
- fetchByStockTakingTaskId
    - args
        - stock\_taking_task.id: string
    - return: stock\_taking\_task_item[] 


## Receipts
none