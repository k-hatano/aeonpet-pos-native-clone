# Table of contents
- [OrderCompleteView](#OrderCompleteView)
- [OrderCompleteActionsPanel](#OrderCompleteActionsPanel)

# Views

## OrderCompleteView
modules/orderComplete/components/OrderCompleteView

![OrderCompleteView](../images/modules/order/components/OrderCompleteView.png)

### Props
- order: Object
  - total_paid
  - deposit
      - Dummy value for now
  - change
      - Dummy value for now
- onComplete: func

## OrderCompleteActionsPanel
modules/orderComplete/components/OrderCompleteActionsPanel
![OrderCompleteActionsPanel](../images/modules/order/components/OrderCompleteActionsPanel.png)

### Props
- onRepringReceipt: func
- onPrintBill: func
- onCancelOrder: func
- onReturnOrder: func
- onPrintTaxFreeReport: func
