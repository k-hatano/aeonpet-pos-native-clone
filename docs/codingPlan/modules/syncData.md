# Table of contents
- [SyncView](#SyncView)
- [SyncHistoryList](#SyncHistoryList)
- [SyncHistoryListItem](#SyncHistoryListItem)

# Views

## SyncView
modules/syncView/components/SyncView

![SyncView](../images/modules/syncData/components/SyncView.png)

### Props
- orSyncAll: func
- onSyncDiff: func

## SyncHisotyList
modules/syncData/components/SyncHisotyList

![SyncHisotryList](../images/modules/syncData/components/SyncHistoryList.png)

### Props
- syncData: Array
- onComplete: func

### Contains
- [SyncHisotyListItem](#SyncHistoryListItem)


## SyncHisotyListItem
modules/syncData/components/SyncHisotyListItem

![SyncHisotryListItem](../images/modules/syncData/components/SyncHistoryListItem.png)

### Patterns
- When sync status failed

![SyncErrorHisotryListItem](../images/modules/syncData/components/SyncErrorHistoryListItem.png)

### Props
- syncData: Object
  - sync_time
  - sync_target
  - sync_status
