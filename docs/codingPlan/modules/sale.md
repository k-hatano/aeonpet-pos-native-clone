# Table of contents
- [SaleReceiptView](#SaleReceiptView)
- [SaleView](#SaleView)
- [SaleActionPanel](#SaleActionPanel)

# Views

## SaleReceiptView
modules/sale/components/SaleReceiptView

<img src="../images/modules/sale/components/SaleReceiptView.png" width="400" />

## SaleView
modules/sale/components/SaleView

<img src="../images/modules/sale/components/SaleView.png" width="400" />

## SaleActionPanel
modules/sale/components/SaleActionPanel

<img src="../images/modules/sale/components/SaleActionPanel.png" width="400" />
