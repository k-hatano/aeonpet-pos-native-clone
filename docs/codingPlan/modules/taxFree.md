# Table of contents
- [TaxFreeInfoForm](#taxfreeinfoform)
- [SellerInfoForm](#sellerinfoform)
- [TaxFreeConfirmView](#taxfreeconfirmview)

# Views

## TaxFreeInfoForm
modules/taxFree/components/TaxFreeInfoForm

<img src="../images/modules/taxFree/components/TaxFreeInfoForm.png" width="800" height="580"/>

## SellerInfoForm
modules/taxFree/components/SellerInfoForm

<img src="../images/modules/taxFree/components/SellerInfoForm.png" width="800" height="580"/>

## TaxFreeConfirmView
modules/taxFree/components/TaxFreeConfirmView

<img src="../images/modules/taxFree/components/TaxFreeConfirmView.png" width="800" height="580"/>
