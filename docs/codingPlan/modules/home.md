# Table of contents
- [HomeMenuView](#homemenuview)
- [BasicMenuPanel](#basicmenupanel)
- [MainMenuPanel](#mainmenupanel)
- [SubMenuPanel](#submenupanel)
- [HomeHistoryView](#homehistoryview)
- [OperationList](#operationlist)
- [OperationListItem](#operationlistitem)
- [OrderList](#orderlist)
- [OrderListItem](#orderlistitem)
- [OrderDetailView](#orderdetailview)
- [OrderProductList](#orderproductlist)
- [OrderProductListItem](#orderproductlistitem)
- [OrderDetailActionPanel](#orderdetailactionpanel)

# Views

## HomeMenuView
modules/home/components/HomeMenuView

<img src="../images/modules/home/components/HomeMenuView.png" width="400" height="580"/>

## BasicMenuPanel
modules/home/components/BasicMenuPanel

<img src="../images/modules/home/components/BasicMenuPanel.png" width="400"/>

## MainMenuPanel
modules/home/components/MainMenuPanel

<img src="../images/modules/home/components/MainMenuPanel.png" width="400"/>

## SubMenuPanel
modules/home/components/SubMenuPanel

<img src="../images/modules/home/components/SubMenuPanel.png" width="400" />

## HomeHistoryView
modules/home/components/HomeHistoryView

<img src="../images/modules/home/components/HomeHistoryView.png" width="400" />

## OperationList
modules/home/components/OperationList

<img src="../images/modules/home/components/OperationList.png" width="400" />

## OperationListItem
modules/home/components/OperationListItem

<img src="../images/modules/home/components/OperationListItem.png" width="400" />

## OrderList
modules/home/components/OrderList

<img src="../images/modules/home/components/OrderList.png" width="400" />

## OrderListItem
modules/home/components/OrderListItem

<img src="../images/modules/home/components/OrderListItem.png" width="400" />

## OrderDetailView
modules/home/components/OrderDetailView

<img src="../images/modules/home/components/OrderDetailView.png" width="400" />

## OrderProductList
modules/home/components/OrderProductList

<img src="../images/modules/home/components/OrderProductList.png" width="400" />

## OrderProductListItem
modules/home/components/OrderProductListItem

<img src="../images/modules/home/components/OrderProductListItem.png" width="400" />

## OrderDetailActionPanel
modules/home/components/OrderDetailActionPanel

<img src="../images/modules/home/components/OrderDetailActionPanel.png" width="400" />

## PendingOrderDetailActionPanel
modules/home/components/PendingOrderDetailActionPanel

<img src="../images/modules/home/components/PendingOrderDetailActionPanel.png" width="400" />
