# Receipt Module

## Table of contents
- [ReceiptView](#ReceiptView)

## Views

### ReceiptView
#### Path
modules/closeSale/components/ReceiptView

![ReceiptView](../images/modules/receipt/components/ReceiptView.png)
