# CashierTotal Module

## Table of contents
- [CheckSaleReceiptView](#CheckSaleReceiptView)
- [CheckSaleOperationPanel](#CheckSaleOperationPanel)
- [CloseSaleOperationPanel](#CloseSaleOperationPanel)
- [AggregateCloseSaleForm](#AggregateCloseSaleForm)


## Views

### CheckSaleReceiptView
#### Path
modules/cashierTotal/components/CheckSaleReceiptView

#### Image
![CheckSaleReceiptView](../images/modules/cashierTotal/components/CheckSaleReceiptView.png)

#### Props
- onSelectReceiptType: func
- selectedReceiptType: number

### CheckSaleOperationPanel
#### Path
modules/cashierTotal/components/CheckSaleOperationPanel

# Image
![CheckSaleOperationPanel](../images/modules/cashierTotal/components/CheckSaleOperationPanel.png)

#### Props
- onPrintCheckSaleReceipt: func

### CloseSaleOperationPanel
#### Path
modules/cashierTotal/components/CloseSaleOperationPanel

![CloseSaleOperationPanel](../images/modules/cashierTotal/components/CloseSaleOperationPanel.png)

#### Props


### AggregateCloseSaleForm
#### Path
modules/cashierTotal/components/AggregateCloseSaleForm

![AggregateCloseSaleForm](../images/modules/cashierTotal/components/AggregateCloseSaleForm.png)
