# Table of contents
- [CashRecordForm](#cashrecordform)
- [CashRecordTotalView](#CashRecordTotalView)

# Views

## CashRecordForm
modules/cashRecord/components/CashRecordForm

![CashRecordForm](../images/modules/cashRecord/components/CashRecordForm.png)

### Props
- cashRecord: Object
  - bill_1000
  - bill_2000
  - bill_5000
  - bill_10000
  - coin_500
  - coin_100
  - coin_50
  - coin_10
  - coin_5
  - coin_1
  - coinbar_500
  - coinbar_100
  - coinbar_50
  - coinbar_10
  - coinbar_5
  - coinbar_1
  - onCrear: func

## CashTotalView
modules/cashRecord/components/CashRecordTotalView

![CashRecordTotalView](../images/modules/cashRecord/components/CashRecordTotalView.png)

### Props
- cashRecord: Object:
  - total_amount
  - carry_forward_amount
  - diff_amount
- onRegisterCashRecord: func
