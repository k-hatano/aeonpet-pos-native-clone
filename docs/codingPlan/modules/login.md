# Table of contents
- [LoginUserForm](#loginuserform)
- [LoginSelectShopForm](#loginselectshopform)
- [LoginSelectCashierForm](#loginselectcashierform)
- [LoginConfirmView](#loginconfirmview)

# Views

## LoginUserForm
modules/login/components/LoginUserForm

<img src="../images/modules/login/components/LoginUserForm.png" width="800" height="580"/>

### Props
- onLogin: func
    - Execute when [ログイン] Button is pushed

Note : Implementation of data entry is not necessary now.

### Translations

|ja|en|
|---|---|
|ログイン情報を入力してください|Please enter login information|
|接続先URL|URL|
|ログインID|Login ID|
|パスワード|Password|
|デバイスID|UDID|
|ログイン|Login|

## LoginSelectShopForm
modules/login/components/LoginSelectShopForm

<img src="../images/modules/login/components/LoginSelectShopForm.png" width="800" height="580"/>

### Props
- onNext: func
    - Execute when [次へ] Button is pushed

Note : Implementation of data entry is not necessary now.

### Translations

|ja|en|
|---|---|
|ログインする店舗を選択してください|Please select a shop|
|選択|Select|
|デバイス名を登録してください|Please enter the device name|
|次へ|Next|

## LoginSelectCashierForm
modules/login/components/LoginSelectCashierForm

<img src="../images/modules/login/components/LoginSelectCashierForm.png" width="800" height="580"/>

### Props
- onNext: func
    - Execute when [次へ] Button is pushed
- onBack: func
    - Execute when [戻る] Button is pushed
- cashiers: array of object
    - name: string
        - Show at POS1, POS2, ...

### Translations

|ja|en|
|---|---|
|POSを選択してください|Please select a device|
|戻る|Back|
|次へ|Next|

## LoginConfirmView
modules/login/components/LoginConfirmView

<img src="../images/modules/login/components/LoginConfirmView.png" width="800" height="580"/>

### Props
- onLogin: func
    - Execute when [ログイン] Button is pushed
- onCancel: func
    - Execute when [キャンセル] Button is pushed
- shopName: string
    - Show at 芝公園店 place
- posName: string
    - Show at POS1 place

### Translations

|ja|en|
|---|---|
|以下の店舗へログインします|Log in to the following|
|ログイン|Login|
|キャンセル|Cancel|
