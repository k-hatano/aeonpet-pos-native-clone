# Table of contents
- [ProductVariant](#productvariant)
- [ShopSearch](#shopsearch)
- [ForgotCard](#forgotcard)
- [SelectMember](#selectmember)
- [MemberInfo](#memberinfo)
- [CustomerSegment](#customersegment)
- [NumberPad](#numberpad)
- [Carendar](#calendar)
- [ReprintTaxFree](#reprinttaxfree)
- [SelectStaff](#selectstaff)

# Views

## ProductVariant
<img src="images/popups/ProductVariant.png" width="800" height="580"/>

## ShopSearch
<img src="images/popups/ShopSearch.png" width="800" height="580"/>

## ForgotCard
<img src="images/popups/ForgotCard.png" width="800" height="580"/>

## SelectMember
<img src="images/popups/SelectMember.png" width="800" height="580"/>

## MemberInfo
<img src="images/popups/MemberInfo.png" width="800" height="580"/>

## CustomerSegment
<img src="images/popups/CustomerSegment.png" width="800" height="580"/>

## NumberPad
<img src="images/popups/NumberPad.png" width="800" height="580"/>

## Calendar
<img src="images/popups/Calendar.png" width="800" height="580"/>

## ReprintTaxFree
<img src="images/popups/ReprintTaxFree.png" width="800" height="580"/>

## SelectStaff
common/components/widgets/PopupSelect

<img src="images/popups/SelectStaff.png" width="800" height="580"/>

### Props
- staffs: Array<Object> (from reducer) ⇒ SelectStaffList
- staff: Object ⇒ SelectStaffListItem
  - staff_code
  - name
