# pos_native コーディングルール Coding rule

## Componentについて
Componentは純粋なReactのUI部品である。
殆どの場合、Container経由で利用されるが、出来る限りReducerを意識しない作りにする。
ただし、操作状態の保持以外の目的でstateは利用しない。アプリの状態で変化する要素は、プロパティを利用する。

ComponentからContainerはできる限り参照しないこと。
Don't use Container from Component.

### Style
- componentに指定するstyleは、stylesディレクトリ以下にファイルを分けて定義する。
- Define the style of component in the file in the style directory.
- styleファイルは１コンポーネントに対して一つ作る。ファイル名は、コンポーネントと同じにする。
- A style file is created for one component. The file name should be the same as the component.
    - component : modules/xxxx/components/XxxxView.js
    - style : modules/xxxx/styles/XxxxView.js


```
// modules/xxxx/components/XxxxView.js
import styles from '../styles/XxxxView'

...
// modules/xxxx/components/XxxxView.js
<View>
  <Text style={styles.text} />
  <CommandButton style={styles.button} />
</View>
```

```
// modules/xxxx/styles/XxxxView.js
import { StyleSheet } from 'react-native'
export default StyleSheet.create({
  text: {
    marginLeft: 36,
    marginTop: 32,
    marginRight: 37,
    height: 126
  },
  button: {
    width: 272,
    height: 64
  }
})
```

- iOSとWindowsで別のstyleを利用する必要がある場合、以下のファイル構成とする。
- If you need to use a different style on iOS and Windows, make the following file structure.
    - component : modules/xxxx/components/XxxxView.js
    - common style : modules/xxxx/styles/XxxxView.coomon.js
    - ios style : modules/xxxx/styles/XxxxView.ios.js
    - windows style : modules/xxxx/styles/XxxxView.windows.js

```
// modules/xxxx/components/XxxxView.js
import styles from '../styles/XxxxView'

...
// modules/xxxx/components/XxxxView.js
<View>
  <Text style={styles.text} />
  <CommandButton style={styles.button} />
</View>
```

```
// modules/xxxx/styles/XxxxView.coomon.js
export default {
  text: {
    marginLeft: 36,
    marginTop: 32,
    marginRight: 37,
    height: 126
  },
  button: {
    width: 272,
    height: 64
  }
}
```

```
// modules/xxxx/styles/XxxxView.ios.js
import { StyleSheet } from 'react-native'
import commonStyle from './XxxxView.coomon'
import mergeStyle from '../../utils/mergeStyle'

export default StyleSheet.create(
  mergeStyle(
    commonStyle,
    {
      button: {
        width: 300
      }
    }
  )
)
```

```
// modules/xxxx/styles/XxxxView.windows.js
import { StyleSheet } from 'react-native'
import commonStyle from './XxxxView.coomon'

export default StyleSheet.create(commonStyle)
```

### PropTypes
必ずPropTypesを書く。
Should define propTypes

```
import PropTypes from 'prop-types'

export default class XxxxView extends Component {
    static propTypes = {
        xxxxs: PropTypes.array,
        onSelectXxxx: PropTypes.func,
        selectedXxxxType: PropTypes.number
    }

    ......
}
```

## Containerについて
ComponentにReducerの機能を付与したラッパーコンポーネントがContainerである。
mapDispatcherToPropsには、本来の役割であるdispatchを実行する以外にも、簡単なロジックを実装することができる。リポジトリやプリンタの利用もここに書く。

mapDispatcherToProps can have simple process. For example, access to a repository, use a printer.

```
// Example
const mapDispatchToProps = dispatch => ({
  onComplete: async (balances) => {
    try {
      const builder = new BalanceReceiptBuilder()
      const receipt = builder.buildBalanceReceipt(balances, new Date())
      const ejournal = makeBalanceEJournal(receipt, balances, Math.floor(new Date().getTime() / 1000))
      await loading(dispatch, async () => {
        await BalanceRepository.bulkPush(balances)
        await EJournalRepository.save(ejournal)
        await PrinterManager.print(receipt)
        await EJournalRepository.saveIsPrintedById(ejournal.id)
        await EJournalRepository.pushById(ejournal.id)
      })
      NoticeView.show('Success')
    } catch (error) {
      NoticeView.show('Error')
    }
  }
})
```

## Repositoryについて
DB/APIへのアクセスは、リポジトリクラスを通して行う
Access DB/API via repository class.

DB/APIアクセスのメソッドは、必ず非同期(async)メソッドとする。
Methods for access DB/API must be async.

DB/APIアクセスメソッドの引数と返り値は、必ずJSONで表現可能な型とする。
Argument and return value of repository methods are simple js object (not a class).

リポジトリは、以下の構成とする。

- repositories/XxxxRepository
    - インタフェースとなるクラス。メソッドは全てstaticとする。
    - 状態に応じて実装を切り替えられるよう、このリポジトリ内では実装は持たず、実装用のリポジトリクラスに処理を移譲する。
    - 実装用のリポジトリクラスのインスタンスをstatic変数として持つ
- repositories/standard/StandardXxxxRepository
    - 通常利用するリポジトリ。実際にデータベースや
- repositories/sample/SampleXxxxRepository
    - 開発モードのときのみ、ログイン画面に表示されるSample Modeボタンをタップして画面に入ると、このリポジトリが利用される。
    - 固定値の配列を返す、変数内にデータを保存するなど、画面を動作させるのに最低限必要な機能のみをダミーで実装する。
- repositories/training/TrainingXxxxRepository
    - トレーニングモードのときのみ利用する
    - トレーニングモードの仕様に従い、適宜ダミーデータを返すなどの実装を行う。

ToDo : 実装の切替方法
ToDo : サンプルデータの書き方

### Example

```
// modules/xxxx/repositories/XxxxRepository.js
import SampleXxxxRepository from './sample/SampleXxxxRepository'
import StandardXxxxRepository from './standard/StandardXxxxRepository'

export class XxxxRepository {
  static _implement

  static async findAll() {
    return this._implement.findAll()
  }

  async save(xxxx) {
    return this._implement.save()
  }
}

// TODO
```

## ビジネスロジック
Componentにはロジックは書かない。
Don't put a logic in component.

ここで言うロジックは、計算処理の他、API/DBアクセス、設定保存などを含む。
The logic contains calculation, accessing to API/DB and Save settings.

ロジックは、models.js、あるいはmodelsディレクトリ以下のファイルに書く。
Put a logic in models.js or under models directory.

Containerにはシンプルなロジックのみ書いてもよい。
Can put only simple logic in Container.

また、カート、棚卸しなど状態を持つ複雑なビジネスロジックはimmutable.jsを利用して、
models以下に個別のクラスを作成する。
Use immutable.js for complex business logic. Put it in modules/{moduleName}/models directory.

## ロギング
何らかのエラーが発生した場合、あるいは重要な操作を行ったら、ログに記録する。
ログ記録には common/utils/logger を利用する。

loggerにはレベルに応じて５種類のログ記録メソッドが用意されている。それぞれ、以下の様に使い分ける。

|メソッド|用途|
|:---|:---|
|debug|開発時のみ利用したい様な細かい情報を記録する|
|info|重要な操作など、記録しておいたほうが良い操作などを記録する|
|warning|想定内だが、後の運用に悪影響を与える可能性のあるエラーを記録する|
|error|想定外だが、ハンドリング可能なエラーを記録する|
|fatal|想定外かつ復帰処理が困難なエラーを記録する|

## 共通処理
### 時間の扱い
通常のDate関数はデバッグモード(ブラウザデバッグ機能をOnにしたとき)とそれ以外の時で挙動が異なる。
そのため、開発時は上手く動いていたが、リリースビルドで上手く動かないということがよく起こる。

日時を扱う場合は、必ずmoment.jsを利用する。こちらは、どちらの環境でも問題なく動き、タイムゾーンもデバイス設定に自動で合う。

<!-- TODO 日時のフォーマットをさらに共通化する仕組みを作って規約に載せる -->

日時を永続化する場合は、必ずunix_timeとしてinteger型で保存する。

引数などで日時を利用する場合は、Date型に変換した上で取り回すことを推奨するが、実際に扱いやすい方を優先する。

### 配列の扱い
underscore.jsを利用しているので、積極的に利用する。ただし、標準のArrayクラスのメソッドで十分であれば、そちらを優先的に利用する。

## 命名規約 Naming
### Method
- ローワーキャメルケース
- 非同期関数（async関数）であることがわかりづらいメソッドの場合、メソッド名はAsyncで終わる
  - 例 : loadAsync, createAsync
  - Repositoryのメソッドは全てasyncなので、メソッド名にはつけなくて良い
- private、protectedに相当するメソッドは、_から始める
  - 例 : _setItem
  
### Variable
- ローワーキャメルケース

### Class
- Upper Camel Case

### Component

- ページ Page
    - XxxxPage
- ボタンが配置されたComponent Component for buttons
    - XxxxPanel
- 入力のためのComponent Component for input
    - XxxxForm
- リスト
    - XxxxList
- リストの要素 Element of list
    - XxxxListItem
- その他の、moduleに配置されたComponent Other component in modules
    - XxxxView

クラスファイルは /modules/{moduleName}/components/ に配置する。
Put the class in /modules/{moduleName}/components/

### ComponentProperty

- 何かを実行するイベント Event for executing something
    - onChangeXxxx (Xxxx is verb)

プロパティ名は、コンポーネントの外側の処理を意識しないこと。

### Key of styles
スタイルのキーは以下を意識して名前をつける

- 適用対象のUIパーツ名やパーツの属性の名前として、styleの値を示す名前はつけない。
  - OK : submitButton, mainText
  - NG : buttonSize, boldText
- containerはreduxのcontainerと紛らわしいので使わない。また、wrapperもプログラミング用語と紛らわしいので使わない。
  - OK : layoutRoot, xxxxLayout, xxxxArea
  - NG : xxxxContainer, xxxxWrapper

### Container
Componentと同じ名前
Same name as component

Containerサフィックスは使用しない。(admin_webと合わせるため)
Don't use "Container" suffix. (To match with admin_web)

クラスファイルは /modules/{moduleName}/containers/ に配置する。
Put the class in /modules/{moduleName}/containers/

サイズや色など、見た目に関する情報は出来る限りstyleファイルに定義する。

### Repository
クラス名には、Repositoryサフィックスをつける。
Add "Repository" suffix

クラスファイルは /modules/{moduleName}/repositories/ に配置する。
Put the class in /modules/{moduleName}/repositories/

### RepositoryMethod

- DBを検索する Search for DB
    - findByXxxx (Xxxx is condition name)
    - findAll (Get all data without condition)
- DBに保存する Save into DB
    - save (Save single object)
    - bulkSave (Save array of object)
    - updateXxxxById(id, value) (Update specified property of existing data.)
- DBから削除する
    - clearAll
    - clearByXxxx (Xxxx is condition name)
- サーバから検索する Search for Server
    - fetchByXxxx (Xxxx is condition name)
- サーバに保存する。 Save to server
    - push (Save single object)
    - bulkPush (Save array of object)
- サーバからデータをダウンロードする。 Download to DB from server
    - syncRemoteToLocal
- DBのデータをサーバにアップロードする。 Upload to server from DB
    - syncLocalToRemote
    - syncLocalToRemoteById

### Entity
エンティティ名には、Entityサフィックスをつける。
Add "Entity" suffix

クラスファイルは /modules/{moduleName}/repositories/entities/ に配置する。
Put the class in /modules/{moduleName}/repositories/entities/

### ActionCreator
名前の最初の単語は動詞にする
The first word of actionCreator name is a verb.

Example
- selectProduct
- updateAmount
- listProducts

## JS一般 JS Generals
- EcmaScriptのstage2の文法まで利用可能とする (現在のプロジェクトのBabelの設定で利用できる範囲は全て利用して良い)
- 構文チェックツールとしてstandardを導入しているので、このツールで警告が出るコードの書き方はNG
  - ただし、ルールを合わせることによって分かりにくいコードになる場合は、部分的に警告を抑制するコメントを入れて良い
<!-- TODO standardの利用方法を書く -->

### できるだけ短縮形を利用する
短縮形を利用できる場合は、積極的に利用する。

#### 短縮形 - 論理演算子を利用する
- Not Good : `object.property ? object.property : 'default value'`
- Good : `object.property && 'default value'`
- Not Good : ``


#### 短縮形 - if判定とbooleanの扱い
できる限り冗長にならないようにする。
- boolean変換が必要ない場合は、明示的にしなくて良い
  - NG : `if (order.is_taxfree === 1)`
  - OK : `if (order.is_taxfree)`
- 明示的なbooleanへの変換が必要な場合は、!!を用いる。（Boolean()も非推奨だが許可する）
  - NG : `cart.setIsTaxfree(order.is_taxfree === 1 ? true : false)`
  - OK (Recommended) : `cart.setIsTaxfree(!!order.is_taxfree)`
  - OK : `cart.setIsTaxfree(Boolean(order.is_taxfree))`
