# ワークフロー Work flow

## Git
### ブランチ一覧
|ブランチ名|役割|
|:---|:---|
|master|最終的にリリースする対象のブランチ|
|feature/xxxx|機能追加用の作業ブランチ。通常xxxxにはチケットコードを入れる|
|milestone/v0.n.m|マイルストーンに紐づくブランチ。各トピックブランチはこれにマージする。|

### ルール
- 他人と編集対象ファイルがぶつかると、コンフリクトが起こる。このコンフリクトの解消は実装担当者のタスクとする。
  - この時、できればリベースによりコンフリクトを解消する。できなければ、マージでも構わない。

## 実装タスクの進め方
- Backlogで担当者となっているチケットの内、マイルストーンの早いものから着手開始する。
- 着手開始した時点で状態を「処理中」に変更する
- 実装対象のチケットに対応したマイルストーンブランチから、作業ブランチを作成する
- 適当なタイミングでコミットし、GitLabにpushする。必ず日の終りにはpushすること。翌日体調を崩しても引き継げるように。
- 最初にpushしたタイミングでGitLab上でマージリクエストを作成しておく。
  - リクエスト先は該当のマイルストーンに紐付いたマイルストーンブランチとする
  - この時、まだ作業完了していないならタイトルの最初に [WIP] と書いておく。また、この場合担当者は自分としておく。
- 自身で軽い動作確認を行い、実装完了したと判断したら以下の作業を行う
  - GitLabのマージリクエストの担当者を実装責任者（久米 or 山口）に変更し、タイトルの [WIP] を削除する
  - Backlogのチケットの状態を「処理済み」にして、担当者をGitLabの担当者と同じくする
  - コードレビュー担当一覧シートに記入する
    - https://docs.google.com/spreadsheets/d/1cA3ecIl7Nbbp3JNedH2dvpZNj9EowhEiQNaKMOvUwrQ/edit#gid=0
- 朝会などで久米が記入されたマージリクエストに事前レビュー担当者を割り振る
- 事前レビュー担当者はコードレビュー会までにマージリクエストの内容を読み、指摘を入れる
  - 指摘点がない場合は、「問題ありません」などのコメントを入れておく。
- レビュー会でレビューを行い、修正点があれば担当者に差し戻す。
  - BackLogのチケットとマージリクエストの担当者を実装担当者とする
  - 直ぐに直るような場合はBacklogの担当者変更は省略することもある
- 指摘点を修正したら再びBackLogのチケットとマージリクエストの担当者を実装責任者とする
- 実装責任者は修正内容を確認し、OKだったらマージを行う。NGだった場合は再度実装担当者に差し戻す。
  - マージを行った時点で、Backlogの課題の担当者をテスト担当者（坂元）に設定する。
- 実装責任者や定期的にJenkinsでアプリのビルドを行い、テスト担当者に最新のビルドの番号を通知する。この時の対象ブランチはマイルストーンブランチ。
- テスト担当者はアプリのビルド通知を受け取った後、テストを行い、問題なければBacklogの課題のステータスを「完了」とする

## リリースの進め方
Backlogのマイルストーン管理でリリース日を管理し、リリース日毎に以下の作業を行う。
- マイルストーン内の全ての課題が「完了」させる。（テスト担当者が全て確認完了する）
  - テストでNGが出た場合は、実装をどうするかの判断が必要となる。
    - 修正 : 単純ミスが原因の簡単なバグとわかった場合は、修正を行う。
    - 巻き戻し : 短時間での修正が難しい、かつそのままリリースすると問題になる場合は実装の巻き戻し(revert)を行う
    - そのまま : 問題が残っていてもお客様への説明で済む場合は、実装はそのままとする。
- 実装責任者が以下の作業を行う。
  - マイルストーンブランチにバージョン変更のコミットを追加する。
  - マイルストーンブランチをmasterにマージする
  - masterブランチをお客様リリース用にビルドし、外部からインストールできる場所に設置、ビルド番号をPMに伝える

## 新規機能開発タスクの進め方
完全新規の機能を作る場合や大きな改修が入る場合は、実装を５ステップに分けて実装していく。
- 1. 画面実装
- 2. 画面動作
- 3. 帳票出力
- 4. ロジック作成
- 5. API/DBアクセス

最後以外の全てのステップにおいて、それぞれの実装計画を書く。
書き方に関しては[実装計画の書き方](codingPlan.md)を参照。

以下、各ステップの詳細を示す。

### 1. 画面実装 View
まずは、画面の見た目のみを作成する。
ただし、見た目が動的に変化する部分については、プロパティで外部から指定できるようにしておき、プロパティへの値のセットは定数値などで行う。

アプリ上で画面を確認するため、画面遷移は仮で実装する。
通常の画面遷移で表現が難しい場合や、部品のみの実装でページが無い場合などは、DebugPageに確認用のボタンを作る

#### 実装対象 Implementation target
- Components (modules/xxxx/components/* )
- styles (modules/xxxx/styles/* )
- Pages (pages/XxxxPage.js)
- Translations (ja.json, en.json)
- models
    - 値とラベルの変換(例: xxxTypeToLabel)と、MODULE_NAMEのみ
    - Only value to label conversion(Ex. xxxTypeToLabel) and MODULE_NAME.

### 2. 画面動作 View State
#### 実装対象 Implementation target
- reducer (modules/xxxx/reducer.js)
- action (modules/xxxx/actions.js)
- Container (modules/xxxx/containers/* )

### 3. 帳票作成 Implement receipt
レシート出力のある機能の場合、レシート作成メソッドを作る。

#### 実装対象 Implementation target
- ReceipBuilder (modules/xxxx/models/XxxxReceiptBuilder)
- Unit test for receipt builder (modules/xxxx/models/__tests__/XxxxReceiptBuilder-test.js)

### 4. ロジック実装 Implement logic
実際に画面が一通りの機能を果たすための機能を実装する。
ただし、APIアクセス/DBアクセスについては、サンプルリポジトリの実装とする。
5でStandardREpositoryを実装すると、全ての機能が実装される、という状態まで持っていく。

#### 実装対象 Implementation target
- models (modules/xxxx/models.js, modules/xxxx/models/* )
- page (pages/XxxxPage)
- container (modules/xxxx/containers/* )
- Repositories (modules/xxxx/repositories/* )
- And others.

### 5 API/DB本接続
#### 実装対象 Implementation target
- StandardRepository (modules/xxxx/repositories/standard/StandardXxxxRepository.js )
- Entities (modules/xxxx/repositories/entities/* )

