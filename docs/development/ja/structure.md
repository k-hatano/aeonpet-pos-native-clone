# 構成

## ファイル構成 File Structure
- docs
  - codingPlan : [実装計画](./codingPlan.md)を記録する
  - development : 開発資料（マークダウン形式）
  - fileTemplates : PhpStorm(WebStorm)で利用する想定のテンプレートを格納する
- ios : iOS向けのアプリをビルドするためのプロジェクト一式が入っている。最初にreact-nativeコマンドで生成し、後に手動で手を加えている。（プリンタ接続コードの追加など）
- windows : Windows向けアプリをビルドするためのプロジェクト一式が入っている。iOSと同様。
- migrations : マイグレーションファイル一覧。sequelizeコマンドで生成できる。
- src : jsのソースコード  
  - assets : リソースファイルを格納する。主に画像ファイル
  - common : 全てのモジュールから共通で利用されうるファイルを格納する。
    - TODO commonのディレクトリを整理してドキュメントに記載する
  - modules : 機能毎に分割されたモジュールを格納する。各モジュール内に何があるかは、次節を参照。
  - pages : 各ページクラス(ルートになるコンポーネントクラス)を格納する
  - samplePages : サンプル用のページクラス
  - App.js : 全てのコンポーネントのルートになるファイル。この中に利用する全てのページへの参照が書かれている。
  - `**/__tests__` : ユニットテストを格納する。モックする対象ファイルと同じ階層にそれぞれ作成する。
  - `**/__mocks__` : ユニットテストで利用するモックファイルを格納する。モックする対象ファイルと同じ階層にそれぞれ作成する。

## 各モジュールのファイル構成
- actions.js : reduxのアクションを定義する
- reducer.js : reduxのreducerを定義する。ここで定義したreducerはsrc/common/store.jsでstoreに登録される。
- models.js : 定数定義、Repositoryや他モジュールのファイルへの参照の必要のない単純なロジックなどを定義する。
- services.js : Repositoryや他モジュールへの参照を持つロジックを定義する。元々models.jsの役割だったが、Repositoryなどとの相互参照問題を解決するためにファイルを分けた。
- samples.js : サンプルモードやユニットテストで利用するサンプルデータを定義する
- components : コンポーネントクラスを格納するa
- componentSamples : ComponentSamplePageで表示するためのサンプルデータ定義を格納する
- containers : componentsに対するreduxコンテナを格納する。
- styles : コンポーネントで利用するスタイルファイルを格納する
- models/ : クラスにまとめた方が良い処理を、このディレクトリの下に配置する
  - XxxxManager.js : あるクラスの制御を含めてラップし、使いやすくしたクラス(ex. PrinterManager)、またはあるオブジェクトを外側から操作し、管理するクラス(CartManager)
  - XxxxReceiptBuilder.js : 元になるデータ(order, cashier_balanceなど)からレシートデータを生成する。
  - Xxxx(モデル名).js : Cart.jsやTax.jsなど、モデルそのものを表すクラスは、モデル名をそのままファイル名とする。
    - es6標準のクラスでも良いし、immutable.jsのRecordクラスを継承したクラスでも良い。
    - immutable.jsを使わなくても、reducerに格納する想定のクラスは出来る限りimmutableに作ること。
- repositories/ : リポジトリクラスを格納する。
  - entities : StandardRepositoryで利用するORマッパーのエンティティを定義するクラスを格納する
  - standard : 通常のリポジトリクラス（API/DBアクセスを行うクラス）を格納する
  - sample : サンプルモードで利用するリポジトリクラスを格納する
  - training : トレーニングモードで利用するリポジトリクラスを格納する
