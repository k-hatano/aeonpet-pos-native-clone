# 実装計画の書き方
新しい機能開発においては、事前に実装計画を記載する。
実装計画は、本リポジトリのdocs/codingPlanディレクトリ内にマークダウン形式で記載する。

以下、実装ステップ毎に書くべき内容を示す。
(実装のステップに関しては、[ワークフロー](./workflow.md)を参照)

## 1. 画面実装 View
画面をどの様な部品に分割するか、下記のフォーマットで書く

### パス Path
```
#### Path
```

### 画面画像 View Image

```
#### Image
![View Name](image path)
```

### プロパティ Properties

```
#### Props
- onXxxxChange: func
- propertyName2: int
- propertyName3: Object
    - childPropertyName1: number
    - childPropertyName2: string
```

### 包含するコンポーネント Contains components

包含するコンポーネントの一覧と、親から子へのプロパティのマッピングを書く

```
#### Contains
- [ChildView1](/modules/xxxx#childview1)
    - childViewProperty1: propertyName1
    - childViewProperty2: propertyName2
```

### 画面遷移 View Transitions

```
#### Transitions
- Go to YyyyPage : onXxxxChanged
```

## 2. 画面動作 View State
reducerで管理する状態を書く。
ステート名: 型 の形式で書く。型はentity一覧を参照する形で書く

```
#### States
- xxxx: entity_name[]
- selectedXxxx: entity_name
- selectedXxxxType: int
```

## 3. 帳票作成 Implement receipt
レシート情報作成の元になるサンプルデータと、出力されるレシート（テキスト表現）を書く
Write input sample data and output receipt text.

````
## Receipts
### BalanceReceipt
#### Sample1

sample data

```
// balances
[
  {
    balance_type: BALANCE_TYPE.DEPOSIT,
    amount: 12000,
    currency: 'jpy',
    payment_method_name: '現金',
    balance_reason_name: '小口入金'
  }
]

// createdAt
new Date("2017-07-07 09:30:00")
```

output

```
                       入金

店舗1
レジ1
2017-07-07 09:30:00

小口入金                                ￥12,000

             ----------------------

現金                                   ￥12,000

                                    担当:担当者1

```
````

## 4. ロジック実装 Implement logic
リポジトリのメソッドの名前とインタフェース（引数と返り値の型）を書く。
型はエンティティを参照する。
Write repository's name and type of args and return value.
The type refers to entity.

```
### BalanceReasonRepository
- findByBalanceType
    - args
        - balanceType: int
    - return: cashier_balance[]

### BalanceRepository
- bulkPush
    - args
        - balances: cashier_balance[]
    - return: none
```

## 5 API/DB本接続
なし

