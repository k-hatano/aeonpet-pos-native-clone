# POS Native

## Requirements

In addition to the requirements from the Getting Started guide:

- NodeJS 6.9.x & NPM 3.x
- Visual Studio 2015 Community
- Windows 10 SDK Build 10586 or later

If this is your first time using `Universal Windows Platform (UWP)`, you might have to install the SDK by opening the solution file in the `windows` folder in `Visual Studio`. After opening the solution, right click the Solution in the `Solution Explorer` and select the option labeled `"Install Missing Components"`. You may have to shutdown `Visual Studio` to continue the installation.

### Install React Native

- Install *React Native CLI*

```sh
npm i -g react-native-cli
```

- Install YARN (_optional_)

```sh
npm i -g yarn
```

- Install Carthage

```sh
brew install carthage
# or
# install manual from https://github.com/Carthage/Carthage/releases
```

### Setting up

- Install project's dependencies:

```sh
npm i
# or
yarn
```

- Install Swift frameworks

```sh
carthage update --platform iOS
```

#### Run React Native app on Windows

- Initialize Windows project by executing this command in the root of project directory:

```sh
react-native windows
```

*Note:*
When you was asked for overwriting `index.windows.js`, please choose `n` to reject the change.

- Run React Native app for Windows:

```sh
react-native run-windows
```

#### Run React Native app on Android

- Initialize Android project by executing this command in the root of project directory:

```sh
react-native upgrade
```

- Run React Native app for Android:

```sh
react-native run-android
```

#### Run React Native app on iOS

- Initialize iOS project by executing this command in the root of project directory:

```sh
react-native upgrade
```

- Run React Native app for iOS:

```sh
react-native run-ios
```

If you got the error "Error watching file for changes: EMFILE", fix it as follows: [brennanMKE's Gist](https://gist.github.com/brennanMKE/f6aa55b452ecda2f4c7a379e21647c88).

### Library Notes

#### React Native Vector Icons

For using [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) package, please follow these under instructions:

**Automatically link**

- With React Native 0.27+

```sh
react-native link react-native-vector-icons
```

- With older versions of React Native, you need `rnpm` (`npm install -g rnpm`)

```sh
rnpm link react-native-vector-icons
```

**Manually link**

Please read the instructions [here](https://github.com/oblador/react-native-vector-icons#option-manually)

#### React Native Device Info

For using [react-native-device-info](https://github.com/rebeccahughes/react-native-device-info) package, please follow these under instructions:

**Automatically link**

- With React Native 0.27+

```sh
react-native link react-native-device-info
```

- With older versions of React Native, you need `rnpm` (`npm install -g rnpm`)

```sh
rnpm link react-native-device-info
```

**Manually link**

Please read the instructions [here](https://github.com/rebeccahughes/react-native-device-info#manually-link)

#### React Native File System v1

For using [react-native-filesystem-v1](https://github.com/hnq90/react-native-filesystem) package, please follow these under instructions:

**Automatically link**

- With React Native 0.27+

```sh
react-native link react-native-filesystem-v1
```

- With older versions of React Native, you need `rnpm` (`npm install -g rnpm`)

```sh
rnpm link react-native-filesystem-v1
```

**Manually link**

- Windows:
  - Open the solution in `Visual Studio` for your Windows apps.
  - Right click your in the Explorer and click `Add` > `Existing Project...`.
  - Navigate to `./<app-name>/node_modules/react-native-filesystem-v1/windows/RNFileSystem` and add `RNFileSystem.csproj`.
  - Right click on your React Native Windows app under your solutions directory and click `Add` > `Reference...`.
  - Check the `RNFileSystem` you just added and press `Ok`.
  - Open `MainPage.cs` in your app and edit it like below:

```c#
using RNFileSystem;

get
  {
      return new List<IReactPackage>
      {
          new MainReactPackage(),
          new RNFileSystemPackage(),
      };
  }
```

#### React Native Sound

For using [react-native-sound](https://github.com/zmxv/react-native-sound) package, please follow these under instructions:

**Automatically link**

- With React Native 0.27+

```sh
react-native link react-native-sound
```

- With older versions of React Native, you need `rnpm` (`npm install -g rnpm`)

```sh
rnpm link react-native-sound
```

**Manually link**

- Please read the instructions [here](https://github.com/zmxv/react-native-sound#manual-installation-on-ios)

### Deploy Notes

#### Windows

**1. Generate offline bundle**

- Generate the `index.windows.bundle` file into `windows\pos_native\ReactAssets` with the following command:

```sh
react-native bundle --platform windows --entry-file index.windows.js \
  --bundle-output windows\pos_native\ReactAssets\index.windows.bundle \
  --assets-dest windows\pos_native\ReactAssets \
  --dev false
```

**2. Setup Solution**

- Using `Bundle Configurations` in solution (for release choose `ReleaseBundle`):

![Bundle](https://github.com/Microsoft/react-native-windows/blob/master/docs/img/ConfigurationBundle.png?raw=true)

**3. Create app package**

- Following step by step:
  - In `Solution Explorer`, open the solution for your UWP app project.
  - Right-click the project and choose `Store` -> `Create App Packages`. If this option is disabled or does not appear at all, check that the project is a UWP project.

![Packages](https://docs.microsoft.com/en-us/windows/uwp/packaging/images/packaging-screen2.jpg)

- Select `Yes` in the first dialog asking if you want to build packages to upload to the Windows Store, then click `Next`.
- Make sure you select all three architecture configurations (x86, x64, and ARM) in the `Select and Configure Packages` dialog. That way your app can be deployed to the widest range of devices. In the `Generate app bundle` listbox, select `Always`. This makes the store submission process more simple because you will only have one file to upload (`*.appxupload`). The single bundle will contain all the necessary packages to deploy to devices with each processor architecture.

![Architecture](https://docs.microsoft.com/en-us/windows/uwp/packaging/images/packaging-screen5.jpg)

- Read detail: [Create An App Package](https://docs.microsoft.com/en-us/windows/uwp/packaging/packaging-uwp-apps#create-an-app-package)

### Database Migration

- Make sure `sequelize-cli` is installed by:

```sh
yarn install
```

- Init migration folder

```sh
yarn run sequelize init:migrations
```

- Create migration files

```sh
yarn run sequelize migration:create
```

- Add your new migration files to `src/common/migrations.js`

```js
export default [
  { name: '20170628090007-unnamed-migration', data: require('../../migrations/20170628090007-unnamed-migration.js') },
  { name: '20170703024029-unnamed-migration', data: require('../../migrations/20170703024029-unnamed-migration.js') }
]
export default migrations
```

- How to use:

```js
import Migrator from './common/utils/migrator'

Migrator.up()
Migrator.down() // or Migrator.down({ step: 1 })
// By default, it will rollback the latest migration
// If you want to rollback more steps, pass your steps into Migrator.down function's argument
// Example:
Migrator.down({ step: 3 })
// If you want to refresh (rollback and migrate) your migrations, please use `refresh`.
// By default, `Migration.refresh()` will rollback latest 1000 migrations before run re-migrate.
// If you want to refresh other steps, pass your steps into Migrator.refresh function's argument
// Example:
Migration.refresh({ step: 10 })
```

- Migration file template

```js
module.exports = {
  up: (queryInterface, Sequelize) => {
    // Logic for transforming into the new state
  },

  down: (queryInterface, Sequelize) => {
    // Logic for reverting the changes
  }
}
```

- Migration Functions

```js
createTable(tableName, attributes, options)
dropTable(tableName, options)
dropAllTables(options)
renameTable(before, after, options)
showAllTables(options)
describeTable(tableName, options)
addColumn(tableNameOrOptions, attributeName, dataTypeOrOptions, options)
removeColumn(tableNameOrOptions, attributeName, options)
changeColumn(tableName, attributeName, dataTypeOrOptions, options)
renameColumn(tableName, attrNameBefore, attrNameAfter, options)
addIndex(tableName, attributes, options)
removeIndex(tableName, indexNameOrAttributes, options)
addConstraint(tableName, attributes, options)
removeConstraint(tableName, constraintName, options)
showConstraint(tableName, options)
```

See [More information](http://docs.sequelizejs.com/manual/tutorial/migrations.html)

### Other Notes

**Remove cache of iOS app:**

```sh
react-native start --reset-cache
```

**React Native Windows Components Compatibility**

- See [Core Parity Status](https://github.com/Microsoft/react-native-windows/blob/master/docs/CoreParityStatus.md)

### Ngrok - Expose your webserver to the internet

**Problem when connect to webserver from your private IP?**

- If you got problem when connect to your webserver with private IP in React Native app, there are two solutions:
  - Use public IP instead of private IP.
  - Use `Ngrok` to expose it to the internet. (Please read more about `ngrok` on their website: [ngrok.com](https://ngrok.com/))

**Using Ngrok**

- Clone this repository:

```sh
git clone https://github.com/rikkeisoft/ngrok_clients
```

- Follow the instruction in the README.
- Use the url from Ngrok as base url for your app's API endpoint.
