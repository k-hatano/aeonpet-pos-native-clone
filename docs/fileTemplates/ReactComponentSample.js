#set( $name = $NAME.replace("Sample", "") )
import ${name} from '../components/${name}'

export default {
  name: '${name}',
  component: ${name},
  properties: [
    {
      title: 'PropertySample1',
      property: {
        name: 'TestName'
      }
    },
    {
      title: 'PropertySample2',
      property: {
        name: 'AnotherName'
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
