import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'
import BasePage from 'common/hocs/BasePage'
import styles from './styles/${NAME}'

class ${NAME} extends React.Component {
  static propTypes = {
    name: PropTypes.string
  }

  render () {
    return (
      <BasePage>
        <View style={styles.layoutLeft} />
        <View style={styles.layoutRight} />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  // onComplete: async (balances) => {
  //   try {
  //     await Repository.findAll(ejournal)
  //     Actions.xxxPage()
  //   } catch (error) {
  //     AlertView.show('Error')
  //   }
  // }
})

const mapStateToProps = state => ({
  // name: state[XXX_MODULE_NAME].name
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...dispatchProps,
    ...stateProps
    // name: ownProps.name + stateProps.name
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(${NAME})
