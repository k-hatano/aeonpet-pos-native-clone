import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import * as _ from 'underscore'

const ${NAME}Record = Record({
  name: '',
  items: List()
})

/**
 * @property {string} name
 * @property {List} items
 */
export default class ${NAME} extends ${NAME}Record {
  constructor (params) {
    super({
      name: params.xxx_name
    })
  }

  addItem (item) {
    return this.set('items', this.items.push(item))
  }
}
