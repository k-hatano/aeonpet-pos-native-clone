import Sample${NAME} from './sample/Sample${NAME}'
import Standard${NAME} from './standard/Standard${NAME}'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class ${NAME} {
  static _implement = new Sample${NAME}()

  // static async findAll () {
  //   return this._implement.findAll()
  // }

  static async createSamples () {
    return this._implement.createSamples()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new Standard${NAME}()
        break

      default:
        this._implement = new Sample${NAME}()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('${NAME}', (context) => {
  ${NAME}.switchImplement(context)
})
