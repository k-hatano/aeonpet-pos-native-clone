import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/${NAME}'
import baseStyle from 'common/styles/baseStyle'

export default class ${NAME} extends React.Component {
  static propTypes = {
    name: PropTypes.string
  }
  
  render () {
    return (
      <View style={styles.layoutRoot} >
        <Text style={baseStyle.mainText}>
          {this.props.name}
        </Text>
      </View>
    )
  }
}
