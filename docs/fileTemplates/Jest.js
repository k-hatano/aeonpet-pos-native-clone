#set( $name = $NAME.replace("-test", "") )
// jest.mock('path/to/class')
// TODO Uncomment following 3 lines if use db
// jest.mock('react-native')
// jest.mock('react-native-filesystem-v1')
// jest.mock('common/DB')

import ${name} from '../${name}'

// TODO Uncomment following line if use db
// import Migrator from 'common/utils/migrator'

describe('${name} Test Sample', () => {
  beforeEach(async () => {
    // TODO Uncomment following line if use db
    // await Migrator.up()
  })

  afterEach(async () => {
    // TODO Uncomment following if use db
    // await Migrator.down()
  })

  it('Test case 1', () => {
    const actual = 1
    expect(actual).toBe(2)
  })
})
