import { connect } from 'react-redux'
import ${NAME} from '../components/${NAME}'
import { } from '../actions'
import { MODULE_NAME } from '../models'
import AlertView from '../../../common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  // onAction: async () => {
  //   try {
  //     const models = await ModelRepository.findAll()
  //     dispatch(listModels(models))
  //   } catch (error) {
  //     AlertView.show('Error')
  //   }
  // }
})

const mapStateToProps = state => ({
  // name: state[MODULE_NAME].name
})

// const mergeProps = (stateProps, dispatchProps, ownProps) => ({
//   ...ownProps,
//   ...stateProps,
//   ...dispatchProps,
//   onSelectAggregateType: async (checkSalesType) => {
//     dispatchProps.onAction(stateProps.xxxx)
//   }
// })

export default connect(mapStateToProps, mapDispatchToProps/* , mergeProps */)(${NAME})
