﻿using ReactNative;
using ReactNative.Modules.Core;
using ReactNative.Shell;
using RNDeviceInfo;
using RNFileSystem;
using RNSqlite2;
using RNSoundModule;
using System.Collections.Generic;

namespace pos_native
{
    class MainPage : ReactPage
    {
        public override string MainComponentName
        {
            get
            {
                return "pos_native";
            }
        }

#if BUNDLE
        public override string JavaScriptBundleFile
        {
            get
            {
                return "ms-appx:///ReactAssets/index.windows.bundle";
            }
        }
#endif

        public override List<IReactPackage> Packages
        {
            get
            {
                return new List<IReactPackage>
                {
                    new MainReactPackage(),
                    new RNDeviceInfoPackage(),
                    new RNFileSystemPackage(),
                    new RNSqlite2Package(),
                    new RNSoundPackage(),
                };
            }
        }

        public override bool UseDeveloperSupport
        {
            get
            {
#if !BUNDLE || DEBUG
                return true;
#else
                return false;
#endif
            }
        }
    }

}
