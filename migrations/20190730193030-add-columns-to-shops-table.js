'use strict'

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'grant_base_points_amount', {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: false,
        defaultValue: 1
      }
    )
    await queryInterface.addColumn(
      table,
      'grant_base_points', {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      }
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'grant_base_points_amount')
    await queryInterface.removeColumn(table, 'grant_base_points')
  }
}
