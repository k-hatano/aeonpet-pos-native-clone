'use strict';

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'cashier_cash_records',
      'withdrawal_amount',
      {type:Sequelize.DECIMAL(19, 4), defaultValue: 0}
    )
    await queryInterface.addColumn(
      'ejournals',
      'withdrawal_amount',
      {type:Sequelize.DECIMAL(19, 4), defaultValue: 0}
    )
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('cashier_cash_records', 'withdrawal_amount')
    queryInterface.removeColumn('ejournals', 'withdrawal_amount')
  }
};