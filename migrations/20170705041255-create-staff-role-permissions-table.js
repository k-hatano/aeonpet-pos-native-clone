'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('staff_role_permissions', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      staff_role_id: Sequelize.STRING,
      permission_code: Sequelize.STRING
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('staff_role_permissions')
  }
}
