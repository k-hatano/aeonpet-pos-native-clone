'use strict'

const table = 'bundle_patterns'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'discount_rate',
      Sequelize.DECIMAL(19, 4)
    )
    await queryInterface.addColumn(
      table,
      'incentive_type',
      Sequelize.INTEGER
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'discount_rate')
    await queryInterface.removeColumn(table, 'incentive_type')
  }
}
