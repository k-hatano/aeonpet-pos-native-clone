'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.renameColumn(
      'pos_order_properties',
      'options_raw',
      'options'
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.renameColumn(
      'pos_order_properties',
      'options',
      'options_raw'
    )
  }
}
