'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cashier_parameter_sets', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      is_print_receipt_header_logo: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      receipt_header_logo_image: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      receipt_header_message: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      is_print_zero_products: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      print_product_code_mode: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      is_print_point_info: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      is_print_tax_stamp: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      tax_office_name: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      is_print_staff: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      is_print_receipt_footer_logo: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      receipt_footer_logo_image: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      receipt_footer_message: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      is_print_bill_logo: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      bill_message: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      bill_logo_image: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      bill_proviso: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      total_receipt_print_num: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 1
      },
      is_print_product_segment_total: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      is_print_staff_total: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      is_taxfree: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('cashier_parameter_sets')
  }
}
