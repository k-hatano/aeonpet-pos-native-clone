'use strict'

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'is_taxfree',
      Sequelize.BOOLEAN
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(table, 'is_taxfree')
  }
}
