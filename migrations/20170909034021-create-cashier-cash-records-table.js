'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cashier_cash_records', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      cashier_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      staff_id: {
        type: Sequelize.STRING,
        allowNull: true
      },
      staff_name: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      data: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      currency: {
        type: Sequelize.STRING,
        allowNull: false
      },
      total_amount: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      deposit_amount: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      carry_forward_amount: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      mode: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      client_created_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('cashier_cash_records')
  }
}
