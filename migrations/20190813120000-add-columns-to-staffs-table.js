'use strict'

const table = 'staffs'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'staff_password', {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: '0000'
      }
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'staff_password')
  }
}
