'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('pos_order_properties', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      options_raw: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      default_value: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      required: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      sort_order: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('pos_order_properties')
  }
}
