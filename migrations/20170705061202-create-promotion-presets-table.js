'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('promotion_presets', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      title: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      promotion_preset_code: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      promotion_preset_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      discount_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      discount_value: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: false
      },
      discount_reason_id: Sequelize.STRING,
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('promotion_presets')
  }
}
