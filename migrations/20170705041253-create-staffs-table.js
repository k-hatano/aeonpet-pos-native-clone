'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('staffs', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      staff_code: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      staff_role_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      // sort_order: {
      //   type: Sequelize.INTEGER,
      //   allowNull: false
      // },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('staffs')
  }
}
