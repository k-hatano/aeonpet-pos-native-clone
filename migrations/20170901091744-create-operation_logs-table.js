'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('operation_logs', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      operation_name: Sequelize.STRING,
      operation_at: Sequelize.INTEGER,
      operation_type: Sequelize.INTEGER,
      staff_name: Sequelize.STRING,
      staff_code: Sequelize.STRING
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('operation_logs')
  }
}
