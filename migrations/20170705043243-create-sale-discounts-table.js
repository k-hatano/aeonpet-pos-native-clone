'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('sale_discounts', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      code: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      discount_rate: {
        type: Sequelize.DECIMAL(5, 4),
        allowNull: true,
        defaultValue: 0.0
      },
      discount_price: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: true,
        defaultValue: 0
      },
      product_tag_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      priority: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
      },
      start_at: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      end_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('sale_discounts')
  }
}
