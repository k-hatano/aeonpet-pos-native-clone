'use strict'

const table = 'cash_voucher'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    return queryInterface.createTable(table, {
      id: {
        primaryKey: true,
        allowNull: true,
        type: Sequelize.STRING(16)
      },
      barcode: {
        allowNull: true,
        type: Sequelize.STRING(128)
      },
      amount: {
        allowNull: true,
        type: Sequelize.DECIMAL(19, 4)
      },
      issuer: {
        allowNull: true,
        type: Sequelize.STRING
      },
      brand: {
        allowNull: true,
        type: Sequelize.STRING
      },
      payment_method_id: {
        allowNull: true,
        type: Sequelize.STRING(16)
      },
      issue_type: {
        allowNull: true,
        type: Sequelize.INTEGER(3)
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(table)
  }
}
