'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cashiers', {
      // ID
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      // レジコード
      cashier_code: {
        type: Sequelize.STRING(64),
        allowNull: true,
        defaultValue: null
      },
      // ステータス
      status: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      // レジ名
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      // 店舗ID
      shop_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      // デバイスID
      device_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      setting_data: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: true
      },
      // クライアント作成日時
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      // 更新時刻
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      // 削除時刻
      deleted_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('cashiers')
  }
}
