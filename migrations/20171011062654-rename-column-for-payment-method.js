'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.renameColumn(
      'payment_methods',
      'acquire_code',
      'acquirer_code'
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.renameColumn(
      'payment_methods',
      'acquirer_code',
      'acquire_code'
    )
  }
}
