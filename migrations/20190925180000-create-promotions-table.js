'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('promotion_messages', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      status: {
        type: Sequelize.INTEGER
      },
      integrated_incentive_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      promotion_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      promotion_code: {
        type: Sequelize.STRING,
        allowNull: false
      },
      priority: {
        type: Sequelize.INTEGER
      },
      start_at: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      end_at: {
        type: Sequelize.INTEGER
      },
      start_time: {
        type: Sequelize.INTEGER
      },
      end_time: {
        type: Sequelize.INTEGER
      },
      on_sunday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_monday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_tuesday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_wednesday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_thursday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_friday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_saturday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      campaign_message: {
        type: Sequelize.STRING,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('promotion_messages')
  }
}
