'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('stock_items', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      warehouse_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      warehouse_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      warehouse_code: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      product_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      product_variant_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      quantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('stock_items')
  }
}
