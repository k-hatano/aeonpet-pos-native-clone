'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('bundle_patterns', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      bundle_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      bundle_price: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: true
      },
      product_tag_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      establishment_condition: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      threshold_quantity: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: true
      },
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('bundle_patterns')
  }
}
