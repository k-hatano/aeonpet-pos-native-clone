'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('stock_taking_task_items', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      stock_taking_task_id: Sequelize.STRING,
      product_id: Sequelize.STRING,
      product_variant_id: Sequelize.STRING,
      quantity: Sequelize.INTEGER,
      quantity_diffs: Sequelize.INTEGER,
      stock_quantity: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('stock_taking_task_items')
  }
}
