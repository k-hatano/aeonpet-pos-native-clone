'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('payment_methods', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      payment_method_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
      },
      sort_order: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('payment_methods')
  }
}
