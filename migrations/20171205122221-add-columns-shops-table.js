'use strict';

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'region',
      Sequelize.STRING
    )
    await queryInterface.addColumn(
      table,
      'locality',
      Sequelize.STRING
    )
    await queryInterface.addColumn(
      table,
      'street',
      Sequelize.STRING
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'region')
    await queryInterface.removeColumn(table, 'locality')
    await queryInterface.removeColumn(table, 'street')
  }
};
