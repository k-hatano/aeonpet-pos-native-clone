'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('staff_roles', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: Sequelize.STRING
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('staff_roles')
  }
}
