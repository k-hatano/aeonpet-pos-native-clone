'use strict';

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'payment_methods',
      'media_type',
      Sequelize.INTEGER
    )
    await queryInterface.addColumn(
      'payment_methods',
      'acquire_code',
      Sequelize.STRING
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn('payment_methods', 'media_type')
    await queryInterface.removeColumn('payment_methods', 'acquire_code')
  }
};
