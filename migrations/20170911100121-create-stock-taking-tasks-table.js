'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('stock_taking_tasks', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      stock_taking_id: Sequelize.STRING,
      is_pushed: Sequelize.INTEGER,
      name: Sequelize.STRING,
      status: Sequelize.INTEGER,
      staff_id: Sequelize.STRING,
      staff_name: Sequelize.STRING,
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('stock_taking_tasks')
  }
}
