'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cashier_balance_reasons', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      cashier_balance_reason_code: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      balance_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      sort_order: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('cashier_balance_reasons')
  }
}
