'use strict'

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'useable_base_points', {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      }
    )
    await queryInterface.addColumn(
      table,
      'store_opening_type', {
        type: Sequelize.INTEGER(3),
        allowNull: false,
        defaultValue: 9
      }
    )
    await queryInterface.addColumn(
      table,
      'issuer_code', {
        type: Sequelize.STRING(64),
        allowNull: true
      }
    )
    await queryInterface.addColumn(
      table,
      'issuer_name', {
        type: Sequelize.STRING(255),
        allowNull: true
      }
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'useable_base_points')
    await queryInterface.removeColumn(table, 'store_opening_type')
    await queryInterface.removeColumn(table, 'issuer_code')
    await queryInterface.removeColumn(table, 'issuer_name')
  }
}
