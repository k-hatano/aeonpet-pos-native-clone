'use strict'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'cashier_cash_records',
      'is_pushed',
      Sequelize.BOOLEAN
    )
    await queryInterface.renameColumn(
      'cashier_cash_records',
      'mode',
      'cashier_cash_record_mode'
    )
    await queryInterface.changeColumn(
      'cashier_cash_records',
      'client_created_at',
      {
        type: Sequelize.INTEGER,
        allowNull: true
      })
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn('cashier_cash_records', 'is_pushed')
    await queryInterface.renameColumn(
      'cashier_cash_records',
      'cashier_cash_record_mode',
      'mode'
    )
    await queryInterface.changeColumn(
      'cashier_cash_records',
      'client_created_at',
      {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    )
  }
}
