'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('multiple_tax_rates', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      tax_code: {
        type: Sequelize.INTEGER(3),
        allowNull: false
      },
      start_date: {
        type: Sequelize.DATE,
        allowNull: false
      },
      end_date: {
        type: Sequelize.DATE,
        allowNull: false
      },
      tax_rate: {
        type: Sequelize.DECIMAL(5, 4),
        allowNull: false
      },
      receipt_sign: {
        type: Sequelize.STRING(1),
        allowNull: true
      },
      receipt_comment: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('multiple_tax_rates')
  }
}
