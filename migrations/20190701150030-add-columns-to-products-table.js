'use strict'

const table = 'products'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'tax_code',
      Sequelize.INTEGER(3)
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'tax_code')
  }
}
