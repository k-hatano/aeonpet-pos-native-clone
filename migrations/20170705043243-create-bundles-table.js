'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('bundles', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      code: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      bundle_riichi_message: {
        type: Sequelize.STRING(255),
        allowNull: true,
        defaultValue: null
      },
      bundle_type: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      priority: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
      },
      start_at: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      end_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('bundles')
  }
}
