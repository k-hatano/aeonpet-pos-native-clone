'use strict'

const table = 'coupon_discounts'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(table, {
      id: {
        primaryKey: true,
        type: Sequelize.STRING(16),
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      code: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      discount_rate: {
        type: Sequelize.DECIMAL(5, 4),
        allowNull: true,
        defaultValue: 0.0
      },
      discount_price: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: true,
        defaultValue: 0
      },
      product_tag_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      barcode: {
        type: Sequelize.STRING(128)
      },
      threshold_type: {
        type: Sequelize.INTEGER
      },
      threshold_min: {
        type: Sequelize.DECIMAL(19, 4)
      },
      threshold_max: {
        type: Sequelize.DECIMAL(19, 4),
        defaultValue: 99999999
      },
      start_at: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      end_at: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      deleted_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }

    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable(table)
  }
}
