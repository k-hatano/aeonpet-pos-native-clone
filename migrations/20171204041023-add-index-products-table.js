'use strict'

const tableName = 'products'
const articleNumberIndexName = 'ix_products_article_number'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addIndex(tableName, {
      fields: ['article_number'],
      name: articleNumberIndexName
    })
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeIndex(tableName, articleNumberIndexName)
  }
}
