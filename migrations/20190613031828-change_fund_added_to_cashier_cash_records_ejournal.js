'use strict';

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'cashier_cash_records',
      'change_fund',
      {type:Sequelize.DECIMAL(19, 4), defaultValue: 0}
    )
    await queryInterface.addColumn(
      'ejournals',
      'change_fund',
      {type:Sequelize.DECIMAL(19, 4), defaultValue: 0}
    )
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('cashier_cash_records', 'change_fund')
    queryInterface.removeColumn('ejournals', 'change_fund')
  }
};
