'use strict'

const table_bundle = 'bundles'
const table_sale_discounts = 'sale_discounts'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table_bundle,
      'barcode', {
        type: Sequelize.STRING,
        allowNull: true
      }
    )
    await queryInterface.addColumn(
      table_sale_discounts,
      'barcode', {
        type: Sequelize.STRING,
        allowNull: true
      }
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table_bundle, 'barcode')
    await queryInterface.removeColumn(table_sale_discounts, 'barcode')
  }
}
