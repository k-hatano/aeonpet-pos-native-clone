'use strict'

const table = 'products'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'change_price_type',
      Sequelize.INTEGER
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(table, 'chnage_price_type')
  }
}
