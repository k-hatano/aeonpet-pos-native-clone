'use strict'

const table = 'product_point_ratios'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(table, {
      id: {
        primaryKey: true,
        allowNull: true,
        type: Sequelize.STRING
      },
      product_tag_id: {
        allowNull: true,
        type: Sequelize.STRING
      },
      promotion_point_id: {
        allowNull: true,
        type: Sequelize.STRING
      },
      point_type: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      applied_type: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      assigned_ratio: {
        allowNull: true,
        type: Sequelize.DECIMAL(19, 4)
      },
      start_at: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      end_at: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      start_time: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      end_time: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      on_sunday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_monday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_tuesday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_wednesday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_thursday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_friday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      on_saturday: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      day_1: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      day_2: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      day_3: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      day_4: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      day_5: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(table)
  }
}
