'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('products', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      product_name_sys21: {
        type: Sequelize.STRING(255),
        allowNull: true,
        default: null
      },
      product_code: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      is_parent_product: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
      },
      is_discountable: {
        type: Sequelize.INTEGER,
        allowNull: true,
        default: 1
      },
      is_sales_excluded: {
        type: Sequelize.INTEGER,
        allowNull: true,
        default: 0
      },
      tax_rate: {
        type: Sequelize.DECIMAL(5, 4),
        allowNull: true,
        default: 0.8
      },
      tax_rule: {
        type: Sequelize.INTEGER,
        allowNull: true,
        default: 4
      },
      tax_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        default: 4
      },
      taxfree_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        default: 1
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('products')
  }
}
