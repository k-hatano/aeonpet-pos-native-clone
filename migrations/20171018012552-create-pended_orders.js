'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('pended_orders', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      data: Sequelize.STRING,
      client_created_at: Sequelize.INTEGER,
      is_pended: Sequelize.BOOLEAN,
      is_minus: Sequelize.BOOLEAN,
      status: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('pended_orders')
  }
}