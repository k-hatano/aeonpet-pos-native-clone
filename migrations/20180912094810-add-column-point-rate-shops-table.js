'use strict'

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'point_rate',
      Sequelize.DECIMAL(5, 4)
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(table, 'point_rate')
  }
}
