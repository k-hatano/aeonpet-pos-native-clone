'use strict';

const table = 'regions'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    return queryInterface.createTable(table, {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: Sequelize.STRING,
      country_code: Sequelize.STRING,
      region_code: Sequelize.INTEGER,
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(table)
  }
};
