'use strict';

const table = 'shops'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'credit_user_id',
      Sequelize.STRING
    )
    await queryInterface.addColumn(
      table,
      'password',
      Sequelize.STRING
    )
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeColumn(table, 'credit_user_id')
    await queryInterface.removeColumn(table, 'password')
  }
};
