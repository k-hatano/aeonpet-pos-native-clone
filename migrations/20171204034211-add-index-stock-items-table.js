'use strict'

const tableName = 'stock_items'
const indexName = 'ix_stock_items_product_variant_id'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addIndex(tableName, {
      fields: ['product_variant_id'],
      name: indexName
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeIndex(tableName, indexName)
  }
}
