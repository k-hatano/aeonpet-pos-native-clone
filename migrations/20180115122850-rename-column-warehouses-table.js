'use strict'

const table = 'warehouses'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.renameColumn(
      table,
      'code',
      'warehouse_code'
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.renameColumn(
      table,
      'warehouse_code',
      'code'
      )
  }
}
