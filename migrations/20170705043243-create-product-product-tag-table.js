'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('product_product_tag', {
      product_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      product_tag_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('product_product_tag')
  }
}
