'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('ejournals', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      cashier_id: Sequelize.STRING,
      device_id: Sequelize.STRING,
      shop_id: Sequelize.STRING,
      staff_id: Sequelize.STRING,
      order_id: Sequelize.STRING,
      pos_order_number: Sequelize.STRING,
      receipt_type: Sequelize.INTEGER,
      receipt_title: Sequelize.STRING,
      data: Sequelize.STRING,
      data_text: Sequelize.STRING,
      currency: Sequelize.STRING,
      amount: Sequelize.DECIMAL,
      is_print_tax_stamp: Sequelize.INTEGER,
      is_printed: Sequelize.INTEGER,
      is_pushed: Sequelize.INTEGER,
      is_reprint: Sequelize.INTEGER,
      client_created_at: Sequelize.INTEGER,
      fixed_at: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('ejournals')
  }
}
