'use strict'

const tableName = 'product_variants'
const productIdIndexName = 'ix_product_variants_product_id'
const articleNumberIndexName = 'ix_product_variants_article_number'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addIndex(tableName, {
      fields: ['product_id'],
      name: productIdIndexName
    })
    await queryInterface.addIndex(tableName, {
      fields: ['article_number'],
      name: articleNumberIndexName
    })
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.removeIndex(tableName, productIdIndexName)
    await queryInterface.removeIndex(tableName, articleNumberIndexName)
  }
}
