'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('warehouses', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      code: {
        type: Sequelize.STRING(255),
        allowNull: true,
        defaultValue: null
      },
      warehouse_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 1
      },
      sort_order: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      created_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('warehouses')
  }
}
