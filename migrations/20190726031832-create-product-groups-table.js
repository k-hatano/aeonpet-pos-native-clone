'use strict';

const table = 'product_groups'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable(table, {
      id: {
        primaryKey: true,
        type: Sequelize.STRING(16)
      },
      group_code: {
        allowNull: true,
        type: Sequelize.STRING(16)
      },
      sort_order: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      status: {
        allowNull: true,
        type: Sequelize.DECIMAL(19, 4),
        defaultValue: 1
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable(table)
  }
};
