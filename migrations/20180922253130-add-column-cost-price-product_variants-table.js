'use strict'

const table = 'product_variants'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      table,
      'cost_price',
      Sequelize.DECIMAL(19, 4)
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(table, 'cost_price')
  }
}
