'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('cashier_settings', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      tax_office: Sequelize.STRING,
      tax_payment_place: Sequelize.STRING,
      company_name: Sequelize.STRING
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('cashier_settings')
  }
}
