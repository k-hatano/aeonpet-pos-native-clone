'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('product_variants', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      product_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      stock_mode: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 1
      },
      sku: {
        type: Sequelize.STRING(255),
        allowNull: true,
        defaultValue: null
      },
      article_number: {
        type: Sequelize.STRING(255),
        allowNull: true,
        defaultValue: null
      },
      closure_type_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      closure_type_name: {
        type: Sequelize.STRING(255),
        allowNull: true,
        defaultValue: null
      },
      closure_type_code: {
        type: Sequelize.STRING(2),
        allowNull: true,
        defaultValue: null
      },
      vintage: {
        type: Sequelize.STRING(4),
        allowNull: true,
        defaultValue: null
      },
      label_version: {
        type: Sequelize.STRING(7),
        allowNull: true,
        defaultValue: null
      },
      box_version: {
        type: Sequelize.STRING(7),
        allowNull: true,
        defaultValue: null
      },
      best_by_date: {
        type: Sequelize.STRING(7),
        allowNull: true,
        defaultValue: null
      },
      capacity: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      liquor_tax_type1: {
        type: Sequelize.STRING(4),
        allowNull: true,
        defaultValue: null
      },
      liquor_tax_type2: {
        type: Sequelize.STRING(4),
        allowNull: true,
        defaultValue: null
      },
      price: {
        type: Sequelize.DECIMAL(19, 4),
        allowNull: false
      },
      quantity_per_carton: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      can_grant_points: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      // sales_method: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   defaultValue: null
      // },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('product_variants')
  }
}
