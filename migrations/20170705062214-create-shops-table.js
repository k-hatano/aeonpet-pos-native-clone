'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('shops', {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      shop_code: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      reserve_code: {
        type: Sequelize.STRING(7),
        allowNull: true,
        defaultValue: null
      },
      calc_type: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      tax_rule: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: null
      },
      tax_rate: {
        type: Sequelize.DECIMAL(5, 4),
        allowNull: true,
        defaultValue: null
      },
      pos_closed_time: {
        type: Sequelize.TIME,
        allowNull: true,
        defaultValue: null
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null
      }
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('shops')
  }
}
