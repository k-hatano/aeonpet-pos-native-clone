'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('orders', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      data: Sequelize.STRING,
      client_created_at: Sequelize.INTEGER,
      is_pushed: Sequelize.BOOLEAN,
      is_minus: Sequelize.BOOLEAN,
      status: Sequelize.INTEGER
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('orders')
  }
}
