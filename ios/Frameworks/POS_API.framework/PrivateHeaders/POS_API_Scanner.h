//
//  POS_API_Scanner.h
//  
//
//  Created by Lifelong-Study on 7/29/15.
//
//

#import <Foundation/Foundation.h>
#import <POS_API/POS_API_ScannerStruct.h>

@interface POS_API_Scanner : NSObject

+ (void)setReadMode:(ScannerReadMode)readMode;
+ (void)enableAutoTrigger:(BOOL)status;
+ (void)enableFloodlight:(BOOL)status;
+ (void)enableBuzzer:(BOOL)status;
+ (void)setBuzzerLoudness:(BuzzerLoudness)loudness duration:(BuzzerDuration)duration;

+ (void)writeWithString:(NSString *)string;
+ (void)writeWithData:(NSData *)data;

@end
