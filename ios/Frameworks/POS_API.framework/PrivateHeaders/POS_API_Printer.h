//
//  POS_API_Printer.h
//  CheckOutCounterObjectiveC
//
//  Created by Peter Pan on 3/3/15.
//  Copyright (c) 2015 Encore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <POS_API/POS_API_PrinterStruct.h>

@interface POS_API_Printer :NSObject

+ (void)printInit;


+ (void)setPrintFormat:(PrintFormat)format;
+ (void)setPrintFont:(PrintFont)font;
+ (void)setCodePage;
+ (void)setPrintCharacterScale:(NSInteger)vScale hScale:(NSInteger)hScale;
+ (void)setPrinterSettings:(int)codePage leftMargin:(int)leftMargin printAreaWidth:(int)printAreaWidth printQuality:(int)printQuality;
+ (void)printText:(NSString *)text;
+ (void)printData:(NSData *)data;
+ (void)printImage:(UIImage *)image;

+ (void)printBarCodeWithString:(NSString *)string type:(BarCodeType)type mode:(BarCodeMode)mode;
+ (void)printBarCodeWithString:(NSString *)string type:(BarCodeType)type width:(int)width height:(int)height ratio:(int)nwRatio mode:(BarCodeMode)mode;
+ (void)printBarCodeWithData:(NSData *)data type:(BarCodeType)type width:(int)width height:(int)height ratio:(int)nwRatio mode:(BarCodeMode)mode;
+ (void)printQRCodeWithString:(NSString *)string mode:(QRCodeMode)mode errorCorrect:(QRCodeECC)errorCorrect size:(int)size;
+ (void)printQRCodeWithData:(NSData *)data mode:(QRCodeMode)mode errorCorrect:(QRCodeECC)errorCorrect size:(int)size;

+ (void)resetPrinter;
+ (void)printPage;
+ (void)stopPage;

+ (BOOL)registerLogoWithNumber:(NSInteger)number image:(UIImage *)image;
+ (void)unregisterLogoWithNumber:(NSInteger)number;
+ (void)printLogoWithNumber:(NSInteger)number;
+ (void)moveToX:(int)x Y:(int)y;

+ (void)startPage:(NSInteger)x y:(NSInteger)y w:(NSInteger)w h:(NSInteger)h;
+ (void)cutPaper;
+ (void)pushPaper:(NSInteger)lines;
+ (void)toPageMode;
+ (void)toStandardMode;
+ (void)CarriageReturn;

+ (void)printTestPage;

+ (void)setLineSpacing:(NSInteger)value;
+ (void)setCharacterSpacing:(NSInteger)value;

+ (void)writeWithString:(NSString *)string;
+ (void)writeWithData:(NSData *)data;
+ (void)requestResponse:(int)value;

@end
