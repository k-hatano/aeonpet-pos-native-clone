//
//  POS_API_ControlCenter.h
//  
//
//  Created by Lifelong-Study on 7/28/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <POS_API/POS_API_SerialPortStruct.h>

#define PosApiSerialPort    [POS_API_SerialPort sharedInstance]

@interface POS_API_SerialPort : NSObject
{

}

- (void)undock:(int)timeout;
- (void)dockPowerON;
- (void)dockPowerOFF;
- (void)openCashBox;
- (void)readCashBoxStatus;

- (void)setSerialPort:(NSString *_Nonnull)port config:(PORT_CONFIG)config isDeviceBtype:(bool) isTypeB;


- (void)saveConf;
- (void)readPort:(NSInteger)port;

+ (NSData *_Nonnull)dataFromHexString:(NSString *_Nonnull)string;
- (void)readPeripheral:(CBPeripheral *_Nonnull)peripheral Service:(NSString *_Nonnull)serviceUUID characteristic:(NSString *_Nonnull)characteristicUUID;
- (void)writeToPeripheral:(CBPeripheral *_Nonnull)peripheral service:(NSString *_Nonnull)serviceUUID characteristic:(NSString *_Nonnull)characteristicUUID data:(NSData *_Nonnull)data;
- (void)writeToPeripheralDirect:(CBPeripheral *_Nonnull)peripheral service:(NSString *_Nonnull)serviceUUID characteristic:(NSString *_Nonnull)characteristicUUID data:(NSData *_Nonnull)data;
- (void)setWriteType:(Boolean)response;
- (void)setWriteNoResponseDelay:(int)delay;
- (void)setWriteQueueDelay:(int)msDelay;

- (void)writeStringWithPortNumber:(NSInteger)portNumber string:(NSString *_Nonnull)string;
- (void)writeDataWithPortNumber:(NSInteger)portNumber data:(NSData *_Nonnull)data;

- (void)peripheral:(CBPeripheral *_Nonnull)peripheral didWriteValueForCharacteristic:(CBCharacteristic *_Nonnull)characteristic error:(nullable NSError *)error;

- (void)sendReadPrinterModelIdCmd:(NSInteger)port;
- (void)sendReadPrinterManufacturerCmd:(NSInteger)port;

- (int)getWriteDataCount;
- (void)resetWriteDataCount;
- (int)getWritePacketCount;
- (void)clearPrinterSendDataQueue;

+ (POS_API_SerialPort *_Nonnull)sharedInstance;

@end
