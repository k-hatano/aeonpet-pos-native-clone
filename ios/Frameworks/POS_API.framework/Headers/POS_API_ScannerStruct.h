//
//  POS_API_ScannerStruct.h
//  POS_API
//
//  Created by Lifelong-Study on 8/17/15.
//  Copyright (c) 2015 Lifelong-Study. All rights reserved.
//

#ifndef POS_API_POS_API_ScannerStruct_h
#define POS_API_POS_API_ScannerStruct_h

typedef enum {
    SCANNER_READMODE_SINGLE,
    SCANNER_READMODE_MULTIPLE,
    SCANNER_READMODE_CONTINUOUS,
    SCANNER_AUTO_TRIGGER_ENABLE,
    SCANNER_AUTO_TRIGGER_DISABLE,
    SCANNER_LIGHT_ENABLE,
    SCANNER_LIGHT_DISABLE,
    SCANNER_BUZZER_DISABLE,
    SCANNER_BUZZER_LOUDNESS_NORMAL,
    SCANNER_BUZZER_LOUDNESS_MINIMUM,
    SCANNER_BUZZER_LOUDNESS_MAXIMUM,
    SCANNER_BUZZER_DURATION_NORMAL,
    SCANNER_BUZZER_DURATION_SHORT,
    SCANNER_BUZZER_DURATION_LONG
} SCANNER_OPTION;

typedef enum {
    ScannerReadModeSingle,
    ScannerReadModeMultiple,
    ScannerReadModeContinuous
} ScannerReadMode;

typedef enum {
    BuzzerLoudnessNormal,
    BuzzerLoudnessMinimum,
    BuzzerLoudnessMaximum
} BuzzerLoudness;

typedef enum {
    BuzzerDurationNormal,
    BuzzerDurationShort,
    BuzzerDurationLong
} BuzzerDuration;

#endif
