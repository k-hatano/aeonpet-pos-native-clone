//
//  POS_API_CoreStruct.h
//  POS_API
//
//  Created by Lifelong-Study on 8/17/15.
//  Copyright (c) 2015 Lifelong-Study. All rights reserved.
//

#ifndef POS_API_CoreStruct_h
#define POS_API_CoreStruct_h

@interface BleDeviceInfo :NSObject
//typedef struct BleDeviceInfo {
//    __unsafe_unretained NSUUID      *mUUID;         //
//    __unsafe_unretained NSString    *mName;         // 廣播名稱
//    int                              mRssi;         // 訊號強度
//} BleDeviceInfo;
@property (strong, nonatomic) NSString              *mName;
@property (strong, nonatomic) NSUUID                *mUUID;
@property int                              mRssi;         // 訊號強度
@end

typedef enum {
    PeripheralDeviceNone,
    PeripheralDeviceScanner,
    PeripheralDevicePrinter,
    PeripheralDeviceOthers
} PeripheralDevice;

//
typedef enum {
    BLE_CONNECTING,
    BLE_CONNECTED,
    BLE_DISCONNECTED
} BleConnectionStatus;

typedef enum {
    POS_API_UNKNOWN = -1,
    POS_API_FALSE = 0,
    POS_API_TRUE = 1
} PosApiBoolean;


#endif
