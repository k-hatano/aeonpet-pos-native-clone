//
//  POS_API.h
//  POS_API
//
//  Created by Lifelong-Study on 7/27/15.
//  Copyright (c) 2015 Lifelong-Study. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for POS_API.
FOUNDATION_EXPORT double POS_APIVersionNumber;

//! Project version string for POS_API.
FOUNDATION_EXPORT const unsigned char POS_APIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <POS_API/PublicHeader.h>
#import <POS_API/POS_API_Core.h>

#import <POS_API/POS_API_CoreStruct.h>
#import <POS_API/POS_API_SerialPortStruct.h>
#import <POS_API/POS_API_PrinterStruct.h>
#import <POS_API/POS_API_ScannerStruct.h>
