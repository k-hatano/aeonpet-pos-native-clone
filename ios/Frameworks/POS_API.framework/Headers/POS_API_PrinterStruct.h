//
//  POS_API_PrintStruct.h
//  POS_API
//
//  Created by Lifelong-Study on 8/17/15.
//  Copyright (c) 2015 Lifelong-Study. All rights reserved.
//

#ifndef POS_API_PrintStruct_h
#define POS_API_PrintStruct_h


typedef enum {
    UNDERLINE_1DOT_START    = 1,
    UNDERLINE_2DOT_START    = 2,
    UNDERLINE_STOP          = 4,
    ROTATE_START            = 8,
    ROTATE_STOP             = 0x10,
    FLIP_START              = 0x20,
    FLIP_STOP               = 0x40,
    REVERSE_START           = 0x80,
    REVERSE_STOP            = 0x100,
    ALIGNMENT_LEFT          = 0x200,
    ALIGNMENT_CENTERD       = 0x400,
    ALIGNMENT_RIGHT         = 0x800,
    KANJI_UNDERLINE_1DOT_START = 0x1000,
    KANJI_UNDERLINE_2DOT_START = 0x2000,
    KANJI_UNDERLINE_STOP = 0x4000,
    BOLD_START              = 0x8000,
    BOLD_STOP               = 0x10000,
} PrintFormat;

typedef enum {
    PRINT_FONT_12x24         = 0,
    PRINT_FONT_8x16          = 1,
    PRINT_KANJI_FONT_24x24      = 3,
    PRINT_KANJI_FONT_16x16      = 4,
} PrintFont;

typedef enum {
    FONT_SCALE_HORIZONTAL_1 = 0x00,
    FONT_SCALE_HORIZONTAL_2 = 0x10,
    FONT_SCALE_HORIZONTAL_3 = 0x20,
    FONT_SCALE_HORIZONTAL_4 = 0x30,
    FONT_SCALE_HORIZONTAL_5 = 0x40,
    FONT_SCALE_HORIZONTAL_6 = 0x50,
    FONT_SCALE_HORIZONTAL_7 = 0x60,
    FONT_SCALE_HORIZONTAL_8 = 0x70,
} PrintFontScaleHorizontal;

typedef enum {
    FONT_SCALE_VERTICAL_1 = 0x00,
    FONT_SCALE_VERTICAL_2 = 0x01,
    FONT_SCALE_VERTICAL_3 = 0x02,
    FONT_SCALE_VERTICAL_4 = 0x03,
    FONT_SCALE_VERTICAL_5 = 0x04,
    FONT_SCALE_VERTICAL_6 = 0x05,
    FONT_SCALE_VERTICAL_7 = 0x06,
    FONT_SCALE_VERTICAL_8 = 0x07,
} PrintFontScaleVertical;

typedef enum {
    BARCODE_MODE_HRI_DONT_PRINT = 0,
    BARCODE_MODE_HRI_OVER       = 1,
    BARCODE_MODE_HRI_UNDER      = 2,
    BARCODE_MODE_HRI_BOTH       = 3
} BarCodeMode;

typedef enum {
    BARCODE_TYPE_UPC_A      = 0,
    BARCODE_TYPE_UPC_E      = 1,
    BARCODE_TYPE_JAN13      = 2,
    BARCODE_TYPE_JAN8       = 3,
    BARCODE_TYPE_CODE39     = 4,
    BARCODE_TYPE_ITF        = 5,
    BARCODE_TYPE_CODABAR    = 6,
    BARCODE_TYPE_JAN13_ADDON = 22,
    BARCODE_TYPE_CODE93 = 72,
    BARCODE_TYPE_CODE128 = 73,
} BarCodeType;

typedef enum {
    BARCODE_WIDTH_2      = 2,
    BARCODE_WIDTH_3      = 3,
    BARCODE_WIDTH_4      = 4,
    BARCODE_WIDTH_5      = 5,
    BARCODE_WIDTH_6      = 6,

} BarCodeWidth;

typedef enum {
    BARCODE_RATIO_1_2    = 0,
    BARCODE_RATIO_1_25    = 1,
    BARCODE_RATIO_1_3    = 2,
    
} BarCodeRatio;

static const int BARCODE_HEIGHT_DEFAULT = 50;

typedef enum {
    QRCODE_NUMERICAL        = 0,
    QRCODE_ALPHANUMERIC     = 1,
    QRCODE_8BITS_BYTE       = 2,
    QRCODE_KANJI            = 3,
    QRCODE_MIXED            = 4
} QRCodeMode;

typedef enum {
    QRCODEECC_HIGH,
    QRCODEECC_NORMAL,
    QRCODEECC_LOW
} QRCodeECC;

typedef enum {
    CHARSET_USA             = 0,
    CHARSET_FRANCE          = 1,
    CHARSET_GERMANY         = 2,
    CHARSET_UK              = 3,
    CHARSET_DENMARK         = 4,
    CHARSET_SWEDEN          = 5,
    CHARSET_ITALY           = 6,
    CHARSET_SPAIN           = 7,
    CHARSET_JAPAN           = 8,
    CHARSET_NORWAY          = 9,
    CHARSET_DENMARK2        = 10,
    CHARSET_SPAIN2          = 11,
    CHARSET_LATIN_AMERICA   = 12,
    CHARSET_KOREA           = 13,
    CHARSET_SLOVENIA        = 14,
    CHARSET_CHINA           = 15,

    CHARSET_BIG5            = 100,
    CHARSET_GBK             = 102,
    CHARSET_SHIFT_JIS       = 103
} PrintCharset;

typedef enum {
    DIRECTION_TOP_LEFT,
    DIRECTION_BOTTOM_LEFT,
    DIRECTION_BOTTOM_RIGHT,
    DIRECTION_TOP_RIGHT,
} PrintDirection;

typedef enum {
    PRINTER_MODEL_UNKNOWN,
    PRINTER_MODEL_SII,
    PRINTER_MODEL_PRT,
    PRINTER_MODEL_EPSON
}PrinterModel;

typedef enum {
    PRINTER_PAPER_OK,
    PRINTER_PAPER_EMPTY,
    PRINTER_PAPER_UNKNOWN
} PrinterPaperStatusEnum;

typedef enum {
    PRINTER_ERR_NONE,
    PRINTER_ERR_AUTOCUTTER,
    PRINTER_ERR_UNRECOVER,
    PRINTER_ERR_AUTORECOVER,
    PRINTER_ERR_UNKNOWN
} PrinterErrorStatusEnum;

typedef struct PrinterStatus{
    Boolean                     connected;
    Boolean                     coverOpen;
    PrinterPaperStatusEnum      paperStatus;
    PrinterErrorStatusEnum      errorStatus;
} PrinterStatus;

#endif
