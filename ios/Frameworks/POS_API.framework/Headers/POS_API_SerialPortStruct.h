//
//  POS_API_SerialPortStruct.h
//  POS_API
//
//  Created by Lifelong-Study on 8/17/15.
//  Copyright (c) 2015 Lifelong-Study. All rights reserved.
//

#ifndef POS_API_POS_API_SerialPortStruct_h
#define POS_API_POS_API_SerialPortStruct_h

//
enum BluetoothState {               // 裝置上的藍芽狀態
    BS_UNKNOWN,                     // 未知的藍芽裝置
    BS_UNSUPPORTED,                 // 裝置不支援藍芽傳輸
    BS_UNAUTHORIZED,                // 裝置上的藍芽裝置未經授權，無法使用
    BS_RESETTING,                   // 已復歸
    BS_POWERED_OFF,                 // 已關閉
    BS_POWERED_ON,                  // 已開啟
    BS_NONE                         // 裝置上沒有藍芽
};


//
enum {
    PARITY_None,
    PARITY_Even,
    PARITY_Odd
};
enum {
    BAUDRATE_1200=1200,
    BAUDRATE_2400=2400,
    BAUDRATE_4800=4800,
    BAUDRATE_9600=9600,
    BAUDRATE_19200=19200,
    BAUDRATE_38400=38400,
    BAUDRATE_57600=57600,
    BAUDRATE_115200=115200,
};
enum {
    DATABIT_7BIT=7,
    DATABIT_8BIT=8
};
enum {
    STOPBIT_1STOP=1,
    STOPBIT_2STOP=2
};

//
typedef struct {
    BOOL        isEnable;
    NSInteger   baudRate;
    NSInteger   parity;
    NSInteger   dataBit;
    NSInteger   stopBit;
    BOOL        isFlowControl;
} PORT_INFO;

//
typedef struct BleDevice {
    
} BleDevice;

//
typedef struct PortConfig{
    BOOL        isPowerON;
    NSInteger   baudRate;
    NSInteger   parity;
    NSInteger   dataBit;
    NSInteger   stopBit;
    BOOL        isFlowControl;
} PORT_CONFIG;

//
struct SerialPortConfig {
    PORT_CONFIG COM1;
    PORT_CONFIG COM2;
    PORT_CONFIG COM3;
};

enum {
    PORT_ID_COM1=1,
    PORT_ID_COM2=2,
    PORT_ID_COM3=3,
};

#endif
