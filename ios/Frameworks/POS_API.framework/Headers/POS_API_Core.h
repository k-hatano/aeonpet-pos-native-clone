//
//  Bluetooth.h
//  [Module] Bluetooth
//
//  Created by Matt on 2014/2/13.
//  Copyright (c) 2014年 Matt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>
#import <QuartzCore/QuartzCore.h>

#import <POS_API/POS_API_CoreStruct.h>
#import <POS_API/POS_API_SerialPortStruct.h>
#import <POS_API/POS_API_PrinterStruct.h>
#import <POS_API/POS_API_ScannerStruct.h>


@protocol POS_APIDelegate;

#define PosApi    [POS_API sharedInstance]

@interface POS_API : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>
{
    // 裝置與連線資訊
    NSInteger        bluetoothState;    // 記錄藍芽的狀態
    NSInteger        connectStatus;     // 記錄與裝置的連線狀態
    NSString        *connectUUID;       // 記錄設備的 UUID
    
    // 埠口設定檔
    struct SerialPortConfig serialPortConfig;
    
    //
    NSDate          *startTime;         // 暫存
    NSMutableData   *barcodeData;       // 暫存
}

@property (strong, nonatomic) CBCentralManager  *centralManager;
@property (strong, nonatomic) CBPeripheral      *peripheral;
@property (strong, nonatomic) NSArray           *characteristicArray;
@property (strong, nonatomic) NSMutableArray           *broadcastCharacteristicArray;

@property (strong, nonatomic) id <POS_APIDelegate> delegate;


#pragma mark - Core
- (void)connectBle:(NSUUID *)deviceId;
- (void)startDiscoverBleDevice;
- (void)stopDiscoverBleDevice;
- (void)disconnect:(NSUUID *)deviceId;
- (int )getBleConnectionStatus:(NSUUID *)deviceId;


#pragma mark - Hardware
- (NSString*)getSystemID;
- (NSString*)getManufactureName;
- (NSString*)getSwVersion;
- (NSString*)getFwVersion;
- (NSString*)getModelName;

#pragma mark - SerialPort
- (void)openCOMPort:(int)portId;
- (void)closeCOMPort:(int)portId;
- (int )getPortSettings:(int)portId portConfig:(PORT_CONFIG*)portConfig;
- (int )changePortSettings:(int)portId portConfig:(PORT_CONFIG)portConfig;
- (void)savePortSettingsPermanently;
- (int )writePortData:(int)portId data:(NSData *)data len:(int)len;

- (void)setPeripheralTypeWithPortNumber:(NSInteger)portNumber peripheralDevice:(PeripheralDevice)peripheralDevice;
- (PeripheralDevice)getPeripheralDeviceTypeByPortNumber:(NSInteger)portNumber;
- (NSDictionary *)getPeripheralConfigWithPeripheralDevice:(PeripheralDevice)peripheralDevice; //Reserved for future development
- (NSDictionary *)getPeripheralConfigWithPortNumber:(NSInteger)portNumber;  //Reserved for future development
- (NSInteger)getPeripheralPortWithPeripheralDevice:(PeripheralDevice)peripheralDevice;  //Reserved for future development

#pragma mark - CashBox
- (void)openCashBox;
- (int)getCashBoxStatus;
- (int )undock:(int)timeout;


#pragma mark - POS_API_Printer
- (void)initPrinter;
- (void)initPrinter:(PrinterModel)printerModel PortInit:(BOOL)initPort;
- (void)releasePrinter;

- (void)setPrintFormat:(PrintFormat)format;
- (void)setPrintFont:(PrintFont)font;
- (void)setCodePage:(int)codePage; //Reserved for future development
- (void)selectCodeTable:(int)codeTable;
- (void)setPrintCharacterScale:(int)vScale hScale:(int)hScale;
- (void)setPrinterSettings:(int)codePage leftMargin:(int)leftMargin printAreaWidth:(int)printAreaWidth printQuality:(int)printQuality;
- (void)printText:(NSString *)text;
- (void)printData:(NSData *)data;
- (void)printImage:(UIImage *)bmp;
- (void)printBarCodeWithString:(NSString *)string type:(BarCodeType)type mode:(BarCodeMode)mode;
- (void)printBarCodeWithString:(NSString *)string type:(BarCodeType)type width:(int)width height:(int)height ratio:(int)nwRatio mode:(BarCodeMode)mode;
- (void)printBarCodeWithHexString:(NSString *)hexStr type:(BarCodeType)type width:(int)width height:(int)height ratio:(int)nwRatio mode:(BarCodeMode)mode;
- (void)printBarCodeWithData:(NSData *)data type:(BarCodeType)type width:(int)width height:(int)height ratio:(int)nwRatio mode:(BarCodeMode)mode;
- (void)printQRCodeWithString:(NSString *)string mode:(QRCodeMode)mode errorCorrect:(QRCodeECC)errorCorrect size:(int)size;
- (void)printQRCodeWithData:(NSData *)data mode:(QRCodeMode)mode errorCorrect:(QRCodeECC)errorCorrect size:(int)size;
- (void)printPDF417WithString:(NSString *)string errorCorrect:(int)eccLevel size:(int)size rowHeight:(int)rowHeight;
- (void)printPDF417WithData:(NSData *)data errorCorrect:(int)eccLevel size:(int)size rowHeight:(int)rowHeight;
- (void)printLogo:(int)Id;
- (BOOL)registerLogo:(int)Id bmp:(UIImage *)bmp;
- (void)unregisterLogo:(int)Id;
- (void)cutPaper;
- (void)pushPaper:(NSInteger)lines;
- (void)resetPrinter;
- (void)startPage:(int)x y:(int)y w:(int)w h:(int)h;
- (void)startPage:(PrintDirection)direction x:(int)x y:(int)y w:(int)w h:(int)h;
- (void)printPage;
- (void)goTo:(int)x Y:(int)y;
- (void)stopPage;
- (void)setPrintLineSpacing:(int)dots;
- (void)setPrintCharacterSpacing:(int)dots;
- (void)requestResponse:(int)value;
- (void)forcePrinterRecover;
- (PrinterStatus*)getCurrentPrinterStatus;
- (void)enablePaperLoadingCut:(Boolean)enable;
- (Boolean)isPaperLoadingCutEnabled;
- (void)enableDiscardDataWhenError:(Boolean)enable;
- (Boolean)isDiscardDataWhenErrorEnabled;

- (int)getPrinterModel; //Reserved for future development
- (NSString*)getPrinterModelName;
- (NSString*)getPOSSDKVersion;

#pragma mark - POS_API_Scanner
- (void)initScanner;
- (void)releaseScanner;
- (void)setScannerReadMode:(int)readMode;
- (void)setScannerAutoTriggerMode:(int)triggerMode;
- (void)setScannerLightMode:(int)lightMode;
- (void)setScannerBuzzerMode:(int)buzzerLoudnessMode buzzerDurationMode:(int)buzzerDurationMode;

#pragma mark - Misc function
- (void)setWriteType:(Boolean)response;//Reserved for future development
- (void)setWriteNoResponseDelay:(int)delay;//Reserved for future development
- (void)setWriteQueueDelay:(int)delay;//Reserved for future development

- (int)getWriteDataCount;//Reserved for future development
- (void)resetWriteDataCount;//Reserved for future development
- (int)getWritePacketCount;//Reserved for future development

+ (POS_API *)sharedInstance;

@end


#pragma mark - Bluetooth Delegate
@protocol POS_APIDelegate <NSObject>
@optional
- (void)onBleConnectionStatusUpdate:(NSString *)addr status:(int)status;
- (void)onBleDiscoveredDevice:(BleDeviceInfo*)deviceInfo;
- (void)onBleError:(int)err;
- (void)onPortConfigReceived:(PORT_CONFIG)portConfig;
- (void)onCashBoxStatusUpdate:(int)status;
- (void)onPortDataReceived:(int)portId data:(NSData *)data len:(int)len;
- (void)onScannerDataReceived:(int)portId data:(NSData *)data len:(int)len;

- (void)onPrinterPlatenSensor:(NSNumber*)status;
- (void)onPrinterAutocutterError:(NSNumber*)status;
- (void)onPrinterHeadError:(NSNumber*)status;
- (void)onPrinterAutomaticRecoveryError:(NSNumber*)status;
- (void)onPrinterOutOfPaperSensor:(NSNumber*)status;
- (void)onPrinterRequestResponse:(NSNumber*)status;


@end
