//
//  mPOS2SDK.h
//  mPOS2SDK
//
//  Created by hiro on 2015/10/15.
//  Copyright (c) 2015年 VeriTrans Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "PpSignatureView.h"
#import "EADSessionController.h"
#import "DELProtocol.h"
#import "QPOSService.h"
#import "PcCardReaderiPosBridge.h"
#import "StringDrawingOptions.h"
#import "BTDeviceFinder.h"
#import "UtilPrefix.h"

#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPMusicPlayerController.h>
#import <sys/utsname.h>


//! Project version number for mPOS2SDK.
FOUNDATION_EXPORT double mPOS2SDKVersionNumber;

//! Project version string for mPOS2SDK.
FOUNDATION_EXPORT const unsigned char mPOS2SDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mPOS2SDK/PublicHeader.h>


