//
//  DELProtocol.h
//  EADemo
//
//  Created by fae on 2014/9/11.
//
//

#import <Foundation/Foundation.h>

@interface NSString (DELProtocol)

// BlueToothデータの開始と終了文字。二つが繋がって、その間のデータが最終的に読み取るデータとなる。
// BlueToothデータ開始文字
#define BT_DATA_START_CHAR1     0x10            // アスキーコード的には「DLE/データリンク拡張(バイナリ通信開始)」
#define BT_DATA_START_CHAR2     0x02            // アスキーコード的には「STX/テキスト開始」
#define BT_DATA_START           @"\x10\x02"
#define BT_DATA_START_LENGTH    2
// BlueToothデータ終了文字
#define BT_DATA_END_CHAR1       0x10            // アスキーコード的には「DLE/データリンク拡張(バイナリ通信開始)」
#define BT_DATA_END_CHAR2       0x03            // アスキーコード的には「ETX/テキスト終了」
#define BT_DATA_END             @"\x10\x03"
#define BT_DATA_END_LENGTH
// BlueToothデータ区切り文字
#define BT_SEPARATOR            @";"

- (char *)Send_DLE;
- (NSString *)Recv_DLE_WithData;

@end