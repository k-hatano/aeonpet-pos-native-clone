//
//  StringDrawingOptions.h
//  mposemv
//
//  Created by hiro on 2015/04/02.
//  Copyright (c) 2015年 VeriTrans Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// 文字列の描画サイズを取得するメソッドをswiftで使うためのクラス。swiftのバグに対応するためobjective-Cを使う
@interface StringDrawingOptions : NSObject

+ (NSStringDrawingOptions)combine:(NSStringDrawingOptions)option1 with:(NSStringDrawingOptions)option2;

@end
