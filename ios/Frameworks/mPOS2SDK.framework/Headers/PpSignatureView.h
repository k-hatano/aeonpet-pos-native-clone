#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

// 入力始めを通知するための通知名
#define kBeginSignatureNotification          @"notificationBeginSignature"

@interface PpSignatureView : GLKView

@property (assign, nonatomic) UIColor *strokeColor;
@property (assign, nonatomic) BOOL hasSignature;
//@property (strong, nonatomic) UIImage *signatureImage;

- (void)erase;
- (UIImage *)signatureImage;

@end
