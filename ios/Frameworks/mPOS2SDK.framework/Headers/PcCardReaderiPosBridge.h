//
//  PcCardReaderiPosBridge.h
//  mposemv
//
//  Created by hiro on 2015/07/17.
//  Copyright (c) 2015年 VeriTrans Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "QPOSService.h"

@interface PcCardReaderiPosBridge : NSObject <QPOSServiceListener>

-(void) onDoTradeResult: (DoTradeResult)result xxDecodeData:(NSDictionary*)decodeData;


@end
