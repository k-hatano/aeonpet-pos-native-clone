//
//  OPNSettings.h
//  OPNBluetoothKit
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

//**************************************************************************************************
// @name OPNSettings
//**************************************************************************************************
/**
 * データコレクターから取得したバイトデータの設定値を取得するためのメソッドを提供します。<br/>
 * [GoF]Singletonパターンを使って実装されているため、インスタンスは1つだけ生成されます。<br/>
 * インスタンスの取得には、OPNSettings#sharedController()を使用してください。<br/>
 */
@interface OPNSettings : NSObject

#pragma mark - Initialize Method
//--------------------------------------------------------------------------------------------------
// Initialize Method
//--------------------------------------------------------------------------------------------------

/**
 * 唯一のインスタンスを取得します。
 * @return インスタンス
 */
+ (OPNSettings *)sharedController;

/**
 * 唯一のインスタンスを取得します。
 * @param data データコレクターから取得したバイナリデータをセットします。
 * @return インスタンス
 */
+ (OPNSettings *)sharedController:(NSData *)data;


#pragma mark - Public Methods
//--------------------------------------------------------------------------------------------------
// Public Methods
//--------------------------------------------------------------------------------------------------
/**
 * ファームウェアバージョンを取得します。
 * @return  ファームウェアバージョン
 */
- (NSString *)getFarmwareVersion;

/**
 * セットしたビット配列を初期化します。
 */
- (void)clearData;


#pragma mark - Public Methods (XSW)
//--------------------------------------------------------------------------------------------------
// Public Methods(XSW)
//--------------------------------------------------------------------------------------------------

#pragma mark - XSW5
//--------------------------------------------------------------------------------------------------
// XSW5
//--------------------------------------------------------------------------------------------------

/**
 * EAN-13/8の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)JAN_EAN_13_8_PERMISSION;

/**
 * EAN-13のCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)JAN_EAN_13_TRANSMIT_CD;

/**
 * WPC（UPC、EAN、JAN）のCD計算に関する設定を取得します。
 * @return YES:CD計算する NO:CD計算しない
 */
- (BOOL)WPC_UPC_EAN_JAN_CD_CALCULATION;//XSW5,6,7,8共通

/**
 * EAN-13/8（Addon2）の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)JAN_EAN_13_8_ADDON2_PERMISSION;

/**
 * EAN-13/8（Addon5）の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)JAN_EAN_13_8_ADDON5_PERMISSION;

#pragma mark - XSW6
//--------------------------------------------------------------------------------------------------
// XSW6
//--------------------------------------------------------------------------------------------------
/**
 * JAN/EAN-8のCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)JAN_EAN_8_TRANSMIT_CD;


#pragma mark - XSW7
//--------------------------------------------------------------------------------------------------
// XSW7
//--------------------------------------------------------------------------------------------------
/**
 * UPC-A/Eの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)UPC_A_E_PERMISSION;

/**
 * UPC-AのCD転送に関する設定を取得します。
 * @return xsw7 2bit目
 */
- (BOOL)UPC_A_TRANSMIT_CD_B2;

/**
 * UPC-AのCD転送に関する設定を取得します。
 * @return xsw7 4bit目
 */
- (BOOL)UPC_A_TRANSMIT_CD_B4;

/**
 * UPC-A/E（Addon2）の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)UPC_A_E_Addon2_PERMISSION;

/**
 * UPC-A/E（Addon5）の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)UPC_A_E_Addon5_PERMISSION;

/**
 * UPC-AのCD転送に関する設定を取得します。
 * @return 
 *  1:１３桁（先頭０とＣＤ付き）<br/>
 *  2:１２桁（先頭０なし）<br/>
 *  3:１２桁（ＣＤなし）<br/>
 *  0:１１桁（先頭０とＣＤなし）<br/>
 */
- (int)UPC_A_TRANSMIT;

#pragma mark - XSW8
//--------------------------------------------------------------------------------------------------
// XSW8
//--------------------------------------------------------------------------------------------------
/**
 * UPC-EのCD転送に関する設定を取得します。
 * @return xsw8 2bit目
 */
- (BOOL)UPC_E_TRANSMIT_CD_B2;

/**
 * UPC-EのCD転送に関する設定を取得します。
 * @return xsw8 4bit目
 */
- (BOOL)UPC_E_TRANSMIT_CD_B4;

/**
 * UPC-EのCD転送に関する設定を取得します。
 * @return
 * 1:８桁（先頭０とＣＤ付き）<br/>
 * 2:７桁（先頭なし）<br/>
 * 3:７桁（ＣＤなし）<br/>
 * 0:６桁（先頭０とＣＤなし）<br/>
 */
- (int)UPC_E_TRANSMIT;

#pragma mark - XSW9
//--------------------------------------------------------------------------------------------------
// XSW9
//--------------------------------------------------------------------------------------------------
/**
 * Code39の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODE_39_PERMISSION;

/**
 * Code39の読取許可設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)CODE_39_TRANSMIT_CD;

/**
 * Code39のCD計算に関する設定を取得します。
 * @return YES:CD計算する NO:CD計算しない
 */
- (BOOL)CODE_39_CALCULATION;

/**
 * Code39のスタートストップに関する設定を取得します。
 * @return YES:転送する NO:転送しない
 */
- (BOOL)CODE_39_START_STOP_CD;

#pragma mark - XSW10
//--------------------------------------------------------------------------------------------------
// XSW10
//--------------------------------------------------------------------------------------------------
/**
 * NW-7の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)NW_7_PERMISSION;

/**
 * NW-7の読取許可設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)NW_7_TRANSMIT_CD;

/**
 * NW-7のCD計算に関する設定を取得します。
 * @return
 * 1: CD計算する Mod10/W1,2 spec1<br/>
 * 2: CD計算する Mod16<br/>
 * 3: CD計算する 7 check<br/>
 * 4: CD計算する Mod11<br/>
 * 0: CD計算しない<br/>
 */
- (int)NW_7_CALCULATION;

/**
 * NW-7のスタートストップに関する設定を取得します。
 * @return
 * 1: 転送する ABCD/TN*E<br/>
 * 2: 転送する abcd/tn*e<br/>
 * 3: 転送する ABCD<br/>
 * 4: 転送する abcd<br/>
 * 5: 転送する DC1DC2DC3DC4<br/>
 * 0: 転送しない<br/>
 */
- (int)NW_7_START_STOP_CD;


#pragma mark - XSW11
//--------------------------------------------------------------------------------------------------
// XSW11
//--------------------------------------------------------------------------------------------------
/**
 * Interleaved2of5の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)INTERLEAVED_2OF5_PERMISSION;

/**
 * Industrial 2 of 5/Interleaved 2 of 5のCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)INDUSTRIAL_INTERLEAVED_2OF5_TRANSMIT_CD;

/**
 * Industrial 2 of 5/Interleaved 2 of 5のCD計算に関する設定を取得します。
 * @return YES:CD計算する NO:CD計算しない
 */
- (BOOL)INDUSTRIAL_INTERLEAVED_2OF5_CALCULATION;

/**
 * Industrial2of5の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)INDUSTRIAL_2OF5_PERMISSION;


#pragma mark - XSW12
//--------------------------------------------------------------------------------------------------
// XSW12
//--------------------------------------------------------------------------------------------------
/**
 * IATAの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)IATA_PERMISSION;

/**
 * IATAのCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)IATA_TRANSMIT_CD;

/**
 * IATAのCD計算に関する設定を取得します。
 * @return
 * 1: C/D計算する (CPN+FORM SERIAL)<br/>
 * 2: C/D計算する (FORM SERIAL)<br/>
 * 3: C/D計算する (ALL DATA)<br/>
 * 0: C/D計算しない<br/>
 */
- (int)IATA_CALCULATION;

/**
 * Code93の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODE_93_PERMISSION;

/**
 * Code93のCD計算に関する設定を取得します。
 * @return YES:CD計算する NO:CD計算しない
 */
- (BOOL)CODE_93_CALCULATION;

#pragma mark - XSW13
//--------------------------------------------------------------------------------------------------
// XSW13
//--------------------------------------------------------------------------------------------------
/**
 * Code128の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODE_128_PERMISSION;

/**
 * Code128のCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)CODE_128_TRANSMIT_CD;

/**
 * Code 128/EAN-128のCD計算に関する設定を取得します。
 * @return YES:CD計算する NO:CD計算しない
 */
- (BOOL)CODE_128_EAN_128_CALCULATION;

/**
 * Code128の読取許可設定を取得します。
 * @return 
 * 2: 許可　if Possible<br/>
 * 1: 許可　EAN-128 Only<br/>
 * 0: 禁止　Code 128として出力<br/>
 */
- (int)EAN_128_PERMISSION;

/**
 * SCodeの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)S_CODE_PERMISSION;

#pragma mark - XSW14
//--------------------------------------------------------------------------------------------------
// XSW14
//--------------------------------------------------------------------------------------------------
/**
 * MSI/Plesseyの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)MSI_PLESSEY_PERMISSION;

/**
 * MSI/PlesseyのCD転送に関する設定を取得します。
 * @return 
 * 1:CD転送する CD1<br/>
 * 2:CD転送する CD1 and CD2<br/>
 * 0:CD転送しない
 */
- (int)MSI_PLESSEY_TRANSMIT;

/**
 * MSI/PlesseyのCD計算に関する設定を取得します
 * @return 
 * 1:C/D計算する CD1 only (Mod10)<br/>
 * 2:C/D計算する CD's (Mod10/Mod10)<br/>
 * 3:C/D計算する CD's (Mod10/Mod11)<br/>
 * 4:C/D計算する CD's (Mod11/Mod10)<br/>
 * 0:C/D計算しない
 */
- (int)MIS_PLESSEY_CALCULATION;
/**
 * Code128の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)UK_PLESSEY_PERMISSION;

#pragma mark - XSW15
//--------------------------------------------------------------------------------------------------
// XSW15
//--------------------------------------------------------------------------------------------------
/**
 * Telepenの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)TELEPEN_PERMISSION;

/**
 * Matrix2of5の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)MATRIX_2OF5_PERMISSION;

/**
 * Tri-Opticの読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)TRI_OPTIC_PERMISSION;

#pragma mark - XSW16
//--------------------------------------------------------------------------------------------------
// XSW16
//--------------------------------------------------------------------------------------------------
/**
 * GS1 DataBar の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)GS1_DATABAR_RSS14_PERMISSION;
/**
 * GS1 DataBar Limited の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)GS1_DATABAR_LIMITED_PERMISSION;
/**
 * GS1 DataBar Expanded の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)GS1_DATABAR_EXPANDED_PERMISSION;

/**
 * GS1 DataBarのCD転送に関する設定を取得します。
 * @return YES:CD転送する NO:CD転送しない
 */
- (BOOL)GS1_DATABAR_TRANSMIT_CD;


#pragma mark - XSW17
//--------------------------------------------------------------------------------------------------
// XSW17
//--------------------------------------------------------------------------------------------------
/**
 * Code 3 of 5 の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODE_3OF5_PERMISSION;


#pragma mark - XSW19
//--------------------------------------------------------------------------------------------------
// XSW19
//--------------------------------------------------------------------------------------------------
/**
 * MicroPDF417 の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)MICRO_PDF_417_PERMISSION;
/**
 * PDF417 の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)PDF_417_PERMISSION;
/**
 * Code11 の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODE_11_PERMISSION;

#pragma mark - XSW26
//--------------------------------------------------------------------------------------------------
// XSW26
//--------------------------------------------------------------------------------------------------
/**
 * IntelligentMail の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)INTELLIGENT_MAIL_PERMISSION;
/**
 * Postnet の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)POSTNET_PERMISSION;
/**
 * Japanese Postal の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)JAPANESE_POSTAL_PERMISSION;
/**
 * CodablockF の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CODABLOCK_F_PERMISSION;
/**
 * Data Matrix(ECC200) の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)DATA_MATRIX_ECC200_PERMISSION;
/**
 * Data Matrix(ECC000-140) の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)DATA_MATRIX_ECC000_140_PERMISSION;
/**
 * Aztec code の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)AZTEC_CODE_PERMISSION;
/**
 * Aztec runes の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)AZTEC_RUNES_PERMISSION;
/**
 * Chinese Sensible code の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)CHINESE_SENSIBLE_CODE_PERMISSION;
/**
 * QR code の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)QR_CODE_PERMISSION;
/**
 * MicroQR code の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)MICRO_QR_CODE_PERMISSION;
/**
 * Maxi code の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)MAXI_CODE_PERMISSION;
/**
 * Composite on GS1 Databar の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)COMPOSITE_ON_GS1_DATABAR_PERMISSION;
/**
 * Composite on UPC/EAN の読取許可設定を取得します。
 * @return YES:許可 NO:禁止
 */
- (BOOL)COMPOSITE_ON_UPC_EAN_PERMISSION;

#pragma mark - Public Methods (BT)
//--------------------------------------------------------------------------------------------------
// Public Methods(BT)
//--------------------------------------------------------------------------------------------------
/**
 * 読み取りモードの設定を取得します。
 * @return 1:単発読み 2:複数読み 3:連続読み
 */
- (int)READ_MODE;
/**
 * 読み取り時間の設定を取得します。
 * @return   0:無限 -1:0秒 50:1秒 100:2秒 150:3秒 200:4秒 250:5秒 300:6秒 350:7秒 400:8秒 450:9秒 -:読み取り時間10倍
 */
- (int)READABLE_TIME;
/**
 * 照合回数の設定を取得します。
  * @return   0:1回読取 0回照合 1:2回読取 1回照合 2:3回読取 2回照合 3:4回読取 3回照合 4:5回読取 4回照合 5:6回読取 5回照合 6:7回読取 6回照合 7:8回読取 7回照合 8:9回読取 8回照合 9:10回読取 9回照合 10:11回読取 10回照合 11:12回読取 11回照合 12:13回読取 12回照合 13:14回読取 13回照合 14:15回読取 14回照合 15:16回読取 15回照合
 */
- (int)REDUNDANT_READING;
/**
 * 二度読み防止タイマーの設定を取得します。
 * @return   0:無限 3:50ms 5:100ms 10:200ms 15:300ms 20:400ms 25:500ms 30:600ms
 */
- (int)MULTIPLE_READ_RESET_TIME;
/**
 * アドオンタイマーの設定を取得します。
 * @return   0:なし 13:250ms 25:500ms 38:750ms
 */
- (int)ADDON_WAIT_TIME;
/**
 * ブザーボリュームの設定を取得します。
 * @return   127:最大 32:大 8:中 1:小
 */
- (int)BUZZER_VOLUME;
/**
 * LED点灯時間の設定を取得します。
 * @return   
 * 0:無効<br/>
 * 10:200ms<br/>
 * 20:400ms<br/>
 * 30:600ms
 */
- (int)GOOD_READ_LED_ON_TIME;
/**
 * トリガモードの設定を取得します。
 * @return   0:トリガ無効 1:トリガ有効              
 */
- (int)TRIGGER_MODE_ENABLE;
/**
 * トリガリピートの設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)TRIGGER_REPEAT_ENABLE;
/**
 * ブザー音の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)BUZZER_ENABLE;
/**
 * ブザートーンの設定を取得します。
 * @return   0:単音 1:高低 2:低高 3:4.5KHｚ 4:2.2KHz-2KHz
 */
- (int)BUZZER_TONE;
/**
 * ブザー鳴動時間の設定を取得します。
 * @return   5:100ms 10:200ms 20:400ms 2:50ms
 */
- (int)BUZZER_PERIOD;
/**
 * ブザー鳴動タイミングの設定を取得します。
 * @return   0:転送前ブザー 1:転送後ブザー
 */
- (int)BUZZER_MODE;
/**
 * 接続相手アドレスの設定を取得します。
 * @return   接続相手アドレス
 */
- (NSString *)DESTINATION_ADDRESS;

/**
 * 認証の設定を取得します。
 * @return   0:認証無し 1:認証有り(毎回)
 */
- (int)AUTHENTICATION;
/**
 * 暗号化の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)ENCRYPTION_ENABLE;
/**
 * コマンド応答の設定を取得します。
 * @return   1:有り（ACK/NAK) 0:BT33(0-7)の設定に従う
 */
- (int)COMMAND_RESPONSE;
/**
 * PINコードの設定を取得します。
 * @return   PINコード
 */
- (NSString *)PIN_CODE;
/**
 * トリガ接続の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)TRIGGER_CONNECTION_ENABLE;
/**
 * アドレスバーコード読み取りによる接続処理の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)ADDRESS_BARCODE_CONNECTION_ENABLE;
/**
 * ACK/NAK制御の設定を取得します。
 * @return   0:無し 1:有り 2:有り（NoResponse）
 */
- (int)ACK_NAK_CONTROL;
/**
 * 接続モードの設定を取得します。
 * @return   0:SPPマスター 1:SPPスレーブ 2:HID
 */
- (int)CONNECTION_MODE;
/**
 * スレーブ接続待ち時間の設定を取得します。
 * @return   
 * 1500:30秒<br/>
 * 3000:1分<br/>
 * 6000:2分<br/>
 * 9000:3分<br/>
 * 12000:4分
 */
- (int)SLAVE_CONNECTION_WAIT_TIME;
/**
 * 自動再接続有効時間の設定を取得します。
 * @return
 * 0:無効<br/>
 * 3000:1分<br/>
 * 6000:2分<br/>
 * 9000:3分<br/>
 * 12000:4分<br/>
 * 15000:5分<br/>
 * 18000:6分<br/>
 * 21000:7分<br/>
 * 24000:8分<br/>
 * 27000:9分<br/>
 * 30000:10分<br/>
 * 33000:11分<br/>
 * 36000:12分<br/>
 * 39000:13分<br/>
 * 42000:14分<br/>
 * 45000:15分
 */
- (int)AUTO_RECONNECTION_TIME;
/**
 * 自動切断時間の設定を取得します。
 * @return   
 * 0:無効<br/>
 * 30000:10分<br/>
 * 60000:20分<br/>
 * 90000:30分<br/>
 * 120000:40分<br/>
 * 150000:50分<br/>
 * 180000:60分<br/>
 * 3000:1分<br/>
 * 6000:2分<br/>
 * 9000:3分<br/>
 * 12000:4分<br/>
 * 15000:5分<br/>
 * 18000:6分<br/>
 * 21000:7分<br/>
 * 24000:8分<br/>
 * 27000:9分<br/>
 * 500:10秒<br/>
 * 1000:20秒<br/>
 * 1500:30秒<br/>
 * 2000:40秒<br/>
 * 2500:50秒
 */
- (int)AUTO_DISCONNECTION_TIME;
/**
 * トリガ接続長押し時間の設定を取得します。
 * @return   0:トリガ接続無効 50:1秒 100:2秒 150:3秒 200:4秒 250:5秒 300:6秒 350:7秒 400:8秒 450:9秒
 */
- (int)TRIGGER_BUTTON_PUSHTIME_FOR_CONNECTION;
/**
 * トリガ切断長押し時間の設定を取得します。
 * @return   0:トリガ切断無効 50:1秒 100:2秒 150:3秒 200:4秒 250:5秒 300:6秒 350:7秒 400:8秒 450:9秒           
 */
- (int)TRIGGER_BUTTON_PUSHTIME_FOR_DISCONNECTION;
/**
 * ACK/NAK待ち時間の設定を取得します。
 * @return   5:100ms 25:500ms 50:1s
 */
- (int)ACK_NAK_WAIT_TIME;
/**
 * 圏外メモリーの設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)OUT_OF_RANGE_MEMORY_ENABLE;
/**
 * データコレクトの設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)DATA_COLLECT_MODE_ENABLE;
/**
 * バーコード読み取り自動接続の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)AUTO_CONNECTION_ENABLE;
/**
 * データコレクターからの切断時ブザーの設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)DISCONNECTION_BUZZER_FROM_DATA_COLLECTOR;
/**
 * 接続相手からの切断ブザーの設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)DISCONNECTION_BUZZER_FROM_DESTINATION;
/**
 * メモリーデータの出力方法の設定を取得します。
 * @return   0:接続時即出力 1:ファンクションキー押下かコマンドで出力 :データ出力コマンド                  
 */
- (int)MEMORY_OUTPUT_METHOD;
/**
 * USB接続時COM通信の設定を取得します。
 * @return   0:無効 1:有効
 */
- (int)USB_CONNECTION;
/**
 * ファンクション押下時出力の設定を取得します。
 * @return   0x09:HT 0x0A:LF 0x0D:CR 0x18:CAN 0x1B:ESC
 */
- (int)FUNCTION_BUTTON_INPUT;
/**
 * Bluetoothデバイス名（自機）の設定を取得します。
 * @return  Bluetoothデバイス名
 */
- (NSString *)DEVICE_NAME;
/**
 * 前回のスレーブ接続時の相手アドレスの設定を取得します。
 * @return  前回のスレーブ接続時の相手アドレス
 */
- (NSString *)DESTINATION_ADDRESS_LAST_TIME;
/**
 * キャラクタ間ディレイの設定を取得します。
 * @return   0:0ms 10:10ms 20:20ms 30:30ms 40:40ms 50:50ms 60:60ms 70:70ms 80:80ms 90:90ms 100:100ms 150:150ms 200:200ms 250:250ms 300:300ms 350:350ms 400:400ms 450:450ms 500:500ms 550:550ms 600:600ms
 */
- (int)INTER_CHARACTER_DELAY;
/**
 * キーボード言語の設定を取得します。
 * @return   0x01:USA (Default) 0x02:UK 0x03:French 0x04:German 0x05:Italian    0x09:Belgian 0x0A:Spanish 0x0B:Portuguese 0x0C:Dutch 0x0D:Swedish 0x0E:Finnish 0x0F:Swiss German 0x10:Swiss French 0x11:Japanese 0x12:Danish 0x13:Norwegian  0x15:Czech
 */
- (int)KEYBOARD_LANGUAGE;
/**
 * NumPad使用の設定を取得します。
 * @return   0xFF:Numパッドを使用しない 0x00:Numパッドを使用する
 */
- (int)USE_NUM_PAD;
/**
 * NumPadモードの設定を取得します。
 * @return   0x00:AUTO ホストのNumlockキーの状態によって動作する 0x01:強制的にNumlockキーが押されていない状態にする                   
 */
- (int)NUM_PAD_MODE;
/**
 * CapsLockモードの設定を取得します。
 * @return 0:自動CapsLockモード　1:CapsLockモード無し　2:CapsLockモード有り
 */
- (int)CAPS_LOCK_MODE;
/**
 * グッドリードバイブレーターの設定を取得します。
 * @return YES:有効 NO:無効
 */
- (BOOL)GOOD_READ_VIBRATOR_ENABLE;



#pragma mark - Public Methods (Prefix)
//--------------------------------------------------------------------------------------------------
// Public Methods(Prefix)
//--------------------------------------------------------------------------------------------------
/** UPC-Aのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)UPCA_PREFIX;
/** UPC-Aアドオンのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)UPCA_ADDON_PREFIX;
/** UPC-Eのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)UPCE_PREFIX;
/** UPC-Eアドオンのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)UPCE_ADDON_PREFIX;
/** EAN-13のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)EAN13_PREFIX;
/** EAN-13アドオンのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)EAN13_ADDON_PREFIX;
/** EAN-8のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)EAN8_PREFIX;
/** EAN-8アドオンのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)EAN8_ADDON_PREFIX;
/** Code-39のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)CODE39_PREFIX;
/** Tri-Opticのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)TRI_OPTIC_PREFIX;
/** NW-7のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)NW7_PREFIX;
/** D.2of5のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)D2OF5_PREFIX;
/** I.2of5のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)I2OF5_PREFIX;
/** SCodeのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)SCODE_PREFIX;
/** Matrix2of5のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)MATRIX2OF5_PREFIX;
/** Code93のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)CODE93_PREFIX;
/** Code128のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)CODE128_PREFIX;
/** MSI/Plesseyのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)MSIPLESSEY_PREFIX;
/** IATAのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)IATA_PREFIX;
/** UK/Plesseyのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)UKPLESSEY_PREFIX;
/** Telepenのプリフィックスを指定します 
 * @return プリフィックス
 */

- (NSData *)TELEPEN_PREFIX;
/** RSSのプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)RSS_PREFIX;
/** PDF417のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)PDF417_PREFIX;
/** MicroPDF417のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)MICRO_PDF417_PREFIX;
/** Code11のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)CODE11_PREFIX;
/** Code3of5のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)CODE3OF5_PREFIX;
/** EAN-128のプリフィックスを指定します 
 * @return プリフィックス
 */
- (NSData *)EAN128_PREFIX;
/** コモンプリフィックス を指定します 
 * @return プリフィックス
 */
- (NSData *)COMMON_PREFIX;

//2D
/** Intelligent Mailのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)INTELLIGENT_MAIL_PREFIX;
/** Postnetのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)POSTNET_PREFIX;
/** Japanese postalのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)JAPANESE_POSTAL_PREFIX;
/** CodablockFのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)CODABLOCK_F_PREFIX;
/** Data Matrix(ECC200,ECC000-140)のプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)DATA_MATRIX_ECC200_ECC000_140_PREFIX;
/** Aztec code/Aztec runesのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)AZTEC_CODE_RUNES_PREFIX;
/** Chinese Sensible codeのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)CHINESE_SENSIBLE_CODE_PREFIX;
/** QR/MicroQRのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)QR_MICRO_QR_PREFIX;
/** Maxi codeのプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)MAXI_CODE_PREFIX;
/** Composite (on GS1Databar,UPC/EAN)のプリフィックスを指定します
 * @return プリフィックス
 */
- (NSData *)COMPOSITE_ON_GS1_DATABAR_ON_UPC_EAN_PREFIX;

#pragma mark - Public Methods (Suffix)
//--------------------------------------------------------------------------------------------------
// Public Methods(Suffix)
//--------------------------------------------------------------------------------------------------

/** UPC-Aのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)UPCA_SUFFIX;
/** UPC-Aアドオンのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)UPCA_ADDON_SUFFIX;
/** UPC-Eのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)UPCE_SUFFIX;
/** UPC-Eアドオンのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)UPCE_ADDON_SUFFIX;
/** EAN-13のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)EAN13_SUFFIX;
/** EAN-13アドオンのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)EAN13_ADDON_SUFFIX;
/** EAN-8のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)EAN8_SUFFIX;
/** EAN-8アドオンのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)EAN8_ADDON_SUFFIX;
/** Code-39のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)CODE39_SUFFIX;
/** Tri-Opticのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)TRI_OPTIC_SUFFIX;
/** NW-7のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)NW7_SUFFIX;
/** D.2of5のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)D2OF5_SUFFIX;
/** I.2of5のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)I2OF5_SUFFIX;
/** SCodeのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)SCODE_SUFFIX;
/** Matrix2of5のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)MATRIX2OF5_SUFFIX;
/** Code93のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)CODE93_SUFFIX;
/** Code128のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)CODE128_SUFFIX;
/** MSI/Plesseyのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)MSIPLESSEY_SUFFIX;
/** IATAのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)IATA_SUFFIX;
/** UK/Plesseyのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)UKPLESSEY_SUFFIX;
/** Telepenのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)TELEPEN_SUFFIX;
/** RSSのサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)RSS_SUFFIX;
/** PDF417のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)PDF417_SUFFIX;
/** MicroPDF417のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)MICRO_PDF417_SUFFIX;
/** Code11のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)CODE11_SUFFIX;
/** Code3of5のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)CODE3OF5_SUFFIX;
/** EAN-128のサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)EAN128_SUFFIX;
/** コモンサフィックスを指定します 
 * @return サフィックス
 */
- (NSData *)COMMON_SUFFIX;

//2D
/** Intelligent Mailのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)INTELLIGENT_MAIL_SUFFIX;
/** Postnetのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)POSTNET_SUFFIX;
/** Japanese postalのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)JAPANESE_POSTAL_SUFFIX;
/** CodablockFのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)CODABLOCK_F_SUFFIX;
/** Data Matrix(ECC200,ECC000-140)のサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)DATA_MATRIX_ECC200_ECC000_140_SUFFIX;
/** Aztec code/Aztec runesのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)AZTEC_CODE_RUNES_SUFFIX;
/** Chinese Sensible codeのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)CHINESE_SENSIBLE_CODE_SUFFIX;
/** QR/MicroQRのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)QR_MICRO_QR_SUFFIX;
/** Maxi codeのサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)MAXI_CODE_SUFFIX;
/** Composite (on GS1Databar,UPC/EAN)のサフィックスを指定します
 * @return サフィックス
 */
- (NSData *)COMPOSITE_ON_GS1_DATABAR_ON_UPC_EAN_SUFFIX;
@end
