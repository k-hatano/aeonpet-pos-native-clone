//
//  OPNSettingFormat.h
//  
//
//  Copyright (c) 2012年 OPTOELECTRONICS CO., LTD. All rights reserved.
//

typedef union {
    struct {
        
        union {
            unsigned char byte[2];
        } XSW0;
        
        union {
            unsigned char byte[2];
        } XSW1;
        
        union {
            unsigned char byte[2];
        } XSW2;
        
        union {
            unsigned char byte[2];
        } XSW3;
        
        union {
            unsigned char byte[2];
        } XSW4;
        
        union {
            struct {
                Byte JAN_EAN_13_8_ADDON2_PERMISSION : 1;
                Byte : 3;
                Byte JAN_EAN_13_8_ADDON5_PERMISSION : 1;
                Byte : 3;
                
                Byte JAN_EAN_13_8_PERMISSION : 1;
                Byte : 1;
                Byte JAN_EAN_13_TRANSMIT_CD : 1;
                Byte WPC_UPC_EAN_JAN_CD_CALCULATION : 1;//XSW5,6,7,8共通
                Byte : 4;
            } FEILDS;
            unsigned char byte[2];
        } XSW5;

        union {
            struct {
                Byte : 8;
                
                Byte : 2;
                Byte JAN_EAN_8_TRANSMIT_CD : 1;
                Byte : 5;
            } FEILDS;
            unsigned char byte[2];
        } XSW6;

        union {
            struct {
                //8-15
                Byte UPC_A_E_Addon2_PERMISSION : 1;
                Byte : 3;
                Byte UPC_A_E_Addon5_PERMISSION : 1;
                Byte : 3;
                
                //0-7
                Byte UPC_A_E_PERMISSION : 1;
                Byte : 1;//XSW5,6,7,8共通
                Byte UPC_A_TRANSMIT_CD_B2 : 1;
                Byte : 1;
                Byte UPC_A_TRANSMIT_CD_B4 : 1;
                Byte : 3;
            } FEILDS;
            unsigned char byte[2];
        } XSW7;        
        
        union {
            struct {
                Byte : 8;
                
                Byte : 2;
                Byte UPC_E_TRANSMIT_CD_B2 : 1;
                Byte : 1;//XSW5,6,7,8共通
                Byte UPC_E_TRANSMIT_CD_B4 : 1;
                Byte : 3;
            } FEILDS;
            unsigned char byte[2];
        } XSW8;
        
        union {
            struct {
                Byte CODE_39_START_STOP_CD : 1;
                Byte : 7;
                
                Byte CODE_39_PERMISSION : 1;
                Byte : 1;
                Byte CODE_39_TRANSMIT_CD : 1;
                Byte CODE_39_CALCULATION : 1;
                Byte : 4;
            } FEILDS;
            unsigned char byte[2];
        } XSW9;
        
        union {
            struct {
                Byte NW_7_START_STOP_CD : 3;
                Byte : 5;
                
                Byte NW_7_PERMISSION : 1;
                Byte : 1;
                Byte NW_7_TRANSMIT_CD : 1;
                Byte NW_7_CALCULATION : 3;
                Byte : 2;
            } FEILDS;
            unsigned char byte[2];
        } XSW10;
        
        union {
            struct {
                Byte INDUSTRIAL_2OF5_PERMISSION : 1;
                Byte :1;
                Byte : 3;
                Byte : 3;
                
                Byte INTERLEAVED_2OF5_PERMISSION : 1;
                Byte :1;
                Byte INDUSTRIAL_INTERLEAVED_2OF5_TRANSMIT_CD : 1;
                Byte INDUSTRIAL_INTERLEAVED_2OF5_CALCULATION : 1;
                Byte : 4;
            } FEILDS;
            unsigned char byte[2];
        } XSW11;
        
        union {
            struct {
                Byte CODE_93_PERMISSION : 1;
                Byte :2;
                Byte CODE_93_CALCULATION : 1;
                Byte :4;
                
                Byte IATA_PERMISSION : 1;
                Byte : 1;
                Byte IATA_TRANSMIT_CD : 1;
                Byte IATA_CALCULATION : 2;
                Byte : 3;
            } FEILDS;
            unsigned char byte[2];
        } XSW12;
        
        union {
            struct {
                Byte S_CODE_PERMISSION : 1;
                Byte : 7;
                
                Byte CODE_128_PERMISSION : 1;
                Byte : 1;
                Byte CODE_128_TRANSMIT_CD : 1;
                Byte CODE_128_EAN_128_CALCULATION : 1;
                Byte EAN_128_PERMISSION : 2;
                Byte : 2;

            } FEILDS;
            unsigned char byte[2];
        } XSW13;
        
        union {
            struct {
                Byte UK_PLESSEY_PERMISSION : 1;
                Byte : 7;
                
                Byte MSI_PLESSEY_PERMISSION : 1;
                Byte : 1;
                Byte MSI_PLESSEY_TRANSMIT_CD : 2;
                Byte MIS_PLESSEY_CALCULATION : 3;//Mode
                Byte : 1;
                
            } FEILDS;
            unsigned char byte[2];
        } XSW14;
        
        union {
            struct {
                Byte MATRIX_2OF5_PERMISSION : 1;
                Byte : 3;
                Byte TRI_OPTIC_PERMISSION : 1;
                Byte : 3;
                
                Byte TELEPEN_PERMISSION : 1;
                Byte : 7;
            } FEILDS;
            unsigned char byte[2];
        } XSW15;
        
        union {
            struct {
                Byte : 8;
                
                Byte GS1_DATABAR_RSS14_PERMISSION : 1;
                Byte GS1_DATABAR_LIMITED_PERMISSION : 1;
                Byte GS1_DATABAR_EXPANDED_PERMISSION : 1;
                Byte GS1_DATABAR_TRANSMIT_CD : 1;
                Byte : 4;
            } FEILDS;
            unsigned char byte[2];
        } XSW16;
        
        union {
            struct {
                Byte CODE_3OF5_PERMISSION : 1;
                Byte : 7;
                
                Byte : 8;
            } FEILDS;
            unsigned char byte[2];
        } XSW17;
        
        union {
            unsigned char byte[2];
        } XSW18;
        
        union {
            struct{
                Byte CODE_11_PERMISSION : 1;
                Byte : 7;
                
                Byte MICRO_PDF_417_PERMISSION :1;
                Byte : 3;
                Byte PDF_417_PERMISSION : 1;
                Byte : 3;
            }FEILDS;
            unsigned char byte[2];
        } XSW19;
        
        union {
            unsigned char byte[2];
        } XSW20;
        
        union {
            unsigned char byte[2];
        } XSW21;
        
        union {
            unsigned char byte[2];
        } XSW22;
        
        union {
            unsigned char byte[2];
        } XSW23;
        
        union {
            unsigned char byte[2];
        } XSW24;
        
        union {
            unsigned char byte[2];
        } XSW25;
        
        union {
            struct {
                //8-15
                Byte CHINESE_SENSIBLE_CODE_PERMISSION : 1;
                Byte QR_CODE_PERMISSION : 1;
                Byte MICRO_QR_CODE_PERMISSION : 1;
                Byte MAXI_CODE_PERMISSION : 1;
                Byte COMPOSITE_ON_GS1_DATABAR_PERMISSION : 1;
                Byte COMPOSITE_ON_UPC_EAN_PERMISSION : 1;
                Byte : 2;
                
                //0-7
                Byte INTELLIGENT_MAIL_PERMISSION : 1;
                Byte POSTNET_PERMISSION : 1;
                Byte JAPANESE_POSTAL_PERMISSION : 1;
                Byte CODABLOCK_F_PERMISSION : 1;
                Byte DATA_MATRIX_ECC200_PERMISSION : 1;
                Byte DATA_MATRIX_ECC000_140_PERMISSION : 1;
                Byte AZTEC_CODE_PERMISSION : 1;
                Byte AZTEC_RUNES_PERMISSION : 1;
            } FEILDS;
            unsigned char byte[2];
        } XSW26;
        
    } XSW;
    unsigned char byte[240];
} OPNSettingXSW;


typedef union {
    struct {
        union {
            struct {
                Byte READ_MODE : 8;
                
                Byte : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT0;
        
        union {
            unsigned char byte[2];
        } BT1;
        
        union {
            struct {
                int32_t READABLE_TIME : 32;
            } FEILDS;
            unsigned char byte[4];
        } BT2_BT3;
        
        union {
            struct {
                Byte REDUNDANT_READING : 8;
                
                Byte MULTIPLE_READ_RESET_TIME : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT4;
        
        union {
            struct {
                Byte ADDON_WAIT_TIME : 8;
                
                Byte BUZZER_VOLUME : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT5;
        
        union {
            struct {
                Byte GOOD_READ_LED_ON_TIME : 8;//ＬＥD点灯時間
                
                Byte TRIGGER_MODE_ENABLE : 8;//トリガーモード
            } FEILDS;
            unsigned char byte[2];
        } BT6;
        
        union {
            struct {
                Byte TRIGGER_REPEAT_ENABLE : 8;//トリガーリピート
                
                Byte : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT7;
        
        union {
            struct {
                int32_t BUZZER_ENABLE : 32;//ブザー音
            } FEILDS;
            unsigned char byte[4];
        } BT8_BT9;
        
        union {
            struct {
                int32_t BUZZER_TONE : 32;//ブザートーン
            } FEILDS;
            unsigned char byte[4];
        } BT10_BT11;
        
        union {
            struct {
                int32_t BUZZER_PERIOD : 32;//ブザー鳴動時間
            } FEILDS;
            unsigned char byte[4];
        } BT12_BT13;
        
        union {
            struct {
                int32_t BUZZER_MODE : 32;//ブザー鳴動タイミング
            } FEILDS;
            unsigned char byte[4];
        } BT14_BT15;
        
        union {
            struct {
                int32_t DESTINATION_ADDRESS_01 : 32;//接続相手アドレス
                int32_t DESTINATION_ADDRESS_02 : 32;//接続相手アドレス
                int32_t DESTINATION_ADDRESS_03 : 32;//接続相手アドレス
            } FEILDS;
            unsigned char byte[12];
        } BT16_BT17_BT18_BT19_BT20_BT21;
        
        union {
            struct {
                Byte : 8;
                
                Byte AUTHENTICATION : 8;//認証
            } FEILDS;
            unsigned char byte[2];
        } BT22;
        
        union {
            struct {
                Byte ENCRYPTION_ENABLE : 8;//暗号化
                
                Byte COMMAND_RESPONSE : 8;//コマンド応答
            } FEILDS;
            unsigned char byte[2];
        } BT23;
        
        union {
            struct {
                int32_t  PIN_CODE_01 : 32;//PIN
                int32_t  PIN_CODE_02 : 32;//PIN
                int32_t  PIN_CODE_03 : 32;//PIN
                int32_t  PIN_CODE_04 : 32;//PIN
            } FEILDS;
            unsigned char byte[16];
        } BT24_BT25_BT26_BT27_BT28_BT29_BT30_BT31;
        
        union {
            struct {
                Byte : 8;
                
                Byte TRIGGER_CONNECTION_ENABLE : 8;//トリガー接続
            } FEILDS;
            unsigned char byte[2];
        } BT32;
        
        union {
            struct {
                Byte ADDRESS_BARCODE_CONNECTION_ENABLE : 8;//アドレスバーコード読取による接続処理実行有無
                
                Byte ACK_NAK_CONTROL : 8;//ACK／NAK制御
            } FEILDS;
            unsigned char byte[2];
        } BT33;
        
        union {
            struct {
                int32_t CONNECTION_MODE : 32;//接続モード
            } FEILDS;
            unsigned char byte[4];
        } BT34_BT35;
        
        union {
            struct {
                int32_t SLAVE_CONNECTION_WAIT_TIME : 32;//スレーブ接続待ち時間
            } FEILDS;
            unsigned char byte[4];
        } BT36_BT37;
        
        union {
            struct {
                int32_t AUTO_RECONNECTION_TIME : 32;//自動再接続有効時間
            } FEILDS;
            unsigned char byte[4];
        } BT38_BT39;
        
        union {
            struct {
                int32_t AUTO_DISCONNECTION_TIME : 32;//自動切断時間
            } FEILDS;
            unsigned char byte[4];
        } BT40_BT41;
        
        union {
            struct {
                int32_t TRIGGER_BUTTON_PUSHTIME_FOR_CONNECTION : 32;//トリガー接続長押し時間
            } FEILDS;
            unsigned char byte[4];
        } BT42_BT43;
        
        union {
            struct {
                int32_t TRIGGER_BUTTON_PUSHTIME_FOR_DISCONNECTION : 32;//トリガー切断長押し時間
            } FEILDS;
            unsigned char byte[4];
        } BT44_BT45;
        
        union {
            struct {
                int32_t ACK_NAK_WAIT_TIME : 32;//ACK/NAK待ち時間
            } FEILDS;
            unsigned char byte[4];
        } BT46_BT47;
        
        union {
            struct {
                Byte OUT_OF_RANGE_MEMORY_ENABLE : 8;//圏外メモリー
                
                Byte DATA_COLLECT_MODE_ENABLE : 8;//データコレクト
            } FEILDS;
            unsigned char byte[2];
        } BT48;
        
        union {
            struct {
                Byte AUTO_CONNECTION_ENABLE : 8;//バーコード読み取り自動接続
                
                Byte DISCONNECTION_BUZZER_FROM_DATA_COLLECTOR : 8;//データコレクターからの切断時ブザー
            } FEILDS;
            unsigned char byte[2];
        } BT49;
        
        union {
            struct {
                Byte DISCONNECTION_BUZZER_FROM_DESTINATION : 8;//接続相手からの切断ブザー
                
                Byte MEMORY_OUTPUT_METHOD : 8;//メモリーデータの出力方法
            } FEILDS;
            unsigned char byte[2];
        } BT50;
        
        union {
            unsigned char byte[2];
        } BT51;
        
        union {
            struct {
                int32_t USB_CONNECTION : 32;//USB接続時COM通信
            } FEILDS;
            unsigned char byte[4];
        } BT52_BT53;
        
        union {
            unsigned char byte[2];
        } BT54;
        
        union {
            unsigned char byte[2];
        } BT55;
        
        union {
            struct {
                Byte FUNCTION_BUTTON_INPUT : 8;//ファンクション押下時出力
                
                Byte : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT56;
        
        union {
            struct {
                short DEVICE_NAME : 16;//Bluetoothデバイス名
            } FEILDS;
            unsigned char byte[2];
        } BT57;
        
        union {
            unsigned char byte[2];
        } BT58;
        
        union {
            unsigned char byte[2];
        } BT59;
        
        union {
            unsigned char byte[2];
        } BT60;
        
        union {
            unsigned char byte[2];
        } BT61;
        
        union {
            unsigned char byte[2];
        } BT62;
        
        union {
            unsigned char byte[2];
        } BT63;
        
        union {
            unsigned char byte[2];
        } BT64;
        
        union {
            unsigned char byte[2];
        } BT65;
        
        union {
            unsigned char byte[2];
        } BT66;
        
        union {
            unsigned char byte[2];
        } BT67;
        
        union {
            struct {
                short DESTINATION_ADDRESS_LAST_TIME : 16;//前回のスレーブ接続時の相手アドレス
            } FEILDS;
            unsigned char byte[2];
        } BT68;
        
        union {
            unsigned char byte[2];
        } BT69;
        
        union {
            unsigned char byte[2];
        } BT70;
        
        union {
            unsigned char byte[2];
        } BT71;
        
        union {
            unsigned char byte[2];
        } BT72;
        
        union {
            unsigned char byte[2];
        } BT73;
        
        union {
            struct {
                Byte : 8;
                
                Byte GOOD_READ_VIBRATOR_ENABLE : 8;
            } FEILDS;
            unsigned char byte[2];
        } BT74;
        
        union {
            unsigned char byte[2];
        } BT75;
        
        union {
            struct {
                int32_t INTER_CHARACTER_DELAY : 32;//キャラクタ間ディレイ
            } FEILDS;
            unsigned char byte[4];
        } BT76_BT77;
        
        union {
            struct {
                Byte KEYBOARD_LANGUAGE : 8;//キーボード言語
                
                Byte USE_NUM_PAD : 8;//numPad使用
            } FEILDS;
            unsigned char byte[2];
        } BT78;
        
        union {
            struct {
                Byte NUM_PAD_MODE : 8;//numPadモード
                
                Byte CAPS_LOCK_MODE : 8;//CapsLockモード
            } FEILDS;
            unsigned char byte[2];
        } BT79;
    } BT;
    unsigned char byte[160];
} OPNSettingBT;



typedef union {
    struct {
        union {
            struct {
                int32_t UPCA_01 : 32;
                int32_t UPCA_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF0_3;
        
        union {
            struct {
                int32_t UPCA_ADDON_01 : 32;
                int32_t UPCA_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF4_7;
        
        union {
            struct {
                int32_t UPCE_01 : 32;
                int32_t UPCE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF8_11;
        
        union {
            struct {
                int32_t UPCE_ADDON_01 : 32;
                int32_t UPCE_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF12_15;
        
        union {
            unsigned char byte[8];
        } PF16_19;
        
        union {
            unsigned char byte[8];
        } PF20_23;
        
        union {
            struct {
                int32_t EAN13_01 : 32;
                int32_t EAN13_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF24_27;
        
        union {
            struct {
                int32_t EAN13_ADDON_01 : 32;
                int32_t EAN13_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF28_31;
        
        union {
            struct {
                int32_t EAN8_01 : 32;
                int32_t EAN8_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF32_35;
        
        union {
            struct {
                int32_t EAN8_ADDON_01 : 32;
                int32_t EAN8_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF36_39;
        
        union {
            struct {
                int32_t CODE39_01 : 32;
                int32_t CODE39_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF40_43;
        
        union {
            struct {
                int32_t TRI_OPTIC_01 : 32;
                int32_t TRI_OPTIC_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF44_47;
        
        union {
            struct {
                int32_t NW7_01 : 32;
                int32_t NW7_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF48_51;
        
        union {
            struct {
                int32_t D2OF5_01 : 32;
                int32_t D2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF52_55;
        
        union {
            struct {
                int32_t I2OF5_01 : 32;
                int32_t I2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF56_59;
        
        union {
            struct {
                int32_t SCODE_01 : 32;
                int32_t SCODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF60_63;
        
        union {
            struct {
                int32_t MATRIX2OF5_01 : 32;
                int32_t MATRIX2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF64_67;
        
        union {
            struct {
                int32_t CODE93_01 : 32;
                int32_t CODE93_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF68_71;
        
        union {
            struct {
                int32_t CODE128_01 : 32;
                int32_t CODE128_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF72_75;
        
        union {
            struct {
                int32_t MSIPLESSEY_01 : 32;
                int32_t MSIPLESSEY_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF76_79;
        
        union {
            struct {
                int32_t IATA_01 : 32;
                int32_t IATA_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF80_83;
        
        union {
            struct {
                int32_t UKPLESSEY_01 : 32;
                int32_t UKPLESSEY_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF84_87;
        
        union {
            struct {
                int32_t TELEPEN_01 : 32;
                int32_t TELEPEN_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF88_91;
        
        union {
            struct {
                int32_t RSS_01 : 32;
                int32_t RSS_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF92_95;
        
        union {
            unsigned char byte[8];
        } PF96_99;
        
        union {
            unsigned char byte[8];
        } PF100_103;
        
        union {
            struct {
                int32_t PDF417_01 : 32;
                int32_t PDF417_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF104_107;
        
        union {
            struct {
                int32_t MICRO_PDF417_01 : 32;
                int32_t MICRO_PDF417_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF108_111;
        
        union {
            struct {
                int32_t CODE11_01 : 32;
                int32_t CODE11_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF112_115;
        
        union {
            struct {
                int32_t CODE3OF5_01 : 32;
                int32_t CODE3OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF116_119;
        
        union {
            struct {
                int32_t EAN128_01 : 32;
                int32_t EAN128_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF120_123;
        
        union {
            unsigned char byte[8];
        } PF124_127;
        
        union {
            struct {
                int32_t COMMON_01 : 32;
                int32_t COMMON_02 : 32;
                int32_t COMMON_03 : 32;
                int32_t COMMON_04 : 32;
            } FEILDS;
            unsigned char byte[16];
        }PF128_135;
        
        
        //サフィックス
        
        union {
            struct {
                int32_t UPCA_01 : 32;
                int32_t UPCA_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF0_3;
        
        union {
            struct {
                int32_t UPCA_ADDON_01 : 32;
                int32_t UPCA_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF4_7;
        
        union {
            struct {
                int32_t UPCE_01 : 32;
                int32_t UPCE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF8_11;
        
        union {
            struct {
                int32_t UPCE_ADDON_01 : 32;
                int32_t UPCE_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF12_15;
        
        union {
            unsigned char byte[8];
        }SF16_19;
        
        union {
            unsigned char byte[8];
        }SF20_23;
        
        union {
            struct {
                int32_t EAN13_01 : 32;
                int32_t EAN13_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF24_27;
        
        union {
            struct {
                int32_t EAN13_ADDON_01 : 32;
                int32_t EAN13_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF28_31;
        
        union {
            struct {
                int32_t EAN8_01 : 32;
                int32_t EAN8_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF32_35;
        
        union {
            struct {
                int32_t EAN8_ADDON_01 : 32;
                int32_t EAN8_ADDON_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF36_39;
        
        union {
            struct {
                int32_t CODE39_01 : 32;
                int32_t CODE39_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF40_43;
        
        union {
            struct {
                int32_t TRI_OPTIC_01 : 32;
                int32_t TRI_OPTIC_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF44_47;
        
        union {
            struct {
                int32_t NW7_01 : 32;
                int32_t NW7_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF48_51;
        
        union {
            struct {
                int32_t D2OF5_01 : 32;
                int32_t D2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF52_55;
        
        union {
            struct {
                int32_t I2OF5_01 : 32;
                int32_t I2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF56_59;
        
        union {
            struct {
                int32_t SCODE_01 : 32;
                int32_t SCODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF60_63;
        
        union {
            struct {
                int32_t MATRIX2OF5_01 : 32;
                int32_t MATRIX2OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF64_67;
        
        union {
            struct {
                int32_t CODE93_01 : 32;
                int32_t CODE93_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF68_71;
        
        union {
            struct {
                int32_t CODE128_01 : 32;
                int32_t CODE128_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF72_75;
        
        union {
            struct {
                int32_t MSIPLESSEY_01 : 32;
                int32_t MSIPLESSEY_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF76_79;
        
        union {
            struct {
                int32_t IATA_01 : 32;
                int32_t IATA_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF80_83;
        
        union {
            struct {
                int32_t UKPLESSEY_01 : 32;
                int32_t UKPLESSEY_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF84_87;
        
        union {
            struct {
                int32_t TELEPEN_01 : 32;
                int32_t TELEPEN_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF88_91;
        
        union {
            struct {
                int32_t RSS_01 : 32;
                int32_t RSS_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF92_95;
        
        union {
            unsigned char byte[8];
        }SF96_99;
        
        union {
            unsigned char byte[8];
        }SF100_103;
        
        union {
            struct {
                int32_t PDF417_01 : 32;
                int32_t PDF417_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF104_107;
        
        union {
            struct {
                int32_t MICRO_PDF417_01 : 32;
                int32_t MICRO_PDF417_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF108_111;
        
        union {
            struct {
                int32_t CODE11_01 : 32;
                int32_t CODE11_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF112_115;
        
        union {
            struct {
                int32_t CODE3OF5_01 : 32;
                int32_t CODE3OF5_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF116_119;
        
        union {
            struct {
                int32_t EAN128_01 : 32;
                int32_t EAN128_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF120_123;
        
        union {
            unsigned char byte[8];
        }SF124_127;
        
        union {
            struct {
                int32_t COMMON_01 : 32;
                int32_t COMMON_02 : 32;
                int32_t COMMON_03 : 32;
                int32_t COMMON_04 : 32;
            } FEILDS;
            unsigned char byte[16];
        }SF128_135;
        
    } PrefixSuffix;
    unsigned char byte[544];
      
} OPNSettingPrefixSuffix;

typedef union {
    struct {
        union {
            struct {
                int32_t INTELLIGENT_MAIL_01 : 32;
                int32_t INTELLIGENT_MAIL_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF136_139;
        
        union {
            struct {
                int32_t POSTNET_01 : 32;
                int32_t POSTNET_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF140_143;
        
        union {
            struct {
                int32_t JAPANESE_POSTAL_01 : 32;
                int32_t JAPANESE_POSTAL_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF144_147;
        
        union {
            struct {
                int32_t CODABLOCK_F_01 : 32;
                int32_t CODABLOCK_F_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF148_151;
        
        union {
            struct {
                int32_t DATA_MATRIX_ECC200_ECC000_140_01 : 32;
                int32_t DATA_MATRIX_ECC200_ECC000_140_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF152_155;
        
        union {
            struct {
                int32_t AZTEC_CODE_RUNES_01 : 32;
                int32_t AZTEC_CODE_RUNES_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF156_159;
        
        union {
            struct {
                int32_t CHINESE_SENSIBLE_CODE_01 : 32;
                int32_t CHINESE_SENSIBLE_CODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF160_163;
        
        union {
            struct {
                int32_t QR_MICRO_QR_01 : 32;
                int32_t QR_MICRO_QR_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF164_167;
        
        union {
            struct {
                int32_t MAXI_CODE_01 : 32;
                int32_t MAXI_CODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF168_171;
        
        union {
            struct {
                int32_t COMPOSITE_ON_GS1DATABAR_ON_UPC_EAN_01 : 32;
                int32_t COMPOSITE_ON_GS1DATABAR_ON_UPC_EAN_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }PF172_175;
        
        
        union {
            struct {
                int32_t INTELLIGENT_MAIL_01 : 32;
                int32_t INTELLIGENT_MAIL_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF136_139;
        
        union {
            struct {
                int32_t POSTNET_01 : 32;
                int32_t POSTNET_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF140_143;
        
        union {
            struct {
                int32_t JAPANESE_POSTAL_01 : 32;
                int32_t JAPANESE_POSTAL_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF144_147;
        
        union {
            struct {
                int32_t CODABLOCK_F_01 : 32;
                int32_t CODABLOCK_F_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF148_151;
        
        union {
            struct {
                int32_t DATA_MATRIX_ECC200_ECC000_140_01 : 32;
                int32_t DATA_MATRIX_ECC200_ECC000_140_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF152_155;
        
        union {
            struct {
                int32_t AZTEC_CODE_RUNES_01 : 32;
                int32_t AZTEC_CODE_RUNES_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF156_159;
        
        union {
            struct {
                int32_t CHINESE_SENSIBLE_CODE_01 : 32;
                int32_t CHINESE_SENSIBLE_CODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF160_163;
        
        union {
            struct {
                int32_t QR_MICRO_QR_01 : 32;
                int32_t QR_MICRO_QR_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF164_167;
        
        union {
            struct {
                int32_t MAXI_CODE_01 : 32;
                int32_t MAXI_CODE_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF168_171;
        
        union {
            struct {
                int32_t COMPOSITE_ON_GS1DATABAR_ON_UPC_EAN_01 : 32;
                int32_t COMPOSITE_ON_GS1DATABAR_ON_UPC_EAN_02 : 32;
            } FEILDS;
            unsigned char byte[8];
        }SF172_175;
        
    } PrefixSuffix;
    unsigned char byte[160];
    
} OPNSettingPrefixSuffix2;


/**
 * 設定値を転送時の形式から内部保管時の形式に変換する機能を追加します。
 */
@interface NSData(OPNSettingFormat)
/**
 * 設定値を転送時の形式から内部保管時の形式に変換します。<br>
 * @return 内部保管データ<br>
 */
- (NSData *)dataSettings;

@end

