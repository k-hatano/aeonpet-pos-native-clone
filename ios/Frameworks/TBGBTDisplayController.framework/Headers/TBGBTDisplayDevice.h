//
//  TBGBTDisplayDevice.h
//  TBGBTDisplayController
//
//
//  Copyright © 2015 TB GROUP. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 表示デバイスの情報を保持するためのプロトコルです。
 */
@protocol TBGBTDisplayDevice <NSObject>

/// デバイスの接続ID
@property (nonatomic, readonly) NSUInteger connectionId;
/// デバイスの製造者情報
@property (nonatomic, readonly, nonnull) NSString* manufacturer;
/// デバイスの名称
@property (nonatomic, readonly, nonnull) NSString* name;
/// デバイスのモデル番号
@property (nonatomic, readonly, nonnull) NSString* modelNumber;
/// デバイスのシリアル番号
@property (nonatomic, readonly, nonnull) NSString* serialNumber;
/// デバイスのファームウェア改訂番号
@property (nonatomic, readonly, nonnull) NSString* firmwareRevision;
/// デバイスのハードウェア改訂番号
@property (nonatomic, readonly, nonnull) NSString* hardwareRevision;
/// デバイスの接続プロトコル情報
@property (nonatomic, readonly, nonnull) NSString* protocolString;

@end

/// Define a type of "NSObject <TBGBTDisplayDevice>*".
typedef NSObject <TBGBTDisplayDevice>* TBGBTDisplayDeviceT;
