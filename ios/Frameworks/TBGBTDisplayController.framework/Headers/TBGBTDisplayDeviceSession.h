//
//  TBGBTDisplayDeviceSession.h
//  TBGBTDisplayController
//
//
//  Copyright © 2015 TB GROUP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TBGBTDisplayController/TBGBTDisplayDevice.h>

/// 表示方式
typedef NS_ENUM(NSInteger, TBGBTDisplayType) {
    TBGBTDisplayTypeUnknown, ///< 不明のタイプ
    TBGBTDisplayTypeVfd, ///< VFDタイプ
    TBGBTDisplayTypeLcd, ///< LCDタイプ
};

/// カーソルモード
typedef NS_ENUM(NSInteger, TBGBTDisplayCursorMode) {
    TBGBTDisplayCursorModeOff = 0, ///< 非表示
    TBGBTDisplayCursorModeBlink = 1, ///< 点滅表示
    TBGBTDisplayCursorModeOn = 2, ///< 表示
};

/**
 * デバイスの制御を行うためのプロトコルです。
 */
@protocol TBGBTDisplayDeviceSession <NSObject>

/// デバイス情報
@property (nonatomic, readonly, nonnull) TBGBTDisplayDeviceT device;
/// 表示方式(TBGBTDisplayType)
@property (nonatomic, readonly) TBGBTDisplayType displayType;

/// 最後に発生したエラー
@property (nonatomic, readonly, nullable) NSError* lastError;
/// エラー発生時の通知を受け取るためのblock
@property (nonatomic, copy, nullable) void (^onError)(NSError* _Nonnull);

/// モジュール情報
@property (nonatomic, readonly, nullable) NSString* moduleInformation;

/**
 * セッションを開始します。
 *
 * @return セッションの開始に失敗した場合にはNOを返します。
 */
- (BOOL)open;

/**
 * セッションを終了します。
 */
- (void)close;

/**
 * ディスプレイのOn/Off状態を変更します。
 *
 * @param on On: YES, Off: NO
 */
- (void)setDisplay:(BOOL)on;

/**
 * カーソルモードを設定します。
 *
 * @param mode カーソルモード(TBGBTDisplayCursorMode)
 */
- (void)setCursorMode:(TBGBTDisplayCursorMode)mode;

/**
 * カーソルをホームポジション(左上)に移動します。
 */
- (void)moveCursorToHome;

/**
 * カーソルを指定位置(X,Y)に移動します。
 *
 * @param x X(桁)位置 (0~19)
 * @param y Y(行)位置 (0~1)
 *
 * @exception NSException 範囲外の値が指定されると発生します。
 */
- (void)moveCursorToX:(uint8_t)x andY:(uint8_t)y;

/**
 * カーソルを1文字左に移動します。
 *
 * カーソルが左端にある場合は、上の行の右端に移動します。
 * 最上行の左端にある場合は、最下行の右端に移動します。
 */
- (void)moveCursorToLeft;

/**
 * カーソルを1文字右に移動します。
 *
 * カーソルが右端にある場合は、下の行の左端に移動します。
 * 最下行の右端にある場合は、最上行の左端に移動します。
 */
- (void)moveCursorToRight;

/**
 * カーソルを1行下に移動します。
 *
 * カーソルが最下行にある場合は、最上行に移動します。
 * 桁位置は変化しません。
 */
- (void)moveCursorDown;

/**
 * カーソルを左端に移動します。
 */
- (void)moveCursorToBeginningOfLine;

/**
 * 全表示内容を消去します。
 *
 * カーソルはホームポジション(左上)に移動します。
 */
- (void)clearScreen;

/**
 * カーソル行の表示内容を消去します。
 *
 * カーソルは左端に移動します。
 */
- (void)clearLine;

/**
 * 国コードを設定しフォントテーブルを選択します。
 *
 * 電源投入時の初期設定は、00h(USA)、30h(PC437)です。
 * 詳細は別途資料を参照して下さい。
 *
 * @param code 国コード
 * <pre>
 * 00h～0Dh： 文字コード範囲 20h-7Fh<br>
 * 30h～3Dh： 文字コード範囲 80h-FFh
 * </pre>
 */
- (void)setCountryCode:(uint8_t)code;

/**
 * カーソル位置に文字列を表示します。
 *
 * 表示文字列はShift_JISコードに変換した上でデバイスへ送信されます。
 * 文字列によってはShift_JISへのコード変換に失敗して文字化け等を引き起こす場合があります。
 * その場合はコード変換を行わない writeText:length: を使用して下さい。
 *
 * @param text 表示文字列
 */
- (void)writeText:(nonnull NSString*)text;

/**
 * カーソル位置に文字列を表示します。
 *
 * 表示文字列はコード変換なしでそのままデバイスへ送信されます。
 *
 * @param text   表示文字列
 * @param length 表示文字列の長さ
 */
- (void)writeText:(nonnull const char*)text length:(NSUInteger)length;

/**
 * 1byte文字の外字を定義します。
 *
 * 定義した外字はその文字コードにより表示されます。
 *
 * 登録できる最大の文字数は32文字となります。
 *
 * 指定されたフォント番号に既に外字が登録されている場合は上書きされます。
 *
 * 外字フォントデータはRAM上に確保されるため、電源遮断により消去されます。
 *
 * 詳細は別途資料を参照して下さい。
 *
 * @param fontNumber    フォント番号 (0~31)
 * @param characterCode 文字コード (20h~7Fh)
 * @param fontData      フォントデータ (16bytes)
 */
- (void)defineOneByteUserCharacterWithFontNumber:(uint8_t)fontNumber characterCode:(uint8_t)characterCode fontData:(nonnull NSData*)fontData;

/**
 * 2bytes文字の外字を定義します。
 *
 * 定義した外字はその文字コードにより表示されます。
 *
 * 登録できる最大の文字数は16文字となります。
 *
 * 指定されたフォント番号に既に外字が登録されている場合は上書きされます。
 *
 * 外字フォントデータはRAM上に確保されるため、電源遮断により消去されます。
 *
 * 詳細は別途資料を参照して下さい。
 *
 * @param fontNumber    フォント番号 (0~15)
 * @param characterCode 文字コード (8000h~FFFFh)
 * @param fontData      フォントデータ (32bytes)
 */
- (void)defineTwoBytesUserCharacterWithFontNumber:(uint8_t)fontNumber characterCode:(uint16_t)characterCode fontData:(nonnull NSData*)fontData;

/**
 * 1byte文字の外字を解除します。
 *
 * @param fontNumber    フォント番号 (0~31)
 */
- (void)undefineOneByteUserCharacterWithFontNumber:(uint8_t)fontNumber;

/**
 * 2bytes文字の外字を解除します。
 *
 * @param fontNumber    フォント番号 (0~15)
 */
- (void)undefineTwoBytesUserCharacterWithFontNumber:(uint8_t)fontNumber;

/**
 * LCDのコントラストを設定します。
 *
 * LCDタイプのデバイス専用のコマンドです。
 *
 * @param contrast コントラスト値 (0~6)
 * <pre>
 * 0: -3<br>
 * 1: -2<br>
 * 2: -1<br>
 * 3: 0 (default)<br>
 * 4: +1<br>
 * 5: +2<br>
 * 6: +3
 * </pre>
 *
 * @exception NSException 範囲外の値が指定されると発生します。
 * @exception NSException LCDタイプでないデバイスに対して呼び出されると発生します。
 */
- (void)setLcdContrast:(uint8_t)contrast;

/**
 * VFDの輝度を設定します。
 *
 * VFDタイプのデバイス専用のコマンドです。
 *
 * @param brightness 輝度値 (0~3)
 * <pre>
 * 0: 20%<br>
 * 1: 40%<br>
 * 2: 60%<br>
 * 3: 100% (default)
 * </pre>
 *
 * @exception NSException 範囲外の値が指定されると発生します。
 * @exception NSException VFDタイプでないデバイスに対して呼び出されると発生します。
 */
- (void)setVfdBrightness:(uint8_t)brightness;

/**
 * イメージデータを表示します。
 *
 * カーソル位置は変化しません。
 *
 * 詳細は別途資料を参照して下さい。
 *
 * @param imageData イメージデータ (800bytes)
 */
- (void)writeImage:(nonnull NSData*)imageData;

/**
 * 直ちにデバイスへコマンドを送信します。
 *
 * デバイスへのコマンド送信はiOSが提供するメインスレッドのRun Loopにて処理されるため、ユーザコード実行中には送信されません。
 * このため、ユーザコード内のループ処理等で連続してコマンドを実行する際には、これを呼び出すことによって逐次送信させることができます。
 */
- (void)flush;

@end

/// Define a type of "NSObject <TBGBTDisplayDeviceSession>*".
typedef NSObject <TBGBTDisplayDeviceSession>* TBGBTDisplayDeviceSessionT;
