//
//  TBGBTDisplayController.h
//  TBGBTDisplayController
//
//
//  Copyright © 2015 TB GROUP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TBGBTDisplayController/TBGBTDisplayDevice.h>
#import <TBGBTDisplayController/TBGBTDisplayDeviceSession.h>
#import <TBGBTDisplayController/TBGBTDisplayErrorCodes.h>

/**
 * 表示デバイスを操作するためのクラスです。
 *
 * 共有インスタンスを使ってアクセスして下さい。
 *
 * <pre><code>
 * TBGBTDisplayController *obj = [TBGBTDisplayController shared];
 * </code></pre>
 */
@interface TBGBTDisplayController : NSObject

/// デバイスの接続通知を受け取るためのblock
@property (nonatomic, copy, nullable) void (^deviceDidConnect)(TBGBTDisplayDeviceT _Nonnull device);
/// デバイスの切断通知を受け取るためのblock
@property (nonatomic, copy, nullable) void (^deviceDidDisconnect)(TBGBTDisplayDeviceT _Nonnull device);

/// 共有インスタンスを取得します。
+ (nonnull instancetype)shared;

/**
 * 接続されているデバイス情報の配列を取得します。
 *
 * @return デバイス情報の配列を返します。
 * デバイスが見つからない場合には空の配列を返します。
 */
- (nonnull NSArray<TBGBTDisplayDeviceT>*)getDevices;

/**
 * デバイスとのセッションを作成します。
 *
 * @param device getDevices で取得したデバイス情報
 * @param error  エラー情報
 *
 * @return セッション操作のためのオブジェクトを返します。
 * セッションの作成に失敗した場合にはnilを返します。
 */
- (nullable TBGBTDisplayDeviceSessionT)createSessionWithDevice:(nonnull TBGBTDisplayDeviceT)device error:(NSError* _Nullable * _Nullable)error;

@end

/// Project version number for TBGBTDisplayController.
FOUNDATION_EXPORT double TBGBTDisplayControllerVersionNumber;

/// Project version string for TBGBTDisplayController.
FOUNDATION_EXPORT const unsigned char TBGBTDisplayControllerVersionString[];
