//
//  TBGBTDisplayErrorCodes.h
//  TBGBTDisplayController
//
//
//  Copyright © 2015 TB GROUP. All rights reserved.
//

#import <Foundation/Foundation.h>

/// エラーコード
typedef NS_ENUM(NSInteger, TBGBTDisplayErrorCode) {
    TBGBTDisplayErrorCodeNone = 0, ///< エラーなし
    TBGBTDisplayErrorCodeAccessoryError, ///< アクセサリ関連エラー
    TBGBTDisplayErrorCodeSessionCreateError, ///< セッション開始エラー
    TBGBTDisplayErrorCodeSessionError, ///< セッション関連エラー
    TBGBTDisplayErrorCodeInternalError, ///< 内部エラー
};
