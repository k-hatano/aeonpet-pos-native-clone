import Foundation

protocol PrinterProtocol {
  var printerProperty: PrinterProperty { get set }
  
  init (property: PrinterProperty)
  
  func findTargetPrinter() -> PrinterResultCode
  
  func startCreateReceipt() -> PrinterResultCode
  func endCreateReceiptOut(completion: ((PrinterResultCode) -> Void)?)
  
  func setReceiptOption(option: ReceiptOption);
  func addText(receiptText: ReceiptTypeText) -> PrinterResultCode
  func addCombineText(receiptCombine: ReceiptTypeCombine) -> PrinterResultCode
  func addImage(receiptImage: ReceiptTypeImage) -> PrinterResultCode
  func feed(receiptFeed: ReceiptTypeFeed) -> PrinterResultCode
  func cut() -> PrinterResultCode
  func barcode(receiptBarcode: ReceiptTypeBarcode) -> PrinterResultCode
  func openDrawer() -> PrinterResultCode
  func setLineSpace(linespc: Int)
}

extension PrinterProtocol {
  func combineLeftRight(leftText: String, rightText: String, width: Int) -> String {
    var space:String = ""
    let leftLength:Int = leftText.lengthOfBytes(using: String.Encoding.shiftJIS) * width
    let rightLength:Int = rightText.lengthOfBytes(using: String.Encoding.shiftJIS) * width
    let spaceSize:Int = printerProperty.receiptLineLength - leftLength - rightLength
    while (spaceSize > space.characters.count * width) {
      space += " "
    }
    return leftText + space + rightText
  }
}
