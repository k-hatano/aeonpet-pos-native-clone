import Foundation

enum PrinterResultCode: Int {
  case SUCCESS = 0
  case ERR_PARAM
  case ERR_CONNECT
  case ERR_TIMEOUT
  case ERR_MEMORY
  case ERR_ILLEGAL
  case ERR_PROCESSING
  case ERR_NOT_FOUND
  case ERR_IN_USE
  case ERR_TYPE_INVALID
  case ERR_DISCONNECT
  case ERR_ALREADY_OPENED
  case ERR_ALREADY_USED
  case ERR_BOX_COUNT_OVER
  case ERR_BOX_CLIENT_OVER
  case ERR_UNSUPPORTED
  case ERR_CANCEL
  case ERR_ALREADY_CONNECT
  case ERR_ILLEGAL_DEVICE
  case ERR_CODE_BEHAVIOR
  case ERR_DEVICE_UNPRINTABLE
  case ERR_IOS_VERSION
  case ERR_IMAGE_SIZE
  case ERR_IMAGE_PATH_NOT_FOUND
  case ERR_HEAD_HEAT_ERROR
  case ERR_HEAD_VP_ERR
  case ERR_PAPER_NOT_FOUND
  case ERR_BLE
  case ERR_PRINTER_DISCONNECTED
  case ERR_PRINTER_AUTOCUTTER
  case ERR_PAPER_NEAR_END
  case ERR_PAPER_EMPTY
  case ERR_UNKNOWN = 255
}

class PrinterResult {
  
  static let PRINTER_RESULT = "PRINTER_RESULT"
  
  class func isSuccess(result: PrinterResultCode) -> Bool {
      return result == PrinterResultCode.SUCCESS
  }
  class func printerResultToCallbackObj(result: PrinterResultCode) -> [String: Int] {
    return [PRINTER_RESULT: result.rawValue]
  }
}
