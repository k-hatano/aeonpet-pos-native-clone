//
//  PrinterBridge.m
//  pos_native
//
//  Created by hiroki.sasabuchi on 2017/05/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(PrinterBridge, RCTEventEmitter<RCTBridgeModule>)

RCT_EXTERN_METHOD(setPrinterProperty:(NSDictionary *)printerPropertyDic)
RCT_EXTERN_METHOD(printReceipt:(NSString *)jsonString openDrawer:(BOOL)openDrawer callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(disconnectCasher:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(connectCasher:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(readCashCount:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(depositCasher:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(pauseDepositCasher:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(restartDepositCasher:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(endDepositCasher:(int)type callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(dispenseCasher:(NSString *)jsonString callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(dispenseChange:(int)changeValue callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(readCasherStatus:(RCTResponseSenderBlock)callback)

@end
