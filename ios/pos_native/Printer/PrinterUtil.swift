//
//  PrinterUtil.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 2019/06/20.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

public final class PrinterUtil {
  
  static func convertErrorStatus(epsonError: Int32) -> PrinterResultCode {
    var error: PrinterResultCode
    switch (epsonError) {
    case EPOS2_SUCCESS.rawValue:
      error = PrinterResultCode.SUCCESS
    case EPOS2_ERR_PARAM.rawValue:
      error = PrinterResultCode.ERR_PARAM
    case EPOS2_ERR_CONNECT.rawValue:
      error = PrinterResultCode.ERR_CONNECT
    case EPOS2_ERR_TIMEOUT.rawValue:
      error = PrinterResultCode.ERR_TIMEOUT
    case EPOS2_ERR_MEMORY.rawValue:
      error = PrinterResultCode.ERR_MEMORY
    case EPOS2_ERR_ILLEGAL.rawValue:
      error = PrinterResultCode.ERR_ILLEGAL
    case EPOS2_ERR_PROCESSING.rawValue:
      error = PrinterResultCode.ERR_PROCESSING
    case EPOS2_ERR_NOT_FOUND.rawValue:
      error = PrinterResultCode.ERR_NOT_FOUND
    case EPOS2_ERR_IN_USE.rawValue:
      error = PrinterResultCode.ERR_IN_USE
    case EPOS2_ERR_TYPE_INVALID.rawValue:
      error = PrinterResultCode.ERR_TYPE_INVALID
    case EPOS2_ERR_DISCONNECT.rawValue:
      error = PrinterResultCode.ERR_DISCONNECT
    case EPOS2_ERR_ALREADY_OPENED.rawValue:
      error = PrinterResultCode.ERR_ALREADY_OPENED
    case EPOS2_ERR_ALREADY_USED.rawValue:
      error = PrinterResultCode.ERR_ALREADY_USED
    case EPOS2_ERR_BOX_COUNT_OVER.rawValue:
      error = PrinterResultCode.ERR_BOX_COUNT_OVER
    case EPOS2_ERR_BOX_CLIENT_OVER.rawValue:
      error = PrinterResultCode.ERR_BOX_CLIENT_OVER
    case EPOS2_ERR_UNSUPPORTED.rawValue:
      error = PrinterResultCode.ERR_UNSUPPORTED
    case EPOS2_ERR_FAILURE.rawValue:
      error = PrinterResultCode.ERR_UNKNOWN
    default:
      error = PrinterResultCode.ERR_UNKNOWN
    }
    return error
  }
  
  static func convertBluetoothError(btError: Int32) -> PrinterResultCode {
    var error: PrinterResultCode
    switch (btError) {
    case EPOS2_BT_SUCCESS.rawValue:
      error = PrinterResultCode.SUCCESS
    case EPOS2_BT_ERR_PARAM.rawValue:
      error = PrinterResultCode.ERR_PARAM
    case EPOS2_BT_ERR_UNSUPPORTED.rawValue:
      error = PrinterResultCode.ERR_UNSUPPORTED
    case EPOS2_BT_ERR_CANCEL.rawValue:
      error = PrinterResultCode.ERR_CANCEL
    case EPOS2_BT_ERR_ALREADY_CONNECT.rawValue:
      error = PrinterResultCode.ERR_ALREADY_CONNECT
    case EPOS2_BT_ERR_ILLEGAL_DEVICE.rawValue:
      error = PrinterResultCode.ERR_ILLEGAL_DEVICE
    case EPOS2_BT_ERR_FAILURE.rawValue:
      error = PrinterResultCode.ERR_UNKNOWN
    default:
      error = PrinterResultCode.ERR_UNKNOWN
    }
    return error
  }
}
