import Foundation

class ReceiptOption {
  var checkErrorStrictly: Bool = false
  
  init(input: Dictionary<String, Any>) {
    if (input["check_error_strictly"] != nil) {
      checkErrorStrictly = input["check_error_strictly"] as! Bool
    }
  }
}
