

import Foundation
import MediaPlayer

@objc(PrinterBridge)
class PrinterBridge: RCTEventEmitter {
  
  var printerProtocol: PrinterProtocol?
  var printerResultCode: PrinterResultCode?
  var cashChangerProtocol: CashChangerProtocol?
 
  // MARK: - PrinterBridge
  @objc(setPrinterProperty:)
  func setPrinterProperty(printerPropertyDic: NSDictionary) {
    var _printerPropertyDic = printerPropertyDic as! [String : Any]
    _printerPropertyDic["BRIDGE"] = self
    let printerProperty = PrinterProperty(dict: _printerPropertyDic)
    
    switch printerProperty.printerSdk {
      case PrinterSDK.EPSON:
          printerProtocol = PrinterEpson(property: printerProperty)
          cashChangerProtocol = CashChanger.sharedInstance
          cashChangerProtocol?.setProperty(property: printerProperty)
      default: break
    }
  }
  
  @objc(printReceipt:openDrawer:callback:)
  func printReceipt(jsonString: NSString, openDrawer: Bool, callback: @escaping RCTResponseSenderBlock) {
    
    var result = printerProtocol?.startCreateReceipt()
    if !PrinterResult.isSuccess(result: result!) {
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
      return
    }

    result = printReceiptFromJson(jsonString: jsonString as String, openDrawer: openDrawer)
    if !PrinterResult.isSuccess(result: result!) {
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
      return

    }
    printerProtocol?.endCreateReceiptOut(completion: { (result) -> Void in
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result)])
    })
  }
  
  // MARK: - helper
  func printReceiptFromJson(jsonString: String, openDrawer: Bool) -> PrinterResultCode {
    let jsonData: Data = jsonString.data(using: String.Encoding.utf8)!
    do {
      let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
      let jsonArray = json as! [String : Any]
      let contents = jsonArray[ReceiptKey.CONTENT.rawValue] as! Array<Dictionary<String, NSObject>>
      
      let option = ReceiptOption(input: jsonArray)
      printerProtocol?.setReceiptOption(option: option) 
      var drawerFlag = true

      for content in contents {
        adjustLineSpace(text: content["text"] as? String)
        var result: PrinterResultCode = PrinterResultCode.SUCCESS
        if(drawerFlag && openDrawer) {
          result = (printerProtocol?.openDrawer())!
          drawerFlag = false
        }
        switch (content[ReceiptKey.ELEMENT.rawValue] as! String) {
        case ReceiptElement.TEXT.rawValue:
          let receiptText = ReceiptTypeText(dict: content)
          result = (printerProtocol?.addText(receiptText: receiptText))!
        case ReceiptElement.COMBINE.rawValue:
          let receiptCombine = ReceiptTypeCombine(dict: content)
          result = (printerProtocol?.addCombineText(receiptCombine: receiptCombine))!
        case ReceiptElement.IMAGE.rawValue:
          let receiptImage = ReceiptTypeImage(dict: content)
          result = (printerProtocol?.addImage(receiptImage: receiptImage))!
        case ReceiptElement.CUT.rawValue:
          result = (printerProtocol?.cut())!
        case ReceiptElement.FEED.rawValue:
          let receiptFeed = ReceiptTypeFeed(dict: content)
          result = (printerProtocol?.feed(receiptFeed: receiptFeed))!
        case ReceiptElement.BARCODE.rawValue:
          let receiptBarcode = ReceiptTypeBarcode(dict: content)
          result = (printerProtocol?.barcode(receiptBarcode: receiptBarcode))!
        default:
          break
        }
        if !PrinterResult.isSuccess(result: result) {
          return result
        }
      }
      return PrinterResultCode.SUCCESS
    } catch {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }
  }
  
  private func adjustLineSpace(text: String?) {
    let narrowLineSpace = 24
    let defaultLineSpace = 30
    if let text = text, (text.contains("┃") || text.contains("━")) {
      printerProtocol?.setLineSpace(linespc: narrowLineSpace)
    } else {
      printerProtocol?.setLineSpace(linespc: defaultLineSpace)
    }
  }
  
  @objc(disconnectCasher:)
  func disconnectCasher(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.disconnectCasher()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(connectCasher:)
  func connectCasher(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.connectCasher()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(readCashCount:)
  func readCashCount(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.readCashCount()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(depositCasher:)
  func depositCasher(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.depositCasher()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(pauseDepositCasher:)
  func pauseDepositCasher(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.pauseDepositCasher()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(restartDepositCasher:)
  func restartDepositCasher(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.restartDepositCasher()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(endDepositCasher:callback:)
  func endDepositCasher(type: Int, callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.endDepositCasher(type: type)
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  @objc(dispenseCasher:callback:)
  func dispenseCasher(jsonString: NSString, callback: @escaping RCTResponseSenderBlock) {
    let jsonStringified = jsonString as String
    let jsonData: Data = jsonStringified.data(using: String.Encoding.utf8)!
    do{
      let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
      let jsonArray = json as! [NSString : Int32]
      
      let result = cashChangerProtocol?.dispenseCasher(data:jsonArray)
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
    } catch {
      callback([NSNull(), PrinterResultCode.ERR_CODE_BEHAVIOR])
      return
    }
  }
  
  @objc(dispenseChange:callback:)
  func dispenseChange(changeValue: Int, callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.dispenseChange(changeValue: changeValue)
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
  
  override func supportedEvents() -> [String]! {
    return [
      "Error",
      "PrintCompleted",
      "CashCountComplete",
      "SendCashChangerError",
      "SendDepositAmountUpdate",
      "SendDispenseComplete",
      "SendCashChangerStatus"
    ]
  }
  
  @objc(sendError:) 
  func sendError(result: Int) {
    sendEvent(withName: "Error", body: PrinterResult.printerResultToCallbackObj(result: PrinterResultCode(rawValue: result)!))
  }
  
  @objc(sendPrintCompleted:)
  func sendPrintCompleted(result: Bool) {
    sendEvent(withName: "PrintCompleted", body: result)
  }

  @objc(cashCountComplete:)
  func cashCountComplete(result: [NSString:Int32]) {
    sendEvent(withName: "CashCountComplete", body: result)
  }
  
  @objc(sendCashChangerError:)
  func sendCashChangerError(result: Int32) {
    sendEvent(withName: "SendCashChangerError", body: result)
  }

  @objc(sendDepositAmountUpdate:)
  func sendDepositAmountUpdate(result: Int) {
    sendEvent(withName: "SendDepositAmountUpdate", body: result)
  }
  
  @objc(sendDispenseComplete:)
  func sendDispenseComplete(result: Int32) {
    sendEvent(withName: "SendDispenseComplete", body: result)
  }

  @objc(sendCashChangerStatus:)
  func sendCashChangerStatus(result: String) {
    sendEvent(withName: "SendCashChangerStatus", body: result)
  }
  
  @objc(readCasherStatus:)
  func readCasherStatus(callback: @escaping RCTResponseSenderBlock) {
    
    let result = cashChangerProtocol?.readCasherStatus()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
}
