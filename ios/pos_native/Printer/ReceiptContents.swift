//
//  ReceiptContents.swift
//  pos_native
//
//  Created by hiroki.sasabuchi on 2017/05/30.
//  Copyright © 2017 Facebook. All rights reserved.
//

import Foundation

enum ReceiptKey : String {
  case CONTENT = "content"
  case ELEMENT = "element"
}

enum ReceiptElement : String {
  case TEXT    = "text"
  case COMBINE = "combine_left_right"
  case IMAGE   = "image"
  case CUT     = "cut"
  case FEED    = "feed"
  case BARCODE = "barcode"
}

enum ReceiptBarcode: Int {
  case CODE39 = 1
}

enum ReceiptAlign: String {
  case CENTER = "center"
  case LEFT   = "left"
  case RIGHT  = "right"
}

class ReceiptTypeText {
  
  let _TEXT      = "text"
  let _ALIGN     = "align"
  let _WIDTH     = "width"
  let _HEIGHT    = "height"
  let _LINE_SPACING = "lineSpacing"
  let _TAG       = "tag"
  let _BOLD      = "bold"
  let _UNDERLINE = "underline"

  var text: String = ""
  var align: ReceiptAlign?
  var width: Int = 1
  var height: Int = 1
  var lineSpacing: Int = 0
  var tag: String?
  var bold: Bool = false
  var underline: Bool = false
  var dict__: Dictionary<String, Any>
  
  init(dict: Dictionary<String, Any>) {
    dict__ = dict
    text = dict[_TEXT] as? String ?? ""
    align = ReceiptAlign(rawValue: dict[_ALIGN] as! String)
    width = dict[_WIDTH] as? Int ?? 1
    height = dict[_HEIGHT] as? Int ?? 1
    lineSpacing = dict[_LINE_SPACING] as? Int ?? 0
    if dict[_TAG] != nil {
      tag = dict[_TAG] as? String
    }
    bold = (dict[_BOLD] != nil && dict[_BOLD] as! Bool == true)
    underline = (dict[_UNDERLINE] != nil && dict[_UNDERLINE] as! Bool == true)
  }
}

class ReceiptTypeCombine {
  
  let _LEFT      = "left"
  let _RIGHT     = "right"
  let _WIDTH     = "width"
  let _HEIGHT    = "height"
  let _LINE_SPACING = "lineSpacing"
  let _TAG       = "tag"
  let _BOLD      = "bold"
  let _UNDERLINE = "underline"
  
  var right: String = ""
  var left: String = ""
  var width: Int = 1
  var height: Int = 1
  var lineSpacing: Int = 0
  var tag: String?
  var bold: Bool = false
  var underline: Bool = false
  var dict__: Dictionary<String, NSObject>
  
  init(dict: Dictionary<String, NSObject>) {
    dict__ = dict
    left = dict[_LEFT] as? String ?? ""
    right = dict[_RIGHT] as? String ?? ""
    width = dict[_WIDTH] as? Int ?? 1
    height = dict[_HEIGHT] as? Int ?? 1
    lineSpacing = dict[_LINE_SPACING] as? Int ?? 0
    if dict[_TAG] != nil {
      tag = dict[_TAG] as? String
    }
    bold = (dict[_BOLD] != nil && dict[_BOLD] as! Bool == true)
    underline = (dict[_UNDERLINE] != nil && dict[_UNDERLINE] as! Bool == true)
  }
}

class ReceiptTypeImage {
  
  let DEFAULT_LOGO = "HeaderLogo.png"
  
  let _PATH = "path"
  
  var path: String?
  
  init(dict: Dictionary<String, NSObject>) {
    if (dict[_PATH] is String) {
      path = dict[_PATH] as? String;
    } else {
      path = DEFAULT_LOGO;
    }
  }
}

class ReceiptTypeFeed {
  
  let _LINES = "lines"
  
  var lines: Int = 1
  
  init(dict: Dictionary<String, NSObject>) {
    lines = dict[_LINES] as? Int ?? 1
  }
}

class ReceiptTypeBarcode {
  
  let _DATA       = "data"
  let _SYMBOLOGY  = "symbology"
  let _HEIGHT     = "height"
  
  var data: String?
  var symbology: ReceiptBarcode = ReceiptBarcode.CODE39
  var height: Int = Int(EPOS2_PARAM_UNSPECIFIED)
  
  init(dict: Dictionary<String, NSObject>) {
    data = dict[_DATA] as? String
    if dict[_SYMBOLOGY] != nil {
      symbology = ReceiptBarcode(rawValue: dict[_SYMBOLOGY] as! Int)!
    }
    if dict[_HEIGHT] != nil {
      height = dict[_HEIGHT] as! Int
    }
  }
}
