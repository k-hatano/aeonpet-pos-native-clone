//
//  PrinterEpson.swift
//  pos_native
//
//  Created by hiroki.sasabuchi on 2017/05/18.
//  Copyright © 2017 Facebook. All rights reserved.
//

import Foundation

@objc(PrinterEpson)
class PrinterEpson: NSObject, PrinterProtocol, Epos2PtrReceiveDelegate, Epos2DiscoveryDelegate {

  var printerProperty: PrinterProperty
  private var filterOption: Epos2FilterOption = Epos2FilterOption()

  var printer: Epos2Printer?
  var printerSeries: Epos2PrinterSeries = EPOS2_TM_M30
  var printerModel: Epos2ModelLang = EPOS2_MODEL_JAPANESE
  var targetPrinter: String!
  var completionEnd: ((PrinterResultCode) -> Void)?
  var timeoutWork: DispatchWorkItem!
  var _openDrawer : Bool = false
  
  fileprivate let TCP_HEADER = "TCP:"
  fileprivate let BT_HEADER = "BT:"

  required init (property: PrinterProperty) {
    printerProperty = property
    filterOption.portType = EPOS2_PORTTYPE_BLUETOOTH.rawValue
    filterOption.deviceType = EPOS2_TYPE_PRINTER.rawValue
  }

  // MARK: - Epos2PtrReceiveDelegate
  func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
    var result = disconnectPrinter()
    if PrinterResult.isSuccess(result: result) {
      result = PrinterUtil.convertErrorStatus(epsonError: status.errorStatus);
      if PrinterResult.isSuccess(result: result) {
        if status.paper != EPOS2_PAPER_OK.rawValue {
          switch status.paper {
          case EPOS2_PAPER_NEAR_END.rawValue:
            result = PrinterResultCode.ERR_PAPER_NEAR_END
          case EPOS2_PAPER_EMPTY.rawValue:
            result = PrinterResultCode.ERR_PAPER_EMPTY
          default:
            result = PrinterResultCode.ERR_UNKNOWN
          }
        } else if status.online != EPOS2_TRUE {
          result = PrinterResultCode.ERR_NOT_FOUND

        }
      }
    }
    
    completionEnd!(result)
    completionEnd = nil
  }

  // MARK: - Epos2DiscoveryDelegate
  func onDiscovery(_ deviceInfo: Epos2DeviceInfo!) {
    if deviceInfo.deviceType == EPOS2_TYPE_PRINTER.rawValue && deviceInfo.bdAddress != "" {
      targetPrinter = BT_HEADER + deviceInfo.bdAddress
      Epos2Discovery.stop()

      let printResult: PrinterResultCode = printData()
      if !PrinterResult.isSuccess(result: printResult) {
        finalizePrinterObject()
        completionEnd!(printResult)
        completionEnd = nil
      }
    }
  }

  // MARK: - PrinterProtocol
  func startCreateReceipt() -> PrinterResultCode {
    if !initializePrinterObject() {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }
    return PrinterResultCode.SUCCESS
  }

  func endCreateReceiptOut(completion: ((PrinterResultCode) -> Void)?) {
    completionEnd = completion

    var result: PrinterResultCode = findTargetPrinter()

    if !PrinterResult.isSuccess(result: result) {
      finalizePrinterObject()
      completionEnd!(result)
      completionEnd = nil
      return
    }

    if printerProperty.printerStd == PrinterStd.IP {
      let printResult: PrinterResultCode = printData()
      if !PrinterResult.isSuccess(result: printResult) {
        finalizePrinterObject()
        completion!(printResult)
        return
      }
    }

    timeoutWork = DispatchWorkItem(){ self.endTimeout() }
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(printerProperty.printTimeoutMili), execute:timeoutWork)
  }

  // bridge
  func addText(receiptText: ReceiptTypeText) -> PrinterResultCode {
    var result = align(receiptAlign: receiptText.align!)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    result = multiplyTextSize(widthSize: receiptText.width, heightSize: receiptText.height)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    if receiptText.bold {
      result = textStyleBold()
      if !PrinterResult.isSuccess(result: result) {
        return result
      }
    }

    if receiptText.underline {
      result = textStyleUnderline()
      if !PrinterResult.isSuccess(result: result) {
        return result
      }
    }

    result = addText(text: receiptText.text)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }
    result = addNewLine(lines: 1)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    return clearTextStyle()
  }

  func addCombineText(receiptCombine: ReceiptTypeCombine) -> PrinterResultCode {
    var result = alignLeft()
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    result = multiplyTextSize(widthSize: receiptCombine.width, heightSize: receiptCombine.height)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    if receiptCombine.bold {
      result = textStyleBold()
      if !PrinterResult.isSuccess(result: result) {
        return result
      }
    }

    if receiptCombine.underline {
      result = textStyleUnderline()
      if !PrinterResult.isSuccess(result: result) {
        return result
      }
    }
    
    result = addText(text: combineLeftRight(
      leftText: receiptCombine.left,
      rightText: receiptCombine.right,
      width: receiptCombine.width))
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    result = addNewLine(lines: 1)
    if !PrinterResult.isSuccess(result: result) {
      return result
    }

    return clearTextStyle()
  }

  func addImage(receiptImage: ReceiptTypeImage) -> PrinterResultCode {
    
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let documentPath        = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true).first
    let imageURL = URL(fileURLWithPath: documentPath!).appendingPathComponent(receiptImage.path!)
    
    let fileManager = FileManager.default
    if fileManager.fileExists(atPath: imageURL.path) {
      let image = UIImage(contentsOfFile: imageURL.path)
      if image != nil {
        return insertLogo(logoData: image!)
      }
    }
    
    return .SUCCESS
  }

  func feed(receiptFeed: ReceiptTypeFeed) -> PrinterResultCode {
    return feed(lines: receiptFeed.lines)
  }

  func cut() -> PrinterResultCode {
    // feed for cut
    printer!.addFeedLine(2)

    let result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func barcode(receiptBarcode: ReceiptTypeBarcode) -> PrinterResultCode {
    var result = alignCenter()
    if !PrinterResult.isSuccess(result: result) {
      return result
    }
    result = addBarcode(data: receiptBarcode.data!, symbology:receiptBarcode.symbology, height:receiptBarcode.height)
    return result
  }

  // MARK: - helper
  func addText(text: String) -> PrinterResultCode {
    var result = printer!.addTextLang(EPOS2_LANG_JA.rawValue)
    if result != EPOS2_SUCCESS.rawValue {
      return PrinterUtil.convertErrorStatus(epsonError: result)
    }
    result = printer!.addText(text)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func addNewLine(lines: Int) -> PrinterResultCode {
    let result = printer!.addFeedLine(lines)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func insertLogo(logoData: UIImage) -> PrinterResultCode {
    var result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
    if result != EPOS2_SUCCESS.rawValue {
      return PrinterUtil.convertErrorStatus(epsonError: result)
    }

    result = printer!.add(logoData, x: 0, y:0,
                          width:Int(logoData.size.width),
                          height:Int(logoData.size.height),
                          color:EPOS2_COLOR_1.rawValue,
                          mode:EPOS2_MODE_MONO.rawValue,
                          halftone:EPOS2_HALFTONE_DITHER.rawValue,
                          brightness:Double(EPOS2_PARAM_DEFAULT),
                          compress:EPOS2_COMPRESS_AUTO.rawValue);

    if result != EPOS2_SUCCESS.rawValue {
      return PrinterUtil.convertErrorStatus(epsonError: result)
    }

    result = printer!.addFeedLine(1)

    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func alignCenter() -> PrinterResultCode {
    let result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func alignLeft() -> PrinterResultCode {
    let result = printer!.addTextAlign(EPOS2_ALIGN_LEFT.rawValue)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func alignRight() -> PrinterResultCode {
    let result = printer!.addTextAlign(EPOS2_ALIGN_RIGHT.rawValue)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func multiplyTextSize(widthSize: Int, heightSize: Int) -> PrinterResultCode {
    let result = printer!.addTextSize(widthSize, height: heightSize)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func align(receiptAlign: ReceiptAlign) -> PrinterResultCode {
    var result: PrinterResultCode
    switch(receiptAlign) {
    case ReceiptAlign.CENTER:
      result = alignCenter()
    case ReceiptAlign.LEFT:
      result = alignLeft()
    case ReceiptAlign.RIGHT:
      result = alignRight()
    }
    return result
  }

  func addBarcode(data: String, symbology: ReceiptBarcode, height: Int) -> PrinterResultCode {
    let barcodeHeight = height > 0 ? height : Int(EPOS2_PARAM_UNSPECIFIED)

    let result = printer!.addBarcode(data,
                                     type:convertToEpsonBarcode(barcode: symbology).rawValue,
                                     hri:EPOS2_HRI_BELOW.rawValue,
                                     font:EPOS2_FONT_A.rawValue,
                                     width:Int(EPOS2_PARAM_UNSPECIFIED),
                                     height:barcodeHeight)

    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func textStyleBold() -> PrinterResultCode {
    let result = printer!.addTextStyle(EPOS2_PARAM_UNSPECIFIED, ul:EPOS2_PARAM_UNSPECIFIED, em:EPOS2_TRUE, color:EPOS2_PARAM_UNSPECIFIED)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func textStyleUnderline() -> PrinterResultCode {
    let result = printer!.addTextStyle(EPOS2_PARAM_UNSPECIFIED, ul:EPOS2_TRUE, em:EPOS2_PARAM_UNSPECIFIED, color:EPOS2_PARAM_UNSPECIFIED)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func clearTextStyle() -> PrinterResultCode {
    let result = printer!.addTextStyle(EPOS2_PARAM_DEFAULT, ul:EPOS2_PARAM_DEFAULT, em:EPOS2_PARAM_DEFAULT, color:EPOS2_PARAM_DEFAULT)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }

  func feed(lines: Int) -> PrinterResultCode {
    let result = printer!.addFeedLine(lines)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }
  
  func openDrawer() -> PrinterResultCode {
    let result = printer!.addPulse(EPOS2_PARAM_DEFAULT, time: EPOS2_PARAM_DEFAULT)
    return PrinterUtil.convertErrorStatus(epsonError: result)
  }
  
  func endTimeout() {
    Epos2Discovery.stop()
    if printer != nil {
      _ = disconnectPrinter()
      finalizePrinterObject()
      completionEnd?(PrinterResultCode.ERR_TIMEOUT)
      completionEnd = nil
    }
  }

  func findTargetPrinter() -> PrinterResultCode {
    var result: PrinterResultCode

    switch (printerProperty.printerStd) {
    case PrinterStd.IP:
      if !printerProperty.printerIP.isEmpty {
        targetPrinter = TCP_HEADER + printerProperty.printerIP
        result = PrinterResultCode.SUCCESS
      } else {
        result = PrinterResultCode.ERR_PARAM
      }
    case PrinterStd.BLUETOOTH:
      let btResult = Epos2Discovery.start(filterOption, delegate:self)
      result = PrinterUtil.convertBluetoothError(btError: btResult)
      if !PrinterResult.isSuccess(result: result) {
        Epos2Discovery.stop()
      }
    }
    return result
  }

  func initializePrinterObject() -> Bool {
    printer = Epos2Printer(printerSeries: printerSeries.rawValue, lang: printerModel.rawValue)
    Epos2Log.setLogSettings(
      EPOS2_PERIOD_TEMPORARY.rawValue,
      output: EPOS2_OUTPUT_STORAGE.rawValue,
      ipAddress: nil,
      port: 1,
      logSize: 1,
      logLevel: EPOS2_LOGLEVEL_LOW.rawValue);

    if printer == nil {
      return false
    }

    return true
  }

  func finalizePrinterObject() {
    if printer == nil {
      return
    }

    printer!.clearCommandBuffer()
    printer!.setReceiveEventDelegate(nil)

    printer = nil

    if timeoutWork != nil {
      timeoutWork.cancel()
      timeoutWork = nil
    }
  }

  func printData() -> PrinterResultCode {
    printer!.setReceiveEventDelegate(self)

    var status: Epos2PrinterStatusInfo?

    if printer == nil {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }

    let connectResult = connectPrinter()
    if connectResult != PrinterResultCode.SUCCESS {
      return connectResult
    }

    status = printer!.getStatus()

    if !isPrintable(status: status) {
      printer!.disconnect()
      return PrinterResultCode.ERR_DEVICE_UNPRINTABLE
    }

    let result = printer!.sendData(Int(EPOS2_PARAM_DEFAULT))
    if result != EPOS2_SUCCESS.rawValue {
      printer!.disconnect()
      return PrinterUtil.convertErrorStatus(epsonError: result)
    }

    return PrinterResultCode.SUCCESS
  }
  
  func setReceiptOption(option: ReceiptOption) {
  }

  func connectPrinter() -> PrinterResultCode {
    var result: Int32 = EPOS2_SUCCESS.rawValue

    if printer == nil {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }

    result = printer!.connect(targetPrinter, timeout:printerProperty.printTimeoutMili)
    if result != EPOS2_SUCCESS.rawValue {
        return PrinterUtil.convertErrorStatus(epsonError: result)
    }

    result = printer!.beginTransaction()
    if result != EPOS2_SUCCESS.rawValue {
      printer!.disconnect()
      return PrinterUtil.convertErrorStatus(epsonError: result)

    }
    return PrinterResultCode.SUCCESS
  }

  func disconnectPrinter() -> PrinterResultCode {
    var result: Int32 = EPOS2_SUCCESS.rawValue

    if printer == nil {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }

    result = printer!.endTransaction()
    if result != EPOS2_SUCCESS.rawValue {
      if #available(iOS 8.0, *) {
        return PrinterUtil.convertErrorStatus(epsonError: result)
      } else {
        return PrinterResultCode.ERR_IOS_VERSION
      }
    }

    result = printer!.disconnect()
    if result != EPOS2_SUCCESS.rawValue {
      if #available(iOS 8.0, *) {
        return PrinterUtil.convertErrorStatus(epsonError: result)
      } else {
        return PrinterResultCode.ERR_IOS_VERSION
      }
    }

    finalizePrinterObject()
    return PrinterResultCode.SUCCESS
  }

  func isPrintable(status: Epos2PrinterStatusInfo?) -> Bool {
    if status == nil {
      return false
    }

    if status!.connection == EPOS2_FALSE {
      return false
    }
    else if status!.online == EPOS2_FALSE {
      return false
    }
    else {
      // print available
    }
    return true
  }

  func convertToEpsonBarcode(barcode: ReceiptBarcode) -> Epos2Barcode {
    var epsonBarcode: Epos2Barcode
    switch (barcode) {
    case ReceiptBarcode.CODE39:
      epsonBarcode = EPOS2_BARCODE_CODE93
    }
    return epsonBarcode
  }
  
  func setLineSpace(linespc: Int) {
    printer?.addLineSpace(linespc)
  }
}

enum EPOS2_BOOLEAN {
  case TRUE
  case FALSE
  
  static func convertToBool(_ int: Int32) -> EPOS2_BOOLEAN {
    if int == 1 {
      return .TRUE
    } else {
      return .FALSE
    }
  }
}
