//
//  CashChangerResultCode.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 2019/06/20.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

enum CashChangerResultCode: Int {
  case SUCCESS = 0
  case ERR_BUSY
  case ERR_DISCREPANCY
  case ERR_CASH_IN_TRAY
  case ERR_SHORTAGE
  case ERR_REJECT_UNIT
  case ERR_OPOSCODE
  case ERR_UNSUPPORTED
  case ERR_PARAM
  case ERR_COMMAND
  case ERR_DEVICE
  case ERR_SYSTEM
  case ERR_FAILURE
  case ERR_UNKNOWN = 999
  
  static func convert(from int: Int32) -> CashChangerResultCode {
    switch int {
    case EPOS2_CCHANGER_CODE_SUCCESS.rawValue:
      return CashChangerResultCode.SUCCESS
    case EPOS2_CCHANGER_CODE_BUSY.rawValue:
      return CashChangerResultCode.ERR_BUSY
    case EPOS2_CCHANGER_CODE_DISCREPANCY.rawValue:
      return CashChangerResultCode.ERR_DISCREPANCY
    case EPOS2_CCHANGER_CODE_ERR_CASH_IN_TRAY.rawValue:
      return CashChangerResultCode.ERR_CASH_IN_TRAY
    case EPOS2_CCHANGER_CODE_ERR_SHORTAGE.rawValue:
      return CashChangerResultCode.ERR_SHORTAGE
    case EPOS2_CCHANGER_CODE_ERR_REJECT_UNIT.rawValue:
      return CashChangerResultCode.ERR_REJECT_UNIT
    case EPOS2_CCHANGER_CODE_ERR_OPOSCODE.rawValue:
      return CashChangerResultCode.ERR_OPOSCODE
    case EPOS2_CCHANGER_CODE_ERR_UNSUPPORTED.rawValue:
      return CashChangerResultCode.ERR_UNSUPPORTED
    case EPOS2_CCHANGER_CODE_ERR_PARAM.rawValue:
      return CashChangerResultCode.ERR_PARAM
    case EPOS2_CCHANGER_CODE_ERR_COMMAND.rawValue:
      return CashChangerResultCode.ERR_COMMAND
    case EPOS2_CCHANGER_CODE_ERR_DEVICE.rawValue:
      return CashChangerResultCode.ERR_DEVICE
    case EPOS2_CCHANGER_CODE_ERR_SYSTEM.rawValue:
      return CashChangerResultCode.ERR_SYSTEM
    case EPOS2_CCHANGER_CODE_ERR_FAILURE.rawValue:
      return CashChangerResultCode.ERR_FAILURE
    default:
      return CashChangerResultCode.ERR_UNKNOWN
    }
  }
}
