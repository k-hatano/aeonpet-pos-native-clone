//
//  CashChangerUtil.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 2019/06/20.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

public final class CashChangerUtil {
  
  static func convertCChangerStatus(cchanger: Int32) -> String {
    switch cchanger {
    case EPOS2_CCHANGER_STATUS_BUSY.rawValue:
      return "EPOS2_CCHANGER_STATUS_BUSY"
    case EPOS2_CCHANGER_STATUS_PAUSE.rawValue:
      return "EPOS2_CCHANGER_STATUS_PAUSE"
    case EPOS2_CCHANGER_STATUS_END.rawValue:
      return "EPOS2_CCHANGER_STATUS_END"
    case EPOS2_CCHANGER_STATUS_ERR.rawValue:
      return "EPOS2_CCHANGER_STATUS_ERR"
    default:
      return "UNKNOWN"
    }
  }
  
}
