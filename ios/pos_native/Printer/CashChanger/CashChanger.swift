//
//  swift
//  Orange POS
//
//  Created by teppei.kikuchi on 8/28/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

class CashChanger: NSObject, CashChangerProtocol {
  
  static let sharedInstance = CashChanger()
  
  var printerProperty: PrinterProperty!
  var epoosCashChanger = Epos2CashChanger()
  
  fileprivate let TCP_HEADER = "TCP:"
  fileprivate let cashChangerDeviceID = "CashChanger"
  
  let depositingKey = "depositing"
  var depositingStatus: Bool {
    get {
      return UserDefaults.standard.bool(forKey: depositingKey)
    }
    set(bool) {
      UserDefaults.standard.set(bool, forKey: depositingKey)
    }
  }
  
  private override init () {}
  
  func setProperty(property: PrinterProperty) {
    printerProperty = property
  }
  
  func connectCasher() -> PrinterResultCode {
    
    if isConnectedToCasher() {
      return PrinterResultCode.SUCCESS
    }
    
    epoosCashChanger?.setDispenseEventDelegate(self)
    epoosCashChanger?.setDepositEventDelegate(self)
    epoosCashChanger?.setCashCountEventDelegate(self)
    epoosCashChanger?.setDirectIOCommandReplyEventDelegate(self)
    
    var result = PrinterResultCode.ERR_UNKNOWN
    if case EPOS2_BOOLEAN.convertToBool(epoosCashChanger!.getStatus()?.connection ?? 0) = EPOS2_BOOLEAN.FALSE {
      result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.connect(TCP_HEADER + printerProperty.printerIP +  "[\(cashChangerDeviceID)]", timeout: 5000))
    }
    
    if depositingStatus {
      Util.dump(endDepositCasher(type: 1))
    }
    
    NotificationCenter.default.addObserver(self, selector: #selector(disconnect), name: .UIApplicationWillTerminate, object: nil)
    
    Util.dump(EPOS2_BOOLEAN.convertToBool(epoosCashChanger!.getStatus()?.connection ?? 0))
    Util.dump(result)
    return result
  }
  
  func isConnectedToCasher() -> Bool {
    return EPOS2_BOOLEAN.convertToBool(epoosCashChanger!.getStatus()?.connection ?? 0) ~= EPOS2_BOOLEAN.TRUE
  }
  
  func confirmConnection() -> Bool {
    if !isConnectedToCasher() {
      return connectCasher() ~= PrinterResultCode.SUCCESS ? true : false
    } else {
      return true
    }
  }
  
  @objc func disconnect() {
    Util.dump(disconnectCasher())
  }
  
  func disconnectCasher() -> PrinterResultCode {
    
    if isConnectedToCasher() {
      var result = PrinterResultCode.ERR_UNKNOWN
      result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.disconnect())
      
      Util.dump(result)
      return result
    } else {
      NotificationCenter.default.removeObserver(self, name: .UIApplicationWillTerminate, object: nil)
      return PrinterResultCode.SUCCESS
    }
    
  }
  
  func readCashCount() -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.readCashCount())
    
    Util.dump(result)
    return result
  }
  
  func depositCasher() -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.beginDeposit())
    
    depositingStatus = true
    NotificationCenter.default.addObserver(self, selector: #selector(endDeposit), name: .UIApplicationWillResignActive, object: nil)
    
    Util.dump(result)
    return result
  }
  
  func pauseDepositCasher() -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.pauseDeposit())
    
    Util.dump(result)
    return result
  }
  
  func restartDepositCasher() -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.restartDeposit())
    
    Util.dump(result)
    return result
  }
  
  @objc func endDeposit() {
    
    guard confirmConnection() else {
      return
    }
    
    Util.dump(endDepositCasher(type: 2))
  }
  
  func endDepositCasher(type: Int) -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    Util.dump(epoosCashChanger!.getStatus()?.connection)
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.endDeposit(Int32(type)))
    
    depositingStatus = false
    NotificationCenter.default.removeObserver(self, name: .UIApplicationWillResignActive, object: nil)
    
    Util.dump(result)
    return result
  }
  
  func dispenseCasher(data: [NSString:Int32]) -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    Util.dump(epoosCashChanger!.getStatus()?.connection)
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.dispenseCash(data))
    
    Util.dump(result)
    return result
  }
  
  func dispenseChange(changeValue: Int) -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    var result = PrinterResultCode.ERR_UNKNOWN
    Util.dump(epoosCashChanger!.getStatus()?.connection)
    result = PrinterUtil.convertErrorStatus(epsonError: epoosCashChanger!.dispenseChange(changeValue))
    
    Util.dump(result)
    return result
  }
  
  fileprivate func sendCashChangerError(cchanger: Epos2CashChanger, from code: Int32) {
    if CashChangerResultCode.SUCCESS ~= CashChangerResultCode.convert(from: code) {
      return
    }else if CashChangerResultCode.convert(from: code) ~= CashChangerResultCode.ERR_OPOSCODE {
      Util.dump(cchanger.getOposErrorCode())
      printerProperty.bridge.sendCashChangerError(result: cchanger.getOposErrorCode())
    }else {
      printerProperty.bridge.sendCashChangerError(result: code)
    }
  }
  
  func readCasherStatus() -> PrinterResultCode {
    
    guard confirmConnection() else {
      return PrinterResultCode.ERR_CONNECT
    }
    
    epoosCashChanger?.sendDirectIOCommand(CashChangerDirectIOCode.CHAN_DI_STATUSREAD.rawValue, data: CashChangerStatusReadParam.allStatus.rawValue, string: "")
    return PrinterResultCode.SUCCESS
  }
  
}

extension CashChanger: Epos2CChangerDispenseDelegate {
  func onCChangerDispense(_ cchangerObj: Epos2CashChanger!, code: Int32) {
    Util.dump(code)
    Util.dump(CashChangerResultCode.convert(from: code))
    if !(CashChangerResultCode.SUCCESS ~= CashChangerResultCode.convert(from: code)) {
      sendCashChangerError(cchanger: cchangerObj, from: code)
    } else{
      printerProperty.bridge.sendDispenseComplete(result: code)
    }
  }
}

extension CashChanger: Epos2CChangerDepositDelegate {
  func onCChangerDeposit(_ cchangerObj: Epos2CashChanger!, code: Int32, status: Int32, amount: Int, data: [AnyHashable : Any]!) {
    Util.dump(CashChangerResultCode.convert(from: code))
    Util.dump(CashChangerUtil.convertCChangerStatus(cchanger: status))
    Util.dump(amount)
    Util.dump(data)
    if CashChangerResultCode.SUCCESS ~= CashChangerResultCode.convert(from: code) {
      printerProperty.bridge.sendDepositAmountUpdate(result: amount)
    } else {
      sendCashChangerError(cchanger: cchangerObj, from: code)
    }
  }
}

extension CashChanger: Epos2CChangerCashCountDelegate {
  func onCChangerCashCount(_ cchangerObj: Epos2CashChanger!, code: Int32, data: [AnyHashable : Any]!) {
    Util.dump(CashChangerResultCode.convert(from: code))
    Util.dump(data)
    let castedData = data as! [NSString:Int32]
    
    if CashChangerResultCode.SUCCESS ~= CashChangerResultCode.convert(from: code) {
      printerProperty.bridge.cashCountComplete(result: castedData)
    } else {
      sendCashChangerError(cchanger: cchangerObj, from: code)
    }
  }
}

extension CashChanger: Epos2CChangerDirectIOCommandReplyDelegate {
  func onCChangerDirectIOCommandReply(_ cchangerObj: Epos2CashChanger!, code: Int32, command: Int, data: Int, string: String!) {
    Util.dump(string)
    printerProperty.bridge.sendCashChangerStatus(result: string)
  }
}
