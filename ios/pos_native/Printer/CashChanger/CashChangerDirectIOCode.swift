
import Foundation

enum CashChangerDirectIOCode: Int {
  case CHAN_DI_RESET = 1
  case CHAN_DI_MEMREAD = 2
  case CHAN_DI_MEMCLR = 3
  case CHAN_DI_CHGMODE = 4
  case CHAN_DI_SSWSET = 5
  case CHAN_DI_TIMESET = 6
  case CHAN_DI_ENQ = 7
  case CHAN_DI_STRING = 8
  case CHAN_DI_COLLECT = 9
  case CHAN_DI_STATUSREAD = 10
  case CHAN_DI_SUPPLYCOUNTS = 11
  case CHAN_DI_SEISA = 12
  case CHAN_DI_DEPOSITDATAREAD = 13
  case CHAN_DI_BEGINDEPOSIT = 14
  case CHAN_DI_ENDDEPOSIT = 15
  case CHAN_DI_PAUSEDEPOSIT = 16
  case CHAN_DI_RESTARTDEPOSIT = 17
  case CHAN_DI_DEPOSITMODE = 18
  case CHAN_DI_COUNTCLR = 19
  case CHAN_DI_GETLOG = 20
  case CHAN_DI_OPENDRAWER = 21
  case CHAN_DI_CHILDLOCK = 22
  case CHAN_DI_CLOSE = 23
  case CHAN_DI_SUPPLY = 24
  case CHAN_DI_BEGINDEPOSITOUTSIDE = 25
  case CHAN_DI_DISPENSECHANGEOUTSIDE = 26
  case CHAN_DI_DISPENSECASHOUTSIDE = 27
  case CHAN_DI_BEGINCASHRETURN = 28
  case CHAN_DI_CLOSEDATAREAD = 29
}

/// 釣銭機状態取得時のリードNo
enum CashChangerStatusReadParam: Int {
  /// 硬貨部の状態確認
  case coinStatus = 0x01
  /// 硬貨部前回の枚数確認
  case coinStatusLastTime = 0x04
  /// 硬貨部累計の枚数確認
  case coinStatusSum = 0x05
  /// 紙幣部・硬貨部の状態確認
  case allStatus  = 0x80
  /// 紙幣部前回の枚数確認
  case billStatusLastTime = 0x82
  /// 紙幣部累計の枚数確認
  case billStatusSum = 0x83
}
