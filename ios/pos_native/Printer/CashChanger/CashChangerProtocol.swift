//
//  CashChangerProtocol.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 8/28/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

protocol CashChangerProtocol {
  func setProperty(property: PrinterProperty)
  func disconnectCasher() -> PrinterResultCode
  func connectCasher() -> PrinterResultCode
  func readCashCount() -> PrinterResultCode
  func depositCasher() -> PrinterResultCode
  func pauseDepositCasher() -> PrinterResultCode
  func restartDepositCasher() -> PrinterResultCode
  func endDepositCasher(type: Int) -> PrinterResultCode
  func dispenseCasher(data: [NSString : Int32]) -> PrinterResultCode
  func dispenseChange(changeValue: Int) -> PrinterResultCode
  func readCasherStatus() -> PrinterResultCode
}
