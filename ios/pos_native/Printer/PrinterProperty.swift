import Foundation

enum PrinterSDK: Int {
  case EPSON
  case STAR
}

enum PrinterStd: Int {
  case IP
  case BLUETOOTH
}

class PrinterProperty {
  let PRINTER_SDK         = "PRINTER_SDK"
  let PRINTER_STD         = "PRINTER_STD"
  let PRINTER_IP          = "PRINTER_IP"
  let PRINT_TIMEOUT_MILI  = "PRINTER_TIMEOUT"
  let RECEIPT_LINE_LENGTH = "RECEIPT_LINE_LENGTH"
  let BRIDGE = "BRIDGE"
  
  var printerSdk: PrinterSDK
  var printerStd: PrinterStd
  var printerIP: String = ""
  var printTimeoutMili: Int = 10000
  var receiptLineLength: Int = 48
  
  var bridge: PrinterBridge
  
  init(dict: [String: Any]) {
    printerSdk = PrinterSDK(rawValue: (dict[PRINTER_SDK] as? Int)!)!
    printerStd = PrinterStd(rawValue: (dict[PRINTER_STD] as? Int)!)!
    printerIP = dict[PRINTER_IP] as? String ?? ""
    bridge = dict[BRIDGE] as! PrinterBridge
    let timeoutMili = dict[PRINT_TIMEOUT_MILI] as? Int
    if timeoutMili != nil {
      printTimeoutMili = timeoutMili!
    }
    let lineLength = dict[RECEIPT_LINE_LENGTH] as? Int
    if lineLength != nil {
      receiptLineLength = lineLength!
    }
  }
}
