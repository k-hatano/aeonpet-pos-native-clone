#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

#import "Orange_POS-Swift.h"

#import <OPNBluetoothKit/OPN2002iBluetoothService.h>

#import "CrashReporter/CrashReporter.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;

  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"pos_native"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  // regist sleep event
  CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),
                                  (__bridge const void *)(self),
                                  applicationWillLocked,
                                  CFSTR("com.apple.springboard.lockcomplete"),
                                  NULL,
                                  CFNotificationSuspensionBehaviorDeliverImmediately);
  
  PLCrashReporter *crashReporter = [PLCrashReporter sharedReporter];
  NSError *error;
  
  if (![crashReporter enableCrashReporterAndReturnError: &error])
    NSLog(@"Warning: Could not enable crash reporter: %@", error);
  
  [CrashReportManager initReport];
  
  return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  OPN2002iBluetoothService *sessionController = [OPN2002iBluetoothService sharedController];
  [sessionController openSession];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  OPN2002iBluetoothService *sessionController = [OPN2002iBluetoothService sharedController];
  [sessionController closeSession];
  
}

void applicationWillLocked(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) {
  
  PosEventRegister *eventRegister = [PosEventRegister sharedInstance];
  [eventRegister dispatchWithName:@"HOME_RETURN_FROM_SLEEP" body:@""];
}

@end
