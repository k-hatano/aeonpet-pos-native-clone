
import Foundation

@objc(PosEventEmitter)
class PosEventEmitter: RCTEventEmitter {

  override init() {
    super.init()
    PosEventRegister.sharedInstance.registerEventEmitter(eventEmitter: self)
  }
  
  @objc open override func supportedEvents() -> [String] {
    return PosEventRegister.sharedInstance.allEvents
  }
}
