
let HOME_RETURN_FROM_SLEEP = "HOME_RETURN_FROM_SLEEP"

@objc(PosEventRegister)
class PosEventRegister : NSObject {

  public static var sharedInstance = PosEventRegister()

  private static var eventEmitter: PosEventEmitter!
  
  private override init() {}

  func registerEventEmitter(eventEmitter: PosEventEmitter) {
    PosEventRegister.eventEmitter = eventEmitter
  }
  
  func dispatch(name: String, body: Any?) {
    if PosEventRegister.eventEmitter != nil {
      PosEventRegister.eventEmitter.sendEvent(withName: name, body: body)
    }
  }
  
  lazy var allEvents: [String] = {
    var allEventNames: [String] = [HOME_RETURN_FROM_SLEEP]
    
    return allEventNames
  }()
  
}
