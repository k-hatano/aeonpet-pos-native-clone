#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(PasoriBridge, NSObject)

RCT_EXTERN_METHOD(startInitialize:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(closePasori)
RCT_EXTERN_METHOD(start:(RCTResponseSenderBlock)callback)

@end
