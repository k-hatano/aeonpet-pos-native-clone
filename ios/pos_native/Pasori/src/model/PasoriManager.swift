//
//  PasoriManager.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 8/29/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import MediaPlayer
import CoreBluetooth

class PasoriManager: NSObject {
  
  let manager = CBCentralManager()
  var bridge: PasoriBridge!
  static let sharedInstance = PasoriManager()
  fileprivate let confirmBLEMesseage = "Bluetoothの設定を確認してください"
  
  private override init() {}
  
  func startInitialize(bridge: PasoriBridge) -> Int32? {
    manager.delegate = self
    self.bridge = bridge
    guard confirmBLEStatus() else {
      return nil
    }
    return PasoriPolling.startInitialize()
  }
  
  @objc
  func closePasori() {
    PasoriPolling.closePasori()
  }
  
  func start() -> NSMutableDictionary? {
    guard confirmBLEStatus() else {
      return nil
    }
    return PasoriPolling.start()
  }
  
  func confirmBLEStatus() -> Bool {
    if manager.state != .poweredOn {
      self.bridge.confirmBluetoothStatus(result: confirmBLEMesseage)
      return false
    } else {
      return true
    }
  }
  
}

extension PasoriManager: CBCentralManagerDelegate {
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    if central.state != .poweredOn {
      bridge.confirmBluetoothStatus(result: confirmBLEMesseage)
    }
  }
}
