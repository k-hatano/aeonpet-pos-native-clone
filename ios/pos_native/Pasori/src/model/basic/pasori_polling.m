/*
 * Copyright 2013 Sony Corporation
 */

#import "pasori_polling.h"

#import "felica_card.h"
#import "felica_cc.h"
#import "felica_cc_stub.h"

#import "ics_types.h"
#import "ics_error.h"
#import "ics_hwdev.h"
#import "icsdrv.h"
#import "icslib_chk.h"
#import "icslog.h"

#ifndef DEFAULT_UUID
#define DEFAULT_UUID ""
#endif
#ifndef DEFAULT_TIMEOUT
#define DEFAULT_TIMEOUT 400 /* ms */
#endif
#ifndef DEFAULT_SYSTEM_CODE
#define DEFAULT_SYSTEM_CODE 0xFE00
#endif
#ifndef DEFAULT_POLLING_MAX_RETRY_TIMES
#define DEFAULT_POLLING_MAX_RETRY_TIMES 9
#endif
#ifndef DEFAULT_POLLING_INTERVAL
#define DEFAULT_POLLING_INTERVAL 500 /* ms */
#endif
#ifndef DEFAULT_POLLING_OPTION
#define DEFAULT_POLLING_OPTION 0
#endif
#ifndef DEFAULT_POLLING_TIMESLOT
#define DEFAULT_POLLING_TIMESLOT 0
#endif

/* begin for get block data */
#ifndef FELICA_CC_MAX_COMMAND_LEN
#define FELICA_CC_MAX_COMMAND_LEN 254
#endif
#ifndef FELICA_CC_CMD_READ_WITHOUT_ENCRYPTION
#define FELICA_CC_CMD_READ_WITHOUT_ENCRYPTION 0x06
#endif
#ifndef FELICA_CC_RES_READ_WITHOUT_ENCRYPTION
#define FELICA_CC_RES_READ_WITHOUT_ENCRYPTION 0x07
#endif
#ifndef DEFAULT_SERVICE_CODE_LIST
#define DEFAULT_SERVICE_CODE_LIST 0x67CF
#endif
#ifndef DEFAULT_NUM_OF_BLOCKS
#define DEFAULT_NUM_OF_BLOCKS 2
#endif
/* end for get block data */

/* These functions are defined in another file. */
extern const icsdrv_basic_func_t* g_drv_func;
extern UINT32 (*g_felica_cc_stub_initialize_func)(
                                                  felica_cc_devf_t* devf,
                                                  ICS_HW_DEVICE* dev);

static const char* s_uuid        = DEFAULT_UUID;
static UINT32 s_timeout          = DEFAULT_TIMEOUT;
static UINT16 s_system_code      = DEFAULT_SYSTEM_CODE;
static UINT32 s_polling_interval = DEFAULT_POLLING_INTERVAL;
static UINT8 s_polling_option    = DEFAULT_POLLING_OPTION;
static UINT8 s_polling_timeslot  = DEFAULT_POLLING_TIMESLOT;
static ICS_HW_DEVICE dev;
static felica_cc_devf_t devf;

static const UINT16 service_code_list = DEFAULT_SERVICE_CODE_LIST;

@implementation PasoriPolling

static int initialize(
                      ICS_HW_DEVICE* dev,
                      felica_cc_devf_t* devf)
{
  UINT32 rc;
  
  printf("start initialization ...\n");
  
  printf("  calling open(%s) ...\n", s_uuid);
  rc = g_drv_func->open(dev, s_uuid);
  if (rc != ICS_ERROR_SUCCESS) {
    fprintf(stderr, "    failure in open():%u\n", rc);
    return -1;
  }
  
  if (g_drv_func->initialize_device != NULL) {
    printf("  calling initialize_device() ...\n");
    rc = g_drv_func->initialize_device(dev, s_timeout);
    if (rc != ICS_ERROR_SUCCESS) {
      fprintf(stderr, "    failure in initialize_device():%u\n", rc);
      rc = g_drv_func->close(dev);
      if (rc != ICS_ERROR_SUCCESS) {
        fprintf(stderr, "    failure in close():%u\n", rc);
        /* Note: continue */
      }
      return -1;
    }
  }
  
  printf("  calling felica_cc_stub_initialize() ...\n");
  rc = (*g_felica_cc_stub_initialize_func)(devf, dev);
  if (rc != ICS_ERROR_SUCCESS) {
    fprintf(stderr, "    failure in felica_cc_stub_initialize():%u\n", rc);
    rc = g_drv_func->close(dev);
    if (rc != ICS_ERROR_SUCCESS) {
      fprintf(stderr, "    failure in close():%u\n", rc);
      /* Note: continue */
    }
    return -1;
  }
  
  printf("  calling ping() ...\n");
  if (g_drv_func->ping != NULL) {
    rc = g_drv_func->ping(dev, s_timeout);
    if (rc != ICS_ERROR_SUCCESS) {
      fprintf(stderr, "    failure in ping():%u\n", rc);
      rc = g_drv_func->close(dev);
      if (rc != ICS_ERROR_SUCCESS) {
        fprintf(stderr, "    failure in close():%u\n", rc);
        /* Note: continue */
      }
      return -1;
    }
  }
  
  return 0;
}

static int readOff(
                   ICS_HW_DEVICE* dev,
                   felica_cc_devf_t* devf)
{
  UINT32 rc;
  
  printf("start stopping ...\n");
  
  printf("  calling off() ...\n");
  rc = g_drv_func->rf_off(dev, s_timeout);
  if (rc != ICS_ERROR_SUCCESS) {
    fprintf(stderr, "    failure in close():%u\n", rc);
    return -1;
  }
  
  return 0;
}

static int closePasori(
                       ICS_HW_DEVICE* dev,
                       felica_cc_devf_t* devf)
{
  UINT32 rc;
  
  printf("start closing ...\n");
  
  printf("  calling close() ...\n");
  rc = g_drv_func->close(dev);
  if (rc != ICS_ERROR_SUCCESS) {
    fprintf(stderr, "    failure in close():%u\n", rc);
    /* Note: continue */
  }
  
  return 0;
}

+ (int)startInitialize
{
  int res;
  
  printf("FeliCa Polling sample.\n");
  
  res = initialize(&dev, &devf);
  if (res != 0) {
    return 0;
  }
  return 1;
}

+ (int)stop
{
  int res;
  
  printf("FeliCa Stoping.\n");
  
  res = readOff(&dev, &devf);
  if (res != 0) {
    return 0;
  }
  return 1;
}

+ (int)closePasori
{
  int res;
  
  printf("FeliCa Closing.\n");
  
  res = closePasori(&dev, &devf);
  if (res != 0) {
    return 0;
  }
  return 1;
}

+ (NSMutableDictionary *)start
{
  UINT32 rc;
  int i;
  felica_card_t card;
  felica_card_option_t card_option;
  
  NSMutableDictionary *dic = [@{
                                @"status": @"",
                                @"data"  : @"",
                                @"error" : @""} mutableCopy];
  
  printf("FeliCa Polling sample.\n");
  
  /*
   * Polling
   */
  {
    UINT8 polling_param[4];
    
    polling_param[0] = (UINT8)((s_system_code >> 8) & 0xff);
    polling_param[1] = (UINT8)((s_system_code >> 0) & 0xff);
    polling_param[2] = s_polling_option;
    polling_param[3] = s_polling_timeslot;
    
    printf("  calling felica_cc_polling() ...\n");
    rc = felica_cc_polling(&devf,
                           polling_param,
                           &card,
                           &card_option,
                           s_timeout);
    if (rc != ICS_ERROR_TIMEOUT) {
      
    } else {
      fprintf(stderr, "    felica_cc_polling(): polling timeout.\n");
      utl_msleep(s_polling_interval);
    }
    if (rc != ICS_ERROR_SUCCESS) {
      fprintf(stderr, "    failure in felica_cc_polling():%u\n", rc);
      dic[@"status"] = @"0";
      dic[@"error"] = (rc != ICS_ERROR_TIMEOUT) ? @"ICカードリーダーの接続を確認してください" : @"";
      return dic;
    }
    
    printf("    IDm: %02x%02x%02x%02x%02x%02x%02x%02x\n",
           card.idm[0], card.idm[1], card.idm[2], card.idm[3],
           card.idm[4], card.idm[5], card.idm[6], card.idm[7]);
    printf("    PMm: %02x%02x%02x%02x%02x%02x%02x%02x\n",
           card.pmm[0], card.pmm[1], card.pmm[2], card.pmm[3],
           card.pmm[4], card.pmm[5], card.pmm[6], card.pmm[7]);
    printf("    Option: ");
    
    for (i = 0; i < (int)card_option.option_len; i++) {
      printf("%02x", card_option.option[i]);
    }
    printf("\n");
  }
  
  NSString *read;
  
 read = [self readWithoutEncryptionBy:card];
  
  if ([read length] == 0) {
    printf("    failure read data\n");
    dic[@"status"] = @"0";
    dic[@"error"] = @"ご利用できないICカードです";
    return dic;
  }
  printf("    success read data\n");
  dic[@"status"] = @"1";
  dic[@"data"] = read;
  return dic;
}

+ (NSString *)readWithoutEncryptionBy:(felica_card_t)card
{
  UINT32 rc;
  UINT8 num_of_services = 1;
  UINT8 num_of_blocks   = DEFAULT_NUM_OF_BLOCKS;
  UINT8 block_list[DEFAULT_NUM_OF_BLOCKS * 2] = {0x80, 0x00, 0x80, 0x01};
  UINT8 block_data;
  UINT8 status_flag1;
  UINT8 status_flag2;
  
  rc = felica_cc_read_without_encryption(&devf,
                                         &card,
                                         num_of_services,
                                         &service_code_list,
                                         num_of_blocks,
                                         block_list,
                                         &block_data,
                                         &status_flag1,
                                         &status_flag2,
                                         s_timeout);
  
  if (rc == ICS_ERROR_TIMEOUT) {
    fprintf(stderr, "    readWithoutEncryptionBy(): timeout.\n");
    utl_msleep(s_polling_interval);
  }
  if (rc != ICS_ERROR_SUCCESS) {
    fprintf(stderr, "    failure in readWithoutEncryptionBy():%u\n", rc);
    return @"";
  }
  
  UINT8* data = &block_data;
  printf("    Num of Blocks: %02d \n", DEFAULT_NUM_OF_BLOCKS);
  printf("    Block Data: \n        ");
  NSMutableString *resultData = [NSMutableString string];
  NSString *tmpData = [NSMutableString stringWithString:@""];
  int waonIdDataStartByte = 12;
  int waonIdDataEndByte = 20;
  for (int i = waonIdDataStartByte; i < waonIdDataEndByte; i++) {
    tmpData = [NSString stringWithFormat:@"%02x", data[i]];
    [resultData appendString:tmpData];
    printf(" %02x ", data[i]);
  }
  printf("\n");
  return resultData;
}

@end
