/*
 * Copyright 2013 Sony Imaging Products & Solutions Inc.
 */

#import <Foundation/Foundation.h>
#import "felica_card.h"

@interface PasoriPolling : NSObject

+ (NSMutableDictionary *)start;
+ (int)startInitialize;
+ (int)stop;
+ (int)closePasori;
+ (NSString *)readWithoutEncryptionBy:(felica_card_t)card;

@end
