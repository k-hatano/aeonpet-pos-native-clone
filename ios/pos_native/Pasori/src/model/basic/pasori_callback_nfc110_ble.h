/*
 * Copyright 2013 Sony Imaging Products & Solutions Inc.
 */

#import <Foundation/Foundation.h>

@interface PasoriCallback : NSObject

+ (void)registerCallback;
+ (void)unregisterCallback;
+ (void)start;
+ (void)stop;

@end
