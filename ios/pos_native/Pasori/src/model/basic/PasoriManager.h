#import <Foundation/Foundation.h>

@interface PasoriManager : NSObject
// 接続されているかどうか
@property Boolean isConnecting;

/**
 インスタンス生成ファクトリメソッド
 @return Singletonクラスのインスタンス
 */
+ (PasoriManager *)sharedManager;

@end
