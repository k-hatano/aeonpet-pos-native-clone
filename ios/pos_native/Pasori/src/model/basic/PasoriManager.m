#import "PasoriManager.h"

@implementation PasoriManager

static PasoriManager *sharedInstance = nil;

+ (PasoriManager *)sharedManager
{
    // dispatch_once_t を利用することでインスタンス生成を1度に制限できる
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PasoriManager alloc] init];
        sharedInstance.isConnecting = false;
    });
    return sharedInstance;
}

/**
 外部からallocされた時のためにallocWithZoneをオーバーライドして、
 一度しかインスタンスを返さないようにする
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    __block id ret = nil;
    
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedInstance = [super allocWithZone:zone];
        ret = sharedInstance;
    });
    
    return  ret;
}

/**
 copyで別インスタンスが返されないようにするため
 copyWithZoneをオーバーライドして、自身のインスタンスを返すようにする。
 */
- (id)copyWithZone:(NSZone *)zone{
    
    return self;
}

@end
