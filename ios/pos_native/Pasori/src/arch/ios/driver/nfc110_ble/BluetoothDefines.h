/**
 * \brief    the header file for NFC Port-110 BLE Driver public defines (iOS)
 * \date     2014/05/16
 * \author   Copyright 2013-2014 Sony Imaging Products & Solutions Inc.
 */

#ifndef BLUETOOTHDEFINES_H_
#define BLUETOOTHDEFINES_H_

/* --------------------------------
 * Constants
 * -------------------------------- */

typedef NS_ENUM(NSInteger, BLEConnectionState) {
    BLEConnectionStateUnknown = 0,
    BLEConnectionStateConnected,
    BLEConnectionStateDisconnected,
    BLEConnectionStateDiscovered,
    BLEConnectionStateCentralStateMask = 0x8000,
};

/* --------------------------------
 * Callbacks
 * -------------------------------- */

typedef void (*BLENotifyCallback)(
    NSData* data,
    id content);

typedef void (*BLEConnectionStateCallback)(
    BLEConnectionState state,
    void* handle,
    id content);

typedef void (*BLEConnectionStateCallback2)(
    BLEConnectionState state,
    void* handle,
    id content,
    void* option);

#endif /* !BLUETOOTHDEFINES_H_ */
