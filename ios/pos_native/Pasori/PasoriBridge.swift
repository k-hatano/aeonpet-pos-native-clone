import Foundation

@objc(PasoriBridge)
class PasoriBridge: RCTEventEmitter {
  
  let manager = PasoriManager.sharedInstance
  
  // MARK: - PasoriBridge
  @objc(startInitialize:)
  func startInitialize(callback: @escaping RCTResponseSenderBlock) {
    let result = manager.startInitialize(bridge: self)
    callback([NSNull(), result])
  }
  
  @objc
  func closePasori() {
    manager.closePasori()
  }
  
  @objc(start:)
  func start(callback: @escaping RCTResponseSenderBlock) {
    let result = manager.start()
    callback([NSNull(), result])
  }
  
  override func supportedEvents() -> [String]! {
    return [
      "ConfirmBluetoothStatus"
    ]
  }
  
  @objc(confirmBluetoothStatus:)
  func confirmBluetoothStatus(result: String) {
    sendEvent(withName: "ConfirmBluetoothStatus", body: result)
  }
  
}
