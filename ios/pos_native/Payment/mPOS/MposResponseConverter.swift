import Foundation
#if os(iOS) && !(arch(i386) || arch(x86_64))
import mPOS2SDK
#endif
  
class MposResponseConverter {
#if os(iOS) && !(arch(i386) || arch(x86_64))
  func convertResponseToDic(response: [MposResponseParam : AnyObject]) -> NSDictionary {
    let responseDic:NSDictionary = NSMutableDictionary()
    
    if let res = response[MposResponseParam.status] {
      responseDic.setValue(res, forKey: "status")
    }
    
    if let res = response[MposResponseParam.errorCode] {
      responseDic.setValue(res, forKey: "errorCode")
    }
    
    if let res = response[MposResponseParam.errorMessage] {
      responseDic.setValue(res, forKey: "errorMessage")
    }
    
    if let res = response[MposResponseParam.resultCode] {
      responseDic.setValue(res, forKey: "resultCode")
    }
    
    if let res = response[MposResponseParam.amount] {
      responseDic.setValue(res, forKey: "amount")
    }
    
    if let res = response[MposResponseParam.orderId] {
      responseDic.setValue(res, forKey: "orderId")
    }
    
    if let res = response[MposResponseParam.paymentMethodCode] {
      responseDic.setValue(res, forKey: "paymentMethodCode")
    }
    
    if let res = response[MposResponseParam.jpo] {
      responseDic.setValue(res, forKey: "jpo")
    }
    
    if let res = response[MposResponseParam.cardType] {
      responseDic.setValue(res, forKey: "cardType")
    }
    
    if let res = response[MposResponseParam.emvAppId] {
      responseDic.setValue(res, forKey: "emvAppId")
    }
    
    if let res = response[MposResponseParam.emvAtc] {
      responseDic.setValue(res, forKey: "emvAtc")
    }
    
    if let res = response[MposResponseParam.emvAppLabel] {
      responseDic.setValue(res, forKey: "emvAppLabel")
    }
    
    if let res = response[MposResponseParam.panSeqNumber] {
      responseDic.setValue(res, forKey: "panSeqNumber")
    }
    
    if let res = response[MposResponseParam.cvmr] {
      responseDic.setValue(res, forKey: "cvmr")
    }
    
    if let res = response[MposResponseParam.historyList] {
      let histories = res as!   [Dictionary <MposResponseParam, AnyObject>]
      var histArray: [Dictionary <String,String>] = []
      for i in 0 ..< histories.count {
        var histDic = Dictionary<String,String>()
        histDic["orderId"] = (histories[i][MposResponseParam.orderId] as! String)
        histDic["paymentMethodCode"] = (histories[i][MposResponseParam.paymentMethodCode] as! String)
        histDic["amount"] = (histories[i][MposResponseParam.amount] as! String)
        histDic["orderStatus"] = (histories[i][MposResponseParam.orderStatus] as! String)
        histDic["registeredTime"] = (histories[i][MposResponseParam.registeredTime] as! String)
        histDic["paymentTime"] = (histories[i][MposResponseParam.paymentTime] as! String)
        histArray.append(histDic)
      }
      responseDic.setValue(histArray, forKey: "historyList")
    }
    
    if let res = response[MposResponseParam.orderStatus] {
      responseDic.setValue(res, forKey: "orderStatus")
    }
    
    if let res = response[MposResponseParam.registeredTime] {
      responseDic.setValue(res, forKey: "registeredTime")
    }
    
    if let res = response[MposResponseParam.paymentTime] {
      responseDic.setValue(res, forKey: "paymentTime")
    }
    
    if let res = response[MposResponseParam.cardNumber] {
      responseDic.setValue(res, forKey: "cardNumber")
    }
    
    if let res = response[MposResponseParam.cardProviderCode] {
      responseDic.setValue(res, forKey: "cardProviderCode")
    }
    
    if let res = response[MposResponseParam.orderDescription] {
      responseDic.setValue(res, forKey: "orderDescription")
    }
    
    if let res = response[MposResponseParam.authCode] {
      responseDic.setValue(res, forKey: "authCode")
    }
    
    if let res = response[MposResponseParam.acquirerCode] {
      responseDic.setValue(res, forKey: "acquirerCode")
    }
    
    if let res = response[MposResponseParam.centerProcessId] {
      responseDic.setValue(res, forKey: "centerProcessId")
    }
    
    if let res = response[MposResponseParam.cupRequestTime] {
      responseDic.setValue(res, forKey: "cupRequestTime")
    }
    
    if let res = response[MposResponseParam.aosa] {
      responseDic.setValue(res, forKey: "aosa")
    }
    
    if let res = response[MposResponseParam.centerTradeId] {
      responseDic.setValue(res, forKey: "centerTradeId")
    }
    
    if let res = response[MposResponseParam.settledAmount] {
      responseDic.setValue(res, forKey: "settledAmount")
    }
    
    if let res = response[MposResponseParam.settledScale] {
      responseDic.setValue(res, forKey: "settledScale")
    }
    
    if let res = response[MposResponseParam.settledCurrency] {
      responseDic.setValue(res, forKey: "settledCurrency")
    }
    
    if let res = response[MposResponseParam.receiptURL] {
      responseDic.setValue(res, forKey: "receiptURL")
    }
    
    return responseDic
  }
#endif
}

