import Foundation
#if os(iOS) && !(arch(i386) || arch(x86_64))
import mPOS2SDK
#endif

class PaymentMpos: NSObject, PaymentProtocol {
    
  var paymentProperty: PaymentProperty
  let converter = MposResponseConverter()
  
  required init(property: PaymentProperty) {
    paymentProperty = property
  }

  func pay(param: NSDictionary, completion: ((NSDictionary) -> Void)?) {
#if os(iOS) && !(arch(i386) || arch(x86_64))
    var requestParams = [MposRequestParam : String]()

    guard let amount = param["amount"] else {
      completion?([:])
      return
    }

    if let orderDescription = param["orderDescription"] {
      requestParams[MposRequestParam.orderDescription] = orderDescription as? String
    }

    if let inputAmountView = param["inputAmountView"] {
      requestParams[MposRequestParam.inputAmountView] = (inputAmountView as! String)
    }

    if let completeView = param["completeView"] {
      requestParams[MposRequestParam.completeView] = (completeView as! String)
    }

    requestParams[MposRequestParam.amount] = (amount as! String)
    
    let header = makeHeader()
    DispatchQueue.main.async {
      let handler = MposHandler()
      var result = NSDictionary()
      handler.startPayment(header, requestParams: requestParams, complete: { (response) -> Void in
        result = self.converter.convertResponseToDic(response: response)
        completion?(result)
      })
    }
#else
    completion?([:])
#endif
  }
  
  func refund(param: NSDictionary, completion: ((NSDictionary) -> Void)?) {
#if os(iOS) && !(arch(i386) || arch(x86_64))
    var requestParams = [MposRequestParam : String]()
    
    guard let orderId = param["orderId"] else {
      completion?([:])
      return
    }
    
    requestParams[MposRequestParam.orderId] = (orderId as! String)
    
    let header = makeHeader()
    DispatchQueue.main.async {
      let handler = MposHandler()
      var result = NSDictionary()
      handler.requestRefund(header, requestParams: requestParams, complete: { (response) -> Void in
        result = self.converter.convertResponseToDic(response: response)
        completion?(result)
      })
    }
#else
    completion?([:])
#endif
  }
  
  
  func search(param: NSDictionary, completion: ((NSDictionary) -> Void)?) {
#if os(iOS) && !(arch(i386) || arch(x86_64))
    var requestParams = [MposRequestParam : String]()
    
    if let orderId = param["orderId"]  {
      requestParams[MposRequestParam.orderId] = (orderId as! String)
    }
    if let transactionCount = param["transactionCount"] {
      requestParams[MposRequestParam.transactionCount] = (transactionCount as! String)
    }
    if let registeredTimeFrom = param["registeredTimeFrom"] {
      requestParams[MposRequestParam.registeredTimeFrom] = (registeredTimeFrom as! String)
    }
    if let registeredTimeTo = param["registerdTimeTo"] {
      requestParams[MposRequestParam.registeredTimeTo] = (registeredTimeTo as! String)
    }

    let header = makeHeader()
    DispatchQueue.main.async {
      let handler = MposHandler()
      var result = NSDictionary()
      handler.requestSearchTransactionHistory(header, requestParams: requestParams, complete: { (response) -> Void in
        result = self.converter.convertResponseToDic(response: response)
        completion?(result)
      })
    }
#else
    completion?([:])
#endif
  }
  
  func receipt(param: NSDictionary, completion: ((NSDictionary) -> Void)?) {
#if os(iOS) && !(arch(i386) || arch(x86_64))
    var requestParams = [MposRequestParam : String]()
    
    guard let orderId = param["orderId"] else {
      completion?([:])
      return
    }
    guard let email = param["email"] else {
      completion?([:])
      return
    }
    
    requestParams[MposRequestParam.orderId] = (orderId as! String)
    requestParams[MposRequestParam.email] = (email as! String)
    if let mailType = param["mailType"] {
      requestParams[MposRequestParam.mailType] = (mailType as! String)
    }
    
    let header = makeHeader()
    DispatchQueue.main.async {
      let handler = MposHandler()
      var result = NSDictionary()
      handler.requestSendReceipt(header, requestParams: requestParams, complete: { (response) -> Void in
        result = self.converter.convertResponseToDic(response: response)
        completion?(result)
      })
    }
#else
    completion?([:])
#endif
  }
  
  func changePassword(param: NSDictionary, completion: ((NSDictionary) -> Void)?) {
#if os(iOS) && !(arch(i386) || arch(x86_64))
    var requestparams = [MposRequestParam : String]()
    
    guard let oldPass = param["oldPassword"] else {
      completion?([:])
      return
    }
    guard let newPass = param["newPassword"] else {
      completion?([:])
      return
    }
    
    requestparams[MposRequestParam.oldPasswd] = (oldPass as! String)
    requestparams[MposRequestParam.newPasswd] = (newPass as! String)
    
    let header = makeHeader()
    DispatchQueue.main.async {
      let handler = MposHandler()
      var result = NSDictionary()
      handler.requestChangePassword(header, requestParams: requestparams, complete: { (response) -> Void in
        result = self.converter.convertResponseToDic(response: response)
        completion?(result)
      })
    }
#else
    completion?([:])
#endif
  }

#if os(iOS) && !(arch(i386) || arch(x86_64))
  func makeHeader() -> [MposRequestParam : String] {
    var header = [MposRequestParam : String]()
    header[MposRequestParam.userId] = self.paymentProperty.userId
    if self.paymentProperty.userPasswrod != nil {
      header[MposRequestParam.userPassword] = self.paymentProperty.userPasswrod
    }
    if self.paymentProperty.userMerchantPassword != nil {
      header[MposRequestParam.merchantPassword] = self.paymentProperty.userMerchantPassword
    }
    header[MposRequestParam.readerType] = self.paymentProperty.readerType
    header[MposRequestParam.locale] = self.paymentProperty.locale
    
    return header
  }
#endif
}
