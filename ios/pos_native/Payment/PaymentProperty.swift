import Foundation

class PaymentProperty {
  let ADDRESS = "ADDRESS"
  let PORT = "PORT"
  let TIMEOUT_SEC = "TIMEOUT_SEC"
  let BRIDGE = "BRIDGE"
  
  var address: String = ""
  var port: Int = 9999
  var timeout_sec: Double = 0
  var bridge: PaymentBridge
  
  init(dict: [String : Any]) {
    let propertyDict = dict 
    
    address = propertyDict[ADDRESS] as! String
    port = propertyDict[PORT] as! Int
    timeout_sec = Double(propertyDict[TIMEOUT_SEC] as! Int)
    bridge = propertyDict[BRIDGE] as! PaymentBridge
  }
  
}
