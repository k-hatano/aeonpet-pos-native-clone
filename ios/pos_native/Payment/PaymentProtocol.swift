import Foundation

protocol PaymentProtocol {
  
  init(property: PaymentProperty)
  
  func connect()
  func send(param: NSDictionary)
  func isStreamOpen() -> Bool

}
