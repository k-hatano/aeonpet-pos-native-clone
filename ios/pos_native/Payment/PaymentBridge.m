#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(PaymentBridge, RCTEventEmitter<RCTBridgeModule>)

RCT_EXTERN_METHOD(setPaymentProperty:(NSDictionary *)dict)
RCT_EXTERN_METHOD(send:(NSDictionary *)param)
RCT_EXTERN_METHOD(isOpen:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(sendInterrupt:(NSDictionary *)param)
RCT_EXTERN_METHOD(getEvents:(RCTResponseSenderBlock)callback)

@end
