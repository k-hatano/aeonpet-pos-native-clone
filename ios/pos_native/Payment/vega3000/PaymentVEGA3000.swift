import UIKit

class PaymentVEGA3000: NSObject, StreamDelegate, PaymentProtocol {
  let REQUEST = "REQUEST"
  
  var paymentProperty: PaymentProperty

  private var inputStream : InputStream?
  private var outputStream: OutputStream?
  private var isOpen: Bool = false
  private var hasRecived: Bool = false
  private var requestTelegram: String?
  
  required init(property: PaymentProperty) {
    paymentProperty = property
    super.init()
  }
  
  func send(param: NSDictionary) {
    requestTelegram = param[REQUEST] as? String
    sendData(requestString: requestTelegram!)
  }
  
  func close() {
    guard let inputStream = inputStream, let outputStream = outputStream else { return }
    
    inputStream.delegate = nil
    outputStream.delegate = nil
    
    inputStream.close()
    outputStream.close()
    
    inputStream.remove(from: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
    outputStream.remove(from: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
  }
  
  func connect(){
    Stream.getStreamsToHost(withName: paymentProperty.address, port: paymentProperty.port, inputStream: &inputStream, outputStream: &outputStream)
    
    guard let inputStream = inputStream, let outputStream = outputStream else { return }

    inputStream.delegate = self
    outputStream.delegate = self
    
    inputStream.schedule(in: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
    outputStream.schedule(in: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
    
    inputStream.open()
    outputStream.open()
    
    // 実機の場合js側のsetTimeoutが正常動作しないため、swift側でタイムアウト処理を行う
    DispatchQueue.global().asyncAfter(deadline: .now() + paymentProperty.timeout_sec) {
      if !self.isOpen {
        self.close()
        self.paymentProperty.bridge.error()
      }
    }
  }
  
  func isStreamOpen() -> Bool {
    guard let inputStream = inputStream, let outputStream = outputStream else { return false }
    return inputStream.streamStatus == Stream.Status.open && outputStream.streamStatus == Stream.Status.open
  }
  
  func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
    switch eventCode {
    case Stream.Event.openCompleted:
      if aStream == outputStream {
        isOpen = true
        paymentProperty.bridge.open()
      }
      return
    case Stream.Event.hasBytesAvailable:
      guard let result = readStream() else { return }
      hasRecived = true
      if isInterrupt(resultString: result) {
        paymentProperty.bridge.reciveInterrupt(data: result)
        return
      }
      paymentProperty.bridge.recive(data: result)
      return
    case Stream.Event.errorOccurred:
      paymentProperty.bridge.error()
      return
    case Stream.Event.endEncountered:
      close()
      
      // 実機ではopenした直後にcloseしてしまう場合があるため
      // close発火後決済端末から応答が確認できなかった場合、リトライする
      if isOpen && !hasRecived {
        if let requestString = requestTelegram {
          sendData(requestString: requestString)
          return
        }
      }
      paymentProperty.bridge.close()
      return
    default:
      return
    }
  }
  
  private func sendData(requestString: String) {
    guard let inputStream = inputStream, let outputStream = outputStream else { return }

    if inputStream.streamStatus == Stream.Status.closed || outputStream.streamStatus == Stream.Status.closed {
      connect()
    }
    
    DispatchQueue.global().async {
      guard let outputStream = self.outputStream else { return }

      var requestData = requestString.data(using: String.Encoding.shiftJIS, allowLossyConversion: false)!
      let _ = requestData.withUnsafeBytes {
        outputStream.write($0, maxLength: requestData.count)
      }
    }
  }
  
  private func readStream() -> String? {
    guard let inputStream = inputStream else { return nil }

    let bufferSize = 512
    var buffer = Array<UInt8>(repeating: 0, count: bufferSize)
    if (inputStream.read(&buffer, maxLength: bufferSize) > 0) {
      do {
        return try String(bytes: buffer, encoding: String.Encoding.shiftJIS)
      } catch {
        return nil
      }
    }
    return nil
  }
  
  private func isInterrupt(resultString: String)-> Bool {
    let from = resultString.index(resultString.startIndex, offsetBy: 3)
    let to = resultString.index(from, offsetBy: 2)
    let apiType = resultString[from...to]
    return apiType == PaymentStruct.apiType.interrupt.rawValue
  }
}
