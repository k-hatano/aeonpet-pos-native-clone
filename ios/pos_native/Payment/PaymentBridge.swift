import Foundation

@objc(PaymentBridge)
class PaymentBridge: RCTEventEmitter {
  
  var paymentProtocol: PaymentProtocol?
  
  @objc(setPaymentProperty:)
  func setPaymentProperty(dict: NSDictionary) {
    var paymentPropertyDict = dict as! [String : Any]
    paymentPropertyDict["BRIDGE"] = self
    
    let paymentProperty = PaymentProperty(dict: paymentPropertyDict)
    paymentProtocol = PaymentVEGA3000(property: paymentProperty)
  }
  
  @objc(isOpen:)
  func isOpen(callback: @escaping RCTResponseSenderBlock) {
    guard let paymentProtocol = paymentProtocol else {
      callback([false])
      return
    }
    callback([paymentProtocol.isStreamOpen()])
  }
  
  @objc(sendInterrupt:)
  func sendInterrupt(param: NSDictionary) {
    guard let paymentProtocol = paymentProtocol else { return }
    paymentProtocol.send(param: param)
  }
  
  @objc(send:)
  func send(param: NSDictionary) {
    guard let paymentProtocol = paymentProtocol else { return }
    paymentProtocol.connect()
    paymentProtocol.send(param: param)
  }
  
  @objc(getEvents:)
  func getEvents(callback: @escaping RCTResponseSenderBlock) {
    let events = [
      "OPEN": PaymentStruct.event(code: .open),
      "CLOSE": PaymentStruct.event(code: .close),
      "RECIVE": PaymentStruct.event(code: .recive),
      "ERROR": PaymentStruct.event(code: .networkError),
      "INTERRUPT": PaymentStruct.event(code: .interrupt)
      ]
    callback([events])
  }
  
  func open() {
    sendEvent(withName: PaymentStruct.event(code: .open), body: PaymentStruct.message(code: .open))
  }
  
  func close() {
    sendEvent(withName: PaymentStruct.event(code: .close), body: PaymentStruct.message(code: .close))
  }
  
  func recive(data: String) {
    sendEvent(withName: PaymentStruct.event(code: .recive), body: data)
  }
  
  func reciveInterrupt(data: String) {
    sendEvent(withName: PaymentStruct.event(code: .interrupt), body: data)
  }
  
  func error() {
    sendEvent(withName: PaymentStruct.event(code: .networkError), body: PaymentStruct.message(code: .networkError))
  }
  
  override func supportedEvents() -> [String]! {
    return [
      PaymentStruct.event(code: .open),
      PaymentStruct.event(code: .close),
      PaymentStruct.event(code: .recive),
      PaymentStruct.event(code: .networkError),
      PaymentStruct.event(code: .interrupt)
    ]
  }
}
