import UIKit

struct PaymentStruct {
  enum Code {
    case open
    case close
    case recive
    case networkError
    case interrupt
  }
  
  enum errorCode: Int {
    case notOpen = -1
  }
  
  enum apiType: String {
    case interrupt = "999"
  }
  
  static func message(code: Code) -> String {
    switch code {
    case .open:
      return "Connection Opend"
    case .close:
      return "Connection Closed"
    case .recive:
      return "Response Recived"
    case .networkError:
      return "Network Error"
    case .interrupt:
      return "payment interrupt"
    }
  }
  
  static func event(code: Code) -> String {
    switch code {
    case .open:
      return "onOpen"
    case .close:
      return "onClose"
    case .recive:
      return "onRecive"
    case .networkError:
      return "onError"
    case .interrupt:
      return "onInterrupt"
    }
  }
}
