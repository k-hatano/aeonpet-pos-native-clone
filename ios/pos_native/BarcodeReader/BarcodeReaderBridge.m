#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(BarcodeReaderBridge, NSObject)

RCT_EXTERN_METHOD(setBarcodeReaderProperty:(NSDictionary *)barcodeReaderPropertyDic)
RCT_EXTERN_METHOD(sendData: (NSString *)eventName data(NSData *)data)

@end
