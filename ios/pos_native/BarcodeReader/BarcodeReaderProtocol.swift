import Foundation

protocol BarcodeReaderProtocol {
  
  func setup()
  func open()
  
  var barcodeReaderBridge: BarcodeReaderBridge { get }
}

