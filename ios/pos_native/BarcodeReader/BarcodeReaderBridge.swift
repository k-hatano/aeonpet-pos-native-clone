import Foundation


@objc(BarcodeReaderBridge)
class BarcodeReaderBridge: NSObject {
  
  var barcodeReaderProtocol: BarcodeReaderProtocol?
  var bridge: RCTBridge!
  
  @objc(setBarcodeReaderProperty:)
  func setBarcodeReaderProperty(barcodeReaderDic: NSDictionary) {
    var _barcodeReaderPropertyDic = barcodeReaderDic as! [String : Any]
    _barcodeReaderPropertyDic["bridge"] = self
    let barcodeReaderProperty = BarcodeReaderProperty(dict: _barcodeReaderPropertyDic)
    
    switch barcodeReaderProperty.barcodeReaderSdk {
    case .OPN3200Si:
      barcodeReaderProtocol = BarcodeReaderOPN3200Si(property: barcodeReaderProperty)
      barcodeReaderProtocol?.setup()
    default: break
    }
  }

  @objc(sendData:data:)
  func sendData(eventName: String,data: String) {
    self.bridge.eventDispatcher().sendAppEvent( withName: eventName, body: data)
  }
}
