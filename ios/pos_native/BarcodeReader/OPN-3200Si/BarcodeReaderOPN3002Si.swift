import Foundation

class BarcodeReaderOPN3200Si: NSObject,OPN2002iBluetoothServiceDelegate,BarcodeReaderProtocol {
  
  var service: OPN2002iBluetoothService?
  var barcodeReaderProperty: BarcodeReaderProperty?
  
  var barcodeReaderBridge: BarcodeReaderBridge
  
  required init (property: BarcodeReaderProperty) {
    barcodeReaderProperty = property
    barcodeReaderBridge = (barcodeReaderProperty?.bridge)!
  }
  
  func setup() {
    NotificationCenter.default.addObserver(self, selector: #selector(accessoryDidConnected(notification:)), name: Notification.Name.EAAccessoryDidConnect, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(accessoryDidDisconnected(notification:)), name: Notification.Name.EAAccessoryDidDisconnect, object: nil)
    
    service = OPN2002iBluetoothService.sharedController()
    service?.delegate = self
    service?.setEnableReadBarcode(OPN2002I_READBORCODE_ENABLE)
    
    EAAccessoryManager.shared().registerForLocalNotifications()
  }
  
  func accessoryDidDisconnected(notification: Notification) {
    service?.closeSession()
  }
  
  func accessoryDidConnected(notification: Notification) {
    let sessionController = OPN2002iBluetoothService.sharedController()
    if sessionController?.accessory == nil || !(sessionController?.accessory.isConnected)! {
      open()
    }
  }
  
  func bluetoothService(_ service: OPN2002iBluetoothService!, didGetBarcode data: Data!) {
    let dataStr = String(data: data, encoding: String.Encoding.utf8)!
    barcodeReaderBridge.sendData(eventName: "barcode", data: dataStr)
  }
  
  func open() {
    let accessoryList = EAAccessoryManager.shared().connectedAccessories
    for accessory in accessoryList {
      if accessory.protocolStrings[0] == "jp.opto.opnprotocol" {
        service?.setupController(for: accessory, withProtocolString: accessory.protocolStrings[0])
        service?.openSession()
      }
    }
  }
}
