#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

#import "CrashReporter/CrashReporter.h"

@interface RCT_EXTERN_MODULE(CrashReportBridge, NSObject)

RCT_EXTERN_METHOD(forceCrash)
RCT_EXTERN_METHOD(getRreportFileNames:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getEpsonLog:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getRreport:(NSString *)name callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(removeReport:(NSString *)name)

@end
