import Foundation

@objc(CrashReportBridge)
class CrashReportBridge: NSObject {
  @objc
  func forceCrash() {
    var a: String? = nil
    a!.append("")
  }
  
  @objc(getRreportFileNames:)
  func getRreportFileNames(callback: @escaping RCTResponseSenderBlock) {
    callback([CrashReportManager.getReportFileNames()])
  }
  
  @objc(getRreport:callback:)
  func getRreport(name: String, callback: @escaping RCTResponseSenderBlock) {
    let report = CrashReportManager.getReport(name: name)
    callback(report == nil ? [] : [report!])
  }
  
  @objc(getEpsonLog:)
  func getRreport(callback: @escaping RCTResponseSenderBlock) {
    let epsonLog = CrashReportManager.getEpsonLog()
    callback(epsonLog == nil ? [] : [epsonLog!])
  }
  
  @objc(removeReport:)
  func removeReport(name: String) {
    CrashReportManager.removeReport(name: name)
  }
}
