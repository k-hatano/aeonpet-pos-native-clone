import Foundation

@objc(CrashReportManager)
class CrashReportManager: NSObject {
  @objc
  public static func initReport() {
    guard let reporter = PLCrashReporter.shared() else {
      return
    }
    
    if reporter.hasPendingCrashReport() {
      saveReportToDocuments(reporter: reporter)
    }
    
    reporter.enable()
  }
  
  public static func getReportFileNames() -> [String] {
    let url = crashLogDirPath().relativePath
    do {
      return try FileManager.default.contentsOfDirectory(atPath: url)
    } catch {
      return []
    }
  }
  
  public static func getReport(name: String) -> String? {
    let crashLogFilePath = crashLogDirPath().appendingPathComponent(name)
    return try? String(contentsOf: crashLogFilePath, encoding: .utf8)
  }
  
  public static func removeReport(name: String) {
    let crashLogFilePath = crashLogDirPath().appendingPathComponent(name)
    try? FileManager.default.removeItem(at: crashLogFilePath)
  }
  
  public static func getEpsonLog() -> String? {
    let documentPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let logFilePath = documentPath.appendingPathComponent("Epos2Log").appendingPathComponent("Epos2Log.txt")
    guard let logText = try? String(contentsOf: logFilePath, encoding: .utf8) else {
      return nil
    }
    let logLines = logText.components(separatedBy: "\n");
    return logLines.suffix(1000).joined(separator: "\n");
  }
  
  private static func saveReportToDocuments(reporter: PLCrashReporter) {
    guard let reportData = reporter.loadPendingCrashReportData() else {
      return
    }
    guard let report = try? PLCrashReport.init(data: reportData) else {
      return
    }
    guard let reportText = PLCrashReportTextFormatter.stringValue(for: report, with: PLCrashReportTextFormatiOS) else {
      return
    }
    
    let crashLogDir = crashLogDirPath()
    if !FileManager.default.fileExists(atPath: crashLogDir.absoluteString) {
      do {
        try FileManager.default.createDirectory(at: crashLogDir, withIntermediateDirectories: true)
      } catch {
        return
      }
    }
    
    let crashLogFilePath = crashLogDir.appendingPathComponent(formatTimestamp(date: report.systemInfo.timestamp))
    if !FileManager.default.fileExists(atPath: crashLogFilePath.absoluteString) {
      do {
        try reportText.write(to: crashLogFilePath, atomically: true, encoding: .utf8)
        reporter.purgePendingCrashReport()
      } catch {
      }
    } else {
      reporter.purgePendingCrashReport()
    }
  }
  
  private static func formatTimestamp(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyyMMdd_HHmm"
    return dateFormatter.string(from: date);
  }
  
  private static func crashLogDirPath() -> URL {
    let libraryUrl = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
    return libraryUrl.appendingPathComponent("crashLogs")
  }
}
