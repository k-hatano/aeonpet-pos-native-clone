import Foundation

class TBGBTDisplay: NSObject,DisplayProtocol {
  
  var displayProperty: DisplayProperty
  var session: TBGBTDisplayDeviceSessionT?
  
  required init(property: DisplayProperty) {
    displayProperty = property
  }
  
  func connectToDevice() {
    setupLibrary()
    
    if connectToAccessoryDevice() {
      initAccessoryDevice()
    }
  }
  
  func sessionExists() -> Bool {
     return self.session != nil
  }
  
  func setupLibrary() {
    TBGBTDisplayController.shared().deviceDidConnect = { (device: TBGBTDisplayDevice) in
      
      print("Device did connected")
      print(device)
      
      DispatchQueue.main.async {
        if self.session != nil {
          self.session?.close()
          self.session = nil
        }
        self.connectToAccessoryDevice()
      }
    }
    TBGBTDisplayController.shared().deviceDidDisconnect = { (device: TBGBTDisplayDevice) in
      print("Device did disconnect")
      
      DispatchQueue.main.async {
        if (self.session != nil && (self.session?.device.isEqual(device))!) {
          self.session?.close()
          self.session = nil
        }
      }
    }
  }
  
  
  func connectToAccessoryDevice() -> Bool {
    var devices = [TBGBTDisplayDevice]()
    DispatchQueue.main.async {
      devices = TBGBTDisplayController.shared().getDevices()
    }
    print(devices.count)
    for device in devices {
      
      var error: Error?
      DispatchQueue.main.async {
        do {
          let session = try TBGBTDisplayController.shared().createSession(with: device)
          self.session = session
        } catch let err {
          error = err
        }
      }
      
      self.session?.onError = { (error: Error) in
        print(error)
      }
      
      DispatchQueue.main.async {
        self.session?.open()
      }
      return true
    }
    return false
  }
  
  func disconnetToAccessoryDevice() {
    DispatchQueue.main.async {
      if self.session != nil {
        self.session?.close()
        self.session = nil
      }
    }
  }
  
  func initAccessoryDevice() {
    DispatchQueue.main.async {
      self.session?.setLcdContrast(UInt8(3))
      self.session?.setCursorMode(.off)
      self.session?.setCountryCode(0x08)
      self.session?.setCountryCode(0x3a)
      self.session?.clearScreen()
    }
  }
  
  func moveCursorToHome() {
    DispatchQueue.main.async {
      self.session?.moveCursorToHome()
      // self.session?.flush()
    }
  }
  
  func moveCursorToLeft() {
    DispatchQueue.main.async {
      self.session?.moveCursorToLeft()
    }
  }
  
  func moveCursorDown() {
    DispatchQueue.main.async {
      self.session?.moveCursorDown()
    }
  }
  
  func moveCursorToRight() {
    DispatchQueue.main.async {
      self.session?.moveCursorToRight()
    }
  }
  
  func writeText(text: String) {
    DispatchQueue.main.async {
      self.session?.writeText(text)
      // self.session?.flush()
    }
  }
  
  func setDisplayOn() {
    DispatchQueue.main.async {
      self.session?.setDisplay(true)
    }
  }
  
  func setDisplayOff() {
    DispatchQueue.main.async {
      self.session?.setDisplay(false)
    }
  }
  
  func setCursorModeOff() {
    DispatchQueue.main.async {
      self.session?.setCursorMode(.off)
    }
  }
  
  func setCursorModeOn() {
    DispatchQueue.main.async {
      self.session?.setCursorMode(.on)
    }
  }
  
  func setCursorModeBlink() {
    DispatchQueue.main.async {
      self.session?.setCursorMode(.blink)
    }
  }
  
  func clearLine() {
    DispatchQueue.main.async {
      self.session?.clearLine()
    }
  }
  
  func clearScreen() {
    DispatchQueue.main.async {
      self.session?.clearScreen()
      // self.session?.flush()
    }
  }
  
  func textAlignmentLeft(text: String,column: Int) {
    guard text.characters.count <= 20 else { return }
    guard column == 1 || column == 0 else { return }
    setCursorPosition(x: 0, y: column)
    writeText(text: text)
  }
  
  func textAlignmentRight(text: String,column: Int) {
    let characters = getMultiBytesTextLength(text: text)
    guard characters <= 20 else { return }
    guard column == 1 || column == 0 else { return }
    setCursorPosition(x: 20 - characters, y: column)
    writeText(text: text)
  }
  
  
  func textAlignmentCenter(text: String,column: Int) {
    let characters = getMultiBytesTextLength(text: text)
    guard characters <= 20 else { return }
    
    guard column == 1 || column == 0 else { return }
    setCursorPosition(x: (20 - characters) / 2, y: column)
    writeText(text: text)
  }
  
  
  func setCursorPosition(x: Int, y: Int) {
    guard 0 <= x && x <= 19 && 0 <= y && y <= 1 else { return }
    DispatchQueue.main.async {
      self.session?.moveCursorTo(x: UInt8(x), andY: UInt8(y))
    }
  }
  
  func setLcdContrast(brightness: Int) {
    DispatchQueue.main.async {
      if (0 <= brightness && brightness <= 6) {
        self.session?.setLcdContrast(UInt8(brightness))
      }
    }
  }
  
  func getMultiBytesTextLength(text: String) -> Int {
    
    let sjis = CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.shiftJIS.rawValue))
    let data = text.data(using: String.Encoding(rawValue: sjis))
    
    if data == nil {
      return -1
    }
    
    return (data?.count)!
  }
}
