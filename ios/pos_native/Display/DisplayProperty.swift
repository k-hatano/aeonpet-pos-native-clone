import Foundation

enum DisplaySDK: Int {
  case TBGBTDisplay
}

enum DisplayStd: Int {
  case IP
  case BLUETOOTH
}

class DisplayProperty {
  let DISPLAY_SDK = "DISPLAY_SDK"
  let DISPLAY_IP = "DISPLAY_IP"
  let DISPLAY_TIMEOUT_MILI = "DISPLAY_TIMEOUT_MILI"
  
  var displaySDK: DisplaySDK
  var displayIP: String = ""
  var displayTimeoutMili: Int = 10000

  init(dict: [String: Any]) {
    displaySDK = DisplaySDK(rawValue: (dict[DISPLAY_SDK] as? Int)!)!
    displayIP = dict[DISPLAY_IP]! as! String
    let timeoutMili = dict[DISPLAY_TIMEOUT_MILI] as? Int
    if timeoutMili != nil {
      displayTimeoutMili = timeoutMili!
    }
  }
}
