import Foundation
import MediaPlayer

@objc(DisplayBridge)
class DisplayBridge: NSObject {
  
  var customerDisplayProtocol: CustomerDisplayProtocol?
  
  @objc(setCustomerDisplayProperty:)
  func setCustomerDisplayProperty(customerDisplayPropertyDic: NSDictionary) {
    var _customerDisplayPropertyDic = customerDisplayPropertyDic as! [String : Any]
    _customerDisplayPropertyDic["BRIDGE"] = self
    let customerDisplayProperty = CustomerDisplayProperty(dict: _customerDisplayPropertyDic)
    customerDisplayProtocol = CustomerDisplayEpson(property: customerDisplayProperty)
  }
  
  @objc(connectCustomerDisplay:)
  func connectCustomerDisplay(callback: @escaping RCTResponseSenderBlock) {
    let result = customerDisplayProtocol?.initializeObject()
    if !PrinterResult.isSuccess(result: result!) {
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
      return
    }
    customerDisplayProtocol?.connectDisplay(completion: { (result) -> Void in
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result)])
    })
  }
  
  @objc(displayMessage:bottomMessage:callback:)
  func displayMessage(topMessage: String, bottomMessage: String, callback: @escaping RCTResponseSenderBlock) {
    if customerDisplayProtocol != nil {
      var result = customerDisplayProtocol?.createDisplayData(
        topMessage: topMessage, bottomMessage: bottomMessage)
      if !PrinterResult.isSuccess(result: result!) {
        let disconnectResult = customerDisplayProtocol?.disconnectDisplay()
        if !PrinterResult.isSuccess(result: disconnectResult!) {
          result = disconnectResult
        }
        callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
        return
      }
      result = customerDisplayProtocol?.sendData()
      if !PrinterResult.isSuccess(result: result!) {
        let disconnectResult = customerDisplayProtocol?.disconnectDisplay()
        if !PrinterResult.isSuccess(result: disconnectResult!) {
          result = disconnectResult
        }
      }
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
    } else {
      callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: PrinterResultCode.ERR_CONNECT)])
    }
  }
  
  @objc(disconnectCustomerDisplay:)
  func disconnectCustomerDisplay(callback: @escaping RCTResponseSenderBlock) {
    let result = customerDisplayProtocol?.disconnectDisplay()
    callback([NSNull(), PrinterResult.printerResultToCallbackObj(result: result!)])
  }
}
