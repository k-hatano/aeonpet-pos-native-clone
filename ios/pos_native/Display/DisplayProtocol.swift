import Foundation

protocol DisplayProtocol {
  
  var displayProperty: DisplayProperty { get set }
  
  init(property: DisplayProperty)
  
  func connectToDevice()
  
  func sessionExists() -> Bool
  
  func moveCursorToHome()
  func moveCursorToLeft()
  func moveCursorDown()
  func moveCursorToRight()
  func writeText(text: String)
  func setDisplayOn()
  func setDisplayOff()
  func setCursorModeOff()
  func setCursorModeOn()
  func setCursorModeBlink()
  func clearLine()
  func clearScreen()
  func textAlignmentLeft(text: String,column: Int)
  func textAlignmentRight(text: String,column: Int)
  func textAlignmentCenter(text: String,column: Int)
  func setCursorPosition(x: Int, y: Int)
  func setLcdContrast(brightness: Int)
  func disconnetToAccessoryDevice()
}
