
import Foundation

enum CustomerDisplayStd: Int {
    case IP
    case BLUETOOTH
}

class CustomerDisplayProperty {
    let CUSTOMER_DISPLAY_STD             = "CUSTOMER_DISPLAY_STD"
    let CUSTOMER_DISPLAY_IP              = "CUSTOMER_DISPLAY_IP"
    let CUSTOMER_DISPLAY_TIMEOUT_MILI    = "CUSTOMER_DISPLAY_TIMEOUT"
    let BRIDGE                           = "BRIDGE"
    
    var customerDisplayStd: CustomerDisplayStd
    var customerDisplayIP: String = ""
    var customerDisplayTimeoutMili: Int = 10000
    
    var bridge: DisplayBridge
    
    init(dict: [String: Any]) {
        customerDisplayStd = CustomerDisplayStd(rawValue: (dict[CUSTOMER_DISPLAY_STD] as? Int)!)!
        customerDisplayIP = dict[CUSTOMER_DISPLAY_IP] as? String ?? ""
        bridge = dict[BRIDGE] as! DisplayBridge
        let timeoutMili = dict[CUSTOMER_DISPLAY_TIMEOUT_MILI] as? Int
        if timeoutMili != nil {
            customerDisplayTimeoutMili = timeoutMili!
        }
    }
}
