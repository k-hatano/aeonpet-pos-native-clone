import Foundation

let CONTENT = "content"
let TEXT = "text"
let ALIGNMENT = "alignment"
let COLUMN = "column"

let LEFT = "left"
let RIGHT = "right"
let CENTER = "center"
