
import Foundation

protocol CustomerDisplayProtocol {
  var customerDisplayProperty: CustomerDisplayProperty { get set }
  
  init (property: CustomerDisplayProperty)
  
  func initializeObject() -> PrinterResultCode
  func createDisplayData(topMessage: String, bottomMessage: String) -> PrinterResultCode
  func connectDisplay(completion: ((PrinterResultCode) -> Void)?)
  func sendData() -> PrinterResultCode
  func disconnectDisplay() ->PrinterResultCode
  func finalizeObject() -> Void
  func getStatus() -> Epos2DisplayStatusInfo?
}
