#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(DisplayBridge, NSObject)

RCT_EXTERN_METHOD(setCustomerDisplayProperty:(NSDictionary *)customerDisplayPropertyDic)
RCT_EXTERN_METHOD(connectCustomerDisplay:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(displayMessage:(NSString *)topMessage bottomMessage:(NSString *)bottomMessage callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(disconnectCustomerDisplay:(RCTResponseSenderBlock)callback)

@end
