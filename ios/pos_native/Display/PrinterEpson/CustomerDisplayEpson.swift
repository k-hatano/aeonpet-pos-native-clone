
import Foundation

class CustomerDisplayEpson: NSObject, CustomerDisplayProtocol, Epos2DiscoveryDelegate, Epos2DispReceiveDelegate {
  
  var customerDisplayProperty: CustomerDisplayProperty
  private var filterOption: Epos2FilterOption = Epos2FilterOption()
  
  var targetDisplay: String!
  var display: Epos2LineDisplay?
  var displayModel: Epos2DisplayModel = EPOS2_DM_D30
  var completionEnd: ((PrinterResultCode) -> Void)?
  
  private let TCP_HEADER = "TCP:"
  private let BT_HEADER = "BT:"
  private let SUFFIX = "[local_display]"
  
  required init (property: CustomerDisplayProperty) {
    customerDisplayProperty = property
    filterOption.portType = EPOS2_PORTTYPE_BLUETOOTH.rawValue
    filterOption.deviceType = EPOS2_TYPE_PRINTER.rawValue
  }
  
  func onDispReceive(_ displayObj: Epos2LineDisplay!, code: Int32) {
  }
  
  func onDiscovery(_ deviceInfo: Epos2DeviceInfo!) {
    if deviceInfo.deviceType == EPOS2_TYPE_PRINTER.rawValue && deviceInfo.bdAddress != "" {
      targetDisplay = BT_HEADER + deviceInfo.bdAddress
      Epos2Discovery.stop()
    }
    
    let connectResult = display!.connect(
      targetDisplay,
      timeout: customerDisplayProperty.customerDisplayTimeoutMili)
    let result = convertErrorStatus(epsonError: connectResult)
    if (!PrinterResult.isSuccess(result: result)) {
      finalizeObject()
    }
    
    completionEnd!(result)
    completionEnd = nil
    return
  }
  
  func initializeObject() -> PrinterResultCode {
    display = Epos2LineDisplay(displayModel: displayModel.rawValue)
    if display == nil {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }
    
    return PrinterResultCode.SUCCESS
  }
  
  func createDisplayData(topMessage: String, bottomMessage: String) -> PrinterResultCode {
    var result = EPOS2_SUCCESS
    if display == nil {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }
    
    result = Epos2ErrorStatus(rawValue: (display!.addInitialize()))
    if result != EPOS2_SUCCESS {
      return convertErrorStatus(epsonError: result.rawValue)
    }
    
    result = Epos2ErrorStatus(rawValue: ((display!.addSetCursorPosition(1, y: 1))))
    if result != EPOS2_SUCCESS {
      return convertErrorStatus(epsonError: result.rawValue)
    }
    
    result = Epos2ErrorStatus(rawValue: (display!.addText(topMessage, x: 1, y: 1, lang: EPOS2_LANG_JA.rawValue)))
    if result != EPOS2_SUCCESS {
      return convertErrorStatus(epsonError: result.rawValue)
    }
    result = Epos2ErrorStatus(rawValue: (display!.addText(bottomMessage, x: 1, y: 2, lang: EPOS2_LANG_JA.rawValue)))
    if result != EPOS2_SUCCESS {
      return convertErrorStatus(epsonError: result.rawValue)
    }
    
    return PrinterResultCode.SUCCESS
  }
  
  func connectDisplay(completion: ((PrinterResultCode) -> Void)?) {
    completionEnd = completion
    
    if display == nil {
        completionEnd!(PrinterResultCode.ERR_CODE_BEHAVIOR)
        completionEnd = nil
        return
    }
    var result: PrinterResultCode
    switch (customerDisplayProperty.customerDisplayStd) {
    case CustomerDisplayStd.IP:
      if !customerDisplayProperty.customerDisplayIP.isEmpty {
        targetDisplay = TCP_HEADER + customerDisplayProperty.customerDisplayIP + SUFFIX
        result = PrinterResultCode.SUCCESS
        let connectResult = display!.connect(
          targetDisplay,
          timeout: customerDisplayProperty.customerDisplayTimeoutMili)
        result = convertErrorStatus(epsonError: connectResult)
      } else {
        result = PrinterResultCode.ERR_PARAM
      }
      if (result != PrinterResultCode.SUCCESS) {
        finalizeObject()
      }
      completionEnd!(result)
      completionEnd = nil
      return
    case CustomerDisplayStd.BLUETOOTH:
      let btResult = Epos2Discovery.start(filterOption, delegate:self)
      result = convertBluetoothError(btError: btResult)
      if !PrinterResult.isSuccess(result: result) {
        Epos2Discovery.stop()
      }
      return
    }
  }
  
  func sendData () -> PrinterResultCode {
    var result = EPOS2_SUCCESS
    result = Epos2ErrorStatus(rawValue: (display!.sendData()))
    if result != EPOS2_SUCCESS {
      return convertErrorStatus(epsonError: result.rawValue)
    }
    display!.clearCommandBuffer()
    return PrinterResultCode.SUCCESS
  }
  
  func finalizeObject() -> Void {
    if display == nil {
      return
    }
    
    display?.clearCommandBuffer()
    display = nil
  }
  
  func disconnectDisplay () -> PrinterResultCode {
    var result: Int32 = EPOS2_SUCCESS.rawValue
    
    if (display == nil) {
      return PrinterResultCode.ERR_CODE_BEHAVIOR
    }
    
    result = display!.disconnect()
    if (result != EPOS2_SUCCESS.rawValue) {
      if #available(iOS 8.0, *) {
        return convertErrorStatus(epsonError: result)
      } else {
        return PrinterResultCode.ERR_IOS_VERSION
      }
    }
    
    finalizeObject()
    return PrinterResultCode.SUCCESS
  }
  
  func convertErrorStatus(epsonError: Int32) -> PrinterResultCode {
    switch (epsonError) {
    case EPOS2_SUCCESS.rawValue:
      return PrinterResultCode.SUCCESS
    case EPOS2_ERR_PARAM.rawValue:
      return PrinterResultCode.ERR_PARAM
    case EPOS2_ERR_CONNECT.rawValue:
      return PrinterResultCode.ERR_CONNECT
    case EPOS2_ERR_TIMEOUT.rawValue:
      return PrinterResultCode.ERR_TIMEOUT
    case EPOS2_ERR_ILLEGAL.rawValue:
      return PrinterResultCode.ERR_ILLEGAL
    case EPOS2_ERR_MEMORY.rawValue:
      return PrinterResultCode.ERR_MEMORY
    case EPOS2_ERR_FAILURE.rawValue:
      return PrinterResultCode.ERR_UNKNOWN
    case EPOS2_ERR_PROCESSING.rawValue:
      return PrinterResultCode.ERR_PROCESSING
    case EPOS2_ERR_NOT_FOUND.rawValue:
      return PrinterResultCode.ERR_NOT_FOUND
    case EPOS2_ERR_IN_USE.rawValue:
      return PrinterResultCode.ERR_IN_USE
    case EPOS2_ERR_TYPE_INVALID.rawValue:
      return PrinterResultCode.ERR_TYPE_INVALID
    case EPOS2_ERR_DISCONNECT.rawValue:
      return PrinterResultCode.ERR_DISCONNECT
    default:
      return PrinterResultCode.ERR_UNKNOWN
    }
  }
  
  func convertBluetoothError(btError: Int32) -> PrinterResultCode {
    var error: PrinterResultCode
    switch (btError) {
    case EPOS2_BT_SUCCESS.rawValue:
      error = PrinterResultCode.SUCCESS
    case EPOS2_BT_ERR_PARAM.rawValue:
      error = PrinterResultCode.ERR_PARAM
    case EPOS2_BT_ERR_UNSUPPORTED.rawValue:
      error = PrinterResultCode.ERR_UNSUPPORTED
    case EPOS2_BT_ERR_CANCEL.rawValue:
      error = PrinterResultCode.ERR_CANCEL
    case EPOS2_BT_ERR_ALREADY_CONNECT.rawValue:
      error = PrinterResultCode.ERR_ALREADY_CONNECT
    case EPOS2_BT_ERR_ILLEGAL_DEVICE.rawValue:
      error = PrinterResultCode.ERR_ILLEGAL_DEVICE
    case EPOS2_BT_ERR_FAILURE.rawValue:
      error = PrinterResultCode.ERR_UNKNOWN
    default:
      error = PrinterResultCode.ERR_UNKNOWN
    }
    return error
  }
  
  func getStatus() -> Epos2DisplayStatusInfo? {
    return display?.getStatus()
  }
  
}
