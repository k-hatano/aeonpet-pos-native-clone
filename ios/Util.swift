//
//  Util.swift
//  Orange POS
//
//  Created by teppei.kikuchi on 7/5/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

final class Util {
  static func dump<T>(_ value: T, name: String? = nil, indent: Int = 0, maxDepth: Int = Int.max, maxItems: Int = Int.max) {
    #if DEBUG
    Swift.dump(value, name: name, indent: indent, maxDepth: maxDepth, maxItems: maxItems)
    #endif
  }
}
