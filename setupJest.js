jest.mock('react-native')
jest.mock('react-native-filesystem-v1')
jest.mock('react-native-router-flux')
jest.mock('common/DB')
jest.mock('common/store')
jest.mock('common/utils/sfx')
jest.mock('common/utils/logger')
jest.mock('common/models/StoreAccessibleBase')
import initialize from 'common/initialize'
import I18n from 'i18n-js'
I18n.locale = 'ja'

import 'sqlite3'

import ShopRepository from './src/modules/shop/repositories/ShopRepository'
import { shopIds } from './src/modules/shop/samples'
ShopRepository.syncRemoteToLocalById(shopIds.A)
import CashierRepository from './src/modules/cashier/repositories/CashierRepository'
import { sampleCashierMap } from './src/modules/cashier/samples'
CashierRepository.save(sampleCashierMap.cashierA_1)

