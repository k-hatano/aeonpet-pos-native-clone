import fetcher from 'common/models/fetcher'
import { handleAxiosError } from '../../../../common/errors'

export default class StandardDeviceLogRepository {
  async push (log, isShortTimeout: boolean) {
    try {
      const options = {}
      if (isShortTimeout) {
        options['timeout'] = 2000
      }
      await fetcher.post('/device-logs', log, options)
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
