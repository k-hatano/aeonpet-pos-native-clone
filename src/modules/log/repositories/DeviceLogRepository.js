import SampleDeviceLogRepository from './sample/SampleDeviceLogRepository'
import StandardDeviceLogRepository from './standard/StandardDeviceLogRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class DeviceLogRepository {
  static _implement = new SampleDeviceLogRepository()

  static async push (log, isShortTimeout: boolean = false) {
    return this._implement.push(log, isShortTimeout)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardDeviceLogRepository()
        break

      default:
        this._implement = new SampleDeviceLogRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('DeviceLogRepository', (context) => {
  DeviceLogRepository.switchImplement(context)
})
