export default {
  listContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: '#fff',
    height: 70,
    paddingHorizontal: 22,
    paddingVertical: 22
  },
  shopItem: {
    flex: 1,
    flexDirection: 'row'
  },
  shopName: {
    flex: 1,
    fontFamily: 'Helvetica',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    marginRight: 15
  },
  shopCode: {
    fontFamily: 'Helvetica',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    marginRight: 15
  }
}
