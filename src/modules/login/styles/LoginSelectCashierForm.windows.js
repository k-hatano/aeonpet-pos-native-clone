import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  formWrapper: {
    width: 700,
    flex: 5
  },
  selectList: {
    marginTop: 56,
    flex: 2
  },
  selectCashierButton: {
    borderWidth: 2,
    borderRadius: 4,
    width: 686,
    height: 74,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 8
  },
  selectCashierButtonIconContainer: {
    width: 74,
    alignItems: 'center'
  },
  selectCashierButtonIcon: {
    width: 30,
    height: 22
  },
  selectCashierButtonText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    letterSpacing: -0.38
  },
  buttons: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 72,
    paddingRight: 72,
    marginTop: 120,
    flex: 1
  },
  commandButton: {
    height: 64,
    width: 306,
    fontSize: 32,
    fontWeight: 'bold'
  },
  commandButtonDisabled: {
    height: 64,
    width: 306,
    backgroundColor: '#f0f0f0',
    fontSize: 32,
    fontWeight: 'bold'
  },
  titleContainer: {
    flex: 1,
    position: 'relative'
  },
  pageTitle: {
    position: 'absolute',
    bottom: 0
  }
})
