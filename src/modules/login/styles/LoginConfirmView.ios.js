import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  viewWrapper: {
    alignItems: 'center',
    marginTop: 130
  },
  shopName: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 48,
    color: '#4a4a4a',
    letterSpacing: -0.58,
    alignItems: 'center'
  },
  posName: {
    marginTop: 60,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 48,
    color: '#4a4a4a',
    letterSpacing: -0.58,
    alignItems: 'center'
  },
  buttons: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 120
  },
  buttonContainerStyle: {
    height: 64,
    width: 306,
    marginRight: 36,
    fontSize: 36,
    fontWeight: 'bold'
  }
})
