import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  formWrapper: {
    width: 700
  },
  selectShopButton: {
    height: 48,
    width: 146,
    marginLeft: 24,
    fontSize: 24
  },
  selectShopButtonTextStyle: {
    height: 24,
    fontFamily: 'HiraginoSans-W6',
    fontSize: 24,
    color: '#ff9024',
    letterSpacing: -0.38
  },
  selectShopInput: {
    flex: 1
  },
  rowForm: {
    marginTop: 40,
    flexDirection: 'row'
  },
  deviceInput: {
    width: 700,
    marginTop: 95
  },
  buttons: {
    width: '100%',
    paddingLeft: 72,
    paddingRight: 72,
    alignItems: 'flex-end',
    marginTop: 120
  },
  nextButton: {
    height: 64,
    width: 306,
    fontSize: 32,
    fontWeight: 'bold'
  }
})
