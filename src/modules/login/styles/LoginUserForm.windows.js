import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  formWidth: {
    width: 720
  },
  formWrapper: {
    width: 720,
    marginTop: 122
  },
  formRow: {
    flexDirection: 'row',
    paddingTop: 16,
    paddingBottom: 16
  },
  formRowLabel: {
    width: 200
  },
  formRowInput: {
    flex: 1
  },
  loginButton: {
    height: 64,
    width: 480,
    fontSize: 32,
    fontWeight: 'bold'
  }
})
