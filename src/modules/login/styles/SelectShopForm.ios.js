export default {
  modalContainer: {
    width: '100%',
    height: '100%',
    padding: 32,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  modalPopup: {
    width: '50%',
    height: '80%',
    flexDirection: 'column'
  },
  shopListContainer: {
    flexDirection: 'column',
    width: '100%',
    flex: 1
  },
  shopSearchContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 55
  },
  shopSearchInner: {
    flex: 1,
    height: 48
  },
  searchInput: {
    height: 48,
    fontSize: 24,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 16
  },
  buttonSearchContainer: {
    width: 128,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchButton: {
    width: 96,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    fontSize: 18,
    fontWeight: 'bold'
  },
  shopListInner: {
    flex: 1,
    width: '100%'
  },
  shopList: {
    width: '100%',
    height: '100%',
    paddingTop: 48
  }
}
