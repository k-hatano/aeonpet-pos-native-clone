import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  row: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    width: '100%',
    alignItems: 'center'
  },
  pageTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 36,
    color: '#4a4a4a',
    letterSpacing: -0.58
  },
  normalText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  textInput: {
    width: '100%',
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    height: 46,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 20
  },
  oneButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 120
  },
  buttonTextStyle: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 32,
    letterSpacing: -0.51,
    fontWeight: 'bold'
  }
})
