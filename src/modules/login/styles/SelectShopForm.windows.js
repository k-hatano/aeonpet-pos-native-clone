export default {
  modalContainer: {
    width: '100%',
    height: '100%',
    padding: 32,
    flexDirection: 'column',
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:1, width: 1}
  },
  modalPopup: {
    width: '50%',
    height: '80%',
    flexDirection: 'column'
  },
  shopListContainer: {
    flexDirection: 'column',
    width: '100%',
    flex: 1
  },
  shopSearchContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 55
  },
  shopSearchInner: {
    flex: 1,
    height: 48
  },
  searchInput: {
    height: 48,
    justifyContent: 'center',
    backgroundColor: '#f0f0f0',
    fontSize: 24,
    letterSpacing: -0.38
  },
  buttonSearchContainer: {
    width: 128,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchButton: {
    width: 96,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    fontSize: 18,
    fontWeight: 'bold'
  },
  shopListInner: {
    flex: 1,
    width: '100%'
  },
  shopList: {
    width: '100%',
    height: '100%',
    paddingTop: 48
  }
}
