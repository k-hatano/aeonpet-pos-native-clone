import configureMockStore from 'redux-mock-store'
import loginReducer, { filterShopByKeyword } from '../loginReducer'
import {
  setLoggedUser,
  setLoggedDevice,
  setAvailableShops,
  selectShop,
  setSearchKeyword,
  filterShop,
  setAvailableCashiers,
  selectCashier,
  resetLoginStates
} from '../../actions'
import { MODULE_NAME, USER_TYPE } from '../../models'

jest.mock('../../../../common/utils/sfx', () => {}) // Fix react-native-sound issue (from AlertView)

const middlewares = []
const mockStore = configureMockStore(middlewares)
const initialState = {}
const store = mockStore(initialState)

afterEach(() => {
  store.clearActions()
})

describe('[Login] Login Reducer Test', () => {
  it('Should dispatch setLoggedUser action', () => {
    const user = {
      id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Demo User'
    }
    store.dispatch(setLoggedUser(user))
    const expectedPayload = {
      payload: user,
      type: `${MODULE_NAME}_setLoggedUser`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setLoggedUser', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    const user = {
      id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Demo User'
    }
    const expected = { ...defaultState, currentUser: user }
    const actual = loginReducer(defaultState, setLoggedUser(user))
    expect(actual).toEqual(expected)
  })

  it('Should dispatch setLoggedDevice action', () => {
    const device = {
      id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Demo Device'
    }
    store.dispatch(setLoggedDevice(device))
    const expectedPayload = {
      payload: device,
      type: `${MODULE_NAME}_setLoggedDevice`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setLoggedDevice', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    const device = {
      id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Demo Device'
    }
    const newState = { ...defaultState, ...{ currentDevice: device } }

    expect(
      loginReducer(defaultState, setLoggedDevice(device))
    )
      .toEqual(newState)
  })

  it('Should dispatch setAvailableShops action', () => {
    const shops = [
      {
        id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Shop 1',
        shop_code: 'Shop Code 1'
      },
      {
        id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Shop 2',
        shop_code: 'Shop Code 2'
      }
    ]
    store.dispatch(setAvailableShops(shops))
    const expectedPayload = {
      payload: shops,
      type: `${MODULE_NAME}_setAvailableShops`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setAvailableShops', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    const shops = [
      {
        id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Shop 1',
        shop_code: 'Shop Code 1'
      },
      {
        id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Shop 2',
        shop_code: 'Shop Code 2'
      }
    ]
    const newState = { ...defaultState, ...{ shops } }

    expect(
      loginReducer(defaultState, setAvailableShops(shops))
    )
      .toEqual(newState)
  })

  it('Should dispatch selectShop action', () => {
    // Dispatch selectShop with shopId (string)
    const selectedShopId = '2546058f-5a25-4334-85ae-e68f2a44bbaf'
    store.dispatch(selectShop(selectedShopId))
    const expectedPayload1 = {
      payload: selectedShopId,
      type: `${MODULE_NAME}_selectShop`
    }

    expect(store.getActions())
      .toEqual([expectedPayload1])

    // Dispatch selectShop with shop (object)
    const shop = {
      id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Shop 2',
      shop_code: 'Shop Code 2'
    }
    store.dispatch(selectShop(shop))
    const expectedPayload2 = {
      payload: shop,
      type: `${MODULE_NAME}_selectShop`
    }

    expect(store.getActions())
      .toEqual([expectedPayload1, expectedPayload2])
  })

  it('Should handle selectShop', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [
        {
          id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Shop 1',
          shop_code: 'Shop Code 1'
        },
        {
          id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Shop 2',
          shop_code: 'Shop Code 2'
        }
      ],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    // selectShop with shopId (string)
    const selectedShopId = '2546058f-5a25-4334-85ae-e68f2a44bbaf'
    const newState1 = {
      ...defaultState,
      ...{ selectedShop: defaultState.shops.find(shop => shop.id === selectedShopId) }
    }

    expect(
      loginReducer(defaultState, selectShop(selectedShopId))
    )
      .toEqual(newState1)

    // selectShop with shop (object)
    const currentShop = {
      id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Shop 2',
      shop_code: 'Shop Code 2'
    }
    const newState2 = {
      ...defaultState,
      ...{ selectedShop: defaultState.shops.find(shop => shop.id === currentShop.id) }
    }

    expect(
      loginReducer(defaultState, selectShop(currentShop))
    )
      .toEqual(newState2)
  })

  it('Should dispatch setSearchKeyword action', () => {
    const keyword = 'Test Shop'
    store.dispatch(setSearchKeyword(keyword))
    const expectedPayload = {
      payload: keyword,
      type: `${MODULE_NAME}_setSearchKeyword`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setSearchKeyword', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    const keyword = 'Test Shop'
    const newState = { ...defaultState, ...{ keyword } }

    expect(
      loginReducer(defaultState, setSearchKeyword(keyword))
    )
      .toEqual(newState)
  })

  it('Should dispatch filterShop action', () => {
    const keyword = 'Shop Test'
    store.dispatch(filterShop(keyword))
    const expectedPayload = {
      payload: keyword,
      type: `${MODULE_NAME}_filterShop`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle filterShop', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [
        {
          id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Shop Name 1',
          shop_code: 'Shop Code 1'
        },
        {
          id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Shop Name 2',
          shop_code: 'Shop Code 2'
        }
      ],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }

    // Search by shop_code
    const keyword1 = 'Shop Code 2'
    const stateAfterSetKeyword1 = loginReducer(defaultState, setSearchKeyword(keyword1))
    const newState1 = {
      ...stateAfterSetKeyword1,
      ...{
        filteredShops: filterShopByKeyword(keyword1, stateAfterSetKeyword1.shops),
      }
    }

    expect(
      loginReducer(stateAfterSetKeyword1, filterShop(keyword1))
    )
      .toEqual(newState1)

    // Search by name
    const keyword2 = 'Shop Name 2'
    const stateAfterSetKeyword2 = loginReducer(defaultState, setSearchKeyword(keyword2))
    const newState2 = {
      ...stateAfterSetKeyword2,
      ...{
        filteredShops: filterShopByKeyword(keyword2, stateAfterSetKeyword2.shops),
      }
    }

    expect(
      loginReducer(stateAfterSetKeyword2, filterShop(keyword2))
    )
      .toEqual(newState2)

    // Search by empty string
    const keyword3 = ''
    const stateAfterSetKeyword3 = loginReducer(defaultState, setSearchKeyword(keyword3))
    const newState3 = {
      ...stateAfterSetKeyword3,
      ...{
        filteredShops: stateAfterSetKeyword3.shops,
      }
    }

    expect(
      loginReducer(stateAfterSetKeyword3, filterShop(keyword3))
    )
      .toEqual(newState3)
  })

  it('Should dispatch setAvailableCashiers action', () => {
    const cashiers = [
      {
        id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Cashier 1',
        cashier_code: 'Cashier Code 1'
      },
      {
        id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Cashier 2',
        cashier_code: 'Cashier Code 2'
      }
    ]
    store.dispatch(setAvailableCashiers(cashiers))
    const expectedPayload = {
      payload: cashiers,
      type: `${MODULE_NAME}_setAvailableCashiers`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setAvailableCashiers', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null
    }
    const cashiers = [
      {
        id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Cashier 1',
        cashier_code: 'Cashier Code 1'
      },
      {
        id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
        name: 'Cashier 2',
        cashier_code: 'Cashier Code 2'
      }
    ]
    const newState = { ...defaultState, ...{ cashiers } }

    expect(
      loginReducer(defaultState, setAvailableCashiers(cashiers))
    )
      .toEqual(newState)
  })

  it('Should dispatch selectCashier action', () => {
    // Dispatch selectCashier with cashierId (string)
    const cashierId = '2546058f-5a25-4334-85ae-e68f2a44bbaf'
    store.dispatch(selectCashier(cashierId))
    const expectedPayload1 = {
      payload: cashierId,
      type: `${MODULE_NAME}_selectCashier`
    }

    expect(store.getActions())
      .toEqual([expectedPayload1])

    // Dispatch selectCashier with cashier (object)
    const cashier = {
      id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Cashier 2',
      cashier_code: 'Cashier Code 2'
    }
    store.dispatch(selectCashier(cashier))
    const expectedPayload2 = {
      payload: cashier,
      type: `${MODULE_NAME}_selectCashier`
    }

    expect(store.getActions())
      .toEqual([expectedPayload1, expectedPayload2])
  })

  it('Should handle selectCashier', () => {
    const defaultState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [
        {
          id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Cashier 1',
          cashier_code: 'Cashier Code 1'
        },
        {
          id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Cashier 2',
          cashier_code: 'Cashier Code 2'
        }
      ],
      selectedCashier: null
    }
    // selectCashier with cashierId (string)
    const cashierId = '2546058f-5a25-4334-85ae-e68f2a44bbaf'
    const newState1 = {
      ...defaultState,
      ...{ selectedCashier: defaultState.cashiers.find(cashier => cashier.id === cashierId) }
    }

    expect(
      loginReducer(defaultState, selectCashier(cashierId))
    )
      .toEqual(newState1)

    // selectCashier with cashier (object)
    const currentCashier = {
      id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
      name: 'Cashier 2',
      cashier_code: 'Cashier Code 2'
    }
    const newState2 = {
      ...defaultState,
      ...{ selectedCashier: defaultState.cashiers.find(cashier => cashier.id === currentCashier.id) }
    }

    expect(
      loginReducer(defaultState, selectCashier(currentCashier))
    )
      .toEqual(newState2)
  })

  it('Should dispatch resetLoginStates action', () => {
    store.dispatch(resetLoginStates({}))
    const expectedPayload = {
      payload: {},
      type: `${MODULE_NAME}_resetLoginStates`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle resetLoginStates', () => {
    const oldState = {
      currentUser: {},
      currentDevice: {},
      shops: [],
      selectedShop: {},
      keyword: 'abcdef',
      filteredShops: [],
      cashiers: [
        {
          id: '1546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Cashier 1',
          cashier_code: 'Cashier Code 1'
        },
        {
          id: '2546058f-5a25-4334-85ae-e68f2a44bbaf',
          name: 'Cashier 2',
          cashier_code: 'Cashier Code 2'
        }
      ],
      selectedCashier: {}
    }

    const newState = {
      currentUser: null,
      currentDevice: null,
      shops: [],
      selectedShop: null,
      keyword: '',
      filteredShops: [],
      cashiers: [],
      selectedCashier: null,
      loggedUserType: USER_TYPE.NONE
    }

    expect(
      loginReducer(oldState, resetLoginStates({}))
    )
      .toEqual(newState)
  })
})
