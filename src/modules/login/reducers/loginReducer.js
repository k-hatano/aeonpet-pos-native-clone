import { handleActions } from 'redux-actions'
import { createSelector } from 'reselect'
import {
  setLoggedUser,
  setLoggedDevice,
  setAvailableShops,
  selectShop,
  setSearchKeyword,
  filterShop,
  setAvailableCashiers,
  selectCashier,
  resetLoginStates,
  setLoggedUserType
} from '../actions'
import { USER_TYPE } from '../models'

const defaultState = {
  currentUser: null,
  currentDevice: null,
  shops: [],
  selectedShop: null,
  keyword: '',
  filteredShops: [],
  cashiers: [],
  selectedCashier: null,
  loggedUserType: USER_TYPE.NONE
}

const handlers = {
  [setLoggedUser]: (state, action) => ({
    ...state,
    ...{ currentUser: action.payload }
  }),
  [setLoggedDevice]: (state, action) => ({
    ...state,
    ...{ currentDevice: action.payload }
  }),
  [setAvailableShops]: (state, action) => ({
    ...state,
    ...{ shops: action.payload }
  }),
  [selectShop]: (state, action) => {
    const { payload } = action
    if (typeof payload === 'string') {
      const shopId = payload
      const { shops } = state
      const selectedShop = shops.find(shop => shop.id === shopId)
      return {
        ...state,
        ...{ selectedShop }
      }
    }
    return {
      ...state,
      ...{ selectedShop: payload }
    }
  },
  [setSearchKeyword]: (state, action) => ({
    ...state,
    ...{ keyword: action.payload }
  }),
  [filterShop]: (state, action) => ({
    ...state,
    ...{ filteredShops: getFilteredShop(state) }
  }),
  [setAvailableCashiers]: (state, action) => ({
    ...state,
    ...{ cashiers: action.payload }
  }),
  [selectCashier]: (state, action) => {
    const { payload } = action
    if (typeof payload === 'string') {
      const cashierId = payload
      const { cashiers } = state
      const selectedCashier = cashiers.find(cashier => cashier.id === cashierId)
      return {
        ...state,
        ...{ selectedCashier }
      }
    }
    return {
      ...state,
      ...{ selectedCashier: payload }
    }
  },
  [resetLoginStates]: state => ({
    ...defaultState
  }),
  [setLoggedUserType]: (state, action) => ({
    ...state,
    loggedUserType: action.payload
  })
}

const getFilteredKeyword = state => state.keyword
const getShops = state => state.shops
export const filterShopByKeyword = (keyword, shops) => {
  if (keyword === '') {
    return shops
  }
  const regex = new RegExp(keyword, 'iu')
  return shops.filter(shop => regex.test(shop.name))
}
const getFilteredShop = createSelector([getFilteredKeyword, getShops], filterShopByKeyword)

export default handleActions(handlers, defaultState)
