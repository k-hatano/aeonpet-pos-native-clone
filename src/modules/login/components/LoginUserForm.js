import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import { Actions } from 'react-native-router-flux'
import { Grid, Row } from 'react-native-easy-grid'
import I18n from 'i18n-js'
import { Field } from '../../../common/components/elements/Form'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import { getUdid } from '../../../common/models'
import { validateUserForm } from '../models'
import commonStyles from '../styles/Login'
import componentStyles from '../styles/LoginUserForm'
import { isDevMode } from '../../../common/utils/environment'
import KeyboardSpace from '../../../common/components/layout/KeyboardSpace'

export const FORM_NAME = 'LoginUserForm'

class LoginUserForm extends Component {
  static propTypes = {
    onLogin: PropTypes.func,
    onLoginForSample: PropTypes.func,
    handleSubmit: PropTypes.func,
    formValues: PropTypes.object
  }

  constructor (props) {
    super(props)

    this.udid = getUdid()
  }

  async _handleSubmit (values) {
    const success = await this.props.onLogin(values)
    if (success) {
      this.props.change('password', '')
      this.props.reset()
    }
  }

  async _handleLoginForSample () {
    await this.props.onLoginForSample()
    Actions.home()
  }

  render () {
    const { handleSubmit } = this.props

    return (
      <Grid>
        <Row style={commonStyles.row}>
          <View style={commonStyles.container}>
            <View style={componentStyles.formWidth}>
              <Text style={commonStyles.pageTitle}>
                {I18n.t('login.please_enter_login_information')}
              </Text>
            </View>
            <View style={componentStyles.formWrapper}>
              <View style={componentStyles.formRow}>
                <View style={componentStyles.formRowLabel}>
                  <Text style={commonStyles.normalText}>
                    {`${I18n.t('login.connecting_url')}:`}
                  </Text>
                </View>
                <View style={componentStyles.formRowInput}>
                  <Field
                    name='connectingUrl'
                    autoCapitalize='none'
                    autoCorrect={false}
                    returnKeyType='next'
                    placeholder='http://'
                    style={commonStyles.textInput}
                  />
                </View>
              </View>
              <View style={componentStyles.formRow}>
                <View style={componentStyles.formRowLabel}>
                  <Text style={commonStyles.normalText}>
                    {`${I18n.t('login.login_id')}:`}
                  </Text>
                </View>
                <View style={componentStyles.formRowInput}>
                  <Field
                    name='loginId'
                    autoCapitalize='none'
                    autoCorrect={false}
                    returnKeyType='next'
                    style={commonStyles.textInput}
                  />
                </View>
              </View>
              <View style={componentStyles.formRow}>
                <View style={componentStyles.formRowLabel}>
                  <Text style={commonStyles.normalText}>
                    {`${I18n.t('login.password')}:`}
                  </Text>
                </View>
                <View style={componentStyles.formRowInput}>
                  <Field secureTextEntry name='password' returnKeyType='send' style={commonStyles.textInput} />
                </View>
              </View>
              <View style={componentStyles.formRow}>
                <View style={componentStyles.formRowLabel}>
                  <Text style={commonStyles.normalText}>
                    {`${I18n.t('login.UDID')}:`}
                  </Text>
                </View>
                <View>
                  <Text style={commonStyles.normalText}>
                    {this.udid}
                  </Text>
                </View>
              </View>
            </View>
            <View style={commonStyles.oneButtonContainer}>
              <CommandButton
                text={I18n.t('login.login')}
                style={componentStyles.loginButton}
                onPress={handleSubmit(value => this._handleSubmit(value))}
              />
            </View>
            {!isDevMode() ? null : (
              <View style={commonStyles.debugButtonsContainer}>
                <CommandButton
                  text='SampleMode'
                  style={componentStyles.loginButton}
                  onPress={() => this._handleLoginForSample()}
                />
              </View>
            )}
          </View>
        </Row>
        <KeyboardSpace />
      </Grid>
    )
  }
}

export default reduxForm({
  form: FORM_NAME,
  enableReinitialize: true,
  validate: validateUserForm
})(LoginUserForm)
