import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { Grid, Row } from 'react-native-easy-grid'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton } from '../../../common/components/elements/StandardButton'
import commonStyles from '../styles/Login'
import componentStyles from '../styles/LoginConfirmView'

export default class LoginConfirmView extends Component {
  static propTypes = {
    onLogin: PropTypes.func,
    onCancel: PropTypes.func,
    onInit: PropTypes.func,
    selectedShop: PropTypes.object,
    selectedCashier: PropTypes.object,
    currentDevice: PropTypes.object,
    deviceName: PropTypes.string
  }

  async componentWillMount () {
    this.props.onInit && await this.props.onInit()
  }

  render () {
    const { deviceName, selectedShop, selectedCashier, onLogin, onCancel } = this.props
    let selectedCashierName = ''
    if (selectedCashier) {
      selectedCashierName = selectedCashier.id ? selectedCashier.name : I18n.t('login.inventory_management_terminal')
    }

    return (
      <Grid>
        <Row style={commonStyles.row}>
          <View style={commonStyles.container}>
            <View>
              <Text style={commonStyles.pageTitle}>
                {I18n.t('login.log_in_to_the_following')}
              </Text>
            </View>
            <View style={componentStyles.viewWrapper}>
              <Text style={componentStyles.shopName}>
                {selectedShop && selectedShop.name}
              </Text>
              <Text style={componentStyles.posName}>
                {selectedCashierName}
              </Text>
            </View>
            <View style={componentStyles.buttons}>
              <CommandButton
                text={I18n.t('login.login')}
                style={componentStyles.buttonContainerStyle}
                onPress={async () => onLogin(deviceName, selectedCashier)}
              />
              <OptionalCommandButton
                text={I18n.t('login.cancel')}
                style={componentStyles.buttonContainerStyle}
                onPress={async () => onCancel()}
              />
            </View>
          </View>
        </Row>
      </Grid>
    )
  }
}
