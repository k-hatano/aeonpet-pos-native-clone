import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import { Grid, Row } from 'react-native-easy-grid'
import I18n from 'i18n-js'
import { Field } from '../../../common/components/elements/Form'
import { CommandButton, OptionalCommandButton } from '../../../common/components/elements/StandardButton'
import { showSelectShopFormModal } from '../containers/SelectShopForm'
import commonStyles from '../styles/Login'
import componentStyles from '../styles/LoginSelectShopForm'
import KeyboardSpace from '../../../common/components/layout/KeyboardSpace'
import { USER_TYPE } from '../models'

export const FORM_NAME = 'LoginSelectShopForm'

class LoginSelectShopForm extends Component {
  static propTypes = {
    onNext: PropTypes.func,
    beforeSelectShop: PropTypes.func,
    handleSubmit: PropTypes.func,
    shops: PropTypes.array,
    selectedShop: PropTypes.object,
    formValues: PropTypes.object,
    loggedUserType: PropTypes.number
  }

  async _handleSubmit (values) {
    const { selectedShop, onNext } = this.props
    await onNext(values, selectedShop)
  }

  render () {
    const { formValues, selectedShop, handleSubmit, loggedUserType } = this.props

    let nextButtonLabel = ''
    switch (loggedUserType) {
      case USER_TYPE.ADMIN:
        nextButtonLabel = I18n.t('login.next')
        break
      case USER_TYPE.SHOP_MEMBER:
        nextButtonLabel = I18n.t('login.register')
        break
    }

    return (
      <Grid>
        <Row style={commonStyles.row}>
          <View style={commonStyles.container}>
            <View style={componentStyles.formWrapper}>
              <Text style={commonStyles.pageTitle}>
                {I18n.t('login.please_select_a_shop')}
              </Text>
              <View style={componentStyles.rowForm}>
                <View style={componentStyles.selectShopInput}>
                  <TextInput
                    value={selectedShop ? selectedShop.name : null}
                    editable={false}
                    autoCapitalize='none'
                    autoCorrect={false}
                    returnKeyType='next'
                    style={commonStyles.textInput}
                  />
                </View>
                <View>
                  <OptionalCommandButton
                    text={I18n.t('login.select')}
                    style={componentStyles.selectShopButton}
                    onPress={async () => {
                      if (await this.props.beforeSelectShop()) {
                        showSelectShopFormModal()
                      }
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={componentStyles.deviceInput}>
              <Text style={commonStyles.pageTitle}>
                {I18n.t('login.please_enter_the_device_name')}
              </Text>
              <View style={componentStyles.rowForm}>
                <Field name='deviceName' autoCorrect={false} returnKeyType='next' style={commonStyles.textInput} />
              </View>
            </View>
            <View style={componentStyles.buttons}>
              <CommandButton
                text={nextButtonLabel}
                style={componentStyles.nextButton}
                onPress={handleSubmit(value => this._handleSubmit(value))}
                disabled={!(selectedShop && formValues.deviceName)}
              />
            </View>
          </View>
        </Row>
        <KeyboardSpace />
      </Grid>
    )
  }
}

export default reduxForm({
  form: FORM_NAME,
  touchOnBlur: false,
  validate: values => {
    const errors = {}
    if (!values.deviceName) {
      errors.deviceName = I18n.t('common.item_required', { item: I18n.t('login.device_name') })
    }
    return errors
  }
})(LoginSelectShopForm)
