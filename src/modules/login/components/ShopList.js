import React, { Component } from 'react'
import { Text, View, ListView, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import componentStyles from '../styles/ShopList'

export default class ShopList extends Component {
  static propTypes = {
    onSelectShop: PropTypes.func,
    shops: PropTypes.array
  }

  constructor (props) {
    super(props)

    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  render () {
    const { shops, onSelectShop } = this.props

    return (
      <ListView
        style={componentStyles.listContainer}
        dataSource={this.dataSource.cloneWithRows(shops)}
        renderRow={(shop, index) =>
          <TouchableOpacity
            key={index}
            style={componentStyles.row}
            onPress={() => {
              onSelectShop(shop)
            }}
          >
            <View style={componentStyles.shopItem}>
              <Text style={componentStyles.shopName}>
                {shop.name}
              </Text>
              <Text style={componentStyles.shopCode}>
                {shop.shop_code}
              </Text>
            </View>
          </TouchableOpacity>}
      />
    )
  }
}
