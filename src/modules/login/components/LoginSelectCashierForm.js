import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ListView } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton } from '../../../common/components/elements/StandardButton'
import ImageButton from '../../../common/components/elements/ImageButton'
import commonStyles from '../styles/Login'
import componentStyles from '../styles/LoginSelectCashierForm'

function CashierSelector ({ cashier, isSelected, onSelect }) {
  return (
    <TouchableOpacity
      style={[
        componentStyles.selectCashierButton,
        {
          backgroundColor: isSelected ? '#ff9024' : '#fff',
          borderColor: isSelected ? '#ff9024' : '#979797'
        }
      ]}
      onPress={() => onSelect(cashier.id)}
    >
      <View style={componentStyles.selectCashierButtonIconContainer}>
        {isSelected &&
          <ImageButton
            style={componentStyles.selectCashierButtonIcon}
            appearance={{
              normal: require('../../../assets/images/login/checked.png'),
              highlight: require('../../../assets/images/login/checked.png')
            }}
          />}
      </View>
      <Text
        style={[
          componentStyles.selectCashierButtonText,
          {
            color: isSelected ? '#fff' : '#4a4a4a'
          }
        ]}
      >
        {cashier.name}
      </Text>
    </TouchableOpacity>
  )
}

export default class LoginSelectCashierForm extends Component {
  static propTypes = {
    onNext: PropTypes.func,
    onBack: PropTypes.func,
    onGetCashiers: PropTypes.func,
    onSelectCashier: PropTypes.func,
    cashiers: PropTypes.array,
    selectedShop: PropTypes.object,
    selectedCashier: PropTypes.object,
    currentDevice: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  async componentWillMount () {
  }

  _onSelectCashier = cashier => {
    const { onSelectCashier } = this.props
    onSelectCashier(cashier)
  }

  render () {
    const { cashiers, selectedCashier, currentDevice, onNext, onBack } = this.props
    const cashiersList = this.dataSource.cloneWithRows(cashiers)
    return (
      <View style={commonStyles.row}>
        <View style={componentStyles.formWrapper}>
          <View style={componentStyles.titleContainer}>
            <Text style={[commonStyles.pageTitle, componentStyles.pageTitle]}>
              {I18n.t('login.please_select_a_device')}
            </Text>
          </View>
          <View style={componentStyles.selectList}>
            <ListView
              enableEmptySections
              dataSource={cashiersList}
              renderRow={(rowData, rowId) =>
                <CashierSelector
                  key={rowId}
                  cashier={rowData}
                  onSelect={cashier => this._onSelectCashier(cashier)}
                  isSelected={selectedCashier && rowData.id === selectedCashier.id}
                />
              } />
          </View>
        </View>
        <View
          style={
            [
              componentStyles.buttons,
              currentDevice ? { justifyContent: 'flex-end' } : { justifyContent: 'space-between' }
            ]
          }
        >
          {!currentDevice &&
            <OptionalCommandButton
              text={I18n.t('login.back')}
              style={componentStyles.commandButton}
              onPress={() => onBack()}
            />
          }
          <CommandButton
            text={I18n.t('login.next')}
            style={
              selectedCashier ? componentStyles.commandButton : componentStyles.commandButtonDisabled
            }
            onPress={() => selectedCashier && onNext(selectedCashier)}
          />
        </View>
      </View>
    )
  }
}
