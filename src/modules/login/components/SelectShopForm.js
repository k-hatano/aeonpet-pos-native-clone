import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import ModalFrame from 'common/components/widgets/ModalFrame'
import Modal from '../../../common/components/widgets/Modal'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import ShopList from '../components/ShopList'
import styles from '../styles/SelectShopForm'

export default class SelectShopForm extends Component {
  static propTypes = {
    onSelectShop: PropTypes.func,
    onFilterShop: PropTypes.func,
    shops: PropTypes.array,
    filteredShops: PropTypes.array,
    keyword: PropTypes.string
  }

  constructor (props) {
    super(props)

    const { keyword } = props
    this.state = { searchText: keyword }
  }

  renderShopList () {
    const { shops, filteredShops, onFilterShop, onSelectShop } = this.props
    const { searchText } = this.state
    const availableShops = (filteredShops.length > 0 || searchText.length > 0) ? filteredShops : shops
    return (
      <View style={styles.shopListContainer}>
        <View style={styles.shopSearchContainer}>
          <View style={styles.shopSearchInner}>
            <TextInput
              value={searchText}
              autoCapitalize='none'
              autoCorrect={false}
              placeholder={I18n.t('login.search_shop_placeholder')}
              style={styles.searchInput}
              onChangeText={value => {
                this.setState({ searchText: value })
                this.props.onFilterShop(value)
              }}
            />
          </View>
        </View>
        <View style={styles.shopListInner}>
          {Array.isArray(availableShops) && availableShops.length > 0
            ? <View style={styles.shopList}>
              <ShopList
                shops={availableShops}
                onSelectShop={async shop => {
                  await onSelectShop(shop)
                  Modal.close('shopSelectorModal')
                }}
              />
            </View>
            : <Text>There's no shop available!</Text>}
        </View>
      </View>
    )
  }

  render () {
    return (
      <ModalFrame style={styles.modalPopup} title={I18n.t('login.shop_select')} onClose={() => Modal.close()}>
        <View style={styles.modalContainer}>
          {this.renderShopList()}
        </View>
      </ModalFrame>
    )
  }
}
