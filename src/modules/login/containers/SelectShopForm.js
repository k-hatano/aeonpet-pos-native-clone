import { connect } from 'react-redux'
import SelectShopForm from '../components/SelectShopForm'
import { MODULE_NAME } from '../models'
import { selectShop, setSearchKeyword, filterShop } from '../actions'
import Modal from '../../../common/components/widgets/Modal'

const mapDispatchToProps = dispatch => ({
  onSelectShop: async shop => {
    dispatch(selectShop(shop))
  },
  onFilterShop: keyword => {
    dispatch(setSearchKeyword(keyword))
    dispatch(filterShop(keyword))
  }
})

const mapStateToProps = state => ({
  shops: state[MODULE_NAME].shops,
  keyword: state[MODULE_NAME].keyword,
  filteredShops: state[MODULE_NAME].filteredShops
})

const SelectShopFormModal = connect(mapStateToProps, mapDispatchToProps)(SelectShopForm)

export const showSelectShopFormModal = () => {
  Modal.open(SelectShopFormModal, {
    props: {
      onClose: () => Modal.close()
    }
  })
}
