import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import fetcher from 'common/models/fetcher'
import LoginSelectShopForm, { FORM_NAME } from '../components/LoginSelectShopForm'
import ShopRepository from '../../shop/repositories/ShopRepository'
import { MODULE_NAME, USER_TYPE } from '../models'
import { setAvailableShops } from '../actions'
import { updateSetting } from '../../setting/actions'
import SettingKeys from '../../setting/models/SettingKeys'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from '../../../common/sideEffects'
import I18n from 'i18n-js'
import DeviceRepository from '../repositories/DeviceRepository'
import { getUdid } from '../../../common/models'
import { fetchCashiers } from '../services'
import { getCommonNetworkErrorMessage, handleAxiosError } from '../../../common/errors'

const mapDispatchToProps = dispatch => ({
  onNext: async (values, selectedShop, loggedUserType, deviceName) => {
    if (values.deviceName.length > 16) {
      AlertView.show(I18n.t('message.J-11-E002'))
      return
    }
    if (loggedUserType === USER_TYPE.ADMIN) {
      await loading(dispatch, async () => {
        try {
          await ShopRepository.saveCurrentShop(selectedShop)
          dispatch(updateSetting({ key: SettingKeys.COMMON.SHOP_ID, value: selectedShop.id }))
          dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.SHOP_NAME, value: selectedShop.name }))
          await fetcher.refreshUserToken(selectedShop.id)
        } catch (error) {
          try {
            handleAxiosError(error)
          } catch (networkError) {
            AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.J-12-E001')))
          }
          return
        }

        const fetchCashierResult = await fetchCashiers(dispatch, { shopId: selectedShop.id })
        if (fetchCashierResult.errorMessage) {
          AlertView.show(fetchCashierResult.errorMessage, () => Actions.popTo('loginUserPage'))
          return
        }

        Actions.loginSelectCashierPage(values)
      })
    } else {
      try {
        const udid = getUdid()
        await fetcher.refreshUserToken(selectedShop.id)
        await DeviceRepository.push(deviceName, udid)
        // TODO リファクタリング refreshUserTokenはaxiosの例外を、DeviceRepository.pushはNetworkErrorを投げるので、適切に処理すること
        await AlertView.showAsync(' デバイスの承認依頼を行いました。\n 承認後、再度ログインしてください。')
      } catch (error) {
        try {
          handleAxiosError(error)
        } catch (networkError) {
          AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.J-11-E001')))
        }
      }
      Actions.popTo('loginUserPage')
    }
  },
  beforeSelectShop: async () => {
    try {
      await loading(dispatch, async () => {
        const shops = await ShopRepository.fetchAllavailableShop()
        dispatch(setAvailableShops(shops))
      })
      return true
    } catch (error) {
      await AlertView.showAsync(
        getCommonNetworkErrorMessage(error, I18n.t('message.J-12-E001')))
      return false
    }
  }
})

const mapStateToProps = state => ({
  shops: state[MODULE_NAME].shops,
  selectedShop: state[MODULE_NAME].selectedShop,
  formValues: (state.form[FORM_NAME] && state.form[FORM_NAME].values) || {},
  loggedUserType: state[MODULE_NAME].loggedUserType,
  deviceName: state.form['LoginSelectShopForm'] && state.form['LoginSelectShopForm'].values && state.form['LoginSelectShopForm'].values.deviceName
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onNext: async (values, selectedShop) => {
      return dispatchProps.onNext(values, selectedShop, stateProps.loggedUserType, stateProps.deviceName)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(LoginSelectShopForm)
