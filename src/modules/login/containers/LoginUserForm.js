import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import LoginUserForm from '../components/LoginUserForm'
import fetcher from 'common/models/fetcher'
import { updateSetting } from '../../setting/actions'
import SettingKeys from '../../setting/models/SettingKeys'
// import logger from 'common/utils/logger'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from '../../../common/sideEffects'
import { setLoggedDevice } from '../actions'
import { getUdid } from '../../../common/models'
import ShopRepository from '../../shop/repositories/ShopRepository'
import { REPOSITORY_CONTEXT } from '../../../common/AppEvents'
import { loadSettingsAsync, loadSettingsWithRepositoryAsync, getSettingFromState } from '../../setting/models'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import { sampleCashierMap } from '../../cashier/samples'
import { shopIds } from '../../shop/samples'
import I18n from 'i18n-js'
import { handleAxiosError } from '../../../common/errors'
import { checkDeviceIsAppling, fetchCashiers, syncShopOnLogin } from '../services'
import NetworkError from '../../../common/errors/NetworkError'

const mapDispatchToProps = dispatch => ({
  onLogin: async values => {
    const { connectingUrl, loginId, password, udid } = values
    dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.SERVER_URL, value: connectingUrl }))
    dispatch(updateSetting({ key: SettingKeys.COMMON.LOGIN_USER_NAME, value: loginId }))
    let message = null
    let fetchCashiersResult = null
    let loggedDevice = null
    const success = await loading(dispatch, async () => {
      try {
        await fetcher.loginByUser(loginId, password)
      } catch (error) {
        try {
          handleAxiosError(error)
        } catch (networkError) {
          switch (networkError.type) {
            case NetworkError.TYPE.OFFLINE:
              message = I18n.t('message.A-01-E100')
              break
            case NetworkError.TYPE.CLIENT_ERROR:
              message = I18n.t('message.J-10-E002')
              break
            case NetworkError.TYPE.BASIC_AUTH:
              message = I18n.t('message.J-10-E010')
              break
            default:
              message = I18n.t('message.J-10-E001')
          }
        }
        return false
      }

      loggedDevice = await fetcher.loginByDevice(udid)
      dispatch(setLoggedDevice(loggedDevice))

      if (loggedDevice) {
        const syncShopResult = await syncShopOnLogin(dispatch, loggedDevice.shop_id)
        if (syncShopResult.hasError) {
          message = syncShopResult.message
          return false
        }

        fetchCashiersResult = await fetchCashiers(dispatch,
          { shopId: loggedDevice.shop_id, deviceId: loggedDevice.id })

        if (fetchCashiersResult.errorMessage) {
          message = fetchCashiersResult.errorMessage
          return false
        }
      } else {
        const checkDeviceResult = await checkDeviceIsAppling(udid)
        if (checkDeviceResult.hasError) {
          message = checkDeviceResult.message
          return false
        }
      }
      return true
    })

    if (message) {
      await AlertView.showAsync(message)
    }

    if (success) {
      if (loggedDevice) {
        if (fetchCashiersResult.registeredCashier) {
          Actions.loginConfirmPage()
        } else {
          Actions.loginSelectCashierPage()
        }
      } else {
        Actions.loginSelectShopPage()
      }
    } else {
      dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: '' }))
    }
    return success
  },
  onLoginForSample: async () => {
    dispatch(updateSetting({
      key: SettingKeys.COMMON.REPOSITORY_CONTEXT,
      value: REPOSITORY_CONTEXT.SAMPLE }))
    dispatch(updateSetting({
      key: SettingKeys.COMMON.CASHIER_ID,
      value: sampleCashierMap.cashierA_1.id }))
    const settings = await loadSettingsAsync(dispatch)
    await loadSettingsWithRepositoryAsync(dispatch, settings)
    await ShopRepository.syncRemoteToLocalById(shopIds.A)
    await CashierRepository.save(sampleCashierMap.cashierA_1)
  }
})

const mapStateToProps = state => ({
  initialValues: {
    connectingUrl: getSettingFromState(state, SettingKeys.NETWORK.ACCESS.SERVER_URL),
    loginId: getSettingFromState(state, SettingKeys.COMMON.LOGIN_USER_NAME),
    password: '',
    udid: getUdid()
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginUserForm)
