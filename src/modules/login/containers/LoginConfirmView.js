import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import LoginConfirmView from '../components/LoginConfirmView'
import { getUdid } from '../../../common/models'
import ShopRepository from '../../shop/repositories/ShopRepository'
import StaffRepository from '../../staff/repositories/StaffRepository'
import { MODULE_NAME } from '../models'
import { selectShop, resetLoginStates } from '../actions'
import DeviceRepository from '../repositories/DeviceRepository'
import fetcher from 'common/models/fetcher'
import AlertView from 'common/components/widgets/AlertView'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import { loading } from '../../../common/sideEffects'
import StaffRoleRepository from '../../staff/repositories/StaffRoleRepository'
import { updateSetting } from '../../setting/actions'
import SettingKeys from '../../setting/models/SettingKeys'
import { needsOpenSale } from 'modules/openSale/services'
import I18n from 'i18n-js'
import { getCommonNetworkErrorMessage } from '../../../common/errors'

const mapDispatchToProps = dispatch => ({
  onLogin: async (deviceName, selectedCashier, currentDevice) => {
    const udid = getUdid()
    const isSucceed = await loading(dispatch, async () => {
      if (deviceName) {
        try {
          await DeviceRepository.push(deviceName, udid)
        } catch (error) {
          AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.J-10-E001')))
          return false
        }
      }
      let device = currentDevice
      if (!fetcher.hasDeviceToken) {
        try {
          device = await fetcher.loginByDevice(udid)
          if (!device) {
            AlertView.show(I18n.t('message.J-10-E006'))
            return false
          }
        } catch (error) {
          AlertView.show(I18n.t('message.J-10-E001'))
          return false
        }
      }
      if (selectedCashier.id) {
        try {
          await CashierRepository.pushDeviceIdById(selectedCashier.id, device.id)
        } catch (error) {
          AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.J-10-E007')))
          return false
        }
      }

      try {
        await StaffRepository.clearAll()
        await StaffRoleRepository.clearAll()
        await StaffRepository.syncRemoteToLocal()
        await StaffRoleRepository.syncRemoteToLocal()
        const AllStaffs = await StaffRepository.findAll()
        if (AllStaffs.length === 0) {
          AlertView.show(I18n.t('message.J-10-E008'))
          return false
        }
        dispatch(updateSetting({ key: SettingKeys.STAFF.CURRENT_STAFF, value: null }))
      } catch (error) {
        AlertView.show(I18n.t('message.J-10-E009'))
        return false
      }

      dispatch(updateSetting({ key: SettingKeys.COMMON.IS_LOGGED_IN, value: true }))

      return true
    })

    if (isSucceed) {
      if (await needsOpenSale(dispatch)) {
        Actions.openSales()
      } else {
        Actions.home()
      }
    }
  },
  onCancel: async () => {
    dispatch(resetLoginStates({}))
    dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_ID, value: '' }))
    dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: '' }))
    Actions.popTo('loginUserPage')
  },
  onInit: async (selectedShop, device) => {
    if (!selectedShop) {
      try {
        const shop = await loading(dispatch, async () => {
          await ShopRepository.syncRemoteToLocalById(device.shop_id)
          return ShopRepository.find()
        })
        dispatch(selectShop(shop))
      } catch (error) {
        AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.J-10-E005')), async () => {
          dispatch(resetLoginStates({}))
          dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_ID, value: '' }))
          dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: '' }))
          Actions.popTo('loginUserPage')
        })
      }
    }
  }
})

const mapStateToProps = state => ({
  currentDevice: state[MODULE_NAME].currentDevice,
  selectedCashier: state[MODULE_NAME].selectedCashier,
  selectedShop: state[MODULE_NAME].selectedShop,
  deviceName:
    state.form.LoginSelectShopForm &&
    state.form.LoginSelectShopForm.values &&
    state.form.LoginSelectShopForm.values.deviceName
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onInit: () => {
      return dispatchProps.onInit(stateProps.selectedShop, stateProps.currentDevice)
    },
    onLogin: (deviceName, selectedCashier) => {
      return dispatchProps.onLogin(deviceName, selectedCashier, stateProps.currentDevice)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(LoginConfirmView)
