import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import LoginSelectCashierForm from '../components/LoginSelectCashierForm'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import { MODULE_NAME } from '../models'
import { selectCashier } from '../actions'
import SettingKeys from '../../setting/models/SettingKeys'
import { updateSetting } from '../../setting/actions'
import I18n from 'i18n-js'
import { fetchCashiers } from '../services'

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      onSelectCashier: selectCashier
    },
    dispatch
  ),
  onNext: async selectedCashier => {
    await CashierRepository.clearAll()
    await CashierRepository.save(selectedCashier)
    if (selectedCashier.id) {
      dispatch(updateSetting({ key: SettingKeys.COMMON.CASHIER_ID, value: selectedCashier.id }))
      dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.CASHIER_NAME, value: selectedCashier.name }))
    } else {
      // キャッシャー名がNULLである場合、デフォルトで「テストPOS」の名が使われるようになるため、ログイン確認画面と同様の名前で登録することとする
      dispatch(updateSetting({
        key: SettingKeys.NETWORK.ACCESS.CASHIER_NAME,
        value: I18n.t('login.inventory_management_terminal')
      }))
    }
    Actions.loginConfirmPage()
  },
  onBack: () => {
    Actions.loginSelectShopPage()
  }
})

const mapStateToProps = state => ({
  currentDevice: state[MODULE_NAME].currentDevice,
  cashiers: state[MODULE_NAME].cashiers,
  selectedCashier: state[MODULE_NAME].selectedCashier,
  selectedShop: state[MODULE_NAME].selectedShop
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginSelectCashierForm)
