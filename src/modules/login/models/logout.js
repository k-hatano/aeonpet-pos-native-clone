import CashierRepository from '../../cashier/repositories/CashierRepository'
import fetcher from 'common/models/fetcher'
import { selectShop, selectCashier } from 'modules/login/actions'
import { updateSetting } from 'modules/setting/actions'
import SettingKeys from 'modules/setting/models/SettingKeys'

export default async function logout (dispatch) {
  fetcher.logout()
  CashierRepository.clearAll()

  dispatch(selectCashier(null))
  dispatch(selectShop(null))

  dispatch(updateSetting({ key: SettingKeys.COMMON.SHOP_ID, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.SHOP_NAME, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.CURRENCY, value: 'jpy' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_ID, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.BASIC_AUTH_TOKEN, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.NETWORK.DEVICE.UDID, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.CASHIER_ID, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.CASHIER_NAME, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.STAFF.CURRENT_STAFF, value: null }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE, value: 0 }))
  dispatch(updateSetting({ key: SettingKeys.STAFF.SYNC_DATE, value: '' }))
  dispatch(updateSetting({ key: SettingKeys.COMMON.IS_LOGGED_IN, value: false }))
}
