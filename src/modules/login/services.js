/**
 * @typedef {Object} FetchCashiersResult
 * @property {Object|undefined} registeredCashier
 * @property {Object|undefined} availableCashiers
 * @property {string|undefined} errorMessage
 */
import I18n from 'i18n-js'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import { selectCashier, setAvailableCashiers } from './actions'
import { updateSetting } from '../setting/actions'
import SettingKeys from '../setting/models/SettingKeys'
import ShopRepository from '../shop/repositories/ShopRepository'
import DeviceRepository from './repositories/DeviceRepository'

/**
 *
 * @param dispatch
 * @param shopId
 * @return {Promise.<{message: string, hasError: boolean}>}
 */
export async function syncShopOnLogin (dispatch, shopId) {
  let message = null
  try {
    await ShopRepository.syncRemoteToLocalById(shopId)
    const shop = await ShopRepository.find()
    if (!shop) {
      message = I18n.t('message.J-10-E005')
    }
    dispatch(updateSetting({ key: SettingKeys.COMMON.SHOP_ID, value: shop.id }))
    dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.SHOP_NAME, value: shop.name }))
    dispatch(updateSetting({ key: SettingKeys.COMMON.USEABLE_BASE_POINTS, value: shop.useable_base_points }))
  } catch (error) {
    message = I18n.t('message.J-10-E001')
  }
  return { message, hasError: !!message }
}

export async function checkDeviceIsAppling (udid) {
  let devices
  try {
    devices = await DeviceRepository.fetchByUdid(udid)
  } catch (error) {
    return {
      message: getCommonNetworkErrorMessage(error, I18n.t('message.J-10-E001')),
      hasError: true
    }
  }
  if (devices && devices.length > 0 && devices[0].status === 0) { // TODO 定数化
    return { message: I18n.t('message.J-10-E004'), hasError: true }
  }
  return { hasError: false }
}

/**
 *
 * @return {Promise.<FetchCashiersResult>}
 */
export async function fetchCashiers (dispatch, { shopId, deviceId }) {
  let cashiers
  try {
    cashiers = await CashierRepository.fetchAll()
  } catch (error) {
    return { errorMessage: getCommonNetworkErrorMessage(error, I18n.t('message.J-10-E001')) }
  }

  if (deviceId) {
    const registeredCashier = cashiers.find(cashier => cashier.device_id === deviceId)
    if (registeredCashier) {
      dispatch(selectCashier(registeredCashier))
      await CashierRepository.clearAll()
      await CashierRepository.save(registeredCashier)
      dispatch(updateSetting({ key: SettingKeys.COMMON.CASHIER_ID, value: registeredCashier.id }))
      dispatch(updateSetting({ key: SettingKeys.NETWORK.ACCESS.CASHIER_NAME, value: registeredCashier.name }))
      return { registeredCashier }
    }
  }

  const availableCashiers = cashiers.filter(cashier => cashier.device_id === null)
  if (availableCashiers.length === 0) {
    return { errorMessage: I18n.t('message.J-10-E003') }
  }

  dispatch(setAvailableCashiers(availableCashiers))
  return { availableCashiers }
}
