import SampleDeviceRepository from './sample/SampleDeviceRepository'
import StandardDeviceRepository from './standard/StandardDeviceRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class DeviceRepository {
  static _implement = new SampleDeviceRepository()

  static async push (name, uuid) {
    return this._implement.push(name, uuid)
  }

  static async fetchByUdid (udid) {
    return this._implement.fetchByUdid(udid)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardDeviceRepository()
        break

      default:
        this._implement = new SampleDeviceRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('DeviceRepository', (context) => {
  DeviceRepository.switchImplement(context)
})
