import fetcher from 'common/models/fetcher'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import { handleAxiosError } from '../../../../common/errors'

export default class StandardDeviceRepository {
  async push (name, udid) {
    try {
      return fetcher.post('devices/', { name, udid })
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchByUdid (udid) {
    try {
      const response = await fetcher.get('devices', {
        ...encodeCriteria({ udid })
      })
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
