import { waitAsync } from '../../../../common/utils/waitAsync'
import logger from 'common/utils/logger'

export default class SampleDeviceRepository {
  async push (name, uuid) {
    logger.info('SampleDeviceRepository.push name=' + name + ' uuid=' + uuid)
    waitAsync(500)
  }
}
