import I18n from 'i18n-js'
import { fetchApi2 } from '../../common/api'
import AlertView from '../../common/components/widgets/AlertView'
import { setUserToken, setDeviceToken } from '../../common/models'
import { getValidationMessages } from '../../common/utils/validations'
import { setLoggedUser, setLoggedDevice } from './actions'

export const MODULE_NAME = 'login'

export function validateUserForm (values) {
  const errors = {}
  if (!values.connectingUrl) {
    errors.connectingUrl = I18n.t('common.item_required', { item: I18n.t('login.connecting_url') })
  }
  if (!values.loginId) {
    errors.loginId = I18n.t('common.item_required', { item: I18n.t('login.login_id') })
  }
  if (!values.password) {
    errors.password = I18n.t('common.item_required', { item: I18n.t('login.password') })
  }
  return errors
}

export const USER_TYPE = {
  NONE: 0,
  ADMIN: 1,
  SHOP_MEMBER: 2
}
