import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setLoggedUser = createAction(`${MODULE_NAME}_setLoggedUser`)
export const setLoggedDevice = createAction(`${MODULE_NAME}_setLoggedDevice`)
export const setAvailableShops = createAction(`${MODULE_NAME}_setAvailableShops`)
export const selectShop = createAction(`${MODULE_NAME}_selectShop`)
export const setSearchKeyword = createAction(`${MODULE_NAME}_setSearchKeyword`)
export const filterShop = createAction(`${MODULE_NAME}_filterShop`)
export const setAvailableCashiers = createAction(`${MODULE_NAME}_setAvailableCashiers`)
export const selectCashier = createAction(`${MODULE_NAME}_selectCashier`)
export const resetLoginStates = createAction(`${MODULE_NAME}_resetLoginStates`)
export const setLoggedUserType = createAction(`${MODULE_NAME}_setLoggedUserType`)
