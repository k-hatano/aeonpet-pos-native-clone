import { StyleSheet } from 'react-native'
import baseStyleValues from '../../../common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    flex: 1
  },
  stockTakingTaskItemHeader: {
    height: 96,
    paddingBottom: 12
  },
  stockTakingTaskItems: {
    flex: 1
  },
  subHeaderWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 14,
    backgroundColor: '#f0f0f0',
    height: 48
  },
  taskItemList: {
    flex: 1
  },
  stockTakingTaskItemListHeaderRow: {
    width: '98%'
  },
  stockTakingTaskItemListHeaderLeft: {
    paddingLeft: 24
  },
  stockTakingTaskItemListHeaderButton: {
    fontSize: 20
  },
  stockTakingTaskItemListHeaderCenterText: {
    fontSize: 24,
    textAlign: 'center',
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  stockTakingViewHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    textAlign: 'center',
    borderColor: '#979797',
    borderBottomWidth: 1
  },
  stockTakingViewHeaderRow: {
    height: 48,
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12,
    borderColor: '#979797',
    borderBottomWidth: 1
  },
  stockTakingTaskItemListHeaderRight: {
    marginRight: 40
  },
  root: {
    borderColor: '#979797',
    borderTopWidth: 1,
    borderBottomWidth: 1
  },
  headerLabelsArea: {
    height: 17
  },
  closureLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#f56c23',
    width: 89,
    height: 17
  },
  labelLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#417505',
    width: 89,
    height: 17
  },
  boxLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#4a90e2',
    width: 89,
    height: 17
  },
  vintageLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#ff0068',
    width: 89,
    height: 17
  },
  invisibleLabelBase: {
    backgroundColor: baseStyleValues.mainBackgroundColor,
    width: 89,
    height: 17
  },
  productLabelArea: {
    flex: 1
  },
  productLabelWrapper: {
    flexDirection: 'row',
    flex: 7,
    justifyContent: 'space-between'
  },
  productLabelText: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    height: 16,
    color: '#ffffff',
    letterSpacing: -0.26,
    textAlign: 'center'
  },
  headerArea: {
    height: 25,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  mainArea: {
    height: 60,
    marginLeft: 20,
    justifyContent: 'center'
  },
  productNameText: {
    height: 30
  },
  aopDomaineText: {
    height: 25
  },
  subArea: {
    height: 50,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  subText: {
    fontSize: 18,
    margin: 5
  },
  subRedText: {
    fontSize: 18,
    color: 'red',
    margin: 5
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24,
    alignItems: 'center'
  },
  discountLabelArea: {
    flex: 2,
    alignItems: 'flex-end',
    marginRight: 20
  },
  footer: {
    height: 10
  }
})
