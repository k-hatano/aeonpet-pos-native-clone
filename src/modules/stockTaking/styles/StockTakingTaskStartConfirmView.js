import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: 640,
    height: 376
  },
  rowButtonStyle: {
    width: 90,
    height: 48,
    marginLeft: 16,
    fontSize: 24,
    fontWeight: 'bold'
  },
  gridStyle: {
    marginTop: 50
  },
  rowStyle: {
    minHeight: 50,
    marginLeft: 32,
    marginBottom: 30,
    flexDirection: 'row'
  },
  taskNameText: {
    fontSize: 24,
    borderColor: '#979797',
    borderWidth: 1,
    backgroundColor: baseStyleValues.subBackgroundColor,
    color: baseStyleValues.mainTextColor,
    width: 454,
    padding: 14,
    height: 48
  },
  rowHeaderText: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    paddingTop: 10,
    paddingBottom: 10,
    width: 120
  },
  staffNameText: {
    fontSize: 24,
    borderColor: '#979797',
    borderWidth: 1,
    backgroundColor: baseStyleValues.subBackgroundColor,
    color: baseStyleValues.mainTextColor,
    padding: 14,
    height: 48,
    width: 342
  },
  startTaskButton: {
    width: 344,
    height: 64,
    marginTop: 18,
    alignSelf: 'center',
    fontSize: 36,
    fontWeight: 'bold'
  }
})
