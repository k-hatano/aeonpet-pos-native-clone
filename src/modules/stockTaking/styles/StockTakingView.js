import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  stockTakingViewHeaderText: {
    fontSize: 24,
    color: '#ffffff',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  stockTakingViewContainer: {
    paddingTop: 10,
    paddingLeft: 10
  },
  stockTakingViewHeaderRow: {
    paddingTop: 12,
    paddingBottom: 12,
    borderColor: '#979797',
    borderBottomWidth: 1,
    backgroundColor: '#9b9b9b',
    height: 64,
    justifyContent: 'center'
  },
  stockTakingViewRow: {
    width: '98%',
    minHeight: 50,
    borderBottomColor: '#979797',
    borderBottomWidth: 0.8,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  stockTakingViewLeftColText: {
    paddingTop: 10,
    paddingLeft: 24,
    fontSize: 20,
    width: '30%'
  },
  stockTakingViewRightColText: {
    paddingTop: 10,
    paddingLeft: 24,
    paddingRight: 15,
    fontSize: 20,
    width: '70%',
    textAlign: 'right'
  },
  stockTakingViewButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 320,
    height: 48,
    fontSize: 32,
    fontWeight: 'bold'
  },
  stockTakingRow: {
    marginHorizontal: 21,
    paddingBottom: 10,
    borderBottomColor: '#979797',
    borderBottomWidth: 0.8
  },
  stockTakingViewRowText: {
    fontSize: 20
  },
  stockTakingViewMultipleRow: {
    minHeight: 200,
    flexDirection: 'column'
  },
  stockTakingViewRightColMultipleText: {
    paddingTop: 10,
    paddingLeft: 24,
    paddingRight: 10,
    fontSize: 20,
    textAlign: 'left'
  },
  tabNameStyle: {
    fontSize: 24,
    color: '#4a4a4a'
  },
  tabStyle: {
    height: 56,
    width: 176
  },
  stockTakingListItemContainer: {
    flexDirection: 'row'
  }
})
