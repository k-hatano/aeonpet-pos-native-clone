import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  stockTakingListHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  stockTakingListContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  stockTakingListHeaderRow: {
    height: 48,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    marginBottom: 20
  },
  stockTakingRow: {
    paddingLeft: 24,
    paddingTop: 18,
    paddingBottom: 18,
    marginRight: 10,
    marginLeft: 10,
    borderColor: '#979797',
    borderWidth: 1
  },
  stockTakingRowText: {
    fontSize: 20
  },
  stockTakingStartButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9024',
    fontSize: 20,
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    width: 120,
    height: 30
  },
  taskItemContainer: {
    justifyContent: 'space-around',
    marginRight: 10,
    marginLeft: 10,
    flexDirection: 'row'
  },
  stockTakingDateContainer: {
    width: '75%'
  },
  stockTakingStatusContainer: {
    width:'25%',
    alignItems: 'flex-end'
  }
})
