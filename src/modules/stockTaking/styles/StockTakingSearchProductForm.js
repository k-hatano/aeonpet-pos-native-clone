import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  viewContainer: {
    height: 873
  },
  switchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 50,
    marginTop: 15,
    marginRight: 10,
    marginBottom: 15
  },
  switchText: {
    fontSize: 20,
    marginRight: 5,
    width: 200,
    height: 40
  },
  searchTextInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    width: '20%',
    marginLeft: 7,
    paddingLeft: 5
  },
  searchConditions: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    width: '100%',
    marginTop: 5
  },
  productResultContainer: {
    flex: 1
  },
  productArea: {
    width: '68%',
    paddingLeft: '1%',
    paddingRight: '1%'
  },
  categoryArea: {
    width: '30%'
  },
  completeTaskButton: {
    width: 350,
    marginTop: 2,
    height: 48,
    fontSize: 32,
    fontWeight: 'bold'
  },
  footerContainer: {
    height: 80,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    borderTopWidth: 1,
    borderColor: '#979797'
  },
  goToTopButton: {
    marginTop: 6,
    height: 46,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  list: {
    backgroundColor: '#f7f7f7'
  },
  listItem: {
    height: 65,
    backgroundColor: '#f7f7f7',
    justifyContent: 'center'
  },

  // search
  searchContainer: {
    marginBottom: 10,
    height: 40,
    width: '100%',
    flexDirection: 'row'
  },
  searchBar: {
    flex: 1
  },
  searchButton: {
    marginLeft: 16,
    width: 100
  },
  searchButtonText: {
    fontSize: 24,
    paddingHorizontal: 10
  },
  searchInput: {
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    height: 38,
    paddingRight: 10,
    paddingLeft: 10
  }
})
