import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  updateStockButton: {
    width: 96,
    height: 40,
    marginRight: 15,
    fontSize: 24,
    fontWeight: 'bold'
  },
  clearButton: {
    width: 96,
    height: 40,
    marginRight: 15,
    fontSize: 24,
    fontWeight: 'bold'
  },
  itemCountText: {
    fontSize: 24
  },
  viewHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    width: '65%'
  },
  viewHeaderRow: {
    height: 60,
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12,
    alignItems: 'flex-end'
  },
  root: {
    height: 36,
    width: 164,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderColor: '#979797',
    backgroundColor: 'white'
  },
  button: {
    width: 36,
    height: 36,
    color: 'white',
    fontSize: 26,
    backgroundColor: baseStyleValues.mainColor,
    marginHorizontal: 8
  },
  number: {
    width: 60,
    height: 30,
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: 8
  },

  root: {
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderColor: '#979797',
    backgroundColor: 'white'
  },
  productBox: {
    width: 95,
    height: 30,
    borderWidth: 2,
    borderColor: baseStyleValues.mainTextColor
  },
  productBoxText: {
    textAlign: 'center'
  },
  articleNumber: {
    paddingLeft: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemName: {
    paddingLeft: 24,
    height: 44,
    justifyContent: 'center'
  },
  quantityArea: {
    paddingLeft: 24,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  subText: {
    color: baseStyleValues.subTextColor,
    fontSize: 14,
    marginRight: 20
  },
  subRedText: {
    color: 'red',
    fontSize: 14
  },
  itemNameText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  CartItemNumberFormRoot: {
    height: 36,
    width: 164,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  CartItemNumberFormButton: {
    width: 36,
    height: 36,
    color: 'white',
    fontSize: 26,
    backgroundColor: baseStyleValues.mainColor,
    marginHorizontal: 8
  },
  CartItemNumberFormNumber: {
    width: 60,
    height: 30,
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: 8
  },
  stockTakingCountViewWrapper: {
    borderRightColor: '#979797',
    borderRightWidth: 0.8,
    flex: 1
  },
  stockTakingCountViewContiner: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: 64
  },
  stockTakingCountListArea: {
    flex: 1
  },
  stockTakingCountList: {
    flex: 1
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
