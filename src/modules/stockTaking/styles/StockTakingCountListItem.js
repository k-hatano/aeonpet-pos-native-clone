import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    backgroundColor: '#f7f7f7'
  },
  headerLabelsArea: {
    height: 17
  },
  closureLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#f56c23',
    width: 89,
    height: 17
  },
  labelLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#417505',
    width: 89,
    height: 17
  },
  boxLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#4a90e2',
    width: 89,
    height: 17
  },
  vintageLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#ff0068',
    width: 89,
    height: 17
  },
  invisibleLabelBase: {
    backgroundColor: '#f7f7f7',
    width: 89,
    height: 17
  },
  productLabelArea: {
    flex: 1
  },
  productLabelWrapper: {
    flexDirection: 'row',
    flex: 7,
    justifyContent: 'space-between'
  },
  productLabelText: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    height: 16,
    color: '#ffffff',
    letterSpacing: -0.26,
    textAlign: 'center'
  },
  headerArea: {
    height: 27,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  mainArea: {
    height: 60,
    marginLeft: 20,
    justifyContent: 'center'
  },
  productNameText: {
    height: 30
  },
  aopDomaineText: {
    height: 25
  },
  subArea: {
    height: 52,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  subText: {
    fontSize: 18,
    margin: 5
  },
  subRedText: {
    fontSize: 18,
    color: 'red',
    margin: 5
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24,
    alignItems: 'center'
  },
  discountLabelArea: {
    flex: 2,
    alignItems: 'flex-end',
    marginRight: 20
  },
  footer: {
    height: 10
  }
})
