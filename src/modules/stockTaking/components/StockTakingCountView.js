import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingCountView'
import { OptionalCommandButton, ProceedButton } from '../../../common/components/elements/StandardButton'
import PropTypes from 'prop-types'
import StockTakingCountListItem from './StockTakingCountListItem'
import { getWindowScale } from '../../../common/components/ComponentDefinitions'

export default class StockTakingCountView extends Component {
  static propTypes = {
    itemCount: PropTypes.number,
    stockTakingTaskItems: PropTypes.array,
    onUpdateStock: PropTypes.func,
    onClear: PropTypes.func,
    onUpdateListItemQuantity: PropTypes.func,
    onDeleteListItem: PropTypes.func
  }

  constructor(props) {
    super(props)

    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  _scrollToEnd (contentWidth, contentHeight) {
    this._scrollViewArea.measure((areaX, areaY, areaWidth, areaHeight) => {

      if (contentHeight > areaHeight / getWindowScale()) {
        this.scrollView.scrollToEnd({ animated: true })
      }
    })
  }

  render () {
    const { itemCount, stockTakingTaskItems } = this.props
    const source = this.dataSource.cloneWithRows(stockTakingTaskItems || [])

    return (
      <View style={styles.stockTakingCountViewWrapper}>
        <View style={styles.stockTakingCountViewContiner}>
          <Text style={styles.viewHeaderText}>
            {I18n.t('stock_taking.item_count')}：{I18n.t(
              'common.amount_of_point',
              { amount: itemCount }
            )}
          </Text>
          <OptionalCommandButton
            style={styles.updateStockButton}
            text={I18n.t('stock_taking.update_stock')}
            onPress={() => this.props.onUpdateStock(stockTakingTaskItems)}
          />
          <ProceedButton
            style={styles.clearButton}
            text={I18n.t('stock_taking.clear')}
            onPress={() => this.props.onClear()}
          />
        </View>
        <View style={styles.stockTakingCountListArea} ref={ref => (this._scrollViewArea = ref)}>
          <ListView
            style={styles.stockTakingCountList}
            enableEmptySections
            dataSource={source}
            ref={ref => (this.scrollView = ref)}
            onContentSizeChange={(...args) => this._scrollToEnd(...args)}
            renderRow={stockTakingTaskItem => (
              <StockTakingCountListItem
                stockTakingTaskItem={stockTakingTaskItem}
                onUpdateListItemQuantity={this.props.onUpdateListItemQuantity}
                onDeleteListItem={this.props.onDeleteListItem}
              />
            )}
            renderSeparator={(sectionId, rowId) => (<View key={rowId} style={styles.separator} />)}
          />
        </View>
      </View>
    )
  }
}
