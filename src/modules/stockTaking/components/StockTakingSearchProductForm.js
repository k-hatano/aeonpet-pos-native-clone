import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingSearchProductForm'
import { CommandButton, ProceedButton } from '../../../common/components/elements/StandardButton'
import PropTypes from 'prop-types'
import CategoryProductView from '../../../modules/product/containers/CategoryProductView'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import {
  MAX_ITEM_QUANTITY,
  MAX_STOCK_TAKING_TASK_ITEM
} from '../models'
import AlertView from 'common/components/widgets/AlertView'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'

export default class StockTakingSearchProductForm extends Component {
  static propTypes = {
    isAutoDisplayNumericKeypad: PropTypes.bool,
    stockTakingTaskItems: PropTypes.array,
    onSearchForBarcodeReader: PropTypes.func,
    onProductSelected: PropTypes.func,
    onToggleNumerichKeyPadAutoDisplay: PropTypes.func,
    onComplete: PropTypes.func,
    onResetItem: PropTypes.func
  }

  _showAmountPad (product) {
    this.props.isAutoDisplayNumericKeypad
      ? this._openNumberPad(product, this.props.stockTakingTaskItems)
      : this.props.onProductSelected(product, this.props.stockTakingTaskItems)
  }
  _openNumberPad (product, stockTakingTaskItems) {
    if (MAX_STOCK_TAKING_TASK_ITEM > stockTakingTaskItems.length) {
      AmountPadModal.open({
        mode: AMOUNT_PAD_MODE.NUMBER,
        maxValue: MAX_ITEM_QUANTITY,
        title: product.name,
        onComplete: amount => {
          this.props.onProductSelected(product, stockTakingTaskItems, amount)
          return true
        }
      })
    } else {
      AlertView.show(I18n.t('message.O-04-E002'))
    }
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      const {
        onSearchForBarcodeReader,
        onProductSelected,
        stockTakingTaskItems,
        isAutoDisplayNumericKeypad
      } = this.props

      const products = await onSearchForBarcodeReader(body.trim())

      if (products && products.length === 1) {
        isAutoDisplayNumericKeypad
          ? this._openNumberPad(products[0], stockTakingTaskItems)
          : onProductSelected(products[0], stockTakingTaskItems)
      }
    })
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  render () {
    const {
      isAutoDisplayNumericKeypad,
      stockTakingTask,
      stockTakingTaskItems
    } = this.props

    return (
      <View>
        <View style={styles.viewContainer}>
          <View style={styles.switchWrapper}>
            <CommandButton
              style={styles.switchText}
              text={
                I18n.t('stock_taking.auto_display_numeric_keypad') +
                (isAutoDisplayNumericKeypad ? 'ON' : 'OFF')
              }
              onPress={() =>
                this.props.onToggleNumerichKeyPadAutoDisplay(
                  isAutoDisplayNumericKeypad
                )}
            />
          </View>
          <View style={styles.productResultContainer}>
            <CategoryProductView
              onProductSelected={product => this._showAmountPad(product)}
              onShowAmountPad={product => this._showAmountPad(product)}
              style={{ flex: 1, marginBottom: 100 }}
            />
          </View>
        </View>
        <View style={styles.footerContainer}>
          <ProceedButton
            style={styles.completeTaskButton}
            text={I18n.t('stock_taking.complete_task')}
            onPress={() =>
              this.props.onComplete(stockTakingTask, stockTakingTaskItems)}
          />
        </View>
      </View>
    )
  }
}
