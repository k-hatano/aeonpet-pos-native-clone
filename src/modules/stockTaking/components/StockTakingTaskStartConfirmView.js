import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingTaskStartConfirmView'
import { CommandButton, ProceedButton } from '../../../common/components/elements/StandardButton'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import PropTypes from 'prop-types'
import Modal from 'common/components/widgets/Modal'
import { showSelectStaffViewModal } from '../../staff/containers/SelectStaffViewModal'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class StockTakingTaskStartConfirmView extends Component {
  static propTypes = {
    taskName: PropTypes.string,
    staff: PropTypes.object,
    stockTaking: PropTypes.object,
    onChangeStaff: PropTypes.func,
    onChangeTaskName: PropTypes.func,
    onStartStockTakingTask: PropTypes.func
  }

  _renderModalContent () {
    const { taskName, staff } = this.props

    return (
      <View style={{ width: 640 }}>
        <View style={styles.gridStyle}>
          <View style={styles.rowStyle}>
            <View>
              <Text style={styles.rowHeaderText}>
                {I18n.t('stock_taking.task_name')}
              </Text>
            </View>
            <View>
              <TextInput
                style={styles.taskNameText}
                onChangeText={taskName => this.props.onChangeTaskName(taskName)}
                value={taskName}
              />
            </View>
          </View>
          <View style={styles.rowStyle}>
            <View>
              <Text style={styles.rowHeaderText}>
                {I18n.t('stock_taking.staff')}
              </Text>
            </View>
            <View>
              <Text style={styles.staffNameText} numberOfLines={1}>{staff && staff.name}</Text>
            </View>
            <View>
              <CommandButton
                style={styles.rowButtonStyle}
                text={I18n.t('check_sale.change')}
                onPress={() =>
                  showSelectStaffViewModal(PERMISSION_CODES.STOCK_TAKING, () => {})}
              />
            </View>
          </View>
          <View>
            <View>
              <ProceedButton
                style={styles.startTaskButton}
                text={I18n.t('stock_taking.start_task')}
                onPress={() =>
                  this.props.onStartStockTakingTask(
                    this.props.taskName,
                    this.props.staff,
                    this.props.stockTaking
                  )}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }

  render () {
    return (
      <ModalFrame
        style={styles.layoutRoot}
        title={I18n.t('stock_taking.start_stock_task_title')}
        onClose={() => Modal.close()}>
        {this._renderModalContent()}
      </ModalFrame>
    )
  }
}
