import React, { Component } from 'react'
import { View, Text, ListView, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingList'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import ConfirmView from 'common/components/widgets/ConfirmView'
import PropTypes from 'prop-types'
import { STOCK_TAKING_STATUS, getStockTakingStatusText } from '../models'
import { formatDateTime } from 'common/utils/dateTime'
import { ProceedButton } from 'common/components/elements/StandardButton'

export default class StockTakingList extends Component {
  static propTypes = {
    stockTakings: PropTypes.array,
    staff: PropTypes.object,
    onStartStockTaking: PropTypes.func,
    onSelectStockTaking: PropTypes.func
  }

  constructor(props) {
    super(props)

    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  _renderStockTakingStatus(stockTaking) {
    const statusText = getStockTakingStatusText(stockTaking.status)

    if (stockTaking.status === STOCK_TAKING_STATUS.CREATED) {
      return (
        <ProceedButton
          style={styles.stockTakingStartButton}
          onPress={() =>
            ConfirmView.show(I18n.t('message.O-03-I001'), () =>
              this.props.onStartStockTaking(
                this.props.stockTakings,
                stockTaking,
                this.props.onSelectStockTaking,
                this.props.staff
              )
            )}
          text={statusText}
        />
      )
    } else {
      return <Text style={styles.stockTakingRowText}>{statusText}</Text>
    }
  }

  _renderHeader() {
    return (
      <View style={styles.stockTakingListHeaderRow}>
        <Text style={styles.stockTakingListHeaderText}>
          {I18n.t('stock_taking.account_month')}
        </Text>
      </View>
    )
  }

  render() {
    const { stockTakings } = this.props

    if (stockTakings == null) {
      return this._renderHeader()
    }

    const source = this.dataSource.cloneWithRows(stockTakings)

    return (
      <View style={styles.stockTakingListontainer}>
        {this._renderHeader()}
        <ListView
          dataSource={source}
          renderRow={(item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                this.props.onSelectStockTaking(item)
              }}>
              <View key={item.account_month} style={styles.stockTakingRow}>
                <View>
                  <View style={styles.taskItemContainer}>
                    <View style={styles.stockTakingDateContainer}>
                      <Text style={styles.stockTakingRowText}>
                        {item.account_month &&
                          formatDateTime(
                            item.account_month,
                            I18n.t('moment.formats.year_month')
                          )}
                      </Text>
                    </View>
                    <View style={styles.stockTakingStatusContainer}>
                      {this._renderStockTakingStatus(item)}
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
          enableEmptySections />
      </View>
    )
  }
}
