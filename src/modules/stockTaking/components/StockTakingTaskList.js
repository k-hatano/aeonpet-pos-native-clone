import React, { Component } from 'react'
import { ListView, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import StockTakingTaskListItem from './StockTakingTaskListItem'

export default class StockTakingTaskList extends Component {
  static propTypes = {
    stockTakingTasks: PropTypes.array,
    onSelectStockTakingTask: PropTypes.func
  }

  constructor(props) {
    super(props)

    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  render() {
    const { stockTakingTasks } = this.props

    if (stockTakingTasks == null) {
      return null
    }

    const source = this.dataSource.cloneWithRows(stockTakingTasks)

    return (
      <ListView
        dataSource={source}
        renderRow={(item, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              this.props.onSelectStockTakingTask(item)
            }}>
            <StockTakingTaskListItem stockTakingTask={item} />
          </TouchableOpacity>
        )}
        enableEmptySections />
    )
  }
}
