import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import baseStyle from 'common/styles/baseStyle'
import styles from '../styles/StockTakingTaskItemList'
import { formatNumber, formatUnit } from 'common/utils/formats'
import Label from 'common/components/elements/Label'
import { isLiquor } from '../models'

export default class StockTakingTaskItemListItem extends Component {
  static propTypes = {
    stockTakingTaskItem: PropTypes.object
  }

  render() {
    const { stockTakingTaskItem } = this.props

    const product = stockTakingTaskItem.product

    const productVariant = product.product_variant
    const closureTypeName = productVariant.closure_type_name
      ? productVariant.closure_type_name
      : ''
    const labelVersion = productVariant.label_version
      ? productVariant.label_version
      : ''
    const boxVersion = productVariant.box_version
      ? productVariant.box_version
      : ''
    const vintage = isLiquor(productVariant)
      ? productVariant.best_by_date ? productVariant.best_by_date : ''
      : productVariant.vintage ? productVariant.vintage : ''

    return (
      <View>
        <View style={styles.root} ref={'layoutRoot'}>
          <View style={styles.headerLabelsArea}>
          </View>
          <View style={styles.headerArea}>
            <View style={[styles.productLabelArea, { flex: 3 }]}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'middle'}
                style={baseStyle.subText}>
                {product.product_code}
              </Text>
            </View>
            <View style={styles.productLabelWrapper}>
              <Label
                baseStyle={
                  (closureTypeName && styles.closureLabelBase) ||
                  styles.invisibleLabelBase
                }
                textStyle={styles.productLabelText}>
                {closureTypeName}
              </Label>
              <Label
                baseStyle={
                  (labelVersion && styles.labelLabelBase) ||
                  styles.invisibleLabelBase
                }
                textStyle={styles.productLabelText}>
                {labelVersion}
              </Label>
              <Label
                baseStyle={
                  (boxVersion && styles.boxLabelBase) ||
                  styles.invisibleLabelBase
                }
                textStyle={styles.productLabelText}>
                {boxVersion}
              </Label>
              <Label
                baseStyle={
                  (vintage && styles.vintageLabelBase) ||
                  styles.invisibleLabelBase
                }
                textStyle={styles.productLabelText}>
                {vintage}
              </Label>
            </View>
            <View style={styles.discountLabelArea}>
            </View>
          </View>
          <View style={styles.mainArea}>
            <Text numberOfLines={1} style={[baseStyle.mainText, styles.productNameText]}>
              {product.name}
            </Text>
            {!productVariant.aop_domaine ? null : (
              <Text numberOfLines={1} style={[baseStyle.commonText, styles.aopDomaineText]}>
                {productVariant.aop_domaine}
              </Text>
            )}
          </View>
          <View style={styles.subArea}>
            <View style={styles.productSubInfoArea}>
              {!productVariant.capacity ? null : (
                <Text style={baseStyle.commonText}>
                  {I18n.t('cart.capacity')} : {formatUnit(productVariant.capacity, 'ml')}
                </Text>
              )}
              <Text style={baseStyle.commonText}>
                {I18n.t('stock_taking.stock_quantity')} :{' '}
                {I18n.t('common.amount_of_point', {
                  amount: formatNumber(stockTakingTaskItem.stock_quantity)
                })}
              </Text>
            </View>
            <View style={styles.operationArea}>
              <Text style={styles.subRedText}>
                {I18n.t('stock_taking.quantity_diffs')} :{' '}
                {I18n.t('common.amount_of_point', {
                  amount: formatNumber(stockTakingTaskItem.quantity_diffs)
                })}
              </Text>
            </View>
            <View style={styles.operationArea}>
              <Text style={baseStyle.strongTextStyle}>
                {I18n.t('common.amount_of_point', {
                  amount: formatNumber(stockTakingTaskItem.quantity)
                })}
              </Text>
            </View>
          </View>
          <View style={styles.footer} />
        </View>
      </View>
    )
  }
}
