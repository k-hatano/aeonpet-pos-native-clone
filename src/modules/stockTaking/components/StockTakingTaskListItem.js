import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingView'
import PropTypes from 'prop-types'
import { formatDateTime } from 'common/utils/dateTime'

export default class StockTakingTaskListItem extends Component {
  static propTypes = {
    stockTakingTask: PropTypes.object
  }

  render() {
    const { stockTakingTask } = this.props

    return (
      <View key={stockTakingTask.name} style={styles.stockTakingRow}>
        <View>
          <View>
            <View style={styles.stockTakingListItemContainer}>
              <Text style={styles.stockTakingViewLeftColText}>
                {I18n.t('stock_taking.task_name')}
              </Text>
              <Text style={styles.stockTakingViewRightColText}>
                {stockTakingTask.name}
              </Text>
            </View>
            <View style={styles.stockTakingListItemContainer}>
              <Text style={styles.stockTakingViewLeftColText}>
                {I18n.t('stock_taking.task_staff_name')}
              </Text>
              <Text style={styles.stockTakingViewRightColText}>
                {stockTakingTask.start_staff_name}
              </Text>
            </View>
            <View style={styles.stockTakingListItemContainer}>
              <Text style={styles.stockTakingViewLeftColText}>
                {I18n.t('stock_taking.started_at')}
              </Text>
              <Text style={styles.stockTakingViewRightColText}>
                {stockTakingTask.created_at &&
                  formatDateTime(
                    stockTakingTask.created_at * 1000,
                    I18n.t('moment.formats.year_month_day_time')
                  )}
              </Text>
            </View>
            <View style={styles.stockTakingListItemContainer}>
              <Text style={styles.stockTakingViewLeftColText}>
                {I18n.t('stock_taking.finished_date')}
              </Text>
              <Text style={styles.stockTakingViewRightColText}>
                {stockTakingTask.updated_at &&
                  formatDateTime(
                    stockTakingTask.updated_at * 1000,
                    I18n.t('moment.formats.year_month_day_time')
                  )}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
