import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingView'
import PropTypes from 'prop-types'
import { getStockTakingStatusText } from '../models'
import { formatDateTime } from 'common/utils/dateTime'

export default class StockTakingDetailView extends Component {
  static propTypes = {
    stockTaking: PropTypes.object
  }

  render() {
    const { stockTaking } = this.props
    const statusText = getStockTakingStatusText(stockTaking.status)

    return (
      <View style={styles.stockTakingViewContainer}>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.account_month')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>
            {stockTaking.account_month &&
              formatDateTime(
                stockTaking.account_month,
                I18n.t('moment.formats.year_month')
              )}
          </Text>
        </View>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.slip_number')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>
            {stockTaking.slip_number}
          </Text>
        </View>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.started_at')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>
            {stockTaking.started_at &&
              formatDateTime(
                stockTaking.started_at * 1000,
                I18n.t('moment.formats.year_month_day_time')
              )}
          </Text>
        </View>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.staff_name')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>
            {stockTaking.staff_name}
          </Text>
        </View>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.pre_fixed_at')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>
            {stockTaking.pre_fixed_at &&
              formatDateTime(
                stockTaking.pre_fixed_at * 1000,
                I18n.t('moment.formats.year_month_day_time')
              )}
          </Text>
        </View>
        <View style={styles.stockTakingViewRow}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.status')}
          </Text>
          <Text style={styles.stockTakingViewRightColText}>{statusText}</Text>
        </View>
        <View
          style={[
            styles.stockTakingViewRow,
            styles.stockTakingViewMultipleRow
          ]}>
          <Text style={styles.stockTakingViewLeftColText}>
            {I18n.t('stock_taking.note')}
          </Text>
          <Text style={styles.stockTakingViewRightColMultipleText}>
            {stockTaking.note}
          </Text>
        </View>
      </View>
    )
  }
}
