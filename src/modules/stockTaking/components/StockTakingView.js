import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingView'
import TabPanel from '../../../common/components/widgets/TabPanel'
import { Col, Row, Grid } from 'react-native-easy-grid'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import PropTypes from 'prop-types'
import StockTakingTaskList from './StockTakingTaskList'
import StockTakingDetailView from './StockTakingDetailView'
import { STOCK_TAKING_STATUS } from '../models'
import ConfirmView from '../../../common/components/widgets/ConfirmView'
import { ProceedButton } from 'common/components/elements/StandardButton'

export default class StockTakingView extends Component {
  static propTypes = {
    stockTaking: PropTypes.object,
    stockTakingTasks: PropTypes.array,
    stockTakingUnpushedTasks: PropTypes.array,
    onPushUnpushedTasks: PropTypes.func,
    onReadyStockTakingTask: PropTypes.func,
    onSelectStockTakingTask: PropTypes.func,
    tabIndex: PropTypes.number,
    selectedTab: PropTypes.func
  }

  constructor(props) {
    super(props)

    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  _renderStockInfo() {
    return (
      <View
        tabName={I18n.t('stock_taking.stock_info')}
        onTabClick={tabId => this.props.selectedTab(tabId)}>
        <StockTakingDetailView stockTaking={this.props.stockTaking} />
      </View>
    )
  }

  _renderTaskList() {
    return (
      <View
        tabName={I18n.t('stock_taking.task_list')}
        onTabClick={tabId => this.props.selectedTab(tabId)}>
        <View style={styles.stockTakingViewContainer}>
          <StockTakingTaskList
            stockTakingTasks={this.props.stockTakingTasks}
            onSelectStockTakingTask={item =>
              this.props.onSelectStockTakingTask(item, false)}
          />
        </View>
      </View>
    )
  }

  _renderUnpushedTaskList() {
    const { stockTakingUnpushedTasks } = this.props
    if (
      !(
        stockTakingUnpushedTasks &&
        this.props.stockTakingUnpushedTasks.length > 0
      )
    ) {
      return <View />
    }

    return (
      <View
        tabName={I18n.t('stock_taking.unpushed_task')}
        onTabClick={tabId => this.props.selectedTab(tabId)}>
        <View style={styles.stockTakingViewContainer}>
          <StockTakingTaskList
            stockTakingTasks={this.props.stockTakingUnpushedTasks}
            onSelectStockTakingTask={item =>
              this.props.onSelectStockTakingTask(item, true)}
          />
        </View>
      </View>
    )
  }

  _renderHeader() {
    return (
      <View style={styles.stockTakingViewHeaderRow}>
        <Text style={styles.stockTakingViewHeaderText}>
          {I18n.t('stock_taking.stock_info')}
        </Text>
      </View>
    )
  }

  _renderTabs() {
    if (Object.keys(this.props.stockTaking).length > 0) {
      return (
        <TabPanel tabStyle={styles.tabStyle} tabNameStyle={styles.tabNameStyle} selectedTab={this.props.tabIndex}>
          {this._renderStockInfo()}
          {this._renderTaskList()}
          {this._renderUnpushedTaskList()}
        </TabPanel>
      )
    } else {
      return null
    }
  }

  _renderReadyStockTakingTask() {
    const { stockTaking } = this.props
    if (stockTaking.status === STOCK_TAKING_STATUS.STOCKING) {
      return (
        <Col
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 20
          }}>
          <ProceedButton
            onPress={() =>
              this.props.onReadyStockTakingTask(
                this.props.staff,
                I18n.t('stock_taking.task_name_prefix') +
                  ' ' +
                  (this.props.stockTakingTasks.length + 1)
              )}
            style={styles.stockTakingViewButton}
            text={I18n.t('stock_taking.start_stock_task')} />
        </Col>
      )
    } else {
      return (
        <Col
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 20
          }}
        />
      )
    }
  }

  _renderUnpushedButton () {
    if (
      this.props.stockTakingUnpushedTasks &&
      this.props.stockTakingUnpushedTasks.length > 0
    ) {
      return (
        <Col
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 20
          }}>
          <ProceedButton
            onPress={() =>
              ConfirmView.show(I18n.t('message.O-02-I001'), () =>
                this.props.onPushUnpushedTasks(
                  this.props.stockTaking,
                  this.props.stockTakingUnpushedTasks))}
            style={styles.stockTakingViewButton}
            text={I18n.t('stock_taking.push_unpushed_task')} />
        </Col>
      )
    } else {
      return (
        <Col
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 20
          }}
        />
      )
    }
  }

  render() {
    const { stockTaking } = this.props
    if (stockTaking == null) {
      return this._renderHeader()
    }

    return (
      <Grid>
        {this._renderHeader()}
        <Row size={3}>{this._renderTabs()}</Row>
        <Row size={1}>
          {this._renderUnpushedButton()}
          {this._renderReadyStockTakingTask()}
        </Row>
      </Grid>
    )
  }
}
