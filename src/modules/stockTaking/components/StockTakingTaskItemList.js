import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockTakingTaskItemList'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import PropTypes from 'prop-types'
import StockTakingTaskItemListItem from './StockTakingTaskItemListItem'

export default class StockTakingTaskItemList extends Component {
  static PropTypes = {
    stockTakingTaskItems: PropTypes.array,
    onBack: PropTypes.func
  }

  _renderHeader () {
    return (
      <View>
        <View style={styles.stockTakingViewHeaderRow}>
          <Text style={styles.stockTakingViewHeaderText}>
            {I18n.t('stock_taking.stock_info')}
          </Text>
        </View>
        <View style={styles.subHeaderWrapper}>
          <View style={styles.stockTakingTaskItemListHeaderLeft}>
            <TouchableOpacitySfx onPress={() => this.props.onBack()}>
              <Text style={styles.stockTakingTaskItemListHeaderButton}>
                {I18n.t('stock_taking.back')}
              </Text>
            </TouchableOpacitySfx>
          </View>
          <View style={styles.stockTakingTaskItemListHeaderCenter}>
            <Text style={styles.stockTakingTaskItemListHeaderCenterText}>
              {I18n.t('stock_taking.task_Detail')}
            </Text>
          </View>
          <View style={styles.stockTakingTaskItemListHeaderRight} />
        </View>
      </View>
    )
  }

  _renderTaskItem () {
    const { stockTakingTaskItems } = this.props
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const source = dataSource.cloneWithRows(stockTakingTaskItems)

    return (
      <ListView
        enableEmptySections
        dataSource={source}
        style={styles.taskItemList}
        renderRow={stockTakingTaskItem => (
          <StockTakingTaskItemListItem
            stockTakingTaskItem={stockTakingTaskItem}
          />
        )}
      />
    )
  }

  render () {
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.stockTakingTaskItemHeader}>
          {this._renderHeader()}
        </View>
        <View style={styles.stockTakingTaskItems}>
          {this._renderTaskItem()}
        </View>
      </View>
    )
  }
}
