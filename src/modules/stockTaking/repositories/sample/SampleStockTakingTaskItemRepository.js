import { sampleProductMap } from '../../../product/samples'
import { sampleStockTakingTasks, unpushedStockTakingTasks} from './SampleStockTakingTaskRepository'
import generateUuid from '../../../../common/utils/generateUuid'
import * as _ from 'underscore'

export const sampleStockTakingTaskItemMap = [
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[0].id,
    product_id: sampleProductMap.standardA.id,
    product_variant_id: '',
    quantity: 1,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[1].id,
    product_id: sampleProductMap.standardB.id,
    product_variant_id: '',
    quantity: 2,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[2].id,
    product_id: sampleProductMap.standardC.id,
    product_variant_id: '',
    quantity: 3,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[3].id,
    product_id: sampleProductMap.bundle_s_1_1.id,
    product_variant_id: '',
    quantity: 4,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[4].id,
    product_id: sampleProductMap.bundle_s_1_2.id,
    product_variant_id: '',
    quantity: 5,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: sampleStockTakingTasks[5].id,
    product_id: sampleProductMap.bundle_s_2_A_1.id,
    product_variant_id: '',
    quantity: 6,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[0].id,
    product_id: sampleProductMap.bundle_s_2_B_1.id,
    product_variant_id: '',
    quantity: 7,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[1].id,
    product_id: sampleProductMap.sale_a_1.id,
    product_variant_id: '',
    quantity: 8,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[2].id,
    product_id: sampleProductMap.sale_a_2.id,
    product_variant_id: '',
    quantity: 9,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[3].id,
    product_id: sampleProductMap.sale_bundle.id,
    product_variant_id: '',
    quantity: 10,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[4].id,
    product_id: sampleProductMap.taxfreeExpentable2.id,
    product_variant_id: '',
    quantity: 11,
    quantity_diffs: 999,
    stock_quantity: 1000
  },
  {
    id: generateUuid(),
    stock_taking_task_id: unpushedStockTakingTasks[5].id,
    product_id: sampleProductMap.taxfreeExpentable1.id,
    product_variant_id: '',
    quantity: 12,
    quantity_diffs: 999,
    stock_quantity: 1000
  }
]

export const sampleStockTakingTaskItems = _.values(sampleStockTakingTaskItemMap)

export default class SampleStockTakingTaskItemRepository {
  async fetchByStockTakingTaskId (stockTakingTaskId) {
    const stockTakingTaskItems = sampleStockTakingTaskItems.filter(stockTakingTaskItem => stockTakingTaskItem.stock_taking_task_id === stockTakingTaskId)
    console.log('SampleStockTakingTaskItemRepository -- fetchByStockTakingTaskId', stockTakingTaskItems)
    return stockTakingTaskItems
  }
}
