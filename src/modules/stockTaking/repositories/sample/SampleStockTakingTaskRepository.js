import { sampleProductMap } from '../../../product/samples'
import { sampleStockTakings } from './SampleStockTakingRepository'
import generateUuid from '../../../../common/utils/generateUuid'
import * as _ from 'underscore'

export const sampleStockTakingTaskMap = [
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[0].id,
    name: '商品棚Aの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000,
    stock_taking_task_items: [
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: '',
        quantity: 1,
        quantity_diffs: 999
      }
    ]
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[0].id,
    name: '商品棚Bの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[0].id,
    name: '商品棚Cの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[1].id,
    name: '商品棚Cの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[2].id,
    name: '商品棚Cの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[3].id,
    name: '商品棚Cの棚卸',
    status: '1',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  }
]

export const unpushedStockTakingTaskMap = [
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[0].id,
    name: 'Unpushed商品棚Aの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000,
    stock_taking_task_items: [
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: '',
        quantity: 1,
        quantity_diffs: 999
      },
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardB.id,
        product_variant_id: '',
        quantity: 2,
        quantity_diffs: 999
      }
    ]
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[1].id,
    name: 'Unpushed商品棚Bの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000,
    stock_taking_task_items: [
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: '',
        quantity: 1,
        quantity_diffs: 999
      },
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardB.id,
        product_variant_id: '',
        quantity: 2,
        quantity_diffs: 999
      },
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: '',
        quantity: 1,
        quantity_diffs: 999
      },
      {
        id: generateUuid(),
        stock_taking_task_id: 1,
        product_id: sampleProductMap.standardB.id,
        product_variant_id: '',
        quantity: 2,
        quantity_diffs: 999
      }
    ]
  },
  {
    id: generateUuid(),
    stock_taking_id: [1],
    name: 'Unpushed商品棚Cの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[2].id,
    name: 'Unpushed商品棚Cの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[3].id,
    name: 'Unpushed商品棚Cの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  },
  {
    id: generateUuid(),
    stock_taking_id: sampleStockTakings[3].id,
    name: 'Unpushed商品棚Cの棚卸',
    status: '0',
    staff_id: '',
    staff_name: '大工',
    created_at: 1494378000,
    updated_at: 1494378000
  }
]

export const sampleStockTakingTasks = _.values(sampleStockTakingTaskMap)
export const unpushedStockTakingTasks = _.values(unpushedStockTakingTaskMap)

export default class SampleStockTakingTaskRepository {
  _unpushedStockTakingTasks = []
  _pushChangeStocktakingTask = {}

  async fetchByStockTakingId(stockTakingId) {
    const fetchIdStock = sampleStockTakingTasks.filter(
      stockTakingTask => stockTakingTask.stock_taking_id === stockTakingId
    )
    console.log(
      'SampleStockTakingTaskRepository -- fetchByStockTakingId',
      fetchIdStock
    )
    return fetchIdStock
  }

  async findUnpushedByStockTakingId(stockTakingId) {
    const fetchIdUnpushedStock = this._unpushedStockTakingTasks.filter(
      stockTakingTask => stockTakingTask.stock_taking_id === stockTakingId
    )
    console.log(
      'SampleStockTakingTaskRepository -- findUnpushedByStockTakingId',
      fetchIdUnpushedStock
    )
    return fetchIdUnpushedStock
  }

  async deleteByStockTakingId(stockTakingId) {
    console.log(
      'SampleStockTakingTaskRepository -- deleteByStockTakingId',
      stockTakingId
    )
  }

  async push(stockTakingTask) {
    this._pushChangeStocktakingTask = stockTakingTask
    console.log('SampleStockTakingTaskRepository -- push', stockTakingTask)

    return stockTakingTask
  }

  async save(stockTakingTask) {
    // todo: DELETE,POST localDB(unpushedStockTaking)
    let _unpushedStockTakingTaskItem = []

    for (const stockTakingTaskItem of stockTakingTask.stock_taking_task_items) {
      _unpushedStockTakingTaskItem.push({
        id: generateUuid(),
        stock_taking_task_id: stockTakingTask.id,
        product_id: stockTakingTaskItem.product.id,
        product_variant_id: stockTakingTaskItem.product.product_variants[0].id,
        is_parent_product: stockTakingTaskItem.product.is_parent_product,
        quantity: stockTakingTaskItem.quantity,
        quantity_diffs: stockTakingTaskItem.quantity_diffs,
        stock_quantity: stockTakingTaskItem.stock_quantity
      })
    }

    this._unpushedStockTakingTasks.push({
      id: stockTakingTask.id,
      stock_taking_id: stockTakingTask.stock_taking_id,
      name: stockTakingTask.name,
      status: stockTakingTask.status,
      staff_id: stockTakingTask.staff.id,
      staff_name: stockTakingTask.staff.name,
      staff_code: stockTakingTask.staff.staff_code,
      created_at: Math.floor(new Date().getTime() / 1000),
      updated_at: Math.floor(new Date().getTime() / 1000),
      is_pushed: 0,
      stock_taking_task_items: _unpushedStockTakingTaskItem
    })

    console.log('SampleStockTakingTaskRepository -- save', stockTakingTask)
  }

  async fetchAllByStockTakingId(stockTakingId) {
    const fetchIdStock = sampleStockTakingTasks.filter(
      stockTakingTask => stockTakingTask.stock_taking_id === stockTakingId
    )
    console.log(
      'SampleStockTakingTaskRepository -- fetchByStockTakingId',
      fetchIdStock
    )
    return fetchIdStock
  }
}
