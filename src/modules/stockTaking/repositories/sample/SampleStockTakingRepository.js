import generateUuid from '../../../../common/utils/generateUuid'
import * as _ from 'underscore'

export const sampleStockTakingMap = [
  {
    id: generateUuid(),
    account_month: '2017/07/01',
    slip_number: '12345678',
    status: 0,
    shop_id: '',
    staff_id: '',
    staff_name: '大工1',
    note: 'メモテスト1',
    started_at: 1499994000,
    pre_fixed_at: 1499994000,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo1'
  },
  {
    id: generateUuid(),
    account_month: '2017/06/01',
    slip_number: '12345678',
    status: 1,
    shop_id: '',
    staff_id: '',
    staff_name: '大工2',
    note: 'メモテスト2',
    started_at: 1504757506,
    pre_fixed_at: 1504757506,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo2'
  },
  {
    id: generateUuid(),
    account_month: '2017/05/01',
    slip_number: '1235452546',
    status: 2,
    shop_id: '',
    staff_id: '',
    staff_name: '大工3',
    note: 'メモテスト3',
    started_at: 1494378000,
    pre_fixed_at: 1494378000,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo3'
  },
  {
    id: generateUuid(),
    account_month: '2017/04/01',
    slip_number: '1235452546',
    status: 3,
    shop_id: '',
    staff_id: '',
    staff_name: '大工4',
    note: 'メモテスト4',
    started_at: 1491613200,
    pre_fixed_at: 1491613200,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo4'
  },
  {
    id: generateUuid(),
    account_month: '2017/03/01',
    slip_number: '1235452546',
    status: 3,
    shop_id: '',
    staff_id: '',
    staff_name: '大工5',
    note: 'メモテスト5',
    started_at: 1488762000,
    pre_fixed_at: 1488762000,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo5'
  },
  {
    id: generateUuid(),
    account_month: '2017/02/01',
    slip_number: '1235452546',
    status: 3,
    shop_id: '',
    staff_id: '',
    staff_name: '大工6',
    note: 'メモテスト6',
    started_at: 1486170000,
    pre_fixed_at: 1486170000,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo6'
  },
  {
    id: generateUuid(),
    account_month: '2017/01/01',
    slip_number: '1235452546',
    status: 3,
    shop_id: '',
    staff_id: '',
    staff_name: '大工7',
    note: 'メモテスト7',
    started_at: 1483318800,
    pre_fixed_at: 1483318800,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo7'
  },
  {
    id: generateUuid(),
    account_month: '2016/12/01',
    slip_number: '1235452546',
    status: 3,
    shop_id: '',
    staff_id: '',
    staff_name: '大工8',
    note: 'メモテスト8',
    started_at: 1482541200,
    pre_fixed_at: 1482541200,
    fixed_at: '',
    created_at: '',
    updated_at: '',
    sequence: 'タスクNo8'
  }
]

export const sampleStockTakings = _.values(sampleStockTakingMap)

export default class SampleStockTakingRepository {
  _push = []
  async fetchAll () {
    console.log('SampleStockTakingRepository -- fetchAll')
    return sampleStockTakings
  }

  async fetchById(stockTakingId) {
    console.log('SampleStockTakingRepository -- fetchAll', stockTakingId)
    return sampleStockTakings.filter((item) => item.id === stockTakingId)[0]
  }

  async push (stockTaking) {
    this._push.push(stockTaking)
    console.log('SampleStockTakingRepository -- push', stockTaking)
  }
}
