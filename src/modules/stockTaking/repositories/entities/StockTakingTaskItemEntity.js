import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const StockTakingTaskItem = sequelize.define(
  'StockTakingTaskItem',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    stock_taking_task_id: Sequelize.STRING,
    product_id: Sequelize.STRING,
    product_variant_id: Sequelize.STRING,
    quantity: Sequelize.INTEGER,
    quantity_diffs: Sequelize.INTEGER,
    stock_quantity: Sequelize.INTEGER
  }, {
    tableName: 'stock_taking_task_items',
    underscored: true,
    timestamps: false
  }
)

export default StockTakingTaskItem
