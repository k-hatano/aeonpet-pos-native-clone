import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import StockTakingTaskItem from './StockTakingTaskItemEntity'

const StockTakingTask = sequelize.define(
  'StockTakingTask',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    stock_taking_id: Sequelize.STRING,
    name: Sequelize.STRING,
    status: Sequelize.INTEGER,
    staff_id: Sequelize.STRING,
    staff_name: Sequelize.STRING,
    created_at: Sequelize.INTEGER,
    updated_at: Sequelize.INTEGER,
    is_pushed: Sequelize.INTEGER
  },
  {
    tableName: 'stock_taking_tasks',
    underscored: true,
    timestamps: false,
    scopes: {
      withStockTakingTaskItems: {
        include: [
          {
            model: StockTakingTaskItem,
            as: 'stock_taking_task_items'
          }
        ]
      }
    }
  }
)

StockTakingTask.hasMany(StockTakingTaskItem, {
  foreignKey: 'stock_taking_task_id',
  as: 'stock_taking_task_items'
})

export default StockTakingTask
