import SampleStockTakingTaskRepository from './sample/SampleStockTakingTaskRepository'
import StandardStockTakingTaskRepository from './standard/StandardStockTakingTaskRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockTakingTaskRepository {
  static _implement = new SampleStockTakingTaskRepository()

  static async fetchByStockTakingId(stockTakingId) {
    return this._implement.fetchByStockTakingId(stockTakingId)
  }

  static async findUnpushedByStockTakingId(stockTakingId) {
    return this._implement.findUnpushedByStockTakingId(stockTakingId)
  }

  static async deleteByStockTakingId(stockTakingId) {
    return this._implement.deleteByStockTakingId(stockTakingId)
  }

  static async push(stockTakingTask) {
    return this._implement.push(stockTakingTask)
  }

  static async save(stockTakingTask) {
    this._implement.save(stockTakingTask)
  }

  static async fetchAllByStockTakingId(stockTakingId) {
    return this._implement.fetchAllByStockTakingId(stockTakingId)
  }

  static switchImplement(context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockTakingTaskRepository()
        break

      default:
        this._implement = new SampleStockTakingTaskRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StockTakingTaskRepository', context => {
  StockTakingTaskRepository.switchImplement(context)
})
