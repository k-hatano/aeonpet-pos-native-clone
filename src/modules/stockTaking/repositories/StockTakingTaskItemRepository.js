import SampleStockTakingTaskItemRepository from './sample/SampleStockTakingTaskItemRepository'
import StandardTakingTaskItemRepository from './standard/StandardTakingTaskItemRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockTakingTaskItemRepository {
  static _implement = new SampleStockTakingTaskItemRepository()

  static async fetchByStockTakingTaskId(id) {
    return this._implement.fetchByStockTakingTaskId(id)
  }

  static switchImplement(context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardTakingTaskItemRepository()
        break

      default:
        this._implement = new SampleStockTakingTaskItemRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged(
  'StockTakingTaskItemRepository',
  context => {
    StockTakingTaskItemRepository.switchImplement(context)
  }
)
