import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'
import generateUuid from '../../../../common/utils/generateUuid'
import StockTakingTask from '../entities/StockTakingTaskEntity'
import StockTakingTaskItem from '../entities/StockTakingTaskItemEntity'
import { encodeCriteria } from '../../../../common/utils/searchUtils'

export default class StandardStockTakingTaskRepository {
  async fetchByStockTakingId(stockTakingId) {
    try {
      const result = await fetcher.get('stock-taking-tasks?limit=1000', {
        ...encodeCriteria({
          stock_taking_id: stockTakingId,
          status: 1
        }),
        orderBy: 'updated_at',
        sortedBy: 'desc'
      })
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async findUnpushedByStockTakingId(stockTakingId) {
    const stockTakings = await StockTakingTask.findAll({
      where: {
        stock_taking_id: stockTakingId,
        is_pushed: 0
      },
      include: [
        {
          model: StockTakingTaskItem,
          as: 'stock_taking_task_items',
          required: true
        }
      ]
    })
    return stockTakings.map(stockTaking =>
      this._formatStockTakingToDto(stockTaking)
    )
  }

  _formatStockTakingToDto(model) {
    const stockTaking = model.dataValues
    stockTaking.stock_taking_task_items = model.stock_taking_task_items.map(
      taskItem => taskItem.dataValues
    )
    return stockTaking
  }

  async deleteByStockTakingId(stockTakingId) {
    const stockTakingTask = await StockTakingTask.find({
      where: {
        stock_taking_id: stockTakingId
      }
    })
    StockTakingTaskItem.destroy({
      where: {
        stock_taking_task_id: stockTakingTask.id
      }
    })

    StockTakingTask.destroy({
      where: {
        stock_taking_id: stockTakingId
      }
    })
  }

  async push(stockTakingTask) {
    try {
      if (stockTakingTask.stock_taking_task_items) {
        const staff_id = stockTakingTask.staff ? stockTakingTask.staff.id : stockTakingTask.staff_id
        const staff_name = stockTakingTask.staff ? stockTakingTask.staff.name : stockTakingTask.staff_name
        await fetcher.put('stock-taking-tasks/' + stockTakingTask.id, {
          status: stockTakingTask.status,
          complete_staff_id: staff_id,
          complete_staff_name: staff_name
        })
        for (const stockTakingTaskItem of stockTakingTask.stock_taking_task_items) {
          // 未送信オブジェクト判定
          if (stockTakingTaskItem.quantity_diffs != null) {
            await fetcher.post('stock-taking-task-items', {
              stock_taking_task_id: stockTakingTask.id,
              product_id: stockTakingTaskItem.product_id,
              product_variant_id: stockTakingTaskItem.product_variant_id,
              is_parent_product: stockTakingTaskItem.is_parent_product,
              quantity: stockTakingTaskItem.quantity,
              quantity_diffs: stockTakingTaskItem.quantity_diffs,
              stock_quantity: stockTakingTaskItem.stock_quantity
            })
          } else {
            await fetcher.post('stock-taking-task-items', {
              stock_taking_task_id: stockTakingTask.id,
              product_id: stockTakingTaskItem.product.id,
              product_variant_id:
                stockTakingTaskItem.product.product_variant.id,
              is_parent_product: stockTakingTaskItem.product.is_parent_product,
              quantity: stockTakingTaskItem.quantity,
              quantity_diffs: stockTakingTaskItem.quantity - stockTakingTaskItem.stock_quantity,
              stock_quantity: stockTakingTaskItem.stock_quantity
            })
          }
        }
      } else {
        const result = await fetcher.post('stock-taking-tasks', stockTakingTask)
        return result.data
      }
    } catch (error) {
      if (error.response && error.response.status === 404) {
        throw new Error(404)
      } else {
        handleAxiosError(error)
      }
    }
  }

  async fetchAllByStockTakingId(stockTakingId) {
    try {
      const result = await fetcher.get('stock-taking-tasks', {
        ...encodeCriteria({
          stock_taking_id: stockTakingId
        })
      })
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async save(stockTakingTask) {
    try {
      await StockTakingTask.create({
        id: stockTakingTask.id,
        stock_taking_id: stockTakingTask.stock_taking_id,
        name: stockTakingTask.name,
        status: stockTakingTask.status,
        staff_id: stockTakingTask.staff.id,
        staff_name: stockTakingTask.staff.name,
        staff_code: stockTakingTask.staff.staff_code,
        created_at: stockTakingTask.created_at,
        updated_at: Math.floor(new Date().getTime() / 1000),
        is_pushed: 0
      })
      for (const stockTakingTaskItem of stockTakingTask.stock_taking_task_items) {
        await StockTakingTaskItem.create({
          id: generateUuid(),
          stock_taking_task_id: stockTakingTask.id,
          product_id: stockTakingTaskItem.product.id,
          product_variant_id: stockTakingTaskItem.product.product_variant.id,
          is_parent_product: stockTakingTaskItem.product.is_parent_product,
          quantity: stockTakingTaskItem.quantity,
          quantity_diffs: stockTakingTaskItem.quantity - stockTakingTaskItem.stock_quantity,
          stock_quantity: stockTakingTaskItem.stock_quantity
        })
      }
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
