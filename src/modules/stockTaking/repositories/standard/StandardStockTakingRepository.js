import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'

import generateUuid from '../../../../common/utils/generateUuid'

export default class StandardStockTakingRepository {
  async fetchAll() {
    try {
      const result = await fetcher.get('stock-takings', {
        orderBy: 'account_month',
        sortedBy: 'desc'
      })
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchById(stockTakingId) {
    try {
      const result = await fetcher.get('stock-takings/' + stockTakingId)
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async push(stockTaking) {
    try {
      await fetcher.put('stock-takings/' + stockTaking.id, stockTaking)
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
