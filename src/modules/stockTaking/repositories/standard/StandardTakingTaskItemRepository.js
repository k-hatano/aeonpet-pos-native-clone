import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'
import generateUuid from '../../../../common/utils/generateUuid'
import { encodeCriteria } from '../../../../common/utils/searchUtils'

export default class StandardStockTakingTaskItemRepository {
  async fetchByStockTakingTaskId (stockTakingTaskId) {
  	try {
      const result = await fetcher.get('stock-taking-task-items', encodeCriteria({stock_taking_task_id: stockTakingTaskId}))

      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
