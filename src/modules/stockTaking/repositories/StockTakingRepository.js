import SampleStockTakingRepository from './sample/SampleStockTakingRepository'
import StandardStockTakingRepository from './standard/StandardStockTakingRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockTakingRepository {
  static _implement = new SampleStockTakingRepository()

  static async fetchAll() {
    return this._implement.fetchAll()
  }

  static async fetchById(stockTakingId) {
    return this._implement.fetchById(stockTakingId)
  }

  static async push(stockTaking) {
    this._implement.push(stockTaking)
  }

  static switchImplement(context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockTakingRepository()
        break

      default:
        this._implement = new SampleStockTakingRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StockTakingRepository', context => {
  StockTakingRepository.switchImplement(context)
})
