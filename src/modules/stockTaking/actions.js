import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const resetItemCount = createAction(`${MODULE_NAME}_resetItemCount`)
export const setStockTakings = createAction(`${MODULE_NAME}_setStockTakings`)
export const setCurrentStockTaking = createAction(`${MODULE_NAME}_setCurrentStockTaking`)
export const setCurrentStockTakingTask = createAction(`${MODULE_NAME}_setCurrentStockTakingTask`)
export const setStockTakingTasks = createAction(`${MODULE_NAME}_setStockTakingTasks`)
export const setStockTakingUnpushedTasks = createAction(`${MODULE_NAME}_setStockTakingUnpushedTasks`)
export const setIsAutoDisplayNumericKeypad = createAction(`${MODULE_NAME}_setIsAutoDisplayNumericKeypad`)
export const setStockTakingTaskItems = createAction(`${MODULE_NAME}_setStockTakingTaskItems`)
export const updateStockTakingTaskItems = createAction(`${MODULE_NAME}_updateStockTakingTaskItems`)
export const setTaskName = createAction(`${MODULE_NAME}_setTaskName`)
export const setItemCount = createAction(`${MODULE_NAME}_setItemCount`)
export const setTabIndex = createAction(`${MODULE_NAME}_setTabIndex`)
export const deleteStockTakingTaskItem = createAction(`${MODULE_NAME}_deleteStockTakingTaskItem`)
