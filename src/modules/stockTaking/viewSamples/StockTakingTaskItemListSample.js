import StockTakingTaskItemList from '../components/StockTakingTaskItemList'

const FirstProduct = {
  product: {
    id: 1,
    name: '商品名あいうえおかきくけこさしすせそ'
  },
  product_variants: {
    vintage: '2017',
    closure: 'コルク',
    label_version: '2017-01',
    box_version: '2017-02',
    article_number: '1234567890123'
  },
  stock_quantity: 10000,
  quantity_diffs: -9999,
  quantity: 999
}
const SecondProduct = {
  product: {
    id: 2,
    name: '商品名あいうえおかきくけこさしすせそ'
  },
  product_variants: {
    vintage: '2017',
    closure: 'コルク',
    label_version: '2017-01',
    box_version: '2017-02',
    article_number: '1234567890123'
  },
  stock_quantity: 10000,
  quantity_diffs: -9999,
  quantity: 999
}

export default {
  name: 'StockTakingTaskItemList',
  component: StockTakingTaskItemList,
  properties: [
    {
      title: 'Default',
      property: {
        stockTakingTaskItems: [FirstProduct, SecondProduct]
      }
    },
    {
      title: 'Empty',
      property: {
        stockTakingTaskItems: []
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
