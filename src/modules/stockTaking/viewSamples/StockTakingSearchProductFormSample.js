import StockTakingSearchProductForm from '../components/StockTakingSearchProductForm'
import I18n from 'i18n-js'

export default {
  name: 'StockTakingSearchProductForm',
  component: StockTakingSearchProductForm,
  properties: [
    {
      title: 'Default',
      property: {
        isAutoDisplayNumericKeypad: true
      }
    },
    {
      title: 'Empty',
      property: {
        isAutoDisplayNumericKeypad: false
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
