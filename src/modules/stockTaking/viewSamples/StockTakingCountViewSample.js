import StockTakingCountView from '../components/StockTakingCountView'

export default {
  name: 'StockTakingCountView',
  component: StockTakingCountView,
  properties: [
    {
      title: 'Default',
      property: {
        itemCount: 4,
        stockTakingTaskItems: [
          {
            product: {
              id: 1,
              name: '商品名あいうえおかきくけこさしすせそ1'
            },
            product_variants: {
              vintage: '2017',
              closure: 'コルク',
              label_version: '2017-01',
              box_version: '2017-02',
              article_number: '1234567890123'
            },
            stock_taking_task_items: {
              stock_quantity: 10000,
              quantity: 999,
              price: 10000
            }
          },
          {
            product: {
              id: 1,
              name: '商品名あいうえおかきくけこさしすせそ2'
            },
            product_variants: {
              vintage: '2017',
              closure: 'コルク',
              label_version: '2017-01',
              box_version: '2017-02',
              article_number: '1234567890123'
            },
            stock_taking_task_items: {
              stock_quantity: 10000,
              quantity: 999,
              price: 10000
            }
          },
          {
            product: {
              id: 1,
              name: '商品名あいうえおかきくけこさしすせそ3'
            },
            product_variants: {
              vintage: '2017',
              closure: 'コルク',
              label_version: '2017-01',
              box_version: '2017-02',
              article_number: '1234567890123'
            },
            stock_taking_task_items: {
              stock_quantity: 10000,
              quantity: 999,
              price: 10000
            }
          },
          {
            product: {
              id: 1,
              name: '商品名あいうえおかきくけこさしすせそ4'
            },
            product_variants: {
              vintage: '2017',
              closure: 'コルク',
              label_version: '2017-01',
              box_version: '2017-02',
              article_number: '1234567890123'
            },
            stock_taking_task_items: {
              stock_quantity: 10000,
              quantity: 999,
              price: 10000
            }
          }
        ]
      }
    },
    {
      title: 'Empty',
      property: {
        itemCount: 0,
        stockTakingTaskItems: []
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
