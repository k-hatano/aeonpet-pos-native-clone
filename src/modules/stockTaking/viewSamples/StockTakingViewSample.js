import StockTakingView from '../components/StockTakingView'

export default {
  name: 'StockTakingView',
  component: StockTakingView,
  properties: [
    {
      title: 'Default',
      property: {
        stockTaking: {
          account_month: '2017年07月',
          slip_number: '12345678',
          started_at: 1504772863243,
          staff_name: '大工',
          pre_fixed_at: 1504772863243,
          status: 0,
          note: 'メモテスト'
        },
        stockTakingTasks: [
          {
            name: '商品棚Aの棚卸',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          },
          {
            name: '商品棚Bの棚卸',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          },
          {
            name: '商品棚Cの棚卸',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          }
        ],
        stockTakingUnpushedTasks: [
          {
            name: '商品棚Aの棚卸(未送信テスト)',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          },
          {
            name: '商品棚Bの棚卸(未送信テスト)',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          },
          {
            name: '商品棚Cの棚卸(未送信テスト)',
            staff_name: '大工',
            created_at: 1504772863243,
            updated_at: 1504772863243
          }
        ]
      }
    },
    {
      title: 'Empty',
      property: {
        stockTaking: null,
        stockTakingTasks: null,
        stockTakingUnpushedTasks: null
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
