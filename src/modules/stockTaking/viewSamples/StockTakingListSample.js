import StockTakingList from '../components/StockTakingList'

export default {
  name: 'StockTakingList',
  component: StockTakingList,
  properties: [
    {
      title: 'Default',
      property: {
        stockTakings: [
          {
            account_month: '2017年07月',
            status: 0,
            slip_number: '12345678',
            started_at: 1504772863243,
            staff_name: '大工',
            pre_fixed_at: 1504772863243,
            note: 'メモテスト'
          },
          {
            account_month: '2017年06月',
            status: 1,
            slip_number: '1235452546',
            started_at: 1504772863243,
            staff_name: '大工',
            pre_fixed_at: 1504772863243,
            note: 'メモテスト123'
          },
          {
            account_month: '2017年05月',
            status: 2
          },
          {
            account_month: '2017年04月',
            status: 3
          },
          {
            account_month: '2017年03月',
            status: 3
          },
          {
            account_month: '2017年02月',
            status: 3
          },
          {
            account_month: '2017年01月',
            status: 3
          },
          {
            account_month: '2016年12月',
            status: 3
          }
        ]
      }
    },
    {
      title: 'Empty',
      property: {
        stockTakings: []
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
