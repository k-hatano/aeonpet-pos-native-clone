import StockTakingTaskStartConfirmView from '../components/StockTakingTaskStartConfirmView'

export default {
  name: 'StockTakingTaskStartConfirmView',
  component: StockTakingTaskStartConfirmView,
  properties: [
    {
      title: 'Default',
      property: {
        taskName: 'タスク名1234567890',
        staff: {
          name: 'Test',
          staff_code: 'a'
        }
      }
    },
    {
      title: 'Empty',
      property: {
        taskName: '',
        staff: {}
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: { width: 683 }
    }
  ]
}
