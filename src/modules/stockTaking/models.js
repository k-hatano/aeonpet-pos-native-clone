import * as actions from './actions'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'
import { Actions } from 'react-native-router-flux'
import Modal from 'common/components/widgets/Modal'
import StockTakingRepository from './repositories/StockTakingRepository'
import StockItemsRepository from '../stock/repositories/StockItemsRepository'
import SettingKeys from '../setting/models/SettingKeys'
import { getSettingFromState } from '../setting/models'
import NetworkError from 'common/errors/NetworkError'
import { loading } from 'common/sideEffects'

export const MODULE_NAME = 'stockTaking'

export const STOCK_TAKING_STATUS = {
  CREATED: 0,
  STOCKING: 1,
  PROVISIONALLY_CONFIRMED: 2,
  CONFIRMED: 3
}

export const STOCK_TAKING_TASK_STATUS = {
  STOCKING: 0,
  COMPLETED: 1
}

export const CARBONATED_ALCOHOLS_CODE = 1004
export const BEER_CODE = 2041

// 制限指定なしのため、最大値をINTの上限に設定
export const MAX_ITEM_QUANTITY = 2147483647

export const MAX_STOCK_TAKING_TASK_ITEM = 200

export const getStockTakingStatusText = status => {
  switch (status) {
    case STOCK_TAKING_STATUS.CREATED:
      return I18n.t('stock_taking.status_start_stock')
    case STOCK_TAKING_STATUS.STOCKING:
      return I18n.t('stock_taking.status_stocking')
    case STOCK_TAKING_STATUS.PROVISIONALLY_CONFIRMED:
      return I18n.t('stock_taking.status_provisionally_confirmed')
    case STOCK_TAKING_STATUS.CONFIRMED:
      return I18n.t('stock_taking.status_confirmed')
    default:
      throw new Error('Invalid stock taking status.')
  }
}

export const isLiquor = productVariant => {
  return (
    productVariant.liquor_tax_code1 === CARBONATED_ALCOHOLS_CODE ||
    productVariant.liquor_tax_code2 === BEER_CODE
  )
}

export const initStockTakings = async dispatch => {
  try {
    await loading(dispatch, async () => {
      const stockTakings = await StockTakingRepository.fetchAll()

      if (stockTakings.length <= 0) {
        AlertView.show(I18n.t('message.O-01-E001'), function () {
          Modal.close()
          Actions.home()
        })
      } else {
        dispatch(actions.setStockTakings(stockTakings))
      }
    })
  } catch (error) {
    const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-01-E002')
    AlertView.show(message, function () {
      Modal.close()
      Actions.home()
    })
  }
}

export const clearStockTakings = async dispatch => {
  dispatch(actions.setStockTakings([]))
  dispatch(actions.setCurrentStockTaking({}))
  dispatch(actions.setCurrentStockTakingTask({}))
}

// StockTakingCountView onUpdateStock
export const updateStock = async function (stockTakingTaskItems) {
  const stockItems = await StockItemsRepository.fetchAll()

  // save into local DB
  await StockItemsRepository.bulkSave(stockItems)

  if (!stockTakingTaskItems) {
    stockTakingTaskItems = []
  }

  for (var i = 0; i < stockItems.length; i++) {
    var index = stockTakingTaskItems
      .map(function (item) {
        return item.product.id
      })
      .indexOf(stockItems[i]['product_id'])

    if (index >= 0) {
      stockTakingTaskItems[index]['stock_quantity'] = stockItems[i]['quantity']
      stockTakingTaskItems[index]['quantity_diffs'] =
        stockTakingTaskItems[index]['quantity'] - stockTakingTaskItems[index]['stock_quantity']
    }
  }

  return stockTakingTaskItems
}

export const makeStockTakingTask = (state, stockTakingTask) => {
  return {
    ...stockTakingTask,
    staff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
  }
}

export const makeStockTakingTaskItems = async stockTakingTaskItems => {
  let newStockTakingTaskItems = []
  let taskItemProductIds = []

  for (const stockTakingTaskItem of stockTakingTaskItems) {
    if (stockTakingTaskItem.product.is_parent_product) {
      for (const product of stockTakingTaskItem.childProducts) {
        if (taskItemProductIds.includes(product.id)) {
          const idx = newStockTakingTaskItems.findIndex(
            item => item.product.id === product.id
          )
          newStockTakingTaskItems[idx].quantity +=
            stockTakingTaskItem.quantity * product.quantity
        } else {
          if (product.variant) {
            product.product_variant = product.variant
            delete product.variant
          }
          newStockTakingTaskItems.push({
            product: product,
            quantity: stockTakingTaskItem.quantity * product.quantity,
            stock_quantity: product.stock_item ? product.stock_item.quantity : 0
          })
        }

        taskItemProductIds.push(product.id)
      }
    } else {
      if (taskItemProductIds.includes(stockTakingTaskItem.product.id)) {
        const idx = newStockTakingTaskItems.findIndex(
          item => item.product.id === stockTakingTaskItem.product.id
        )
        newStockTakingTaskItems[idx].quantity += stockTakingTaskItem.quantity
      } else {
        newStockTakingTaskItems.push(stockTakingTaskItem)
        taskItemProductIds.push(stockTakingTaskItem.product.id)
      }
    }
  }

  return newStockTakingTaskItems
}
