import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultStatus = {
  stockTakings: [],
  stockTakingTasks: [],
  stockTakingUnpushedTasks: [],
  currentStockTaking: {},
  currentStockTakingTask: {},
  staff: {},
  taskName: null,
  itemCount: 0,
  stockTakingTaskItems: [],
  isAutoDisplayNumericKeypad: true,
  tabIndex: 0
}

const handlers = {
  [actions.resetItemCount]: (state, action) => {
    return {
      ...state,
      taskName: null,
      itemCount: 0,
      stockTakingTaskItems: [],
      isAutoDisplayNumericKeypad: true
    }
  },
  [actions.setStockTakings]: (state, action) => {
    return {
      ...state,
      stockTakings: action.payload
    }
  },
  [actions.setCurrentStockTaking]: (state, action) => {
    return {
      ...state,
      currentStockTaking: action.payload
    }
  },
  [actions.setCurrentStockTakingTask]: (state, action) => {
    return {
      ...state,
      currentStockTakingTask: action.payload
    }
  },
  [actions.setIsAutoDisplayNumericKeypad]: (state, action) => {
    return {
      ...state,
      isAutoDisplayNumericKeypad: action.payload
    }
  },
  [actions.setStockTakingTasks]: (state, action) => {
    return {
      ...state,
      ...{stockTakingTasks: action.payload}
    }
  },
  [actions.setStockTakingUnpushedTasks]: (state, action) => {
    return {
      ...state,
      ...{stockTakingUnpushedTasks: action.payload}
    }
  },
  [actions.setStockTakingTaskItems]: (state, action) => {
    return {
      ...state,
      ...{stockTakingTaskItems: action.payload}
    }
  },
  [actions.updateStockTakingTaskItems]: (state, action) => {
    let newStockTakingTaskItems = state.stockTakingTaskItems
    const index = newStockTakingTaskItems.findIndex(item => item.product.id == action.payload.product.id)
    newStockTakingTaskItems[index] = action.payload

    return {
      ...state,
      stockTakingTaskItems: Object.assign([], newStockTakingTaskItems),
      itemCount: newStockTakingTaskItems.length
    }
  },
  [actions.setTaskName]: (state, action) => {
    return {
      ...state,
      taskName: action.payload
    }
  },
  [actions.setItemCount]: (state, action) => {
    return {
      ...state,
      itemCount: action.payload
    }
  },
  [actions.setProducts]: (state, action) => {
    return {
      ...state,
      products: action.payload
    }
  },
  [actions.setTabIndex]: (state, action) => {
    return {
      ...state,
      tabIndex: action.payload
    }
  },
  [actions.deleteStockTakingTaskItem]: (state, action) => {
    let newStockTakingTaskItems = state.stockTakingTaskItems
    const index = newStockTakingTaskItems.findIndex(item => item.product.id == action.payload.product.id)
    newStockTakingTaskItems.splice(index, 1)

    return {
      ...state,
      stockTakingTaskItems: Object.assign([], newStockTakingTaskItems),
      itemCount: newStockTakingTaskItems.length
    }
  }
}

export default handleActions(handlers, defaultStatus)
