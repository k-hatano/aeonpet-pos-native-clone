import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import StockTakingTaskItemList from '../components/StockTakingTaskItemList'

const mapDispatchToProps = dispatch => ({
  onBack: async () => {
    dispatch(actions.setStockTakingTaskItems([]))
  }
})

const mapStateToProps = state => ({
  stockTakingTaskItems: state[MODULE_NAME].stockTakingTaskItems
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockTakingTaskItemList
)
