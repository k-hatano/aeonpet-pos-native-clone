import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import * as _ from 'underscore'
import StockTakingView from '../components/StockTakingView'
import StockTakingTaskStartConfirmView from '../containers/StockTakingTaskStartConfirmView'
import Modal from 'common/components/widgets/Modal'
import SettingKeys from '../../setting/models/SettingKeys'
import I18n from 'i18n-js'
import StockTakingTaskRepository from '../repositories/StockTakingTaskRepository'
import StockTakingTaskItemRepository from '../repositories/StockTakingTaskItemRepository'
import ProductRepository from '../../product/repositories/ProductRepository'
import AlertView from 'common/components/widgets/AlertView'
import NetworkError from 'common/errors/NetworkError'
import { loading } from 'common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onPushUnpushedTasks: async (stockTaking, stockTakingUnpushedTasks) => {
    try {
      await loading(dispatch, async () => {
        const stockTakingTasks = await StockTakingTaskRepository.fetchAllByStockTakingId(stockTaking.id)
        let isExists
        for (const stockTakingUnpushedTask of stockTakingUnpushedTasks) {
          isExists = stockTakingTasks.some(task => task.id === stockTakingUnpushedTask.id)
          if (!isExists) {
            AlertView.show(I18n.t('message.O-04-E003'))
          } else {
            await StockTakingTaskRepository.push(stockTakingUnpushedTask)
          }
        }
        await StockTakingTaskRepository.deleteByStockTakingId(stockTaking.id)
        await dispatch(actions.setStockTakingUnpushedTasks([]))

        const latestTasks = await StockTakingTaskRepository.fetchByStockTakingId(stockTaking.id)
        // タスク一覧を更新
        await dispatch(actions.setStockTakingTasks(latestTasks))
        // タブをタスク一覧に変更
        dispatch(actions.setTabIndex(1))
      })
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-02-E001')
      AlertView.show(message)
    }
  },
  onReadyStockTakingTask: async (staff, taskName) => {
    Modal.open(StockTakingTaskStartConfirmView, {
      props: {
        onClose: () => Modal.close()
      }
    })
    dispatch(actions.setTaskName(taskName))
  },
  onSelectStockTakingTask: async (stockTakingTask, withStockitems) => {
    try {
      await loading(dispatch, async () => {
        let stockTakingTaskItems = []
        if (!withStockitems) {
          stockTakingTaskItems = await StockTakingTaskItemRepository.fetchByStockTakingTaskId(
            stockTakingTask.id
          )
        } else {
          stockTakingTaskItems = stockTakingTask.stock_taking_task_items
        }

        let _stockTakingTaskItems = []
        for (const stockTakingTaskItem of stockTakingTaskItems) {
          let product = await ProductRepository.findWithDetail(
            stockTakingTaskItem.product_id
          )

          if (!product) {
            // 商品マスタとの不整合で商品が見つからなかった場合のダミー商品 暫定対応
            product = {
              name: '',
              product_code: '',
              product_variant: {}
            }
          } else if (product.product_variants) {
            product.product_variant = product.product_variants[0]
          }

          _stockTakingTaskItems.push({
            ...stockTakingTaskItem,
            product: product
          })
        }

        _stockTakingTaskItems = _.chain(_stockTakingTaskItems)
          .sortBy(item => item.product.product_code)
          .sortBy(item => item.product.name)
          .value()

        dispatch(actions.setStockTakingTaskItems(_stockTakingTaskItems))
      })
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-02-E001')
      AlertView.show(message)
    }
  },
  selectedTab: async tabIndex => {
    dispatch(actions.setTabIndex(tabIndex))
  }
})

const mapStateToProps = state => ({
  stockTaking: state[MODULE_NAME].currentStockTaking,
  stockTakingTasks: state[MODULE_NAME].stockTakingTasks,
  stockTakingUnpushedTasks: state[MODULE_NAME].stockTakingUnpushedTasks,
  staff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  tabIndex: state[MODULE_NAME].tabIndex
})

export default connect(mapStateToProps, mapDispatchToProps)(StockTakingView)
