import { connect } from 'react-redux'
import { MODULE_NAME, STOCK_TAKING_STATUS } from '../models'
import StockTakingTaskStartConfirmViewComponent from '../components/StockTakingTaskStartConfirmView'
import * as actions from '../actions'
import * as productActions from 'modules/product/actions'
import I18n from 'i18n-js'
import ConfirmView from 'common/components/widgets/ConfirmView'
import AlertView from 'common/components/widgets/AlertView'
import Modal from 'common/components/widgets/Modal'
import { Actions } from 'react-native-router-flux'
import StockTakingRepository from '../repositories/StockTakingRepository'
import StockTakingTaskRepository from '../repositories/StockTakingTaskRepository'
import SettingKeys from 'modules/setting/models/SettingKeys'
import NetworkError from 'common/errors/NetworkError'
import { loading } from 'common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onChangeTaskName: async taskName => {
    await dispatch(actions.setTaskName(taskName))
  },
  onStartStockTakingTask: (taskName, staff, stockTaking) => {
    if (taskName == null || taskName == '') {
      AlertView.show(I18n.t('message.O-03-E001'))
    } else {
      Modal.close()
      ConfirmView.show(I18n.t('message.O-03-I001'), async () => {
        try {
          await loading(dispatch, async () => {
            const _stockTaking = await StockTakingRepository.fetchById(
              stockTaking.id
            )
            if (_stockTaking.status === STOCK_TAKING_STATUS.STOCKING) {
              const stockTakingTask = {
                stock_taking_id: _stockTaking.id,
                name: taskName,
                status: 0,
                start_staff_id: staff.id,
                start_staff_name: staff.name,
                is_pushed: 0
              }
              const _stockTakingTask = await StockTakingTaskRepository.push(
                stockTakingTask
              )
              dispatch(actions.setCurrentStockTakingTask(_stockTakingTask))

              Modal.close()
              dispatch(actions.resetItemCount())

              Actions.stockTakingCountPage()
            } else {
              await loading(dispatch, async () => {
                const stockTakings = await StockTakingRepository.fetchAll()
                if (_stockTaking.status !== STOCK_TAKING_STATUS.CREATED) {
                  await dispatch(actions.setCurrentStockTaking(_stockTaking))

                  const stockTakingTasks = await StockTakingTaskRepository.fetchByStockTakingId(
                    _stockTaking.id
                  )

                  const unpushedTasks = await StockTakingTaskRepository.findUnpushedByStockTakingId(
                    _stockTaking.id
                  )
                  const margeStockTakingTasks = [
                    ...stockTakingTasks,
                    ...unpushedTasks
                  ]
                  await dispatch(actions.setStockTakingTaskItems([]))
                  await dispatch(actions.setStockTakingTasks(margeStockTakingTasks))
                  await dispatch(actions.setStockTakingUnpushedTasks(unpushedTasks))
                } else {
                  await dispatch(actions.setCurrentStockTaking({}))
                  await dispatch(actions.setStockTakingTaskItems([]))
                  await dispatch(actions.setStockTakingTasks([]))
                  await dispatch(actions.setStockTakingUnpushedTasks([]))
                }
                dispatch(actions.setStockTakings(stockTakings))
              })
              AlertView.show(I18n.t('message.O-03-E003'))
            }
          })
        } catch (error) {
          const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-03-E002')
          AlertView.show(message, () => Modal.close())
        }
      })
    }
  }
})

const mapStateToProps = state => ({
  taskName: state[MODULE_NAME].taskName,
  staff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  stockTaking: state[MODULE_NAME].currentStockTaking
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockTakingTaskStartConfirmViewComponent
)
