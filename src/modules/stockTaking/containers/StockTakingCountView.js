import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import StockTakingCountView from '../components/StockTakingCountView'
import StockItemRepository from '../../stock/repositories/StockItemsRepository'
import { loading } from 'common/sideEffects'
import NetworkError from 'common/errors/NetworkError'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  onUpdateStock: async stockTakingTaskItems => {
    try {
      await loading(dispatch, async () => {
        let stockItems = []
        for (const item of stockTakingTaskItems) {
          const stockItem = await StockItemRepository.fetchByProductId(item.product.id)
          // 在庫なしの場合は NULL なので、華麗にスルー
          if (stockItem === null) continue
          stockItems.push(stockItem)
        }
        await StockItemRepository.bulkSave(stockItems)

        let newStockTakingTaskItems = []
        for (const stockTakingTaskItem of stockTakingTaskItems) {
          const stockItem = await StockItemRepository.findByProductVariantId(
            stockTakingTaskItem.product.product_variant.id
          )
          if (stockItem && stockItem.length > 0) {
            newStockTakingTaskItems.push({
              ...stockTakingTaskItem,
              stock_quantity: stockItem[0].quantity
            })
          } else {
            newStockTakingTaskItems.push(stockTakingTaskItem)
          }
        }
        dispatch(actions.setStockTakingTaskItems(newStockTakingTaskItems))
        dispatch(actions.setItemCount(newStockTakingTaskItems.length))
      })
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-03-E002')
      AlertView.show(message)
    }
  },
  onClear: async () => {
    dispatch(actions.setStockTakingTaskItems([]))
    dispatch(actions.setItemCount(0))
  },
  onUpdateListItemQuantity: async (stockTakingTaskItem, quantity) => {
    stockTakingTaskItem.quantity = quantity

    dispatch(actions.updateStockTakingTaskItems(stockTakingTaskItem))
  },
  onDeleteListItem: async (stockTakingTaskItem) => {
    dispatch(actions.deleteStockTakingTaskItem(stockTakingTaskItem))
  }
})

const mapStateToProps = state => ({
  itemCount: state[MODULE_NAME].itemCount,
  stockTakingTaskItems: state[MODULE_NAME].stockTakingTaskItems
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockTakingCountView
)
