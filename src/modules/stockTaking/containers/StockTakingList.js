import { connect } from 'react-redux'
import { MODULE_NAME, startStockTaking, STOCK_TAKING_STATUS } from '../models'
import * as actions from '../actions'
import StockTakingListComponent from '../components/StockTakingList'
import AlertView from 'common/components/widgets/AlertView'
import StockTakingTaskRepository from '../repositories/StockTakingTaskRepository'
import StockTakingRepository from '../repositories/StockTakingRepository'
import I18n from 'i18n-js'
import Modal from 'common/components/widgets/Modal'
import SettingKeys from '../../setting/models/SettingKeys'
import { getSettingFromState } from '../../setting/models'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import NetworkError from 'common/errors/NetworkError'
import { loading } from 'common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onStartStockTaking: async (
    stockTakings,
    stockTaking,
    onSelectStockTaking,
    staff
  ) => {
    try {
      await loading(dispatch, async () => {
        let _stockTaking = await StockTakingRepository.fetchById(stockTaking.id)
        const cashier = await CashierRepository.find()
        const index = stockTakings
          .map(function(item) {
            return item.id
          })
          .indexOf(stockTaking.id)
        if (_stockTaking.status === STOCK_TAKING_STATUS.CREATED) {
          _stockTaking.status = STOCK_TAKING_STATUS.STOCKING
          _stockTaking.staff_id = staff.id
          _stockTaking.staff_name = staff.name
          _stockTaking.staff_code = staff.staff_code
          _stockTaking.cashier_code = cashier.cashier_code
          _stockTaking.started_at = Math.floor(new Date().getTime() / 1000)
          _stockTaking.updated_at = Math.floor(new Date().getTime() / 1000)
          await StockTakingRepository.push(_stockTaking)
          if (index >= 0) {
            stockTakings[index] = _stockTaking
            dispatch(actions.setStockTakingTaskItems([]))
            dispatch(actions.setStockTakings(Object.assign([], stockTakings)))
          }
        } else {
          AlertView.show(I18n.t('message.O-01-E003'), () => {
            stockTakings[index] = _stockTaking
            dispatch(actions.setStockTakingTaskItems([]))
            dispatch(actions.setStockTakings(Object.assign([], stockTakings)))
            Modal.close()
          })
        }
        await onSelectStockTaking(_stockTaking)
        dispatch(actions.setTabIndex(0))
      })
    } catch (error) {
      await dispatch(actions.setCurrentStockTaking({}))
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-01-E002')
      AlertView.show(message)
    }
  },
  onSelectStockTaking: async stockTaking => {
    try {
      let _stockTaking = {}
      try {
        await loading(dispatch, async () => {
          _stockTaking = await StockTakingRepository.fetchById(stockTaking.id)

          if (Object.keys(_stockTaking).length <= 0) {
            AlertView.show(I18n.t('message.O-01-E001'))
          }
        })
      } catch (error) {
        await dispatch(actions.setCurrentStockTaking({}))
        const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-01-E002')
        AlertView.show(message)
        throw new Error(error)
      }
      try {
        await loading(dispatch, async () => {
          if (_stockTaking || Object.keys(_stockTaking).length > 0) {
            if (_stockTaking.status !== STOCK_TAKING_STATUS.CREATED) {
              await dispatch(actions.setCurrentStockTaking(_stockTaking))

              const stockTakingTasks = await StockTakingTaskRepository.fetchByStockTakingId(
                _stockTaking.id
              )

              const unpushedTasks = await StockTakingTaskRepository.findUnpushedByStockTakingId(
                _stockTaking.id
              )
              const margeStockTakingTasks = [
                ...stockTakingTasks,
                ...unpushedTasks
              ]
              await dispatch(actions.setStockTakingTaskItems([]))
              await dispatch(actions.setStockTakingTasks(margeStockTakingTasks))
              await dispatch(actions.setStockTakingUnpushedTasks(unpushedTasks))
            } else {
              if (stockTaking.status !== STOCK_TAKING_STATUS.CREATED) {
                await dispatch(actions.setCurrentStockTaking(stockTaking))
              } else if (_stockTaking) {
                await dispatch(actions.setCurrentStockTaking(_stockTaking))
              } else {
                await dispatch(actions.setCurrentStockTaking({}))
              }
              await dispatch(actions.setStockTakingTaskItems([]))
              await dispatch(actions.setStockTakingTasks([]))
              await dispatch(actions.setStockTakingUnpushedTasks([]))
            }
          }
        })
      } catch (error) {
        await dispatch(actions.setCurrentStockTaking({}))
        const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.O-02-E001')
        AlertView.show(message)
        throw new Error(error)
      }
    } catch (error) {
      console.info(error)
    }
  }
})

const mapStateToProps = state => ({
  stockTakings: state[MODULE_NAME].stockTakings,
  staff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockTakingListComponent
)
