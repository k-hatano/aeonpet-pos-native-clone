import { connect } from 'react-redux'
import {
  MODULE_NAME,
  STOCK_TAKING_TASK_STATUS,
  MAX_STOCK_TAKING_TASK_ITEM,
  makeStockTakingTask,
  makeStockTakingTaskItems
} from '../models'
import { MODULE_NAME as productModuleName, PREV_SEARCH_COND } from '../../../modules/product/models'
import * as actions from '../actions'
import * as productActions from '../../../modules/product/actions'
import StockTakingSearchProductForm from '../components/StockTakingSearchProductForm'
import StockTakingTaskRepository from '../repositories/StockTakingTaskRepository'
import ProductRepository from '../../product/repositories/ProductRepository'
import ConfirmView from '../../../common/components/widgets/ConfirmView'
import Modal from '../../../common/components/widgets/Modal'
import I18n from 'i18n-js'
import AlertView from '../../../common/components/widgets/AlertView'
import { Actions, ActionConst } from 'react-native-router-flux'
import { loading } from '../../../common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onSearchForBarcodeReader: async (barcode) => {
    const products = await loading(dispatch, async () => {
      let products = await ProductRepository.findProductsBySearchWordForStocks(barcode, true)
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      switch (products.length) {
        case 0:
          let productVariantsByWord = await ProductRepository.findVariantsBySearchWordForStocks(barcode)
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              const productDetail = await ProductRepository.findWithDetail(productVariantsByWord[0].id)
              products.push(productDetail)
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              dispatch(productActions.listProducts(productVariantsByWord))
              dispatch(productActions.listOriginProducts(productVariantsByWord))
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
              dispatch(productActions.setSearchLayoutFlg(true))
              break
          }
          break
        case 1:
          const productVariantsById = await ProductRepository.findVariantsByProductId(products[0].id)
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              break
          }
          break
        default:
          dispatch(productActions.listProducts(products))
          dispatch(productActions.listOriginProducts(products))
          dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
          dispatch(productActions.setSearchLayoutFlg(true))
          break
      }
      return products
    })
    return products
  },
  onProductSelected: async (product, stockTakingTaskItems, quantity = 1) => {
    let index = -1
    let childProducts = []
    // find if existed
    if (stockTakingTaskItems && stockTakingTaskItems.length > 0) {
      index = stockTakingTaskItems
        .map(item => {
          return item.product.id
        })
        .indexOf(product.id)
    }
    if (index >= 0) {
      stockTakingTaskItems[index]['quantity'] =
        stockTakingTaskItems[index]['quantity'] + quantity
    } else {
      if (MAX_STOCK_TAKING_TASK_ITEM > stockTakingTaskItems.length) {
        if (stockTakingTaskItems === null) {
          stockTakingTaskItems = []
        }
        if (product.product_variants) {
          product.product_variant = product.product_variants[0]
        }
        if (product.is_parent_product) {
          childProducts = await ProductRepository.findSetById(product.id)
        }
        const newItem = {
          product: product,
          childProducts: childProducts,
          stock_quantity:
            (product.product_variant.stock_item &&
              product.product_variant.stock_item.quantity) ||
            0,
          quantity: quantity
        }
        stockTakingTaskItems.push(newItem)
      } else {
        AlertView.show(I18n.t('message.O-04-E002'))
      }
    }
    dispatch(
      actions.setStockTakingTaskItems(Object.assign([], stockTakingTaskItems))
    )
    dispatch(actions.setItemCount(stockTakingTaskItems.length))
  },
  onToggleNumerichKeyPadAutoDisplay: async isAutoDisplayNumericKeypad => {
    await dispatch(
      actions.setIsAutoDisplayNumericKeypad(!isAutoDisplayNumericKeypad)
    )
  },
  onComplete: async (stockTakingTask, stockTakingTaskItems) => {
    ConfirmView.show(I18n.t('message.O-04-I001'), async () => {
      Modal.close()
      try {
        await loading(dispatch, async () => {
          const _stockTakingTaskItems = await makeStockTakingTaskItems(
            stockTakingTaskItems
          )

          stockTakingTask.status = STOCK_TAKING_TASK_STATUS.COMPLETED
          await StockTakingTaskRepository.push({
            ...stockTakingTask,
            ...{ stock_taking_task_items: _stockTakingTaskItems }
          })
          const stockTakingTasks = await StockTakingTaskRepository.fetchByStockTakingId(
            stockTakingTask.stock_taking_id
          )

          dispatch(actions.setStockTakingTasks(stockTakingTasks))
          dispatch(actions.setItemCount(0))
          dispatch(actions.setCurrentStockTakingTask({}))
          dispatch(actions.setStockTakingTaskItems([]))

          Actions.pop({ type: ActionConst.REFRESH })
        })
      } catch (error) {
        if (error.message == 404) {
          AlertView.show(I18n.t('message.O-04-E003'), async () => {
            dispatch(actions.setItemCount(0))
            dispatch(actions.setCurrentStockTakingTask({}))
            dispatch(actions.setStockTakingTaskItems([]))

            Modal.close()
            Actions.pop({ type: ActionConst.REFRESH })
          })
        } else {
          AlertView.show(I18n.t('message.O-04-E001'), async () => {
            await loading(dispatch, async () => {
              const _stockTakingTaskItems = await makeStockTakingTaskItems(
                stockTakingTaskItems
              )

              stockTakingTask.status = STOCK_TAKING_TASK_STATUS.COMPLETED
              await StockTakingTaskRepository.save({
                ...stockTakingTask,
                ...{ stock_taking_task_items: _stockTakingTaskItems }
              })
              setTimeout(async () => {
                const unpushedTasks = await StockTakingTaskRepository.findUnpushedByStockTakingId(
                  stockTakingTask.stock_taking_id
                )
                dispatch(actions.setStockTakingUnpushedTasks(unpushedTasks))
                dispatch(actions.setItemCount(0))
                dispatch(actions.setCurrentStockTakingTask({}))
                dispatch(actions.setStockTakingTaskItems([]))

                dispatch(actions.setTabIndex(2))

                Modal.close()
                Actions.pop({ type: ActionConst.REFRESH })
              }, 500)
            })
          })
        }
      }
    })
  }
})

const mapStateToProps = state => ({
  isAutoDisplayNumericKeypad: state[MODULE_NAME].isAutoDisplayNumericKeypad,
  stockTakingTask: makeStockTakingTask(
    state,
    state[MODULE_NAME].currentStockTakingTask
  ),
  stockTakingTaskItems: state[MODULE_NAME].stockTakingTaskItems,
  stockConditionFlg: state[productModuleName].stockConditionFlg
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onSearchForBarcodeReader: (barcode) => {
      return dispatchProps.onSearchForBarcodeReader(barcode, stateProps.stockConditionFlg)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  StockTakingSearchProductForm
)
