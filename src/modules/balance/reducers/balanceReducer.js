import { handleActions } from 'redux-actions'
import * as actions from '../action'
import { BALANCE_TYPE } from '../models'

const defaultState = {
  balanceReasons: [],
  selectedBalance: null,
  selectedBalanceReason: null,
  selectedDepositReason: null,
  selectedWithdrawalReason: null,
  selectedBalanceType: 1,
  balanceAmount: 0,
  depositBalances: [],
  withdrawalBalances: []
}

const handlers = {
  [actions.listBalanceReasons]: (state, action) => ({
    ...state,
    ...{ balanceReasons: action.payload }
  }),
  [actions.selectDepositReason]: (state, action) => ({
    ...state,
    ...{
      selectedDepositReason: action.payload,
      selectedBalanceReason: action.payload
    }
  }),
  [actions.selectWithdrawalReason]: (state, action) => ({
    ...state,
    ...{
      selectedWithdrawalReason: action.payload,
      selectedBalanceReason: action.payload
    }
  }),
  [actions.selectBalanceType]: (state, action) => {
    if (state.selectedBalanceType !== action.payload) {
      return {
        ...state,
        ...{
          selectedBalanceType: action.payload,
          selectedBalanceReason: null
        }
      }
    } else {
      return state
    }
  },
  [actions.updateBalanceAmount]: (state, action) => {
    if (state.selectedBalance) {
      if (state.selectedBalanceType === BALANCE_TYPE.DEPOSIT) {
        const amount = action.payload
        const balances = state.depositBalances.concat()
        const selectedBalance = { ...state.selectedBalance, amount }
        for (let i in balances) {
          if (balances[i].payment_method_id === state.selectedBalance.payment_method_id) {
            balances[i] = selectedBalance
          }
        }
        return {
          ...state,
          ...{ selectedBalance, depositBalances: balances }
        }
      } else {
        const amount = action.payload
        const balances = state.withdrawalBalances.concat()
        const selectedBalance = { ...state.selectedBalance, amount }
        for (let i in balances) {
          if (balances[i].payment_method_id === state.selectedBalance.payment_method_id) {
            balances[i] = selectedBalance
          }
        }
        return {
          ...state,
          ...{ selectedBalance, withdrawalBalances: balances }
        }
      }
    } else {
      return state
    }
  },
  [actions.listBalances]: (state, action) => {
    return {
      ...state,
      ...{
        depositBalances: action.payload,
        withdrawalBalances: action.payload
      }
    }
  },
  [actions.selectBalance]: (state, action) => ({
    ...state,
    ...{ selectedBalance: action.payload }
  })
}
export default handleActions(handlers, defaultState)
