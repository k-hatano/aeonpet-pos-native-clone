import configureMockStore from 'redux-mock-store'
import balanceReducer from '../balanceReducer'
import * as actions from '../../action'
import { MODULE_NAME, BALANCE_TYPE } from '../../models'
import { PAYMENT_METHOD_TYPE } from '../../../payment/models'

const middlewares = []
const mockStore = configureMockStore(middlewares)
const initialState = {}
const store = mockStore(initialState)

afterEach(() => {
  store.clearActions()
})

describe('[Balance] Balance Reducer Test', () => {
  it('Should dispatch listBalanceReasons action', () => {
    const sampleBalanceReasons = [
      {
        id: 'A0000000000000000000000000000001',
        name: '入金理由１',
        balance_type: BALANCE_TYPE.DEPOSIT
      }
    ]
    store.dispatch(actions.listBalanceReasons(sampleBalanceReasons))
    const expectedPayload = {
      payload: sampleBalanceReasons,
      type: `${MODULE_NAME}_listBalanceReasons`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle listBalanceReasons', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      balances: []
    }
    const sampleBalanceReasons = [
      {
        id: 'A0000000000000000000000000000001',
        name: '入金理由１',
        balance_type: BALANCE_TYPE.DEPOSIT
      }
    ]
    const expectedState = {...defaultState, ...{ balanceReasons: sampleBalanceReasons } }

    expect(
      balanceReducer(defaultState, actions.listBalanceReasons(sampleBalanceReasons))
    )
      .toEqual(expectedState)
  })

  it('Should dispatch selectDepositReason actions', () => {
    const depositReason = {
      id: 'A0000000000000000000000000000001',
      name: '入金理由１',
      balance_type: BALANCE_TYPE.DEPOSIT
    }
    store.dispatch(actions.selectDepositReason(depositReason))
    const expectedPayload = {
      payload: depositReason,
      type: `${MODULE_NAME}_selectDepositReason`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle selectDepositReason', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      balances: []
    }
    const depositReason = {
      id: 'A0000000000000000000000000000001',
      name: '入金理由１',
      balance_type: BALANCE_TYPE.DEPOSIT
    }
    const expectedState = {
      ...defaultState,
      ...{
        selectedDepositReason: depositReason,
        selectedBalanceReason: depositReason
      }
    }

    expect(
      balanceReducer(defaultState, actions.selectDepositReason(depositReason))
    )
      .toEqual(expectedState)
  })

  it('Should dispatch selectWithdrawalReason actions', () => {
    const withdrawalReason = {
      id: 'A0000000000000000000000000000001',
      name: '入金理由１',
      balance_type: BALANCE_TYPE.WITHDRAWAL
    }
    store.dispatch(actions.selectWithdrawalReason(withdrawalReason))
    const expectedPayload = {
      payload: withdrawalReason,
      type: `${MODULE_NAME}_selectWithdrawalReason`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle selectWithdrawalReason', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      balances: []
    }
    const withdrawalReason = {
      id: 'A0000000000000000000000000000001',
      name: '入金理由１',
      balance_type: BALANCE_TYPE.WITHDRAWAL
    }
    const expectedState = {
      ...defaultState,
      ...{
        selectedWithdrawalReason: withdrawalReason,
        selectedBalanceReason: withdrawalReason
      }
    }

    expect(
      balanceReducer(defaultState, actions.selectWithdrawalReason(withdrawalReason))
    )
      .toEqual(expectedState)
  })

  it('Should dispatch selectBalanceType actions', () => {
    store.dispatch(actions.selectBalanceType(BALANCE_TYPE.WITHDRAWAL))
    const expectedPayload = {
      payload: BALANCE_TYPE.WITHDRAWAL,
      type: `${MODULE_NAME}_selectBalanceType`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle selectBalanceType', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      balances: []
    }
    const expectedState = {
      ...defaultState,
      ...{
        selectedBalanceType: BALANCE_TYPE.WITHDRAWAL,
        selectedBalanceReason: null
      }
    }

    expect(
      balanceReducer(defaultState, actions.selectBalanceType(BALANCE_TYPE.WITHDRAWAL))
    )
      .toEqual(expectedState)

    expect(
      balanceReducer(expectedState, actions.selectBalanceType(BALANCE_TYPE.WITHDRAWAL))
    )
      .toEqual(expectedState)
  })

  it('Should dispatch updateBalanceAmount actions', () => {
    const balanceAmount = 1000
    store.dispatch(actions.updateBalanceAmount(balanceAmount))
    const expectedPayload = {
      payload: balanceAmount,
      type: `${MODULE_NAME}_updateBalanceAmount`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle updateBalanceAmount', () => {
    const defaultBalances = [
      {
        amount: 0,
        payment_method_id: 'A0000000000000000000000000000001',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        payment_method_name: 'Cash'
      },
      {
        amount: 100,
        payment_method_id: 'A0000000000000000000000000000002',
        payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
        payment_method_name: 'Credit'
      },
      {
        amount: 200,
        payment_method_id: 'A0000000000000000000000000000003',
        payment_method_type: PAYMENT_METHOD_TYPE.EMONEY,
        payment_method_name: 'EMoney'
      }
    ]
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      depositBalances: defaultBalances,
      withdrawalBalances: []
    }

    const balanceAmount = 1500
    const expectedStateBeforeSelectBalance = defaultState

    // Test before select balance
    expect(
      balanceReducer(defaultState, actions.updateBalanceAmount(balanceAmount))
    )
      .toEqual(expectedStateBeforeSelectBalance)

    // Test after select balance
    const selectedBalance = {
      amount: 0,
      payment_method_id: 'A0000000000000000000000000000001',
      payment_method_type: PAYMENT_METHOD_TYPE.CASH,
      payment_method_name: 'Cash'
    }
    const stateAfterSelectBalance = balanceReducer(defaultState, actions.selectBalance(selectedBalance))
    selectedBalance.amount += balanceAmount
    const nextBalancesState = defaultBalances.map(balance => {
      if (balance.payment_method_id === selectedBalance.payment_method_id) {
        balance = selectedBalance
      }
      return balance
    })
    const expectedStateAfterSelectBalance = {...defaultState, ...{ selectedBalance }, ...{ depositBalances: nextBalancesState } }
    expect(
      balanceReducer(stateAfterSelectBalance, actions.updateBalanceAmount(balanceAmount))
    )
      .toEqual(expectedStateAfterSelectBalance)
  })

  it('Should dispatch listBalances actions', () => {
    const balances = [
      {
        amount: 0,
        payment_method_id: 'A0000000000000000000000000000001',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        payment_method_name: 'Cash'
      },
      {
        amount: 100,
        payment_method_id: 'A0000000000000000000000000000002',
        payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
        payment_method_name: 'Credit'
      },
      {
        amount: 200,
        payment_method_id: 'A0000000000000000000000000000003',
        payment_method_type: PAYMENT_METHOD_TYPE.EMONEY,
        payment_method_name: 'EMoney'
      }
    ]
    store.dispatch(actions.listBalances(balances))
    const expectedPayload = {
      payload: balances,
      type: `${MODULE_NAME}_listBalances`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle listBalances', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      depositBalances: [],
      withdrawalBalances: []
    }
    const balances = [
      {
        amount: 0,
        payment_method_id: 'A0000000000000000000000000000001',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        payment_method_name: 'Cash'
      },
      {
        amount: 100,
        payment_method_id: 'A0000000000000000000000000000002',
        payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
        payment_method_name: 'Credit'
      },
      {
        amount: 200,
        payment_method_id: 'A0000000000000000000000000000003',
        payment_method_type: PAYMENT_METHOD_TYPE.EMONEY,
        payment_method_name: 'EMoney'
      }
    ]
    const expectedState = {...defaultState, ...{ depositBalances: balances, withdrawalBalances: balances } }

    expect(
      balanceReducer(defaultState, actions.listBalances(balances))
    )
      .toEqual(expectedState)
  })

  it('Should dispatch selectBalance actions', () => {
    const balance = {
      amount: 0,
      payment_method_id: 'A0000000000000000000000000000001',
      payment_method_type: PAYMENT_METHOD_TYPE.CASH,
      payment_method_name: 'Cash'
    }
    store.dispatch(actions.selectBalance(balance))
    const expectedPayload = {
      payload: balance,
      type: `${MODULE_NAME}_selectBalance`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle selectBalance', () => {
    const defaultState = {
      balanceReasons: [],
      selectedBalance: null,
      selectedBalanceReason: null,
      selectedDepositReason: null,
      selectedWithdrawalReason: null,
      selectedBalanceType: 1,
      balanceAmount: 0,
      balances: []
    }
    const selectedBalance = {
      amount: 0,
      payment_method_id: 'A0000000000000000000000000000001',
      payment_method_type: PAYMENT_METHOD_TYPE.CASH,
      payment_method_name: 'Cash'
    }
    const expectedState = {...defaultState, ...{ selectedBalance } }

    expect(
      balanceReducer(defaultState, actions.selectBalance(selectedBalance))
    )
      .toEqual(expectedState)
  })
})
