import I18n from 'i18n-js'
import { OPERATION_NAME, OPERATION_TYPE } from 'modules/home/models'

export const MODULE_NAME = 'balance'
export const MAX_AMOUNT = 99999999

export const BALANCE_TYPE = {
  DEPOSIT: 1,
  WITHDRAWAL: 2
}

export const BALANCE_REASON_CODE = {
  WAON: 'CBR100000001'
}

export const isEmoneyCharge = (balance) => {
  switch (balance.balance_reason_code) {
    case BALANCE_REASON_CODE.WAON:
      return true
    default:
      return false
  }
}

export const SUPPORTED_CHARGE_AMOUNTS = [
  1000,
  2000,
  3000,
  5000,
  10000
]

export const balanceTypeToLabel = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.deposit')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.withdrawal')
    default:
      return '-'
  }
}

export const balanceTypeToOperationType = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return OPERATION_TYPE.DEPOSIT
    case BALANCE_TYPE.WITHDRAWAL:
      return OPERATION_TYPE.WITHDRAWAL
    default:
      return null
  }
}

export const balanceTypeToReasonLabel = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.deposit_reason')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.withdrawal_reason')
    default:
      return '-'
  }
}

export const balanceTypeToModalTitle = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.deposit_reason_select')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.withdrawal_reason_select')
    default:
      return '-'
  }
}

export const balanceTypeToPaymentLabel = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.deposit_method')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.withdrawal_method')
    default:
      return '-'
  }
}

export const balanceAmountToLabel = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.deposit_amount')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.withdrawal_amount')
    default:
      return '-'
  }
}

export const balanceTypeToProceedButtonLabel = (balanceType) => {
  switch (balanceType) {
    case BALANCE_TYPE.DEPOSIT:
      return I18n.t('balance.do_deposit')
    case BALANCE_TYPE.WITHDRAWAL:
      return I18n.t('balance.do_withdrawal')
    default:
      return '-'
  }
}

export const getSupputedAmountString = () => {
  const supportAmountStringList = []
  SUPPORTED_CHARGE_AMOUNTS.forEach((number) => {
    supportAmountStringList.push(number.toLocaleString() + I18n.t('common.japanese_yen'))
  })
  return supportAmountStringList.join('、')
}
