import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  tabWrapperStyle: {
    backgroundColor: baseStyleValues.subBackgroundColor
  },
  tabStyle: {
    width: 136
  },
  reasonWrapper: {
    paddingRight: 15,
    paddingBottom: 15,
    paddingTop: 15,
    backgroundColor: baseStyleValues.subBackgroundColor
  },
  reasonTitle: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor
  },
  reasonContainer: {
    zIndex: 1,
    padding: 30
  },
  reasonSelector: {
    marginTop: 10,
    zIndex: 1000,
    width: '80%'
  },
  methodTitle: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    marginTop: 80
  },
  paymentMethodList: {
    marginTop: 10,
    backgroundColor: baseStyleValues.mainBackgroundColor,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor
  },
  paymentsSeparator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  },
  amountPanel: {
    flex: 1,
    backgroundColor: baseStyleValues.mainBackgroundColor,
    height: '95%',
    paddingBottom: 10,
    padding: 10
  },
  amountDisplayPanel: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 136,
    marginTop: 20
  },
  balanceAmountPanel: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderWidth: 2,
    borderColor: baseStyleValues.mainTextColor,
    backgroundColor: '#f7f7f7',
    width: 472
  },
  verticalTitleCenter: {
    flex: 3.5,
    height: '100%',
    justifyContent: 'center'
  },
  verticalTextCenter: {
    flex: 6.5,
    height: '100%',
    justifyContent: 'center'
  },
  balanceAmountTitle: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    fontWeight: '500',
    paddingLeft: 40
  },
  balanceAmountText: {
    fontSize: 36,
    color: '#000',
    textAlign: 'right',
    fontWeight: '500',
    paddingRight: 25,
    letterSpacing: -0.58,
    marginRight: 3
  },
  numberPadContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  numberPadWrapper: {
    width: 472,
    height: 544,
    justifyContent: 'center',
    alignItems: 'center'
  },
  numberPad: {
    borderColor: baseStyleValues.borderColor,
    borderWidth: 1,
    width: '100%',
    height: '100%'
  },
  proceedButtonWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  ProceedButton: {
    width: 536,
    height: 56,
    marginBottom: 24,
    fontSize: 32,
    fontWeight: 'bold'
  },
  BalanceReasonContainer: {
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    height: 48,
    justifyContent: 'center',
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  },
  cautionText: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    marginTop: 50
  },
  supportAmountText: {
    fontSize: 28,
    color: baseStyleValues.mainTextColor,
    marginTop: 20
  },
  notSettingPaymentText: {
    fontSize: 20,
    color: 'red',
    marginTop: 15
  }
})
