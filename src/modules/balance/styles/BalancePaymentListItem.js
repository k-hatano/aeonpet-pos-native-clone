import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: '100%',
    height: 65,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconArea: {
    width: 28,
    height: 28,
    backgroundColor: '#f0f0f0',
    margin: 15
  },
  nameText: {
    flex: 1
  },
  numberBox: {
    backgroundColor: '#EEE',
    height: '70%',
    width: 250,
    borderWidth: 1,
    borderColor: '#979797',
    justifyContent: 'center',
    margin: 5
  },
  unitText: {
    fontSize: 30,
    color: baseStyleValues.mainTextColor,
    width: 60,
    margin: 5
  },
  button: {
    width: 86,
    height: 32,
    margin: 8,
    fontSize: 16,
    fontWeight: 'bold'
  },
  mainText: {
    fontSize: 30,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right',
    height: '90%',
    marginRight: 5
  }
})
