import I18n from 'i18n-js'
import * as actions from './action'
import { makeBaseEJournal, RECEIPT_TYPE } from '../printer/models'
import Settings from '../setting/models/Setting'
import generateUuid from '../../common/utils/generateUuid'
import { getSettingFromState } from '../setting/models'
import SettingKeys from '../setting/models/SettingKeys'
import { BALANCE_TYPE, MODULE_NAME } from './models'
import BalanceReasonRepository from './repositories/BalanceReasonRepository'
import PaymentMethodRepository from '../payment/repositories/PaymentMethodRepository'
import { PAYMENT_METHOD_TYPE } from '../payment/models'

export const clearSelected = async (dispatch) => {
  dispatch(actions.selectDepositReason(null))
  dispatch(actions.selectWithdrawalReason(null))
}

export const makeBalancesFromState = (state) => {
  const balanceReason = state[MODULE_NAME].selectedBalanceReason
  const staff = getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
  const balanceType = state[MODULE_NAME].selectedBalanceType
  const balances = balanceType === BALANCE_TYPE.DEPOSIT ? state[MODULE_NAME].depositBalances : state[MODULE_NAME].withdrawalBalances
  return balances.map(balance => {
    return {
      ...balance,
      currency: 'jpy',
      balance_type: balanceType,
      cashier_id: getSettingFromState(state, SettingKeys.COMMON.CASHIER_ID),
      staff_id: staff.id,
      staff_name: staff.name,
      balance_reason_id: balanceReason && balanceReason.id,
      balance_reason_code: balanceReason && balanceReason.cashier_balance_reason_code,
      balance_reason_name: balanceReason && balanceReason.name,
      order_id: null,
      pos_order_number: null,
      client_created_at: Math.floor(new Date().getTime() / 1000)
    }
  })
}

export const canCompleteCreatingBalance = (state) => {
  const moduleState = state[MODULE_NAME]
  return moduleState.selectedBalanceReason && moduleState.selectedBalance && moduleState.selectedBalance.amount > 0
}

export const makeBalanceEJournal = (receipt, balances, createdAt) => {
  let title = ''
  let type = 0
  switch (balances[0].balance_type) {
    case BALANCE_TYPE.DEPOSIT:
      title = I18n.t('receipt.title.deposit')
      type = RECEIPT_TYPE.DEPOSIT
      break

    case BALANCE_TYPE.WITHDRAWAL:
      title = I18n.t('receipt.title.withdrawal')
      type = RECEIPT_TYPE.WITHDRAWAL
      break
  }

  const ejournal = makeBaseEJournal(title, receipt, type, createdAt)
  ejournal.currency = Settings.currency
  ejournal.amount = balances.reduce((sum, balance) => sum + balance.amount, 0)
  ejournal.staff_id = balances[0].staff_id
  ejournal.is_print_tax_stamp = false

  return ejournal
}

export const initForCreateBalances = async (dispatch) => {
  const balanceReasons = await BalanceReasonRepository.findByBalanceType(BALANCE_TYPE.DEPOSIT)
  const paymentMethods = await PaymentMethodRepository.findByPaymentMethodType(PAYMENT_METHOD_TYPE.CASH)
  const balances = paymentMethods.map(paymentMethod => {
    return {
      amount: 0,
      payment_method_id: paymentMethod.id,
      payment_method_type: paymentMethod.payment_method_type,
      payment_method_name: paymentMethod.name
    }
  })
  dispatch(actions.listBalanceReasons(balanceReasons))
  dispatch(actions.listBalances(balances))
  dispatch(actions.selectBalance(balances[0]))
  dispatch(actions.selectDepositReason(null))
  dispatch(actions.selectWithdrawalReason(null))
}
