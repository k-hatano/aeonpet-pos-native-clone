import I18n from 'i18n-js'
import ReceiptBuilder from '../../printer/models/ReceiptBuilder'

export default class BalanceReceiptBuilder extends ReceiptBuilder {
  buildBalanceReceipt (balances, createdAt) {
    const balance = balances[0]
    return {
      content: [
        this.titleElement(this._title(balance.balance_type)),
        this.feedElement(1),
        this.shopNameElement(),
        this.cashierNameElement(),
        {
          element: 'text',
          align: 'left',
          text: this.formatDateTime(createdAt)
        },
        this.feedElement(),
        {
          element: 'combine_left_right',
          left: balance.balance_reason_name,
          right: this.formatMoney(balance.amount, balance.currency)
        },
        this.feedElement(),
        this.textLineElement(),
        this.feedElement(),
        ...this.amountElements(balances),
        this.feedElement(),
        this.staffElement(undefined, 'left'),
        this.cutElement()
      ]
    }
  }

  amountElements (balances) {
    return balances.map(balance => {
      return {
        element: 'combine_left_right',
        left: balance.payment_method_name,
        right: this.formatMoney(balance.amount, balance.currency)
      }
    })
  }

  _title (balanceType) {
    switch (balanceType) {
      case 1:
        return I18n.t('receipt.title.deposit')

      case 2:
        return I18n.t('receipt.title.withdrawal')

      default:
        throw new RangeError()
    }
  }
}
