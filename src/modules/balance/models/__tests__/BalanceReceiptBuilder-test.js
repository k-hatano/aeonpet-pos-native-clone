import BalanceReceiptBuilder from '../BalanceReceiptBuilder'
import ReceiptTextConverter from '../../../printer/models/ReceiptTextConverter'
import { propertyA } from '../../../printer/samples'
import { BALANCE_TYPE } from '../../models'

const targetReceiptText = `
                      入金

テスト店舗A
テストレジA-1
2017-07-07 09:30:00

小口入金                                ￥12,000

             ----------------------

現金                                    ￥12,000

担当者:スタッフ１                               
`.slice(1)

describe('Balance Receipt Builder test', () => {
  it('balance receipt text build', async () => {
    const balanceReceiptBulder = new BalanceReceiptBuilder()
    await balanceReceiptBulder.initializeAsync()
    const balances = [
      {
        balance_type: BALANCE_TYPE.DEPOSIT,
        amount: 12000,
        currency: 'jpy',
        payment_method_name: '現金',
        balance_reason_name: '小口入金'
      }
    ]
    const createdAt = new Date("2017-07-07 09:30:00")
    const receiptObj = balanceReceiptBulder.buildBalanceReceipt(balances, createdAt)
    const receiptTextConverter = new ReceiptTextConverter()
    const balanceReceiptText = receiptTextConverter.contentToText(receiptObj.content, propertyA)
    expect(balanceReceiptText).toBe(targetReceiptText)
  })
})
