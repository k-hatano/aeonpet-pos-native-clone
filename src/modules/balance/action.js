import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listBalances = createAction(`${MODULE_NAME}_listBalances`)
export const listBalanceReasons = createAction(`${MODULE_NAME}_listBalanceReasons`)
export const selectBalance = createAction(`${MODULE_NAME}_selectBalance`)
export const selectBalanceType = createAction(`${MODULE_NAME}_selectBalanceType`)
export const selectDepositReason = createAction(`${MODULE_NAME}_selectDepositReason`)
export const selectWithdrawalReason = createAction(`${MODULE_NAME}_selectWithdrawalReason`)
export const updateBalanceAmount = createAction(`${MODULE_NAME}_updateBalanceAmount`)
