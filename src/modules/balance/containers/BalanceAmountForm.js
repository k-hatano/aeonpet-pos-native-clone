import { connect } from 'react-redux'
import BalanceAmountForm from '../components/BalanceAmountForm'
import { MODULE_NAME, MAX_AMOUNT, SUPPORTED_CHARGE_AMOUNTS, isEmoneyCharge, getSupputedAmountString } from '../models'
import { updateBalanceAmount } from '../action'
import { canCompleteCreatingBalance } from '../services'
import AlertView from 'common/components/widgets/AlertView'
import I18n from 'i18n-js'

const mapDispatchToProps = dispatch => ({
  onAmountChanged: async (amount, selectedBalance) => {
    if (amount > MAX_AMOUNT) {
      AlertView.show(I18n.t('message.H-01-E001'))
      return
    }
    if (amount > 0 &&
        isEmoneyCharge(selectedBalance) &&
        !SUPPORTED_CHARGE_AMOUNTS.includes(amount)) {
      AlertView.show(I18n.t('message.H-01-E004', {supportAmountString: getSupputedAmountString()}))
      return
    }
    dispatch(updateBalanceAmount(amount))
  }
})

const mapStateToProps = state => ({
  amount: state[MODULE_NAME].selectedBalance ? state[MODULE_NAME].selectedBalance.amount : 0,
  balanceType: state[MODULE_NAME].selectedBalanceType,
  canComplete: canCompleteCreatingBalance(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(BalanceAmountForm)
