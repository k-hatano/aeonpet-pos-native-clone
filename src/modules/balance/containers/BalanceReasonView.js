import { connect } from 'react-redux'
import BalanceReasonView from '../components/BalanceReasonView'
import { MODULE_NAME, BALANCE_TYPE } from '../models'
import * as actions from '../action'
import BalanceReasonRepository from '../repositories/BalanceReasonRepository'

const selectBalanceReason = (dispatch, balanceType, balanceReason) => {
  if (balanceType === BALANCE_TYPE.DEPOSIT) {
    dispatch(actions.selectDepositReason(balanceReason))
    dispatch(actions.updateBalanceAmount(0))
  } else {
    dispatch(actions.selectWithdrawalReason(balanceReason))
  }
}

const mapDispatchToProps = dispatch => ({
  onSelectBalanceReason: async (balanceType, balanceReason) => {
    const balanceReasonValue = balanceReason.id ? balanceReason : null
    selectBalanceReason(dispatch, balanceType, balanceReasonValue)
  },
  onSelectBalanceType: async (balanceType, balanceReason, balance) => {
    try {
      dispatch(actions.selectBalanceType(balanceType))
      const balanceReasons = await BalanceReasonRepository.findByBalanceType(balanceType)
      dispatch(actions.listBalanceReasons(balanceReasons))
      selectBalanceReason(dispatch, balanceType, balanceReason)
      dispatch(actions.selectBalance(balance))
    } catch (error) {
      dispatch(actions.listBalanceReasons([]))
      console.error(error)
    }
  }
})

const mapStateToProps = state => ({
  selectedBalanceType: state[MODULE_NAME].selectedBalanceType,
  balanceReasons: state[MODULE_NAME].balanceReasons,
  selectedDepositReason: state[MODULE_NAME].selectedDepositReason,
  selectedWithdrawalReason: state[MODULE_NAME].selectedWithdrawalReason,
  depositBalances: state[MODULE_NAME].depositBalances,
  withdrawalBalances: state[MODULE_NAME].withdrawalBalances
})

export default connect(mapStateToProps, mapDispatchToProps)(BalanceReasonView)
