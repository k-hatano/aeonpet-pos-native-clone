import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const CashierBalanceReason = sequelize.define(
  'CashierBalanceReason',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    cashier_balance_reason_code: Sequelize.STRING,
    balance_type: Sequelize.INTEGER,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'cashier_balance_reasons',
    underscored: true,
    timestampe: false
  }
)

export default CashierBalanceReason
