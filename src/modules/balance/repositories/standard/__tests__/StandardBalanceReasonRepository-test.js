jest.mock('react-native')
jest.mock('react-native-filesystem-v1')
jest.mock('common/DB')

import StandardBalanceReasonRepository from '../StandardBalanceReasonRepository'
import Migrator from 'common/utils/migrator'
import { BALANCE_TYPE } from '../../../models'

describe('StandardStaffRoleRepository test sample', () => {
  beforeEach(async () => {
    await Migrator.up()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('Create samples', async () => {
    const repository = new StandardBalanceReasonRepository()
    await repository.createSamples()
    const depositReasons = await repository.findByBalanceType(BALANCE_TYPE.DEPOSIT)
    expect(depositReasons[0].balance_type).toBe(BALANCE_TYPE.DEPOSIT)
    const withdrawalReasons = await repository.findByBalanceType(BALANCE_TYPE.WITHDRAWAL)
    expect(withdrawalReasons[0].balance_type).toBe(BALANCE_TYPE.WITHDRAWAL)
  })
})
