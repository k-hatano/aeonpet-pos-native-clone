import CashierBalanceReason from '../entities/CashierBalanceReasonEntity'
import { balanceReasons } from '../sample/SampleBalanceReasonRepository'
import AppEvents from 'common/AppEvents'

export default class StandardBalanceReasonRepository {
  async findByBalanceType (balanceType) {
    return CashierBalanceReason.findAll({
      where: {
        balance_type: balanceType,
      },
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async findByBalanceName (balanceName) {

    return CashierBalanceReason.findOne({
      where: {
        name: {
          $like : '%'+ balanceName +'%'
        },
      },
      order: [
        ['sort_order', 'DESC']
      ],
      raw: true
    })
  }
  
  async createSamples () {
    const existingCount = await CashierBalanceReason.count()
    console.log('CashierBalanceReason count => ', existingCount)
    if (existingCount === 0) {
      await CashierBalanceReason.bulkCreate(
        balanceReasons()
      )
    }
  }
}

AppEvents.onSampleDataCreate('StandardBalanceReasonRepository', async () => {
  return (new StandardBalanceReasonRepository()).createSamples()
})
