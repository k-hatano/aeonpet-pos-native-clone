import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'
import { encodeCriteria } from 'common/utils/searchUtils'

export default class StanderdCashierBalanceRepository {
  async bulkPush (balances) {
    if (balances.length === 1) {
      try {
        await fetcher.post('cashier-balances', balances[0])
      } catch (error) {
        handleAxiosError(error)
      }
    } else {
      throw new Error('Not implemented')
    }
  }
  async fetchAccountAmount (cashierId) {
    try {
      const amount = await fetcher.get(
        'cashier-balances/amount', {
          cashier_id: cashierId
        }
      )
      return amount.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
