import SampleBalanceReasonRepository from './sample/SampleBalanceReasonRepository'
import StandardBalanceReasonRepository from './standard/StandardBalanceReasonRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class BalanceReasonRepository {
  static _implement = new SampleBalanceReasonRepository()

  static async findByBalanceType (balanceType) {
    return this._implement.findByBalanceType(balanceType)
  }

  static async findByBalanceName (balanceName) {
    return this._implement.findByBalanceName(balanceName)
  }
  
  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardBalanceReasonRepository()
        break

      default:
        this._implement = new SampleBalanceReasonRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('BalanceReasonRepository', (context) => {
  BalanceReasonRepository.switchImplement(context)
})
