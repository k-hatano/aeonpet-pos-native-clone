import generateUuid from '../../../../common/utils/generateUuid'
import { BALANCE_TYPE } from '../../models'


export const balanceReasons = () => {
  return [
    {
      id: generateUuid(),
      name: '入金理由１',
      cashier_balance_reason_code: 'cbi1',
      balance_type: BALANCE_TYPE.DEPOSIT,
      sort_order: 1
    },
    {
      id: generateUuid(),
      name: '入金理由2',
      cashier_balance_reason_code: 'cbi2',
      balance_type: BALANCE_TYPE.DEPOSIT,
      sort_order: 2
    },
    {
      id: generateUuid(),
      name: '入金理由3',
      cashier_balance_reason_code: 'cbi3',
      balance_type: BALANCE_TYPE.DEPOSIT,
      sort_order: 3
    },
    {
      id: generateUuid(),
      name: '入金理由4',
      cashier_balance_reason_code: 'cbi4',
      balance_type: BALANCE_TYPE.DEPOSIT,
      sort_order: 4
    },
    {
      id: generateUuid(),
      name: '入金理由5',
      cashier_balance_reason_code: 'cbi5',
      balance_type: BALANCE_TYPE.DEPOSIT,
      sort_order: 5
    },
    {
      id: generateUuid(),
      name: '出金理由1',
      cashier_balance_reason_code: 'cbo1',
      balance_type: BALANCE_TYPE.WITHDRAWAL,
      sort_order: 6
    },
    {
      id: generateUuid(),
      name: '出金理由2',
      cashier_balance_reason_code: 'cbo2',
      balance_type: BALANCE_TYPE.WITHDRAWAL,
      sort_order: 7
    },
    {
      id: generateUuid(),
      name: '出金理由3',
      cashier_balance_reason_code: 'cbo3',
      balance_type: BALANCE_TYPE.WITHDRAWAL,
      sort_order: 8
    },
    {
      id: generateUuid(),
      name: '出金理由4',
      cashier_balance_reason_code: 'cbo4',
      balance_type: BALANCE_TYPE.WITHDRAWAL,
      sort_order: 9
    },
    {
      id: generateUuid(),
      name: '出金理由5',
      cashier_balance_reason_code: 'cbo5',
      balance_type: BALANCE_TYPE.WITHDRAWAL,
      sort_order: 10
    }
  ]
}

export default class SampleBalanceReasonRepository {
  async findByBalanceType (balanceType) {
    console.log('balanceReasons => ', balanceReasons)
    return balanceReasons().filter(reason => (reason.balance_type === balanceType))
  }
  
  async findByBalanceName (balanceName) {
    console.log('balanceReasons => ', balanceReasons)
    return balanceReasons().filter(reason => (reason.name.indexOf(balanceName) !== -1))
  }
}
