import SampleCashierBalanceRepository from './sample/SampleCashierBalanceRepository'
import StandardCashierBalanceRepository from './standard/StandardCashierBalanceRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierBalanceRepository {
  static _implement = new SampleCashierBalanceRepository()

  static async bulkPush (balance) {
    return this._implement.bulkPush(balance)
  }

  /**
   * 帳簿在高をサーバーから取得します。
   * @param cashierId
   * @returns {Promise}
   */
  static async fetchAccountAmount (cashierId) {
    return this._implement.fetchAccountAmount(cashierId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashierBalanceRepository()
        break

      default:
        this._implement = new SampleCashierBalanceRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierBalanceRepository', (context) => {
  CashierBalanceRepository.switchImplement(context)
})
