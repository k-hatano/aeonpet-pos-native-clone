'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { Col, Grid } from 'react-native-easy-grid'
import TabPanel from '../../../common/components/widgets/TabPanel'
import BalanceReasonForm from './BalanceReasonForm'
import balanceStyles from '../styles/Balance'
import { BALANCE_TYPE } from '../models'
import PropTypes from 'prop-types'
import { View } from 'react-native'

export default class BalanceReasonView extends Component {
  static propTypes = {
    onSelectBalanceReason: PropTypes.func,
    onSelectBalanceType: PropTypes.func,
    selectedBalanceType: PropTypes.number,
    balanceReasons: PropTypes.array,
    selectedDepositReason: PropTypes.object,
    selectedWithdrawalReason: PropTypes.object,
    depositBalances: PropTypes.array,
    withdrawalBalances: PropTypes.array,
    amount: PropTypes.number
  }

  componentWillUnmount () {
    this.props.onSelectBalanceType(BALANCE_TYPE.DEPOSIT, null, this.props.depositBalances[0])
  }

  _renderBalanceReasonForm (balanceType) {
    const balances = balanceType === BALANCE_TYPE.DEPOSIT ? this.props.depositBalances : this.props.withdrawalBalances
    return <BalanceReasonForm
      onSelectBalanceReason={(balanceReason) => this.props.onSelectBalanceReason(balanceType, balanceReason)}
      selectedBalanceReason={balanceType === BALANCE_TYPE.DEPOSIT ? this.props.selectedDepositReason : this.props.selectedWithdrawalReason}
      balanceType={this.props.selectedBalanceType}
      balanceReasons={this.props.balanceReasons}
      balances={balances}/>
  }

  render () {
    return (
      <Col>
        <Grid style={[balanceStyles.reasonWrapper]}>
          {/* 出金タブの非表示
          <TabPanel tabWrapperStyle={balanceStyles.tabWrapperStyle} tabStyle={balanceStyles.tabStyle}>
          */}
          <View
            style={{paddingLeft: 15}}
            tabName={I18n.t('balance.deposit')}
            onTabClick={() => this.props.onSelectBalanceType(BALANCE_TYPE.DEPOSIT, this.props.selectedDepositReason, this.props.depositBalances[0])}>
            {this._renderBalanceReasonForm(BALANCE_TYPE.DEPOSIT)}
          </View>
          {/* 出金タブの非表示
            <View
              style={{paddingLeft: 15}}
              tabName={I18n.t('balance.withdrawal')}
              onTabClick={() => this.props.onSelectBalanceType(BALANCE_TYPE.WITHDRAWAL, this.props.selectedWithdrawalReason, this.props.withdrawalBalances[0])}>
              {this._renderBalanceReasonForm(BALANCE_TYPE.WITHDRAWAL)}
            </View>
          </TabPanel>
          */}
        </Grid>
      </Col>
    )
  }
}
