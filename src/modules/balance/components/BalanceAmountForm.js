'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid'
import NumberPad from '../../../common/components/elements/NumberPad'
import I18n from 'i18n-js'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import balanceStyles from '../styles/Balance'
import { balanceAmountToLabel, balanceTypeToProceedButtonLabel } from '../models'
import PropTypes from 'prop-types'
import { ProceedButton } from '../../../common/components/elements/StandardButton'
import AmountPad, { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import { CASH_AMOUNT_MAX_LENGTH } from 'common/models'

export default class BalanceAmountForm extends Component {
  static propTypes = {
    balanceType: PropTypes.number,
    amount: PropTypes.number,
    onAmountChanged: PropTypes.func,
    onComplete: PropTypes.func,
    canComplete: PropTypes.bool,
    selectedBalance: PropTypes.object
  }

  render () {
    return (
      <Col>
        <Grid style={{ padding: 15 }}>
          <Row>
            <View style={[balanceStyles.amountPanel]}>
              <View style={[balanceStyles.amountDisplayPanel]}>
                <View style={[balanceStyles.balanceAmountPanel]}>
                  <View style={[balanceStyles.verticalTitleCenter]}>
                    <Text style={[balanceStyles.balanceAmountTitle]}>{balanceAmountToLabel(this.props.balanceType)}</Text>
                  </View>
                  <View style={[balanceStyles.verticalTextCenter]}>
                    <Text style={[balanceStyles.balanceAmountText]}>{formatPriceWithCurrency(this.props.amount, 'jpy')}</Text>
                  </View>
                </View>
              </View>
              <View style={balanceStyles.numberPadContainer}>
                <View style={balanceStyles.numberPadWrapper}>
                  <AmountPad
                    mode={AMOUNT_PAD_MODE.CASH}
                    maxLength={CASH_AMOUNT_MAX_LENGTH}
                    containerStyles={balanceStyles.numberPad}
                    onComplete={value => {
                      this.props.onAmountChanged(value, this.props.selectedBalance)
                    }}
                  />
                </View>
              </View>
            </View>
          </Row>
          <Row style={{ height: 60, width: '100%' }}>
            <View style={[balanceStyles.proceedButtonWrapper]}>
              <ProceedButton
                disabled={!this.props.canComplete}
                style={balanceStyles.ProceedButton}
                onPress={this.props.onComplete}
                text={balanceTypeToProceedButtonLabel(this.props.balanceType)} />
            </View>
          </Row>
        </Grid>
      </Col>
    )
  }
}
