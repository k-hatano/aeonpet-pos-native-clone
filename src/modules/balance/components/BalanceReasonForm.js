import React, { Component } from 'react'
import { Text, View, ListView, TouchableOpacity, Image } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid'
import I18n from 'i18n-js'
import Select from '../../../common/components/elements/Select'
import balanceStyles from '../styles/Balance'
import PropTypes from 'prop-types'
import { BALANCE_TYPE, getSupputedAmountString, balanceTypeToReasonLabel, balanceTypeToPaymentLabel, balanceTypeToModalTitle } from '../models'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import BalancePaymentListItem from './BalancePaymentListItem'
import ModalFrame from 'common/components/widgets/ModalFrame'
import Modal from 'common/components/widgets/Modal'
import { hasSettingPayment } from 'modules/setting/models'

export default class BalanceReasonForm extends Component {
  static propTypes = {
    onSelectBalanceReason: PropTypes.func,
    balanceType: PropTypes.number,
    selectedBalanceReason: PropTypes.object,
    balanceReasons: PropTypes.array,
    balances: PropTypes.array,
    amount: PropTypes.number
  }

  componentWillMount () {
    this.setState({balanceReason: I18n.t('common.select_one')})
  }

  _renderEmoneyChargeCautionText () {
    return (
      <View>
        { !hasSettingPayment() &&
          <View>
            <Text style={balanceStyles.notSettingPaymentText}>
              {I18n.t('message.V-03-E001')}
            </Text>
            <Text style={balanceStyles.notSettingPaymentText}>
              {I18n.t('message.V-03-E002')}
            </Text>
          </View>
        }
        <Text style={balanceStyles.cautionText}>
          {I18n.t('balance.caution')}
        </Text>
        <Text style={balanceStyles.supportAmountText}>
          {getSupputedAmountString()}
        </Text>
      </View>
    )
  }

  render () {
    const balances = this.props.balances
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    return (
      <View>
        <View style={[balanceStyles.reasonContainer]}>
          <Text style={[balanceStyles.reasonTitle]}>
            {balanceTypeToReasonLabel(this.props.balanceType)}
          </Text>
          <View style={[balanceStyles.reasonSelector]}>
            <Select
              items={this.props.balanceReasons}
              selectedItem={this.props.selectedBalanceReason}
              displayProperty='name'
              onItemChange={item => {
                this.props.onSelectBalanceReason(item)
              }}
              placeholder={I18n.t('common.select_one')}
              selectorModalTitle={balanceTypeToModalTitle(this.props.balanceType)} />
          </View>
          <Text style={[balanceStyles.methodTitle]}>
            {balanceTypeToPaymentLabel(this.props.balanceType)}
          </Text>
          <ListView
            style={[balanceStyles.paymentMethodList]}
            enableEmptySections
            dataSource={dataSource.cloneWithRows(balances)}
            renderRow={(balance) => (
              <BalancePaymentListItem
                balance={balance} />)}
            renderSeparator={(sectionId, rowId) => (<View key={rowId} style={balanceStyles.paymentsSeparator} />)} />
        </View>
        {
          this.props.balanceType === BALANCE_TYPE.DEPOSIT && this._renderEmoneyChargeCautionText()
        }
      </View>
    )
  }
}

class ModalContent extends Component {
  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  }

  _renderReason (reason) {
    return (
      <TouchableOpacity style={{flex: 1, borderBottomWidth: 0.7, justifyContent: 'center'}} onPress={() => {
        this.props.onSelectBalanceReason(reason)
        this.props.onSetState(reason.name)
        Modal.close()
      }}>
        <Text style={{fontSize: 24}}>
          {reason.name}
        </Text>
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <ModalFrame style={{height: 64 * (this.props.balanceReasons.length + 1), width: 472, backgroundColor: '#fff'}} title={balanceTypeToModalTitle(this.props.balanceType)} onClose={() => Modal.close()} >
        <ListView
          contentContainerStyle={{flexDirection: 'column', height: '100%', marginRight: 52, marginLeft: 52}}
          dataSource={this.dataSource.cloneWithRows(this.props.balanceReasons)}
          renderRow={reason => this._renderReason(reason)} />
      </ModalFrame>
    )
  }
}
