import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/BalancePaymentListItem'
import baseStyle from 'common/styles/baseStyle'
import NumberBox from 'common/components/elements/NumberBox'
import { formatNumber } from '../../../common/utils/formats'
import { PAYMENT_METHOD_TYPE, paymentMethodTypeToImage } from 'modules/payment/models'

export default class BalancePaymentListItem extends Component {
  static propTypes = {
    balance: PropTypes.object
  }

  render () {
    /** @type CartPayment */
    const balance = this.props.balance
    return (
      <View style={styles.layoutRoot}>
        <Image source={paymentMethodTypeToImage(balance.payment_method_type)} style={{marginLeft: 17, marginRight: 17}} />
        <Text style={[baseStyle.mainText, styles.nameText]}>
          {balance.payment_method_name}
        </Text>
        <View style={[styles.numberBox]}>
          <Text style={[styles.mainText]}>
            {formatNumber(balance.amount)}
          </Text>
        </View>
        <Text style={styles.unitText}>
          {I18n.t('cart.amount_suffix')}
        </Text>
      </View>
    )
  }
}
