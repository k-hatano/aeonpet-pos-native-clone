import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton } from '../../../common/components/elements/StandardButton'
import CheckBox from '../../../common/components/elements/CheckBox'
import componentStyles from '../styles/SyncSettingView'
import PropTypes from 'prop-types'

export default class SyncSetting extends Component {
  static props = {
    onWholeSync: PropTypes.func,
    onIncrementalSync: PropTypes.func,
    onInitialize: PropTypes.func,
    onClose: PropTypes.func,
    onSyncOptionProductsChange: PropTypes.func,
    onSyncOptionMastersChange: PropTypes.func,
    canWholeSync: PropTypes.bool,
    canIncrementalSync: PropTypes.bool,
    syncOptionProducts: PropTypes.bool,
    syncOptionMasters: PropTypes.bool
  }
  async componentDidMount () {
    const { syncOptionProducts, syncOptionMasters } = this.props
    await this.props.onInitialize()
    this.props.isImmediateExec && await this.props.onIncrementalSync(syncOptionProducts, syncOptionMasters)
  }
  componentWillUnmount () {
    this.props.onClose()
  }
  render () {
    const { syncOptionProducts, syncOptionMasters } = this.props
    return (
      <View style={componentStyles.mainContainer}>
        <View style={componentStyles.checkboxContainer}>
          <View style={componentStyles.checkboxWrapper}>
            <CheckBox
              label={I18n.t('product.product')}
              labelStyle={componentStyles.checkboxStyle}
              onChange={this.props.onSyncOptionProductsChange}
              checked
            />
            <View>
              <CheckBox
                label={I18n.t('product.other')}
                labelStyle={componentStyles.checkboxStyle}
                onChange={this.props.onSyncOptionMastersChange}
                checked
              />
              <Text style={componentStyles.checkboxNote}>{I18n.t('sync_data.always_whole_sync')}</Text>
            </View>
          </View>
        </View>
        <View style={componentStyles.buttonsContainer}>
          <View style={componentStyles.firstButtonWrapper}>
            <CommandButton
              style={{width: 400, height: 48, fontSize: 24, fontWeight: 'bold'}}
              onPress={() => this.props.onIncrementalSync(syncOptionProducts, syncOptionMasters)}
              text={I18n.t('sync_data.incremental_sync')}
              disabled={!this.props.canIncrementalSync}
            />
          </View>
          <View style={componentStyles.secondButtonWrapper}>
            <OptionalCommandButton
              style={{width: 400, height: 48, fontSize: 24, fontWeight: 'bold'}}
              onPress={() => this.props.onWholeSync(syncOptionProducts, syncOptionMasters)}
              text={I18n.t('sync_data.whole_sync')}
              disabled={!this.props.canWholeSync}
            />
          </View>
        </View>
      </View>
    )
  }
}
