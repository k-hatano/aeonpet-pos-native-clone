import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import { View, Text, ListView } from 'react-native'
import moment from 'moment'
import I18n from 'i18n-js'
import { ProceedButton } from '../../../common/components/elements/StandardButton'
import componentStyles from '../styles/SyncResultView'
import PropTypes from 'prop-types'

export default class SyncResultView extends Component {
  static props = {
    syncResults: PropTypes.object,
    isSyncComplete: PropTypes.bool,
    isFromOpenSale: PropTypes.bool
  }
  static defaultProps = {
    completeSync: false
  }
  constructor (props) {
    super(props)

    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  }

  /**
   *
   * @param {SyncDataResult} item
   * @return {XML}
   * @private
   */
  _renderItem = (item) => {
    const textColor = !item.error ? {} : { color: '#f00' }
    return (
      <View style={componentStyles.syncItem}>
        <View style={componentStyles.syncItemWrapper}>
          <Text style={[componentStyles.syncItemTime, textColor]}>
            {moment.unix(item.syncedAt).format('YYYY-MM-DD HH:mm:ss')}
          </Text>
          <Text style={[componentStyles.syncItemTitle, textColor]}>
            {I18n.t(item.titleKey)}
          </Text>
          <Text style={[componentStyles.syncItemStatus, textColor]}>
            {!item.error ? I18n.t('sync_data.sync_succeed') : I18n.t('sync_data.sync_failed')}
          </Text>
          <Text style={[componentStyles.syncedCount, textColor]}>
            {item.itemsCount + I18n.t('common.item_count')}
          </Text>
        </View>
      </View>
    )
  }

  render () {
    return (
      <View style={componentStyles.syncResultContainer}>
        <ListView
          enableEmptySections
          dataSource={this.dataSource.cloneWithRows(this.props.syncResults.toArray())}
          renderRow={(item, rowId) => this._renderItem(item)} />
        <View style={componentStyles.buttonContainer}>
          <ProceedButton
            style={componentStyles.syncDoneButton}
            text={I18n.t('sync_data.sync_done')}
            disabled={!this.props.isSyncComplete}
            onPress={() => {
              if (this.props.isFromOpenSale) {
                Actions.openSales()
              } else {
                Actions.redirectPage({redirectTo: 'home'})
              }
            }
          } />
        </View>
      </View>
    )
  }
}
