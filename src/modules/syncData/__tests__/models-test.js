import { OTHER_CARD, PAYMENT_METHOD_TYPE } from '../../payment/models'
import { fixPaymentMethodsFromRemoteToLocal } from '../models'

describe('fixPaymentMethodsFromRemoteToLocal Test', () => {
  const makePaymentMethod = (number, paymentMethodType = PAYMENT_METHOD_TYPE.CASH) => ({
    id: '000000000000000000000000000000' + number,
    shop_id: '12345678901234567890123456789012',
    name: '支払い方法' + number,
    payment_method_type: paymentMethodType,
    is_offline: 0
  })

  const allPaymentMethod = Object.keys(PAYMENT_METHOD_TYPE).map(paymentMethodType => {
    return makePaymentMethod(paymentMethodType, paymentMethodType)
  }).concat([OTHER_CARD])

  it('shop_idがnullの支払い方法は除外される。', () => {
    const paymentMethods = [
      makePaymentMethod('01'),
      {
        ...makePaymentMethod('02'),
        shop_id: null
      }
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(1)
    expect(localPaymentMethods[0].id).toBe('00000000000000000000000000000001')
  })

  it('券つり、券差額は除外される。', () => {
    const paymentMethods = [
      makePaymentMethod('01', PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT),
      makePaymentMethod('02', PAYMENT_METHOD_TYPE.GIFT_SURPLUS)
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(0)
  });

  it('オンラインクレジットがある場合、その他カードが作られる', () => {
    const paymentMethods = [makePaymentMethod('01', PAYMENT_METHOD_TYPE.CREDIT)]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(2)
    expect(localPaymentMethods[1].id).toBe(OTHER_CARD.id)
  })

  it('オンラインクレジットがあっても店舗に紐付いていない場合、その他カードは作られない', () => {
    const paymentMethods = [{
      ...makePaymentMethod('01', PAYMENT_METHOD_TYPE.CREDIT),
      shop_id: null
    }]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(0)
  })

  it('オフラインクレジットがあってもその他カードは作られない', async () => {
    const paymentMethods = [{
      ...makePaymentMethod('01', PAYMENT_METHOD_TYPE.CREDIT),
      is_offline: 1
    }]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(1)
  });

  it('オンラインクレジットがあり、その他カードもある場合、サーバーのものが優先して使われる', () => {
    const paymentMethods = [
      makePaymentMethod('01', PAYMENT_METHOD_TYPE.CREDIT),
      {...OTHER_CARD, name: 'サーバー定義のその他カード'}
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(2)
    expect(localPaymentMethods[1].name).toBe('サーバー定義のその他カード')
  })

  it('サーバーのその他カードは、shop_idがnullでも利用される。', () => {
    const paymentMethods = [
      makePaymentMethod('01', PAYMENT_METHOD_TYPE.CREDIT),
      {...OTHER_CARD, name: 'サーバー定義のその他カード', shop_id: null}
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods[1].name).toBe('サーバー定義のその他カード')
  })

  it('shop_idがnullのサーバーのその他カードは、他のオンラインクレジットが無い場合は除外される。', () => {
    const paymentMethods = [
      {...OTHER_CARD, name: 'サーバー定義のその他カード', shop_id: null}
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(0)
  })

  it('現金、電子マネー、券つり有、券つり無、売掛については、特に増減なくそのまま利用される。', () => {
    const paymentMethods = [
      makePaymentMethod('01', PAYMENT_METHOD_TYPE.CASH),
      makePaymentMethod('02', PAYMENT_METHOD_TYPE.EMONEY),
      makePaymentMethod('03', PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE),
      makePaymentMethod('04', PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE),
      makePaymentMethod('05', PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE),
    ]
    const localPaymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethods)
    expect(localPaymentMethods.length).toBe(5)
  });
})

