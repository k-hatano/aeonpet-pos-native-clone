import { handleActions } from 'redux-actions'
import * as actions from './action'
import { List } from 'immutable'

const defaultState = {
  syncResults: List(),
  syncOptionProducts: true,
  syncOptionMasters: true,
  isSyncProgress: false,
  isSyncComplete: false
}

const handlers = {
  [actions.resetState]: (state, action) => ({
    ...state,
    ...{
      syncOptionProducts: true,
      syncOptionMasters: true,
      isSyncProgress: false,
      isSyncComplete: false
    }
  }),
  [actions.clearSyncResults]: (state, action) => ({
    ...state,
    ...{
      syncResults: state.syncResults.clear(),
      isSyncComplete: false
    }
  }),
  [actions.appendSyncResults]: (state, action) => ({
    ...state,
    ...{ syncResults: state.syncResults.push(action.payload) }
  }),
  [actions.setSyncOptionProducts]: (state, action) => ({
    ...state,
    ...{ syncOptionProducts: action.payload }
  }),
  [actions.setSyncOptionMasters]: (state, action) => ({
    ...state,
    ...{ syncOptionMasters: action.payload }
  }),
  [actions.beginSync]: (state, action) => ({
    ...state,
    ...{
      isSyncProgress: true,
      isSyncComplete: false
    }
  }),
  [actions.completeSync]: (state, action) => ({
    ...state,
    ...{
      isSyncProgress: false,
      isSyncComplete: true
    }
  }),
  [actions.abortSync]: (state, action) => ({
    ...state,
    ...{
      isSyncProgress: false,
      isSyncComplete: false
    }
  })
}
export default handleActions(handlers, defaultState)
