import { connect } from 'react-redux'
import SyncSettingView from '../components/SyncSettingView'
import { MODULE_NAME } from '../models'
import * as actions from '../action'
import { syncData } from '../services'

const mapDispatchToProps = dispatch => ({
  onWholeSync: async (syncOptionProducts, syncOptionMasters, settings) => {
    return syncData(dispatch, syncOptionProducts, syncOptionMasters, settings)
  },
  onIncrementalSync: async (syncOptionProducts, syncOptionMasters, settings) => {
    return syncData(dispatch, syncOptionProducts, syncOptionMasters, settings, true)
  },
  onInitialize: async () => {
    dispatch(actions.clearSyncResults())
  },
  onClose: async () => {
    dispatch(actions.resetState())
  },
  onSyncOptionProductsChange: async value => {
    dispatch(actions.setSyncOptionProducts(value))
  },
  onSyncOptionMastersChange: async value => {
    dispatch(actions.setSyncOptionMasters(value))
  }
})

const mapStateToProps = state => ({
  canWholeSync: _canWholeSync(state),
  canIncrementalSync: _canIncrementalSync(state),
  syncOptionProducts: state[MODULE_NAME].syncOptionProducts,
  syncOptionMasters: state[MODULE_NAME].syncOptionMasters,
  settings: state.setting.settings
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...dispatchProps,
    ...stateProps,
    onIncrementalSync: async (syncOptionProducts, syncOptionMasters) => {
      dispatchProps.onIncrementalSync(syncOptionProducts, syncOptionMasters, stateProps.settings)
    },
    onWholeSync: async (syncOptionProducts, syncOptionMasters) => {
      dispatchProps.onWholeSync(syncOptionProducts, syncOptionMasters, stateProps.settings)
    }
  }
}

const _canWholeSync = (state) => {
  const moduleState = state[MODULE_NAME]
  if (moduleState.isSyncProgress) return false
  // どちらかにチェックされていれば、全同期できる。
  return (moduleState.syncOptionProducts || moduleState.syncOptionMasters)
}
const _canIncrementalSync = (state) => {
  const moduleState = state[MODULE_NAME]
  if (moduleState.isSyncProgress) return false
  // 商品同期にチェックが入っていれば、差分同期可能
  return moduleState.syncOptionProducts
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(SyncSettingView)
