import { connect } from 'react-redux'
import SyncResultView from '../components/SyncResultView'
import { MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  syncResults: state[MODULE_NAME].syncResults,
  isSyncComplete: _isSyncComplete(state)
})

const _isSyncComplete = state => {
  const moduleState = state[MODULE_NAME]
  return moduleState.isSyncComplete
}
export default connect(mapStateToProps, mapDispatchToProps)(SyncResultView)
