import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  rowHeader: {
    height: 72,
    zIndex: 1
  },
  rowTopMenus: {
    paddingTop: 37,
    paddingLeft: 39,
    paddingBottom: 20,
    alignItems: 'flex-start'
  },
  rowMainButtonsStyle: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowMainMenusStyle: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 10
  },
  leftColumn: {
    borderColor: '#979797',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0
  }
})
