import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  buttonContainer: {
    borderTopWidth: 1,
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  syncDoneButton: {
    height: 56,
    width: 266,
    margin: 20,
    fontSize: 24,
    fontWeight: 'bold'
  },
  syncResultContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  syncResultItemsContainer: {
    width: '100%',
    flex: 1,
    padding: 22,
    paddingTop: 16
  },
  syncScrollViewContainer: {
    width: '100%',
    height: '100%'
  },
  syncItem: {
    flexDirection: 'row',
    flex: 1,
    height: 48,
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: 'transparent',
    marginHorizontal: 10
  },
  syncItemWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  syncItemTime: {
    flex: 1.5,
    fontFamily: 'Helvetica',
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29
  },
  syncItemTitle: {
    flex: 2,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    height: 20
  },
  syncItemStatus: {
    flex: 1,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    marginLeft: 5,
    height: 20
  },
  syncedCount: {
    flex: 1,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    height: 20,
    textAlign: 'right'
  }
})

export default styles
