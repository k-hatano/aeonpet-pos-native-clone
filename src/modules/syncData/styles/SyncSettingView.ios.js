import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  checkboxContainer: {
    marginTop: 72,
    flex: 3,
    alignItems: 'center',
    flexDirection: 'column'
  },
  checkboxWrapper: {
    justifyContent: 'space-around',
    width: '70%',
    height: '60%'
  },
  checkboxStyle: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  checkboxNote: {
    fontSize: 24,
    marginLeft: 20
  },
  buttonsContainer: {
    alignItems: 'center',
    flex: 5.5,
    flexDirection: 'column',
    width: '100%'
  },
  firstButtonWrapper: {
    flex: 1,
    width: '50%',
    alignItems: 'center'
  },
  standardButtonText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  standardButtonContainer: {
    height: 48
  },
  secondButtonWrapper: {
    flex: 2,
    width: '50%',
    alignItems: 'center'
  }
})
