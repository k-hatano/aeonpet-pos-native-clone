import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  checkboxContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  checkboxWrapper: {
    justifyContent: 'space-around',
    width: '70%',
    height: '60%'
  },
  checkboxStyle: {
    fontSize: 28
  },
  checkboxNote: {
    fontSize: 24,
    marginLeft: 20
  },
  buttonsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 4,
    flexDirection: 'column',
    width: '100%'
  },
  firstButtonWrapper: {
    flex: 1,
    width: '50%',
    alignItems: 'center'
  },
  standardButtonText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  standardButtonContainer: {
    height: 48
  },
  secondButtonWrapper: {
    flex: 2,
    width: '50%',
    alignItems: 'center'
  }
})
