/**
 * @typedef {Object} SyncDataResult
 * @property {string} titleKey データ同期画面に表示する名前の翻訳キー
 * @property {integer} syncedAt 同期完了日時
 * @property {integer} itemsCount
 * @property {Object|undefined} error
 */

import I18n from 'i18n-js'
import logger from 'common/utils/logger'
import { clearSyncResults, beginSync, appendSyncResults, completeSync, abortSync } from './action'
import SyncDataRepository from './repositories/SyncDataRepository'
import {
  updateReceiptParameter,
  updateOrderParameter,
  mapCashierPropertySetToSettings,
  mapOrderToSettings,
  updateReceiptStoreImage
} from 'modules/setting/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { updateSetting } from 'modules/setting/actions'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import moment from 'moment'
import { loading } from '../../common/sideEffects'
import CashierRepository from '../cashier/repositories/CashierRepository'
import OrderRepository from '../order/repositories/OrderRepository'
import { SYNC_DATA_TYPE } from './models'
import NetworkError from '../../common/errors/NetworkError'
import AlertView from '../../common/components/widgets/AlertView'
import ShopRepository from '../shop/repositories/ShopRepository'

export const syncData = async (dispatch, syncOptionProducts, syncOptionMasters, settings, isIncremental = false) => {
  const startAt = new Date()
  const lastSyncedAt = isIncremental ? settings[SettingKeys.COMMON.LAST_SYNCED_AT] : null
  dispatch(clearSyncResults())
  dispatch(beginSync())
  logger.info('Start wholeSync products=' + syncOptionProducts + ' masters=' + syncOptionMasters)

  try {
    await loading(dispatch, async () => {
      await SyncDataRepository.syncRemoteToLocal(
        buildSyncDataTypes(syncOptionProducts, syncOptionMasters),
        (item) => dispatch(appendSyncResults(item)),
        [syncOrderNumberSequence.bind(null, dispatch, settings[SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE])],
        lastSyncedAt)
      await syncReceiptSettingStorage(dispatch)
      await syncOrderSettingStorage(dispatch)
    })

    await saveOperationLog(OPERATION_TYPE.COMPLETED_SYNC, new Date())
    if (syncOptionProducts) {
      dispatch(updateSetting({
        key: SettingKeys.COMMON.LAST_SYNCED_AT,
        value: moment(startAt).unix()
      }))
    }
    if (syncOptionMasters) {
      // 「商品以外」での同期にスタッフの同期が含まれるようになったので、
      // 設定画面での同期に倣い設定の更新を行う
      dispatch(updateSetting({
        key: SettingKeys.STAFF.CURRENT_STAFF,
        value: null
      }))
      dispatch(updateSetting({
        key: SettingKeys.STAFF.SYNC_DATE,
        value: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
      }))
      const shop = await ShopRepository.find()
      dispatch(updateSetting({
        key: SettingKeys.NETWORK.ACCESS.SHOP_NAME,
        value: shop.name
      }))
      dispatch(updateSetting({
        key: SettingKeys.COMMON.USEABLE_BASE_POINTS,
        value: shop.useable_base_points
      }))
      const cashier = await CashierRepository.find()
      dispatch(updateSetting({
        key: SettingKeys.NETWORK.ACCESS.CASHIER_NAME,
        value: cashier.name
      }))
    }
    dispatch(completeSync())
    logger.info('wholeSync success')
  } catch (error) {
    await saveOperationLog(OPERATION_TYPE.FAILED_SYNC, new Date())
    if (error instanceof NetworkError) {
      switch (error.type) {
        case NetworkError.TYPE.OFFLINE:
          await AlertView.showAsync(I18n.t('message.A-01-E100'))
          break
        case NetworkError.TYPE.TIMEOUT:
          await AlertView.showAsync(I18n.t('message.A-09-E002'))
          break
        default:
          await AlertView.showAsync(I18n.t('message.A-09-E001'))
      }
    } else {
      await AlertView.showAsync(I18n.t('message.A-09-E001'))
    }
    await AlertView.showAsync(I18n.t('message.A-09-E003'))
    dispatch(abortSync())
    logger.info('wholeSync error')
  }
}

const syncReceiptSettingStorage = async (dispatch) => {
  const values = await mapCashierPropertySetToSettings()
  updateReceiptParameter(values, 'BILL', dispatch)
  updateReceiptParameter(values, 'CASHIER_TOTAL', dispatch)
  updateReceiptParameter(values, 'COMMON', dispatch)
  updateReceiptParameter(values, 'ACCOUNTING', dispatch)
  await updateReceiptStoreImage(values, dispatch)
}

const syncOrderSettingStorage = async (dispatch) => {
  const values = await mapOrderToSettings()
  updateOrderParameter(values, 'TAX', dispatch)
}

/**
 *
 * @param dispatch
 * @param orderNumberSequence
 * @return {Promise.<{SyncDataResult}>}
 */
const syncOrderNumberSequence = async (dispatch, orderNumberSequence) => {
  try {
    const cashier = await CashierRepository.find()
    const posOrder = await OrderRepository.fetchLastPosOrderByCashierId(cashier.id)
    const posOrderNumberSequence = posOrder ? posOrder.pos_order_number_sequence : null
    logger.info(
      `pos_order_number_sequence=${posOrderNumberSequence} 
      orderNumberSequence=${orderNumberSequence}`)
    if (posOrder && posOrder.pos_order_number_sequence > orderNumberSequence) {
      dispatch(updateSetting({
        key: SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE,
        value: posOrder.pos_order_number_sequence
      }))
    }

    return {
      titleKey: 'sync_data.data_type_order_sequence',
      itemsCount: posOrder ? 1 : 0,
      syncedAt: moment().unix()
    }
  } catch (error) {
    return {
      titleKey: 'sync_data.data_type_order_sequence',
      itemsCount: 0,
      syncedAt: moment().unix(),
      error
    }
  }
}

const buildSyncDataTypes = (syncOptionProducts, syncOptionMasters) => {
  const types = []
  if (syncOptionProducts) types.push(SYNC_DATA_TYPE.PRODUCTS)
  if (syncOptionMasters) types.push(SYNC_DATA_TYPE.MASTER)
  return types
}
