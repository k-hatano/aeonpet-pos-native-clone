import _ from 'underscore'
import fetcher from 'common/models/fetcher'
import logger from 'common/utils/logger'
import { encodeCriteria } from 'common/utils/searchUtils'
import moment from 'moment'
import settingGetter from 'modules/setting/models/settingGetter'
import Product from 'modules/product/repositories/entities/ProductEntity'
import ProductVariant from 'modules/product/repositories/entities/ProductVariantEntity'
import ProductGroup from 'modules/product/repositories/entities/ProductGroupEntity'
import ProductCategory from 'modules/product/repositories/entities/ProductCategoryEntity'
import ProductProductCategory from 'modules/product/repositories/entities/ProductProductCategoryEntity'
import ProductProductVariant from 'modules/product/repositories/entities/ProductProductVariantEntity'
import ProductProductTag from 'modules/product/repositories/entities/ProductProductTagEntity'
import MultipleTaxRates from 'modules/product/repositories/entities/MultipleTaxRatesEntity'
import Bundle from 'modules/promotion/repositories/entities/BundleEntity'
import BundlePattern from 'modules/promotion/repositories/entities/BundlePatternEntity'
import SaleDiscount from 'modules/promotion/repositories/entities/SaleDiscountEntity'
import CouponDiscount from 'modules/promotion/repositories/entities/CouponDiscountEntity'
import StockItem from 'modules/stock/repositories/entities/StockItemEntity'
import Warehouse from 'modules/stock/repositories/entities/WarehouseEntity'
import PaymentMethod from 'modules/payment/repositories/entities/PaymentMethodEntity'
import CashVoucher from 'modules/cash/repositories/entities/CashVoucherEntity'
import DiscountReason from 'modules/promotion/repositories/entities/DiscountReasonEntity'
import CashierBalanceReason from 'modules/balance/repositories/entities/CashierBalanceReasonEntity'
import StockAdjustmentReason from 'modules/stock/repositories/entities/StockAdjustmentReasonEntity'
import Countries from 'modules/geo/repositories/entities/CountriesEntity'
import POSOrderProperty from 'modules/order/repositories/entities/POSOrderPropertyEntity'
import PromotionPreset from 'modules/promotion/repositories/entities/PromotionPresetEntity'
import CashierParameterSet from 'modules/setting/repositories/entities/CashierParameterSetEntity'
import CashierSetting from 'modules/setting/repositories/entities/CashierSettingEntity'
import Shop from 'modules/shop/repositories/entities/ShopEntity'
import Regions from 'modules/geo/repositories/entities/RegionsEntity'
import ShopPointRatio from 'modules/promotion/repositories/entities/ShopPointRatioEntity'
import ProductPointRatio from 'modules/promotion/repositories/entities/ProductPointRatioEntity'
import PromotionMessages from 'modules/promotion/repositories/entities/PromotionMessagesEntity'
import { fixPaymentMethodsFromRemoteToLocal, SYNC_DATA_TYPE, toBundleEntity, toSalesEntity, toCouponEntity } from '../../models'
import { handleAxiosError, handleDatabaseError } from '../../../../common/errors'
import { sequelize } from 'common/DB'
import DbError from '../../../../common/errors/DbError'
import { profileMemory } from '../../../../common/utils/debug'
import Staff from 'modules/staff/repositories/entities/StaffEntity'
import StaffRole from 'modules/staff/repositories/entities/StaffRoleEntity'
import StaffRolePermission from 'modules/staff/repositories/entities/StaffRolePermissionEntity'
import { isNullOrUndefined } from 'common/models'
import Cashier from 'modules/cashier/repositories/entities/CashierEntity'

/**
 * @typedef {Object} SyncDataDefinition
 * @property {string} titleKey データ同期画面に表示する名前の翻訳キー
 * @property {bool|undefined} canDifferentialSync 差分同期に対応しているか否か
 * @property {integer} syncType SYNC_TYPEのどちらか
 * @property {function} fetchItems サーバからデータを取得する非同期メソッド
 * @property {bool} paging ページングをするか否か。する場合、件数で区切って複数のリクエストでデータを取得する
 * @property {Class|undefined} entityClass 保存先テーブルを表すエンティティクラス
 * @property {function|undefined} toEntities サーバから取得したデータを保存する形式に変換する
 * @property {function|undefined} saveEntities entityClassが存在しない場合の保存処理を行う
 */

export default class StandardSyncDataRepository {
  /**
   *
   * @param {Array.<int>} syncDataTypes
   * @param {function} notifyProgress 進行状況を通知するコールバック
   * @param {Array.<function>} additionalProcesses 本リポジトリ内で扱わない追加の同期処理
   * @param {integer|null} lastSyncedAt 差分同期をする場合、最終更新日時(unixtime)を指定する
   * @return {Promise.<Array.<SyncDataResult>>}
   */
  async syncRemoteToLocal (syncDataTypes, notifyProgress, additionalProcesses, lastSyncedAt = null) {
    const results = []
    const syncDataDefinitions = StandardSyncDataRepository._FilteredSyncItemDefinitions(syncDataTypes)
    await sequelize.transaction(async (transaction) => {
      for (let i = 0; i < syncDataDefinitions.length; i++) {
        const definition = syncDataDefinitions[i]
        try {
          const itemsCount = await StandardSyncDataRepository._syncRemoteToLocal(definition, transaction, lastSyncedAt)
          results.push({
            titleKey: definition.titleKey,
            itemsCount,
            syncedAt: moment().unix()
          })
          notifyProgress(_.last(results))
        } catch (error) {
          await logger.warning('Sync data error ' + error.message)
          results.push({
            titleKey: definition.titleKey,
            itemsCount: 0,
            syncedAt: moment().unix(),
            error
          })
          notifyProgress(_.last(results))
          throw error
        }
      }
      for (let i = 0; i < additionalProcesses.length; i++) {
        const result = await additionalProcesses[i]()
        notifyProgress(result)
        if (result.error) throw result.error
        results.push(result)
      }
    })
    return results
  }

  /**
   *
   * @param {SyncDataDefinition} definition
   * @param transaction
   * @param {integer|null} lastSyncedAt
   * @return {Promise.<void>}
   * @private
   */
  static async _syncRemoteToLocal (definition, transaction, lastSyncedAt) {
    const isDifferential = !!lastSyncedAt && definition.canDifferentialSync
    let itemsCount = 0

    if (!isDifferential && definition.entityClass) {
      // 全同期の場合はここで全削除。差分同期の場合だけデータ取得後に削除
      try {
        await profileMemory('SyncData.destroyAll.' + definition.titleKey, async () => {
          await definition.entityClass.destroy({ truncate: true, transaction })
        })
      } catch (error) {
        handleDatabaseError(error)
        throw new DbError()
        // TODO handleDatabaseErrorでDbErrorを投げる(他の処理の動作保証ができなくなるので、一旦ここで別に投げる)
      }
    }

    let pageIndex = 0
    let pages
    do {
      pageIndex++

      let data
      try {
        const response = await profileMemory(
          'SyncData.fetchItems.' + definition.titleKey + '.' + pageIndex,
          () => definition.fetchItems({pageIndex, lastSyncedAt}))
        data = response.data
        pages = response.pagination.pages
      } catch (error) {
        handleAxiosError(error)
      }

      // １件のレコードを取得するAPIにアクセスした場合にはオブジェクトが１つ返ってくるが、後の処理を統一化するため配列にする
      if (!Array.isArray(data)) data = [data]

      const entities = await profileMemory(
        'SyncData.toEntities.' + definition.titleKey + '.' + pageIndex,
        () => definition.toEntities ? definition.toEntities(data) : data)
      await profileMemory(
        'SyncData.convertDateTime.' + definition.titleKey + '.' + pageIndex,
        () => entities.forEach(this._convertDateTime))
      itemsCount += definition.countItems ? definition.countItems(entities) : entities.length

      try {
        if (isDifferential && definition.entityClass) {
          const entityIds = entities.map(entity => entity.id)
          await definition.entityClass.destroy({ where: { id: entityIds }, transaction })
        }
        if (definition.entityClass) {
          await profileMemory(
            'SyncData.bulkCreate.' + definition.titleKey + '.' + pageIndex,
            () => definition.entityClass.bulkCreate(entities, { transaction }))
        }
        if (!definition.entityClass) {
          await definition.saveEntities(entities, definition, transaction, isDifferential, pageIndex)
        }
      } catch (error) {
        handleDatabaseError(error)
        throw new DbError()
        // TODO handleDatabaseErrorでDbErrorを投げる(他の処理の動作保証ができなくなるので、一旦ここで別に投げる)
      }
    } while (definition.paging && pageIndex < pages)
    return itemsCount
  }

  /**
   * 指定したSyncDataTypeの種類に対応したデータ同期定義を取得する
   * @param syncDataTypes
   * @return {Array.<SyncDataDefinition>}
   * @private
   */
  static _FilteredSyncItemDefinitions (syncDataTypes) {
    const typeIdMap = {}
    syncDataTypes.forEach(syncDataType => { typeIdMap[syncDataType] = true })
    return this._syncItemDefinitions().filter(syncDataItem => typeIdMap[syncDataItem.syncType])
  }

  /**
   * サーバの日付をDBへの保存形式に自動的に変換する。
   * パフォーマンス対策のため、引数を直接変更する
   * @param entity
   * @private
   */
  static _convertDateTime (entity) {
    // TODO Refactoring 日時はDate形式でなく、サーバからのレスポンス通りintegerで保存する
    if (entity.created_at) entity.created_at = new Date(entity.created_at * 1000)
    if (entity.updated_at) entity.updated_at = new Date(entity.updated_at * 1000)
    if (entity.deleted_at) entity.deleted_at = new Date(entity.deleted_at * 1000)
    // TODO Refactoring start_at, end_atは個別のentityで変換を定義する
    if (entity.start_at) entity.start_at = moment(entity.start_at).unix()
    if (entity.end_at) entity.end_at = moment(entity.end_at).unix()
  }

  /**
   * データ同期処理の定義を取得する
   * @return {Array.<SyncDataDefinition>}
   * @private
   */
  static _syncItemDefinitions () {
    return [
      {
        titleKey: 'sync_data.data_type_products',
        syncType: SYNC_DATA_TYPE.PRODUCTS,
        canDifferentialSync: true,
        fetchItems: async ({pageIndex, lastSyncedAt}) => {
          return fetcher.get('products',
            { limit: 5000, page: pageIndex, ...encodeCriteria({ updated_at__gte: lastSyncedAt, deleted_at: lastSyncedAt }) })
        },
        countItems: (entities) => {
          return entities.length
        },
        paging: true,
        saveEntities: async (entities, definition, transaction, isDifferential, pageIndex) => {
          if (isDifferential) {
            const entityIds = entities.map(entity => entity.id)
            await Product.destroy({ where: { id: entityIds }, transaction })
          } else {
            if (pageIndex === 1) {
              // 最初だけ全件あらいがえる
              await Product.destroy({truncate: true, transaction})
            }
          }
          const availableEntities = entities.filter(entity => isNullOrUndefined(entity.deleted_at))
          await profileMemory(
            'SyncData.bulkCreate.' + definition.titleKey + '.' + pageIndex,
            () => Product.bulkCreate(availableEntities, { transaction }))
        }
      },
      {
        titleKey: 'sync_data.data_type_product_variants',
        syncType: SYNC_DATA_TYPE.PRODUCTS,
        canDifferentialSync: true,
        fetchItems: async ({pageIndex, lastSyncedAt}) => {
          return fetcher.get('product-variants/sync',
            { limit: 5000, page: pageIndex, ...encodeCriteria({ updated_at__gte: lastSyncedAt }) })
        },
        countItems: (entities) => {
          return entities.length
        },
        paging: true,
        saveEntities: async (entities, definition, transaction, isDifferential, pageIndex) => {
          if (isDifferential) {
            const entityIds = entities.map(entity => entity.id)
            await ProductVariant.destroy({ where: { id: entityIds }, transaction })
          } else {
            if (pageIndex === 1) {
              // 最初だけ全件あらいがえる
              await ProductVariant.destroy({truncate: true, transaction})
            }
          }
          await profileMemory(
            'SyncData.bulkCreate.' + definition.titleKey + '.' + pageIndex,
            () => ProductVariant.bulkCreate(entities, { transaction }))
        }
      },
      {
        titleKey: 'sync_data.data_type_product_groups',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('product-groups')
        },
        entityClass: ProductGroup
      },
      {
        titleKey: 'sync_data.data_type_product_categories',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('product-categories',
            {
              search: 'shopProductCategories.status:1'
            }
          )
        },
        entityClass: ProductCategory
      },
      {
        titleKey: 'sync_data.data_type_product_product_categories',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('product-product-categories')
        },
        entityClass: ProductProductCategory
      },
      {
        titleKey: 'sync_data.data_type_product_product_variants',
        syncType: SYNC_DATA_TYPE.PRODUCTS,
        fetchItems: async () => {
          return fetcher.get('product-product-variants')
        },
        entityClass: ProductProductVariant
      },
      {
        titleKey: 'sync_data.data_type_product_product_tag',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('product-tags', {
            with: 'products'
          })
        },
        toEntities: (entities) => {
          return Array.prototype.concat.apply([], entities.map(obj => {
            return obj.products.map(p => {
              return {
                product_tag_id: obj.id,
                product_id: p.id,
                created_at: obj.created_at,
                updated_at: obj.updated_at
              }
            })
          }))
        },
        entityClass: ProductProductTag
      },
      {
        titleKey: 'sync_data.data_type_multiple_tax_rate',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('multiple-tax-rate')
        },
        entityClass: MultipleTaxRates
      },
      {
        titleKey: 'sync_data.data_type_bundles',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('bundles', {
            with: 'incentives.productTagIncentives',
            search: 'status:2'
          })
        },
        toEntities: (entities) => entities.map(toBundleEntity),
        saveEntities: async (entities, definition, transaction, isDifferential, pageIndex) => {
          await BundlePattern.destroy({truncate: true, transaction})
          await Bundle.destroy({truncate: true, transaction})
          // bundlePattern を取り出して bulkCreate する
          const bundlePatterns = Array.prototype.concat.apply([], entities.map(o => o.bundle_patterns))
          await BundlePattern.bulkCreate(bundlePatterns, { transaction })
          await Bundle.bulkCreate(entities, { transaction })
        }
      },
      {
        titleKey: 'sync_data.data_type_sale_discounts',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const shopId = settingGetter.shopId
          const shopTagIds = []
          const shopTags = await fetcher.get('shop-tags', {
            search: 'shops.id:' + shopId
          })
          shopTags.data.forEach(shopTag => shopTagIds.push(shopTag.id))

          return fetcher.get('sale-discounts', {
            with: 'incentives.productTagIncentives',
            ...encodeCriteria({
              status: 2,
              'incentives.shopTagIncentives.shop_tag_id': shopTagIds
            })
          })
        },
        toEntities: (entities) => entities.map(toSalesEntity),
        entityClass: SaleDiscount
      },
      {
        titleKey: 'sync_data.data_type_coupon_discounts',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const shopId = settingGetter.shopId
          const shopTagIds = []
          const shopTags = await fetcher.get('shop-tags', {
            search: 'shops.id:' + shopId
          })
          shopTags.data.forEach(shopTag => shopTagIds.push(shopTag.id))

          return fetcher.get('coupon-discounts', {
            with: 'incentives.productTagIncentives',
            ...encodeCriteria({
              status: 2,
              'incentives.shopTagIncentives.shop_tag_id': shopTagIds
            })
          })
        },
        toEntities: (entities) => entities.map(toCouponEntity),
        entityClass: CouponDiscount
      },
      {
        titleKey: 'sync_data.data_type_stock_items',
        syncType: SYNC_DATA_TYPE.PRODUCTS,
        canDifferentialSync: true,
        fetchItems: async ({lastSyncedAt}) => {
          return fetcher.get('stock-items', { ...encodeCriteria({updated_at__gte: lastSyncedAt}) })
        },
        entityClass: StockItem
      },
      {
        titleKey: 'sync_data.data_type_warehouses',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('warehouses', encodeCriteria({ status: 1 }))
        },
        entityClass: Warehouse
      },
      {
        titleKey: 'sync_data.data_type_payment_methods',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('payment-methods', encodeCriteria({ status: 1 }))
        },
        toEntities: fixPaymentMethodsFromRemoteToLocal,
        entityClass: PaymentMethod
      },
      {
        titleKey: 'sync_data.data_type_cash_voucher',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('cash-voucher')
        },
        entityClass: CashVoucher
      },
      {
        titleKey: 'sync_data.data_type_discount_reasons',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('discount-reasons', encodeCriteria({ status: 1 }))
        },
        toEntities: (entities) => {
          // 現在のAPIでは店舗紐付けがないものは shop_id = null で返ってくるのでPOS側で弾いとく
          return entities.filter(o => o.shop_id !== null)
        },
        entityClass: DiscountReason
      },
      {
        titleKey: 'sync_data.data_type_cashier_balance_reasons',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('cashier-balance-reasons')
        },
        toEntities: (entities) => {
          // 現在のAPIでは店舗紐付けがないものは shop_id = null で返ってくるのでPOS側で弾いとく
          return entities.filter(o => o.shop_id !== null)
        },
        entityClass: CashierBalanceReason
      },
      {
        titleKey: 'sync_data.data_type_stock_adjustment_reasons',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('stock-adjustment-reasons', encodeCriteria({ status: 1 }))
        },
        entityClass: StockAdjustmentReason
      },
      {
        titleKey: 'sync_data.data_type_countries',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('countries')
        },
        entityClass: Countries
      },
      {
        titleKey: 'sync_data.data_type_pos_order_properties',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('pos-order-property', encodeCriteria({ status: 1 }))
        },
        entityClass: POSOrderProperty
      },
      {
        titleKey: 'sync_data.data_type_promotion_presets',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('promotion-presets', encodeCriteria({ status: 1 }))
        },
        entityClass: PromotionPreset
      },
      {
        titleKey: 'sync_data.data_type_cashier_parameter_sets',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('cashier-parameter-sets')
        },
        toEntities: (entities) => {
          // DB上からなくなったけど、マイグレーション作るのめんどいので応急処置
          // TODO いつか治す
          entities[0].is_taxfree = 0
          return entities
        },
        entityClass: CashierParameterSet
      },
      {
        titleKey: 'sync_data.data_type_cashier_settings',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('cashier-settings')
        },
        entityClass: CashierSetting
      },
      {
        titleKey: 'sync_data.data_type_shops',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const shopId = settingGetter.shopId
          return fetcher.get(`shops/${shopId}`)
        },
        toEntities: (entities) => {
          const entity = entities[0]
          return entity ? [{ ...entity.shop_address, ...entity }] : []
        },
        entityClass: Shop
      },
      {
        titleKey: 'sync_data.data_type_staffs',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('staffs', {...encodeCriteria({ status: 1}), shouldFilterByStatus: true})
        },
        entityClass: Staff
      },
      {
        titleKey: 'sync_data.data_type_staff_roles',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('staff-roles')
        },
        saveEntities: async (entities, definition, transaction, isDifferential, pageIndex) => {
          await StaffRole.destroy({truncate: true, transaction})
          await StaffRolePermission.destroy({truncate: true, transaction})
          await StaffRole.bulkCreate(entities, { transaction })
          await StaffRolePermission.bulkCreate(_.flatten(entities.map(role => {
            return role.staff_role_permissions
          })), { transaction })
        }
      },
      {
        titleKey: 'sync_data.data_type_regions',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get('regions')
        },
        entityClass: Regions
      },
      {
        titleKey: 'sync_data.data_type_cashiers',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const cashierId = settingGetter.cashierId
          return fetcher.get(`cashiers/${cashierId}`)
        },
        entityClass: Cashier
      },
      {
        titleKey: 'sync_data.data_type_shop_point_calendar',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const shopId = settingGetter.shopId
          return fetcher.get(`point-promotions/shop/index/${shopId}`)
        },
        entityClass: ShopPointRatio
      },
      {
        titleKey: 'sync_data.data_type_product_point_calendar',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          const shopId = settingGetter.shopId
          return fetcher.get(`point-promotions/product/index/${shopId}`)
        },
        entityClass: ProductPointRatio
      },
      {
        titleKey: 'sync_data.data_type_promotion_message',
        syncType: SYNC_DATA_TYPE.MASTER,
        fetchItems: async () => {
          return fetcher.get(`promotion-messages`)
        },
        entityClass: PromotionMessages
      }
    ]
  }
}
