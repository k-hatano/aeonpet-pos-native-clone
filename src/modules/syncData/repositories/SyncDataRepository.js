import StandardSyncDataRepository from './standard/StandardSyncDataRepository'
import { REPOSITORY_CONTEXT } from '../../../common/AppEvents'

/**
 * データ同期リポジトリ
 */
export default class SyncDataRepository {
  static _implement = new StandardSyncDataRepository()

  /**
   *
   * @param {Array.<int>} syncDataTypes
   * @param {function} notifyProgress 進行状況を通知するコールバック
   * @param {Array.<function>} additionalProcesses 本リポジトリ内で扱わない追加の同期処理
   * @param {integer|null} lastSyncedAt 差分同期をする場合、最終更新日時(unixtime)を指定する
   * @return {Promise.<Array.<SyncDataResult>>}
   */
  static async syncRemoteToLocal (syncDataTypes, notifyProgress, additionalProcesses, lastSyncedAt = null) {
    return this._implement.syncRemoteToLocal(syncDataTypes, notifyProgress, additionalProcesses, lastSyncedAt)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.SAMPLE:
        this._implement = new StandardSyncDataRepository()
        break

      default:
        this._implement = new StandardSyncDataRepository()
        break
    }
  }
}
