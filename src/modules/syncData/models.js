import { INCENTIVE_TYPE } from '../promotion/models'
import { isGiftChangeOrSurplusPaymentMethodType, OTHER_CARD, PAYMENT_METHOD_TYPE } from '../payment/models'
import I18n from 'i18n-js'

export const MODULE_NAME = 'syncData'

export const SYNC_DATA_TYPE = {
  /** 商品系 */
  PRODUCTS: 1,

  /** マスター系 */
  MASTER: 2
}

const getProductTagIdFromProductTagIncentives = (productTagIncentives) => {
  return productTagIncentives[0] ? productTagIncentives[0].product_tag_id : null
}

const toBundlePattern = (incentive) => {
  const productTagId = getProductTagIdFromProductTagIncentives(incentive.product_tag_incentives)
  return {
    id: incentive.id,
    bundle_id: incentive.promotion_id,
    incentive_type: INCENTIVE_TYPE.FIXED_RATE_DISCOUNT,
    bundle_price: incentive.incentive_type === INCENTIVE_TYPE.FIXED_FLAT_RATE ? incentive.amount : 0,
    discount_rate: incentive.incentive_type === INCENTIVE_TYPE.FIXED_RATE_DISCOUNT ? incentive.amount : 0,
    product_tag_id: productTagId,
    establishment_condition: incentive.establishment_condition,
    threshold_quantity: incentive.threshold_min,
    created_at: incentive.created_at,
    updated_at: incentive.updated_at
  }
}

export const toBundleEntity = (promotionDto) => {
  return {
    ...promotionDto,
    code: promotionDto.promotion_code,
    bundle_riichi_message: promotionDto.bundle_riichi_message,
    bundle_patterns: promotionDto.incentives.map(toBundlePattern)
  }
}

export const toSalesEntity = (promotionDto) => {
  const incentive = promotionDto.incentives[0]
  const productTagId = getProductTagIdFromProductTagIncentives(incentive.product_tag_incentives)

  return {
    ...promotionDto,
    code: promotionDto.promotion_code,
    product_tag_id: productTagId,
    discount_rate: incentive.incentive_type === INCENTIVE_TYPE.FIXED_RATE_DISCOUNT
      ? incentive.amount : 0,
    discount_price: incentive.incentive_type === INCENTIVE_TYPE.FIXED_AMOUNT_DISCOUNT
      ? incentive.amount : 0
  }
}

export const toCouponEntity = (promotionDto) => {
  const incentive = promotionDto.incentives[0]
  const productTagId = getProductTagIdFromProductTagIncentives(incentive.product_tag_incentives)

  return {
    ...promotionDto,
    code: promotionDto.promotion_code,
    product_tag_id: productTagId,
    discount_rate: incentive.incentive_type === INCENTIVE_TYPE.FIXED_RATE_DISCOUNT
      ? incentive.amount : 0,
    discount_price: incentive.incentive_type === INCENTIVE_TYPE.FIXED_AMOUNT_DISCOUNT
      ? incentive.amount : 0,
    barcode: promotionDto.barcode,
    threshold_type: incentive.threshold_type,
    threshold_min: incentive.threshold_min,
    threshold_max: incentive.threshold_max
  }
}

export function isOnlineCreditPaymentMethod (paymentMethod) {
  return paymentMethod.payment_method_type === PAYMENT_METHOD_TYPE.CREDIT && !paymentMethod.is_offline
}

/**
 * サーバーから取得した支払い方法マスタを、POS用に適切に加工した形で返す
 * @param {Array.<Object>} remotePaymentMethods
 * @return {Array.<Object>}
 */
export const fixPaymentMethodsFromRemoteToLocal = (remotePaymentMethods) => {
  const localPaymentMethods = remotePaymentMethods.filter(paymentMethod => {
    // 券釣り、券差額はPOS支払い金種に使用しないため除外
    if (isGiftChangeOrSurplusPaymentMethodType(paymentMethod.payment_method_type)) return false

    // shop_idがnullのものは店舗に紐づけられていないので除外
    return paymentMethod.shop_id !== null
  })

  return localPaymentMethods
}
