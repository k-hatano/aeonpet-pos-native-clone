import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const resetState = createAction(`${MODULE_NAME}_resetState`)
export const clearSyncResults = createAction(`${MODULE_NAME}_clearSyncResults`)
export const appendSyncResults = createAction(`${MODULE_NAME}_appendSyncResults`)
export const setSyncOptionProducts = createAction(`${MODULE_NAME}_setSyncOptionProducts`)
export const setSyncOptionMasters = createAction(`${MODULE_NAME}_setSyncOptionMasters`)
export const beginSync = createAction(`${MODULE_NAME}_beginSync`)
export const completeSync = createAction(`${MODULE_NAME}_completeSync`)
export const abortSync = createAction(`${MODULE_NAME}_abortSync`)
