import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const Countries = sequelize.define(
  'Countries',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    country_code: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'countries',
    underscored: true,
    timestamps: false
  }
)

export default Countries
