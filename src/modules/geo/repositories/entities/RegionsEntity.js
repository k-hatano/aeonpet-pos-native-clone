import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const Regions = sequelize.define(
  'Regions',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    country_code: Sequelize.STRING,
    region_code: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'regions',
    underscored: true,
    timestamps: false
  }
)

export default Regions
