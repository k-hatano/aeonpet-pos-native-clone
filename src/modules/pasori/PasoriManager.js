import Pasori from './Pasori'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'

class PasoriManager extends StoreAccessibleBase {

  startInitialize () {
    return new Promise((resolve, reject) => {
      Pasori.startInitialize(() => {
        resolve()
      }, () => {
        reject()
      })
    })
  }

  close () {
    Pasori.close()
  }

  start () {
    return new Promise((resolve, reject) => {
      Pasori.start((data) => {
        resolve(data)
      }, (error) => {
        reject(error)
      })
    })
  }
}

const pasoriManager = new PasoriManager()
export default pasoriManager
