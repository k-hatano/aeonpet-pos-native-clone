import { NativeModules } from 'react-native'

class Pasori {
  constructor () {
    this.pasoriBridge = NativeModules.PasoriBridge
  }

  startInitialize (successCompletion, onError) {
    this.pasoriBridge.startInitialize(
      (bridgeError, result) => {
        if (result !== 1) {
          onError()
        } else {
          successCompletion()
        }
      }
    )
  }

  close () {
    this.pasoriBridge.closePasori()
  }

  start (successCompletion, onError) {
    this.pasoriBridge.start(
      (bridgeError, result) => {
        const error = result['error']
        if (error !== '') {
          onError(error)
        } else {
          successCompletion(result)
        }
      }
    )
  }
}

export default new Pasori()
