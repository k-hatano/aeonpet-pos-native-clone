import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  goToTopButton: {
    height: 46,
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  list: {
    backgroundColor: '#f7f7f7'
  },
  listItem: {
    height: 65,
    backgroundColor: '#f7f7f7',
    justifyContent: 'center'
  },
  categoryName: {
    fontSize: 18,
    color: baseStyleValues.mainTextColor,
    marginLeft: 17
  },
  listItemSelected: {
    height: 65,
    backgroundColor: '#d8d8d8'
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8',
    marginHorizontal: 17
  }
})
