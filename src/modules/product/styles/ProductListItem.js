import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
    backgroundColor: '#f7f7f7',
    borderColor: '#979797',
    borderWidth: 1
  },
  headerArea: {
    marginLeft: 5,
    marginRight: 1,
    marginTop: 3,
    marginBottom: 6,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  mainArea: {
    height: 30,
    marginLeft: 5,
    marginRight: 1,
    justifyContent: 'center'
  },
  subArea: {
    marginLeft: 5,
    marginRight: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24
  },
  productNameText: {
    height: 25,
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  priceText: {
    height: 24,
    color: baseStyleValues.mainTextColor,
    fontSize: 20
  },
  outOfStockText: {
    color: 'red',
    fontSize: 14
  },
  subText: {
    color: baseStyleValues.subTextColor,
    fontSize: 14,
    height: 15
  },
  stockSubText: {
    color: baseStyleValues.subTextColor,
    fontSize: 16
  },
  footer: {
    height: 11
  },
  productSubInfoArea: {
    justifyContent: 'space-around'
  }
})
