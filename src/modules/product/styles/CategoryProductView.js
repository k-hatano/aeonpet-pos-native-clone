import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  col: {
    flexDirection: 'column'
  },
  row: {
    flexDirection: 'row',
    flex: 1
  },
  searchAreaContainer: {
    flexDirection: 'row',
    marginLeft: 10
  },
  productArea: {
    flex: 9,
    paddingHorizontal: 20
  },
  categoryArea: {
    flex: 5
  },
  categoryListContainer: {
    flex: 1
  },
  productListContainer: {
  },
  searchTextInput: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#000',
    letterSpacing: -0.38,
    textAlign: 'left',
    width: 344,
    height: 38,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797'
  },
  iconImageSearchButton: {
    backgroundColor: '#ff9024',
    borderWidth: 3,
    borderColor: '#ff9024',
    borderRadius: 10,
    width: 66,
    height: 40,
    marginLeft: 10
  },
  searchIcon: {
    width: 23,
    height: 23
  },
  changeStockCondButtonContainer: {
    width: '100%'
  },
  changeStockCondButton: {
    width: 280,
    height: 50,
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10
  },
  categoryLink: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    textAlign: 'left',
    paddingVertical: 8,
    marginLeft: 20
  },
  selectArea: {
    width: '25%'
  },
  modalSelectorButton: {
    width: 150,
    height: 36,
    fontSize: 18,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  conditionSwitchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10
  },
  conditionSwitchLabel: {
    fontSize: 20,
    marginTop: 10
  },
  conditionSwitchToggle: {
    marginTop: 5
  },
  refineModalContainer: {
    width: 140,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  }
})
