import React, { Component } from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/ProductList'
import ProductListItem from './ProductListItem'
import CustomPropTypes from '../../../common/components/CustomPropTypes'

export default class ProductList extends Component {
  static propTypes = {
    products: PropTypes.array,
    currency: PropTypes.string,
    style: CustomPropTypes.Style,
    onSelect: PropTypes.func
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    return (
      <View style={this.props.style}>
        <ListView
          enableEmptySections
          style={styles.list}
          dataSource={dataSource.cloneWithRows(this.props.products)}
          renderRow={(product) => <ProductListItem
            onSelect={this.props.onSelect}
            product={product}
            currency={this.props.currency} />}
          renderSeparator={(sectionID, rowID) => <View key={rowID} style={styles.separator} />} />
      </View>
    )
  }
}
