import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput, Switch } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CategoryProductView'
import ProductList from './ProductList'
import CategoryList from './CategoryList'
import CustomPropTypes from '../../../common/components/CustomPropTypes'
import SimpleImageButton from '../../../common/components/elements/SimpleImageButton'
import Cart from '../../../modules/cart/models/Cart'
import { PREV_SEARCH_COND } from '../models'
import ProgressBar from 'common/components/widgets/ProgressBar'

export default class CategoryProductView extends Component {
  static propTypes = {
    style: CustomPropTypes.Style,
    products: PropTypes.array,
    originProducts: PropTypes.array,
    categories: PropTypes.array,
    selectedCategoryPath: PropTypes.array,
    onCategorySelected: PropTypes.func,
    onProductSelected: PropTypes.func,
    onChangeSearchWord: PropTypes.func,
    onSearch: PropTypes.func,
    onShowAmountPad: PropTypes.func,
    searchWord: PropTypes.string,
    cart: PropTypes.instanceOf(Cart),
    stockConditionFlg: PropTypes.bool,
    showStockConditionFlg: PropTypes.bool,
    excludeSetProduct: PropTypes.bool,
    searchLayoutFlg: PropTypes.bool,
    prevCategory: PropTypes.object,
    searchCategoryId: PropTypes.string,
    prevSearchCond: PropTypes.number,
    repository: PropTypes.object,
    searchBtnDisabled: PropTypes.bool,
    onProgress: PropTypes.func
  }
  static defaultProps = {
    showStockConditionFlg: true,
    excludeSetProduct: false
  }

  progress = null

  _switchStockCondittion () {
    const {
      cart,
      stockConditionFlg,
      showStockConditionFlg,
      excludeSetProduct, searchWord, prevCategory, searchCategoryId, prevSearchCond,
      onShowAmountPad
    } = this.props

    switch (prevSearchCond) {
      case PREV_SEARCH_COND.SEARCH_BY_WORD:
        this.props.onSearch(
          cart,
          searchWord,
          !stockConditionFlg,
          showStockConditionFlg,
          excludeSetProduct,
          onShowAmountPad, false, true
        )
        break

      case PREV_SEARCH_COND.SEARCH_BY_CATEGORY:
        this.props.onCategorySelected(prevCategory, !stockConditionFlg, showStockConditionFlg, excludeSetProduct)
        break

      case PREV_SEARCH_COND.SEARCH_BY_PREV_CATEGORY:
        this.props.onPrevCategorySelected(
          searchCategoryId, !stockConditionFlg, showStockConditionFlg, excludeSetProduct
        )
        break

      default:
        break
    }
  }

  async search () {
    const {
      onShowAmountPad, searchWord, cart, stockConditionFlg, showStockConditionFlg, excludeSetProduct
    } = this.props

    this.props.onProgress(true, this.progress)
    await this.props.onSearch(
      cart,
      searchWord,
      stockConditionFlg,
      showStockConditionFlg,
      excludeSetProduct,
      onShowAmountPad
    )
    this.props.onProgress(false, this.progress)
  }

  /**
   * 在庫なし商品表示フラグを描画します。
   * @param stockConditionFlg
   * @param showStockConditionFlg
   * @returns {*}
   * @private
   */
  _renderStockConditionFlg (stockConditionFlg, showStockConditionFlg) {
    if (!showStockConditionFlg) return null
    return (
      <View style={styles.conditionSwitchContainer}>
        <Text style={styles.conditionSwitchLabel}>{I18n.t('product.stock_condition')}</Text>
        <Switch
          style={styles.conditionSwitchToggle}
          onValueChange={() => {
            this.props.onChangeStockConditionFlg(stockConditionFlg)
            this._switchStockCondittion()
          }}
          value={stockConditionFlg} />
      </View>
    )
  }
  render () {
    const {
      searchWord,
      stockConditionFlg,
      showStockConditionFlg,
      excludeSetProduct,
      searchLayoutFlg,
      searchBtnDisabled
    } = this.props
    const LinkText = searchLayoutFlg ? I18n.t('product.category_link') : ''

    return (
      <View style={[styles.col, this.props.style]}>
        <ProgressBar.Component
          ref={progress => {
            this.progress = progress
          }}
        />
        <View style={styles.col}>
          <View style={styles.searchAreaContainer}>
            <TextInput
              style={styles.searchTextInput}
              value={searchWord}
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={(word) => this.props.onChangeSearchWord(word)}
              placeholder={I18n.t('order.product_search_holder')} />
            <SimpleImageButton
              style={styles.iconImageSearchButton}
              ImageStyle={styles.SearchIcon}
              disabled={searchBtnDisabled}
              source={require('../../../assets/images/searchButton.png')}
              onPress={() => this.search()} />
            {this._renderStockConditionFlg(stockConditionFlg, showStockConditionFlg)}
          </View>
          <TouchableOpacity
            style={{}}
            onPress={this.props.onPressCategoryLink} >
            <Text style={styles.categoryLink}> {LinkText}</Text>
          </TouchableOpacity>
        </View>
        {!searchLayoutFlg &&
          <View style={styles.row}>
            <View style={styles.productArea}>
              <ProductList
                enableEmptySections
                products={this.props.products}
                onSelect={this.props.onProductSelected}
                currency={'jpy'}
                style={styles.productListContainer} />
            </View>
            <View style={styles.categoryArea}>
              <CategoryList
                enableEmptySections
                categories={this.props.categories}
                selectedCategoryPath={this.props.selectedCategoryPath}
                onSelect={(id, flg) => this.props.onCategorySelected(id, flg, showStockConditionFlg, excludeSetProduct)}
                prevCategory={this.props.prevCategory}
                onPrevSelect={(id, flg) => this.props.onPrevCategorySelected(id, flg, showStockConditionFlg)}
                stockConditionFlg={stockConditionFlg}
                style={styles.categoryListContainer} />
            </View>
          </View>
        }
        {searchLayoutFlg &&
          <View style={styles.productArea}>
            <View>
              <ProductList
                enableEmptySections
                products={this.props.products}
                onSelect={this.props.onProductSelected}
                currency={'jpy'} />
            </View>
          </View>
        }
      </View>
    )
  }
}
