import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CategoryList'
import CustomPropTypes from 'common/components/CustomPropTypes'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'

class CategoryListItem extends Component {
  static propTypes = {
    category: PropTypes.object,
    onSelect: PropTypes.func
  }

  render () {
    const categoryName = this.props.category.name.length > 22 ? this.props.category.name.substr(0,21)+'…' : this.props.category.name
    const dispCategoryName = categoryName.length > 11 ? categoryName.substr(0,11) + '\n' + categoryName.substr(11) : categoryName
    const {category, stockConditionFlg} = this.props
    return (
      <TouchableOpacitySfx onPress={() => this.props.onSelect(category, stockConditionFlg)}>
        <View style={styles.listItem} >
          <Text style={styles.categoryName}>{dispCategoryName}</Text>
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export default class CategoryList extends Component {
  static propTypes = {
    onSelect: PropTypes.func,
    categories: PropTypes.array,
    style: CustomPropTypes.Style
  }

  render () {
    const dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    const {prevCategory, stockConditionFlg} = this.props
    const prevCategoryName = Object.keys(prevCategory).length > 0 ? '＜＜ ' + prevCategory.name : ''
    const prevCategoryParentId = (Object.keys(prevCategory).length > 0 && prevCategory.parent_id) ? prevCategory.parent_id : null

    return (
      <View style={this.props.style}>
        <TouchableOpacitySfx onPress={() => this.props.onSelect(null)}>
          <View style={styles.goToTopButton}>
            <Text style={styles.buttonText} >
              {I18n.t('cart.category_top')}
            </Text>
          </View>
        </TouchableOpacitySfx>
        { prevCategoryName.length > 0 &&
          <TouchableOpacitySfx onPress={() => this.props.onPrevSelect(prevCategoryParentId, stockConditionFlg)}>
            <View style={styles.goToTopButton}>
              <Text style={styles.buttonText} >
                {prevCategoryName}
              </Text>
            </View>
          </TouchableOpacitySfx>
        }
        <ListView
          enableEmptySections
          style={styles.list}
          dataSource={dataSource.cloneWithRows(this.props.categories)}
          renderRow={category => <CategoryListItem
            key={category.id}
            category={category}
            onSelect={this.props.onSelect}
            stockConditionFlg={stockConditionFlg} />}
          renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
        />
      </View>
    )
  }
}
