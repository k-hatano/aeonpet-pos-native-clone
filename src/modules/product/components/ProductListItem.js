import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/ProductListItem'
import { formatPriceWithCurrency, formatUnit } from '../../../common/utils/formats'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import { STOCK_MODE } from '../models'

class StockCountView extends Component {
  static propTypes = {
    quantity: PropTypes.number,
    stockMode: PropTypes.number
  }

  render () {
    if (this.props.stockMode === STOCK_MODE.UNUSED) {
      return (
        <Text style={styles.stockSubText}>無制限</Text>
      )
    }

    if (this.props.quantity <= 0) {
      return (
        <Text style={styles.outOfStockText}>在庫なし</Text>
      )
    } else {
      return (
        <Text style={styles.stockSubText}>在庫 : {formatUnit(this.props.quantity, '点')}</Text>
      )
    }
  }
}

export default class ProductListItem extends Component {
  static propTypes = {
    product: PropTypes.object,
    currency: PropTypes.string,
    onSelect: PropTypes.func
  }

  render () {
    const { product, currency } = this.props
    const productVariant = product.product_variants[0]
    const stockItem = productVariant.stock_item
    const quantity = stockItem ? stockItem.quantity : 0

    return (
      <TouchableOpacitySfx style={styles.root} onPress={() => this.props.onSelect(product)}>
        <View style={styles.headerArea}>
          <Text style={styles.subText}>{product.product_code}</Text>
        </View>
        <View style={styles.mainArea}>
          <Text numberOfLines={1} style={styles.productNameText}>
            {product.name}
          </Text>
        </View>
        <View style={styles.subArea}>
          <View style={styles.productSubInfoArea}>
            <StockCountView quantity={quantity} stockMode={productVariant.stock_mode} />
          </View>
          <View style={{justifyContent: 'flex-end'}}>
            <Text style={styles.priceText}>
              {I18n.t('cart.unit_price')} : {formatPriceWithCurrency(productVariant.price, currency)}
            </Text>
          </View>
          <View style={styles.operationArea} />
        </View>
        <View style={styles.footer} />
      </TouchableOpacitySfx>
    )
  }
}
