import { handleActions } from 'redux-actions'
import {
  listCategories,
  listProducts,
  listOriginProducts,
  setSearchWord,
  setStockConditionFlg,
  setSearchLayoutFlg,
  setClosureData,
  setVintageData,
  setLabelData,
  setBoxData,
  setClosureCond,
  setVintageCond,
  setLabelCond,
  setBoxCond,
  setPrevCategory,
  setSearchCategoryId,
  setPrevSearchCond,
  setSearchBtnDisabled
} from './actions'

const defaultState = {
  categories: [],
  products: [],
  originProducts: [],
  searchWord: '',
  stockConditionFlg: true,
  searchLayoutFlg: false,
  closureData: [],
  vintageData: [],
  labelData: [],
  boxData: [],
  closureCond: '',
  vintageCond: '',
  labelCond: '',
  boxCond: '',
  prevCategory: {},
  searchCategoryId: '',
  prevSearchCond: 0,
  searchBtnDisabled: true
}

const handlers = {
  [listCategories]: (state, action) => ({
    ...state,
    ...{ categories: action.payload }
  }),
  [listProducts]: (state, action) => ({
    ...state,
    ...{ products: action.payload }
  }),
  [listOriginProducts]: (state, action) => ({
    ...state,
    ...{ originProducts: action.payload }
  }),
  [setSearchWord]: (state, action) => ({
    ...state,
    ...{ searchWord: action.payload }
  }),
  [setStockConditionFlg]: (state, action) => ({
    ...state,
    ...{stockConditionFlg: action.payload}
  }),
  [setSearchLayoutFlg]: (state, action) => ({
    ...state,
    ...{searchLayoutFlg: action.payload}
  }),
  [setClosureData]: (state, action) => ({
    ...state,
    ...{closureData: action.payload}
  }),
  [setVintageData]: (state, action) => ({
    ...state,
    ...{vintageData: action.payload}
  }),
  [setLabelData]: (state, action) => ({
    ...state,
    ...{labelData: action.payload}
  }),
  [setBoxData]: (state, action) => ({
    ...state,
    ...{boxData: action.payload}
  }),
  [setClosureCond]: (state, action) => ({
    ...state,
    ...{closureCond: action.payload}
  }),
  [setVintageCond]: (state, action) => ({
    ...state,
    ...{vintageCond: action.payload}
  }),
  [setLabelCond]: (state, action) => ({
    ...state,
    ...{labelCond: action.payload}
  }),
  [setBoxCond]: (state, action) => ({
    ...state,
    ...{boxCond: action.payload}
  }),
  [setPrevCategory]: (state, action) => ({
    ...state,
    ...{prevCategory: action.payload}
  }),
  [setSearchCategoryId]: (state, action) => ({
    ...state,
    ...{searchCategoryId: action.payload}
  }),
  [setPrevSearchCond]: (state, action) => ({
    ...state,
    ...{prevSearchCond: action.payload}
  }),
  [setSearchBtnDisabled]: (state, action) => ({
    ...state,
    searchBtnDisabled: action.payload
  })
}
export default handleActions(handlers, defaultState)
