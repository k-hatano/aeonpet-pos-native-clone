export const MODULE_NAME = 'product'

export const PREV_SEARCH_COND = {
  EMPTY: 0,
  SEARCH_BY_WORD: 1,
  SEARCH_BY_CATEGORY: 2,
  SEARCH_BY_PREV_CATEGORY: 3
}

export const STOCK_MODE = {
  USE: 1,
  UNUSED: 0
}

export const CHANGE_PRICE_TYPE = {
  UNREQUIRED: 1,
  REQUIRED: 2
}

export const LIQUOR_TAX_TYPE1_BEER = '1004'
export const LIQUOR_TAX_TYPE2_BEER = '2041'

export const BOOKS_BARCODE = {
  FIRST_PREFIX: ['978', '979'],
  SECOND_PREFIX: '192'
}

export const PROMOTION_BARCODE_PREFIX = '98'

export const isBeer = (liquorTaxType1, liquorTaxType2) => {
  return liquorTaxType1 === LIQUOR_TAX_TYPE1_BEER && liquorTaxType2 === LIQUOR_TAX_TYPE2_BEER
}

export const isBooks = barcode => {
  return isFirstStepBarcodeInBooks(barcode) || isSecondStepBarcodeInBooks(barcode)
}

export const isFirstStepBarcodeInBooks = (barcode = '') => {
  return BOOKS_BARCODE.FIRST_PREFIX.includes(barcode.substr(0, 3))
}

export const isSecondStepBarcodeInBooks = (barcode = '') => {
  return barcode.substr(0, 3) === BOOKS_BARCODE.SECOND_PREFIX
}

export const isPromotionBarcode = (barcode = '') => {
  return barcode.substr(0, 2) === PROMOTION_BARCODE_PREFIX
}
