import generateUuid from '../../common/utils/generateUuid'
import { TAX_RULE, TAXFREE_TYPE } from '../../common/models/Tax'
import { sampleCategoryMap } from './repositories/sample/SampleCategoryRepository'
import * as _ from 'underscore'

const defaultTax = {
  tax_rule: TAX_RULE.EXCLUDED_FLOOR,
  tax_rate: 0.08
}

const defaultProductProperties = {
  status: 1,
  shipping_mode: 1,
  shop_mode: 0,
  is_parent_product: 0,
  is_discountable: 1,
  is_sales_excluded: 1,
  ...defaultTax,
  taxfree_type: 1,
  special_tax_type: 0,
  temperature_zone: 1,
  purchase_limit: 1,
  is_share: 1,
  slug: 'http://testtest.com',
  short_description: '一覧表示です。',
  description: '詳細表示です。',
  stock_out_message: '在庫切れです。'
}

const defaultProductVariantProperties = {
  currency: 'jpy',
  stock_mode: 1,
  weight: 10,
  closure_type_id: null,
  closure_type_name: null,
  closure_type_code: null,
  vintage: null,
  label_version: null,
  box_version: null,
  best_by_date: null,
  capacity: null,
  liquor_tax_type1: null,
  liquor_tax_type2: null,
  quantity_per_carton: null,
  can_grant_points: null
}

const generateRandomCode = () => {
  return ('000000' + Math.floor((Math.random() * 10000000)).toString()).slice(-7)
}

const createProductSample = (name, articleNumber, categoryIds, variants, otherProperties) => {
  const id = generateUuid()
  return {
    ...defaultProductProperties,
    id: id,
    name: name,
    product_name_sys21: name + '-基幹用名',
    status: 1,
    product_code: articleNumber,
    article_number: articleNumber,
    short_name: 'S' + name,
    product_categories: categoryIds,
    product_variants: variants.map(variant => {
      return { product_id: id, ...variant }
    }),
    ...otherProperties
  }
}
const createProductVariantSample = (
  articleNumber,
  price,
  quantity,
  closure,
  vintage,
  labelVersion,
  boxVersion,
  otherProperties = {}) => {
  const id = generateUuid()
  return {
    ...defaultProductVariantProperties,
    id: id,
    list_price: price,
    cost_price: price,
    price: price,
    maker_code: generateRandomCode(),
    sku: articleNumber,
    article_number: articleNumber,
    own_company_code: generateRandomCode(),
    warehouse_code: generateRandomCode(),
    closure_type_name: closure,
    vintage: vintage,
    label_version: labelVersion,
    box_version: boxVersion,
    stock_item: {
      id: generateUuid(),
      product_variant_id: id,
      quantity: quantity,
      product_id: 'dummy',
      warehouse_id: 'dummy',
      warehouse_code: 'dummy'
    },
    ...otherProperties
  }
}

const productsForSet = {
  setA_child1: createProductSample(
    'セットA-内容１',
    '00000000005',
    [],
    [createProductVariantSample('00000001005', 1000, 100, 'クロージャA', 'ヴィンテージA', 'ラベルA', '箱A')]
  ),
  setA_child2: createProductSample(
    'セットA-内容２',
    '00000000006',
    [],
    [createProductVariantSample('00000001006', 1000, 100, 'クロージャA', 'ヴィンテージA', 'ラベルA', '箱A')]
  ),
  setA_child3: createProductSample(
    'セットA-内容３',
    '00000000007',
    [],
    [createProductVariantSample('00000001007', 1000, 100, 'クロージャA', 'ヴィンテージA', 'ラベルA', '箱A')]
  )
}

export const sampleProductMap = {
  ...productsForSet,
  standardA: createProductSample(
    '基本商品A',
    '00000000001',
    [sampleCategoryMap.standard.id],
    [createProductVariantSample('00000001001', 1000, 100, 'クロージャA', 'ヴィンテージA', 'ラベルA', '箱A')]
  ),
  standardB: createProductSample(
    '基本商品B',
    '00000000002',
    [sampleCategoryMap.standard.id],
    [createProductVariantSample('00000001002', 1500, 100, '全全全全全', '全全全全全', '全全全全全', '全全全全全')]
  ),
  standardC: createProductSample(
    '基本商品C',
    '00000000003',
    [sampleCategoryMap.standard.id],
    [createProductVariantSample('00000001003', 2000, 100, 'ABCDEABCDE', '1234567890', 'ABCDEFGHIJ', 'ABCDEABCDE')]
  ),
  setA: createProductSample(
    'セット商品A',
    '00000000004',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('00000001004', 2000, 100, 'クロージャA', 'ヴィンテージC', 'ラベルA', '箱D')],
    {
      is_parent_product: 1,
      _sets: [
        { product_variant_id: productsForSet.setA_child1.product_variants[0].id, quantity: 1 },
        { product_variant_id: productsForSet.setA_child2.product_variants[0].id, quantity: 2 },
        { product_variant_id: productsForSet.setA_child3.product_variants[0].id, quantity: 3 }
      ]
    }
  ),
  notDiscountable: createProductSample(
    '割引不可',
    '01000000002',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('01000001002', 1000, 100, 'クロージャA', 'ヴィンテージC', 'ラベルA', '箱D')],
    {
      is_discountable: 0
    }
  ),
  notTaxable: createProductSample(
    '課税対象外',
    '01000000003',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('01000001003', 1000, 100, 'クロージャA', 'ヴィンテージC', 'ラベルA', '箱D')],
    {
      tax_rule: TAX_RULE.NONE,
      tax_rate: 0.0000
    }
  ),
  longName: createProductSample(
    'とっても長い商品名のテスト用のテスト商品ですよろしくお願いいたします',
    '01000000004',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('01000001004', 1500, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱B')]
  ),
  zero1: createProductSample(
    'ゼロ円商品1',
    '01000000006',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('01000001006', 0, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱B')]
  ),
  zero2: createProductSample(
    'ゼロ円商品2',
    '01000000007',
    [sampleCategoryMap.various.id],
    [createProductVariantSample('01000001007', 0, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱B')]
  ),
  bundle_s_1_1: createProductSample(
    'よりどり1対象1',
    '00001000001',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001001', 1000, 100, 'クロージャB', 'ヴィンテージB', 'ラベルB', '箱B')]
  ),
  bundle_s_1_2: createProductSample(
    'よりどり1対象2',
    '00001000002',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001002', 1200, 100, 'クロージャB', 'ヴィンテージA', 'ラベルA', '箱A')]
  ),
  bundle_s_1_3: createProductSample(
    'よりどり1対象3',
    '00001000092',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001092', 1200, 100, 'クロージャB', 'ヴィンテージA', 'ラベルA', '箱A')]
  ),
  bundle_s_2_A_1: createProductSample(
    'よりどり2 A対象1',
    '00001000011',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001011', 2000, 100, 'クロージャB', 'ヴィンテージC', 'ラベルC', '箱D')]
  ),
  bundle_s_2_B_1: createProductSample(
    'よりどり2 B対象1',
    '00001000012',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001012', 3000, 100, 'クロージャB', 'ヴィンテージB', 'ラベルA', '箱D')]
  ),
  bundle_xy_1_1: createProductSample(
    'XY1対象1',
    '00001000201',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001201', 1000, 100)]
  ),
  bundle_xy_1_2: createProductSample(
    'XY1対象2',
    '00001000202',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('', 1200, 100, '', '', '', '', { sku: '00001000202' })],
    { article_number: '' }
  ),
  bundle_xy_1_3: createProductSample(
    'XY1対象3',
    '00001000203',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('', 1200, 100, '', '', '', '', { sku: '00001000203' })],
    { article_number: '' }
  ),
  bundle_xy_2_A_1: createProductSample(
    'XY2 A対象1',
    '00001000211',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001211', 2000, 100)]
  ),
  bundle_xy_2_B_1: createProductSample(
    'XY2 B対象1',
    '00001000212',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001212', 3000, 100)]
  ),
  bundle_flat_1_1: createProductSample(
    'FLATPRICE1対象1',
    '00001000301',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001301', 1200, 100)]
  ),
  bundle_flat_1_2: createProductSample(
    'FLATPRICE1対象2',
    '00001000302',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001302', 1500, 100)]
  ),
  bundle_s_1_xy_1: createProductSample(
    'よりどり&XY対象',
    '00001000302',
    [sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001302', 1500, 100)]
  ),
  sale_a_1: createProductSample(
    'セールA 対象商品1',
    '00001000401',
    [sampleCategoryMap.sale.id],
    [createProductVariantSample('00001001401', 1000, 100, '', 'ヴィンテージA', 'ラベルC', '箱C')]
  ),
  sale_a_2: createProductSample(
    'セールA 対象商品2',
    '00001000402',
    [sampleCategoryMap.sale.id],
    [createProductVariantSample('00001001402', 3000, 100, 'クロージャB', '', 'ラベルC', '箱A')]
  ),
  sale_b_1: createProductSample(
    'セールB 対象商品1',
    '00001000411',
    [sampleCategoryMap.sale.id],
    [createProductVariantSample('00001001411', 3000, 100, 'クロージャA', 'ヴィンテージC', '', '箱D')]
  ),
  sale_bundle: createProductSample(
    'セール&バンドル 対象商品',
    '00001000121',
    [sampleCategoryMap.sale.id, sampleCategoryMap.bundle.id],
    [createProductVariantSample('00001001121', 2000, 100, 'クロージャA', 'ヴィンテージB', 'ラベルA', '')]
  ),
  taxfreeExpentable1: createProductSample(
    '免税対象 消耗品1',
    '00002000001',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001001', 1000, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱A')],
    { taxfree_type: TAXFREE_TYPE.EXPENDABLE }
  ),
  taxfreeExpentable2: createProductSample(
    '免税対象 消耗品2',
    '00002000002',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001002', 2000, 100, 'クロージャA', 'ヴィンテージC', 'ラベルC', '箱A')],
    { taxfree_type: TAXFREE_TYPE.EXPENDABLE }
  ),
  taxfreeNotApplicable1: createProductSample(
    '免税対象 対象外1',
    '00002000011',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001011', 1000, 100, 'クロージャA', 'ヴィンテージA', 'ラベルA', '箱D')],
    { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE }
  ),
  taxfreeNotApplicable2: createProductSample(
    '免税対象 対象外2',
    '00002000012',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001012', 2000, 100, 'クロージャA', 'ヴィンテージB', 'ラベルC', '箱D')],
    { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE }
  ),
  taxfreeExpentableAndBundled: createProductSample(
    '免税対象 消耗品 & バンドル対象',
    '00002000021',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001021', 1300, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱A')],
    { taxfree_type: TAXFREE_TYPE.EXPENDABLE }
  ),
  taxfreeNotApplicableAndBundled: createProductSample(
    '免税対象 対象外 & バンドル対象',
    '00002000022',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001022', 1200, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱A')],
    { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE }
  ),
  bonusPoint: createProductSample(
    '単品ポイント2倍対象',
    '00002000022',
    [sampleCategoryMap.taxfree.id],
    [createProductVariantSample('00002001022', 1200, 100, 'クロージャA', 'ヴィンテージB', 'ラベルB', '箱A')],
    { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE }
  )
}

export const sampleProducts = _.values(sampleProductMap)
export const sampleProductSets = []

sampleProducts.forEach(product => {
  if (!product._sets) return
  product._sets.forEach(set => {
    sampleProductSets.push({
      product_id: product.id,
      product_variant_id: set.product_variant_id,
      quantity: set.quantity
    })
  })
  delete product._sets
})

export const sampleProductTagMap = {
  bundle_s_1: {
    id: generateUuid(),
    name: 'よりどり1対象',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundle1',
    products: [
      sampleProductMap.bundle_s_1_1.id,
      sampleProductMap.bundle_s_1_2.id,
      sampleProductMap.bundle_s_1_3.id,
      sampleProductMap.bundle_s_1_xy_1.id,
      sampleProductMap.sale_bundle.id,
      sampleProductMap.taxfreeExpentableAndBundled.id,
      sampleProductMap.taxfreeNotApplicableAndBundled.id
    ]
  },
  bundle_s_2_A: {
    id: generateUuid(),
    name: 'よりどり2対象A',
    sort_order: 2,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundle2A1',
    products: [
      sampleProductMap.bundle_s_2_A_1.id
    ]
  },
  bundle_s_2_B: {
    id: generateUuid(),
    name: 'よりどり2対象B',
    sort_order: 2,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundle2B1',
    products: [
      sampleProductMap.bundle_s_2_B_1.id
    ]
  },
  bundle_xy_1: {
    id: generateUuid(),
    name: 'BUY_X_GET_Y1対象',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundleXY1',
    products: [
      sampleProductMap.bundle_xy_1_1.id,
      sampleProductMap.bundle_xy_1_2.id,
      sampleProductMap.bundle_xy_1_3.id,
      sampleProductMap.bundle_s_1_xy_1.id
    ]
  },
  bundle_xy_2: {
    id: generateUuid(),
    name: 'BUY_X_GET_Y2対象',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundleXY2',
    products: [
      sampleProductMap.bundle_xy_2_A_1.id,
      sampleProductMap.bundle_xy_2_B_1.id
    ]
  },
  bundle_flat_1: {
    id: generateUuid(),
    name: 'FLATPRICE1対象',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundleF1',
    products: [
      sampleProductMap.bundle_flat_1_1.id,
      sampleProductMap.bundle_flat_1_2.id
    ]
  },
  sale_a: {
    id: generateUuid(),
    name: 'セールA',
    sort_order: 4,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeBundle2B1',
    products: [
      sampleProductMap.sale_a_1.id,
      sampleProductMap.sale_a_2.id,
      sampleProductMap.sale_bundle.id
    ]
  },
  sale_b: {
    id: generateUuid(),
    name: 'セールB',
    sort_order: 4,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeSaleA',
    products: [
      sampleProductMap.sale_b_1.id
    ]
  },
  coupon_a: {
    id: generateUuid(),
    name: 'クーポンA',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeCouponA',
    products: [
      sampleProductMap.standardA.id
    ]
  },
  coupon_b: {
    id: generateUuid(),
    name: 'クーポンB',
    sort_order: 1,
    product_tag_type: 1,
    product_tag_code: 'GroupCodeCouponB',
    products: [
      sampleProductMap.standardB.id
    ]
  }
}

export const sampleProductTags = _.values(sampleProductTagMap)

export const sampleProductProductTags = _.flatten(sampleProductTags.map(tag => {
  return tag.products.map(productId => {
    return {
      product_id: productId,
      product_tag_id: tag.id
    }
  })
}))
