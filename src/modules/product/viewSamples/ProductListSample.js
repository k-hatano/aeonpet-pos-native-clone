import ProductList from '../components/ProductList'
import { NormalProduct, LongNameProduct, OutOfStockProduct } from './ProductListItemSample'

export default {
  name: 'ProductList',
  component: ProductList,
  properties: [
    {
      title: 'Default',
      property: {
        products: [
          NormalProduct,
          LongNameProduct,
          OutOfStockProduct
        ],
        onSelect: (product) => console.log('OnSelectProduct', product)
      }
    }
  ],
  frames: [
    {
      title: '438 x 800',
      style: {width: 438, height: 800}
    }
  ]
}
