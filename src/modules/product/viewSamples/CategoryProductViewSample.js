import CategoryProductView from '../components/CategoryProductView'
import { NormalProduct, LongNameProduct, OutOfStockProduct } from './ProductListItemSample'

export default {
  name: 'CategoryProductView',
  component: CategoryProductView,
  properties: [
    {
      title: 'Default',
      property: {
        products: [
          NormalProduct,
          LongNameProduct,
          OutOfStockProduct
        ],
        selectedCategoryPath: [
          {
            name: 'カテゴリー1'
          }
        ],
        categories: [
          {
            name: 'カテゴリー1-A'
          },
          {
            name: 'カテゴリー1-B'
          },
          {
            name: 'カテゴリー1-C'
          }
        ],
        onProductSelected: (product) => console.log('OnSelectProduct', product)
      }
    }
  ],
  frames: [
    {
      title: '683 x 953',
      style: {width: 683, height: 953}
    }
  ]
}
