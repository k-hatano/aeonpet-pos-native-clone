import CategoryList from '../components/CategoryList'

export default {
  name: 'CategoryList',
  component: CategoryList,
  properties: [
    {
      title: 'Default',
      property: {
        selectedCategoryPath: [
        ],
        categories: [
          {
            name: 'カテゴリー1'
          },
          {
            name: 'カテゴリー2'
          },
          {
            name: 'カテゴリー3'
          },
          {
            name: 'カテゴリー4'
          },
          {
            name: 'カテゴリー5'
          },
          {
            name: 'カテゴリー6'
          },
          {
            name: 'カテゴリー7'
          },
          {
            name: 'カテゴリー8'
          }
        ]
      }
    },
    {
      title: 'Layer2',
      property: {
        selectedCategoryPath: [
          {
            name: 'カテゴリー1'
          }
        ],
        categories: [
          {
            name: 'カテゴリー1-A'
          },
          {
            name: 'カテゴリー1-B'
          },
          {
            name: 'カテゴリー1-C'
          }
        ]
      }
    },
    {
      title: 'Deep Layer',
      property: {
        selectedCategoryPath: [
          {
            name: 'カテゴリー1'
          },
          {
            name: 'カテゴリー1-B'
          },
          {
            name: 'カテゴリー1-B-い'
          }
        ],
        categories: [
          {
            name: 'カテゴリー1-B-い-壱'
          },
          {
            name: 'カテゴリー1-B-い-弐'
          },
          {
            name: 'カテゴリー1-B-い-参'
          }
        ]
      }
    },
  ],
  frames: [
    {
      title: '218 x 400',
      style: {width: 218, height: 400}
    }
  ]
}
