import ProductListItem from '../components/ProductListItem'

export const NormalProduct = {
  name: '商品名1',
  product_code: 'product-code1',
  product_variants: [
    {
      price: 100,
      stock_item: {
        quantity: 1999
      }
    }
  ]
}

export const LongNameProduct = {
  name: '商品名です商品名です商品名です商品名です商品名です商品名です',
  product_code: 'product-code2',
  product_variants: [
    {
      price: 100,
      stock_item: {
        quantity: 19
      }
    }
  ]
}

export const OutOfStockProduct = {
  name: '超人気ゲーム機',
  product_code: 'product-code3',
  product_variants: [
    {
      price: 100,
      stock_item: {
        quantity: 0
      }
    }
  ]
}

export default {
  name: 'ProductListItem',
  component: ProductListItem,
  properties: [
    {
      title: 'Default',
      property: {
        product: NormalProduct,
        onSelect: (product) => console.log('OnSelectProduct', product)
      }
    },
    {
      title: 'Long Product Name',
      property: {
        product: LongNameProduct,
        onSelect: (product) => console.log('OnSelectProduct', product)
      }
    },
    {
      title: 'Out Of Stock',
      property: {
        product: OutOfStockProduct,
        onSelect: (product) => console.log('OnSelectProduct', product)
      }
    }
  ],
  frames: [
    {
      title: '428 x 104',
      style: {width: 428, height: 104}
    }
  ]
}
