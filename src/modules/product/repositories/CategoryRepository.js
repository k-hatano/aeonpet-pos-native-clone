import StandardCategoryRepository from './standard/StandardCategoryRepository'
import SampleCategoryRepository from './sample/SampleCategoryRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CategoryRepository {
  static _implement = new SampleCategoryRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static async findByParentId (parentId) {
    return this._implement.findByParentId(parentId)
  }

  static async findById (id) {
    return this._implement.findById(id)
  }

  static async createSamples () {
    return this._implement.createSamples()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCategoryRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardCategoryRepository()
        break

      default:
        this._implement = new SampleCategoryRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CategoryRepository', (context) => {
  CategoryRepository.switchImplement(context)
})
