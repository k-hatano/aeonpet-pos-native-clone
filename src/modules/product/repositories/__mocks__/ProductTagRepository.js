import SampleProductTagRepository from '../sample/SampleProductTagRepository'

export default class ProductTagRepository {
  static _implement = new SampleProductTagRepository()

  static async findByProductId (productId) {
    return this._implement.findByProductId(productId)
  }
}
