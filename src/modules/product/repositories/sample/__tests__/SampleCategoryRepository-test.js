import SampleCategoryRepository, { sampleCategoryMap } from '../SampleCategoryRepository'

describe('SampleCategoryRepository Test', () => {
  it('findByParentId', async () => {
    const categories = await (new SampleCategoryRepository()).findByParentId(sampleCategoryMap.deep._1._1.id)
    expect(categories.length).toBe(2)
    expect(categories[0].parent_id).toBe(sampleCategoryMap.deep._1._1.id)
    expect(categories[0].id).toBe(sampleCategoryMap.deep._1._1._1.id)
  })
})
