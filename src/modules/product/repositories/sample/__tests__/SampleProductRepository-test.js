import SampleProductRepository from '../SampleProductRepository'
import { sampleProductMap } from '../../../samples'

describe('SampleProductRepository Test', () => {
  const repository = new SampleProductRepository()

  it('findSetById', async () => {
    const set = await repository.findSetById(sampleProductMap.setA.id)
    expect(set.length).toBe(3)
    expect(set[0].id).toBe(sampleProductMap.setA_child1.id)
    expect(set[0].quantity).toBe(1)
    expect(set[0].variant).not.toBeNull()
  })
})
