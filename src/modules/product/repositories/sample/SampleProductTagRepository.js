import { sampleProductProductTags } from '../../samples'

export default class SampleProductTagRepository {
  async findByProductId (productId) {
    return sampleProductProductTags.filter(tag => tag.product_id === productId)
  }

  async createSamples () {
  }
}
