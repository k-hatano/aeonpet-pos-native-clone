import { sampleProducts, sampleProductSets } from '../../samples'

export default class SampleProductRepository {
  async fetchProductVariantById (productVariantId) {
  }

  async findAll () {
    return sampleProducts
  }

  async findWithDetail (productId) {
    return sampleProducts.find(product => (product.id === productId))
  }
  async findProductsByProductId (productId) {
    return sampleProducts.findAll()
  }
  async findProductsWithCategoryIdForStocks (productCategoryId, excludeSetProduct = false) {
    return this.findProductsWithCategoryId(productCategoryId, true)
  }
  async findProductsWithCategoryId (productCategoryId, stockConditionFlg) {
    return sampleProducts.filter(
      product => (product.product_categories.includes(productCategoryId) && product.status === 1)
    )
  }
  async findProductsBySearchWordForStocks (word, conditionFlg, isBarcode, excludeSetProduct = false) {
    return this.findProductsBySearchWord(word, true, isBarcode)
  }
  async findProductsBySearchWord (word, conditionFlg, isBarcode) {
    let productData = null
    productData = sampleProducts.filter(product => product.status === 1)
    if (isBarcode) {
      productData = productData.filter(product => product.article_number === word)
    } else {
      productData = productData.filter(product => (
        product.article_number === word ||
        (' ' + product.product_code).indexOf(' ' + word) >= 0 ||
        (product.name).indexOf(word) >= 0))
    }
    if (!conditionFlg) {
      productData = productData.filter(product => product.product_variants[0].stock_item.quantity > 0)
    }
    return productData
  }

  async findVariantsBySearchWordForStocks (word, excludeSetProduct = false) {
    return this.findVariantsBySearchWord(word, true)
  }
  async findVariantsBySearchWord (word, conditionFlg) {
    let variantData = sampleProducts.filter(
      product => product.product_variants[0].article_number === word && product.status === 1
    )
    if (!conditionFlg) {
      variantData = variantData.filter(product => product.product_variants[0].stock_item.quantity > 0)
    }
    return variantData
  }

  async findVariantsByProductId (productId) {
    return sampleProducts.filter(product => product.product_variants[0].product_id === productId)
  }

  async findSetById (productId) {
    return sampleProductSets.filter(set => set.product_id === productId).map(set => {
      const product = sampleProducts.find(product => product.product_variants[0].id === set.product_variant_id)
      return {
        ...product,
        quantity: set.quantity,
        variant: product.product_variants[0]
      }
    })
  }

  updateQuantityByProductId (quantity, productId, productVariantId) {
    // TODO SampleMode 対応
  }
}
