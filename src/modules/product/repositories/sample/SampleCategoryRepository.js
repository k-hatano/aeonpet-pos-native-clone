import generateUuid from 'common/utils/generateUuid'
import * as _ from 'underscore'

const generateRandomCode = () => {
  return ('000000' + Math.floor((Math.random() * 10000000)).toString()).slice(-7)
}

export const sampleCategoryMap = {
  standard: {
    id: generateUuid(),
    name: '基本カテゴリ',
    category_code: generateRandomCode(),
    sort_order: 1
  },
  various: {
    id: generateUuid(),
    name: '様々なパターン',
    category_code: generateRandomCode(),
    sort_order: 2
  },
  bundle: {
    id: generateUuid(),
    name: 'バンドル対象',
    category_code: generateRandomCode(),
    sort_order: 3
  },
  sale: {
    id: generateUuid(),
    name: 'セール対象',
    category_code: generateRandomCode(),
    sort_order: 4
  },
  taxfree: {
    id: generateUuid(),
    name: '免税 (他は一般品)',
    category_code: generateRandomCode(),
    sort_order: 5
  },
  deep: {
    id: generateUuid(),
    name: '深カテゴリ',
    category_code: generateRandomCode(),
    sort_order: 6,
    _1: {
      id: generateUuid(),
      name: '深カテゴリ-1',
      category_code: generateRandomCode(),
      sort_order: 7,
      _1: {
        id: generateUuid(),
        name: '深カテゴリ-1-1',
        category_code: generateRandomCode(),
        sort_order: 9,
        _1: {
          id: generateUuid(),
          name: '深カテゴリ-1-1-1',
          category_code: generateRandomCode(),
          sort_order: 11
        },
        _2: {
          id: generateUuid(),
          name: '深カテゴリ-1-1-2',
          category_code: generateRandomCode(),
          sort_order: 12
        }
      },
      _2: {
        id: generateUuid(),
        name: '深カテゴリ-1-2',
        category_code: generateRandomCode(),
        sort_order: 10
      }
    },
    _2: {
      id: generateUuid(),
      name: '深カテゴリ-2',
      category_code: generateRandomCode(),
      sort_order: 8
    }
  }
}

const propertyKeys = ['id', 'name', '_lft', '_rgt', 'category_code']
const buildCategories = (source, parentId, rangeNumber, categories) => {
  const childKeys = _.difference(Object.keys(source), propertyKeys)
  const lft = rangeNumber
  childKeys.forEach(childKey => {
    rangeNumber = buildCategories(source[childKey], source.id || null, rangeNumber + 1, categories)
  })

  if (source.id) {
    rangeNumber += 1
    const category = {
      id: source.id,
      name: source.name,
      parent_id: parentId,
      category_code: source.category_code,
      _lft: lft,
      _rgt: rangeNumber
    }
    categories.push(category)
  }
  return rangeNumber + 1
}

export const sampleCategories = []
buildCategories(sampleCategoryMap, null, 1, sampleCategories)

export default class SampleCategoryRepository {
  async findAll () {
    return sampleCategories
  }

  async findByParentId (parentId) {
    return sampleCategories.filter(category => (category.parent_id === parentId))
  }

  async findById (id) {
    return sampleCategories.filter(category => (category.id === id))
  }

  async createSamples () {
    console.log('Create Catalogs')
  }
}
