import SampleProductTagRepository from './sample/SampleProductTagRepository'
import StandardProductTagRepository from './standard/StandardProductTagRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class ProductTagRepository {
  static _implement = new SampleProductTagRepository()

  static async findByProductId (productId) {
    return this._implement.findByProductId(productId)
  }

  static async createSamples () {
    return this._implement.createSamples()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardProductTagRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardProductTagRepository()
        break

      default:
        this._implement = new SampleProductTagRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('ProductTagRepository', (context) => {
  ProductTagRepository.switchImplement(context)
})
