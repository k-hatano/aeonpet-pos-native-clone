import SampleProductRepository from './sample/SampleProductRepository'
import StandardProductRepository from './standard/StandardProductRepository'
import TrainingProductRepository from './training/TrainingProductRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class ProductRepository {
  static _implement = new SampleProductRepository()

  static async fetchProductVariantById (productVariantId) {
    return this._implement.fetchProductVariantById(productVariantId)
  }

  static async findWithDetail (productId) {
    return this._implement.findWithDetail(productId)
  }

  /**
   * カテゴリに紐づく商品を検索します。（在庫系機能で利用）
   * @param {string} productCategoryId
   * @param {bool} excludeSetProduct 検索対象からセット商品を除外するかどうか
   * @returns {Promise<*>}
   */
  static async findProductsWithCategoryIdForStocks (productCategoryId, excludeSetProduct = false) {
    return this._implement.findProductsWithCategoryIdForStocks(productCategoryId, excludeSetProduct)
  }
  static async findProductsWithCategoryId (productCategoryId, stockConditionFlg) {
    return this._implement.findProductsWithCategoryId(productCategoryId, stockConditionFlg)
  }

  /**
   * 検索ワードから商品を検索します。（在庫系機能で利用）
   * @param {string} word 検索ワード
   * @param {bool} isBarcode バーコードスキャンされたかどうか
   * @param {bool} excludeSetProduct 検索対象からセット商品を除外するかどうか
   * @returns {Promise<*>}
   */
  static async findProductsBySearchWordForStocks (word, isBarcode, excludeSetProduct = false) {
    return this._implement.findProductsBySearchWordForStocks(word, isBarcode, excludeSetProduct)
  }
  static async findProductsBySearchWord (word, conditionFlg, isBarcode) {
    return this._implement.findProductsBySearchWord(word, conditionFlg, isBarcode)
  }

  /**
   * 検索ワードから商品規格を検索します。（在庫系機能で利用）
   * @param {string} word 検索ワード
   * @param {bool} excludeSetProduct 検索対象からセット商品を除外するかどうか
   * @returns {Promise<*>}
   */
  static async findVariantsBySearchWordForStocks (word, excludeSetProduct = false) {
    return this._implement.findVariantsBySearchWordForStocks(word, excludeSetProduct)
  }
  static async findVariantsBySearchWord (word, conditionFlg) {
    return this._implement.findVariantsBySearchWord(word, conditionFlg)
  }

  /**
   * 商品IDから商品詳細情報を検索します。
   * @param {array|integer} productId
   * @returns {Promise<*>}
   */
  static async findProductsByProductId (productId) {
    return this._implement.findProductsByProductId(productId)
  }
  static async findVariantsByProductId (productId) {
    return this._implement.findVariantsByProductId(productId)
  }

  /**
   * セット商品の一覧を取得する
   * @param productId
   * @return {Promise.<Array<ProductEntity>>}
   */
  static async findSetById (productId) {
    return this._implement.findSetById(productId)
  }

  static async updateQuantityByProductId (quantity, productId, productVariantId) {
    return this._implement.updateQuantityByProductId(quantity, productId, productVariantId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardProductRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingProductRepository()
        break

      default:
        this._implement = new SampleProductRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('ProductRepository', (context) => {
  ProductRepository.switchImplement(context)
})
