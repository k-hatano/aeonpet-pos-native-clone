import StandardProductRepository from '../standard/StandardProductRepository'

export default class TrainingProductRepository {

  _standardProductRepository = new StandardProductRepository()

  async findAll () {
    return this._standardProductRepository.findAll()
  }

  async findWithDetail (productId) {
    return this._standardProductRepository.findWithDetail(productId)
  }

  async findProductsWithCategoryId (productCategoryId, stockConditionFlg) {
    return this._standardProductRepository.findProductsWithCategoryId(productCategoryId, stockConditionFlg)
  }

  async findProductsBySearchWord (word, conditionFlg, isBarcode) {
    return this._standardProductRepository.findProductsBySearchWord(word, conditionFlg, isBarcode)
  }

  async findVariantsBySearchWord (word, conditionFlg) {
    return this._standardProductRepository.findVariantsBySearchWord(word, conditionFlg)
  }

  async findVariantsByProductId (productId) {
    return this._standardProductRepository.findVariantsByProductId(productId)
  }

  /**
   * セット商品の一覧を取得する
   * @param productId
   * @return {Promise.<Array<ProductEntity>>}
   */
  async findSetById (productId) {
    return this._standardProductRepository.findSetById(productId)
  }

  async updateQuantityByProductId (quantity, productId, productVariantId) {
    console.log('Training mode：updateQuantityByProductId')
  }
}

