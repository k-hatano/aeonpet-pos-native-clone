import StandardProductRepository from '../StandardProductRepository'
import StandardCategoryRepository from '../StandardCategoryRepository'
import Migrator from 'common/utils/migrator'
import { sampleCategoryMap } from '../../sample/SampleCategoryRepository'
import { sampleProductMap } from '../../../samples'
import StandardProductTagRepository from '../StandardProductTagRepository'

describe('StandardProductRepository test sample', () => {
  const repository = new StandardProductRepository()
  beforeEach(async () => {
    await Migrator.up()
    await repository.createSamples()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('Find products by categoryId', async () => {
    await (new StandardCategoryRepository()).createSamples()
    const products = await repository.findProductsWithCategoryId(sampleCategoryMap.standard.id)
    expect(products.length).toBe(3)
  })

  it('Find product detail', async () => {
    await (new StandardProductTagRepository()).createSamples()
    const product = await repository.findWithDetail(sampleProductMap.standardA.id)
    expect(product.id).toBe(sampleProductMap.standardA.id)
    expect(product.product_variants.length).toBe(1)
    expect(product.product_variants[0].stock_item.quantity).toBe(100)
  })

  it('findSetById', async () => {
    const set = await repository.findSetById(sampleProductMap.setA.id)
    expect(set.length).toBe(3)
    expect(set.map(s => s.id)).toContain(sampleProductMap.setA_child1.id)
    expect(set[0].quantity).toBe(1)
    expect(set[0].variant).not.toBeNull()
  })
})
