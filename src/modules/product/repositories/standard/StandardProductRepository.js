import * as _ from 'underscore'
import Sequelize from 'sequelize'
import ProductEntity from '../entities/ProductEntity'
import ProductVariantEntity from '../entities/ProductVariantEntity'
import ProductProductCategoryEntity from '../entities/ProductProductCategoryEntity'
import ProductCategoryEntity from '../entities/ProductCategoryEntity'
import { sampleProducts, sampleProductSets } from '../../samples'
import AppEvents from '../../../../common/AppEvents'
import StockItemEntity from '../../../../modules/stock/repositories/entities/StockItemEntity'
import ProductProductVariantEntity from '../entities/ProductProductVariantEntity'
import fetcher from '../../../../common/models/fetcher'
import { handleAxiosError } from '../../../../common/errors'
import { encodeCriteria } from '../../../../common/utils/searchUtils'

export default class StandardProductRepository {
  async fetchProductVariantById (productVariantId) {
    try {
      const response = await fetcher.get('product-variants', encodeCriteria({id: productVariantId}))
      return response.data.length > 0 ? response.data[0] : null
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async findWithDetail (productId) {
    const productModel = await ProductEntity.scope('withVariants').find({
      where: {
        id: productId
      }
    })
    return productModel ? productModel.dataValues : null
  }

  async findProductsWithCategoryIdForStocks (productCategoryId, excludeSetProduct = false) {
    const whereCondition = {
      whereForStocks: null,
      whereForProducts: {
        is_parent_product: excludeSetProduct ? 0 : [0, 1]
      },
      whereForProductVariants: {
        stock_mode: 1
      }
    }
    return this.findProductsWithCategoryIdCore(productCategoryId, whereCondition)
  }
  async findProductsWithCategoryId (productCategoryId, conditionFlg) {
    let whereForStocks = null
    if (!conditionFlg) {
      whereForStocks = {}
      whereForStocks['$or'] = [
        {
          '$products.product_variants.stock_mode$': 0
        },
        {
          quantity: {$gt: 0}
        }
      ]
    }
    const condition = {
      whereForStocks,
      whereForProducts: {
        status: 1
      },
      whereForProductVariants: {}
    }
    return this.findProductsWithCategoryIdCore(productCategoryId, condition)
  }
  async findProductsWithCategoryIdCore (productCategoryId, condition) {
    const {whereForStocks, whereForProducts, whereForProductVariants} = condition
    const categoryModel = await ProductCategoryEntity.findAll({
      where: {
        id: productCategoryId
      },
      include: [{
        model: ProductEntity,
        as: 'products',
        attributes: {
          // iOSのjsエンジンで動かした時にdate型のマッピングが正しく動かないため、unixtimestampとして取得する
          include: [[Sequelize.fn('strftime', Sequelize.col('products.created_at')), 'created_at_unix']]
        },
        where: whereForProducts,
        include: [{
          model: ProductVariantEntity,
          as: 'product_variants',
          where: whereForProductVariants,
          include: [{
            model: StockItemEntity,
            as: 'stock_item',
            where: whereForStocks
          }]
        }]
      }]
    })
    if (categoryModel.length > 0) {
      return _.chain(categoryModel[0].dataValues.products)
        .map(product => product.dataValues)
        .sortBy(product => product.product_code)
        .sortBy(product => product.article_number)
        .value()
    }
    return []
  }

  async findProductsBySearchWordForStocks (word, isBarcode, excludeSetProduct = false) {
    const condition = {
      whereForStocks: null,
      whereForProducts: {
        is_parent_product: excludeSetProduct ? 0 : [0, 1]
      },
      whereForProductVariants: {
        stock_mode: 1
      }
    }
    return this.findProductsBySearchWordCore(word, condition, isBarcode)
  }
  async findProductsBySearchWord (word, conditionFlg, isBarcode) {
    let whereForStocks = null
    if (!conditionFlg) {
      whereForStocks = {}
      whereForStocks['$or'] = [
        {
          '$product_variants.stock_mode$': 0
        },
        {
          quantity: {$gt: 0}
        }
      ]
    }
    const condition = {
      whereForStocks,
      whereForProducts: {
        status: 1
      },
      whereForProductVariants: {}
    }
    return this.findProductsBySearchWordCore(word, condition, isBarcode)
  }
  async findProductsBySearchWordCore (word, condition, isBarcode) {
    const {whereForStocks, whereForProductVariants} = condition
    let {whereForProducts} = condition
    if (isBarcode) {
      whereForProducts['$product_variants.article_number$'] = word
    } else {
      whereForProducts = {
        ...whereForProducts,
        '$or': [
          {'$product_variants.article_number$': word},
          {product_code: {$like: word + '%'}},
          {name: {$like: '%' + word + '%'}}
        ]
      }
    }
    const productModel = await ProductEntity.findAll({
      where: whereForProducts,
      include: [{
        model: ProductVariantEntity,
        as: 'product_variants',
        where: whereForProductVariants,
        include: [{
          model: StockItemEntity,
          as: 'stock_item',
          where: whereForStocks
        }]
      }],
      order: [
        ['product_code', 'ASC'],
        ['article_number', 'ASC']
      ]
    })
    return _.compact(productModel.map((product) => {
      return product.dataValues
    }))
  }

  async findVariantsBySearchWordForStocks (word, excludeSetProduct = false) {
    const condition = {
      whereForStocks: null,
      whereForProducts: {
        is_parent_product: excludeSetProduct ? 0 : [0, 1]
      },
      whereForProductVariants: {
        stock_mode: 1,
        article_number: word
      }
    }
    return this.findVariantsBySearchWordCore(condition)
  }
  async findVariantsBySearchWord (word, conditionFlg) {
    let whereForStocks = null
    if (!conditionFlg) {
      whereForStocks = {
        quantity: {$gt: 0}
      }
    }
    const condition = {
      whereForStocks,
      whereForProducts: {
        status: 1
      },
      whereForProductVariants: {
        article_number: word
      }
    }
    return this.findVariantsBySearchWordCore(condition)
  }
  async findVariantsBySearchWordCore (condition) {
    const {whereForStocks, whereForProducts, whereForProductVariants} = condition
    const productModel = await ProductEntity.findAll({
      where: whereForProducts,
      include: [{
        model: ProductVariantEntity,
        as: 'product_variants',
        where: whereForProductVariants,
        include: [{
          model: StockItemEntity,
          as: 'stock_item',
          where: whereForStocks
        }]
      }],
      order: [
        ['product_code', 'ASC'],
        ['article_number', 'ASC']
      ]
    })
    return _.compact(productModel.map((product) => {
      return product.dataValues
    }))
  }

  async findProductsByProductId (productId) {
    const products = await ProductEntity.scope('withVariants').findAll({
      where: {
        id: productId
      }
    })
    return products.map(item => {
      return {
        ...item.dataValues,
        product_variants: item.dataValues.product_variants.map(vitem => {
          return {
            ...vitem.dataValues,
            stock_item: vitem.dataValues.stock_item ? vitem.dataValues.stock_item.dataValues : null
          }
        })
      }
    })
  }
  async findVariantsByProductId (productId) {
    return ProductVariantEntity.findAll({
      where: {
        product_id: productId
      }
    })
  }

  async findSetById (productId) {
    const setProductMaps = await ProductProductVariantEntity.findAll({
      where: { product_id: productId },
      include: [{
        model: ProductVariantEntity,
        as: 'variant',
        include: [{
          model: ProductEntity,
          as: 'product'
        },
        {
          model: StockItemEntity,
          as: 'stock_item'
        }]
      }]
    })
    return setProductMaps.map(map => {
      const product = map.variant.product.dataValues
      product.variant = map.variant.dataValues
      product.stock_item = map.variant.stock_item ? map.variant.stock_item.dataValues : null
      product.quantity = map.quantity
      return product
    })
  }

  async updateQuantityByProductId (quantity, productId, productVariantId) {
    const stockItemsData = await ProductEntity.find({
      where: {id: productId},
      include: [{
        model: ProductVariantEntity,
        as: 'product_variants',
        where: {id: productVariantId},
        include: [{
          model: StockItemEntity,
          as: 'stock_item',
          attributes: ['id', 'quantity']
        }]
      }]
    })

    // stock_itemが存在しない場合は処理を行わない
    if (stockItemsData.product_variants[0].stock_item) {
      const stockId = stockItemsData.product_variants[0].stock_item.id
      const newQuantity = stockItemsData.product_variants[0].stock_item.quantity - quantity

      await StockItemEntity.update(
        {quantity: newQuantity},
        {where: {id: stockId}}
      )
    }
  }

  async createSamples () {
    const existingCount = await ProductEntity.count()
    if (existingCount === 0) {
      await ProductEntity.bulkCreate(sampleProducts.map(product => {
        const { product_variants, ...rest } = product
        return rest
      }))

      await ProductVariantEntity.bulkCreate(_.flatten(sampleProducts.map(product => {
        return product.product_variants.map(productVariant => {
          const { stock_item, ...rest } = productVariant
          return rest
        })
      })))

      const stockItems = _.flatten(sampleProducts.map(product => {
        return product.product_variants.map(productVariant => {
          return productVariant.stock_item
        })
      }))
      await StockItemEntity.bulkCreate(stockItems)

      await ProductProductCategoryEntity.bulkCreate(_.flatten(sampleProducts.map(product => {
        return product.product_categories.map(categoryId => {
          return {
            product_id: product.id,
            product_category_id: categoryId
          }
        })
      })))

      // let sql = 'insert into stock_items (id, quantity, product_variant_id, warehouse_id, product_code, warehouse_code) VALUES '
      // sql += stockItems.map(stockItem => {
      //   return '(' +
      //     [
      //       `unhex('${generateUuid()}')`,
      //       stockItem.quantity,
      //       `unhex('${stockItem.product_variant_id}')`,
      //       'unhex(@warehouseId)',
      //       "''",
      //       "''"
      //     ].join(', ') + ')'
      // }).join(',\n') + ';'
      // console.log(sql)

      await ProductProductVariantEntity.bulkCreate(sampleProductSets)
    }
  }
}

AppEvents.onSampleDataCreate('StandardProductRepository', async () => {
  return (new StandardProductRepository()).createSamples()
})
