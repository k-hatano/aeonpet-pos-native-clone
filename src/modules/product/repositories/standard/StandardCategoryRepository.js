import ProductCategoryEntity from '../entities/ProductCategoryEntity'
import AppEvents from 'common/AppEvents'
import { sampleCategories } from '../sample/SampleCategoryRepository'

export default class StandardCategoryRepository {
  async findAll () {
    return ProductCategoryEntity.findAll()
  }

  async findByParentId (parentCategoryId) {
    return ProductCategoryEntity.findAll({
      attributes: ['id', 'name', 'parent_id'],
      where: {
        parent_id: parentCategoryId
      },
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async findById (id) {
    return ProductCategoryEntity.findAll({
      attributes: ['id', 'name', 'parent_id'],
      where: {
        id: id
      },
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async createSamples () {
    const existingCount = await ProductCategoryEntity.count()
    if (existingCount === 0) {
      await ProductCategoryEntity.bulkCreate(sampleCategories)
    }
  }
}

AppEvents.onSampleDataCreate('StandardCategoryRepository', async () => {
  return (new StandardCategoryRepository()).createSamples()
})
