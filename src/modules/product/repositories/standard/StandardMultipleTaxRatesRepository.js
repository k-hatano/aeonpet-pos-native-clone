import MultipleTaxRatesEntity from '../entities/MultipleTaxRatesEntity'
import moment from 'moment'

export default class StandardMultipleTaxRatesRepository {
  async findTaxWithTaxCode (taxCode) {
    const nowDate = moment()
    const taxModel = await MultipleTaxRatesEntity.find({
      where: {
        tax_code: taxCode,
        start_date: {
          $lte: nowDate
        },
        end_date: {
          $gt: nowDate
        }
      },
      order: [
        ['start_date', 'DESC']
      ],
      limit: 1
    })
    return taxModel ? taxModel.dataValues : null
  }
}
