import * as _ from 'underscore'
import ProductProductTagEntity from '../entities/ProductProductTagEntity'
import { sampleProductTags } from '../../samples'
import AppEvents from 'common/AppEvents'

export default class StandardProductTagRepository {
  async findByProductId (productId) {
    return ProductProductTagEntity.findAll({
      where: {
        product_id: productId
      },
      raw: true
    })
  }

  async createSamples () {
    if (await ProductProductTagEntity.count() === 0) {
      await ProductProductTagEntity.bulkCreate(
        _.flatten(sampleProductTags.map(tag => {
          return tag.products.map(productId => {
            return {
              product_tag_id: tag.id,
              product_id: productId
            }
          })
        }))
      )
    }
  }
}

AppEvents.onSampleDataCreate('StandardProductTagRepository', async () => {
  return (new StandardProductTagRepository()).createSamples()
})
