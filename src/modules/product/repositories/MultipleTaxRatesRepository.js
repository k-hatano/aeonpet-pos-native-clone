import StandardMultipleTaxRatesRepository from './standard/StandardMultipleTaxRatesRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class MultipleTaxRatesRepository {
  static _implement = new StandardMultipleTaxRatesRepository()

  static async findTaxWithTaxCode (taxCode) {
    return this._implement.findTaxWithTaxCode(taxCode)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardMultipleTaxRatesRepository()
        break

      default:
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('MultipleTaxRatesRepository', (context) => {
  MultipleTaxRatesRepository.switchImplement(context)
})
