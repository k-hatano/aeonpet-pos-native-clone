import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductTag = sequelize.define(
  'ProductTag',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    sort_order: Sequelize.INTEGER,
    product_tag_type: Sequelize.INTEGER,
    product_tag_code: Sequelize.STRING
  }, {
    tableName: 'product_tags',
    underscored: true,
    timestamps: false
  }
)

export default ProductTag
