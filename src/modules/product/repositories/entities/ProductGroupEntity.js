import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductGroup = sequelize.define(
  'ProductGroup',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    group_code: Sequelize.STRING,
    sort_order: Sequelize.INTEGER,
    status: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'product_groups',
    underscored: true,
    timestamps: false
  }
)

export default ProductGroup
