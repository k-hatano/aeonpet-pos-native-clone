import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import ProductVariant from './ProductVariantEntity'
import ProductCategory from './ProductCategoryEntity'
import ProductProductCategory from './ProductProductCategoryEntity'
import ProductProductTag from './ProductProductTagEntity'
import ProductProductVariant from './ProductProductVariantEntity'
import StockItem from 'modules/stock/repositories/entities/StockItemEntity'
import productPointRatio from '../../../promotion/repositories/entities/ProductPointRatioEntity'

/**
 * @typedef {Object} ProductEntity
 * @property {string} id
 * @property {string} name
 * @property {string} product_name_sys21
 * @property {string} product_code
 * @property {string} article_number
 * @property {integer} is_parent_product
 * @property {integer} is_discountable
 * @property {integer} is_sales_excluded
 * @property {integer} tax_rule
 * @property {number|string} tax_rate
 * @property {integer} tax_type
 * @property {string} taxfree_type
 * @property {string} product_group_id
 * @property {Array<ProductVariantEntity>} product_variants
 */

/**
 * @typedef {ProductEntity} SetProductEntity
 * @property {integer} quantity
 * @property {ProductVariantEntity} variant
 */

const Product = sequelize.define(
  'Product',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    short_name: Sequelize.STRING,
    product_name_sys21: Sequelize.STRING,
    product_code: Sequelize.STRING,
    article_number: Sequelize.STRING,
    is_parent_product: Sequelize.INTEGER,
    is_discountable: Sequelize.INTEGER,
    is_sales_excluded: Sequelize.INTEGER,
    change_price_type: Sequelize.INTEGER,
    tax_rule: Sequelize.INTEGER,
    tax_rate: Sequelize.DECIMAL(5, 4),
    tax_type: Sequelize.INTEGER,
    taxfree_type: Sequelize.INTEGER,
    product_group_id: Sequelize.STRING,
    status: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    tax_code: Sequelize.INTEGER
  }, {
    tableName: 'products',
    underscored: true,
    timestamps: false,
    scopes: {
      withVariants: {
        include: [{
          model: ProductVariant.scope('withStockItems'),
          as: 'product_variants'
        }]
      },
      withTags: {
        include: [{
          model: ProductProductTag,
          as: 'product_tags'
        }]
      }
    }
  }
)

export default Product

Product.hasMany(ProductVariant, {
  foreignKey: 'product_id',
  as: 'product_variants'
})

Product.hasMany(ProductProductTag, {
  foreignKey: 'product_id',
  as: 'product_tags'
})

Product.hasMany(ProductProductVariant, {
  targetKey: 'product_id',
  as: 'child_products'
})

Product.belongsToMany(ProductCategory, {
  as: 'product_categories',
  through: ProductProductCategory
})

ProductCategory.belongsToMany(Product, {
  as: 'products',
  through: ProductProductCategory
})

Product.belongsToMany(ProductVariant, {
  as: 'set_product_variants',
  through: ProductProductVariant
})

ProductVariant.belongsToMany(Product, {
  as: 'parent_products',
  through: ProductProductVariant
})

ProductProductVariant.belongsTo(ProductVariant, {
  as: 'variant',
  foreignKey: 'product_variant_id'
})

ProductVariant.hasOne(StockItem, {
  foreignKey: 'product_variant_id',
  as: 'stock_item'
})

ProductVariant.belongsTo(Product, {
  foreignKey: 'product_id',
  as: 'product'
})
