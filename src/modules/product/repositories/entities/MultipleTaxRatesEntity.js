import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const MultipleTaxRates = sequelize.define(
  'MultipleTaxRates',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    tax_code: Sequelize.INTEGER,
    start_date: Sequelize.DATE,
    end_date: Sequelize.DATE,
    tax_rate: Sequelize.DECIMAL(5, 4),
    receipt_sign: Sequelize.STRING,
    receipt_comment: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    deleted_at: Sequelize.DATE
  },
  {
    tableName: 'multiple_tax_rates',
    underscored: true,
    timestamps: false
  }
)

export default MultipleTaxRates
