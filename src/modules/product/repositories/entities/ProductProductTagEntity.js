import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductProductTag = sequelize.define(
  'ProductProductTag',
  {
    product_id: Sequelize.STRING,
    product_tag_id: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'product_product_tag',
    underscored: true,
    timestamps: false
  }
)
ProductProductTag.removeAttribute('id')

export default ProductProductTag
