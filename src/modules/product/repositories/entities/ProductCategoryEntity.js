import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductCategory = sequelize.define(
  'ProductCategory',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    parent_id: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    sort_order: Sequelize.INTEGER
  }, {
    tableName: 'product_categories',
    underscored: true,
    timestamps: false
  }
)

export default ProductCategory
