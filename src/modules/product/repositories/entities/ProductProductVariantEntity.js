import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductProductVariant = sequelize.define(
  'ProductProductVariant',
  {
    product_id: Sequelize.STRING,
    product_variant_id: Sequelize.STRING,
    quantity: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  },
  {
    tableName: 'product_product_variant',
    underscored: true,
    timestamps: false
  }
)

export default ProductProductVariant
