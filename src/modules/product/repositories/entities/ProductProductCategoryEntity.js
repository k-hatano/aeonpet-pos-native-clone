import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ProductProductCategory = sequelize.define(
  'ProductProductCategory',
  {
    product_id: Sequelize.STRING,
    product_category_id: Sequelize.STRING,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'product_product_category',
    underscored: true,
    timestamps: false
  }
)

export default ProductProductCategory
