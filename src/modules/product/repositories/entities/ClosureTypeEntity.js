import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const ClosureType = sequelize.define(
  'ClosureType',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    closure_type_code: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'closure_types',
    underscored: true,
    timestamps: false
  }
)

export default ClosureType
