import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import StockItem from '../../../stock/repositories/entities/StockItemEntity'

/**
 * @typedef {Object} ProductVariantEntity
 * @property {string} id
 * @property {string} product_id
 * @property {integer} stock_mode
 * @property {string} sku
 * @property {string} article_number
 * @property {integer} can_grant_points
 * @property {number|string} price
 * @property {string} created_at
 * @property {string} updated_at
 */

const ProductVariant = sequelize.define(
  'ProductVariant',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    product_id: Sequelize.STRING,
    stock_mode: Sequelize.INTEGER,
    sku: Sequelize.STRING,
    article_number: Sequelize.STRING,
    closure_type_id: Sequelize.INTEGER,
    closure_type_name: Sequelize.STRING,
    closure_type_code: Sequelize.STRING,
    aop_domaine: Sequelize.STRING,
    vintage: Sequelize.STRING,
    label_version: Sequelize.STRING,
    box_version: Sequelize.STRING,
    best_by_date: Sequelize.STRING,
    capacity: Sequelize.INTEGER,
    liquor_tax_type1: Sequelize.STRING,
    liquor_tax_type2: Sequelize.STRING,
    quantity_per_carton: Sequelize.INTEGER,
    can_grant_points: Sequelize.INTEGER,
    // sales_method: Sequelize.INTEGER,
    price: Sequelize.DECIMAL(19, 4),
    cost_price: Sequelize.DECIMAL(19, 4),
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  },
  {
    tableName: 'product_variants',
    underscored: true,
    timestamps: false,
    scopes: {
      withStockItems: {
        include: [{
          model: StockItem,
          as: 'stock_item'
        }]
      }
    }
  }
)

export default ProductVariant
