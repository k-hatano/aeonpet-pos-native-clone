import { connect } from 'react-redux'
import CategoryProductView from '../components/CategoryProductView'
import { MODULE_NAME as PRODUCT_MODULE_NAME, PREV_SEARCH_COND, CHANGE_PRICE_TYPE } from '../models'
import { MODULE_NAME as CART_MODULE_NAME } from '../../cart/models'
import I18n from 'i18n-js'
import * as actions from '../actions'
import ProductRepository from '../../product/repositories/ProductRepository'
import CategoryRepository from '../../product/repositories/CategoryRepository'
import AlertView from '../../../common/components/widgets/AlertView'
import Modal from '../../../common/components/widgets/Modal'
import { proceedUpdateCart } from '../../cart/services'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import logger from '../../../common/utils/logger'

const mapDispatchToProps = dispatch => ({
  onCategorySelected: async (category, stockConditionFlg, showStockConditionFlg, excludeSetProduct) => {
    if (category) {
      dispatch(actions.setPrevCategory(category))
      const categories = await CategoryRepository.findByParentId(category.id)
      dispatch(actions.listCategories(categories))
      dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_CATEGORY))

      let products
      // 在庫０の商品を表示しない＝在庫系の機能なので、その場合は在庫用の商品検索を利用する。
      if (showStockConditionFlg) {
        products = await ProductRepository.findProductsWithCategoryId(category.id, stockConditionFlg)
      } else {
        products = await ProductRepository.findProductsWithCategoryIdForStocks(category.id, excludeSetProduct)
      }
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      dispatch(actions.listProducts(products))
      dispatch(actions.listOriginProducts(products))
    } else {
      dispatch(actions.setPrevCategory({}))
      const categories = await CategoryRepository.findByParentId(null)
      dispatch(actions.listCategories(categories))
      dispatch(actions.listProducts([]))
      dispatch(actions.listOriginProducts([]))
      dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
    }
  },
  onPrevCategorySelected: async (id, stockConditionFlg, showStockConditionFlg, excludeSetProduct) => {
    dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_PREV_CATEGORY))
    if (id) {
      const categories = await CategoryRepository.findByParentId(id)
      dispatch(actions.listCategories(categories))
      dispatch(actions.setSearchCategoryId(id))

      let products
      // 在庫０の商品を表示しない＝在庫系の機能なので、その場合は在庫用の商品検索を利用する。
      if (showStockConditionFlg) {
        products = await ProductRepository.findProductsWithCategoryId(id, stockConditionFlg)
      } else {
        products = await ProductRepository.findProductsWithCategoryIdForStocks(id, excludeSetProduct)
      }
      const parentId = (categories.length > 0) ? categories[0].parent_id : null

      if (parentId) {
        const prevCategory = await CategoryRepository.findById(parentId)
        dispatch(actions.setPrevCategory(prevCategory[0]))
      } else {
        dispatch(actions.setPrevCategory({}))
      }
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      dispatch(actions.listProducts(products))
      dispatch(actions.listOriginProducts(products))
    } else {
      dispatch(actions.setPrevCategory({}))
      const categories = await CategoryRepository.findByParentId(null)
      dispatch(actions.listCategories(categories))
      dispatch(actions.listProducts([]))
      dispatch(actions.listOriginProducts([]))
      dispatch(actions.setSearchCategoryId(''))
    }
  },
  onChangeSearchWord: async (word) => {
    dispatch(actions.setSearchWord(word))
    if (word.length === 0) {
      dispatch(actions.setSearchBtnDisabled(true))
      dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
      return
    }
    dispatch(actions.setSearchBtnDisabled(false))
  },
  onChangeStockConditionFlg: async (conditionFlg) => {
    if (conditionFlg) {
      dispatch(actions.setStockConditionFlg(false))
    } else {
      dispatch(actions.setStockConditionFlg(true))
    }
  },
  onPressCategoryLink: async () => {
    dispatch(actions.setSearchLayoutFlg(false))
  },
  onSearch: async (
    cart,
    word,
    conditionFlg,
    showStockConditionFlg,
    excludeSetProduct,
    onShowAmountPad,
    isBarcode = false, isConditionFlagSearch = false
  ) => {
    let searchLayoutFlg = true
    // 検索結果用のレイアウト
    if (word.length === 0) {
      AlertView.show(I18n.t('product.not_exist'))
      logger.warning(I18n.t('product.not_affected_search') + '：' + word)
    } else {
      let products
      // 入力された条件をもとに、productsからデータを取得する
      // 何件ヒットするか
      //   2件以上 → 規格一覧を表示する
      //   1件のみ → 取得した商品の持つ規格の数を取得する分岐へ遷移
      //   0件 → 入力された条件でproduct_variantsを取得する
      //
      // 在庫０の商品を表示しない＝在庫系の機能なので、その場合は在庫用の商品検索を利用する。
      if (showStockConditionFlg) {
        products = await ProductRepository.findProductsBySearchWord(word, conditionFlg, isBarcode)
      } else {
        products = await ProductRepository.findProductsBySearchWordForStocks(word, isBarcode, excludeSetProduct)
      }
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      const hitCount = !isBarcode && products.length > 0 ? 2 : products.length
      // 手入力で検索した場合は、1件ヒット時と2件以上ヒット時で処理をわけず、必ず全件表示する。
      switch (hitCount) {
        case 0:
          let productVariantsByWord
          // 在庫０の商品を表示しない＝在庫系の機能なので、その場合は在庫用の商品検索を利用する。
          if (showStockConditionFlg) {
            productVariantsByWord = await ProductRepository.findVariantsBySearchWord(word, conditionFlg)
          } else {
            productVariantsByWord = await ProductRepository.findVariantsBySearchWordForStocks(word, excludeSetProduct)
          }
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          // 取得した商品規格をもとに、次処理の分岐を行う
          // 商品規格数 2件以上→規格一覧の表示 1件のみ→購入一覧に追加 0件→エラー表示
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              logger.warning(I18n.t('product.not_affected_search') + '：' + word)
              break
            case 1:
              if (onShowAmountPad) {
                onShowAmountPad(productVariantsByWord[0])
              } else {
                const product = productVariantsByWord[0]
                const productDetail = await ProductRepository.findWithDetail(productVariantsByWord[0].id)

                if (product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED && !isConditionFlagSearch) {
                  await AmountPadModal.open({
                    mode: AMOUNT_PAD_MODE.CASH,
                    onComplete: async (amount) => {
                      Modal.close()
                      await _updateCart(dispatch, cart, productDetail, amount)
                    },
                    title: product.name
                  })
                } else if (!isConditionFlagSearch) {
                  await _updateCart(dispatch, cart, productDetail)
                }
              }
              break
            default:
              dispatch(actions.listProducts(productVariantsByWord))
              dispatch(actions.listOriginProducts(productVariantsByWord))
              dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
              dispatch(actions.setSearchLayoutFlg(searchLayoutFlg))
              break
          }
          break
        case 1:
          // 取得した商品の持つ規格の数を取得する分岐へ遷移
          const productVariantsById = await ProductRepository.findVariantsByProductId(products[0].id)
          // 取得した商品規格をもとに、次処理の分岐を行う
          // 商品規格数 1件のみ→購入一覧に追加 0件→エラー表示
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              logger.warning(I18n.t('product.not_affected_search') + '：' + word)
              break
            case 1:
              if (onShowAmountPad) {
                onShowAmountPad(products[0])
              } else {
                const product = products[0]
                const productDetail = await ProductRepository.findWithDetail(products[0].id)

                if (product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED && !isConditionFlagSearch) {
                  await AmountPadModal.open({
                    mode: AMOUNT_PAD_MODE.CASH,
                    onComplete: async (amount) => {
                      Modal.close()
                      await _updateCart(dispatch, cart, productDetail, amount)
                    },
                    title: product.name
                  })
                } else if (!isConditionFlagSearch) {
                  await _updateCart(dispatch, cart, productDetail)
                }
              }
              break
            default:
              // 1商品1SKUの想定のため何もせず
              break
          }
          break
        default:
          // 商品一覧を表示する
          dispatch(actions.listProducts(products))
          dispatch(actions.listOriginProducts(products))
          dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
          dispatch(actions.setSearchLayoutFlg(true))
          break
      }
      // バーコードで検索された場合、検索条件に読み込まれた値を設定する。
      // バーコード読み込みではおそらくこのメソッド自体呼ばれないかも
      if (isBarcode) {
        dispatch(actions.setSearchWord(word))
        dispatch(actions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
      }
    }
  },
  onProgress: (bool, progressBar) => {
    if (bool) {
      progressBar.show()
    } else {
      progressBar.hide()
    }
    dispatch(actions.setSearchBtnDisabled(bool))
  }
})

const _updateCart = async (dispatch, cart, productDetail, amount = undefined) => {
  const nextCartState = await cart.addProductAsync(productDetail, productDetail.product_variants[0], 1, amount)
  await proceedUpdateCart(cart, nextCartState, dispatch)
}

const mapStateToProps = state => ({
  products: state[PRODUCT_MODULE_NAME].products,
  originProducts: state[PRODUCT_MODULE_NAME].originProducts,
  categories: state[PRODUCT_MODULE_NAME].categories,
  searchWord: state[PRODUCT_MODULE_NAME].searchWord,
  cart: state[CART_MODULE_NAME].cart,
  stockConditionFlg: state[PRODUCT_MODULE_NAME].stockConditionFlg,
  searchLayoutFlg: state[PRODUCT_MODULE_NAME].searchLayoutFlg,
  prevCategory: state[PRODUCT_MODULE_NAME].prevCategory,
  searchCategoryId: state[PRODUCT_MODULE_NAME].searchCategoryId,
  prevSearchCond: state[PRODUCT_MODULE_NAME].prevSearchCond,
  searchBtnDisabled: state[PRODUCT_MODULE_NAME].searchBtnDisabled
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryProductView)
