import { connect } from 'react-redux'
import I18n from 'i18n-js'
import CheckSalesView from '../components/CheckSalesView'
import { AGGREGATE_TYPE, MODULE_NAME, TOTAL_MODE, CASHIER_RECORD_MODE } from '../models'
import {
  getAccountAmount,
  makeCashierRecordFromState,
  pushCashierCashRecord,
  savePosEJournal,
  saveShopEJournal
} from '../services'
import * as actions from '../actions'
import CashierTotalRepository from '../repositories/CashierTotalRepository'
import CashierTotalReceiptBuilder from '../models/CashierTotalReceiptBuilder'
import PrinterManager from 'modules/printer/models/PrinterManager'
import { loading } from 'common/sideEffects'
import { Actions } from 'react-native-router-flux'
import AlertView from 'common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { formatPriceWithCurrency } from 'common/utils/formats'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import logger from 'common/utils/logger'
import { PERMISSION_CODES } from 'modules/staff/models'
import { RECEIPT_TYPE } from 'modules/printer/models'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { getCommonNetworkErrorMessage } from '../../../common/errors'

/**
 * 在高登録画面を表示して、在高登録を行う。
 * @param dispatch
 * @private
 */
const _onStartCashierRecord = (dispatch) => {
  Actions.cashierRecord({
    cashierRecordMode: CASHIER_RECORD_MODE.CHECK_SALES,
    permissionCode: PERMISSION_CODES.CHECK_SALES,
    onCompleteConfirm: (cashierRecord, cashierRecordData) => {
      return new Promise((resolve, reject) => {
        const currency = cashierRecord.currency
        const options = {
          totalAmount: formatPriceWithCurrency(cashierRecord.total_amount, currency),
          accountAmount: formatPriceWithCurrency(cashierRecord.account_amount, currency),
          withdrawalAmount: formatPriceWithCurrency(cashierRecord.withdrawal_amount, currency),  // 2019.6 中間回収額追加
          diff: formatPriceWithCurrency(cashierRecord.withdrawal_amount + cashierRecord.total_amount - cashierRecord.account_amount, currency)  // 2019.6 中間回収額追加
        }
        ConfirmView.show(I18n.t('message.G-03-I001', options), async () => {
          const pushSuccess = await pushCashierCashRecord(dispatch, cashierRecord, cashierRecordData, TOTAL_MODE.CHECK)
          resolve(pushSuccess)
        }, () => {
          resolve(false)
        })
      })
    },
    onCashierRecordUpdated: async (dispatch) => {
      await _onSelectAggregateType(dispatch, AGGREGATE_TYPE.POS)
    }
  })
}
/**
 *
 * @param dispatch
 * @param aggregateType
 * @param cashierTotalPosEJournal
 * @param cashierTotalShopEJournal
 * @returns {Promise.<void>}
 * @private
 */
const _onSelectAggregateType = async (dispatch, aggregateType, cashierTotalPosEJournal = null, cashierTotalShopEJournal = null) => {
  let cashierTotal = null
  let receipt = null
  let existingEjournal = null
  switch (aggregateType) {
    case AGGREGATE_TYPE.POS:
      existingEjournal = cashierTotalPosEJournal
      break
    case AGGREGATE_TYPE.SHOP:
      existingEjournal = cashierTotalShopEJournal
  }

  if (existingEjournal) {
    receipt = JSON.parse(existingEjournal.data)
  } else {
    try {
      await loading(dispatch, async () => {
        const date = new Date()
        cashierTotal = await CashierTotalRepository.fetchCheckSales(TOTAL_MODE.CHECK, aggregateType)
        await saveOperationLog(OPERATION_TYPE.CHECK_SALE, date)
        const builder = new CashierTotalReceiptBuilder()
        await builder.initializeAsync()
        receipt = builder.buildCashierTotalReceipt(cashierTotal, date, RECEIPT_TYPE.CHECK_SALES)

        switch (aggregateType) {
          case AGGREGATE_TYPE.POS:
            const posEjournal = await savePosEJournal(dispatch, receipt, cashierTotal, cashierTotalPosEJournal)
            dispatch(actions.setCashierTotalPosEJournal(posEjournal))
            break
          case AGGREGATE_TYPE.SHOP:
            const shopEjournal = await saveShopEJournal(dispatch, receipt, cashierTotal, cashierTotalShopEJournal)
            dispatch(actions.setCashierTotalShopEJournal(shopEjournal))
            break
        }
      })
    } catch (error) {
      if (error.errorType === 'InvalidOperation') {
        AlertView.show('不正な操作です。\n\n' + error.message)
      } else {
        logger.error('Unexpected Error ' + JSON.stringify(cashierTotal))
        AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.G-03-E004')))
      }
    }
  }
  dispatch(actions.setReceipt(receipt))
}
const mapDispatchToProps = dispatch => ({
  onInitialize: async (cashierRecord) => {
    // 合計金額が未入力の場合は、在高登録画面に遷移して入力させる。
    if (cashierRecord.total_amount === 0) {
      // 帳簿在高を取得する処理がローディングをだすので、
      // 先に在高画面を出さないと点検画面が表に出てしまう。
      _onStartCashierRecord(dispatch)
      try {
        const accountAmount = await getAccountAmount(dispatch)
        dispatch(actions.setAccountAmount(accountAmount))
      } catch (error) {
        AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.G-03-E004')), () => {
          logger.error(`Failed to get Account amount - ${error.message}`)
          // エラーが出たら前の画面に戻るしかねぇ！
          Actions.home()

          return true
        })
      }
    }
  },
  onStartCashierRecord: async () => _onStartCashierRecord(dispatch),
  onSelectAggregateType: async (aggregateType, cashierTotalPosEJournal, cashierTotalShopEJournal) => {
    await _onSelectAggregateType(dispatch, aggregateType, cashierTotalPosEJournal, cashierTotalShopEJournal)
  },
  onPrint: async (receipt, eJournal) => {
    try {
      await loading(dispatch, async () => {
        await PrinterManager.print(receipt)
      })
    } catch (error) {
      await EJournalRepository.saveIsNotPrintedById(eJournal.id)
      AlertView.show(I18n.t('message.G-02-E001'), () => true)
    }
  },
  onClose: async () => {
    dispatch(actions.resetState())
  }
})

const mapStateToProps = state => ({
  cashierRecord: makeCashierRecordFromState(state),
  cashierTotalPosEJournal: state[MODULE_NAME].cashierTotalPosEJournal,
  cashierTotalShopEJournal: state[MODULE_NAME].cashierTotalShopEJournal,
  receipt: state[MODULE_NAME].receipt,
  receiptText: state[MODULE_NAME].receiptText
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSelectAggregateType: async (aggregateType) => {
    dispatchProps.onSelectAggregateType(
      aggregateType,
      stateProps.cashierTotalPosEJournal,
      stateProps.cashierTotalShopEJournal)
  }
})

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CheckSalesView)
