import { connect } from 'react-redux'
import I18n from 'i18n-js'
import * as actions from '../actions'
import CloseSalesView from '../components/CloseSalesView'
import PrinterManager from 'modules/printer/models/PrinterManager'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import CashierTotalRepository from '../repositories/CashierTotalRepository'
import CashierTotalReceiptBuilder from '../models/CashierTotalReceiptBuilder'
import { MODULE_NAME, TOTAL_MODE, AGGREGATE_TYPE, CASHIER_RECORD_MODE } from '../models'
import {
  getAccountAmount,
  getWithdrawalAmountSummary,  // カスタマイズ-WBS-124
  makeCashierTotalEJournal,
  makeCashierRecordFromState,
  pushCashierCashRecord, printTotalReceipt, reprintCloseSale
} from '../services'
import { RECEIPT_TYPE } from 'modules/printer/models'
import { loading } from 'common/sideEffects'
import { Actions } from 'react-native-router-flux'
import logger from 'common/utils/logger'
import { toUnixTimestamp } from 'common/utils/dateTime'
import AlertView from 'common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { formatPriceWithCurrency } from 'common/utils/formats'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import { getSettingFromState } from 'modules/setting/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { PERMISSION_CODES } from 'modules/staff/models'
import { updateSetting } from 'modules/setting/actions'
import { getCommonNetworkErrorMessage } from '../../../common/errors'
import NetworkError from '../../../common/errors/NetworkError'
import { waitAsync } from '../../../common/utils/waitAsync'
import { cChangerErrorMessage } from 'modules/printer/models/CashChangerResult'

/**
 * 在高登録画面を表示して、在高登録を行う。
 * @param dispatch
 * @private
 */
const _onStartCashierRecord = (dispatch) => {
  Actions.cashierRecord({
    cashierRecordMode: CASHIER_RECORD_MODE.CLOSE_SALES,
    permissionCode: PERMISSION_CODES.CLOSE_SALES,
    onCompleteConfirm: (cashierRecord, cashierRecordData) => {
      return new Promise((resolve, reject) => {
        const currency = cashierRecord.currency
        const options = {
          totalAmount: formatPriceWithCurrency(cashierRecord.total_amount, currency),
          accountAmount: formatPriceWithCurrency(cashierRecord.account_amount, currency),
          diff: formatPriceWithCurrency(cashierRecord.total_amount - cashierRecord.account_amount, currency),
          depositAmount: formatPriceWithCurrency(cashierRecord.deposit_amount, currency),
          carryForwardAmount: formatPriceWithCurrency(cashierRecord.carry_forward_amount, currency),
          withdrawalAmountSummary: formatPriceWithCurrency(cashierRecord.withdrawal_amount_summary, currency), //カスタマイズ-WBS-124
        }
        ConfirmView.show(I18n.t('message.G-03-I002', options), async () => {
          const pushSuccess = await pushCashierCashRecord(dispatch, cashierRecord, cashierRecordData, TOTAL_MODE.CLOSE)
          resolve(pushSuccess)
        }, () => {
          resolve(false)
        })
      })
    },
    onCashierRecordUpdated: async (dispatch) => {
      let cashierTotal = null
      try {
        await loading(dispatch, async () => {
          cashierTotal = await CashierTotalRepository.fetchCheckSales(TOTAL_MODE.CHECK, AGGREGATE_TYPE.POS)
          const builder = new CashierTotalReceiptBuilder()
          await builder.initializeAsync()
          const receipt = builder.buildCashierTotalReceipt(cashierTotal, new Date())
          dispatch(actions.setReceipt(receipt))

          const eJournal = makeCashierTotalEJournal(
            receipt, cashierTotal, RECEIPT_TYPE.CHECK_SALES, toUnixTimestamp(new Date())
          )
          dispatch(actions.setCashierTotalCloseSalesEJournal(eJournal))
          await EJournalRepository.save(eJournal)
          try {
            await EJournalRepository.pushById(eJournal.id)
            // 通信に成功したら印刷済みとする。実際に印刷はしてないけどね。
            await EJournalRepository.saveIsPrintedById(eJournal.id)
          } catch (error) {
            // エラーは握りつぶす
            logger.warning('checkSales - saveShopEJournal error. ' + error.message)
          }
        })
      } catch (error) {
        dispatch(actions.setReceipt(null))

        if (error.errorType === 'InvalidOperation') {
          AlertView.show('不正な操作です。\n\n' + error.message)
        } else {
          logger.error('Unexpected Error ' + JSON.stringify(cashierTotal))
          AlertView.show('ネットワークエラーです。\n\n' + error.message)
        }
      }
    }
  })
}
/**
 * 精算処理を実施します。
 * @param createdAt
 * @param aggregateType
 * @param receiptPrintNum
 * @param businessDate
 * @returns {Promise.<void>}
 * @private
 */
const _closeSales = async (createdAt, aggregateType, receiptPrintNum, businessDate = null) => {
  let cashierTotal
  try {
    cashierTotal = await CashierTotalRepository.fetchCheckSales(TOTAL_MODE.CLOSE, aggregateType, businessDate)
  } catch (error) {
    if (aggregateType === AGGREGATE_TYPE.POS && error instanceof NetworkError) {
      const cashier = await CashierRepository.find()
      if (error.hasSuccessPossibility) {
        if (await checkIsOpenedWithRetry(cashier.id)) {
          throw error
        } else {
          cashierTotal = await CashierTotalRepository.fetchLastCloseSales(cashier.id)
        }
      } else {
        throw error
      }
    } else {
      logger.error('Unexpected error for close sales ' + error.message)
      throw error
    }
  }
  await saveOperationLog(OPERATION_TYPE.CLOSE_SALE, createdAt)
  return printTotalReceipt(createdAt, cashierTotal, aggregateType, receiptPrintNum)
}

async function checkIsOpenedWithRetry (cashierId): boolean {
  for (let i = 0; i < 5; i++) {
    try {
      await waitAsync(3000)
      const result = await CashierRepository.fetchIsOpenedByCashierId(cashierId)
      return result
    } catch (error) {
      // 失敗したらリトライするので、例外は握り潰す
    }
  }
  throw new Error()
}

function goToOpenSalesPage(dispatch) {
  Actions.openSales()
  // 開局画面遷移後の在高登録画面で中途半端に入力値が残ってしまい、
  // おかしな動きをするので、精算処理時のみクリアしておく。
  // https://s-cubism.backlog.jp/view/ENOTECA_POS_INNER-1421
  dispatch(actions.resetState())
  // 開局画面へ遷移する際に現在のスタッフ設定を初期化しておく
  dispatch(updateSetting({ key: SettingKeys.STAFF.CURRENT_STAFF, value: null }))
  dispatch(updateSetting({ key: SettingKeys.OPENSALE.IS_OPEN_SALE, value: false }))
}

async function checkIsOpenSaleBefore (dispatch, receiptPrintNum) {
  const [cashier, isOpened] = await loading(dispatch, async () => {
    const cashier = await CashierRepository.find()
    const isOpened = await CashierRepository.fetchIsOpenedByCashierId(cashier.id)
    return [cashier, isOpened]
  })
  // 未開局の場合は既に精算済みと判定して、精算レシートの再印字をするかどうかを尋ねる
  if (isOpened === 0) {
    const isOk = await ConfirmView.showAsync(I18n.t('message.G-02-E007'))
    if (isOk) {
      const reprintResult = await loading(dispatch, async () => {
        return reprintCloseSale(cashier, receiptPrintNum)
      })
      if (reprintResult.messages.length === 0) {
        goToOpenSalesPage(dispatch)
      } else {
        for (const message of reprintResult.messages) {
          AlertView.showAsync(message)
        }
      }
    }
    return false
  }
  return true
}

const mapDispatchToProps = dispatch => ({
  onInitialize: async (cashierRecord) => {
    // 合計金額が未入力の場合は、在高登録画面に遷移して入力させる。
    if (cashierRecord.total_amount === 0) {
      // 帳簿在高を取得する処理がローディングをだすので、
      // 先に在高画面を出さないと精算画面が表に出てしまう。
      _onStartCashierRecord(dispatch)
      try {
        const accountAmount = await getAccountAmount(dispatch)
        dispatch(actions.setAccountAmount(accountAmount))

        // カスタマイズ-WBS-124
        const withdrawalAmountSummary = await getWithdrawalAmountSummary(dispatch)
        dispatch(actions.setWithdrawalAmountSummary(withdrawalAmountSummary))
        // カスタマイズ-WBS-124
      } catch (error) {
        AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.G-03-E004')), () => {
          logger.error(`Failed to get Account amount - ${error.message}`)
          // エラーが出たら前の画面に戻るしかねぇ！
          Actions.home()

          return true
        })
      }
    }
  },
  onStartCashierRecord: async () => _onStartCashierRecord(dispatch),
  onPrint: async (receipt, eJournal) => {
    try {
      await loading(dispatch, async () => {
        await PrinterManager.print(receipt)
      })
    } catch (error) {
      logger.error(`Failed to print - ${error.message}`)
      try {
        await EJournalRepository.saveIsNotPrintedById(eJournal.id)
      } catch (error2) {
        logger.error(`Failed to update ejournal - ${error2.message}`)
      }

      AlertView.show(I18n.t('message.G-02-E001'), () => true)
    }
  },
  onCloseSales: async (receiptPrintNum) => {
    const isOk = await ConfirmView.showAsync(I18n.t('message.G-02-I002'))
    if (!isOk) return

    const createdAt = new Date()
    try {
      if (!(await checkIsOpenSaleBefore(dispatch, receiptPrintNum))) { return }

      const result = await loading(dispatch, async () => {
        // 自店舗の精算を行う。
        const cashierTotal = await _closeSales(createdAt, AGGREGATE_TYPE.POS, receiptPrintNum)
        // 全体精算フラグが 1 の場合は、引き続いて全体精算（店舗精算）を行う。
        if (cashierTotal.total_sales === 1) {
          const cashierTotals = await _closeSales(createdAt, AGGREGATE_TYPE.SHOP, receiptPrintNum, cashierTotal.business_date)
          cashierTotal.messages = cashierTotal.messages.concat(cashierTotals.messages)
        }
        return {messages: cashierTotal.messages}
      })
      for (const i in result.messages) {
        await AlertView.showAsync(result.messages[i])
      }
      goToOpenSalesPage(dispatch)
    } catch (error) {
      AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.G-02-E002')))
    }
  },
  onClose: async () => {
    dispatch(actions.resetState())
  }
})

const mapStateToProps = state => ({
  cashierRecord: makeCashierRecordFromState(state),
  receipt: state[MODULE_NAME].receipt,
  receiptText: state[MODULE_NAME].receiptText,
  receiptPrintNum: getSettingFromState(state, SettingKeys.RECEIPT.CASHIER_TOTAL.PRINT_NUM),
  cashierTotalCloseSalesEJournal: state[MODULE_NAME].cashierTotalCloseSalesEJournal
})

export default connect(mapStateToProps, mapDispatchToProps)(CloseSalesView)
