import { connect } from 'react-redux'
import SalesControl from '../components/SalesControl'
import { MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  cashierRecordData: state[MODULE_NAME].cashierRecordData
})

export default connect(mapStateToProps, mapDispatchToProps)(SalesControl)
