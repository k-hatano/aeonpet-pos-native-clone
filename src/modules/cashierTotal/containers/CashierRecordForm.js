import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import CashierRecordComponent from '../components/CashierRecordForm'
import { MODULE_NAME, CASHIER_RECORD_MODE } from '../models'
import * as actions from '../actions'
import { makeCashierRecordFromState, getCashOut, allCashOut, getAndSetCashInfo } from '../services'
import { backUpCashierRecordData, setIsUpdated } from 'modules/openSale/actions'
import { CASHIER_CASH_RECORD_MODE } from '../../cashier/models'
import AlertView from 'common/components/widgets/AlertView'
import Modal from 'common/components/widgets/Modal'
import I18n from 'i18n-js'  //2019.6.26
import PrinterManager from 'modules/printer/models/PrinterManager'

const mapDispatchToProps = dispatch => ({
  onInit: ()=>{
    getAndSetCashInfo(dispatch)
  },
  onChangedCashierRecordData: async (key, value, cashierRecordMode, withdrawalAmountSummary) => {
    dispatch(actions.updateCashierRecordData([{ key: key, value: value }]))
    if (cashierRecordMode === CASHIER_CASH_RECORD_MODE.CLOSE_SALE) {
      dispatch(actions.setCarryForwardAmount(withdrawalAmountSummary)) // カスタマイズ-WBS-124
    }
  },
  onClear: async (cashierRecordMode) => {
    dispatch(actions.resetState(cashierRecordMode))
  },
  onComplete: async (cashierRecord, cashierRecordData, onCompleteConfirm, onCashierRecordUpdated, isOpening, cashierRecordMode) => {
    let success = true
    // 点検・精算の時は事前確認する。
    
    if (onCompleteConfirm != null) {
      success = await onCompleteConfirm(cashierRecord, cashierRecordData)
    }
    if (isOpening) {
      dispatch(backUpCashierRecordData(JSON.parse(JSON.stringify(cashierRecordData))))
      dispatch(setIsUpdated(true))
    }
    if (success) {

      // 変更を外部に通知
      if (onCashierRecordUpdated != null) await onCashierRecordUpdated(dispatch, cashierRecord)

      // 2019.6 出金アラート処理 start
      if ((cashierRecord.withdrawal_amount!==0 || cashierRecordMode === CASHIER_CASH_RECORD_MODE.CLOSE_SALE) && PrinterManager.isSelectedCashChanger() && PrinterManager.isSetPrinter()) {
        let withdrawalAmount = {withdrawalAmount:cashierRecord.withdrawal_amount}
        if (cashierRecordMode === CASHIER_CASH_RECORD_MODE.CLOSE_SALE) {
          withdrawalAmount = {withdrawalAmount:cashierRecord.sum_amount}
        }
        const asyncfunction = ()=>{
          if (cashierRecordMode === CASHIER_CASH_RECORD_MODE.CLOSE_SALE) {
            return allCashOut()
          } else {
            return getCashOut(cashierRecord.withdrawal_amount)
          }
        }
        let retryCount =0;
        const generatorFunction4Retry = (function*() {
          while(retryCount<3){
            yield AlertView.showWithProgress(I18n.t('message.G-03-I003',withdrawalAmount)+(cashierRecordMode === CASHIER_CASH_RECORD_MODE.CLOSE_SALE?'\n\n※2千円札の回収は、手動で回収してください。':''),
              ()=>{ Modal.close(); Actions.pop(); return true },
              asyncfunction,
              (error)=>{AlertView.show(error, ()=> { generatorFunction4Retry.next(); if(retryCount>=3){ Modal.close(); Actions.pop();} return true })}
            )//2019.6 中間回収額を出金金として表示
            retryCount++;
          }
        })()
        generatorFunction4Retry.next()
      }else{
        Actions.pop()
      }
    }
  },
  onSkip: async (cashierRecord, onCashierRecordUpdated) => {
    Actions.pop()
    onCashierRecordUpdated(dispatch, cashierRecord)
  }
})

const mapStateToProps = state => ({
  cashierRecord: makeCashierRecordFromState(state),
  cashierRecordData: state[MODULE_NAME].cashierRecordData,
})

export default connect(mapStateToProps, mapDispatchToProps)(CashierRecordComponent)
