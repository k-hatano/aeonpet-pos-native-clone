import { connect } from 'react-redux'
import SalesAggregateForm from '../components/SalesAggregateForm'
import { MODULE_NAME } from '../models'
import { RECEIPT_TYPE } from 'modules/printer/models'
import * as actions from '../actions'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import CashierTotalRepository from '../repositories/CashierTotalRepository'
import AlertView from 'common/components/widgets/AlertView'
import CashierTotalReceiptBuilder from '../models/CashierTotalReceiptBuilder'
import { loading } from 'common/sideEffects'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { saveAggregateEJournal } from '../services'
import PrinterManager from 'modules/printer/models/PrinterManager'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { getCommonNetworkErrorMessage } from '../../../common/errors'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    try {
      const shop = { id: null, name: '店舗' }
      let cashiers = await loading(dispatch, () => CashierRepository.fetchAll())
      cashiers.unshift(shop)
      dispatch(actions.listCashiers(cashiers))
    } catch (error) {
      await AlertView.showAsync(getCommonNetworkErrorMessage(error))
      Actions.home()
    }
  },
  onSearch: async (aggregateType, fromDate, toDate, cashier) => {
    try {
      await loading(dispatch, async () => {
        const cashierTotal = await CashierTotalRepository.fetchSalesAggregateCashierTotal(aggregateType, fromDate, toDate, cashier)
        if (cashierTotal.total_data.length === 0) {
          dispatch(actions.setReceipt({}))
        } else {
          const builder = new CashierTotalReceiptBuilder()
          await builder.initializeAsync()
          const receipt = builder.buildCashierTotalReceipt(cashierTotal, new Date(),
            RECEIPT_TYPE.TERM_TOTAL_SALES, fromDate, toDate)
          dispatch(actions.setReceipt(receipt))
          const eJournal = await saveAggregateEJournal(dispatch, receipt, cashierTotal)
          dispatch(actions.setCashierTotalAggregateEJournal(eJournal))
        }
      })
    } catch (error) {
      dispatch(actions.setReceipt(null))

      if (error.errorType === 'InvalidOperation') {
        AlertView.show('不正な操作です。\n\n' + error.message)
      } else {
        AlertView.show(getCommonNetworkErrorMessage(error, I18n.t('message.G-03-E004')))
      }
    }
  },
  onPrintAggregateReceipt: async (receipt, eJournal) => {
    if (receipt) {
      try {
        await loading(dispatch, async () => {
          await PrinterManager.print(receipt)
        })
      } catch (error) {
        await EJournalRepository.saveIsNotPrintedById(eJournal.id)
        AlertView.show(I18n.t('message.G-02-E001'), () => true)
      }
    }
  }
})

const mapStateToProps = state => ({
  cashiers: state[MODULE_NAME].cashiers,
  receipt: state[MODULE_NAME].receipt,
  canPrintReceipt: _canPrintReceipt(state),
  cashierTotalAggregateEJournal: state[MODULE_NAME].cashierTotalAggregateEJournal
})
const _canPrintReceipt = (state) => {
  return state[MODULE_NAME].receiptText !== null && state[MODULE_NAME].receiptText !== ''
}
export default connect(mapStateToProps, mapDispatchToProps)(SalesAggregateForm)
