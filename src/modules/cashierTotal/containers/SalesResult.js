import { connect } from 'react-redux'
import SalesResult from '../components/SalesResult'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import { CASHIER_RECORD_MODE } from '../models'

const mapDispatchToProps = dispatch => ({
  onSetDepositAmount: async (amount, totalAmount) => {
    dispatch(actions.setDepositAmount(amount))
    dispatch(actions.setCarryForwardAmount(totalAmount - amount))
  },
  onSetCarryForwardAmount: async (amount, totalAmount) => {
    dispatch(actions.setCarryForwardAmount(amount))
    dispatch(actions.setDepositAmount(totalAmount - amount))
  }, 
  onSetWithdrawalAmount: async (amount, totalAmount) => {
    dispatch(actions.setWithdrawalAmount(amount))
  } // 2019.06  中間回収額追加
})

const mapStateToProps = state => ({
  totalAmount: state[MODULE_NAME].cashierRecord.total_amount,
  depositAmount: state[MODULE_NAME].cashierRecord.deposit_amount,
  carryForwardAmount: state[MODULE_NAME].cashierRecord.carry_forward_amount,
  accountAmount: state[MODULE_NAME].cashierRecord.account_amount,
  cashierRecordData: state[MODULE_NAME].cashierRecordData,
  withdrawalAmountSummary:state[MODULE_NAME].cashierRecord.withdrawal_amount_summary, // カスタマイズ-WBS-124
  diffAmount: _getDiffAmount(state),
  withdrawalAmount: state[MODULE_NAME].cashierRecord.withdrawal_amount,  // 2019.06  中間回収額追加
  sumAmount: state[MODULE_NAME].cashierRecord.sum_amount
})

const _getDiffAmount = (state) => {
  const cashierRecord = state[MODULE_NAME].cashierRecord
  return state[MODULE_NAME].cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES ? (
    cashierRecord.totalAmount - cashierRecord.carryForwardAmount
  ) : (
    state[MODULE_NAME].cashierRecordMode === CASHIER_RECORD_MODE.CHECK_SALES ? (
      cashierRecord.withdrawal_amount + cashierRecord.totalAmount - cashierRecord.accountAmount //2019.6　中間回収額追加
    ) :  (
      cashierRecord.withdrawal_amount_summary + cashierRecord.totalAmount - cashierRecord.accountAmount
    )
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(SalesResult)
