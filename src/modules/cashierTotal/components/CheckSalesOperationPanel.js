'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton } from 'common/components/elements/StandardButton'
import PropTypes from 'prop-types'
import styles from '../styles/CheckSales'

export default class CheckSalesOperationPanel extends Component {
  static propTypes = {
    receipt: PropTypes.object,
    onRerun: PropTypes.func,
    onPrint: PropTypes.func
  }
  render () {
    return (
      <View style={styles.CheckSalesOperationContainer}>
        <View style={styles.cashierTotalButtonView}>
          <CommandButton
            style={styles.cashierTotalButton}
            text={I18n.t('check_sale.print_check_sales_receipt')}
            disabled={!this.props.receipt}
            onPress={() => this.props.onPrint(this.props.receipt, this.props.eJournal)} />
        </View>
        <View style={styles.cashierTotalButtonView}>
          <OptionalCommandButton
            style={styles.cashierTotalButton}
            text={I18n.t('check_sale.rerun')}
            onPress={() => this.props.onRerun()} />
        </View>
      </View>
    )
  }
}
