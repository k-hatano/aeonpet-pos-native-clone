'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton } from 'common/components/elements/StandardButton'
import PropTypes from 'prop-types'
import style from '../styles/SalesAggregateForm'

export default class SalesAggregateOperationPanel extends Component {
  static propTypes = {
    cashierTotal: PropTypes.object,
    onPrintAggregateReceipt: PropTypes.func
  }

  render () {
    return (
      <View style={style.SalesAggregateOperationContainer}>
        <CommandButton
          style={style.cashierTotalButton}
          text={I18n.t('check_sale.print_check_sales_receipt')}
          onPress={() => this.props.onPrintAggregateReceipt(this.props.cashierTotal)} />
      </View>
    )
  }
}
