'use strict'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import NumberBox from 'common/components/elements/NumberBox'
import I18n from 'i18n-js'
import styles from '../styles/SalesResult'
import PropTypes from 'prop-types'
import { CommandButton } from 'common/components/elements/StandardButton'
import { formatPriceWithCurrency, formatNumber } from 'common/utils/formats'
import { CASHIER_RECORD_MODE } from '../models'

export default class SalesResult extends Component {
  static propTypes = {
    cashierRecord: PropTypes.object,
    cashierRecordData: PropTypes.object,
    cashierRecordMode: PropTypes.number,
    onComplete: PropTypes.func,
    onSetDepositAmount: PropTypes.func,
    onSetCarryForwardAmount: PropTypes.func,
    onSetWithdrawalAmount: PropTypes.func // 2019.06  中間回収額追加
  }

  render () {
    const { totalAmount, carryForwardAmount, withdrawalAmount, accountAmount, currency, cashierRecordMode, withdrawalAmountSummary, sumAmount } = this.props // カスタマイズ-WBS-124
    return (
      <View style={styles.salesResultContainer}>
        <View style={styles.totalCashLeftPanel}>
          <Text style={styles.headerText}>
            {I18n.t('check_sale.input_cash')}
          </Text>
        </View>
        <View style={styles.totalCashRightPanel}>
          <Text style={styles.totalCashText}>
            {formatPriceWithCurrency(sumAmount, currency)}
          </Text>
        </View>
        <View style={styles.lineView} />
        <View style={styles.totalCashRightBottomPanel}>

          {/* 2019.06  中間回収額追加 start */}
          {cashierRecordMode === CASHIER_RECORD_MODE.CHECK_SALES &&
            <View style={styles.amountCashRow}>
              <Text style={styles.amountCashCol}>
                {I18n.t('check_sale.withdrawal_amount')}
              </Text>
              <View style={styles.numberPadContainer}>
                <NumberBox
                  style={styles.salesButton}
                  onChange={amount => this.props.onSetWithdrawalAmount(amount, withdrawalAmount)}
                  amount={this.props.withdrawalAmount}
                  formatAmount={amount => (formatNumber(amount))}
                  //maxAmount={totalAmount}
                />
                <Text style={styles.amountCashCol}>
                  {I18n.t('common.japanese_yen')}
                </Text>
              </View>
            </View>
          }
          {/* 2019.06  中間回収額追加 end */}
          {/* カスタマイズ-WBS-124 */}
          {cashierRecordMode === CASHIER_RECORD_MODE.CLOSE_SALES &&
            <View style={styles.amountCashRow}>
              <Text style={styles.amountCashCol}>
                {I18n.t('check_sale.withdrawal_amount_summary')}
              </Text>
              <Text style={styles.totalCashCol}>
              {formatPriceWithCurrency(withdrawalAmountSummary, currency)}
              </Text>
            </View>
          }
          {/* カスタマイズ-WBS-124 */}

          <View style={styles.amountCashRow}>
            <Text style={styles.amountCashCol}>
              {cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES
                ? I18n.t('check_sale.change_fund_reserve') : I18n.t('check_sale.total_amount')}
            </Text>
            <Text style={styles.totalCashCol}>
              {formatPriceWithCurrency(totalAmount, currency)}
            </Text>
          </View>
          <View style={styles.amountCashRow}>
            <Text style={styles.amountCashCol}>
              {cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES ? 
                I18n.t('check_sale.carry_forward_amount') : I18n.t('check_sale.account_amount')}
            </Text>
            <Text style={styles.totalCashCol}>
              {cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES ? 
                formatPriceWithCurrency(carryForwardAmount, currency) : formatPriceWithCurrency(accountAmount, currency)}
            </Text>
          </View>
          <View style={styles.amountCashRow}>
            <Text style={styles.amountCashCol}>
              {cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES
                ? I18n.t('check_sale.diff_amount') : I18n.t('check_sale.deficiency_amount')}
            </Text>
            <Text style={styles.totalCashCol}>

              {/* 2019.06  中間回収額追加 start */}              
              {
                cashierRecordMode === CASHIER_RECORD_MODE.OPEN_SALES ? (
                  formatPriceWithCurrency(totalAmount - carryForwardAmount, currency)
                ) : (
                  cashierRecordMode === CASHIER_RECORD_MODE.CHECK_SALES ? (
                      formatPriceWithCurrency(this.props.withdrawalAmount + totalAmount - accountAmount, currency) //2019.6　中間回収額追加
                  ) :  (
                      formatPriceWithCurrency(totalAmount - accountAmount, currency)
                  )
                )
              }
              {/* 2019.06  中間回収額追加 end */}

            </Text>
          </View>
          {cashierRecordMode === CASHIER_RECORD_MODE.CLOSE_SALES &&
            <View style={styles.amountCashRow}>
              <Text style={styles.amountCashCol}>
                {I18n.t('check_sale.deposit_amount')}
              </Text>
              <View style={styles.numberPadContainer}>
                <NumberBox
                  style={styles.salesButton}
                  // カスタマイズ-WBS-124
                  onChange={amount => this.props.onSetDepositAmount(amount, totalAmount + withdrawalAmountSummary)}
                  amount={this.props.depositAmount}
                  formatAmount={amount => (formatNumber(amount))}
                   // カスタマイズ-WBS-124
                  maxAmount={totalAmount + withdrawalAmountSummary}/>
                <Text style={styles.amountCashCol}>
                  {I18n.t('common.japanese_yen')}
                </Text>
              </View>
            </View>
          }
          {cashierRecordMode === CASHIER_RECORD_MODE.CLOSE_SALES &&
            <View style={styles.amountCashRow}>
              <Text style={styles.amountCashCol}>
                {I18n.t('check_sale.carry_forward_amount')}
              </Text>
              <View style={styles.numberPadContainer}>
                <NumberBox
                  style={styles.salesButton}
                  // カスタマイズ-WBS-124
                  onChange={amount => this.props.onSetCarryForwardAmount(amount, totalAmount + withdrawalAmountSummary)}
                  amount={carryForwardAmount}
                  formatAmount={amount => (formatNumber(amount))}
                  // カスタマイズ-WBS-124
                  maxAmount={totalAmount + withdrawalAmountSummary} />
                <Text style={styles.amountCashCol}>
                  {I18n.t('common.japanese_yen')}
                </Text>
              </View>
            </View>
          }
        </View>
        <View style={[
          styles.registerButtonContainer,
          cashierRecordMode === CASHIER_RECORD_MODE.CLOSE_SALES ? {marginBottom: 32} : {marginBottom: 152}]}>
          <CommandButton
            style={styles.registerButton}
            text={I18n.t('check_sale.register')}
            onPress={() => this.props.onComplete(this.props.cashierRecordData)} />
        </View>
      </View>
    )
  }
}
