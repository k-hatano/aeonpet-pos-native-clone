'use strict'

import React, { Component } from 'react'
import { Text, ScrollView, View } from 'react-native'
import { Col, Row } from 'react-native-easy-grid'

function HeaderAggregate (props) {
  const { topItems, title, bottomItems } = props
  return (
    <View style={{ padding: 10 }}>
      {topItems.map((item, index) => <Text style={{ textAlign: 'left' }} key={index}>{item}</Text>)}
      <Text style={{ textAlign: 'center' }} key={title}>{title}</Text>
      {bottomItems.map((item, index) =>
        <Text style={{ textAlign: 'right' }} key={index}>
          {`${item.label}:   ${item.text}`}
        </Text>
      )}
    </View>
  )
}

function DataAggregate (props) {
  const { title, dataItems } = props
  return (
    <View style={{ paddingTop: 10 }}>
      <Text style={{ paddingLeft: 5 }} key={title}>{title}</Text>
      {dataItems.map((item, index) =>
        <View style={{ flexDirection: 'row' }} key={index}>
          <View style={{ flex: 1 }}>
            <Text style={{ textAlign: 'left' }}>{item.title}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ textAlign: 'right' }}>{item.value}</Text>
          </View>
        </View>
      )}
    </View>
  )
}

function FooterAggregate (props) {
  const { data } = props
  return (
    <View style={{ padding: 10 }}>
      <Text style={{ textAlign: 'right' }}>{`${data.title}:   ${data.total}`}</Text>
    </View>
  )
}

export default class SalesAggregate extends Component {
  render () {
    const {
      componentStyles,
      salesAggregate: { headerTitle, headerTopItems, headerBottomItems, dataAggregate, footerData }
    } = this.props
    return (
      <Col style={componentStyles.salesAgreegateContainer}>
        <Row style={componentStyles.salesAggregateForm}>
          <ScrollView style={{ backgroundColor: '#fff', width: 250 }}>
            <HeaderAggregate title={headerTitle} topItems={headerTopItems} bottomItems={headerBottomItems} />
            <View style={{ padding: 10, paddingTop: 0 }}>
              {dataAggregate.map((itemAggregate, index) =>
                <DataAggregate title={itemAggregate.title} dataItems={itemAggregate.data} key={index} />
              )}
            </View>
            <FooterAggregate data={footerData} />
          </ScrollView>
        </Row>
      </Col>
    )
  }
}
