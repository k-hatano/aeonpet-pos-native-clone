'use strict'
import I18n from 'i18n-js'
import React, { Component } from 'react'
import { View } from 'react-native'
import { Col, Row } from 'react-native-easy-grid'
import TabPanel from 'common/components/widgets/TabPanel'
import ReceiptView from 'modules/printer/components/ReceiptView'
import CheckSalesOperationPanel from './CheckSalesOperationPanel'
import { AGGREGATE_TYPE } from '../models'
import PropTypes from 'prop-types'
import styles from '../styles/CheckSales'

export default class CheckSalesView extends Component {
  static propTypes = {
    receipt: PropTypes.object,
    receiptText: PropTypes.string,
    onInitialize: PropTypes.func,
    onStartCashierRecord: PropTypes.func,
    onSelectAggregateType: PropTypes.func,
    onPrint: PropTypes.func,
    onClose: PropTypes.func
  }
  componentWillMount () {
    this.props.onInitialize(this.props.cashierRecord)
  }
  componentWillUnmount () {
    this.props.onClose()
  }
  _renderCheckSales (aggregateType) {
    const eJournal = aggregateType === AGGREGATE_TYPE.POS ? this.props.cashierTotalPosEJournal : this.props.cashierTotalShopEJournal
    return (
      <View style={[styles.CheckSalesContainer, {height: '100%'}]}>
        <Row>
          <Col>
            <ReceiptView receiptDataText={this.props.receiptText} />
          </Col>
          <Col>
            <CheckSalesOperationPanel
              receipt={this.props.receipt}
              onRerun={this.props.onStartCashierRecord}
              onPrint={this.props.onPrint}
              eJournal={eJournal} />
          </Col>
        </Row>
      </View>
    )
  }
  async _onSelectTab (aggregateType) {
    await this.props.onSelectAggregateType(aggregateType)
  }

  render () {
    return (
      <Col style={styles.TabContainer}>
        <TabPanel tabWrapperStyle={styles.tabWrapperStyle} tabStyle={styles.tabStyle}>
          <View
            tabName={I18n.t('check_sale.pos')}
            onTabClick={() => this._onSelectTab(AGGREGATE_TYPE.POS)}>
            {this._renderCheckSales(AGGREGATE_TYPE.POS)}
          </View>
          <View
            tabName={I18n.t('check_sale.shop')}
            onTabClick={() => this._onSelectTab(AGGREGATE_TYPE.SHOP)}>
            {this._renderCheckSales(AGGREGATE_TYPE.SHOP)}
          </View>
        </TabPanel>
      </Col>
    )
  }
}
