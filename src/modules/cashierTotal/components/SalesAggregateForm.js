'use strict'
import React, { Component } from 'react'
import { View, TextInput, TouchableOpacity, Text } from 'react-native'
import Select from 'common/components/elements/Select'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton } from 'common/components/elements/StandardButton'
import style from '../styles/SalesAggregateForm'
import moment from 'moment'
import { AGGREGATE_TYPE } from '../models'
import DateTimePicker from 'react-native-modal-datetime-picker'

function RowLayout (props) {
  return (
    <View style={[style.sampleRowContainer, props.style && props.style]}>
      <View style={style.sampleRowTitle}>
        {props.title && <Text style={style.commonText}>{props.title}</Text>}
      </View>
      <View style={style.sampleRowChildContainer}>
        {props.children}
      </View>
    </View>
  )
}

export default class SalesAggregateForm extends Component {
  _MINIMUM_DATE = new Date('1900-01-01')
  _MAXIMUM_DATE = new Date()
  constructor (props) {
    super(props)
    const today = new Date()
    this._MAXIMUM_DATE.setDate(today.getDate() + 1)
    this.state = {
      fromDate: today,
      toDate: today,
      minimumDate: this._MINIMUM_DATE,
      maximumDate: this._MAXIMUM_DATE,
      selectedCashier: null,
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }
  _showDateTimePicker (title, date, key) {
    let minimumDate = null
    let maximumDate = null
    if (key === 'fromDate') {
      minimumDate = this._MINIMUM_DATE
      maximumDate = this.state.toDate
    } else if (key === 'toDate') {
      minimumDate = this.state.fromDate
      maximumDate = this._MAXIMUM_DATE
    }
    this.setState({
      isDatePickerVisible: true,
      minimumDate: minimumDate,
      maximumDate: maximumDate,
      date: date,
      datePickerTitle: title,
      dateKey: key
    })
  }
  _hideDateTimePicker () { this.setState({ isDatePickerVisible: false }) }
  _handleDatePicked (date) {
    if (this.state.dateKey === 'fromDate') {
      this.setState({
        isDatePickerVisible: false,
        fromDate: date
      })
    } else if (this.state.dateKey === 'toDate') {
      this.setState({
        isDatePickerVisible: false,
        toDate: date
      })
    }
  }
  render () {
    let aggregateType = AGGREGATE_TYPE.POS
    const isDisabled = this.state.selectedCashier === null
    return (
      <View style={style.SearchAggregateFormContainer}>
        <RowLayout title={I18n.t('check_sale.business_day')}>
          <View style={style.dateWrapper}>
            <View style={style.inputContainer}>
              <TouchableOpacity
                style={style.touchableContainer}
                onPress={() => {
                  this._showDateTimePicker(
                    I18n.t('check_sale.business_day'),
                    this.state.fromDate,
                    'fromDate')
                }}>
                <TextInput
                  editable={false}
                  style={style.searchInput}
                  value={moment(this.state.fromDate).format('YYYY-MM-DD')} />
              </TouchableOpacity>
            </View>
            <View style={style.dateTextWrapper}>
              <Text style={style.commonText}>{I18n.t('common.from')}</Text>
            </View>
          </View>
        </RowLayout>
        <RowLayout style={style.rowLayout}>
          <View style={style.dateWrapper}>
            <View style={style.inputContainer}>
              <TouchableOpacity
                style={style.touchableContainer}
                onPress={() => {
                  this._showDateTimePicker(
                    I18n.t('check_sale.business_day'),
                    this.state.toDate,
                    'toDate')
                }}>
                <TextInput
                  editable={false}
                  style={style.searchInput}
                  value={moment(this.state.toDate).format('YYYY-MM-DD')} />
              </TouchableOpacity>
            </View>
            <View style={style.dateTextWrapper}>
              <Text style={style.commonText}>{I18n.t('common.until')}</Text>
            </View>
          </View>
        </RowLayout>
        <RowLayout title={I18n.t('check_sale.aggregation_target')}>
          <Select
            style={{width: 358}}
            selectStyle={{backgroundColor: '#f0f0f0'}}
            items={this.props.cashiers}
            selectedItem={this.state.selectedCashier}
            displayProperty='name'
            onItemChange={item => {
              const selectedItem = item.id !== undefined ? item : null
              this.setState({selectedCashier: selectedItem})
            }}
            placeholder={I18n.t('common.select_one')} />
        </RowLayout>
        <View style={style.buttonContainer}>
          <CommandButton
            style={style.cashierTotalButton}
            text={I18n.t('check_sale.search_aggregate')}
            onPress={() => {
              if (this.state.selectedCashier.id === null) {
                aggregateType = AGGREGATE_TYPE.SHOP
              } else {
                aggregateType = AGGREGATE_TYPE.POS
              }
              this.props.onSearch(aggregateType,
                moment(this.state.fromDate).format('YYYY/MM/DD'),
                moment(this.state.toDate).format('YYYY/MM/DD'), this.state.selectedCashier)
            }}
            disabled={isDisabled}/>
          <OptionalCommandButton
            style={style.cashierTotalButton}
            text={I18n.t('check_sale.print_aggregate')}
            onPress={() => this.props.onPrintAggregateReceipt(this.props.receipt, this.props.cashierTotalAggregateEJournal)}
            disabled={!this.props.canPrintReceipt}
          />
        </View>
        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          mode='date'
          date={this.state.date}
          titleIOS={this.state.datePickerTitle}
          titleStyle={style.datePickerTitle}
          confirmTextIOS={I18n.t('common.ok')}
          confirmTextStyle={style.datePickerConfirm}
          cancelTextIOS={I18n.t('common.cancel')}
          cancelTextStyle={style.datePickerCancel}
          minimumDate={this.state.minimumDate}
          maximumDate={this.state.maximumDate}
          onConfirm={(date) => { this._handleDatePicked(date) }}
          onCancel={() => { this._hideDateTimePicker() }}
          neverDisableConfirmIOS />
      </View>
    )
  }
}
