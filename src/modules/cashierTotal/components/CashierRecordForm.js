import React, { Component } from 'react'
import { View } from 'react-native'
import SalesResult from '../containers/SalesResult'
import SalesControl from '../containers/SalesControl'
import styles from '../styles/CashierRecordForm'
import { getCashierMenuList } from '../models'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import { getTotalAmount } from '../services'
import AlertView from '../../../common/components/widgets/AlertView'

export default class CashierRecordForm extends Component {
  static propTypes = {
    cashierRecord: PropTypes.object,
    cashierRecordData: PropTypes.object,
    cashierRecordMode: PropTypes.number,
    onChangedCashierRecordData: PropTypes.func,
    onCompleteConfirm: PropTypes.func,
    onCashierRecordUpdated: PropTypes.func,
    isOpening: PropTypes.bool,
    onInit: PropTypes.func
  }

  componentWillMount () {
    this.cashierMenu = getCashierMenuList()
    this.props.onInit()
  }
  async _onChange (key, value) {
    const cashierRecordData = {...this.props.cashierRecordData}
    cashierRecordData[key] = value
    const totalAmount = getTotalAmount(cashierRecordData)

    if (totalAmount > 99999999) {
      await AlertView.showAsync(I18n.t('message.G-03-E002'))
    } else if (value > 999) {
      await AlertView.showAsync(I18n.t('message.G-03-E001'))
    } else {
      await this.props.onChangedCashierRecordData(key, value, this.props.cashierRecordMode, this.props.cashierRecord.withdrawal_amount_summary) // カスタマイズ-WBS-124
    }
  }
  render () {
    return (
      <View style={styles.cashierRecordFormContainer}>
        <SalesControl
          menus={this.cashierMenu}
          onChange={(key, value) => this._onChange(key, value)}
          onClear={() => this.props.onClear(this.props.cashierRecordMode)}
          isOpening={this.props.isOpening}
          onSkip={() => this.props.onSkip(this.props.cashierRecord, this.props.onCashierRecordUpdated)}
        />
        <SalesResult
          cashierRecordMode={this.props.cashierRecordMode}
          onComplete={
            (cashierRecordData) => this.props.onComplete(
              this.props.cashierRecord, cashierRecordData, this.props.onCompleteConfirm, this.props.onCashierRecordUpdated, this.props.isOpening, this.props.cashierRecordMode
            )
          }
        />
      </View>
    )
  }
}
