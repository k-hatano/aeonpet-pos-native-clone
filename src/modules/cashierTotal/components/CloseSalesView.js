'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import ReceiptView from 'modules/printer/components/ReceiptView'
import CloseSalesOperationPanel from 'modules/cashierTotal/components/CloseSalesOperationPanel'
import PropTypes from 'prop-types'
import styles from '../styles/CloseSales'

export default class CloseSalesView extends Component {
  static propTypes = {
    receipt: PropTypes.object,
    receiptText: PropTypes.string,
    receiptPrintNum: PropTypes.number,
    onInitialize: PropTypes.func,
    onStartCashierRecord: PropTypes.func,
    onPrint: PropTypes.func,
    onClose: PropTypes.func
  }

  componentWillMount () {
    this.props.onInitialize(this.props.cashierRecord)
  }
  componentWillUnmount () {
    this.props.onClose()
  }

  render () {
    return (
      <View style={styles.receiptViewContainer}>
        <View style={styles.receiptViewContainer}>
          <ReceiptView receiptDataText={this.props.receiptText} />
        </View>
        <View style={styles.OperationPanelContainer}>
          <CloseSalesOperationPanel
            receipt={this.props.receipt}
            onRerun={this.props.onStartCashierRecord}
            onPrint={this.props.onPrint}
            onCloseSales={() => this.props.onCloseSales(this.props.receiptPrintNum)}
            eJournal={this.props.cashierTotalCloseSalesEJournal} />
        </View>
      </View>
    )
  }
}
