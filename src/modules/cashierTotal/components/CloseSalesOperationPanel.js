'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton, OptionalCommandButton, ProceedButton } from 'common/components/elements/StandardButton'
import PropTypes from 'prop-types'
import styles from '../styles/CloseSales'

export default class CloseSalesOperationPanel extends Component {
  static propTypes = {
    receipt: PropTypes.object,
    onRerun: PropTypes.func,
    onPrint: PropTypes.func,
    onCloseSales: PropTypes.func
  }
  render () {
    return (
      <View style={styles.CloseSalesOperationContainer}>
        <View style={[styles.cashierTotalButtonView, styles.receiptButton]}>
          <CommandButton
            style={styles.cashierTotalButton}
            text={I18n.t('check_sale.print_check_sales_receipt')}
            onPress={() => this.props.onPrint(this.props.receipt, this.props.eJournal)} />
        </View>
        <View style={styles.cashierTotalButtonView}>
          <OptionalCommandButton
            style={styles.cashierTotalButton}
            text={I18n.t('check_sale.rerun')}
            onPress={() => this.props.onRerun()} />
        </View>
        <View style={[styles.cashierTotalButtonView, styles.closeSalesButton]}>
          <ProceedButton
            style={styles.cashierTotalButton}
            text={I18n.t('check_sale.close_sales')}
            onPress={() => this.props.onCloseSales()} />
        </View>
      </View>
    )
  }
}
