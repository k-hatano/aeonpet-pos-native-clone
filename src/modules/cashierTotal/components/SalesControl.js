'use strict'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/SalesControl'
import NumberBox from 'common/components/elements/NumberBox'
import PropTypes from 'prop-types'
import { AMOUNT_PAD_MODE } from 'common/components/elements/AmountPad'
import { ProceedButton, OptionalCommandButton } from 'common/components/elements/StandardButton'

export default class SalesControl extends Component {
  static propTypes = {
    menus: PropTypes.object,
    cashierRecordData: PropTypes.object,
    onChange: PropTypes.func,
    onClear: PropTypes.func,
    isOpening: PropTypes.bool,
    onSkip: PropTypes.func
  }

  renderTopMenuItem (title, key, onChange) {
    return (
      <View key={key} style={styles.topMenuItemContainer}>
        <View style={styles.menuItemPanelTitle}>
          <Text style={styles.menuTitle}>
            {title}
          </Text>
        </View>
        <View style={styles.menuItemPanelRow}>
          <View style={styles.numberBoxContainer}>
            <NumberBox
              style={styles.salesButton}
              onChange={amount => onChange(key, amount)}
              amount={this.props.cashierRecordData[key] || 0}
              formatAmount={amount => amount}
              mode={AMOUNT_PAD_MODE.NUMBER}
              disableCounterSuffix={true}
            />
          </View>
          <View style={styles.counterSuffixContainer}>
            <Text style={styles.menuTitle}>
              {I18n.t('common.pieces')}
            </Text>
          </View>
        </View>
        <View style={styles.menuItemPanelTitle} />
      </View>
    )
  }

  renderBottomMenuItem (title, key, onChange) {
    return (
      <View key={key} style={styles.bottomMenuItemContainer}>
        <View style={styles.menuItemPanelTitle}>
          <Text style={styles.menuTitle}>
            {title}
          </Text>
        </View>
        <View style={styles.menuItemPanelRow}>
          <View style={styles.numberBoxContainer}>
            <NumberBox
              style={styles.salesButton}
              onChange={amount => onChange(`coin_${key}`, amount)}
              amount={this.props.cashierRecordData[`coin_${key}`] || 0}
              formatAmount={amount => amount}
              mode={AMOUNT_PAD_MODE.NUMBER}
              disableCounterSuffix={true}
            />
          </View>
          <View style={styles.counterSuffixContainer}>
            <Text style={styles.menuTitle}>
              {I18n.t('common.pieces')}
            </Text>
          </View>
        </View>

        <View style={styles.menuItemPanelRow}>
          <View style={styles.numberBoxContainer}>
            <NumberBox
              style={styles.salesButton}
              onChange={amount => onChange(`coinbar_${key}`, amount)}
              amount={this.props.cashierRecordData[`coinbar_${key}`] || 0}
              formatAmount={amount => amount}
              mode={AMOUNT_PAD_MODE.NUMBER}
              disableCounterSuffix={true}
            />
          </View>
          <View style={styles.counterSuffixContainer}>
            <Text style={styles.menuTitle}>
              {I18n.t('common.bar')}
            </Text>
          </View>
        </View>
      </View>
    )
  }

  render () {
    return (
      <View style={styles.salesControlMain}>
        <View style={{ height: 70 + this.props.menus.bill.length * 74, flexDirection: 'row'}}>
          <View style={styles.panelContainer}>
            <View style={styles.headerContainer} />
            {this.props.menus.bill.map(item =>
              this.renderTopMenuItem(item.title, item.key, this.props.onChange)
            )}
            <View style={styles.separatorContainer}>
              <View style={styles.separator} />
            </View>
          </View>
        </View>
        <View style={styles.coinPanelArea}>
          <View style={styles.panelContainer}>
            <View style={styles.headerContainer}>
              <View style={styles.menuItemPanelTitle} />
              <View style={styles.menuItemPanelText}>
                <Text style={styles.menuHeader}>
                  {I18n.t('check_sale.coin_count')}
                </Text>
              </View>
              <View style={styles.menuItemPanelText}>
                <Text style={styles.menuHeader}>
                  {I18n.t('check_sale.coin_bar_count')}
                </Text>
              </View>
            </View>
            {this.props.menus.coin.map(item =>
              this.renderBottomMenuItem(item.title, item.key, this.props.onChange)
            )}
          </View>
        </View>
        <View
          style={styles.clearButtonContainer}>
          {!this.props.isOpening &&
            <OptionalCommandButton
              style={styles.skipButton}
              text={I18n.t('common.skip')}
              onPress={() => this.props.onSkip()} />
          }
          <ProceedButton
            style={styles.clearButton}
            text={I18n.t('common.clear')}
            onPress={() => this.props.onClear()} />
        </View>
      </View>
    )
  }
}
