/**
 * サーバー送信用の在高情報を生成します。
 * @param cashierRecord
 * @param cashierRecordData
 * @param cashier
 * @param cashierCashRecordMode
 * @returns {{cashier_id, cash_record_data, deposit_amount: number, cashier_cash_record_mode: *, client_created_at: *}}
 */
import { NativeEventEmitter, NativeModules } from 'react-native'
import I18n from 'i18n-js'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { getSettingFromState } from 'modules/setting/models'
import { AGGREGATE_TYPE, MODULE_NAME, TOTAL_MODE } from './models'
import { CASHIER_CASH_RECORD_MODE } from 'modules/cashier/models'
import { toUnixTimestamp } from 'common/utils/dateTime'
import { makeBaseEJournal, RECEIPT_TYPE } from 'modules/printer/models'
import { CASHIER_RECORD_PRICE_LIST } from 'modules/cashierTotal/models'
import { loading, loadStart, loadEnd } from 'common/sideEffects'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import CashierCashRecordRepository from 'modules/cashier/repositories/CashierCashRecordRepository'
import CashierBalanceRepository from 'modules/balance/repositories/CashierBalanceRepository'
import CashierRecordReceiptBuilder from 'modules/cashierTotal/models/CashierRecordReceiptBuilder'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import BalanceReasonRepository from 'modules/balance/repositories/BalanceReasonRepository'
import PaymentMethodRepository from 'modules/payment/repositories/PaymentMethodRepository'
import PrinterManager from 'modules/printer/models/PrinterManager'
import AlertView from 'common/components/widgets/AlertView'
import logger from 'common/utils/logger'
import generateUuid from '../../common/utils/generateUuid'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import CashierTotalRepository from './repositories/CashierTotalRepository'
import CashierTotalReceiptBuilder from './models/CashierTotalReceiptBuilder'
import { OPERATION_TYPE } from '../home/models'
import { saveOperationLog } from '../home/service'
import { makeReprintEJournal } from '../order/models'
import ReprintOrderReceiptBuilder from '../order/models/ReprintOrderReceiptBuilder'
import * as actions from './actions'
import { PAYMENT_METHOD_TYPE } from '../payment/models'
import { cChangerErrorMessage } from 'modules/printer/models/CashChangerResult'

export const makeCashierRecordFromState = state => {
  const currentStaff = getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
  const cashierRecord = state[MODULE_NAME].cashierRecord
  return {
    ...cashierRecord,
    currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY),
    device_id: getSettingFromState(state, SettingKeys.COMMON.DEVICE_ID),
    staff_id: currentStaff ? currentStaff.id : null,
    staff_name: currentStaff ? currentStaff.name : null
  }
}

export const makeCashierRecordForPush = (cashierRecord, cashierRecordData, cashier, cashierCashRecordMode) => {
  return {
    ...cashierRecord,
    id: generateUuid(),
    cashier_id: cashier.id,
    cash_record_data: JSON.stringify(cashierRecordData),
    cashier_cash_record_mode: cashierCashRecordMode,
    client_created_at: toUnixTimestamp(new Date())
  }
}

//2019.6.28 入出金処理 start -> 臨時データ
export const makeBalanceForBulkPush = async (dispatch, cashierRecord, cashierRecordData, cashier) => {

  const balanceName = '中間回収'
  const balanceReason = await BalanceReasonRepository.findByBalanceName(balanceName)
  const paymentMethod = await PaymentMethodRepository.findByPaymentMethodType(PAYMENT_METHOD_TYPE.CASH)

  return  [{
    amount: cashierRecord.withdrawal_amount,
    payment_method_id: paymentMethod[0].id,
    payment_method_type: paymentMethod[0].payment_method_type,
    payment_method_name: paymentMethod[0].name,
    currency: cashierRecord.currency,
    balance_type: balanceReason.balance_type,
    cashier_id: cashier.id,
    staff_id: cashierRecord.staff_id,
    staff_name: cashierRecord.staff_name,
    balance_reason_id: balanceReason.id,
    balance_reason_name: balanceReason.name,
    order_id: null,
    pos_order_number: null,
    client_created_at: toUnixTimestamp(new Date())
  }]
}
//2019.6.28 入出金処理 end

/**
 *
 * @param receipt
 * @param cashierRecord
 * @param createdAt
 * @returns {{id, data, data_text, receipt_title, receipt_type, cashier_id, is_printed, is_pushed, is_reprint, client_created_at}}
 */
export const makeCashierRecordEJournal = (receipt, cashierRecord, createdAt) => {
  const receiptTitle = I18n.t('receipt.title.cashierRecord')
  const eJournal = makeBaseEJournal(receiptTitle, receipt, RECEIPT_TYPE.CASH_RECORD, createdAt)
  eJournal.device_id = cashierRecord.device_id
  eJournal.shop_id = cashierRecord.shop_id
  eJournal.staff_id = cashierRecord.staff_id
  eJournal.currency = cashierRecord.currency
  eJournal.amount = cashierRecord.total_amount
  eJournal.is_print_tax_stamp = false

  return eJournal
}

/**
 * 帳簿在高をサーバーから取得します。
 * @param dispatch
 * @returns {Promise}
 */
export const getAccountAmount = async (dispatch) => {
  return loading(dispatch, async () => {
    const cashier = await CashierRepository.find()
    const accountAmount = await CashierBalanceRepository.fetchAccountAmount(cashier.id)
    // 前回開局時在高情報の釣銭準備金も帳簿在高とする。
    const cashierRecord = await CashierCashRecordRepository.fetchLatestRecord(
      cashier.id, cashier.shop_id, CASHIER_CASH_RECORD_MODE.OPEN_SALE
    )
    if (cashierRecord == null) return parseInt(accountAmount.amount)

    return parseInt(accountAmount.amount) + parseInt(cashierRecord.total_amount)
  })
}

// カスタマイズ-WBS-124
/**
 * 中間回収額合計をサーバーから取得します。
 * @param dispatch
 * @returns {Promise}
 */
export const getWithdrawalAmountSummary = async (dispatch) => {
  return loading(dispatch, async () => {
    const cashier = await CashierRepository.find()
    const withdrawalAmountSummary = await CashierCashRecordRepository.fetchWithdrawalAmountSummary(cashier.id)

    return parseInt(withdrawalAmountSummary.summary)
  })
}
// カスタマイズ-WBS-124

/**
 * 在高情報をサーバーに送信します。
 * @param dispatch
 * @param cashierRecord
 * @param cashierRecordData
 * @returns {Promise.<void>}
 */
export const pushCashierCashRecord = async (dispatch, cashierRecord, cashierRecordData, totalMode) => {
  const builder = new CashierRecordReceiptBuilder()
  await builder.initializeAsync()
  const receipt = builder.buildCashierRecordReceipt(
    cashierRecord, cashierRecordData, new Date(), totalMode || TOTAL_MODE.CHECK)
  const eJournal = makeCashierRecordEJournal(
    receipt,
    {
      ...cashierRecord,
      ...cashierRecordData
    },
    toUnixTimestamp(new Date())
  )
  const result = await loading(dispatch, async () => {
    const messages = []
    let pushSuccess = false
    try {
      const cashier = await CashierRepository.find()
      const cashierRecordNew = await makeCashierRecordForPush(
        cashierRecord, cashierRecordData, cashier, CASHIER_CASH_RECORD_MODE.CLOSE_SALE
      )

      try {
        await CashierCashRecordRepository.push(cashierRecordNew)
        if (cashierRecord.withdrawal_amount && cashierRecord.withdrawal_amount>0){
          const balances = await makeBalanceForBulkPush(dispatch, cashierRecord, cashierRecordData, cashier)
          await CashierBalanceRepository.bulkPush(balances)
        }
        pushSuccess = true
      } catch (error) {
        if (!totalMode) {
          // 開局の時のみ保存する
          cashierRecordNew.is_pushed = false
          await CashierCashRecordRepository.save(cashierRecordNew)
        }
        throw new Error(getCommonNetworkErrorMessage(error, I18n.t('message.G-03-E006')))
      }
      await EJournalRepository.save(eJournal)
      try {
        await PrinterManager.print(receipt)
        // 印刷済み
        await EJournalRepository.saveIsPrintedById(eJournal.id)
      } catch (error) {
        // 印刷エラー時はエラー表示のみで、処理は続行する。
        messages.push(I18n.t('message.G-02-E001'))
      }
      try {
        await EJournalRepository.pushById(eJournal.id)
      } catch (error) {
        // 電子ジャーナル送信に失敗した場合は、エラー通知は行わない。
        logger.warning('checkSales: ejournals save error. ' + error.message)
      }
    } catch (error) {
      messages.push(error.message)
    }
    return {
      messages: messages,
      pushSuccess: pushSuccess
    }
  })
  for (const i in result.messages) {
    await AlertView.showAsync(result.messages[i])
  }
  return result.pushSuccess
}

/**
 *
 * @param receipt
 * @param cashierTotal
 * @param receiptType
 * @param createdAt
 * @returns {{id, data, data_text, receipt_title, receipt_type, cashier_id, is_printed, is_pushed, is_reprint, client_created_at}}
 */
export const makeCashierTotalEJournal = (receipt, cashierTotal, receiptType, createdAt) => {
  let type = 0
  let title
  switch (receiptType) {
    case RECEIPT_TYPE.CHECK_SALES:
      type = RECEIPT_TYPE.CHECK_SALES
      title = I18n.t('receipt.title.check_sales')
      break
    case RECEIPT_TYPE.CLOSE_SALES:
      type = RECEIPT_TYPE.CLOSE_SALES
      title = I18n.t('receipt.title.close_sales')
      break
    case RECEIPT_TYPE.TERM_TOTAL_SALES:
      type = RECEIPT_TYPE.TERM_TOTAL_SALES
      title = I18n.t('receipt.title.sales_aggregate')
      break
  }
  let eJournal = makeBaseEJournal(title, receipt, type, createdAt)
  eJournal.staff_id = cashierTotal.staff_id
  eJournal.is_print_tax_stamp = false
  eJournal.amount = 0 // TODO 実在高を設定する

  return eJournal
}

/**
 *
 * @param dispatch
 * @param receipt
 * @param cashierTotal
 * @param cashierTotalPosEJournal
 * @returns {Promise.<{id, data, data_text, receipt_title, receipt_type, cashier_id, is_printed, is_pushed, is_reprint, client_created_at}>}
 */
export const savePosEJournal = async (dispatch, receipt, cashierTotal, cashierTotalPosEJournal) => {
  const createdAt = toUnixTimestamp(new Date())
  const eJournal = makeCashierTotalEJournal(receipt, cashierTotal, RECEIPT_TYPE.CHECK_SALES, createdAt)

  await loading(dispatch, async () => {
    await EJournalRepository.save(eJournal)
    try {
      await EJournalRepository.pushById(eJournal.id)
      // 通信に成功したら印刷済みとする。実際に印刷はしてないけどね。
      await EJournalRepository.saveIsPrintedById(eJournal.id)
    } catch (error) {
      // エラーは握りつぶす
      logger.warning('checkSales - savePosEJournal error. ' + error.message)
    }
  })
  return eJournal
}

/**
 *
 * @param dispatch
 * @param receipt
 * @param cashierTotal
 * @param cashierTotalShopEJournal
 * @returns {Promise.<{id, data, data_text, receipt_title, receipt_type, cashier_id, is_printed, is_pushed, is_reprint, client_created_at}>}
 */
export const saveShopEJournal = async (dispatch, receipt, cashierTotal, cashierTotalShopEJournal) => {
  const createdAt = toUnixTimestamp(new Date())
  const eJournal = makeCashierTotalEJournal(receipt, cashierTotal, RECEIPT_TYPE.CHECK_SALES, createdAt)

  await loading(dispatch, async () => {
    await EJournalRepository.save(eJournal)
    try {
      await EJournalRepository.pushById(eJournal.id)
      // 通信に成功したら印刷済みとする。実際に印刷はしてないけどね。
      await EJournalRepository.saveIsPrintedById(eJournal.id)
    } catch (error) {
      // エラーは握りつぶす
      logger.warning('checkSales - saveShopEJournal error. ' + error.message)
    }
  })
  return eJournal
}

// 期間レシート
export const saveAggregateEJournal = async (dispatch, receipt, cashierTotal) => {
  const createdAt = toUnixTimestamp(new Date())
  const eJournal = makeCashierTotalEJournal(receipt, cashierTotal, RECEIPT_TYPE.TERM_TOTAL_SALES, createdAt)

  await loading(dispatch, async () => {
    await EJournalRepository.save(eJournal)
    try {
      await EJournalRepository.pushById(eJournal.id)
      // 通信に成功したら印刷済みとする。実際に印刷はしてないけどね。
      await EJournalRepository.saveIsPrintedById(eJournal.id)
    } catch (error) {
      // エラーは握りつぶす
      logger.warning('checkSales - saveAggregateEJournal error. ' + error.message)
    }
  })
  return eJournal
}

export const getTotalAmount = cashierRecord => {
  let amount = 0
  Object.keys(cashierRecord).map((key) => {
    amount += (CASHIER_RECORD_PRICE_LIST[key] || 0) * (parseInt(cashierRecord[key]) || 0)
  })
  return amount
}

export async function printTotalReceipt (createdAt, cashierTotal, aggregateType, receiptPrintNum, isReprint = false) {
  const builder = new CashierTotalReceiptBuilder()
  await builder.initializeAsync()
  let receipt = builder.buildCashierTotalReceipt(cashierTotal, createdAt, RECEIPT_TYPE.CLOSE_SALES)
  let eJournal
  if (isReprint) {
    const builder = new ReprintOrderReceiptBuilder()
    await builder.initializeAsync()
    receipt = builder.buildReprintOrderReceipt(receipt, true)
  }
  const messages = []
  // 印字枚数の数だけ繰り返す。
  for (let i = 0; i < receiptPrintNum; i++) {
    eJournal = makeCashierTotalEJournal(
      receipt, cashierTotal, RECEIPT_TYPE.CLOSE_SALES, toUnixTimestamp(createdAt)
    )
    if (isReprint) {
      eJournal = makeReprintEJournal(receipt, eJournal, new Date(), RECEIPT_TYPE.CLOSE_SALES)
    }

    try {
      await EJournalRepository.save(eJournal)
      await PrinterManager.print(receipt)
      // 印刷に成功した場合のみ、印刷済みとする。
      // 失敗したらエラーにして、処理を続行する。
      await EJournalRepository.saveIsPrintedById(eJournal.id)
    } catch (error) {
      const message = typeof error === 'string' ? error : error.message
      logger.warning('closeSales - print receipt error. ' + message)
      // エラーは通知しない
      messages.push(I18n.t('message.G-02-E001'))
    }

    try {
      await EJournalRepository.pushById(eJournal.id)
    } catch (error) {
      // 電子ジャーナルの送信に失敗してもエラーは通知しない。
      logger.warning('closeSales - push eJournal error. ' + error.message)
    }
  }
  cashierTotal.messages = messages
  return cashierTotal
}

export async function reprintCloseSale (cashier, receiptPrintNum) {
  const result = { messages: [], success: false }
  const createdAt = new Date()
  try {
    const cashierTotal = await CashierTotalRepository.fetchLastCloseSales(cashier.id)
    const reprintResult = await printTotalReceipt(createdAt, cashierTotal, AGGREGATE_TYPE.POS, receiptPrintNum, true)
    result.messages = result.messages.concat(reprintResult.messages)
    // 全体精算フラグが 1 の場合は、引き続いて全体精算（店舗精算）を行う。
    if (cashierTotal.total_sales === 1) {
      await saveOperationLog(OPERATION_TYPE.CLOSE_SALE, createdAt)
      const businessDate = cashierTotal.business_date.replace(/-/g, '/')
      const shopCashierTotal = await CashierTotalRepository.fetchCheckSales(
        TOTAL_MODE.CLOSE, AGGREGATE_TYPE.SHOP, businessDate)
      const shopCashierTotalResult = await printTotalReceipt(
        createdAt, shopCashierTotal, AGGREGATE_TYPE.SHOP, receiptPrintNum, true)
      result.messages = result.messages.concat(shopCashierTotalResult.messages)
    }
    result.success = true
  } catch (error) {
    result.messages.push(getCommonNetworkErrorMessage(error), I18n.t('message.G-02-E009'))
  }
  return result
}
export const getAndSetCashInfo = async (dispatch) => {
  if (PrinterManager.isSelectedCashChanger() && PrinterManager.isSetPrinter()){
    let sendCashChangerErrorListener
    let cashCountCompleteListener
    try{
      dispatch(loadStart({config: {key: 'loading'}}))
      await PrinterManager.connectCasher()
      const printerBridgeManager = NativeModules.PrinterBridge
      const printerBridgeEventEmitter = new NativeEventEmitter(printerBridgeManager)
      sendCashChangerErrorListener = await printerBridgeEventEmitter.addListener('SendCashChangerError', async (body) => {
        const errorMsg = cChangerErrorMessage(body)
        AlertView.show(errorMsg, () => true)
        dispatch(loadEnd({config: {key: 'loading'}}))
        await PrinterManager.disconnectCasher()
        if (sendCashChangerErrorListener){
          sendCashChangerErrorListener.remove()
          sendCashChangerErrorListener = null
        }
      })
      cashCountCompleteListener = await printerBridgeEventEmitter.addListener('CashCountComplete', async(body) => {
        await setChangerCashCount(dispatch, body)
        await PrinterManager.disconnectCasher()
        dispatch(loadEnd({config: {key: 'loading'}}))
        if (cashCountCompleteListener){
          cashCountCompleteListener.remove()
          cashCountCompleteListener = null
        }
      })
      await PrinterManager.readCashCount()
    }catch(error){
      await PrinterManager.disconnectCasher()
      setTimeout(()=>{
        dispatch(loadEnd({config: {key: 'loading'}}))
        AlertView.show(error, () => {
          return true;
        })
      },30000)
      if (sendCashChangerErrorListener){
        sendCashChangerErrorListener.remove()
        sendCashChangerErrorListener = null
      }
      if (cashCountCompleteListener){
        cashCountCompleteListener.remove()
        cashCountCompleteListener = null
      }
    }
  }
}

export const setChangerCashCount = async (dispatch, cashInfo) => {
  let cashierRecordData = {
    bill_10000: 0,
    bill_5000: 0,
    bill_2000: 0,
    bill_1000: 0,
    coinbar_500: 0,
    coin_500: 0,
    coinbar_100: 0,
    coin_100: 0,
    coinbar_50: 0,
    coin_50: 0,
    coinbar_10: 0,
    coin_10: 0,
    coinbar_5: 0,
    coin_5: 0,
    coinbar_1: 0,
    coin_1: 0
  }
  for (key in cashInfo) {
    switch(key){
      case "jpy10000":
        cashierRecordData["bill_10000"] = cashInfo["jpy10000"]
      break;
      case "jpy5000":
        cashierRecordData["bill_5000"] = cashInfo["jpy5000"]
      break;
      case "jpy2000":
        cashierRecordData["bill_2000"] = cashInfo["jpy2000"]
      break;
      case "jpy1000":
        cashierRecordData["bill_1000"] = cashInfo["jpy1000"]
      break;
      case "jpy500":
        cashierRecordData["coin_500"] = cashInfo["jpy500"]
      break;
      case "jpy100":
        cashierRecordData["coin_100"] = cashInfo["jpy100"]
      break;
      case "jpy50":
        cashierRecordData["coin_50"] = cashInfo["jpy50"]
      break;
      case "jpy10":
        cashierRecordData["coin_10"] = cashInfo["jpy10"]
      break;
      case "jpy5":
        cashierRecordData["coin_5"] = cashInfo["jpy5"]
      break;
      case "jpy1":
        cashierRecordData["coin_1"] = cashInfo["jpy1"]
      break;
    }
  }
  await dispatch(actions.setCashCount(cashierRecordData))
  dispatch(actions.setTotalAmount(getTotalAmount(cashierRecordData)))  
}

export const getCashOut = (amount) => {
  return new Promise(async (resolve, reject)=>{
    let sendCashChangerErrorListener
    let sendDispenseCompleteListener
    try {
      await PrinterManager.connectCasher()
      const printerBridgeManager = NativeModules.PrinterBridge
      const printerBridgeEventEmitter = new NativeEventEmitter(printerBridgeManager)
      sendCashChangerErrorListener = await printerBridgeEventEmitter.addListener('SendCashChangerError', async (body) => {
        const errorMsg = cChangerErrorMessage(body)
        reject(errorMsg)
        await PrinterManager.disconnectCasher()
        if (sendCashChangerErrorListener){
          sendCashChangerErrorListener.remove()
          sendCashChangerErrorListener = null
        }
      })
      sendDispenseCompleteListener = await printerBridgeEventEmitter.addListener('SendDispenseComplete', async (body) => {
        resolve()
        await PrinterManager.disconnectCasher()
        if (sendDispenseCompleteListener){
          sendDispenseCompleteListener.remove()
          sendDispenseCompleteListener = null
        }
      })
      await PrinterManager.dispenseChange(amount)
    } catch (error) {
      await PrinterManager.disconnectCasher()
      setTimeout(()=>{reject(error)},30000)
      if (sendCashChangerErrorListener){
        sendCashChangerErrorListener.remove()
        sendCashChangerErrorListener = null
      }
      if (sendDispenseCompleteListener){
        sendDispenseCompleteListener.remove()
        sendDispenseCompleteListener = null
      }
    }
  })
}

export const allCashOut = async () => {
  return new Promise(async (resolve, reject)=>{
    let cashCountEventListener
    let sendCashChangerErrorListener
    let sendDispenseCompleteListener
    try{
      await PrinterManager.connectCasher()
      const printerBridgeManager = NativeModules.PrinterBridge
      const printerBridgeEventEmitter = new NativeEventEmitter(printerBridgeManager)
      cashCountEventListener = await printerBridgeEventEmitter.addListener('CashCountComplete', async(jsonCoin) => {
        sendCashChangerErrorListener = await printerBridgeEventEmitter.addListener('SendCashChangerError', async (body) => {
          const errorMsg = cChangerErrorMessage(body)
          reject(errorMsg)
          await PrinterManager.disconnectCasher()
          if (cashCountEventListener){
            cashCountEventListener.remove()
            cashCountEventListener = null
          }
          if (sendCashChangerErrorListener){
            sendCashChangerErrorListener.remove()
            sendCashChangerErrorListener = null
          }
        })
        sendDispenseCompleteListener = await printerBridgeEventEmitter.addListener('SendDispenseComplete', async (body) => {
          resolve()
          await PrinterManager.disconnectCasher()
          if (cashCountEventListener){
            cashCountEventListener.remove()
            cashCountEventListener = null
          }
          if (sendDispenseCompleteListener){
            sendDispenseCompleteListener.remove()
            sendDispenseCompleteListener = null
          }
        })
        jsonCoin['jpy2000'] = 0;
        await PrinterManager.dispenseCasher(jsonCoin)
      })
      await PrinterManager.readCashCount()
    }catch(error){
      await PrinterManager.disconnectCasher()
      setTimeout(()=>{reject(error)},30000)
      if (cashCountEventListener){
        cashCountEventListener.remove()
        cashCountEventListener = null
      }
      if (sendCashChangerErrorListener){
        sendCashChangerErrorListener.remove()
        sendCashChangerErrorListener = null
      }
      if (sendDispenseCompleteListener){
        sendDispenseCompleteListener.remove()
        sendDispenseCompleteListener = null
      }
    }
  })
}
