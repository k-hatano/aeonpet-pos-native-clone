import I18n from 'i18n-js'
import CashierRecordReceiptBuilderBase from 'modules/cashierTotal/models/CashierRecordReceiptBuilderBase'
import { TOTAL_MODE } from 'modules/cashierTotal/models'

export default class CashierRecordReceiptBuilder extends CashierRecordReceiptBuilderBase {
  // カスタマイズ-WBS-123
  getTitle (totalMode) {
      return totalMode === TOTAL_MODE.CLOSE ? I18n.t('receipt.title.cashierRecord') : I18n.t('receipt.title.cashierRecord_check_sales')
  }
  getElements (cashierRecord, currency, totalMode) {
    let elements = [
      {
        element: 'combine_left_right',
        left: I18n.t('check_sale.total_amount'),
        right: this.formatMoney(cashierRecord.total_amount, currency)
      },
      {
        element: 'combine_left_right',
        left: I18n.t('check_sale.account_amount'),
        right: this.formatMoney(cashierRecord.account_amount, currency)
      },
      {
        element: 'combine_left_right',
        left: I18n.t('check_sale.deficiency_amount'),
        right: this.formatMoney(cashierRecord.withdrawal_amount + cashierRecord.total_amount - cashierRecord.account_amount, currency)  //2019.6 中間回収額
      }
    ]
    // 2019.6 中間回収額追加 start
    if (totalMode === TOTAL_MODE.CHECK) {
      return[
        {
          element: 'combine_left_right',
          left: I18n.t('check_sale.withdrawal_amount'),
          right: this.formatMoney(cashierRecord.withdrawal_amount, currency)
        },
        ...elements
      ]
    }
    // 2019.6 中間回収額追加 end

    if (totalMode === TOTAL_MODE.CLOSE) {
      return [
        // カスタマイズ-WBS-123
        {
          element: 'combine_left_right',
          left: I18n.t('check_sale.withdrawal_amount_summary'),
          right: this.formatMoney(cashierRecord.withdrawal_amount_summary, currency)
        },
        // カスタマイズ-WBS-123
        ...elements,
        {
          element: 'combine_left_right',
          left: I18n.t('check_sale.deposit_amount'),
          right: this.formatMoney(cashierRecord.deposit_amount, currency)
        },
        {
          element: 'combine_left_right',
          left: I18n.t('check_sale.carry_forward_amount'),
          right: this.formatMoney(cashierRecord.carry_forward_amount, currency)
        }
      ]
    }
    return elements
  }
}
