import CashierRecordReceiptBuilder from '../CashierRecordReceiptBuilder'
import ReceiptTextConverter from '../../../printer/models/ReceiptTextConverter'
import I18n from 'i18n-js'
import { propertyA } from '../../../printer/samples'
import initialize from 'common/initialize'
import { TOTAL_MODE } from '../../models'
I18n.locale = 'ja'

const targetReceiptText = `
                    在高登録

テスト店舗A
テストレジA-1
2017-07-07 09:30:00
担当者１

￥10,000                       1枚      ￥10,000
￥5,000                        1枚       ￥5,000
￥2,000                        1枚       ￥2,000
￥1,000                        1枚       ￥1,000
￥500                 1本      0枚      ￥25,000
￥100                 0本      1枚         ￥100
￥50                  0本      0枚           ￥0
￥10                  1本      1枚         ￥510
￥5                   1本      1枚         ￥255
￥1                   1本      1枚          ￥51
合計                  4本      8枚      ￥10,000

実在高                                  ￥10,000
帳簿在高                                 ￥9,500
過不足                                     ￥500
銀行入金額                               ￥4,500
翌日繰越金                               ￥5,000
`.slice(1)

describe('CashierRecord Receipt Builder test', () => {
  it('CashierRecord receipt text build', async () => {
    const receiptBuilder = new CashierRecordReceiptBuilder()
    await receiptBuilder.initializeAsync()
    const cashierRecord = {
      currency: 'jpy',
      total_amount: 10000,
      account_amount: 9500,
      deposit_amount: 4500,
      carry_forward_amount: 5000,
      staff_name: '担当者１'
    }
    const cashierRecordData = {
      bill_1000: 1,
      bill_2000: 1,
      bill_5000: 1,
      bill_10000: 1,
      coin_1: 1,
      coin_5: 1,
      coin_10: 1,
      coin_50: 0,
      coin_100: 1,
      coin_500: 0,
      coinbar_1: 1,
      coinbar_5: 1,
      coinbar_10: 1,
      coinbar_50: 0,
      coinbar_100: 0,
      coinbar_500: 1
    }
    const propertyA = {
      RECEIPT_LINE_LENGTH: 48
    }
    const createdAt = new Date('2017-07-07 09:30:00')
    const receipt = receiptBuilder.buildCashierRecordReceipt(
      cashierRecord,
      cashierRecordData,
      createdAt,
      TOTAL_MODE.CLOSE
    )
    const receiptTextConverter = new ReceiptTextConverter()
    const openSaleReceiptText = receiptTextConverter.contentToText(
      receipt.content,
      propertyA
    )
    expect(openSaleReceiptText).toBe(targetReceiptText)
  })
})
