import CashierTotalReceiptBuilder from '../CashierTotalReceiptBuilder'
import ReceiptTextConverter from 'modules/printer/models/ReceiptTextConverter'
import I18n from 'i18n-js'

import { propertyA } from 'modules/printer/samples'
import initialize from 'common/initialize'
I18n.locale = 'ja'

const targetReceiptText = `
                日計明細(POS別)

                          営業日：2017年07月07日
                                店舗：テスト店舗
                                 POS：テストレジ
                          担当者：テストスタッフ


【取引合計】
総取引額                                10,000円
売上点数                                   100点

             ----------------------


【レシート/領収証発行】
収入印紙                    100件      100,000円
`



describe('cashierTotal Receipt Builder test', () => {
  it('cashierTotal receipt text build', () => {
    const cashierTotalReceiptBuilder = new CashierTotalReceiptBuilder()
    const cashierTotal =
      {
        receipt_title: '日計明細(POS別)',
        shop_name: 'テスト店舗',
        cashier_name: 'テストレジ',
        staff_name: 'テストスタッフ',
        business_date: '2017-07-07',
        total_data: [
          {
            key: 'total_sales',
            label: '取引合計',
            values: [
              {
                kye: 'net_sales',
                label: '総取引額',
                amount: 10000,
                amount_unit: '円'
              },
              {
                key: 'total_quantity',
                label: '売上点数',
                count: 100,
                count_unit: '点'
              }
            ]
          },
          {
            key: '__line__'
          },
          {
            key: 'receipt_total',
            label: 'レシート/領収証発行',
            values: [
              {
                key: 'receipt_total',
                label: '収入印紙',
                count: 100,
                count_unit: '件',
                amount: 100000,
                amount_unit: '円'
              }
            ]
          }
        ]
      }

    const createdAt = new Date("2017-07-07 09:30:00")
    const receiptObj = cashierTotalReceiptBuilder.buildCashierTotalReceipt(cashierTotal, createdAt)
    const receiptTextConverter = new ReceiptTextConverter()
    const cashierTotalReceiptText = receiptTextConverter.contentToText(receiptObj.content, propertyA)
    expect(cashierTotalReceiptText).toBe(targetReceiptText)
  })
})
