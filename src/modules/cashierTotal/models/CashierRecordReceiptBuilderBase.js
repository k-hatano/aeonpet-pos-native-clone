import ReceiptBuilder from 'modules/printer/models/ReceiptBuilder'
import { CASHIER_RECORD_PRICE_LIST, SIMPLE_ELEMENT_LIST, MULTIPLE_ELEMENT_LIST } from 'modules/cashierTotal/models'

export default class CashierRecordReceiptBuilderBase extends ReceiptBuilder {
  /**
   * 在高登録レコードのレシート情報を生成します。
   * @param cashierRecord
   * @param cashierRecordData
   * @param createdAt
   * @param totalMode
   * @returns {{content: [null,null,null,null,null,null,null,null,null,null,null]}}
   */
  buildCashierRecordReceipt (cashierRecord, cashierRecordData, createdAt, totalMode) {
    const currency = cashierRecord.currency
    const formattedCashierRecord = this._formatCashierRecordData(cashierRecord, cashierRecordData)
    return {
      content: [
        this.titleElement(this.getTitle(totalMode)), // カスタマイズ-WBS-123
        this.feedElement(1),
        this.shopNameElement(),
        this.cashierNameElement(),
        {
          element: 'text',
          align: 'left',
          text: this.formatDateTime(createdAt)
        },
        {
          element: 'text',
          align: 'left',
          text: cashierRecord.staff_name
        },
        this.feedElement(),
        ...this._combineThreeColumns(formattedCashierRecord, currency),
        ...this.getElements(cashierRecord, currency, totalMode),
        this.cutElement()
      ]
    }
  }
  // abstract methods
  getTitle () { return '' }
  getElements () { return [] }

  _combineThreeColumns (formattedCashierRecord, currency) {
    if (formattedCashierRecord === null || formattedCashierRecord.length === 0) return []
    const elements = formattedCashierRecord.map(data => {
      const rightText = this.formatMoney(data.right, currency)
      const rightPadding = this._getPadding(rightText, 11)  //2019.6 58mm対応
      const centerText = data.center !== null ? `${data.center}枚` : ''
      const centerPadding = this._getPadding(centerText, 8)  //2019.6 58mm対応
      const leftText = data.left !== null ? `${data.left}本` : ''
      return {
        element: 'combine_left_right',
        left: data.label,
        right: leftText + centerPadding + centerText + rightPadding + rightText
      }
    })
    return [
      ...elements,
      this.feedElement()
    ]
  }
  _getPadding (text, maxWidth) {
    const width = maxWidth - text.length
    return width > 0 ? ' '.repeat(width) : ''
  }
  _formatCashierRecordData (cashierRecord, cashierRecordData) {
    if (cashierRecordData == null || cashierRecordData.length === 0) return null
    return [
      {
        label: '￥10,000',
        left: null,
        center: cashierRecordData.bill_10000,
        right: cashierRecordData.bill_10000 * CASHIER_RECORD_PRICE_LIST.bill_10000
      },
      {
        label: '￥5,000',
        left: null,
        center: cashierRecordData.bill_5000,
        right: cashierRecordData.bill_5000 * CASHIER_RECORD_PRICE_LIST.bill_5000
      },
      {
        label: '￥2,000',
        left: null,
        center: cashierRecordData.bill_2000,
        right: cashierRecordData.bill_2000 * CASHIER_RECORD_PRICE_LIST.bill_2000
      },
      {
        label: '￥1,000',
        left: null,
        center: cashierRecordData.bill_1000,
        right: cashierRecordData.bill_1000 * CASHIER_RECORD_PRICE_LIST.bill_1000
      },
      {
        label: '￥500',
        left: cashierRecordData.coinbar_500,
        center: cashierRecordData.coin_500,
        right:
          cashierRecordData.coinbar_500 * CASHIER_RECORD_PRICE_LIST.coinbar_500 +
          cashierRecordData.coin_500 * CASHIER_RECORD_PRICE_LIST.coin_500
      },
      {
        label: '￥100',
        left: cashierRecordData.coinbar_100,
        center: cashierRecordData.coin_100,
        right:
          cashierRecordData.coinbar_100 * CASHIER_RECORD_PRICE_LIST.coinbar_100 +
          cashierRecordData.coin_100 * CASHIER_RECORD_PRICE_LIST.coin_100
      },
      {
        label: '￥50',
        left: cashierRecordData.coinbar_50,
        center: cashierRecordData.coin_50,
        right:
          cashierRecordData.coinbar_50 * CASHIER_RECORD_PRICE_LIST.coinbar_50 +
          cashierRecordData.coin_50 * CASHIER_RECORD_PRICE_LIST.coin_50
      },
      {
        label: '￥10',
        left: cashierRecordData.coinbar_10,
        center: cashierRecordData.coin_10,
        right:
          cashierRecordData.coinbar_10 * CASHIER_RECORD_PRICE_LIST.coinbar_10 +
          cashierRecordData.coin_10 * CASHIER_RECORD_PRICE_LIST.coin_10
      },
      {
        label: '￥5',
        left: cashierRecordData.coinbar_5,
        center: cashierRecordData.coin_5,
        right:
          cashierRecordData.coinbar_5 * CASHIER_RECORD_PRICE_LIST.coinbar_5 +
          cashierRecordData.coin_5 * CASHIER_RECORD_PRICE_LIST.coin_5
      },
      {
        label: '￥1',
        left: cashierRecordData.coinbar_1,
        center: cashierRecordData.coin_1,
        right:
          cashierRecordData.coinbar_1 * CASHIER_RECORD_PRICE_LIST.coinbar_1 +
          cashierRecordData.coin_1 * CASHIER_RECORD_PRICE_LIST.coin_1
      },
      {
        label: '合計',
        left: Object.keys(cashierRecordData)
          .filter(key => MULTIPLE_ELEMENT_LIST.includes(key))
          .map(key => cashierRecordData[key])
          .reduce((a, b) => a + b, 0),
        center: Object.keys(cashierRecordData)
          .filter(key => SIMPLE_ELEMENT_LIST.includes(key))
          .map(key => cashierRecordData[key])
          .reduce((a, b) => a + b, 0),
        right: cashierRecord.total_amount
      }
    ]
  }
}
