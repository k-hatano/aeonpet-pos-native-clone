import ReceiptBuilder from '../../printer/models/ReceiptBuilder'
import { RECEIPT_TYPE } from 'modules/printer/models'
import moment from 'moment'
import I18n from 'i18n-js'
import { toUnixTimestamp } from 'common/utils/dateTime'
import SettingKeys from '../../setting/models/SettingKeys'

const HEADER_LABELS = {
  cashier_total_number: '通番：',
  shop_name: '店舗：',
  cashier_name: 'POS：',
  staff_name: '担当者：'
}
export default class CashierTotalReceiptBuilder extends ReceiptBuilder {
  buildCashierTotalReceipt (cashierTotal, createdAt, receiptType = 1, fromDate, toDate) {
    return {
      content: [
        this._receiptType(receiptType, this.formatDateTime(createdAt)),
        this.titleElement(cashierTotal.receipt_title),
        this.feedElement(),
        this._headerElement(receiptType !== RECEIPT_TYPE.TERM_TOTAL_SALES
          ? '営業日：' + moment.unix(toUnixTimestamp(new Date(cashierTotal.business_date))).format('YYYY年MM月DD日')
          : '期間：' + moment.unix(toUnixTimestamp(new Date(fromDate))).format('YYYY年MM月DD日') + ' 〜 ' + moment.unix(toUnixTimestamp(new Date(toDate))).format('YYYY年MM月DD日')),
        ...this._headerDataElement(cashierTotal),
        this.feedElement(),
        ...this._totalDataElement(cashierTotal.total_data, null),
        this.cutElement()
      ],
      check_error_strictly: receiptType === RECEIPT_TYPE.CLOSE_SALES
    }
  }

  _headerDataElement (cashierTotal) {
    var content = []
    'cashier_total_number' in cashierTotal && content.push(this._headerElement(HEADER_LABELS['cashier_total_number'] + (cashierTotal['cashier_total_number'] || '')))
    content.push(this._headerElement(HEADER_LABELS['shop_name'] + (cashierTotal['shop_name'] || '')))
    'cashier_name' in cashierTotal && content.push(this._headerElement(HEADER_LABELS['cashier_name'] + (cashierTotal['cashier_name'] || '')))
    content.push(this._headerElement(HEADER_LABELS['staff_name'] + (cashierTotal['staff_name'] || '')))
    return content
  }

  _totalDataElement (totalData, contentArgs, level = 0) {
    var content = contentArgs != null ? contentArgs : []
    totalData.map(data => {
      if (data.key === '__line__') {
        content.push(this.feedElement())
        content.push(this.textLineElement())
        content.push(this.feedElement())
      } else {
        if (!this.getSetting(SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_STAFF_TOTAL) && (data.key === 'staff_sales' || data.key === 'staff_return_sales')) {
          if (data.key === 'staff_sales') {
            content.pop()
            content.pop()
          }
        } else if ('values' in data) {
          if (level === 0) {
            content.push(this.feedElement())
            content.push(this._blockName(data.label))
          } else {
            this._itemElement(data, content, level)
          }
          this._totalDataElement(data.values, content, level + 1)
        } else {
          this._itemElement(data, content, level)
        }
      }
    })
    return content
  }

  _formatMoney (amount) {
    return parseFloat(amount).toLocaleString('ja-JP')
  }

  _headerElement (text) {
    return {
      element: 'text',
      align: 'right',
      text: text
    }
  }

  _blockName (text) {
    return {
      element: 'text',
      align: 'left',
      text: '【' + text + '】'
    }
  }

  _receiptType (receiptType, createdAt) {
    switch (receiptType) {
      case RECEIPT_TYPE.CHECK_SALES:
        return this.combineLeftRightElement('<' + I18n.t('home.check_sales') + '>', createdAt)
      case RECEIPT_TYPE.CLOSE_SALES:
        return this.combineLeftRightElement('<' + I18n.t('home.close_sales') + '>', createdAt)
      default:
        return this.feedElement()
    }
  }

  _itemElement (data, content, level) {
    if ('amount' in data) {
      if ('count' in data) {
        content.push(this._threeRows(data, level))
      } else {
        content.push(this._twoRows(data, level))
      }
    } else if ('count' in data) {
      content.push(this._twoRows(data, level))
    }
  }

  _twoRows (data, level) {
    const padding = this._getLevelPadding(level)
    if ('amount' in data) {
      return {
        element: 'combine_left_right',
        left: padding + data.label,
        right: this._formatMoney(data.amount) + data.amount_unit
      }
    } else {
      return {
        element: 'combine_left_right',
        left: padding + data.label,
        right: data.count + data.count_unit
      }
    }
  }

  _threeRows (data, level) {
    const leftPadding = this._getLevelPadding(level)
    const center = data.count + data.count_unit
    const right = this._formatMoney(data.amount) + data.amount_unit
    const centerIndent = 15
    const rightPadding = ' '.repeat(centerIndent - this.lengthByShiftJisByte(right))
    return {
      element: 'combine_left_right',
      left: leftPadding + data.label,
      right: center + rightPadding + right
    }
  }

  _getLevelPadding (level) {
    return level > 1 ? ' '.repeat((level - 1) * 2) : ''
  }
}
