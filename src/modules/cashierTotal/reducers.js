import * as actions from './actions'
import { handleActions } from 'redux-actions'
import { makeReceiptText } from 'modules/printer/models'
import { getTotalAmount } from './services'
import { CASHIER_RECORD_MODE } from './models'

const defaultState = {
  receipt: null,
  receiptText: null,
  cashierTotalPosEJournal: null,
  cashierTotalShopEJournal: null,
  cashierTotalAggregateEJournal: null,
  cashierTotalCloseSalesEJournal: null,
  cashierRecord: {
    total_amount: 0,
    real_amount: 0,
    deposit_amount: 0, // 銀行入金額
    account_amount: 0, // 帳簿在高
    carry_forward_amount: 0,
    withdrawal_amount: 0, // 2019.06  中間回収額追加
    withdrawal_amount_summary:0 // 中間回収額合計 // カスタマイズ-WBS-124
  },
  cashierRecordData: {
    bill_10000: 0,
    bill_5000: 0,
    bill_2000: 0,
    bill_1000: 0,
    coinbar_500: 0,
    coin_500: 0,
    coinbar_100: 0,
    coin_100: 0,
    coinbar_50: 0,
    coin_50: 0,
    coinbar_10: 0,
    coin_10: 0,
    coinbar_5: 0,
    coin_5: 0,
    coinbar_1: 0,
    coin_1: 0
  },
  cashiers: []
}

const handlers = {
  [actions.resetState]: (state, action) => ({
    ...state,
    receipt: null,
    receiptText: null,
    cashierTotalPosEJournal: null,
    cashierTotalShopEJournal: null,
    cashierTotalCloseSalesEJournal: null,
    cashierRecord: {
      total_amount: 0,
      deposit_amount: 0,
      account_amount: action.payload === CASHIER_RECORD_MODE.CHECK_SALES || action.payload === CASHIER_RECORD_MODE.CLOSE_SALES ? state.cashierRecord.account_amount : 0,
      carry_forward_amount: action.payload === CASHIER_RECORD_MODE.OPEN_SALES ? state.cashierRecord.carry_forward_amount : 0,
      withdrawal_amount: 0,  // 2019.06  中間回収額追加
      withdrawal_amount_summary: action.payload === CASHIER_RECORD_MODE.CLOSE_SALES ? state.cashierRecord.withdrawal_amount_summary : 0  // カスタマイズ-WBS-124
    },
    cashierRecordData: {
      bill_10000: 0,
      bill_5000: 0,
      bill_2000: 0,
      bill_1000: 0,
      coinbar_500: 0,
      coin_500: 0,
      coinbar_100: 0,
      coin_100: 0,
      coinbar_50: 0,
      coin_50: 0,
      coinbar_10: 0,
      coin_10: 0,
      coinbar_5: 0,
      coin_5: 0,
      coinbar_1: 0,
      coin_1: 0
    }
  }),
  [actions.setTotalAmount]: (state, action) => ({
    ...state,
    cashierRecord: {
      ...state.cashierRecord,
      total_amount: action.payload,
      sum_amount: action.payload,
      withdrawal_amount: 0
    }
  }),
  [actions.setDepositAmount]: (state, action) => ({
    ...state,
    cashierRecord: {
      ...state.cashierRecord,
      deposit_amount: action.payload
    }
  }),
  [actions.setAccountAmount]: (state, action) => ({
    ...state,
    cashierRecord: {
      ...state.cashierRecord,
      account_amount: action.payload
    }
  }),
  // カスタマイズ-WBS-124
  [actions.setWithdrawalAmountSummary]: (state, action) => ({
    ...state,
    cashierRecord: {
      ...state.cashierRecord,
      withdrawal_amount_summary: action.payload
    }
  }),
  // カスタマイズ-WBS-124
  [actions.setCarryForwardAmount]: (state, action) => ({
    ...state,
    cashierRecord: {
        ...state.cashierRecord,
        carry_forward_amount: action.payload
      }
  }),
  /* 2019.6 中間回収額追加 start */
  [actions.setWithdrawalAmount]: (state, action) => {
    const total_amount = state.cashierRecord.sum_amount-action.payload
    return ({
      ...state,
      cashierRecord: {
          ...state.cashierRecord,
          withdrawal_amount: action.payload,
          total_amount
        }
    })
  },
  /* 2019.6 中間回収額追加 end */
  [actions.setReceipt]: (state, action) => ({
    ...state,
    receipt: action.payload,
    receiptText: makeReceiptText(action.payload)
  }),
  [actions.setCashierTotalPosEJournal]: (state, action) => ({
    ...state,
    cashierTotalPosEJournal: action.payload
  }),
  [actions.setCashierTotalShopEJournal]: (state, action) => ({
    ...state,
    cashierTotalShopEJournal: action.payload
  }),
  [actions.setCashierTotalAggregateEJournal]: (state, action) => ({
    ...state,
    cashierTotalAggregateEJournal: action.payload
  }),
  [actions.setCashierTotalCloseSalesEJournal]: (state, action) => ({
    ...state,
    cashierTotalCloseSalesEJournal: action.payload
  }),
  [actions.setCashierRecordData]: (state, action) => {
    return {
      ...state,
      cashierRecord: {
        ...state.cashierRecord,
        total_amount: getTotalAmount(action.payload),
        sum_amount: getTotalAmount(action.payload),
        withdrawal_amount: 0
      },
      cashierRecordData: action.payload
    }
  },
  [actions.updateCashierRecordData]: (state, action) => {
    if (action.payload != null) {
      const cashierRecordData = state.cashierRecordData
      action.payload.forEach(record => {
        cashierRecordData[record.key] = record.value
      })
      const totalAmount = getTotalAmount(cashierRecordData)
      return {
        ...state,
        cashierRecord: {
          ...state.cashierRecord,
          total_amount: totalAmount,
          deposit_amount: totalAmount,
          sum_amount: totalAmount,
          withdrawal_amount: 0
        },
        cashierRecordData: Object.assign({}, cashierRecordData)
      }
    } else {
      return state
    }
  },
  [actions.listCashiers]: (state, action) => ({
    ...state,
    cashiers: action.payload
  }),
  [actions.setCashCount]: (state, action) => ({
    ...state,
    cashierRecordData: action.payload
  })
}

export default handleActions(handlers, defaultState)
