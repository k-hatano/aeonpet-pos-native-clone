import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setTotalAmount = createAction(`${MODULE_NAME}_setTotalAmount`)
export const setDepositAmount = createAction(`${MODULE_NAME}_setDepositAmount`)
export const setAccountAmount = createAction(`${MODULE_NAME}_setAccountAmount`)
export const setCarryForwardAmount = createAction(`${MODULE_NAME}_setCarryForwardAmount`)
export const setWithdrawalAmount = createAction(`${MODULE_NAME}_setWithdrawalAmount`)  // 2019.06  中間回収額追加
export const setWithdrawalAmountSummary = createAction(`${MODULE_NAME}_setWithdrawalAmountSummary`) // 2019.06  中間回収額追加->確認必要
export const setReceipt = createAction(`${MODULE_NAME}_setReceipt`)
export const setCashierTotalPosEJournal = createAction(`${MODULE_NAME}_setCashierTotalPosEJournal`)
export const setCashierTotalShopEJournal = createAction(`${MODULE_NAME}_setCashierTotalShopEJournal`)
export const setCashierTotalAggregateEJournal = createAction(`${MODULE_NAME}_setCashierTotalAggregateEJournal`)
export const setCashierTotalCloseSalesEJournal = createAction(`${MODULE_NAME}_setCashierTotalCloseSalesEJournal`)
export const setCashierRecordData = createAction(`${MODULE_NAME}_setCashierRecordData`)
export const updateCashierRecordData = createAction(`${MODULE_NAME}_updateCashierRecordData`)
export const resetState = createAction(`${MODULE_NAME}_resetState`)
export const listCashiers = createAction(`${MODULE_NAME}_listCashiers`)
export const setCashCount = createAction(`${MODULE_NAME}_setCashCount`)

