import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  salesAgreegateContainer: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  salesAggregateForm: {
    flexDirection: 'column',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#000'
  }
})
