import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  cashierRecordFormContainer: {
    flexDirection: 'row',
    flex: 1
  },
})
