import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  salesResultContainer: {
    paddingBottom: 10,
    backgroundColor: '#fff',
    flex: 1
  },
  saleItemText: {
    fontSize: 18
  },
  totalCashLeftPanel: {
    width: '100%',
    backgroundColor: '#94989B',
    height: 48,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  lineView: {
    width: '100%',
    backgroundColor: '#ddd',
    height: 3,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  totalCashRightPanel: {
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'flex-end',
    justifyContent: 'center',
    height: 86
  },
  totalCashRightBottomPanel: {
    flex:1,
    width: '100%',
    paddingTop: 20,
    paddingHorizontal: 102,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  amountCashRow: {
    width: '100%',
    backgroundColor: '#fff',
    height: 30,
    marginBottom: 80,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  totalCashCol: {
    fontSize: 30,
    fontWeight: '500',
    textAlign: 'right',
    marginRight: 20
  },
  amountCashCol: {
    fontSize: 30,
    fontWeight: '500'
  },
  totalCashText: {
    fontSize: 36,
    marginRight: 20,
    fontWeight: '500'
  },
  headerText: {
    color: '#ffffff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 24
  },
  salesButton: {
    borderColor: '#7E7E7E',
    borderWidth: 2,
    marginRight: 10,
    width: 200,
    height: '80%',
    backgroundColor: 'transparent'
  },
  registerButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 56,
    fontSize: 32,
    fontWeight: 'bold',
    width: '100%'
  },
  approvalButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9024',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    width: 160,
    height: 30
  },
  resettingButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ccc',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    width: 160,
    height: 30
  },
  modalContainer: {
    backgroundColor: '#fff',
    padding: 20,
    flexDirection: 'column'
  },
  modalTitleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 40
  },
  modalTitle: {
    fontSize: 22,
    fontWeight: '500'
  },
  modalItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60
  },
  modalCenterContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  openingModalStyle: {
    backgroundColor: '#fff',
    alignItems: 'center',
    width: 400,
    height: 240
  },
  registerButtonContainer: {
    alignItems: 'flex-end',
    width: '80%',
    marginLeft: '10%'
  },
  numberPadContainer: {
    marginRight: 10,
    flexDirection: 'row'
  }
})
