import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  cashierTotalButtonView: {
    marginVertical: 48
  },
  cashierTotalButton: {
    width: 360,
    height: 56,
    fontSize: 32,
    fontWeight: 'bold'
  },
  receiptButton: {
    marginTop: 340
  },
  closeSalesButton: {
    marginTop: 272
  },
  CloseSalesOperationContainer: {
    alignItems: 'center',
    height: '100%'
  },
  closeSalesViewContainer: {
    flexDirection: 'row',
    width: '100%'
  },
  receiptViewContainer: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    flexDirection: 'row',
    borderRightWidth: 0.8,
    borderRightColor: '#979797'
  },
  OperationPanelContainer: {
    flex: 1
  }
})
