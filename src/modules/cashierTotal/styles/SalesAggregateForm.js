import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  cashierTotalButton: {
    width: 360,
    height: 56,
    margin: 20,
    marginBottom: 76,
    fontSize: 32,
    fontWeight: 'bold'
  },
  SearchAggregateFormContainer: {
    marginTop: 70,
    marginBottom: 104,
    marginHorizontal: 33,
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%'
  },
  buttonContainer: {
    marginTop: 315
  },
  SalesAggregateOperationContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    margin: 40
  },
  inputStyle: {
    height: 40,
    borderWidth: 1,
    width: 350,
    margin: 20
  },
  calendarPosition: {
    right: 100,
    top: 50
  },
  calendarView: {
    position: 'absolute'
  },
  SalesAggregateColumn: {
    borderColor: '#979797',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: '#f0f0f0'
  },
  SalesAggregateModalContainer: {
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  },
  sampleRowContainer: {
    flexDirection: 'row',
    marginBottom: 30
  },
  sampleRowChildContainer: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sampleRowTitle: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  commonText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  dateWrapper: {
    flexDirection: 'row',
    width: '100%'
  },
  dateTextWrapper: {
    flex: 4.5,
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  searchInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: 296,
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: '#f0f0f0'
  },
  rowLayout: {
    marginBottom: 20
  },
  inputContainer: {
    height: 50,
    flex: 8
  },
  touchableContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  datePickerTitle: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  datePickerConfirm: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#44bcb9',
    color: '#ffffff',
    padding: 15
  },
  datePickerCancel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#ffffff',
    color: '#44bcb9'
  }
})
