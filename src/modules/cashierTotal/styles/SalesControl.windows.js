import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  salesControlMain: {
    flex: 1
  },
  panelContainer: {
    flexDirection: 'column',
    backgroundColor: 'transparent',
    flex: 1,
    marginHorizontal: 20
  },
  topMenuItemContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 50
  },
  menuItemPanelTitle: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center'
  },
  menuItemPanelText: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  menuItemPanelRow: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  menuTitle: {
    fontSize: 18
  },
  menuHeader: {
    fontSize: 20,
    fontWeight: '500'
  },
  salesButton: {
    borderColor: '#7E7E7E',
    borderWidth: 2,
    width: '60%',
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  bottomMenuItemContainer: {
    flexDirection: 'row',
    width: '100%',
    flex: 1,
    maxHeight: 50
  },
  separatorContainer: {
    width: '100%',
    height: 20,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#ccc',
    marginBottom: 0
  },
  headerContainer: {
    flexDirection: 'row',
    height: 50
  },
  clearButtonRow: {
    width: '25%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  clearButton: {
    fontSize: 24,
    fontWeight: 'bold',
    width: 144,
    height: 56,
    marginRight: 74
  },
  skipButton: {
    fontSize: 24,
    fontWeight: 'bold',
    width: 144,
    height: 56,
    marginRight: 24
  },
  clearButtonContainer: {
    width: '100%',
    height: 82,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  coinPanelArea: {
    flexDirection: 'row',
    flex: 70
  }
})
