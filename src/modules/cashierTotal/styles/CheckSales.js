import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  cashierTotalButtonView: {
    marginVertical: 48
  },
  cashierTotalButton: {
    width: 360,
    height: 56,
    fontSize: 32,
    fontWeight: 'bold',
  },
  CheckSalesOperationContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  CheckSalesContainer: {
    padding: 20,
    paddingTop: 30,
    paddingBottom: 30,
    height: '100%'
  },
  TabContainer: {
    backgroundColor: '#f0f0f0'
  },
  tabWrapperStyle: {
    backgroundColor: '#f0f0f0'
  },
  tabStyle: {
    width: 136,
    paddingBottom: 13
  }
})
