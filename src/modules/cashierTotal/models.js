import I18n from 'i18n-js'

export const MODULE_NAME = 'cashierTotal'

export const TOTAL_MODE = {
  CHECK: 1, // 点検
  CLOSE: 2  // 精算
}
export const AGGREGATE_TYPE = {
  POS: 1, // 端末
  SHOP: 2 // 店舗
}
export const CASHIER_RECORD_MODE = {
  CHECK_SALES: 1,  // 点検
  CLOSE_SALES: 2,  // 精算
  OPEN_SALES: 3    // 開局
}

const BILL_LIST = [
  {
    i18n: 'check_sale.ten_thousand_yen',
    key: 'bill_10000'
  },
  {
    i18n: 'check_sale.five_thousand_yen',
    key: 'bill_5000'
  },
  {
    i18n: 'check_sale.two_thousand_yen',
    key: 'bill_2000'
  },
  {
    i18n: 'check_sale.one_thousand_yen',
    key: 'bill_1000'
  }
]
const COIN_LIST = [
  {
    i18n: 'check_sale.five_hundred_yen',
    key: '500'
  },
  {
    i18n: 'check_sale.one_hundred_yen',
    key: '100'
  },
  {
    i18n: 'check_sale.fifty_yen',
    key: '50'
  },
  {
    i18n: 'check_sale.ten_yen',
    key: '10'
  },
  {
    i18n: 'check_sale.five_yen',
    key: '5'
  },
  {
    i18n: 'check_sale.one_yen',
    key: '1'
  }
]

export const CASHIER_RECORD_PRICE_LIST = {
  bill_10000: 10000,
  bill_5000: 5000,
  bill_2000: 2000,
  bill_1000: 1000,
  coinbar_500: 500 * 50,
  coin_500: 500,
  coinbar_100: 100 * 50,
  coin_100: 100,
  coinbar_50: 50 * 50,
  coin_50: 50,
  coinbar_10: 10 * 50,
  coin_10: 10,
  coinbar_5: 5 * 50,
  coin_5: 5,
  coinbar_1: 1 * 50,
  coin_1: 1
}

export const SIMPLE_ELEMENT_LIST = [
  'bill_10000',
  'bill_5000',
  'bill_2000',
  'bill_1000',
  'coin_500',
  'coin_100',
  'coin_50',
  'coin_10',
  'coin_5',
  'coin_1'
]

export const MULTIPLE_ELEMENT_LIST = [
  'coinbar_500',
  'coinbar_100',
  'coinbar_50',
  'coinbar_10',
  'coinbar_5',
  'coinbar_1'
]

export const getCashierMenuList = () => {
  const billList = BILL_LIST.map(data => {
    return { title: I18n.t(data.i18n), key: data.key }
  })
  const coinList = COIN_LIST.map(data => {
    return { title: I18n.t(data.i18n), key: data.key }
  })
  return { bill: billList, coin: coinList }
}
