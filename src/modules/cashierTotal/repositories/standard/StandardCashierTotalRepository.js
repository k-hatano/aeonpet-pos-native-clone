import fetcher from 'common/models/fetcher'
// import logger from 'common/utils/logger'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import SettingKeys from 'modules/setting/models/SettingKeys'
import InvalidOperationError from 'common/errors/InvalidOperationError'
import ShopRepository from 'modules/shop/repositories/ShopRepository'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import { handleAxiosError } from '../../../../common/errors'

export default class StandardCashierTotalRepository extends StoreAccessibleBase {
  async fetchCheckSales (totalMode, aggregateType, businessDate) {
    try {
      const currentShop = await this._getCurrentShop()
      const shopName = currentShop.name
      const cashierName = await this._getCashierName()
      const posClosedTime = await this._getPosClosedTime()
      const cashierTotals = await fetcher.post('cashier-totals/check-sales',
        {
          total_mode: totalMode,
          aggregate_type: aggregateType,
          shop_name: shopName,
          cashier_id: this._cashierId,
          cashier_name: cashierName,
          staff_id: this._staffId,
          staff_name: this._staffName,
          pos_closed_time: posClosedTime,
          business_date: businessDate
        }
      )
      return cashierTotals.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchSalesAggregateCashierTotal (aggregateType, fromDate, toDate, cashier) {
    try {
      const currentShop = await this._getCurrentShop()
      const shopId = currentShop.id
      const shopName = currentShop.name
      const salesAggregates = await fetcher.get('cashier-totals/aggregate-sales',
        {
          aggregate_type: aggregateType,
          business_date_from: fromDate,
          business_date_to: toDate,
          cashier_id: cashier.id,
          cashier_name: cashier.name,
          staff_id: this._staffId,
          staff_name: this._staffName,
          shop_id: shopId,
          shop_name: shopName
        }
      )
      return salesAggregates.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchLastCloseSales (cashierId: string) {
    try {
      const response = await fetcher.get('cashier-totals/latest-close', {
        cashier_id: cashierId
      })
      const responseData = {...response.data}
      responseData.total_data = JSON.parse(responseData.total_data)
      return responseData
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchForcedCloseSales () {
    try {
      const cashierName = await this._getCashierName()
      const currentShop = await this._getCurrentShop()
      const shopId = currentShop.id
      const shopName = currentShop.name
      const posClosedTime = await this._getPosClosedTime()
      const cashierTotals = await fetcher.post('cashier-totals/force-close-sales',
        {
          cashier_id: this._cashierId,
          cashier_name: cashierName,
          staff_id: this._staffId,
          staff_name: this._staffName,
          shop_id: shopId,
          shop_name: shopName,
          pos_closed_time: posClosedTime
        })
      return cashierTotals.data.cashier_totals
    } catch (error) {
      handleAxiosError(error)
    }
  }

  get _cashierId () {
    return this.getSetting(SettingKeys.COMMON.CASHIER_ID)
  }
  get _staff () {
    const currentStaff = this.getSetting(SettingKeys.STAFF.CURRENT_STAFF)
    if (currentStaff == null) {
      throw new InvalidOperationError('staff not selected')
    }
    return currentStaff
  }
  get _staffId () { return this._staff.id }
  get _staffName () { return this._staff.name }

  async _getPosClosedTime () {
    const currentShop = await ShopRepository.find()
    return currentShop.pos_closed_time
  }

  async _getCashierName () {
    const currentCashier = await CashierRepository.find()
    if (currentCashier == null) {
      throw new InvalidOperationError('current cashier not found')
    }
    return currentCashier.name
  }
  async _getCurrentShop () {
    const currentShop = await ShopRepository.find()
    if (currentShop == null) {
      throw new InvalidOperationError('current shop not found')
    }
    return currentShop
  }
}
