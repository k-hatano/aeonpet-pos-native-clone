export default class SampleCashierTotalRepository {
  async fetchCheckSales (totalMode, aggregateType, businessDate) {
    if (aggregateType === 1) {
      return {
        receipt_title: '日計明細(POS別)',
        shop_name: 'テスト店舗',
        cashier_name: 'テストレジ',
        staff_name: 'テストスタッフ',
        total_data: [
          ...this._totalData()
        ]
      }
    } else {
      return {
        receipt_title: '日計明細(店舗別)',
        shop_name: 'テスト店舗',
        staff_name: 'テストスタッフ',
        total_data: [
          ...this._totalData()
        ]
      }
    }
  }

  async fetchLastCloseSales (cashierId: string) {
    return {
      receipt_title: '日計明細(POS別)',
      shop_name: 'テスト店舗',
      cashier_name: 'テストレジ',
      staff_name: 'テストスタッフ',
      total_data: [
        ...this._totalData()
      ]
    }
  }

  async fetchSalesAggregateCashierTotal (fromDate, toDate, posId, shopId) {
    return {
      receipt_title: '日計明細(POS別)',
      period: fromDate + '~' + toDate,
      shop_name: 'テスト店舗',
      cashier_name: 'テストレジ',
      staff_name: 'テストスタッフ',
      total_data: [
        ...this._totalData()
      ]
    }
  }

  _totalData () {
    return [
      {
        key: 'total_sales',
        label: '取引合計',
        values: [
          {
            kye: 'net_sales',
            label: '総取引額',
            amount: 10000,
            amount_unit: '円'
          },
          {
            key: 'total_quantity',
            label: '売上点数',
            count: 100,
            count_unit: '点'
          },
          {
            key: 'tax',
            label: '消費税',
            amount: 2000,
            amount_unit: '円'
          },
          {
            key: 'net_sales_tax_excluded',
            label: '純売上',
            amount: 50000,
            amount_unit: '円'
          },
          {
            key: 'customer_count',
            label: '来店人数',
            count: 5000,
            count_unit: '人'
          },
          {
            key: 'average_amount_per_customer',
            label: '客単価',
            amount: 2000,
            amount_unit: '円'
          }
        ]
      },
      {
        key: 'payment_sales',
        label: '支払別取引',
        values: [
          {
            key: '',
            label: '',
            count: 100,
            count_unit: '件',
            amount: 10000,
            amount_unit: '円'
          },
          {
            key: '',
            label: '',
            count: 100,
            count_unit: '件',
            amount: 10000,
            amount_unit: '円'
          },
          {
            key: 'payment_total',
            label: '合計',
            amount: 10000,
            amount_unit: '円'
          }
        ]
      },
      {
        key: '__line__'
      },
      {
        key: 'receipt_total',
        label: 'レシート/領収証発行',
        values: [
          {
            key: 'receipt_total',
            label: '収入印紙',
            count: 100,
            count_unit: '件',
            amount: 100000,
            amount_unit: '円'
          },
          {
            key: 'bill',
            label: '領収証',
            count: 100,
            count_unit: '件',
            amount: 1000,
            amount_unit: '円'
          }
        ]
      }
    ]
  }
}
