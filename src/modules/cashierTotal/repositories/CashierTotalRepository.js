import SampleCashierTotalRepository from './sample/SampleCashierTotalRepository'
import StandardCashierTotalRepository from './standard/StandardCashierTotalRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierTotalRepository {
  static _implement = new SampleCashierTotalRepository()

  static async fetchCheckSales (totalMode, aggregateType, businessDate) {
    return this._implement.fetchCheckSales(totalMode, aggregateType, businessDate)
  }

  static async fetchLastCloseSales (cashierId: string) {
    return this._implement.fetchLastCloseSales(cashierId)
  }

  static async fetchSalesAggregateCashierTotal (aggregateType, fromDate, toDate, cashier) {
    return this._implement.fetchSalesAggregateCashierTotal(aggregateType, fromDate, toDate, cashier)
  }

  static async fetchForcedCloseSales () {
    return this._implement.fetchForcedCloseSales()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashierTotalRepository()
        break
      default:
        this._implement = new SampleCashierTotalRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierTotalRepository', (context) => {
  CashierTotalRepository.switchImplement(context)
})
