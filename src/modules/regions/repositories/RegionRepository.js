import SampleRegionRepository from './sample/SampleRegionRepository'
import StandardRegionRepository from './standard/StandardRegionRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class RegionRepository {
  static _implement = new SampleRegionRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardRegionRepository()
        break
      default:
        this._implement = new SampleRegionRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('RegionRepository', context => {
  RegionRepository.switchImplement(context)
})
