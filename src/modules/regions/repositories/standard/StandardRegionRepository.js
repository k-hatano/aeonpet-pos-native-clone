import fetcher from 'common/models/fetcher'
import { handleAxiosError } from '../../../../common/errors'
import RegionsEntity from 'modules/geo/repositories/entities/RegionsEntity'

export default class StandardRegionRepository {
  async findAll () {
    return RegionsEntity.findAll({
      order: [
        ['region_code', 'ASC']
      ],
      raw: true
    })
  }
}
