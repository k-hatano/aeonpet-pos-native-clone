import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    flex: 1,
    backgroundColor: baseStyleValues.mainBackgroundColor,
    flexDirection: 'row'
  },
  listItemLayout: {
    height: 63,
    paddingHorizontal: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  listItemMainArea: {
    flex: 1
  },
  discountListArea: {
    flex: 1
  },
  discountListHeader: {
    height: 39,
    paddingHorizontal: 14,
    marginTop: 10,
    backgroundColor: baseStyleValues.subBackgroundColor,
    flexDirection: 'row',
    alignItems: 'center'
  },
  discountHeaderText: {
    fontSize: 18,
    color: baseStyleValues.mainTextColor,
    fontWeight: 'bold'
  },
  reasonNameText: {
    fontSize: 18,
    color: baseStyleValues.mainTextColor
  },
  separator: {
    height: 1,
    marginHorizontal: 14,
    backgroundColor: baseStyleValues.separatorColor
  }
})
