import { StyleSheet } from 'react-native'
// import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    backgroundColor: 'white'
  },
  item: {
    height: 63,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20
  },
  itemText: {
    fontSize: 18,
    color: '#4a4a4a'
  },
  itemDisableText: {
    fontSize: 18,
    color: '#A9A9A9'
  },
  discountLabelSet: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  clearButton: {
    width: 80,
    height: 40,
    fontSize: 20
  },
  separator: {
    marginHorizontal: 20,
    height: 1,
    backgroundColor: '#d8d8d8'
  },
  sectionHeader: {
    height: 36,
    marginTop: 16,
    backgroundColor: '#f0f0f0',
    flexDirection: 'row',
    alignItems: 'center'
  },
  sectionHeaderText: {
    fontSize: 18,
    color: '#4a4a4a',
    fontWeight: 'bold',
    marginLeft: 7
  }
})
