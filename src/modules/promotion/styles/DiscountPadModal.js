import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: 772,
    height: 628,
    backgroundColor: baseStyleValues.mainBackgroundColor
  }
})
