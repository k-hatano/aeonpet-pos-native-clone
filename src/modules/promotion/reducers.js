import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  promotions: [],
  discountReasons: [],
  selectedDiscountReason: null
}

const handlers = {
  [actions.listPromotions]: (state, action) => ({
    ...state,
    promotions: action.payload
  }),
  [actions.listDiscountReasons]: (state, action) => ({
    ...state,
    discountReasons: action.payload
  }),
  [actions.selectDiscountReason]: (state, action) => ({
    ...state,
    selectedDiscountReason: action.payload
  })
}
export default handleActions(handlers, defaultState)
