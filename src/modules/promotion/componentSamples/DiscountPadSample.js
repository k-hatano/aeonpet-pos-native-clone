import DiscountPad from '../components/DiscountPad'
import { DISCOUNT_TYPE } from '../models'

export default {
  name: 'DiscountPad',
  component: DiscountPad,
  properties: [
    {
      title: 'Default',
      property: {
        discountReasons: [
          {
            id: '1',
            name: '値割引理由１'
          },
          {
            id: '2',
            name: '値割引理由２'
          },
          {
            id: '3',
            name: '値割引理由３'
          }
        ],
        selectedDiscountReason: {
          id: '2',
          name: 'なんでもいい'
        },
        onComplete: (amount) => console.log('onComplete', {amount}),
        onSelectDiscountReason: (discountReason) => console.log('onSelectDiscountReason', {discountReason}),
        discountType: DISCOUNT_TYPE.AMOUNT
      }
    }
  ],
  frames: [
    {
      title: '772 x 552',
      style: {width: 772, height: 552}
    }
  ]
}
