import DiscountOptionsView from '../components/DiscountOptionsView'
import { DISCOUNT_TYPE } from '../models'

const promotionPresets = [
  {
    title: '雨の日割引',
    discount_type: DISCOUNT_TYPE.RATE,
    discount_value: 0.1
  },
  {
    title: 'タイムセール',
    discount_type: DISCOUNT_TYPE.AMOUNT,
    discount_value: 100
  }
]

export default {
  name: 'DiscountOptionsView',
  component: DiscountOptionsView,
  properties: [
    {
      title: 'Default',
      property: {
        discountAmount: null,
        discountRate: null,
        currency: 'jpy',
        promotionPresets
      }
    },
    {
      title: 'Amount Discount',
      property: {
        discountAmount: 300,
        discountRate: null,
        currency: 'jpy',
        promotionPresets
      }
    },
    {
      title: 'Rate Discount',
      property: {
        discountAmount: null,
        discountRate: 0.1,
        currency: 'jpy',
        promotionPresets
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
