import I18n from 'i18n-js'
import { formatMoney } from '../../common/utils/formats'

export const MODULE_NAME = 'promotion'

export const PROMOTION_TYPE = {
  SALE: 1,
  BUNDLE: 2,
  COUPON: 8,
  SUBTOTAL: 10
}

export const BUNDLE_TYPE = {
  SELECTION: 1,
  BUY_X_GET_Y: 2,
  FLATPRICE: 3,
  CASES: 4
}

export const INCENTIVE_TYPE = {
  FIXED_AMOUNT_DISCOUNT: 1,
  FIXED_RATE_DISCOUNT: 2,
  FIXED_FLAT_RATE: 3,
  PRODUCT_GIFT: 7
}

export const PROMOTION_PRESET_TYPE = {
  ITEM: 1,
  SUBTOTAL: 2
}

export const DISCOUNT_REASON_TYPE = {
  COMMON: 0,
  ITEM: 1,
  SUBTOTAL: 2
}

export const DISCOUNT_TARGET_TYPE = {
  ITEM: 1,
  SUBTOTAL: 2
}

export const DISCOUNT_TYPE = {
  AMOUNT: 1,
  RATE: 2,
  OWNER: 3
}

/**
 * 小計値引順序(普通の小計値引とオーナーカードによる小計値引)
 * OWNER_DISCOUNT_FISRT: オーナー値引後に小計値引
 * DISCOUNT_FIRST: 小計値引後にオーナー値引
 */
export const DISCOUNT_ORDER = {
  OWNER_DISCOUNT_FISRT: 1,
  DISCOUNT_FIRST: 2
}

export const OWNER_DISCOUNT = {
  NAME: 'オーナー割引',
  RATE: 0.05,
  CODE: 'PRM999999999'
}

export const ESTABLISHMENT_CONDITION = {
  EQUAL: 1,
  GREATER_EQUAL: 2
}

export const THRESHOLD_TYPE = {
  ITEM: 2,
  SUBTOTAL: 3
}

export const PRICE_TYPE = {
  SALE_PRICE: 1,
  ORIGINAL_PRICE: 2
}

export const POINT_TYPE = {
  AEON_PET_POINT: 1,
  WAON_POINT: 2
}

export const APPLIED_TYPE = {
  DAY: 1,
  WEEK: 2
}

export const formatDiscountValue = (type, value, currency) => {
  switch (type) {
    case DISCOUNT_TYPE.AMOUNT:
      const amount = value ? formatMoney(value * -1) : 'ー'
      return I18n.t('currency.format_money.' + currency, { amount })

    case DISCOUNT_TYPE.RATE:
      value = value ? Math.round(value * -100) : 'ー'
      return I18n.t('promotion.format_rate_discount', { value })

    default:
      return '(ー)'
  }
}
