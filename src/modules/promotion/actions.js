import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listPromotions = createAction(`${MODULE_NAME}_listPromotions`)
export const listDiscountReasons = createAction(`${MODULE_NAME}_listDiscountReasons`)
export const selectDiscountReason = createAction(`${MODULE_NAME}_selectDiscountReason`)
