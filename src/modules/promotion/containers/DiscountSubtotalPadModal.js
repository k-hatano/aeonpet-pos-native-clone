import { connect } from 'react-redux'
import DiscountPadModal from '../components/DiscountPadModal'
import { MODULE_NAME } from '../models'
import Modal from 'common/components/widgets/Modal'
import DiscountReasonRepository from '../repositories/DiscountReasonRepository'
import { listDiscountReasons, selectDiscountReason } from '../actions'
import CartDiscount from '../../cart/models/CartDiscount'
import { PROMOTION_TYPE } from 'modules/promotion/models'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    const reasons = await DiscountReasonRepository.findForSubtotal()
    dispatch(listDiscountReasons(reasons))
    dispatch(selectDiscountReason(null))
  },
  onSelectDiscountReason: async (discountReason) => {
    dispatch(selectDiscountReason(discountReason))
  }
})

const mapStateToProps = state => ({
  discountReasons: state[MODULE_NAME].discountReasons,
  selectedDiscountReason: state[MODULE_NAME].selectedDiscountReason
})

const discountSubtotalPadModal = connect(mapStateToProps, mapDispatchToProps)(DiscountPadModal)
export default discountSubtotalPadModal

export const showDiscountSubtotalPadModal = (discountType, maxValue, onApply, onCancel) => {
  Modal.open(discountSubtotalPadModal, {
    props: {
      onComplete: (amount, discountType, discountReason) => {
        const discount = CartDiscount.createWithDiscountReason(
          amount, discountType, discountReason.id, discountReason.name, PROMOTION_TYPE.SUBTOTAL)
        onApply(discount)
      },
      onCancel: () => {
        onCancel(discountType)
      },
      discountType,
      maxValue
    }
  })
}
