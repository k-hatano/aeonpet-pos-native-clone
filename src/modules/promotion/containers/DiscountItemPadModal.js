import { connect } from 'react-redux'
import DiscountPadModal from '../components/DiscountPadModal'
import { MODULE_NAME } from '../models'
import Modal from 'common/components/widgets/Modal'
import DiscountReasonRepository from '../repositories/DiscountReasonRepository'
import { listDiscountReasons, selectDiscountReason } from '../actions'
import CartItemDiscount from '../../cart/models/CartItemDiscount'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    const reasons = await DiscountReasonRepository.findForItems()
    dispatch(listDiscountReasons(reasons))
    dispatch(selectDiscountReason(null))
  },
  onSelectDiscountReason: async (discountReason) => {
    dispatch(selectDiscountReason(discountReason))
  }
})

const mapStateToProps = state => ({
  discountReasons: state[MODULE_NAME].discountReasons,
  selectedDiscountReason: state[MODULE_NAME].selectedDiscountReason
})

const discountItemPadModal = connect(mapStateToProps, mapDispatchToProps)(DiscountPadModal)
export default discountItemPadModal

export const showDiscountItemPadModal = (discountType, maxValue, onApply, onCancel) => {
  Modal.open(discountItemPadModal, {
    props: {
      onComplete: (amount, discountType, discountReason) => {
        const discount = CartItemDiscount.createWithDiscountReason(
          amount, discountType, discountReason.id, discountReason.name)
        onApply(discount)
      },
      onCancel: () => {
        onCancel(discountType)
      },
      discountType,
      maxValue
    }
  })
}
