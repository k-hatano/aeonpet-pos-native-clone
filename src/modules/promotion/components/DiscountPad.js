import React from 'react'
import { Text, View, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/DiscountPad'
// import baseStyle from 'common/styles/baseStyle'
import AmountPad, { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import { DISCOUNT_TYPE } from '../models'

class DiscountReasonListItem extends React.Component {
  static propTypes = {
    discountReason: PropTypes.shape({
      name: PropTypes.string
    }),
    isChecked: PropTypes.bool,
    onSelect: PropTypes.func
  }

  render () {
    return (
      <TouchableOpacitySfx onPress={this.props.onSelect}>
        <View style={styles.listItemLayout}>
          <Text style={styles.reasonNameText}>
            {this.props.discountReason.name}
          </Text>
          {!this.props.isChecked ? null : (
            <View style={{backgroundColor: '#ff9024', height: 20, width: 20}} />
          )}
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export default class DiscountPad extends React.Component {
  static propTypes = {
    discountReasons: PropTypes.array,
    selectedDiscountReason: PropTypes.object,
    onSelectDiscountReason: PropTypes.func,
    onComplete: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    discountType: PropTypes.number,
    maxValue: PropTypes.number
  }

  _dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

  _complete (amount) {
    if (this.props.selectedDiscountReason) {
      if (this.props.discountType === DISCOUNT_TYPE.RATE) {
        amount = amount / 100
      }
      this.props.onComplete(amount, this.props.discountType, this.props.selectedDiscountReason)
    } else if (amount === 0 && this.props.onCancel) {
      this.props.onCancel()
    }
  }

  render () {
    let mode = 0
    if (this.props.discountType === DISCOUNT_TYPE.RATE) {
      mode = AMOUNT_PAD_MODE.DISCOUNT_PERCENT
    } else if (this.props.discountType === DISCOUNT_TYPE.AMOUNT) {
      mode = AMOUNT_PAD_MODE.CASH
    }

    // TODO 翻訳対応
    return (
      <View style={styles.layoutRoot}>
        <AmountPad
          mode={mode}
          maxValue={this.props.maxValue}
          onComplete={(amount) => this._complete(amount)}
          disableConfirmButton={!this.props.selectedDiscountReason} />
        <View style={styles.discountListArea}>
          <View style={styles.discountListHeader}>
            <Text style={styles.discountHeaderText}>
              {'値割引理由'}
            </Text>
          </View>
          <ListView
            enableEmptySections
            dataSource={this._dataSource.cloneWithRows(this.props.discountReasons)}
            renderRow={(discountReason) => (
              <DiscountReasonListItem
                discountReason={discountReason}
                isChecked={
                  this.props.selectedDiscountReason &&
                  discountReason.id === this.props.selectedDiscountReason.id}
                onSelect={() => this.props.onSelectDiscountReason(discountReason)} />
            )}
            renderSeparator={(sectionId, rowId) => (
              <View key={rowId} style={styles.separator} />
            )} />
        </View>
      </View>
    )
  }
}
