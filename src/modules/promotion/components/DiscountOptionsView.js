import React, { Component } from 'react'
import { Text, View, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import Decimal from 'decimal.js'
import styles from '../styles/DiscountOptionsView'
// import baseStyle from 'common/styles/baseStyle'
import baseStyleValues from '../../../common/styles/baseStyleValues'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import { DISCOUNT_TARGET_TYPE, DISCOUNT_TYPE, formatDiscountValue, OWNER_DISCOUNT, PRICE_TYPE } from '../models'
import CustomPropTypes from '../../../common/components/CustomPropTypes'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import { showDiscountItemPadModal } from '../containers/DiscountItemPadModal'
import logger from 'common/utils/logger'
import { showDiscountSubtotalPadModal } from '../containers/DiscountSubtotalPadModal'
import AlertView from '../../../common/components/widgets/AlertView'
import { ProceedButton } from '../../../common/components/elements/StandardButton'
import { PROMOTION_TYPE } from 'modules/promotion/models'

class OptionItem extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    label: PropTypes.string,
    isChecked: PropTypes.bool,
    isDiscountable: PropTypes.bool
  }

  constructor (props) {
    super(props)
  }

  render () {
    const isDiscountable = this.props.isDiscountable
    return (
      <TouchableOpacitySfx
        onPress={this.props.onPress}
        disabled={!isDiscountable} >
        <View style={styles.item}>
          <View style={styles.discountLabelSet}>
            {!this.props.isChecked ? null : (
              <View style={{height: 30, width: 30, backgroundColor: baseStyleValues.mainColor, marginRight: 8}} />
            )}
            <Text style={isDiscountable ? styles.itemText : styles.itemDisableText}>
              {this.props.label}
            </Text>
          </View>
          {!this.props.isChecked ? null : (
            <ProceedButton
              onPress={this.props.onClear}
              style={styles.clearButton}
              text={I18n.t('cart.clear')}
            />
          )}
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export default class DiscountOptionsView extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    discountAmount: PropTypes.number,
    ownerDiscountAmount: PropTypes.number,
    maxDiscountAmount: PropTypes.number,
    onApplyDiscount: PropTypes.func,
    onCancelDiscount: PropTypes.func,
    discountRate: PropTypes.number,
    maxDiscountRate: PropTypes.number,
    onChangePrice: PropTypes.func,
    onChangeOriginalPrice: PropTypes.func,
    minChangePrice: PropTypes.instanceOf(Decimal),
    promotionPresets: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      discount_type: PropTypes.number.isRequired,
      discount_value: PropTypes.number.isRequired
    })).isRequired,
    onSelectPromotionPreset: PropTypes.func,
    currency: PropTypes.string,
    style: CustomPropTypes.Style,
    discountTargetType: PropTypes.number,
    isDiscountable: PropTypes.bool,
    disableDiscountReasonMessage: PropTypes.string,
    isChangedPrice: PropTypes.bool,
    isChangedOriginalPrice: PropTypes.bool,
    disableChangePrice: PropTypes.bool,
    disableChangePriceReasonMessage: PropTypes.string
  }

  static defaultProps = {
    isDiscountable: true
  }

  constructor (props) {
    super(props)
    this._dataSource = new ListView.DataSource({
      rowHasChanged: (a, b) => a !== b
    })
  }

  componentDidMount () {
    this.props.onInit && this.props.onInit()
  }

  _promotionPresetsToLabel (promotionPreset) {
    const discountValue = formatDiscountValue(
      promotionPreset.discount_type,
      promotionPreset.discount_value,
      this.props.currency
    )
    return `${promotionPreset.title} (${discountValue})`
  }

  _rateDiscountLabel () {
    const discountValue = formatDiscountValue(
      DISCOUNT_TYPE.RATE,
      this.props.discountRate,
      this.props.currency
    )
    return I18n.t('promotion.rate_discount') + ` (${discountValue})`
  }

  _amountDiscountLabel () {
    const discountValue = formatDiscountValue(
      DISCOUNT_TYPE.AMOUNT,
      this.props.discountAmount,
      this.props.currency
    )
    return I18n.t('promotion.amount_discount') + ` (${discountValue})`
  }

  // 画面表示データ
  _ownerDiscountLabel () {
    const discountValue = formatDiscountValue(
      DISCOUNT_TYPE.RATE,
      OWNER_DISCOUNT.RATE,
      this.props.currency
    )
    return I18n.t('promotion.owner_discount') + ` (${discountValue})`
  }

  _openDiscountAmountView () {
    if (this.props.disableDiscountReasonMessage) {
      AlertView.show(this.props.disableDiscountReasonMessage)
      return
    }
    switch (this.props.discountTargetType) {
      case DISCOUNT_TARGET_TYPE.SUBTOTAL:
        showDiscountSubtotalPadModal(DISCOUNT_TYPE.AMOUNT, this.props.maxDiscountAmount, (discount) => {
          this.props.onApplyDiscount(discount)
        }, () => {
          this.props.onCancelDiscount && this.props.onCancelDiscount(DISCOUNT_TYPE.AMOUNT)
        })
        break

      case DISCOUNT_TARGET_TYPE.ITEM:
        showDiscountItemPadModal(DISCOUNT_TYPE.AMOUNT, this.props.maxDiscountAmount, (discount) => {
          this.props.onApplyDiscount(discount)
        }, () => {
          this.props.onCancelDiscount && this.props.onCancelDiscount(DISCOUNT_TYPE.AMOUNT)
        })
        break

      default:
        logger.warnning('DiscountOptionsView._openDiscountAmountView - Invalid discountTargetType')
    }
  }

  _openDiscountRateView () {
    if (this.props.disableDiscountReasonMessage) {
      AlertView.show(this.props.disableDiscountReasonMessage)
      return
    }
    switch (this.props.discountTargetType) {
      case DISCOUNT_TARGET_TYPE.SUBTOTAL:
        showDiscountSubtotalPadModal(DISCOUNT_TYPE.RATE, this.props.maxDiscountRate, (discount) => {
          this.props.onApplyDiscount(discount)
        }, () => {
          this.props.onCancelDiscount && this.props.onCancelDiscount(DISCOUNT_TYPE.RATE)
        })
        break

      case DISCOUNT_TARGET_TYPE.ITEM:
        showDiscountItemPadModal(DISCOUNT_TYPE.RATE, this.props.maxDiscountRate, (discount) => {
          this.props.onApplyDiscount(discount)
        }, () => {
          this.props.onCancelDiscount && this.props.onCancelDiscount(DISCOUNT_TYPE.RATE)
        })
        break

      default:
        logger.warnning('DiscountOptionsView._openDiscountAmountView - Invalid discountTargetType')
    }
  }

  // オーナー割引プリセット(任意の割引データ作成)
  _ownerDiscount () {
    const ownerDiscountPreset = {
      promotion_type: PROMOTION_TYPE.SUBTOTAL,
      promotion_preset_code: OWNER_DISCOUNT.CODE,
      discount_type: DISCOUNT_TYPE.RATE,
      discount_value: new Decimal(OWNER_DISCOUNT.RATE)
    }
    this._applyPromotionPreset(ownerDiscountPreset)
  }

  _openChangePricePad (changePriceType) {
    if (this.props.disableChangePrice) {
      AlertView.show(this.props.disableChangePriceReasonMessage)
    } else {
      AmountPadModal.open({
        mode: AMOUNT_PAD_MODE.CASH,
        onComplete: (amount) => {
          this._applyPriceChange(amount, changePriceType)
          return true
        }
      })
    }
  }

  _applyPriceChange (amount, changePriceType) {
    switch (changePriceType) {
      case PRICE_TYPE.SALE_PRICE:
        this.props.onChangePrice(amount)
        break
      case PRICE_TYPE.ORIGINAL_PRICE:
        this.props.onChangeOriginalPrice(amount)
        break
      default:
        break
    }
  }

  _applyPromotionPreset (promotionPreset) {
    if (this.props.disableDiscountReasonMessage) {
      AlertView.show(this.props.disableDiscountReasonMessage)
      return
    }
    this.props.onSelectPromotionPreset(promotionPreset)
  }

  _clearAmountDiscount () {
    this.props.onCancelDiscount(DISCOUNT_TYPE.AMOUNT)
  }

  _clearRateDiscount () {
    this.props.onCancelDiscount(DISCOUNT_TYPE.RATE)
  }

  _clearOwnerDiscount () {
    this.props.onCancelDiscount(DISCOUNT_TYPE.OWNER)
  }

  _resetChangedPrice () {
    this.props.onChangePrice(this.props.minChangePrice.toNumber())
  }

  _resetChangedOriginalPrice () {
    this.props.onChangeOriginalPrice(this.props.minChangePrice.toNumber())
  }

  render () {
    const isDiscountable = this.props.isDiscountable || this.props.disableDiscountReasonMessage
    return (
      <View style={[styles.container, this.props.style]}>
        <OptionItem
          label={this._amountDiscountLabel()}
          onPress={() => this._openDiscountAmountView()}
          onClear={() => this._clearAmountDiscount()}
          isChecked={!!this.props.discountAmount}
          isDiscountable={isDiscountable} />
        <View style={styles.separator} />
        <OptionItem
          label={this._rateDiscountLabel()}
          onPress={() => this._openDiscountRateView()}
          isChecked={!!this.props.discountRate}
          onClear={() => this._clearRateDiscount()}
          isDiscountable={isDiscountable} />
        <View style={styles.separator} />
        {this.props.discountTargetType === DISCOUNT_TARGET_TYPE.ITEM ? null : (
          <OptionItem
            label={this._ownerDiscountLabel()}
            onPress={() => this._ownerDiscount()}
            isChecked={!!this.props.ownerDiscountAmount}
            onClear={() => this._clearOwnerDiscount()}
            isDiscountable={isDiscountable} />
        )}
        {!this.props.onChangePrice ? null : (
          <View>
            <OptionItem
              label={I18n.t('cart.change_original_price')}
              onPress={() => this._openChangePricePad(PRICE_TYPE.ORIGINAL_PRICE)}
              isChecked={this.props.isChangedOriginalPrice}
              onClear={() => this._resetChangedOriginalPrice()}
              isDiscountable />
            <View style={styles.separator} />
            <OptionItem
              label={I18n.t('cart.change_price')}
              onPress={() => this._openChangePricePad(PRICE_TYPE.SALE_PRICE)}
              isChecked={this.props.isChangedPrice}
              onClear={() => this._resetChangedPrice()}
              isDiscountable />
          </View>
        )}
        <View style={styles.separator} />
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>{I18n.t('promotion.promotion_preset')}</Text>
        </View>
        <ListView
          enableEmptySections
          dataSource={this._dataSource.cloneWithRows(this.props.promotionPresets)}
          renderRow={(promotionPreset) => (
            <OptionItem
              label={this._promotionPresetsToLabel(promotionPreset)}
              onPress={() => this._applyPromotionPreset(promotionPreset)}
              isDiscountable={isDiscountable} />
          )}
          renderSeparator={(sectionId, rowId) => (
            <View key={rowId} style={styles.separator} />
          )}
        />
      </View>
    )
  }
}
