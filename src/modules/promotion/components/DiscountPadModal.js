import React from 'react'
// import { } from 'react-native'
// import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/DiscountPadModal'
// import baseStyle from 'common/styles/baseStyle'
import DiscountPad from './DiscountPad'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import Modal from '../../../common/components/widgets/Modal'

// TODO 割引周りのUIが複雑化しすぎているので、要リファクタリング

export default class DiscountPadModal extends React.Component {
  static propTypes = {
    format: PropTypes.number,
    maxValue: PropTypes.number,
    justValue: PropTypes.number,
    discountType: PropTypes.number,
    discountReasons: PropTypes.array,
    selectedDiscountReason: PropTypes.object,
    onSelectDiscountReason: PropTypes.func,
    onComplete: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    title: PropTypes.string,
    onInit: PropTypes.func
  }

  async componentWillMount () {
    this.props.onInit && await this.props.onInit()
  }

  render () {
    const { title, onComplete, onCancel, ...passProps } = this.props
    const onCompleteForOverride = (...args) => {
      onComplete(...args)
      Modal.close()
    }

    const onCancelForOverride = (...args) => {
      onCancel(...args)
      Modal.close()
    }

    return (
      <ModalFrame style={styles.layoutRoot} title={title} onClose={() => Modal.close()} >
        <DiscountPad onComplete={onCompleteForOverride} onCancel={onCancelForOverride} {...passProps} />
      </ModalFrame>
    )
  }
}
