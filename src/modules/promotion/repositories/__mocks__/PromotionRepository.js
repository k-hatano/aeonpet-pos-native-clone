import SamplePromotionRepository, { promotionIds } from '../sample/SamplePromotionRepository'

export { promotionIds }

export default class PromotionRepositoryMock {
  static _implement = new SamplePromotionRepository()

  static async findBundlesByProductId (productId) {
    return this._implement.findBundlesByProductId(productId)
  }

  static async findSalesByProductId (productId) {
    return this._implement.findSalesByProductId(productId)
  }

  static async createSamples () {
    return this._implement.createSamples()
  }
}
