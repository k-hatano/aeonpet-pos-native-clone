import SamplePromotionPresetRepository from './sample/SamplePromotionPresetRepository'
import StandardPromotionPresetRepository from './standard/StandardPromotionPresetRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class PromotionPresetRepository {
  static _implement = new SamplePromotionPresetRepository()

  static async findByPromotionPresetType (type) {
    return this._implement.findByPromotionPresetType(type)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardPromotionPresetRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardPromotionPresetRepository()
        break

      default:
        this._implement = new SamplePromotionPresetRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('PromotionPresetRepository', (context) => {
  PromotionPresetRepository.switchImplement(context)
})
