import { sampleProductTags } from 'modules/product/samples'
import { sampleBundles, sampleSales, sampleCoupons, sampleProductPointRatios, sampleShopPointRatios, samplePromotionMessages } from '../../samples'

export default class SamplePromotionRepository {
  async findBundlesByProductId (productId) {
    const productTagIds = this._getProductTagIdsByProductId(productId)
    return sampleBundles
      .filter(bundle => bundle.bundle_patterns.some(pattern => (
        productTagIds.includes(pattern.product_tag_id)
      )))
  }

  async findSalesByProductId (productId) {
    const productTagIds = this._getProductTagIdsByProductId(productId)
    return sampleSales
      .filter(sale => productTagIds.includes(sale.product_tag_id))
  }

  async findProductPointRatioByProductId (productId) {
    const ratios = sampleProductPointRatios
      .filter(ratio => ratio.product_id === productId)
    return ratios.length === 0 ? null : ratios[0]
  }

  /**
   * ランダムで店舗ポイントキャンペーンを有効化させる
   */ 
  async findTodaysShopPointRatio () {
    const rand = Math.floor(Math.random() * 100) % 2
    return rand === 0 ? sampleShopPointRatios[0] : null
  }

  async findBundlesByBarcode (barcode) {
    return sampleBundles.filter(bundle => bundle.barcode === barcode)
  }
  
  async findSalesByBarcode (barcode) {
    return sampleSales.filter(sales => sales.barcode === barcode)
  }

  async findCouponsByBarcode (barcode) {
    return sampleCoupons.filter(coupon => coupon.barcode === barcode)
  }

  async findPromotionMesseages () {
    return samplePromotionMessages.find()
  }

  _getProductTagIdsByProductId (productId) {
    return sampleProductTags
      .filter(productTag => (productTag.products.includes(productId)))
      .map(productTag => (productTag.id))
  }
}
