import { DISCOUNT_TYPE, PROMOTION_PRESET_TYPE } from '../../models'
import generateUuid from 'common/utils/generateUuid'

export const samplePromotionPresets = [
  {
    id: generateUuid(),
    title: '雨の日割引',
    promotion_preset_code: 'PREMEM01',
    promotion_preset_type: PROMOTION_PRESET_TYPE.SUBTOTAL,
    discount_type: DISCOUNT_TYPE.RATE,
    discount_value: 0.1,
    discount_reason_id: null
  },
  {
    id: generateUuid(),
    title: 'DM特典',
    promotion_preset_code: 'PREMEM02',
    promotion_preset_type: PROMOTION_PRESET_TYPE.SUBTOTAL,
    discount_type: DISCOUNT_TYPE.AMOUNT,
    discount_value: 100,
    discount_reason_id: null
  },
  {
    id: generateUuid(),
    title: '全店セール',
    promotion_preset_code: 'PREMEM03',
    promotion_preset_type: PROMOTION_PRESET_TYPE.SUBTOTAL,
    discount_type: DISCOUNT_TYPE.AMOUNT,
    discount_value: 1000,
    discount_reason_id: null
  },
  {
    id: generateUuid(),
    title: 'アウトレット',
    promotion_preset_code: 'PREMEM11',
    promotion_preset_type: PROMOTION_PRESET_TYPE.ITEM,
    discount_type: DISCOUNT_TYPE.RATE,
    discount_value: 0.2,
    discount_reason_id: null
  },
  {
    id: generateUuid(),
    title: 'おにぎり20円引き',
    promotion_preset_code: 'PREMEM12',
    promotion_preset_type: PROMOTION_PRESET_TYPE.ITEM,
    discount_type: DISCOUNT_TYPE.AMOUNT,
    discount_value: 20,
    discount_reason_id: null
  }
]

export default class SamplePromotionPresetRepository {
  async findByPromotionPresetType (type) {
    return samplePromotionPresets.filter(preset => preset.promotion_preset_type === type)
  }
}
