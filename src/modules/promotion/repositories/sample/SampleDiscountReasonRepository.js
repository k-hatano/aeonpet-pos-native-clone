import * as _ from 'underscore'
import { DISCOUNT_REASON_TYPE } from '../../models'
import generateUuid from '../../../../common/utils/generateUuid'

export const sampleDiscountReasonMap = {
  common1: {
    id: generateUuid(),
    name: '共通割引理由1',
    discount_reason_code: 'common1',
    discount_reason_type: DISCOUNT_REASON_TYPE.COMMON,
    sort_order: 1
  },
  common2: {
    id: generateUuid(),
    name: '共通割引理由2',
    discount_reason_code: 'common2',
    discount_reason_type: DISCOUNT_REASON_TYPE.COMMON,
    sort_order: 2
  },
  item1: {
    id: generateUuid(),
    name: '個別割引理由1',
    discount_reason_code: 'item1',
    discount_reason_type: DISCOUNT_REASON_TYPE.ITEM,
    sort_order: 3
  },
  item2: {
    id: generateUuid(),
    name: '個別割引理由2',
    discount_reason_code: 'item2',
    discount_reason_type: DISCOUNT_REASON_TYPE.ITEM,
    sort_order: 4
  },
  item3: {
    id: generateUuid(),
    name: '個別割引理由3',
    discount_reason_code: 'item3',
    discount_reason_type: DISCOUNT_REASON_TYPE.ITEM,
    sort_order: 5
  },
  subtotal1: {
    id: generateUuid(),
    name: '全体割引理由1',
    discount_reason_code: 'subtotal1',
    discount_reason_type: DISCOUNT_REASON_TYPE.SUBTOTAL,
    sort_order: 6
  },
  subtotal2: {
    id: generateUuid(),
    name: '全体割引理由2',
    discount_reason_code: 'subtotal2',
    discount_reason_type: DISCOUNT_REASON_TYPE.SUBTOTAL,
    sort_order: 7
  },
  subtotal3: {
    id: generateUuid(),
    name: '全体割引理由3',
    discount_reason_code: 'subtotal3',
    discount_reason_type: DISCOUNT_REASON_TYPE.SUBTOTAL,
    sort_order: 8
  }
}

export const sampleDiscountReasons = _.values(sampleDiscountReasonMap)

export default class SampleDiscountReasonRepository {
  async findForItems () {
    return sampleDiscountReasons.filter(reason =>
      reason.discount_reason_type === DISCOUNT_REASON_TYPE.COMMON ||
      reason.discount_reason_type === DISCOUNT_REASON_TYPE.ITEM)
  }

  async findForSubtotal () {
    return sampleDiscountReasons.filter(reason =>
      reason.discount_reason_type === DISCOUNT_REASON_TYPE.COMMON ||
      reason.discount_reason_type === DISCOUNT_REASON_TYPE.SUBTOTAL)
  }

  async findById (discountReasonId) {
    return sampleDiscountReasons.filte(reason => reason.id === discountReasonId)
  }
}
