import SamplePromotionRepository from '../SamplePromotionRepository'
import { sampleProductMap } from 'modules/product/samples'
import { sampleBundleMap, sampleSaleMap } from '../../../samples'

describe('Test SamplePromotionRepository', () => {
  it ('Create samples', () => {
    new SamplePromotionRepository()
  })

  it ('Find bundle by productId A', async () => {
    const repository = new SamplePromotionRepository()
    const promotions = await repository.findBundlesByProductId(sampleProductMap.bundle_s_1_1.id)

    expect(promotions.length).toEqual(3)
    expect(promotions[0].id).toEqual(sampleBundleMap.selection1.id)
  })

  it ('Find bundle by productId B', async () => {
    const repository = new SamplePromotionRepository()
    const promotions = await repository.findBundlesByProductId(sampleProductMap.bundle_s_2_A_1.id)

    expect(promotions.length).toEqual(1)
    expect(promotions[0].id).toEqual(sampleBundleMap.selection2.id)
  })

  it ('Find bundle by productId not found', async () => {
    const repository = new SamplePromotionRepository()
    const promotions = await repository.findBundlesByProductId(sampleProductMap.standardC.id)

    expect(promotions.length).toEqual(0)
  })

  it ('Find sales by productId B', async () => {
    const repository = new SamplePromotionRepository()
    const promotions = await repository.findSalesByProductId(sampleProductMap.sale_a_1.id)

    expect(promotions.length).toEqual(1)
    expect(promotions[0].id).toEqual(sampleSaleMap.saleA.id)
  })
})
