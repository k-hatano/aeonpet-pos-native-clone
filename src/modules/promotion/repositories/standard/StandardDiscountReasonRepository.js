import DiscountReasonEntity from '../entities/DiscountReasonEntity'
import AppEvents from 'common/AppEvents'
import { sampleDiscountReasons } from '../sample/SampleDiscountReasonRepository'
import { DISCOUNT_REASON_TYPE } from '../../models'

export default class StandardDiscountReasonRepository {
  async findForItems () {
    return DiscountReasonEntity.findAll({
      where: {
        discount_reason_type: {
          $in: [DISCOUNT_REASON_TYPE.COMMON, DISCOUNT_REASON_TYPE.ITEM]
        }
      },
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async findForSubtotal () {
    return DiscountReasonEntity.findAll({
      where: {
        discount_reason_type: {
          $in: [DISCOUNT_REASON_TYPE.COMMON, DISCOUNT_REASON_TYPE.SUBTOTAL]
        }
      },
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async findById (discountReasonId) {
    return DiscountReasonEntity.find({
      where: {
        id: discountReasonId
      },
      raw: true
    })
  }

  async createSamples () {
    const existingCount = await DiscountReasonEntity.count()
    if (existingCount === 0) {
      await DiscountReasonEntity.bulkCreate(sampleDiscountReasons)
    }
  }
}

AppEvents.onSampleDataCreate('StandardDiscountReasonRepository', async () => {
  return (new StandardDiscountReasonRepository()).createSamples()
})
