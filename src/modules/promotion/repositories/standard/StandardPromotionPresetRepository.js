import AppEvents from 'common/AppEvents'
import PromotionPresetEntity from '../entities/PromotionPresetEntity'
import { samplePromotionPresets } from '../sample/SamplePromotionPresetRepository'

export default class StandardPromotionPresetRepository {
  async findByPromotionPresetType (type) {
    return PromotionPresetEntity.findAll({
      where: {
        promotion_preset_type: type
      }
    })
  }

  async createSamples () {
    if (await PromotionPresetEntity.count() === 0) {
      PromotionPresetEntity.bulkCreate(samplePromotionPresets)
    }
  }
}

AppEvents.onSampleDataCreate('StandardPromotionPresetRepository', async () => {
  return (new StandardPromotionPresetRepository()).createSamples()
})
