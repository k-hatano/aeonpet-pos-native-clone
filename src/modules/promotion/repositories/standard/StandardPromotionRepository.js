import * as _ from 'underscore'
import BundleEntity from '../entities/BundleEntity'
import BundlePatternEntity from '../entities/BundlePatternEntity'
import AppEvents from 'common/AppEvents'
import { sampleSales, sampleBundles } from '../../samples'
import SaleDiscountEntity from '../entities/SaleDiscountEntity'
import CouponDiscountEntity from '../entities/CouponDiscountEntity'
import ProductProductTagEntity from '../../../product/repositories/entities/ProductProductTagEntity'
import moment from 'moment'
import ProductPointRatio from '../entities/ProductPointRatioEntity'
import ShopPointRatio from '../entities/ShopPointRatioEntity'
import PromotionMessages from '../entities/PromotionMessagesEntity'
import { ISO_WEEK } from 'common/models'
import { APPLIED_TYPE, POINT_TYPE } from 'modules/promotion/models'

export default class StandardPromotionRepository {
  async findBundlesByProductId (productId) {
    let bundleModels = await BundleEntity.findAll({
      include: [{
        model: BundlePatternEntity,
        as: 'bundle_patterns',
        required: true,
        include: [{
          model: ProductProductTagEntity,
          as: 'bundle_item',
          required: true,
          foreignKey: 'product_tag_id',
          targetKey: 'product_tag_id',
          where: {
            product_id: productId
          }
        }]
      }]
    })
    const nowDate = moment().format('YYYY-MM-DD')
    const nowTimeStamp = moment(nowDate).unix()
    // bundle_itemが絞り込まれてしまっているので、再度検索
    bundleModels = await BundleEntity.findAll({
      where: {
        id: {
          $in: bundleModels.map(bundle => bundle.id)
        },
        start_at: {
          $lte: nowTimeStamp
        },
        $or: [
          {
            end_at: null
          },
          {
            end_at: {
              $gte: nowTimeStamp
            }
          }
        ]
      },
      include: [{
        model: BundlePatternEntity,
        as: 'bundle_patterns',
        required: true,
        include: [{
          model: ProductProductTagEntity,
          as: 'bundle_item',
          required: true,
          foreignKey: 'product_tag_id',
          targetKey: 'product_tag_id'
        }]
      }]
    })
    return bundleModels.map(bundle => this._formatBundleToDto(bundle))
  }

  _formatBundleToDto (model) {
    const bundle = model.dataValues
    bundle.bundle_patterns = model.bundle_patterns.map(pattern => pattern.dataValues)
    return bundle
  }

  async findSalesByProductId (productId) {
    const nowDate = moment().format('YYYY-MM-DD')
    const nowTimeStamp = moment(nowDate).unix()
    const sales = await SaleDiscountEntity.findAll({
      where: {
        start_at: {
          $lte: nowTimeStamp
        },
        $or: [
          {
            end_at: null
          },
          {
            end_at: {
              $gte: nowTimeStamp
            }
          }
        ]
      },
      include: [{
        model: ProductProductTagEntity,
        as: 'sale_item',
        required: true,
        foreignKey: 'product_tag_id',
        targetKey: 'product_tag_id',
        where: {
          product_id: productId
        }
      }],
      raw: true
    })
    return sales
  }

  async findProductPointRatioByProductId (productId) {
    return ProductPointRatio.find({
      ...this._getPromotionPointCondition(POINT_TYPE.AEON_PET_POINT),
      include: [{
        model: ProductProductTagEntity,
        as: 'promotion_point_items',
        required: true,
        foreignKey: 'product_tag_id',
        targetKey: 'product_tag_id',
        where: {
          product_id: productId
        }
      }],
      raw: true
    })
  }

  async findTodaysShopPointRatio (pointType) {
    return ShopPointRatio.find({
      ...this._getPromotionPointCondition(pointType),
      raw: true
    })
  }

  _getPromotionPointCondition (pointType) {
    const now = moment().format('YYYY-MM-DD')
    const nowTimeStamp = moment(now).unix()
    const dayOfMonth = moment().date()
    return {
      where: {
        $and: [
          {
            point_type: pointType
          },
          {
            start_at: {
              $lte: nowTimeStamp
            }
          },
          {
            end_at: {
              $or: {
                $eq: null,
                $gte: nowTimeStamp
              }
            }
          },
          {
            $or: [
              {
                applied_type: APPLIED_TYPE.WEEK,
                ...this._getWeekCondition()
              },
              {
                applied_type: APPLIED_TYPE.DAY,
                $or: {
                  day_1: dayOfMonth,
                  day_2: dayOfMonth,
                  day_3: dayOfMonth,
                  day_4: dayOfMonth,
                  day_5: dayOfMonth
                }
              }
            ]
          }
        ]
      },
      order: [
        ['assigned_ratio', 'DESC']
      ]
    }
  }

  _getWeekCondition () {
    switch (moment().isoWeekday()) {
      case ISO_WEEK.MONDAY:
        return {
          on_monday: 1
        }
      case ISO_WEEK.TUESDAY:
        return {
          on_tuesday: 1
        }
      case ISO_WEEK.WEDNESDAY:
        return {
          on_wednesday: 1
        }
      case ISO_WEEK.THURSDAY:
        return {
          on_thursday: 1
        }
      case ISO_WEEK.FRIDAY:
        return {
          on_friday: 1
        }
      case ISO_WEEK.SATURDAY:
        return {
          on_saturday: 1
        }
      case ISO_WEEK.SUNDAY:
        return {
          on_sunday: 1
        }
    }
  }

  async findBundlesByBarcode (barcode) {
    const bundleEntities = await BundleEntity.find({
      where: {
        barcode: barcode
      }
    })
    return bundleEntities
  }

  async findSalesByBarcode (barcode) {
    const saleDiscountEntities = await SaleDiscountEntity.find({
      where: {
        barcode: barcode
      }
    })
    return saleDiscountEntities
  }

  async findCouponsByBarcode (barcode) {
    const nowDate = moment().format('YYYY-MM-DD')
    const nowTimeStamp = moment(nowDate).unix()
    const coupons = await CouponDiscountEntity.findAll({
      where: {
        barcode: barcode,
        start_at: {
          $lte: nowTimeStamp
        },
        $or: [
          {
            end_at: null
          },
          {
            end_at: {
              $gte: nowTimeStamp
            }
          }
        ]
      },
      include: [
        {
          model: ProductProductTagEntity,
          as: 'coupon_item',
          required: true,
          foreignKey: 'product_tag_id',
          targetKey: 'product_tag_id'
        }
      ],
      raw: true
    })
    return coupons
  }

  async findPromotionMesseages () {
    return PromotionMessages.findAll({
      ...this._getTodaysPromotionMessagesCondition(),
      raw: true
    })
  }

  _getTodaysPromotionMessagesCondition () {
    const now = moment().format('YYYY-MM-DD')
    const nowTimeStamp = moment(now).unix()
    const displayNum = 3
    return {
      where: {
        $and: [
          {
            start_at: {
              $lte: nowTimeStamp
            }
          },
          {
            end_at: {
              $or: {
                $eq: null,
                $gte: nowTimeStamp
              }
            }
          },
          {
            $or: [
              {
                ...this._getWeekCondition()
              }
            ]
          }
        ]
      },
      order: [
        ['priority', 'ASC']
      ],
      limit: displayNum
    }
  }

  async createSamples () {
    if (await BundleEntity.count() === 0) {
      await BundleEntity.bulkCreate(sampleBundles)
    }

    if (await BundlePatternEntity.count() === 0) {
      await BundlePatternEntity.bulkCreate(_.flatten(sampleBundles.map(bundle => bundle.bundle_patterns)))
    }

    if (await SaleDiscountEntity.count() === 0) {
      await SaleDiscountEntity.bulkCreate(sampleSales)
    }
  }
}

AppEvents.onSampleDataCreate('StandardPromotionRepository', async () => {
  return (new StandardPromotionRepository()).createSamples()
})
