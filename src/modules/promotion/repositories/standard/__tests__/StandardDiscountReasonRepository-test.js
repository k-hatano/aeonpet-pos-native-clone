// jest.mock('path/to/class')

import StandardDiscountReasonRepository from '../StandardDiscountReasonRepository'

import Migrator from 'common/utils/migrator'
import { sampleDiscountReasonMap } from '../../sample/SampleDiscountReasonRepository'

describe('StandardDiscountReasonRepository Test Sample', () => {
  beforeEach(async () => {
    await Migrator.up()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('findForSubtotal', async () => {
    const repository = new StandardDiscountReasonRepository()
    await repository.createSamples()
    const reasons = await repository.findForSubtotal()
    expect(reasons.length).toBe(5)
    expect(reasons[0].discount_reason_code).toBe(sampleDiscountReasonMap.common1.discount_reason_code)
    expect(reasons[2].discount_reason_code).toBe(sampleDiscountReasonMap.subtotal1.discount_reason_code)
  })

  it('findForItems', async () => {
    const repository = new StandardDiscountReasonRepository()
    await repository.createSamples()
    const reasons = await repository.findForItems()
    expect(reasons.length).toBe(5)
    expect(reasons[0].discount_reason_code).toBe(sampleDiscountReasonMap.common1.discount_reason_code)
    expect(reasons[2].discount_reason_code).toBe(sampleDiscountReasonMap.item1.discount_reason_code)
  })
})
