// jest.mock('path/to/class')

import StandardPromotionPresetRepository from '../StandardPromotionPresetRepository'

import Migrator from 'common/utils/migrator'
import { PROMOTION_PRESET_TYPE } from '../../../models'

describe('StandardPromotionPresetRepository Test', () => {
  beforeEach(async () => {
    await Migrator.up()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('', async () => {
    const repository = new StandardPromotionPresetRepository()
    await repository.createSamples()
    const forItem = await repository.findByPromotionPresetType(PROMOTION_PRESET_TYPE.ITEM)
    expect(forItem.length).toBe(2)
  })
})
