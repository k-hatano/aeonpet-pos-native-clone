import configureMockStore from 'redux-mock-store'
const middlewares = []
const mockStore = configureMockStore(middlewares)
const initialState = {}
const store = mockStore(initialState)



import StandardPromotionRepository from '../StandardPromotionRepository'

import Migrator from 'common/utils/migrator'
import { sampleProductMap } from 'modules/product/samples'
import { sampleSaleMap } from '../../../samples'
import StandardProductTagRepository from 'modules/product/repositories/standard/StandardProductTagRepository'

import { sequelize } from 'common/DB'
import { sampleBundleMap } from '../../../samples'

describe('StandardPromotionRepository Test', () => {
  beforeEach(async () => {
    await Migrator.up()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('findBundlesByProductId', async () => {
    const repository = new StandardPromotionRepository()
    await repository.createSamples()
    await (new StandardProductTagRepository()).createSamples()
    const bundles = await repository.findBundlesByProductId(sampleProductMap.bundle_s_1_1.id)
    expect(bundles.length).toBe(3)
    // expect(bundles[0].id).toBe(sampleBundleMap.selection1.id)
  })

  it('findBundlesByProductId for two patterns', async () => {
    const repository = new StandardPromotionRepository()
    await repository.createSamples()
    await (new StandardProductTagRepository()).createSamples()
    const bundles = await repository.findBundlesByProductId(sampleProductMap.bundle_s_2_A_1.id)
    expect(bundles[0].bundle_patterns.length).toBe(2)
  })

  it ('findSalesByProductId', async () => {
    const repository = new StandardPromotionRepository()
    await repository.createSamples()
    await (new StandardProductTagRepository()).createSamples()
    const sales = await repository.findSalesByProductId(sampleProductMap.sale_a_1.id)
    expect(sales.length).toBe(1)
    expect(sales[0].id).toBe(sampleSaleMap.saleA.id)
  })
})
