import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const PromotionMessages = sequelize.define(
  'PromotionMessages',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    status: Sequelize.INTEGER,
    integrated_incentive_type: Sequelize.INTEGER,
    promotion_type: Sequelize.INTEGER,
    promotion_code: Sequelize.STRING,
    priority: Sequelize.INTEGER,
    start_at: Sequelize.INTEGER,
    end_at: Sequelize.INTEGER,
    start_time: Sequelize.INTEGER,
    end_time: Sequelize.INTEGER,
    on_sunday: Sequelize.BOOLEAN,
    on_monday: Sequelize.BOOLEAN,
    on_tuesday: Sequelize.BOOLEAN,
    on_wednesday: Sequelize.BOOLEAN,
    on_thursday: Sequelize.BOOLEAN,
    on_friday: Sequelize.BOOLEAN,
    on_saturday: Sequelize.BOOLEAN,
    campaign_message: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    deleted_at: Sequelize.DATE },
  {
    tableName: 'promotion_messages',
    underscored: true,
    timestamps: false
  }
)

export default PromotionMessages
