import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import ProductProductTag from 'modules/product/repositories/entities/ProductProductTagEntity'

const ProductPointRatio = sequelize.define(
  'ProductPointRatios',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    product_tag_id: Sequelize.STRING,
    promotion_point_id: Sequelize.STRING,
    point_type: Sequelize.INTEGER,
    applied_type: Sequelize.STRING,
    assigned_ratio: Sequelize.DECIMAL(19, 4),
    start_at: Sequelize.INTEGER,
    end_at: Sequelize.INTEGER,
    start_time: Sequelize.TIME,
    end_time: Sequelize.TIME,
    on_sunday: Sequelize.BOOLEAN,
    on_monday: Sequelize.BOOLEAN,
    on_tuesday: Sequelize.BOOLEAN,
    on_wednesday: Sequelize.BOOLEAN,
    on_thursday: Sequelize.BOOLEAN,
    on_friday: Sequelize.BOOLEAN,
    on_saturday: Sequelize.BOOLEAN,
    day_1: Sequelize.INTEGER,
    day_2: Sequelize.INTEGER,
    day_3: Sequelize.INTEGER,
    day_4: Sequelize.INTEGER,
    day_5: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE },
  {
    tableName: 'product_point_ratios',
    underscored: true,
    timestamps: false
  }
)

ProductPointRatio.hasMany(ProductProductTag, {
  foreignKey: 'product_tag_id',
  sourceKey: 'product_tag_id',
  as: 'promotion_point_items'
})

export default ProductPointRatio
