import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'
import ProductProductTag from 'modules/product/repositories/entities/ProductProductTagEntity'

/**
 * @class BundlePatternEntity
 * @property {string} id
 * @property {string} bundle_id
 * @property {number} bundle_price
 * @property {string} product_tag_id
 * @property {integer} establishment_condition
 * @property {integer} threshold_quantity
 */
const BundlePattern = sequelize.define(
  'BundlePattern',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    bundle_id: Sequelize.STRING,
    incentive_type: Sequelize.INTEGER,
    bundle_price: Sequelize.DECIMAL(19, 4),
    discount_rate: Sequelize.DECIMAL(19, 4),
    product_tag_id: {
      type: Sequelize.STRING,
      // unique: true
    },
    establishment_condition: Sequelize.INTEGER,
    threshold_quantity: Sequelize.DECIMAL(19, 4),
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'bundle_patterns',
    underscored: true,
    timestamps: false,
    scopes: {
      withBundleItems: {
        include: [
          {model: ProductProductTag, as: 'bundle_item'}
        ]
      }
    }
  }
)

BundlePattern.hasMany(ProductProductTag, {
  foreignKey: 'product_tag_id',
  sourceKey: 'product_tag_id',
  as: 'bundle_item'
})

export default BundlePattern
