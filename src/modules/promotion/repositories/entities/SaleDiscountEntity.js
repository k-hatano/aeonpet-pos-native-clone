import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'
import ProductProductTag from 'modules/product/repositories/entities/ProductProductTagEntity'

const SaleDiscount = sequelize.define(
  'SaleDiscount',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    code: Sequelize.STRING,
    barcode: Sequelize.STRING,
    discount_rate: Sequelize.DECIMAL(5, 4),
    discount_price: Sequelize.DECIMAL(19, 4),
    product_tag_id: Sequelize.STRING,
    priority: Sequelize.INTEGER,
    start_at: Sequelize.INTEGER,
    end_at: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    deleted_at: Sequelize.DATE
  }, {
    tableName: 'sale_discounts',
    underscored: true,
    timestamps: false
  }
)

SaleDiscount.hasMany(ProductProductTag, {
  foreignKey: 'product_tag_id',
  sourceKey: 'product_tag_id',
  as: 'sale_item'
})

export default SaleDiscount
