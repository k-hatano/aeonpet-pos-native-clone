import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'
import ProductProductTag from 'modules/product/repositories/entities/ProductProductTagEntity'

const CouponDiscount = sequelize.define(
  'CouponDiscount',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    code: Sequelize.STRING,
    discount_rate: Sequelize.DECIMAL(5, 4),
    discount_price: Sequelize.DECIMAL(19, 4),
    product_tag_id: Sequelize.STRING,
    barcode: Sequelize.STRING,
    threshold_type: Sequelize.INTEGER,
    threshold_min: Sequelize.DECIMAL(19, 4),
    threshold_max: Sequelize.DECIMAL(19, 4),
    start_at: Sequelize.INTEGER,
    end_at: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    deleted_at: Sequelize.DATE
  }, {
    tableName: 'coupon_discounts',
    underscored: true,
    timestamps: false
  }
)

CouponDiscount.hasMany(ProductProductTag, {
  foreignKey: 'product_tag_id',
  sourceKey: 'product_tag_id',
  as: 'coupon_item'
})

export default CouponDiscount
