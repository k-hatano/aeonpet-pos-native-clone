import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import BundlePattern from './BundlePatternEntity'

/**
 * @class BundleEntity
 * @property {string} id
 * @property {string} name
 * @property {string} code
 * @property {string} bundle_riichi_message
 * @property {number} bundle_type
 * @property {Array<BundlePatternEntity>} bundle_patterns
 * @property {number} priority
 */
const Bundle = sequelize.define(
  'Bundle',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    code: Sequelize.STRING,
    barcode: Sequelize.STRING,
    bundle_riichi_message: Sequelize.STRING,
    bundle_type: Sequelize.INTEGER,
    priority: Sequelize.INTEGER,
    start_at: Sequelize.INTEGER,
    end_at: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    deleted_at: Sequelize.DATE
  }, {
    tableName: 'bundles',
    underscored: true,
    timestamps: false,
    scopes: {
      withBundlePatterns: {
        include: [
          {model: BundlePattern, as: 'bundle_patterns'}
        ]
      }
    }
  }
)
Bundle.hasMany(BundlePattern, {foreignKey: 'bundle_id', as: 'bundle_patterns'})

export default Bundle
