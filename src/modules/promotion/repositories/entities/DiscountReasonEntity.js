import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const DiscountReason = sequelize.define(
  'DiscountReason',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    discount_reason_code: Sequelize.STRING,
    discount_reason_type: Sequelize.INTEGER,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'discount_reasons',
    underscored: true,
    timestampe: false
  }
)

export default DiscountReason
