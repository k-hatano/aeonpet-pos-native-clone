import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'

const PromotionPreset = sequelize.define(
  'PromotionPreset',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    title: Sequelize.STRING,
    promotion_preset_code: Sequelize.STRING,
    promotion_preset_type: Sequelize.INTEGER,
    discount_type: Sequelize.INTEGER,
    discount_value: Sequelize.DECIMAL(19, 4),
    discount_reason_id: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'promotion_presets',
    underscored: true,
    timestamps: false
  }
)

export default PromotionPreset
