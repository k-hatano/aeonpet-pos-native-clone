import SamplePromotionRepository from './sample/SamplePromotionRepository'
import StandardPromotionRepository from './standard/StandardPromotionRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class PromotionRepository {
  static _implement = new SamplePromotionRepository()

  static async findBundlesByProductId (productId) {
    return this._implement.findBundlesByProductId(productId)
  }

  static async findSalesByProductId (productId) {
    return this._implement.findSalesByProductId(productId)
  }

  static findProductPointRatioByProductId (productId) {
    return this._implement.findProductPointRatioByProductId(productId)
  }

  static findTodaysShopPointRatio (pointType) {
    return this._implement.findTodaysShopPointRatio(pointType)
  }

  static async findBundlesByBarcode (barcode) {
    return this._implement.findBundlesByBarcode(barcode)
  }
  
  static async findSalesByBarcode (barcode) {
    return this._implement.findSalesByBarcode(barcode)
  }

  static async findCouponsByBarcode (barcode) {
    return this._implement.findCouponsByBarcode(barcode)
  }

  static async findPromotionMessages () {
    return this._implement.findPromotionMesseages()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardPromotionRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardPromotionRepository()
        break

      default:
        this._implement = new SamplePromotionRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('PromotionRepository', (context) => {
  PromotionRepository.switchImplement(context)
})
