import SampleDiscountReasonRepository from './sample/SampleDiscountReasonRepository'
import StandardDiscountReasonRepository from './standard/StandardDiscountReasonRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class DiscountReasonRepository {
  static _implement = new SampleDiscountReasonRepository()

  static async findForItems () {
    return this._implement.findForItems()
  }

  static async findForSubtotal () {
    return this._implement.findForSubtotal()
  }

  static async findById (discountReasonId) {
    return this._implement.findById(discountReasonId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardDiscountReasonRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardDiscountReasonRepository()
        break

      default:
        this._implement = new SampleDiscountReasonRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('DiscountReasonRepository', (context) => {
  DiscountReasonRepository.switchImplement(context)
})
