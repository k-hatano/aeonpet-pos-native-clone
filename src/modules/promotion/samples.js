import * as _ from 'underscore'
import { BUNDLE_TYPE, ESTABLISHMENT_CONDITION } from './models'
import generateUuid from 'common/utils/generateUuid'
import { sampleProductTagMap } from 'modules/product/samples'
import { sampleProductMap } from '../../modules/product/samples'
import { POINT_TYPE, APPLIED_TYPE } from 'modules/promotion/models'
import moment from 'moment'

const createBundle = (name, code, type, priority, bandlePatterns, otherProperties = {}) => {
  const id = generateUuid()
  bandlePatterns.forEach(bundlePattern => {
    bundlePattern.bundle_id = id
  })

  return {
    id,
    name,
    code,
    bundle_riichi_message: `あと1点で ${name} 成立です。`,
    bundle_type: type,
    priority,
    start_at: 1000,
    end_at: null,
    bundle_patterns: bandlePatterns,
    ...otherProperties
  }
}
const createBundlePattern = (bundlePrice, quantity, establishmentCondition, productTagId) => {
  return {
    id: generateUuid(),
    // bundle_id: '', // createBundleで加える
    bundle_price: bundlePrice,
    product_tag_id: productTagId,
    establishment_condition: establishmentCondition,
    threshold_quantity: quantity
  }
}
const createSale = (name, code, discountRate, discountPrice, productTagId) => {
  const id = generateUuid()

  return {
    id,
    name,
    code,
    discount_rate: discountRate,
    discount_price: discountPrice,
    product_tag_id: productTagId,
    priority: null,
    start_at: 1000,
    end_at: null
  }
}

const createCoupon = (name, code, discountRate, discountPrice, productTagId, barcode, thresholdType, thresholdMin, thresholdMax) => {
  const id = generateUuid()
  return {
    id,
    name,
    code,
    discount_rate: discountRate,
    discount_price: discountPrice,
    product_tag_id: productTagId,
    priority: null,
    start_at: 1000,
    end_at: null,
    barcode: barcode,
    threshold_type: thresholdType,
    threshold_min: thresholdMin,
    threshold_max: thresholdMax
  }
}

const createProductPointRatio = (productId, assignedRatio) => {
  const id = generateUuid()
  return {
    id,
    product_id: productId,
    point_type: POINT_TYPE.AEON_PET_POINT,
    applied_type: APPLIED_TYPE.WEEK,
    assigned_ratio: assignedRatio,
    start_at: moment(moment().format('YYYY-MM-DD')).unix(),
    on_sunday: 1,
    on_monday: 1,
    on_tuesday: 1,
    on_wednesday: 1,
    on_thursday: 1,
    on_friday: 1,
    on_saturday: 1
  }
}

const createShopPointRatio = (assignedRatio) => {
  const id = generateUuid()
  return {
    id,
    point_type: POINT_TYPE.AEON_PET_POINT,
    applied_type: APPLIED_TYPE.DAY,
    assigned_ratio: assignedRatio,
    start_at: moment(moment().format('YYYY-MM-DD')).unix(),
    day_1: moment().date()
  }
}

const createPromotionMessage = () => {
  const id = generateUuid()
  return {
    id,
    integrated_incentive_type: 1,
    promotion_type: 9,
    promotion_code: 'SAL000000011',
    start_at: moment(moment().format('YYYY-MM-DD')).unix(),
    campaign_message: 'テスト'
  }
}

export const sampleBundleMap = {
  selection1: createBundle(
    'よりどり1',
    'selection1',
    BUNDLE_TYPE.SELECTION,
    100,
    [ createBundlePattern(3000, 3, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_s_1.id) ]
  ),
  selection1_5x: createBundle(
    'よりどり1 - 5個',
    'selection1_5x',
    BUNDLE_TYPE.SELECTION,
    99,
    [ createBundlePattern(4000, 5, ESTABLISHMENT_CONDITION.GREATER_EQUAL, sampleProductTagMap.bundle_s_1.id) ]
  ),
  selection1_10x: createBundle(
    'よりどり1 - 10個',
    'selection1_10x',
    BUNDLE_TYPE.SELECTION,
    98,
    [ createBundlePattern(7000, 10, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_s_1.id) ]
  ),
  selection2: createBundle(
    'よりどり2',
    'selection2',
    BUNDLE_TYPE.SELECTION,
    1,
    [
      createBundlePattern(3000, 2, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_s_2_A.id),
      createBundlePattern(3000, 3, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_s_2_B.id)
    ]
  ),
  xy1: createBundle(
    'BuyX GetY 1',
    'xy1',
    BUNDLE_TYPE.BUY_X_GET_Y,
    1,
    [
      createBundlePattern(null, 3, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_xy_1.id)
    ]
  ),
  xy2: createBundle(
    'BuyX GetY 2',
    'xy2',
    BUNDLE_TYPE.BUY_X_GET_Y,
    1,
    [
      createBundlePattern(null, 3, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_xy_2.id)
    ]
  ),
  flat1: createBundle(
    'FLAT PRICE',
    'flat1',
    BUNDLE_TYPE.FLATPRICE,
    1,
    [
      createBundlePattern(1000, 3, ESTABLISHMENT_CONDITION.EQUAL, sampleProductTagMap.bundle_flat_1.id)
    ]
  )
}
export const sampleSaleMap = {
  saleA: createSale(
    'セールA',
    'sale_a',
    0,
    100,
    sampleProductTagMap.sale_a.id
  ),
  saleB: createSale(
    'セールB',
    'sale_b',
    0.05,
    0,
    sampleProductTagMap.sale_b.id
  )
}

export const sampleCouponMap = {
  saleC: createCoupon(
    'クーポンA',
    'coupon_a',
    0,
    100,
    sampleProductTagMap.sale_a.id,
    '9870068160005',
    3,
    0,
    99999
  ),
  saleD: createCoupon(
    'クーポンB',
    'coupon_b',
    0.1,
    0,
    sampleProductTagMap.sale_a.id,
    '9870068010003',
    3,
    5000
  )
}

export const sampleProductPointRatioMap = {
  ratioA: createProductPointRatio(
    sampleProductMap.bonusPoint.id,
    2.0000
  )
}

export const sampleShopPointRatioMap = {
  ratioA: createShopPointRatio(
    3.0000
  )
}

export const samplePromotionMessageMap = {
  messageA: createPromotionMessage()
}

export const sampleBundles = _.values(sampleBundleMap)
export const sampleSales = _.values(sampleSaleMap)
export const sampleCoupons = _.values(sampleCouponMap)
export const sampleProductPointRatios = _.values(sampleProductPointRatioMap)
export const sampleShopPointRatios = _.values(sampleShopPointRatioMap)
export const samplePromotionMessages = _.values(samplePromotionMessageMap)
