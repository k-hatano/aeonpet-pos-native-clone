import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  basicLayoutRootStyle: {
    backgroundColor: '#fff'
  },
  inputsWrapper: {
    width: '90%',
    height: '80%',
    alignItems: 'center',
    alignSelf: 'center'
  },
  titleWrapper: {
    marginBottom: 150
  },
  titleText: {
    fontSize: 40,
    color: '#494949'
  },
  contentWrapper: {
    width: '90%'
  },
  rowWrapper: {
    flexDirection: 'row'
  },
  rowTitleWrapper: {
    flex: 1,
    height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  rowTitle: {
    fontSize: 24,
    color: '#5D5D5D'
  },
  rowInputsWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 70
  },
  rowInputReadonly: {
    paddingLeft: 10,
    paddingRight: 10,
    width: '70%',
    height: 46,
    backgroundColor: '#F0F0F0',
    borderWidth: 1,
    borderColor: '#979797',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginLeft: 15
  },
  rowInputReadonlyStaff: {
    width: '90%',
    alignItems: 'flex-start'
  },
  rowInputReadonlyScrollViewContainer: {
    alignItems: 'center'
  },
  rowInputReadonlyText: {
    fontSize: 24,
    color: '#5D5D5D'
  },
  rowInputCurrency: {
    fontSize: 24,
    color: '#5D5D5D',
    marginLeft: 10,
    marginRight: 30
  },
  rowButtons: {
    flex: 1,
    alignItems: 'flex-start'
  },
  rowButtonStyle: {
    width: 120,
    height: 46
  },
  bigButtonWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  bigButtonStyle: {
    fontSize: 20,
    width: '30%',
    height: 56
  },
  approvalButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9024',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    width: 160,
    height: 30
  },
  resettingButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ccc',
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    width: 160,
    height: 30
  },
  saleItemText: {
    fontSize: 18
  },
  modalContainer: {
    backgroundColor: '#fff',
    padding: 20,
    flexDirection: 'column'
  },
  modalTitleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 40
  },
  modalTitle: {
    fontSize: 22,
    fontWeight: '500'
  },
  modalDescription: {
    fontSize: 16
  },
  modalItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60
  },
  modalCenterContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  checkSalesModalStyle: {
    width: 420,
    height: 360
  },
  currencyContainer: {
    height: 40,
    justifyContent: 'center',
    marginLeft: 16
  },
  currencyText: {
    fontSize: 24
  }
})
