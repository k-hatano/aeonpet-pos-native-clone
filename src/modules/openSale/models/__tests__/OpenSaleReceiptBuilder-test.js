import I18n from 'i18n-js'
I18n.locale = 'ja'
import OpenSaleReceiptBuilder from '../OpenSaleReceiptBuilder'
import ReceiptTextConverter from 'modules/printer/models/ReceiptTextConverter'

describe('OpenSale Receipt Builder test', () => {
  it('OpenSale receipt text build case1', async () => {
    // 標準ケース
    const builder = new OpenSaleReceiptBuilder()
    await builder.initializeAsync()
    const cashierRecord = {
      currency: 'jpy',
      total_amount: 18666,
      carry_forward_amount: 10000,
      staff_name: '担当者１'
    }
    const cashierRecordData = {
      bill_1000: 1,
      bill_2000: 1,
      bill_5000: 1,
      bill_10000: 1,
      coin_1: 1,
      coin_5: 1,
      coin_10: 1,
      coin_50: 1,
      coin_100: 1,
      coin_500: 1,
      coinbar_1: 1,
      coinbar_5: 1,
      coinbar_10: 1,
      coinbar_50: 1,
      coinbar_100: 1,
      coinbar_500: 1,
    }
    const propertyA = {
      RECEIPT_LINE_LENGTH: 48
    }
    const createdAt = new Date('2017-07-07 09:30:00')
    const receipt = builder.buildCashierRecordReceipt(cashierRecord, cashierRecordData, createdAt)
    const receiptTextConverter = new ReceiptTextConverter()
    const openSaleReceiptText = receiptTextConverter.contentToText(receipt.content, propertyA)
    expect(openSaleReceiptText).toBe(`
                      開局

テスト店舗A
テストレジA-1
2017-07-07 09:30:00
担当者１

￥10,000                       1枚      ￥10,000
￥5,000                        1枚       ￥5,000
￥2,000                        1枚       ￥2,000
￥1,000                        1枚       ￥1,000
￥500                 1本      1枚      ￥25,500
￥100                 1本      1枚       ￥5,100
￥50                  1本      1枚       ￥2,550
￥10                  1本      1枚         ￥510
￥5                   1本      1枚         ￥255
￥1                   1本      1枚          ￥51
合計                  6本     10枚      ￥18,666

釣銭準備金                              ￥18,666
翌日繰越金                              ￥10,000
変更差額                                 ￥8,666
`.slice(1))
  })
  it('OpenSale receipt text build case2', async () => {
    // 標準ケース２：在高登録を行わなかった場合
    const builder = new OpenSaleReceiptBuilder()
    await builder.initializeAsync()
    const cashierRecord = {
      currency: 'jpy',
      total_amount: 18666,
      carry_forward_amount: 10000,
      staff_name: '担当者１'
    }
    const propertyA = {
      RECEIPT_LINE_LENGTH: 48
    }
    const createdAt = new Date('2017-07-07 09:30:00')
    const receipt = builder.buildCashierRecordReceipt(cashierRecord, null, createdAt)
    const receiptTextConverter = new ReceiptTextConverter()
    const openSaleReceiptText = receiptTextConverter.contentToText(receipt.content, propertyA)
    expect(openSaleReceiptText).toBe(`
                      開局

テスト店舗A
テストレジA-1
2017-07-07 09:30:00
担当者１

釣銭準備金                              ￥18,666
翌日繰越金                              ￥10,000
変更差額                                 ￥8,666
`.slice(1))
  })
})
