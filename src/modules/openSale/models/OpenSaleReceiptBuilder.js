import I18n from 'i18n-js'
import CashierRecordReceiptBuilderBase from 'modules/cashierTotal/models/CashierRecordReceiptBuilderBase'

export default class OpenSaleReceiptBuilder extends CashierRecordReceiptBuilderBase {
  getTitle () {
    return I18n.t('receipt.title.openSale')
  }
  getElements (cashierRecord, currency) {
    return [
      {
        element: 'combine_left_right',
        left: '釣銭準備金',
        right: this.formatMoney(cashierRecord.total_amount, currency)
      },
      {
        element: 'combine_left_right',
        left: '翌日繰越金',
        right: this.formatMoney(cashierRecord.carry_forward_amount, currency)
      },
      {
        element: 'combine_left_right',
        left: '変更差額',
        right: this.formatMoney(
          cashierRecord.total_amount - cashierRecord.carry_forward_amount, currency
        )
      }
    ]
  }
}
