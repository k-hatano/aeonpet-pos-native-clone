import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  selectedStaff: null,
  originCashierRecordData: null,
  isUpdated: false
}

const handlers = {
  [actions.resetState]: (state, action) => ({
    ...state,
    ...{ selectedStaff: null }
  }),
  [actions.selectStaffForOpenSales]: (state, action) => {
    return {
      ...state,
      selectedStaff: action.payload
    }
  },
  [actions.backUpCashierRecordData]: (state, action) => ({
    ...state,
    originCashierRecordData: action.payload
  }),
  [actions.setIsUpdated]: (state, action) => ({
    ...state,
    isUpdated: action.payload
  })
}

export default handleActions(handlers, defaultState)
