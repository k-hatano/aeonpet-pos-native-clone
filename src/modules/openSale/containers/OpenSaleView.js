import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import OpenSaleView from '../components/OpenSaleView'
import { MODULE_NAME } from '../models'
import { MODULE_NAME as MODULE_NAME_CASHIER_TOTAL, CASHIER_RECORD_MODE } from 'modules/cashierTotal/models'
import * as actions from '../actions'
import { resetState } from 'modules/cashierTotal/actions'
import { PERMISSION_CODES } from 'modules/staff/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { selectStaffForOpenSales } from 'modules/openSale/services'

const mapDispatchToProps = dispatch => ({
  onStartCashierRecord: async (carryForwardAmount) => {
    Actions.cashierRecord({
      hideHomeButton: true,
      isOpening: true,
      cashierRecordMode: CASHIER_RECORD_MODE.OPEN_SALES,
      permissionCode: PERMISSION_CODES.OPEN_SALES
    })
  },
  onClose: () => {
    dispatch(resetState())
    dispatch(actions.resetState())
  },
  onInit: () => {
    dispatch(actions.setIsUpdated(false))
  },
  onSelectedStaff: (staff) => {
    selectStaffForOpenSales(dispatch, staff)
  }
})

const mapStateToProps = state => ({
  totalAmount: state[MODULE_NAME_CASHIER_TOTAL].cashierRecord.total_amount,
  carryForwardAmount: state[MODULE_NAME_CASHIER_TOTAL].cashierRecord.carry_forward_amount,
  selectedStaff: state[MODULE_NAME].selectedStaff,
  cashierName: state.setting.settings[SettingKeys.NETWORK.ACCESS.CASHIER_NAME],
  isUpdated: state[MODULE_NAME].isUpdated
})

export default connect(mapStateToProps, mapDispatchToProps)(OpenSaleView)
