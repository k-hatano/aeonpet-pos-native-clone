import I18n from 'i18n-js'
import * as actions from './actions'
import { setCarryForwardAmount } from 'modules/cashierTotal/actions'
import { Actions } from 'react-native-router-flux'
import { makeBaseEJournal, RECEIPT_TYPE } from 'modules/printer/models'
import CashierRepository from '../cashier/repositories/CashierRepository'
import CashierCashRecordRepository from 'modules/cashier/repositories/CashierCashRecordRepository'
import ConfirmView from 'common/components/widgets/ConfirmView'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import OpenSaleReceiptBuilder from 'modules/openSale/models/OpenSaleReceiptBuilder'
import PrinterManager from 'modules/printer/models/PrinterManager'
import AlertView from 'common/components/widgets/AlertView'
import logger from 'common/utils/logger'
import { toUnixTimestamp } from 'common/utils/dateTime'
import { loading } from 'common/sideEffects'
import { makeCashierRecordForPush } from 'modules/cashierTotal/services'
import { CASHIER_CASH_RECORD_MODE } from 'modules/cashier/models'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import SettingGetter from 'modules/setting/models/settingGetter'
import { updateSetting } from 'modules/setting/actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import { setIsOpening } from '../../common/actions'

export const makeOpenSaleEJournal = (receipt, cashierRecord, createdAt) => {
  const receiptTitle = I18n.t('receipt.title.openSale')
  const eJournal = makeBaseEJournal(receiptTitle, receipt, RECEIPT_TYPE.OPEN_SALES, createdAt)
  eJournal.device_id = cashierRecord.device_id
  eJournal.shop_id = cashierRecord.shop_id
  eJournal.staff_id = cashierRecord.staff_id
  eJournal.currency = cashierRecord.currency
  eJournal.amount = cashierRecord.total_amount
  eJournal.is_print_tax_stamp = false

  return eJournal
}

/**
 * 開局情報を初期化します。
 * @param dispatch
 * @param cashierRecord
 * @returns {Promise.<void>}
 */
export const initOpenSale = async (dispatch, cashierRecord, cashierRecordData) => {
  const cashier = await CashierRepository.find()

  let latestCashierRecord = null
  try {
    latestCashierRecord = await CashierCashRecordRepository.fetchLatestRecord(
      cashier.id, cashierRecord.shop_id, CASHIER_CASH_RECORD_MODE.CLOSE_SALE
    )
  } catch (error) {
    // NetowrkErrorの場合のみしつこく繰り返させる
    ConfirmView.show(I18n.t('message.I-01-I001'), () =>
      initOpenSale(dispatch, cashierRecord, cashierRecordData)
    )
  }
  // 0件の場合は何もしない。
  if (latestCashierRecord != null) {
    dispatch(setCarryForwardAmount(parseInt(latestCashierRecord.carry_forward_amount)))
  }
  dispatch(actions.backUpCashierRecordData(JSON.parse(JSON.stringify(cashierRecordData))))
}

/**
 * 開局時在高情報をサーバーに送信します。
 * @param dispatch
 * @param cashierRecord
 * @param cashierRecordData
 * @returns {Promise.<void>}
 */
export const pushCashierCashRecord = async (dispatch, cashierRecord, cashierRecordData, isUpdated) => {
  if (cashierRecord.total_amount === 0 && !isUpdated) {
    cashierRecord.total_amount = cashierRecord.carry_forward_amount
    cashierRecord.account_amount = 0 // 点検在高登録後の値が残っていることがあるのでクリアしとく
    cashierRecordData = []
  }
  try {
    const date = new Date()
    const builder = new OpenSaleReceiptBuilder()
    await builder.initializeAsync()
    const receipt = builder.buildCashierRecordReceipt(cashierRecord, cashierRecordData, date)
    const ejournal = makeOpenSaleEJournal(
      receipt,
      {
        ...cashierRecord,
        ...cashierRecordData
      },
      toUnixTimestamp(date)
    )
    const result = await loading(dispatch, async () => {
      await EJournalRepository.save(ejournal)
      const cashier = await CashierRepository.find()
      const cashierRecordNew = await makeCashierRecordForPush(
        cashierRecord, cashierRecordData, cashier, CASHIER_CASH_RECORD_MODE.OPEN_SALE
      )
      const messages = []
      try {
        await CashierCashRecordRepository.push(cashierRecordNew)
        await saveOperationLog(OPERATION_TYPE.OPEN, date)
      } catch (error) {
        cashierRecordNew.is_pushed = false
        await CashierCashRecordRepository.save(cashierRecordNew)
        // 在高登録時にエラーが発生した場合は未送信在高とし、開局は成功させる。
        dispatch(updateSetting({key: SettingKeys.OPENSALE.IS_OPEN_SALE, value: true}))
        messages.push(I18n.t('message.I-02-E004'))
      }
      try {
        await PrinterManager.print(receipt)
        // 印刷済み
        await EJournalRepository.saveIsPrintedById(ejournal.id)
      } catch (error) {
        // 印刷エラー時はエラー表示のみで、処理は続行する。
        messages.push(I18n.t('message.I-02-E003'))
      }
      try {
        await EJournalRepository.pushById(ejournal.id)
      } catch (error) {
        // 電子ジャーナル送信に失敗した場合は、エラー通知は行わない。
        logger.warning('openSales: ejournals save error. ' + error.message)
      }

      try {
        const pendedOrders = await OrderPendRepository.findAll()
        if (pendedOrders.length > 0) {
          await OrderPendRepository.clearAll()
        }
      } catch (error) {
        logger.warning('failed to delete pended order.')
        messages.push(I18n.t('message.G-02-E008'))
      }
      return {messages: messages}
    })
    // 開局済み
    dispatch(updateSetting({key: SettingKeys.OPENSALE.IS_OPEN_SALE, value: true}))
    dispatch(setIsOpening(false))
    // まとめてエラー
    for (const i in result.messages) {
      await AlertView.showAsync(result.messages[i])
    }
    Actions.syncData({isImmediateExec: true})
  } catch (error) {
    AlertView.show(error.message)
  }
}

/**
 * 開局の必要があるかどうかを判定します。
 * @returns {Promise.<boolean>}
 */
export const needsOpenSale = async (dispatch) => {
  // 設定のキャッシャーIDがnullの場合（在庫管理モードがONの場合）常に開局しない
  if (!SettingGetter.cashierId) {
    return false
  }

  const unpushedCashierCashRecord = await CashierCashRecordRepository.findUnpushed()
  // 未送信在高情報を取得する
  if (unpushedCashierCashRecord.length > 0) {
    // 未送信在高情報が1件でもあれば、処理続行の為falseを返す
    return false
  } else {
    // 未送信在高情報がなかった場合に開局済み判定APIからデータを取得する
    try {
      // 開局済み判定APIから開局済みかどうかを取得する
      const isOpened = await CashierRepository.fetchIsOpenedByCashierId(SettingGetter.cashierId)
      // POS設定を取得した値でセットする
      dispatch(updateSetting({key: SettingKeys.OPENSALE.IS_OPEN_SALE, value: !!isOpened}))
      // 開局済みかどうかを返す(開局が必要かどうかを取得するメソッドのため、このような形になる)
      return !isOpened
    } catch (error) {
      // 取得できなかった場合は、POS設定より取得する
      const isOpenedFromSetting = SettingGetter.isOpenSale
      // 同様にこのような形になる
      return !isOpenedFromSetting
    }
  }
}

export const selectStaffForOpenSales = async (dispatch, staff) => {
  if (staff != null) {
    dispatch(actions.selectStaffForOpenSales(staff))
  }
}
