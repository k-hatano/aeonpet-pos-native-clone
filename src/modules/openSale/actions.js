import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const resetState = createAction(`${MODULE_NAME}_resetState`)
export const selectStaffForOpenSales = createAction(`${MODULE_NAME}_selectStaffForOpenSales`)
export const backUpCashierRecordData = createAction(`${MODULE_NAME}_backUpCashierRecordData`)
export const setIsUpdated = createAction(`${MODULE_NAME}_setIsUpdated`)