import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import I18n from 'i18n-js'
import StandardButton, { CommandButton } from 'common/components/elements/StandardButton'
import { formatPriceWithCurrency, formatMoney } from 'common/utils/formats'
import commonStyles from '../../../common/styles/layout/Page'
import componentStyles from '../styles/OpenSaleView'
import PropTypes from 'prop-types'
import { showSelectStaffViewModal } from 'modules/staff/containers/SelectStaffViewModal'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class OpenSaleView extends React.Component {
  static propTypes = {
    cashierRecord: PropTypes.object,
    totalAmount: PropTypes.number,
    carryForwardAmount: PropTypes.number,
    cashierName: PropTypes.string
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  componentWillUnmount () {
    this.props.onClose()
  }

  render () {
    const { selectedStaff, totalAmount, carryForwardAmount, currency, cashierName, isUpdated } = this.props
    const amount = !isUpdated && totalAmount === 0
      ? carryForwardAmount
      : totalAmount
    return (
      <View style={[ commonStyles.basicLayoutRootStyle, componentStyles.basicLayoutRootStyle ]}>
        <View style={componentStyles.inputsWrapper}>
          <View style={componentStyles.titleWrapper}>
            <Text style={componentStyles.titleText}>{cashierName}</Text>
          </View>
          <View style={componentStyles.contentWrapper}>
            <View style={componentStyles.rowWrapper}>
              <View style={componentStyles.rowTitleWrapper}>
                <Text style={componentStyles.rowTitle}>
                  {I18n.t('check_sale.change_fund_reserve')}:
                </Text>
              </View>
              <View style={componentStyles.rowInputsWrapper}>
                <View style={componentStyles.rowInputReadonly}>
                  <ScrollView
                    horizontal
                    contentContainerStyle={
                      componentStyles.rowInputReadonlyScrollViewContainer
                    }>
                    <Text style={componentStyles.rowInputReadonlyText}>
                      {formatMoney(amount)}
                    </Text>
                  </ScrollView>
                </View>
                <View style={componentStyles.currencyContainer}>
                  <Text style={componentStyles.currencyText}>
                    {I18n.t('common.japanese_yen')}
                  </Text>
                </View>
              </View>
              <View style={componentStyles.rowButtons}>
                <StandardButton
                  isBorderButton
                  containerStyle={componentStyles.rowButtonStyle}
                  text={I18n.t('check_sale.correction')}
                  onPress={() => this.props.onStartCashierRecord(carryForwardAmount)}
                />
              </View>
            </View>
            <View style={componentStyles.rowWrapper}>
              <View style={componentStyles.rowTitleWrapper}>
                <Text style={componentStyles.rowTitle}>
                  {I18n.t('check_sale.staff')}:
                </Text>
              </View>
              <View style={componentStyles.rowInputsWrapper}>
                <View
                  style={[
                    componentStyles.rowInputReadonly,
                    componentStyles.rowInputReadonlyStaff
                  ]}>
                  <ScrollView
                    horizontal
                    contentContainerStyle={
                      componentStyles.rowInputReadonlyScrollViewContainer
                    }>
                    <Text style={[componentStyles.rowInputReadonlyText]}>
                      {selectedStaff && selectedStaff.name}
                    </Text>
                  </ScrollView>
                </View>
              </View>
              <View style={componentStyles.rowButtons}>
                <StandardButton
                  isBorderButton
                  containerStyle={componentStyles.rowButtonStyle}
                  text={I18n.t('check_sale.change')}
                  onPress={() => showSelectStaffViewModal(PERMISSION_CODES.OPEN_SALES, (staff) => {
                    this.props.onSelectedStaff(staff)
                  })}
                />
              </View>
            </View>
          </View>
          <View style={componentStyles.bigButtonWrapper}>
            <CommandButton
              disabled={!selectedStaff}
              style={componentStyles.bigButtonStyle}
              text={I18n.t('check_sale.back_to_home')}
              onPress={() => ConfirmView.show(I18n.t('message.I-02-I001', {totalAmount: formatPriceWithCurrency((amount), currency)}), this.props.onComplete)}
            />
          </View>
        </View>
      </View>
    )
  }
}
