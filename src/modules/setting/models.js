import { AsyncStorage, Platform } from 'react-native'
import I18n from 'i18n-js'
import FileSystem from 'react-native-filesystem-v1'
import SensitiveInfo from 'react-native-sensitive-info'
import SettingKeys from './models/SettingKeys'
import SettingDefinitions from './models/SettingDefinitions'
import SettingStorage, { getLocalReceiptParameter } from './models/SettingStorage'
import { setSettings, updateSetting } from './actions'
import { listPermissions } from '../staff/actions'
import StaffRoleRepository from '../staff/repositories/StaffRoleRepository'
import SettingRepository from './repositories/SettingRepository'
import AppEvents, { REPOSITORY_CONTEXT } from '../../common/AppEvents'
import ImageFetcher from '../../common/models/ImageFetcher'
import { isNullOrEmpty } from 'common/models'
import settingGetter from 'modules/setting/models/settingGetter'

export const MODULE_NAME = 'setting'

export function formatPrinterInfo (printer) {
  const { maker, model, paperWidth, networkInterface, ip } = printer
  const printerInfo = [maker, model, paperWidth, networkInterface, ip].filter(prop => prop).join(' / ')
  return printerInfo
}

export function formatPaymentServiceInfo (paymentService) {
  const { paymentDevice, ip, port } = paymentService
  const paymentServiceInfo = [paymentDevice, ip, port].filter(prop => prop).join(' / ')
  return paymentServiceInfo
}

export function getSettingFromState (state, key) {
  return state[MODULE_NAME].settings[key]
}

export function getIsTrainingModeFromState (state) {
  return state[MODULE_NAME].settings[SettingKeys.COMMON.REPOSITORY_CONTEXT] === REPOSITORY_CONTEXT.TRAINING
}

export async function loadSettingsAsync (dispatch) {
  const settings = {}
  for (const i in SettingKeys) {
    for (const j in SettingKeys[i]) {
      if (typeof SettingKeys[i][j] === 'object') {
        for (const k in SettingKeys[i][j]) {
          const key = SettingKeys[i][j][k]
          const type = SettingDefinitions[key].type
          const isSensitive = SettingDefinitions[key].isSensitive
          try {
            const value = isSensitive && Platform.OS === 'ios'
              ? await SensitiveInfo.getItem(key, {})
              : await SettingStorage.load(key, type)
            settings[key] = value
          } catch (error) {
            console.error('Load Settings Failed For Key = ' + key)
          }
        }
      } else {
        const key = SettingKeys[i][j]
        const type = SettingDefinitions[key].type
        const isSensitive = SettingDefinitions[key].isSensitive
        try {
          const value = isSensitive && Platform.OS === 'ios'
            ? await SensitiveInfo.getItem(key, {})
            : await SettingStorage.load(key, type)
          settings[key] = value
        } catch (error) {
          console.error('Load Settings Failed For Key = ' + key)
        }
      }
    }
  }
  dispatch(setSettings(settings))
  return settings
}

export async function loadSettingsWithRepositoryAsync (dispatch, settings) {
  AppEvents.repositoryContextChange(settings[SettingKeys.COMMON.REPOSITORY_CONTEXT])
  const currentStaff = settings[SettingKeys.STAFF.CURRENT_STAFF]
  if (currentStaff) {
    const role = await StaffRoleRepository.findByRoleId(currentStaff.staff_role_id)
    if (role) {
      dispatch(listPermissions(role.permissions))
    } else {
      dispatch(listPermissions([]))
      dispatch(updateSetting({key: SettingKeys.STAFF.CURRENT_STAFF, value: null}))
      console.warn('StaffRole not found')
    }
  }
}

export const PRINT_MODE = {
  ITEM_NUMBER: 1,
  JAN_CODE: 2,
  DO_NOT_PRINT: 0
}

export const PRINT_STAFF = {
  PRINT_PERSON_IN_CHARGE_NAME: 1,
  SELECT_CONTACT_PERSON_CODE: 2
}

export const IMAGE = {
  ITEM_NUMBER: 1,
  JAN_CODE: 2,
  DO_NOT_PRINT: 0
}

export const FILE_PATH_HEADER = 'file://'

export async function mapCashierPropertySetToSettings () {
  const settings = await SettingRepository.findAll()
  return {
    'accounting.is_print_header_logo': settings.is_print_receipt_header_logo,
    'accounting.header_message': settings.receipt_header_message,
    'accounting.header_logo': settings.receipt_header_logo_image,
    'accounting.is_print_zero_products': settings.is_print_zero_products,
    'accounting.print_product_code_mode': settings.print_product_code_mode,
    'accounting.is_print_point_info': settings.is_print_point_info,
    'accounting.is_print_footer_logo': settings.is_print_receipt_footer_logo,
    'accounting.footer_logo': settings.receipt_footer_logo_image,
    'accounting.footer_message': settings.receipt_footer_message,
    'bill.is_print_header_logo': settings.is_print_bill_logo,
    'bill.header_logo': settings.bill_logo_image,
    'bill.header_message': settings.bill_message,
    'bill.proviso': settings.bill_proviso,
    'common.is_print_staff': settings.is_print_staff,
    'common.is_print_tax_stamp': settings.is_print_tax_stamp,
    'common.tax_office_name': settings.tax_office_name,
    'cashier_total.print_num': settings.total_receipt_print_num,
    'cashier_total.is_print_product_segment_total': settings.is_print_product_segment_total,
    'cashier_total.is_print_staff_total': settings.is_print_staff_total
  }
}

export async function mapOrderToSettings () {
  const settings = await SettingRepository.findAll()
  return {
    'tax.rate': settings.tax_rate,
    'tax.rule': settings.tax_rule,
    'tax.calculate_rule': settings.calc_type,
    'tax.is_taxfree': settings.is_taxfree
  }
}

export function updateReceiptParameter (values, receiptType, dispatch) {
  updateSettingParameter(values, receiptType, dispatch, SettingKeys.RECEIPT)
}

export function updateOrderParameter (values, receiptType, dispatch) {
  updateSettingParameter(values, receiptType, dispatch, SettingKeys.ORDER)
}

function updateSettingParameter (values, receiptType, dispatch, upperKey) {
  for (const i in upperKey[receiptType]) {
    const key = upperKey[receiptType][i]
    const value = values[key]
    if (value !== undefined && value !== null) {
      dispatch(updateSetting({key: key, value: value}))
    }
  }
}

export async function backupLocalSettings (backupType) {
  switch (backupType) {
    case SettingKeys.BACKUP.ACCOUNTING:
      return AsyncStorage.setItem(SettingKeys.BACKUP.ACCOUNTING, JSON.stringify(await getLocalReceiptParameter('ACCOUNTING')))
    case SettingKeys.BACKUP.BILL:
      return AsyncStorage.setItem(SettingKeys.BACKUP.BILL, JSON.stringify(await getLocalReceiptParameter('BILL')))
    case SettingKeys.BACKUP.COMMON:
      return AsyncStorage.setItem(SettingKeys.BACKUP.COMMON, JSON.stringify(await getLocalReceiptParameter('COMMON')))
    case SettingKeys.BACKUP.CASHIER_TOTAL:
      return AsyncStorage.setItem(SettingKeys.BACKUP.CASHIER_TOTAL, JSON.stringify(await getLocalReceiptParameter('CASHIER_TOTAL')))
    default:
      console.error('Invalid backupType')
      return undefined
  }
}

export async function updateReceiptStoreImage (values, dispatch) {
  await updateStoreImage(values['accounting.header_logo'], SettingKeys.RECEIPT.ACCOUNTING.HEADER_LOGO_FILE, dispatch)
  await updateStoreImage(values['accounting.footer_logo'], SettingKeys.RECEIPT.ACCOUNTING.FOOTER_LOGO_FILE, dispatch)
  await updateStoreImage(values['bill.header_logo'], SettingKeys.RECEIPT.BILL.HEADER_LOGO_FILE, dispatch)
}

async function updateStoreImage (imagePath, key, dispatch) {
  if (imagePath) {
    await saveImageOnDevice(imagePath, key, dispatch)
  } else {
    await dispatch(updateSetting({key: key, value: ''}))
  }
}

async function saveImageOnDevice (imagePath, key, dispatch) {
  const savedFileDocumentPath = await ImageFetcher.saveImage(imagePath, key)
  const filePath = savedFileDocumentPath.replace(documentPath(), '')
  await dispatch(updateSetting({key: key, value: filePath}))
}

export const imageFilePath = (filePath) => {
  return FILE_PATH_HEADER + documentPath() + filePath
}

export const documentPath = () => {
  return FileSystem.absolutePath('').replace('RNFS-BackedUp/', '')
}

export const PAYMENT_SERVICE = {
  NOT_SELECTED: 0,
  VEGA3000: 1
}

export const paymentServiceToLabel = paymentService => {
  switch (paymentService) {
    case PAYMENT_SERVICE.NOT_SELECTED:
      return I18n.t('settings_peripheral_device.not_selected')
    case PAYMENT_SERVICE.VEGA3000:
      return I18n.t('settings_peripheral_device.vega3000')
  }
}

export const SERVER_REQUEST_TIMEOUT = [
  { label: '15', value: 15 },
  { label: '30', value: 30 },
  { label: '45', value: 45 },
  { label: '60', value: 60 },
  { label: '75', value: 75 },
  { label: '90', value: 90 },
  { label: '105', value: 105 },
  { label: '120', value: 120 },
  { label: '135', value: 135 },
  { label: '150', value: 150 },
  { label: '165', value: 165 },
  { label: '180', value: 180 }
]

export const CUSTOMER_DISPLAY_MAKERS = {
  NOT_SELECTED: 0,
  EPSON: 1,
  EPSON_BLUETOOTH: 2
}

export const customerDisplayMakerToLabel = (maker) => {
  switch (maker) {
    case CUSTOMER_DISPLAY_MAKERS.NOT_SELECTED:
      return I18n.t('settings_peripheral_device.not_selected')
    case CUSTOMER_DISPLAY_MAKERS.EPSON:
      return I18n.t('settings_peripheral_device.epson')
    case PRINTER_MAKERS.EPSON_BLUETOOTH:
      return I18n.t('settings_peripheral_device.epson_bluetooth')
  }
}

export const customerDisplayParameter = (maker) => {
  switch (maker) {
    case CUSTOMER_DISPLAY_MAKERS.NOT_SELECTED:
      return {model: '', interface: 99, interfaceLabel: ''}
    case CUSTOMER_DISPLAY_MAKERS.EPSON:
      return {model: 'EPSON 1', interface: 0, interfaceLabel: 'IP'}
    case PRINTER_MAKERS.EPSON_BLUETOOTH:
      return {model: 'EPSON 2', interface: 1, interfaceLabel: 'BLUETOOTH'}
  }
}

export const PRINTER_MAKERS = {
  NOT_SELECTED: 0,
  EPSON: 1,
  EPSON_BLUETOOTH: 2
}

export const printerMakerToLabel = (maker) => {
  switch (maker) {
    case PRINTER_MAKERS.NOT_SELECTED:
      return I18n.t('settings_peripheral_device.not_selected')
    case PRINTER_MAKERS.EPSON:
      return I18n.t('settings_peripheral_device.epson')
    case PRINTER_MAKERS.EPSON_BLUETOOTH:
      return I18n.t('settings_peripheral_device.epson_bluetooth')
  }
}

const PRINTER_SDK = {
  EPSON: 0,
  STAR: 1
}

export const printerParameter = (maker) => {
  switch (maker) {
    case PRINTER_MAKERS.NOT_SELECTED:
      return {model: '', interface: 99, interfaceLabel: 'IP', paperWidth: 0, sdk: 99}
    case PRINTER_MAKERS.EPSON:
      return {model: 'EPSON 1', interface: 0, interfaceLabel: 'IP', paperWidth: 34, sdk: PRINTER_SDK.EPSON}  //2019.6 58mm
    case PRINTER_MAKERS.EPSON_BLUETOOTH:
      return {model: 'EPSON 2', interface: 1, interfaceLabel: 'BLUETOOTH', paperWidth: 34, sdk: PRINTER_SDK.EPSON}
  }
}

export const NUMBER_OF_RECEIPT = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 }
]

export const hasSettingPayment = () => {
  return settingGetter.paymentTerminal !== PAYMENT_SERVICE.NOT_SELECTED &&
    !isNullOrEmpty(settingGetter.paymentTerminalAddress) &&
    !isNaN(settingGetter.paymentTerminalPort)
}
