import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  imgContainer: {
    width: 280,
    borderWidth: 1,
    borderColor: '#d7d7d7',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  imgContent: {
    margin: 5,
    width: 270,
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  button: {
    marginBottom: 10,
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5
  },
  inputContent: {
    width: '92%',
    borderWidth: 2,
    borderColor: '#828282',
    paddingLeft: 5,
    height: 60,
    borderRadius: 5
  },
  formContainer: {
    marginBottom: 10
  }
})

export default styles
