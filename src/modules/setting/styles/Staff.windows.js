import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  staffButton: {
    backgroundColor: '#ddd',
    position: 'absolute',
    minWidth: 70,
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 4
  }
})

export default styles
