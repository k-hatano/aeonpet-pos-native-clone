import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  componentHeader: {
    fontSize: 20,
    marginBottom: 20
  },
  subContainer: {
    marginBottom: 15
  },
  subContainerHeader: {
    fontSize: 15,
    marginBottom: 5
  },
  button: {
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 10
  }
})

export default styles
