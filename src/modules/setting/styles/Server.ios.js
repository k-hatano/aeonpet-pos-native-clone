import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputWrapper: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingBottom: 10,
    alignSelf: 'stretch'
  },
  inputContent: {
    borderWidth: 1,
    borderColor: '#ddd',
    paddingLeft: 5,
    height: 35,
    borderRadius: 1
  },
  button: {
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5
  }
})

export default styles
