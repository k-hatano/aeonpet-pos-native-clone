import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  staffButtonView: {
    flex: 1,
    alignSelf: 'flex-end',
    padding: 10
  },
  staffButton: {
    backgroundColor: '#ddd',
    minWidth: 70,
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 4
  },
  bottomCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default styles
