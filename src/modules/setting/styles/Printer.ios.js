import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  listHeader: {
    flex: 1,
    height: 25,
    backgroundColor: '#ddd'
  },
  listSeparator: {
    backgroundColor: '#ccc',
    flex: 1,
    height: StyleSheet.hairlineWidth
  },
  listRow: {
    width: '100%',
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#eee'
  },
  rowDataDetail: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flex: 1
  },
  menu: {
    paddingLeft: 10,
    paddingRight: 5,
    paddingTop: 10
  },
  addNewButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  addNewButton: {
    padding: 5,
    width: 80,
    backgroundColor: '#d4d4d4',
    borderRadius: 2,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  modalWrapper: {
    position: 'absolute',
    width: '100%',
    height: 0,
    paddingTop: 50,
    flex: 1,
    zIndex: 999
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: 400,
    height: 470,
    borderWidth: 0,
    backgroundColor: 'transparent'
  },
  modalContainer: {
    flex: 1,
    padding: 10,
    width: '100%'
  },
  modalHeaderContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 5,
    borderBottomColor: '#f2f2f2'
  },
  modalHeader: {
    fontSize: 20,
    flex: 1
  },
  closeButton: {
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalMainContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  modalSubContainer: {
    marginTop: 10
  },
  picker: {
    width: '100%'
  },
  textInput: {
    width: '90%',
    height: 30,
    borderWidth: 1,
    marginTop: 5
  },
  search: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    padding: 5,
    width: 120,
    backgroundColor: '#d4d4d4',
    borderRadius: 2
  },
  registerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  register: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    padding: 5,
    width: 150,
    backgroundColor: '#ff9024',
    borderRadius: 2
  },
  cancel: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    padding: 5,
    width: 150,
    backgroundColor: '#d4d4d4',
    borderRadius: 2
  },
  modalHeaderStyle: {
    marginBottom: 0,
    height: 60
  }
})

export default styles
