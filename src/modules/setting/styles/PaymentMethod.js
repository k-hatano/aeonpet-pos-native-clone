import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  componentHeader: {
    fontSize: 20,
    marginBottom: 20
  },
  listHeader: {
    flex: 1,
    height: 25,
    backgroundColor: '#ddd'
  },
  listSeparator: {
    backgroundColor: '#ccc',
    flex: 1,
    height: StyleSheet.hairlineWidth
  },
  listRow: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#eee'
  },
  rowButtonComponent: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: 150
  },
  rowButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 3,
    marginTop: 60,
    paddingTop: 5,
    paddingBottom: 5,
    width: 110
  },
  rowDataDetail: {
    flex: 1,
    justifyContent: 'center'
  },
  creditContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.8,
    borderBottomColor: '#888'
  },
  otherContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.8,
    borderBottomColor: '#888',
    paddingTop: 15,
    paddingBottom: 15
  },
  paymentMethodName: {
    flex: 1,
    fontSize: 32
  },
  paymentMethodSelector: {
    flex: 2
  },
  paymentMethodSameText: {
    fontSize: 24
  }
})

export default styles
