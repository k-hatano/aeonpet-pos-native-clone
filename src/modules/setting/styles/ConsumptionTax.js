import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  subContainer: {
    marginBottom: 15
  },
  subContainerItem: {
    marginRight: 300
  },
  subContainerHeader: {
    fontSize: 15
  },
  marginLeftView: {
    marginLeft: 30
  },
  taxRatioButton: {
    justifyContent: 'center',
    marginRight: 300,
    padding: 5,
    width: 80,
    backgroundColor: '#d4d4d4',
    borderRadius: 2
  },
  groupButton: {
    width: 250,
    height: 40
  }
})

export default styles
