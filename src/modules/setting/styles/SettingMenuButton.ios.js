import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerMenu: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginRight: 100
  },
  button: {
    marginTop: 5,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    width: '100%'
  }
})

export default styles
