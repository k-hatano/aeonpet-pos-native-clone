import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerMenu: {
    flex: 1
  },
  button: {
    marginTop: 5,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  }
})

export default styles
