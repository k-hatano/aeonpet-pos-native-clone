import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  componentHeader: {
    fontSize: 20,
    marginBottom: 20
  },
  subContainer: {
    flexDirection: 'row',
    marginBottom: 15
  },
  subContainerHeader: {
    flex: 1,
    alignSelf: 'flex-start'
  },
  subContainerItem: {
    flex: 1,
    fontSize: 15,
    alignSelf: 'flex-end'
  },
  selectContainer: {
    flex: 1
  },
  subContainerItemView: {
    alignSelf: 'flex-end'
  },
  button: {
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 10
  },
  fotterLeftbutton: {
    position: 'absolute',
    backgroundColor: '#ddd',
    width: 130,
    height: 40,
    alignItems: 'center',
    marginRight: 10,
    marginBottom: 10,
    right: 10,
    bottom: 10
  }
})

export default styles
