import { connect } from 'react-redux'
import CashierTotalReceiptSettingForm from '../components/CashierTotalReceiptSettingForm'
import { MODULE_NAME, updateReceiptParameter, mapCashierPropertySetToSettings, backupLocalSettings } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import SettingStorage from '../models/SettingStorage'
import { SETTING_TYPE } from '../models/SettingDefinitions'

const mapDispatchToProps = dispatch => ({
  onUpdateReceiptCashierTotal: async (useShopSetinng, key, value) => {
    if (!useShopSetinng) {
      dispatch(actions.updateSetting({key: key, value: value}))
    }
  },
  onBackupSettings: async (isOn) => {
    if (isOn) {
      await backupLocalSettings(SettingKeys.BACKUP.CASHIER_TOTAL)
      const values = await mapCashierPropertySetToSettings()
      updateReceiptParameter(values, 'CASHIER_TOTAL', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.CASHIER_TOTAL.USE_SHOP_SETTING, value: isOn}))
    } else {
      const values = await SettingStorage.load(SettingKeys.BACKUP.CASHIER_TOTAL, SETTING_TYPE.OBJECT)
      updateReceiptParameter(values, 'CASHIER_TOTAL', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.CASHIER_TOTAL.USE_SHOP_SETTING, value: isOn}))
    }
  }
})

const mapStateToProps = state => ({
  useShopSetting: state[MODULE_NAME].settings[SettingKeys.RECEIPT.CASHIER_TOTAL.USE_SHOP_SETTING],
  printNum: state[MODULE_NAME].settings[SettingKeys.RECEIPT.CASHIER_TOTAL.PRINT_NUM],
  isPrintProductSegmentTotal: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_PRODUCT_SEGMENT_TOTAL],
  isPrintStaffTotal: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_STAFF_TOTAL]
})

export default connect(mapStateToProps, mapDispatchToProps)(CashierTotalReceiptSettingForm)
