import { connect } from 'react-redux'
import LogConfiguration from '../components/LogConfiguration'
import { updateSetting } from '../actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import logger from '../../../common/utils/logger'
import { getSettingFromState, MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
  onUpdateSetting: async (key, value) => {
    dispatch(updateSetting({ key, value }))
  }
})

const mapStateToProps = state => ({
  level: getSettingFromState(state, SettingKeys.LOG.LEVEL),
  reactNativeError: getSettingFromState(state, SettingKeys.LOG.REACT_NATIVE_ERROR),
  rotationDays: getSettingFromState(state, SettingKeys.LOG.ROTATION_DAYS)
})


export default connect(mapStateToProps, mapDispatchToProps)(LogConfiguration)

