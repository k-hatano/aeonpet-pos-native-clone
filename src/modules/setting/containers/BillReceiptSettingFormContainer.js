import { connect } from 'react-redux'
import BillReceiptSettingForm from '../components/BillReceiptSettingForm'
import { MODULE_NAME, updateReceiptParameter, mapCashierPropertySetToSettings, backupLocalSettings } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import SettingStorage from '../models/SettingStorage'
import { SETTING_TYPE } from '../models/SettingDefinitions'

const mapDispatchToProps = dispatch => ({
  onUpdateReceiptBill: async (useShopSetinng, key, value) => {
    if (!useShopSetinng) {
      dispatch(actions.updateSetting({key: key, value: value}))
    }
  },
  onBackupSettings: async (isOn) => {
    if (isOn) {
      await backupLocalSettings(SettingKeys.BACKUP.BILL)
      const values = await mapCashierPropertySetToSettings()
      updateReceiptParameter(values, 'BILL', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.BILL.USE_SHOP_SETTING, value: isOn}))
    } else {
      const values = await SettingStorage.load(SettingKeys.BACKUP.BILL, SETTING_TYPE.OBJECT)
      updateReceiptParameter(values, 'BILL', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.BILL.USE_SHOP_SETTING, value: isOn}))
    }
  }
})

const mapStateToProps = state => ({
  useShopSetting: state[MODULE_NAME].settings[SettingKeys.RECEIPT.BILL.USE_SHOP_SETTING],
  isPrintHeaderLogo: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.BILL.IS_PRINT_HEADER_LOGO],
  headerLogoFile: state[MODULE_NAME].settings[SettingKeys.RECEIPT.BILL.HEADER_LOGO_FILE],
  headerMessage: state[MODULE_NAME].settings[SettingKeys.RECEIPT.BILL.HEADER_MESSAGE],
  proviso: state[MODULE_NAME].settings[SettingKeys.RECEIPT.BILL.PROVISO]
})

export default connect(mapStateToProps, mapDispatchToProps)(BillReceiptSettingForm)
