import { connect } from 'react-redux'
import SettingHomeButton from '../components/SettingHomeButton'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onUpdateHomeButton: async (menus, menu) => {
    menus[menu] = !menus[menu]
    dispatch(actions.updateSetting({key: SettingKeys.MENU.MAIN_MENU, value: menus}))
  }
})

const mapStateToProps = state => ({
  menus: state[MODULE_NAME].settings[SettingKeys.MENU.MAIN_MENU]
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingHomeButton)
