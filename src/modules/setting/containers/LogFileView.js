import { NativeModules } from 'react-native'
import { connect } from 'react-redux'
import LogFileView from '../components/LogFile'
import { getSettingFromState, MODULE_NAME } from '../models'
import AlertView from '../../../common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import SettingKeys from '../models/SettingKeys'
import logger from 'common/utils/logger'
import { setCurrentLog } from '../actions'
import DeviceLogRepository from '../../log/repositories/DeviceLogRepository'
import { loading } from '../../../common/sideEffects'
import I18n from 'i18n-js'
import NetworkError from 'common/errors/NetworkError'

const mapDispatchToProps = dispatch => ({
  onSelectLogPath: async (path) => {
    try {
      const log = await logger.readLogFile(path)
      dispatch(setCurrentLog(log))
    } catch (error) {
      AlertView.show('ログの読み込みに失敗しました。') // TODO 翻訳対応
    }
  },
  onSendLog: async (log) => {
    try {
      await loading(dispatch, async () => {
        await DeviceLogRepository.push({
          log_text: log,
          device_log_code: 'pos.logfile',
          extra_data: JSON.stringify({
          })
        })
      })
      AlertView.show(I18n.t('message.J-01-I003'))
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.J-01-E001')
      AlertView.show(message)
    }
  },
  onSendCrashLog: async (logNames) => {
    return await loading(dispatch, async () => {
      try {
        for (const logName of logNames) {
          console.log('sendLog ', logName)
          const report = await new Promise((resolve) => {
            NativeModules.CrashReportBridge.getRreport(logName, resolve)
          })
          if (report) {
            await DeviceLogRepository.push({
              log_text: report,
              device_log_code: 'pos.crashlog',
            })
            NativeModules.CrashReportBridge.removeReport(logName)
          }
        }

        const epsonLog = await new Promise((resolve) => {
          NativeModules.CrashReportBridge.getEpsonLog(resolve)
        });
        if (epsonLog) {
          await DeviceLogRepository.push({
            log_text: epsonLog,
            device_log_code: 'pos.printerlog',
          })
        }
        AlertView.show(I18n.t('message.J-01-I003'))
        return true
      } catch (error) {
        console.log(error)
        const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.J-01-E001')
        AlertView.show(message)
        return false
      }
    })
  },
  onForceCrash: async () => {
    const isOk = await ConfirmView.showAsync(I18n.t('settings_log.confirm_force_crash'))
    if (isOk) {
      NativeModules.CrashReportBridge.forceCrash()
    }
  },
})

const mapStateToProps = state => ({
  paths: getSettingFromState(state, SettingKeys.LOG.FILE_PATHS),
  log: state[MODULE_NAME].currentLog
})

export default connect(mapStateToProps, mapDispatchToProps)(LogFileView)
