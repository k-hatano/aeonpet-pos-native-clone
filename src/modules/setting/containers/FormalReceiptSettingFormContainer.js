import { connect } from 'react-redux'
import FormalReceiptSettingForm from '../components/FormalReceiptSettingForm'
import { MODULE_NAME, updateReceiptParameter, mapCashierPropertySetToSettings, backupLocalSettings } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import SettingStorage from '../models/SettingStorage'
import { SETTING_TYPE } from '../models/SettingDefinitions'

const mapDispatchToProps = dispatch => ({
  onUpdateReceiptCommon: async (useShopSetinng, key, value) => {
    if (!useShopSetinng) {
      dispatch(actions.updateSetting({key: key, value: value}))
    }
  },
  onBackupSettings: async (isOn) => {
    if (isOn) {
      await backupLocalSettings(SettingKeys.BACKUP.COMMON)
      const values = await mapCashierPropertySetToSettings()
      updateReceiptParameter(values, 'COMMON', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.COMMON.USE_SHOP_SETTING, value: isOn}))
    } else {
      const values = await SettingStorage.load(SettingKeys.BACKUP.COMMON, SETTING_TYPE.OBJECT)
      updateReceiptParameter(values, 'COMMON', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.COMMON.USE_SHOP_SETTING, value: isOn}))
    }
  }
})

const mapStateToProps = state => ({
  useShopSetting: state[MODULE_NAME].settings[SettingKeys.RECEIPT.COMMON.USE_SHOP_SETTING],
  isPrintStaff: state[MODULE_NAME].settings[SettingKeys.RECEIPT.COMMON.IS_PRINT_STAFF],
  isPrintTaxStamp: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.COMMON.IS_PRINT_TAX_STAMP],
  taxOfficeName: state[MODULE_NAME].settings[SettingKeys.RECEIPT.COMMON.TAX_OFFICE_NAME]
})

export default connect(mapStateToProps, mapDispatchToProps)(FormalReceiptSettingForm)
