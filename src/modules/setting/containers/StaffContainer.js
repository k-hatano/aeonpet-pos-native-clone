import { connect } from 'react-redux'
import Staff from '../components/Staff'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import StaffRepository from 'modules/staff/repositories/StaffRepository'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from 'common/sideEffects'
import moment from 'moment'

const mapDispatchToProps = dispatch => ({
  onUpdateFixStaff: async (isFix) => {
    dispatch(actions.updateSetting({key: SettingKeys.STAFF.IS_FIX_STAFF, value: isFix}))
  },
  onSyncStaff: async () => {
    try {
      await loading(dispatch, async () => {
        await StaffRepository.syncStaff()
      })
      dispatch(actions.updateSetting({ key: SettingKeys.STAFF.CURRENT_STAFF, value: null }))
      dispatch(actions.updateSetting({ key: SettingKeys.STAFF.SYNC_DATE,
        value: moment(new Date()).format('YYYY-MM-DD HH:mm:ss') }))
    } catch (error) {
      AlertView.show('担当者の同期に失敗しました。')
      return false
    }
  }
})

const mapStateToProps = state => ({
  isFixStaff: state[MODULE_NAME].settings[SettingKeys.STAFF.IS_FIX_STAFF],
  currentStaff: state[MODULE_NAME].settings[SettingKeys.STAFF.CURRENT_STAFF],
  syncDate: state[MODULE_NAME].settings[SettingKeys.STAFF.SYNC_DATE]
})

export default connect(mapStateToProps, mapDispatchToProps)(Staff)
