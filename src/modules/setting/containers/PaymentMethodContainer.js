import { connect } from 'react-redux'
import PaymentMethod from '../components/PaymentMethod'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import PaymentMethodRepository from 'modules/payment/repositories/PaymentMethodRepository'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from 'common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onFindPaymentMethod: async () => {
    try {
      const methods = await PaymentMethodRepository.findAll()
      dispatch(actions.setPaymentMethods(methods))
    } catch (error) {
      AlertView.show('Error')
    }
  },
  onSyncPaymentMethod: async () => {
    try {
      await loading(dispatch, async () => {
        await PaymentMethodRepository.syncPaymentMethod()
        const methods = await PaymentMethodRepository.findAll()
        dispatch(actions.setPaymentMethods(methods))
      })
    } catch (error) {
      AlertView.show('支払方法の同期に失敗しました。')
    }
  },
  onUpdatePayment: async (serviceId) => {
    dispatch(actions.updateSetting({key: SettingKeys.ORDER.PAYMENT_SERVICE, value: serviceId}))
  }
})

const mapStateToProps = state => ({
  payment_service: state[MODULE_NAME].settings[SettingKeys.ORDER.PAYMENT_SERVICE],
  paymentMethods: state[MODULE_NAME].paymentMethods
})

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethod)
