import { connect } from 'react-redux'
import OutputSetting from '../components/OutputSetting'
import { updateSetting, setLogLevel } from '../actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import logger from '../../../common/utils/logger'
import { MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
  onChangeLogLevel: async (item) => {
    try {
      dispatch(updateSetting({key: SettingKeys.LOG.LOG_LEVEL, value: item.value}))
      logger.setLogLevel(item.value)
    } catch (error) {
      console.error()
    }
  },
  setStateLogLevel: async (value) => {
    dispatch(setLogLevel(value))
  }
})

const mapStateToProps = state => ({
  logLevel: state[MODULE_NAME].logLevel
})

export default connect(mapStateToProps, mapDispatchToProps)(OutputSetting)
