import { connect } from 'react-redux'
import PaymentService from '../components/PaymentService'
import { getSettingFromState } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onUpdatePaymentTerminal: async (key, value) => {
    dispatch(actions.updateSetting({key: key, value: value}))
  }
})

const mapStateToProps = state => ({
  maker: getSettingFromState(state, SettingKeys.ORDER.PAYMENT_SERVICE),
  address: getSettingFromState(state, SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ADDRESS),
  port: getSettingFromState(state, SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.PORT)
})

export default connect(mapStateToProps, mapDispatchToProps)(PaymentService)
