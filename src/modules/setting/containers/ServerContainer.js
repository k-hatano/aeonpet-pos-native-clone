import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import Server from '../components/Server'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import logout from 'modules/login/models/logout'
import AppEvents from 'common/AppEvents'
import { REPOSITORY_CONTEXT } from '../../../common/AppEvents'
import {listPermissions} from '../../staff/actions'

const mapDispatchToProps = dispatch => ({
  onUpdateRequestTime: async (time) => {
    dispatch(actions.updateSetting({key: SettingKeys.NETWORK.ACCESS.REQUEST_TIMEOUT, value: time}))
  },
  onLogout: async () => {
    await logout(dispatch)
    dispatch(actions.updateSetting({
      key: SettingKeys.COMMON.REPOSITORY_CONTEXT,
      value: REPOSITORY_CONTEXT.STANDARD
    }))
    dispatch(listPermissions([]))
    AppEvents.repositoryContextChange('standard')
    Actions.loginUserPage()
  }
})

const mapStateToProps = state => ({
  serverUrl: state[MODULE_NAME].settings[SettingKeys.NETWORK.ACCESS.SERVER_URL],
  shopName: state[MODULE_NAME].settings[SettingKeys.NETWORK.ACCESS.SHOP_NAME],
  cashierName: state[MODULE_NAME].settings[SettingKeys.NETWORK.ACCESS.CASHIER_NAME],
  serverRequestTime: state[MODULE_NAME].settings[SettingKeys.NETWORK.ACCESS.REQUEST_TIMEOUT]
})

export default connect(mapStateToProps, mapDispatchToProps)(Server)
