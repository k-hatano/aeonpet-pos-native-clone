import { connect } from 'react-redux'
import ConsumptionTax from '../components/ConsumptionTax'
import { MODULE_NAME } from '../models'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  useShopSetting: state[MODULE_NAME].settings[SettingKeys.ORDER.TAX.USE_SHOP_SETTING],
  rate: state[MODULE_NAME].settings[SettingKeys.ORDER.TAX.RATE],
  rule: state[MODULE_NAME].settings[SettingKeys.ORDER.TAX.RULE],
  calculateRule: state[MODULE_NAME].settings[SettingKeys.ORDER.TAX.CALCULATE_RULE],
  isTaxfree: !!state[MODULE_NAME].settings[SettingKeys.ORDER.TAX.IS_TAXFREE]
})

export default connect(mapStateToProps, mapDispatchToProps)(ConsumptionTax)
