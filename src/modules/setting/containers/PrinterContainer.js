import { connect } from 'react-redux'
import Printer from '../components/Printer'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onUpdatePrinter: async (key, value) => {
    dispatch(actions.updateSetting({key: key, value: value}))
  }
})

const mapStateToProps = state => ({
  maker: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.PRINTER.MAKER],
  address: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.PRINTER.ADDRESS],
  useCashChanger: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER]
})

export default connect(mapStateToProps, mapDispatchToProps)(Printer)
