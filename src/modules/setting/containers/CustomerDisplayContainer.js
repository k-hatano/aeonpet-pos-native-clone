import { connect } from 'react-redux'
import CustomerDisplay from '../components/CustomerDisplay'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onUpdateCustomerDisplay: async (key, value) => {
    dispatch(actions.updateSetting({key: key, value: value}))
  }
})

const mapStateToProps = state => ({
  maker: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER],
  address: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.ADDRESS],
  topMessage: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.TOP_MESSAGE],
  bottomMessage: state[MODULE_NAME].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.BOTTOM_MESSAGE]
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDisplay)
