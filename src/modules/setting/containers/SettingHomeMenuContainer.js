import { connect } from 'react-redux'
import SettingHomeMenu from '../components/SettingHomeMenu'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onUpdateHomeMenu: async (menus, menu) => {
    menus[menu] = !menus[menu]
    dispatch(actions.updateSetting({key: SettingKeys.MENU.SUB_MENU, value: menus}))
  }
})

const mapStateToProps = state => ({
  menus: state[MODULE_NAME].settings[SettingKeys.MENU.SUB_MENU]
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingHomeMenu)
