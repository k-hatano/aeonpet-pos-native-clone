import { connect } from 'react-redux'
import OrderReceiptSettingForm from '../components/OrderReceiptSettingForm'
import { MODULE_NAME, updateReceiptParameter, mapCashierPropertySetToSettings, backupLocalSettings } from '../models'
import * as actions from '../actions'
import SettingKeys from '../models/SettingKeys'
import SettingStorage from '../models/SettingStorage'
import { SETTING_TYPE } from '../models/SettingDefinitions'

const mapDispatchToProps = dispatch => ({
  onUpdateReceiptAccounting: async (useShopSetinng, key, value) => {
    if (!useShopSetinng) {
      dispatch(actions.updateSetting({key: key, value: value}))
    }
  },
  onBackupSettings: async (isOn) => {
    if (isOn) {
      await backupLocalSettings(SettingKeys.BACKUP.ACCOUNTING)
      const values = await mapCashierPropertySetToSettings()
      updateReceiptParameter(values, 'ACCOUNTING', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.ACCOUNTING.USE_SHOP_SETTING, value: isOn}))
    } else {
      const values = await SettingStorage.load(SettingKeys.BACKUP.ACCOUNTING, SETTING_TYPE.OBJECT)
      updateReceiptParameter(values, 'ACCOUNTING', dispatch)
      dispatch(actions.updateSetting({key: SettingKeys.RECEIPT.ACCOUNTING.USE_SHOP_SETTING, value: isOn}))
    }
  }
})

const mapStateToProps = state => ({
  useShopSetting: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.USE_SHOP_SETTING],
  isPrintHeaderLogo: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_HEADER_LOGO],
  headerLogoFile: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.HEADER_LOGO_FILE],
  headerMessage: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE],
  isPrintFooterLogo: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_FOOTER_LOGO],
  footerLogoFile: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.FOOTER_LOGO_FILE],
  footerMessage: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE],
  isPrintZeroProducts: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_ZERO_PRODUCTS],
  isPrintPointInfo: !!state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_POINT_INFO],
  printProductCodeMode: state[MODULE_NAME].settings[SettingKeys.RECEIPT.ACCOUNTING.PRINT_PRODUCT_CODE_MODE]
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderReceiptSettingForm)
