import generateUuid from '../../common/utils/generateUuid'
import SettingKeys from './models/SettingKeys'
import { sampleStaffMap } from '../staff/samples'
import { TAX_RULE } from '../../common/models/Tax'

/** @type {CashierParameterSetEntity} */
export const sampleCashierParameterSet = {
  id: generateUuid(),
  is_print_receipt_header_logo: 1,
  receipt_header_logo_image: '',
  receipt_header_message: [
    'OrangeShop 芝公園店',
    '東京都港区芝公園2-4-1',
    '03-6430-6730',
    '営業時間 10：00-18：00',
    '',
    'ご来店ありがとうございました。',
    'またのお越しをお待ちしております。',
    'クリスマスセール　全品20％OFF'
  ].join('\n'),
  is_print_zero_products: 1,
  print_product_code_mode: 0, // TODO 値の定数化
  is_print_point_info: 1,
  is_print_tax_stamp: 1,
  tax_office_name: '名古屋中村',
  is_print_staff: 1,
  is_print_receipt_footer_logo: 1,
  receipt_footer_message: [
    'ご返品は未使用未洗濯でご購入日より',
    '１ヶ月以内にレシートをご持参ください'
  ].join('\n'),
  is_print_bill_logo: 1,
  bill_logo_image: '',
  bill_message: [
    'OrangeShop 芝公園店',
    '東京都港区芝公園2-4-1',
    '03-6430-6730',
    '営業時間 10：00-18：00',
    '',
    'ご来店ありがとうございました。',
    'またのお越しをお待ちしております。',
    'クリスマスセール　全品20％OFF'
  ].join('\n'),
  bill_proviso: 'TODO 領収証但し書き',
  total_receipt_print_num: 2,
  is_print_product_segment_total: 1,
  is_print_staff_total: 1,
  is_taxfree: 1
}

export const sampleSettings = {
  [SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE]: sampleCashierParameterSet.receipt_header_message,
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_HEADER_LOGO]: 1,
  [SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE]: sampleCashierParameterSet.receipt_footer_message,
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_FOOTER_LOGO]: 1,
  [SettingKeys.RECEIPT.BILL.HEADER_MESSAGE]: sampleCashierParameterSet.bill_message,
  [SettingKeys.STAFF.CURRENT_STAFF]: sampleStaffMap.staff1,
  [SettingKeys.ORDER.TAX.RATE]: 0.08,
  [SettingKeys.ORDER.TAX.RULE]: TAX_RULE.EXCLUDED_FLOOR
}
