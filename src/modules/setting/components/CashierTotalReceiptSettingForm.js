'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import SettingRow from 'common/components/elements/SettingRow'
import OnOffButton from 'common/components/elements/OnOffButton'
import Select from 'common/components/elements/Select'
import PropTypes from 'prop-types'
import SettingKeys from '../models/SettingKeys'
import { NUMBER_OF_RECEIPT } from '../models'

export default class CashierTotalReceiptSettingForm extends Component {
  static propTypes = {
    useShopSetting: PropTypes.bool,
    printNum: PropTypes.number,
    isPrintProductSegmentTotal: PropTypes.bool,
    isPrintStaffTotal: PropTypes.bool,
    onUpdateReceiptCashierTotal: PropTypes.func,
    onBackupSettings: PropTypes.func
  }

  constructor (props) {
    super(props)
    if (this.props.useShopSetting) {
      this.props.onBackupSettings(this.props.useShopSetting)
    }
  }

  render () {
    return (
      <View>
        <SettingRow label={I18n.t('settings_receipt.receipt_print_number')}>
          <View style={{width: 100}}>
            <Select
              disable={true}
              items={NUMBER_OF_RECEIPT}
              displayProperty='label'
              selectedItem={NUMBER_OF_RECEIPT[this.props.printNum - 1]}
              onItemChange={item =>
                this.props.onUpdateReceiptCashierTotal(this.props.useShopSetting, SettingKeys.RECEIPT.CASHIER_TOTAL.PRINT_NUM, item.value)}
              placeholder={String(this.props.printNum)}
              hideDefaultValue={true} />
          </View>
        </SettingRow>
        <SettingRow label={I18n.t('settings_receipt.is_print_product_segment_total')}>
          <OnOffButton
            isOn={this.props.isPrintProductSegmentTotal}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptCashierTotal(this.props.useShopSetting, SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_PRODUCT_SEGMENT_TOTAL, isOn)}
          />
        </SettingRow>
        <SettingRow label={I18n.t('settings_receipt.is_print_staff_total')}>
          <OnOffButton
            isOn={this.props.isPrintStaffTotal}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptCashierTotal(this.props.useShopSetting, SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_STAFF_TOTAL, isOn)}
          />
        </SettingRow>
      </View>
    )
  }
}
