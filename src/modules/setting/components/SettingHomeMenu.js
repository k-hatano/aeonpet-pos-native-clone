'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import SettingMenuButton from './SettingMenuButton'
import PropTypes from 'prop-types'

export default class SettingHomeMenu extends Component {
  static propTypes = {
    menus: PropTypes.object,
    onUpdateHomeMenu: PropTypes.func
  }

  render () {
    let mainMenu = {
      buttonColor: '#a5a5a5',
      activeButtonColor: '#ff9024',
      textColor: '#fff',
      menus: [
        {
          id: 'orderSearch',
          selected: this.props.menus.orderSearch,
          name: I18n.t('settings_menu.orders_search'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'customerSearch',
          selected: this.props.menus.customerSearch,
          name: I18n.t('settings_menu.customer_search'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'customerRegister',
          selected: this.props.menus.customerRegister,
          name: I18n.t('settings_menu.customer_register'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'stockSearch1',
          selected: this.props.menus.stockSearch1,
          name: I18n.t('settings_menu.stock_search'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'return',
          selected: this.props.menus.return,
          name: I18n.t('settings_menu.return'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'depositsAndWithdrawals',
          selected: this.props.menus.depositsAndWithdrawals,
          name: I18n.t('settings_menu.deposits_and_withdrawals')
        },
        {
          id: 'stockAdjustment1',
          selected: this.props.menus.stockAdjustment1,
          name: I18n.t('settings_menu.stock_adjustment'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'stockOut1',
          selected: this.props.menus.stockOut1,
          name: I18n.t('settings_menu.stock_out'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'stockIn1',
          selected: this.props.menus.stockIn1,
          name: I18n.t('settings_menu.stock_in'),
          isAvailableForInventoryManagement: false
        },
        {
          id: 'closeSalesSummary',
          selected: this.props.menus.closeSalesSummary,
          name: I18n.t('settings_menu.close_sales_summary'),
          isAvailableForInventoryManagement: false
        }
      ]
    }

    return (
      <View style={{ flex: 1 }}>
        <SettingMenuButton
          menus={mainMenu.menus}
          onMenuClick={(item) => this.props.onUpdateHomeMenu(this.props.menus, item)}
          buttonColor={mainMenu.buttonColor}
          activeButtonColor={mainMenu.activeButtonColor}
        />
      </View>
    )
  }
}
