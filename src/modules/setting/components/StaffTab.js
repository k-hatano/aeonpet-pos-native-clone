'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import Staff from './Staff'
import StaffContainer from '../containers/StaffContainer'

export default class StaffTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings.staff')}>
          <StaffContainer />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
