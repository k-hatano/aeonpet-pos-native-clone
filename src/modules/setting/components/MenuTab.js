'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import SettingHomeButtonContainer from '../containers/SettingHomeButtonContainer'
import SettingHomeMenuContainer from '../containers/SettingHomeMenuContainer'
import SettingGetter from 'modules/setting/models/settingGetter'

export default class MenuTab extends Component {
  render () {
    return (
      <VerticalTab>
        { SettingGetter.cashierId &&
          <VerticalTabChild tabName={I18n.t('settings_menu.main_menu')}>
            <SettingHomeButtonContainer />
          </VerticalTabChild>
        }
        <VerticalTabChild tabName={I18n.t('settings_menu.sub_menu')}>
          <SettingHomeMenuContainer />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
