'use strict'

import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import I18n from 'i18n-js'
import Select from '../../../../src/common/components/elements/Select'
import settingStyles from '../styles/Setting'
import { LogLevel, getLogLevelLabel } from '../../../common/utils/logger'
import { updateSetting } from 'modules/setting/actions'
import PropTypes from 'prop-types'

export default class OutputSetting extends Component {
  static propTypes = {
    logLevel: PropTypes.number,
    onUpdateLogLevel: PropTypes.func,
    onChangeLogLevel: PropTypes.func,
    setStateLogLevel: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.logLevels = Object.keys(LogLevel).map(key => (
      { label: getLogLevelLabel(LogLevel[ key ]), value: LogLevel[ key ] }
    ))
  }

  render () {
    const logLevels = [
      { label: 'System Error', value: 1 },
      { label: 'Error', value: 2 },
      { label: 'Warning', value: 3 },
      { label: 'Information', value: 4 },
      { label: 'Debug', value: 5 },
      { label: 'Trace', value: 6 }
    ]

    return (
      <View style={ settingStyles.container }>
        <Text style={ settingStyles.componentHeader }>{ I18n.t('setting.output_settings') }</Text>
        <View style={ settingStyles.subContainer }>
          <Text style={ settingStyles.subContainerHeader }>{ I18n.t('setting.log_level') }</Text>
          <Select
            items={logLevels}
            displayProperty='label'
            onItemChange={item => this.props.onUpdateLogLevel(item.value)}
            placeholder={logLevels[this.props.logLevel - 1].label}
            hideDefaultValue={true}
          />
        </View>
      </View>
    )
  }
}

const componentStyles = StyleSheet.create({
  copyButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    padding: 5,
    width: 80,
    backgroundColor: '#d4d4d4',
    borderRadius: 2
  }
})
