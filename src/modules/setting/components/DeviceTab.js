'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import PrinterContainer from '../containers/PrinterContainer'
import CustomerDisplayContainer from '../containers/CustomerDisplayContainer'
import PaymentServiceContainer from '../containers/PaymentServiceContainer'

export default class DeviceTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings_peripheral_device.printer')}>
          <PrinterContainer />
        </VerticalTabChild>

        <VerticalTabChild tabName={I18n.t('settings_peripheral_device.customer_display')}>
          <CustomerDisplayContainer />
        </VerticalTabChild>

        <VerticalTabChild tabName={I18n.t('settings_peripheral_device.payment_service')}>
          <PaymentServiceContainer />
        </VerticalTabChild>

      </VerticalTab>
    )
  }
}
