'use strict'

import React, { Component } from 'react'
import { StyleSheet, Switch, Text, View } from 'react-native'
import I18n from 'i18n-js'

/*
  USAGE:
  <OES />
 */
export default class OES extends Component {
  constructor (props) {
    super(props)

    this.state = { oesCheckout: false }
  }

  _onToggleOESCheckout = () => {
    const { oesCheckout } = this.state
    this.setState({ oesCheckout: !oesCheckout })
  }

  render () {
    return (
      <View style={componentStyles.container}>
        <Text style={componentStyles.componentHeader}>{I18n.t('settings_order.oes')}</Text>
        <View style={componentStyles.subContainer}>
          <Text style={componentStyles.subContainerHeader}>{I18n.t('settings_order.use_oes_checkout')}</Text>
          <Switch onValueChange={() => this._onToggleOESCheckout()} value={this.state.oesCheckout} />
        </View>
      </View>
    )
  }
}

const componentStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  componentHeader: {
    fontSize: 20,
    marginBottom: 20
  },
  subContainer: {
    marginBottom: 10
  },
  subContainerHeader: {
    fontSize: 15
  }
})
