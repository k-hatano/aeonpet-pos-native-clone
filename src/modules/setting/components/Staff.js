'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import componentStyles from '../styles/Staff'
import settingStyles from '../styles/Setting'
import StaffSettings from '../../../common/settings/StaffSettings'
import SettingRow from 'common/components/elements/SettingRow'
import OnOffButton from 'common/components/elements/OnOffButton'
import { showSelectStaffViewModal } from 'modules/staff/containers/SelectStaffViewModal'
import PropTypes from 'prop-types'

export default class Staff extends Component {
  static propTypes = {
    isFixStaff: PropTypes.bool,
    currentStaff: PropTypes.object,
    onUpdateFixStaff: PropTypes.func,
    onSyncStaff: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      fixStaff: this.props.isFixStaff,
      selectedStaff: 'test'
    }
  }

  _onSwitch (value) {
    this.setState({switchOn: value})
    StaffSettings.setIsStaffFixed(value)
  }

  _onItemChoose (staff) {
    this.setState({selectedStaff: staff})
  }

  render () {
    const syncDate = !this.props.syncDate ? '--:--:--' : this.props.syncDate
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings.staff')}</Text>
        <View style={componentStyles.bottomCenter}>
          <TouchableOpacity onPress={async () => { this.props.onSyncStaff() }} style={settingStyles.button}>
            <Text> {I18n.t('settings_staff.sync')} </Text>
          </TouchableOpacity>
          <Text> {syncDate + ' ' + I18n.t('settings_staff.sync_date_message')} </Text>
        </View>
      </View>
    )
  }
}
