'use strict'

import React, { Component } from 'react'
import { Text, TextInput, View } from 'react-native'
import I18n from 'i18n-js'
import Select from '../../../../src/common/components/elements/Select'
import { PRINTER_MAKERS, printerMakerToLabel, printerParameter } from '../models'
import settingStyles from '../styles/Setting'
import componentStyles from '../styles/Printer'
import SettingRow from 'common/components/elements/SettingRow'
import SettingKeys from '../models/SettingKeys'
import OnOffButton from '../../../common/components/elements/OnOffButton'

export default class Printer extends Component {
  constructor () {
    super()
    this.state = {
      model: '',
      interface: 0,
      interfaceLabel: '',
      useAdress: false,
      address: '',
      paperWidth: '',
      sdk: 99,
      useCashChanger: true,
    }
  }

  componentWillMount () {
    const maker = this.props.maker === 3 ? 2 : this.props.maker
    this._setPrinterParameter(maker)
    this.setState({address: this.props.address})
    this.setState({useCashChanger: this.props.useCashChanger})
  }

  componentWillUnmount () {
    this.props.onUpdatePrinter(SettingKeys.EQUIPMENT.PRINTER.ADDRESS,
      this.state.address ? this.state.address : '')
    this.props.onUpdatePrinter(SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER,
      this.state.useCashChanger ? true : false)
  }

  _setPrinterParameter (maker) {
    try {
      console.log('_setPrinterParameter', {maker, params: printerParameter(maker)})
    } catch (error) {
      console.log(error)
    }
    this.setState({model: printerParameter(maker).model})
    this.setState({interfaceLabel: printerParameter(maker).interfaceLabel})
    this.setState({paperWidth: printerParameter(maker).paperWidth})
    this.setState({sdk: printerParameter(maker).sdk})
    if (maker === 1) {
      this.setState({useAdress: true})
    } else {
      this.setState({useAdress: false})
    }
  }

  _renderForEpson () {
    return (
      <View style={settingStyles.container}>
        <SettingRow label={I18n.t('settings_peripheral_device.model')}>
          <Text>{this.state.model}</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.interface')}>
          <Text>{this.state.interfaceLabel}</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.address')}>
          <TextInput
            placeholder={I18n.t('settings_peripheral_device.address')}
            style={componentStyles.textInput}
            editable={this.state.useAdress}
            maxLength={16}
            value={this.state.address}
            onChangeText={(text) => {
              this.setState({address: text})
            }}
          />
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.paper_width')}>
          <Text>{/* this.state.paperWidth + 'mm' */}58mm</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.useCashChanger')}>
          <OnOffButton
            isOn={this.state.useCashChanger}
            onValueChange={(isOn) => this.setState({useCashChanger: isOn})}
          />
        </SettingRow>
      </View>
    )
  }

  _renderForSpecifiedPrinter () {
    // 選択肢からiSAPPOSを除外した分がずれて不整合を起こすので暫定対応
    const maker = this.props.maker === 3 ? 2 : this.props.maker
    switch (maker) {
      case PRINTER_MAKERS.EPSON:
      case PRINTER_MAKERS.EPSON_BLUETOOTH:
        return this._renderForEpson()
      default:
        return (<View />)
    }
  }

  render () {
    const printerMakers = [
      {value: PRINTER_MAKERS.NOT_SELECTED,
        label: printerMakerToLabel(PRINTER_MAKERS.NOT_SELECTED)},
      {value: PRINTER_MAKERS.EPSON,
        label: printerMakerToLabel(PRINTER_MAKERS.EPSON)},
      {value: PRINTER_MAKERS.EPSON_BLUETOOTH,
        label: printerMakerToLabel(PRINTER_MAKERS.EPSON_BLUETOOTH)}
    ]
    // 選択肢からiSAPPOSを除外した分がずれて不整合を起こすので暫定対応
    const maker = this.props.maker === 3 ? 2 : this.props.maker
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_peripheral_device.printer')}</Text>
        <SettingRow label={I18n.t('settings_peripheral_device.maker')}>
          <View style={{width: 300}}>
            <Select
              items={printerMakers}
              displayProperty='label'
              onItemChange={item => {
                this.props.onUpdatePrinter(SettingKeys.EQUIPMENT.PRINTER.MAKER, item.value)
                this._setPrinterParameter(item.value)
              }}
              placeholder={printerMakers[maker].label}
              width={300}
              hideDefaultValue />
          </View>
        </SettingRow>
        {this._renderForSpecifiedPrinter()}
      </View>
    )
  }
}
