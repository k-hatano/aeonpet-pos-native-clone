import React, { Component } from 'react'
import { Text, View } from 'react-native'
import DeviceInfo from 'react-native-device-info'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'

export default class Device extends Component {
  render () {
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_network.server')}</Text>

        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.device_id')}</Text>
          <Text style={settingStyles.subContainerItem}>{DeviceInfo.getUniqueID()}</Text>
        </View>

        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.version')}</Text>
          <Text style={settingStyles.subContainerItem}>{DeviceInfo.getVersion()}</Text>
        </View>
      </View>
    )
  }
}
