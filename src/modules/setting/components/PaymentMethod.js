'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import componentStyles from '../styles/PaymentMethod'
import PaymentMethodListItem from './PaymentMethodListItem'
import { PAYMENT_SERVICE, paymentServiceToLabel } from '../models'
import { PAYMENT_METHOD_TYPE } from 'modules/payment/models'

export default class PaymentMethod extends Component {
  static propTypes = {
    payment_service: PropTypes.number,
    onFindPaymentMethod: PropTypes.func,
    onSyncPaymentMethod: PropTypes.func,
    onUpdatePayment: PropTypes.func
  }

  componentWillMount () {
    this.props.onFindPaymentMethod()
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const paymentServices = [PAYMENT_SERVICE.NOT_SELECTED, PAYMENT_SERVICE.VEGA3000]
      .map(service => ({ value: service, label: paymentServiceToLabel(service) }))

    const methods = []
    if (this.props.paymentMethods) {
      let isExistOnlineCard = false
      this.props.paymentMethods.forEach(payment => {
        if (payment.payment_method_type === PAYMENT_METHOD_TYPE.CREDIT && payment.is_offline === 0) {
          if (!isExistOnlineCard) {
            methods.push(payment)
            isExistOnlineCard = true
          }
        } else {
          methods.push(payment)
        }
      })
    }
    return (
      <View style={componentStyles.container}>
        <Text style={componentStyles.componentHeader}>{I18n.t('settings_order.payment_method')}</Text>
        {
          methods &&
          <ListView
            enableEmptySections
            dataSource={dataSource.cloneWithRows(methods)}
            renderRow={payment =>
              <PaymentMethodListItem
                paymentMethod={payment}
                paymentServices={paymentServices}
                onUpdatePayment={this.props.onUpdatePayment}
                selectedPaymentService={this.props.payment_service}
              />
            }
          />
        }
        <View style={componentStyles.bottomCenter}>
          <TouchableOpacity
            onPress={() => this.props.onSyncPaymentMethod()}
            style={componentStyles.rowButton}>
            <Text> {I18n.t('settings_staff.sync')} </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
