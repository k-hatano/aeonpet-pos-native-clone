'use strict'

import React, { Component } from 'react'
import { Text, TextInput, View } from 'react-native'
import I18n from 'i18n-js'
import Select from '../../../../src/common/components/elements/Select'
import { CUSTOMER_DISPLAY_MAKERS, customerDisplayParameter, customerDisplayMakerToLabel } from '../models'
import settingStyles from '../styles/Setting'
import componentStyles from '../styles/Printer'
import SettingRow from 'common/components/elements/SettingRow'
import SettingKeys from '../models/SettingKeys'
import ReceiptTextConverter from 'modules/printer/models/ReceiptTextConverter'

export default class CustomerDisplay extends Component {
  constructor () {
    super()
    this.state = {
      model: '',
      interface: '',
      interfaceLabel: '',
      useAdress: false,
      address: '',
      topMessage: '',
      bottomMessage: ''
    }
  }
  
  componentWillMount () {
    this._setCustomerDisplayParameter(this.props.maker)
    this.setState({address: this.props.address})
    this.setState({topMessage: this.props.topMessage})
    this.setState({bottomMessage: this.props.bottomMessage})
  }

  componentWillUnmount () {
    this.props.onUpdateCustomerDisplay(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.ADDRESS,
      this.state.address ? this.state.address : '')
    this.props.onUpdateCustomerDisplay(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.TOP_MESSAGE,
      this.state.topMessage ? this.state.topMessage : '')
    this.props.onUpdateCustomerDisplay(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.BOTTOM_MESSAGE,
      this.state.bottomMessage ? this.state.bottomMessage : '')
  }

  _setCustomerDisplayParameter (maker) {
    this.setState({model: customerDisplayParameter(maker).model})
    this.setState({interface: customerDisplayParameter(maker).interface})
    this.setState({interfaceLabel: customerDisplayParameter(maker).interfaceLabel})
    this.setState({useAdress: maker !== CUSTOMER_DISPLAY_MAKERS.NOT_SELECTED})
  }

  render () {
    const customerDisplayMakers = [
      {value: CUSTOMER_DISPLAY_MAKERS.NOT_SELECTED,
        label: customerDisplayMakerToLabel(CUSTOMER_DISPLAY_MAKERS.NOT_SELECTED)},
      {value: CUSTOMER_DISPLAY_MAKERS.EPSON,
        label: customerDisplayMakerToLabel(CUSTOMER_DISPLAY_MAKERS.EPSON)},
      {value: CUSTOMER_DISPLAY_MAKERS.EPSON_BLUETOOTH,
        label: customerDisplayMakerToLabel(CUSTOMER_DISPLAY_MAKERS.EPSON_BLUETOOTH)},
    ]
    const receiptTextConverter = new ReceiptTextConverter()
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_peripheral_device.customer_display')}</Text>

        <SettingRow label={I18n.t('settings_peripheral_device.maker')}>
          <View style={{width: 300}}>
            <Select
              items={customerDisplayMakers}
              displayProperty='label'
              onItemChange={item => {
                this.props.onUpdateCustomerDisplay(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER, item.value)
                this._setCustomerDisplayParameter(item.value)
              }}
              placeholder={customerDisplayMakers[this.props.maker].label}
              hideDefaultValue={true} />
          </View>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.model')}>
          <Text>{this.state.model}</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.interface')}>
          <Text>{this.state.interfaceLabel}</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.address')}>
          <TextInput
            placeholder={I18n.t('settings_peripheral_device.address')}
            style={componentStyles.textInput}
            editable={this.state.useAdress}
            maxLength={16}
            value={this.state.address}
            onChangeText={(text) => this.setState({address: text})}
          />
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.top_waiting_message')}>
          <TextInput
            placeholder={I18n.t('settings_peripheral_device.top_waiting_message')}
            style={componentStyles.textInput}
            value={this.state.topMessage}
            onChangeText={(text) => {
              if (receiptTextConverter.lengthByShiftJisByte(text) < 21) {
                this.setState({topMessage: text})
              }
            }}
          />
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.bottom_waiting_message')}>
          <TextInput
            placeholder={I18n.t('settings_peripheral_device.bottom_waiting_message')}
            style={componentStyles.textInput}
            value={this.state.bottomMessage}
            onChangeText={(text) => {
              if (receiptTextConverter.lengthByShiftJisByte(text) < 21) {
                this.setState({bottomMessage: text})
              }
            }}
          />
        </SettingRow>
      </View>
    )
  }
}
