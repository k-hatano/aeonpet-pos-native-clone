import React from 'react'
import { Text, View, Switch } from 'react-native'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import PropTypes from 'prop-types'
import Select from 'common/components/elements/Select'
import SettingKeys from '../models/SettingKeys'
import { LogLevel } from '../../../common/utils/logger'

export default class LogConfiguration extends React.Component {
  static propTypes = {
    level: PropTypes.number,
    reactNativeError: PropTypes.bool,
    rotationDays: PropTypes.number,
    onUpdateSetting: PropTypes.func
  }

  render () {
    const daysOptions = []
    for (let i = 1; i < 30; i++) {
      daysOptions.push({ label: i, value: i })
    }

    const levels = []
    for (const i in LogLevel) {
      levels.push({ label: i, value: LogLevel[i] })
    }
    levels.reverse()

    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_log.configuration')}</Text>
        <View style={settingStyles.subContainer}>
          <View style={{ marginBottom: 10 }}>
            <Text>{I18n.t('settings_log.log_rotation_days')}</Text>
          </View>
          <View style={{ marginBottom: 10 }}>
            <Select
              style={{width: 100}}
              items={daysOptions}
              selectedItem={daysOptions[this.props.rotationDays - 1]}
              onItemChange={(item) => this.props.onUpdateSetting(SettingKeys.LOG.ROTATION_DAYS, item.value)}
              hideDefaultValue={true} />
          </View>
          <View style={{ marginBottom: 10 }}>
            <Text>{I18n.t('settings_log.log_level')}</Text>
          </View>
          <View style={{ marginBottom: 10 }}>
            <Select
              style={{width: 100}}
              items={levels}
              selectedItem={levels[this.props.level - 1]}
              onItemChange={(item) => this.props.onUpdateSetting(SettingKeys.LOG.LEVEL, item.value)}
              hideDefaultValue={true} />
          </View>
          <View style={{ marginBottom: 10 }}>
            <Text>{I18n.t('settings_log.enable_error_log')}</Text>
          </View>
          <View style={{ marginBottom: 10 }}>
            <Switch
              onValueChange={value => this.props.onUpdateSetting(SettingKeys.LOG.REACT_NATIVE_ERROR, value)}
              value={this.props.reactNativeError} />
          </View>
        </View>
      </View>
    )
  }
}
