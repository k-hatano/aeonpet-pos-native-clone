'use strict'

import React, { Component } from 'react'
import { Text, TextInput, View } from 'react-native'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import componentStyles from '../styles/Printer'
import SettingRow from 'common/components/elements/SettingRow'
import SettingKeys from '../models/SettingKeys'
import { paymentServiceToLabel } from 'modules/setting/models'

export default class PaymentService extends Component {
  constructor (props) {
    super(props)
    this.state = {
      address: props.address,
      port: props.port
    }
  }

  componentWillUnmount () {
    this.props.onUpdatePaymentTerminal(
      SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ADDRESS,
      this.state.address ? this.state.address : '')
    this.props.onUpdatePaymentTerminal(
      SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.PORT,
      this.state.port ? this.state.port : '')
  }

  render () {
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_peripheral_device.payment_service')}</Text>

        <SettingRow label={I18n.t('settings_peripheral_device.payment_service')}>
          <Text>{paymentServiceToLabel(this.props.maker)}</Text>
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.ip_address')}>
          <TextInput
            placeholder={I18n.t('settings_peripheral_device.ip_address')}
            autoCapitalize='none'
            style={componentStyles.textInput}
            maxLength={15}
            value={this.state.address}
            onChangeText={(text) => this.setState({address: text})}
          />
        </SettingRow>

        <SettingRow label={I18n.t('settings_peripheral_device.port_number')}>
          <TextInput
            autoCapitalize='none'
            placeholder={I18n.t('settings_peripheral_device.port_number')}
            style={componentStyles.textInput}
            maxLength={5}
            value={this.state.port}
            onChangeText={(text) => this.setState({port: text})}
          />
        </SettingRow>

      </View>
    )
  }
}
