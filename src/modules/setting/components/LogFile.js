'use strict'

import React, { Component } from 'react'
import { ListView, StyleSheet, Text, TouchableOpacity, View, ScrollView, NativeModules } from 'react-native'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import PropTypes from 'prop-types'

export default class LogFile extends Component {
  static propTypes = {
    log: PropTypes.string,
    paths: PropTypes.array,
    onSelectLogPath: PropTypes.func,
    onSendLog: PropTypes.func,
    onSendCrashLog: PropTypes.func,
    onForceCrash: PropTypes.func,
  }

  constructor (props) {
    super(props)

    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = { crashLogNames: [] }
  }

  componentDidMount () {
    NativeModules.CrashReportBridge.getRreportFileNames((reports) => {
      this.setState({ crashLogNames: reports })
    })
  }

  onSendCrashLog = async () => {
    const result = await this.props.onSendCrashLog(this.state.crashLogNames)
    if (result) {
      this.setState({ crashLogNames: [] })
    }
  }

  render () {
    let source = this.dataSource.cloneWithRows(this.props.paths || [])

    const logLines = this.props.log.split('\n')
    const log = logLines.slice(-300).join('\n')

    return (
      <View style={[settingStyles.container, componentStyles.container]}>
        <View style={componentStyles.logListArea}>
          <Text style={settingStyles.componentHeader}>
            {I18n.t('setting.log_file')}
          </Text>
          <ListView dataSource={source} renderRow={rowData => (
            <TouchableOpacity onPress={() => this.props.onSelectLogPath(rowData)}>
              <Text style={componentStyles.rowData}>{rowData}</Text>
            </TouchableOpacity>
          )}
          />
          <View style={componentStyles.hiddenArea}>
            <ScrollView horizontal style={componentStyles.hiddenAreaScroll}>
              <View style={componentStyles.hiddenAreaScrollContent}>
                <TouchableOpacity
                  onPress={() => this.props.onForceCrash()}
                  style={settingStyles.button}>
                  <Text> {I18n.t('settings_log.force_crash')} </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
          <View style={componentStyles.buttonArea}>
            {!(this.props.log && this.props.log !== '') ? null : (
              <TouchableOpacity
                onPress={() => this.props.onSendLog(this.props.log)}
                style={settingStyles.button}>
                <Text> {I18n.t('settings_log.send_log')} </Text>
              </TouchableOpacity>
            )}
            {this.state.crashLogNames.length > 0 &&
              <TouchableOpacity
                onPress={this.onSendCrashLog}
                style={[settingStyles.button, componentStyles.crashLogButton]}>
                <Text>{I18n.t('settings_log.send_crash_log')}</Text>
              </TouchableOpacity>
            }
          </View>
        </View>
        <View style={componentStyles.logTextArea}>
          <ScrollView
            ref='scrollView'
            style={componentStyles.logTextScroll}
            onContentSizeChange={() => this.refs.scrollView.scrollToEnd()} >
            <Text>{log}</Text>
          </ScrollView>
        </View>
      </View>
    )
  }
}

const componentStyles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  backupButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  logListArea: {
  },
  backupButton: {
    padding: 5,
    width: 80,
    backgroundColor: '#d4d4d4',
    borderRadius: 2,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  rowData: {
    marginHorizontal: 20,
    marginVertical: 8,
    fontSize: 18
  },
  logTextArea: {
    flex: 1
  },
  logTextScroll: {
  },
  hiddenArea: {
    height: 40,
    width: 200,
    marginBottom: 100
  },
  hiddenAreaScroll: {
    height: 40,
    width: 200,
  },
  hiddenAreaScrollContent: {
    height: 40,
    width: 2000,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  buttonArea: {
    height: 30,
    flexDirection: 'row',
  },
  crashLogButton: {
    backgroundColor: 'red',
    width: 120,
    marginLeft: 4,
  }
})
