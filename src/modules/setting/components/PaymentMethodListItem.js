import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { PAYMENT_METHOD_TYPE } from 'modules/payment/models'
import componentStyles from '../styles/PaymentMethod'
import PropTypes from 'prop-types'
import Select from 'common/components/elements/Select'
import I18n from 'i18n-js'

export default class PaymentMethodListItem extends Component {
 static propTypes = {
   paymentMethod: PropTypes.object,
   paymentServices: PropTypes.array,
   onUpdatePayment: PropTypes.func,
   selectedPaymentService: PropTypes.number
 }

 constructor (props) {
   super(props)
   this.state = {
     selectedPaymentService: props.selectedPaymentService ? props.selectedPaymentService : 0
   }
 }

 getStyle () {
   switch (this.props.paymentMethod.payment_method_type) {
     case PAYMENT_METHOD_TYPE.CREDIT:
       if (this.props.paymentMethod.is_offline) {
         return componentStyles.otherContainer
       }
       return componentStyles.creditContainer
     default:
       return componentStyles.otherContainer
   }
 }

 getMethodName () {
   switch (this.props.paymentMethod.payment_method_type) {
     case PAYMENT_METHOD_TYPE.CREDIT:
       if (this.props.paymentMethod.is_offline) {
         return this.props.paymentMethod.name
       }
       return I18n.t('cart.credit_card')
     default:
       return this.props.paymentMethod.name
   }
 }

 getInnerComponent () {
   switch (this.props.paymentMethod.payment_method_type) {
     case PAYMENT_METHOD_TYPE.CREDIT:
       if (this.props.paymentMethod.is_offline) {
         return
       }
       const selectedPayment = this.state.selectedPaymentService ? this.state.selectedPaymentService : 0
       return (
         <Select
           items={this.props.paymentServices}
           displayProperty='label'
           onItemChange={item => {
             this.props.onUpdatePayment(item.value)
             this.setState({selectedPaymentService: item.value})
           }}
           placeholder={this.props.paymentServices[selectedPayment].label}
           hideDefaultValue
         />
       )
     case PAYMENT_METHOD_TYPE.EMONEY:
     case PAYMENT_METHOD_TYPE.UNION_PAY:
       if (this.props.paymentMethod.is_offline !== 0) {
         return
       }
       return (
         <Text style={componentStyles.paymentMethodSameText}>
           {I18n.t('settings_order.same_credit_setting')}
         </Text>
       )
   }
 }

 render () {
   const style = this.getStyle()
   return (
     <View style={style}>
       <Text style={componentStyles.paymentMethodName}>
         {this.getMethodName()}
       </Text>
       <View style={componentStyles.paymentMethodSelector}>
         {this.getInnerComponent()}
       </View>
     </View>
   )
 }
}
