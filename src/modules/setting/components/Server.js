import React, { Component } from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/Server'
import settingStyles from '../styles/Setting'
import { SERVER_REQUEST_TIMEOUT } from '../models'
import Select from 'common/components/elements/Select'
import PropTypes from 'prop-types'
import ConfirmView from 'common/components/widgets/ConfirmView'

export default class Server extends Component {
  static propTypes = {
    serverRequestTime: PropTypes.node,
    serverUrl: PropTypes.string,
    shopName: PropTypes.string,
    cashierName: PropTypes.string,
    onUpdateRequestTime: PropTypes.func,
    onLogout: PropTypes.func
  }

  render () {
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_network.server')}</Text>

        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.access_point')}</Text>
          <Text style={settingStyles.subContainerItem}>{this.props.serverUrl}</Text>
        </View>

        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.shop_name')}</Text>
          <Text style={settingStyles.subContainerItem}>{this.props.shopName}</Text>
        </View>

        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.pos_name')}</Text>
          <Text style={settingStyles.subContainerItem}>{this.props.cashierName}</Text>
        </View>
        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_network.timeout')}</Text>
          <View style={settingStyles.selectContainer}>
            <Select
              items={SERVER_REQUEST_TIMEOUT}
              displayProperty='label'
              onItemChange={item => this.props.onUpdateRequestTime(String(item.value))}
              placeholder={this.props.serverRequestTime + I18n.t('common.second')}
              hideDefaultValue={true}/>
          </View>
        </View>

        <View style={settingStyles.fotterLeftbutton}>
          <TouchableOpacity onPress={() => ConfirmView.show(I18n.t('message.J-01-I001'), this.props.onLogout)} style={styles.button}>
            <Text> {I18n.t('settings_network.logout')} </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
