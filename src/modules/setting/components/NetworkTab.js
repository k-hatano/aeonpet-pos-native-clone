'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import ServerContainer from '../containers/ServerContainer'
import Device from './Device'

export default class NetworkTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings_network.server')}>
          <ServerContainer />
        </VerticalTabChild>
        <VerticalTabChild tabName={I18n.t('settings_network.device')}>
          <Device />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
