'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import OrderReceiptSettingFormContainer from '../containers/OrderReceiptSettingFormContainer'
import FormalReceiptSettingFormContainer from '../containers/FormalReceiptSettingFormContainer'
import CashierTotalReceiptSettingFormContainer from '../containers/CashierTotalReceiptSettingFormContainer'
import BillReceiptSettingFormContainer from '../containers/BillReceiptSettingFormContainer'

export default class ReceiptTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings_receipt.order_receipt')}>
          <OrderReceiptSettingFormContainer />
        </VerticalTabChild>
        <VerticalTabChild tabName={I18n.t('settings_receipt.bill_receipt')}>
          <BillReceiptSettingFormContainer />
        </VerticalTabChild>
        <VerticalTabChild tabName={I18n.t('settings_receipt.formal_receipt')}>
          <FormalReceiptSettingFormContainer />
        </VerticalTabChild>
        <VerticalTabChild
          tabName={`${I18n.t('settings_receipt.check_sales')} / ${I18n.t('settings_receipt.settlement_receipt')}`}
        >
          <CashierTotalReceiptSettingFormContainer />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
