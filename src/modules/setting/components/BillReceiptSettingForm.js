'use strict'

import React, { Component } from 'react'
import { TextInput, View, Text, Image, StyleSheet, ScrollView } from 'react-native'
import SettingRow from '../../../common/components/elements/SettingRow'
import OnOffButton from '../../../common/components/elements/OnOffButton'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import PropTypes from 'prop-types'
import SettingKeys from '../models/SettingKeys'
import { imageFilePath } from '../models'

export default class BillReceiptSettingForm extends Component {
  static propTypes = {
    useShopSetting: PropTypes.bool,
    isPrintHeaderLogo: PropTypes.bool,
    headerLogoFile: PropTypes.string,
    headerMessage: PropTypes.string,
    proviso: PropTypes.string,
    onUpdateReceiptBill: PropTypes.func,
    onBackupSettings: PropTypes.func
  }

  constructor (props) {
    super(props)
    if (this.props.useShopSetting) {
      this.props.onBackupSettings(this.props.useShopSetting)
    }
  }

  componentWillMount () {
    this.state = {
      billHeaderMessage: this.props.headerMessage,
      proviso: this.props.proviso
    }
  }

  componentWillReceiveProps (nextProps) {
    this.state = {
      billHeaderMessage: nextProps.headerMessage,
      proviso: nextProps.proviso
    }
  }

  componentWillUnmount () {
    this.props.onUpdateReceiptBill(this.props.useShopSetting, SettingKeys.RECEIPT.BILL.HEADER_MESSAGE,
      this.state.billHeaderMessage ? this.state.billHeaderMessage : '')
    this.props.onUpdateReceiptBill(this.props.useShopSetting, SettingKeys.RECEIPT.BILL.PROVISO,
      this.state.proviso ? this.state.proviso : '')
  }

  _backUp (isOn) {
    this.props.onUpdateReceiptBill(this.props.useShopSetting, SettingKeys.RECEIPT.BILL.HEADER_MESSAGE,
      this.state.billHeaderMessage ? this.state.billHeaderMessage : '')
    this.props.onUpdateReceiptBill(this.props.useShopSetting, SettingKeys.RECEIPT.BILL.PROVISO,
      this.state.proviso ? this.state.proviso : '')
    this.props.onBackupSettings(isOn)
  }

  render () {
    const booleanTrue = true
    return (
      <ScrollView>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_receipt.bill_receipt')}</Text>

        <SettingRow label={I18n.t('settings_receipt.use_header_logo')}>
          <OnOffButton
            isOn={this.props.isPrintHeaderLogo}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptBill(this.props.useShopSetting, SettingKeys.RECEIPT.BILL.IS_PRINT_HEADER_LOGO, isOn)}
          />
        </SettingRow>

        <View style={orderReceiptStyles.imgContainer}>
          <Image
            style={orderReceiptStyles.imgContent}
            resizeMode='contain'
            source={this.props.headerLogoFile ? {uri: imageFilePath(this.props.headerLogoFile)} : null}
          />
        </View>

        <View style={{ marginBottom: 10 }}>
          <Text>{I18n.t('settings_receipt.header_message')}</Text>
        </View>
        <View style={{ marginBottom: 10 }}>
          <TextInput
            placeholder={I18n.t('settings_receipt.header_message')}
            style={orderReceiptStyles.inputContent}
            multiline={booleanTrue}
            editable={!this.props.useShopSetting}
            maxLength={40}
            value={this.state.billHeaderMessage}
            onChangeText={(text) => this.setState({billHeaderMessage: text})}
          />
        </View>

        <View style={{ marginBottom: 10 }}>
          <Text>{I18n.t('settings_receipt.proviso')}</Text>
        </View>
        <View style={{ marginBottom: 10 }}>
          <TextInput
            placeholder={I18n.t('settings_receipt.proviso')}
            style={orderReceiptStyles.inputContent}
            multiline={booleanTrue}
            editable={!this.props.useShopSetting}
            maxLength={40}
            value={this.state.proviso}
            onChangeText={(text) => this.setState({proviso: text})}
          />
        </View>
      </ScrollView>
    )
  }
}

const orderReceiptStyles = StyleSheet.create({
  imgContainer: {
    width: 280,
    borderWidth: 1,
    borderColor: '#d7d7d7',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  imgContent: {
    margin: 5,
    width: 270,
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  button: {
    marginBottom: 10,
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5
  },
  inputContent: {
    width: '92%',
    borderWidth: 2,
    borderColor: '#828282',
    paddingLeft: 5,
    height: 60,
    borderRadius: 5
  }
})
