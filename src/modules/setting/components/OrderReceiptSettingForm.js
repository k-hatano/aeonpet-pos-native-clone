'use strict'

import React, { Component } from 'react'
import { TextInput, View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import SettingRow from '../../../common/components/elements/SettingRow'
import OnOffButton from '../../../common/components/elements/OnOffButton'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import styles from '../styles/OrderReceiptSettingForm'
import { PRINT_MODE, imageFilePath } from '../models'
import { RadioGroup } from 'common/components/elements/Inputs'
import PropTypes from 'prop-types'
import SettingKeys from '../models/SettingKeys'

export default class OrderReceiptSettingForm extends Component {
  static propTypes = {
    useShopSetting: PropTypes.bool,
    isPrintHeaderLogo: PropTypes.bool,
    headerLogoFile: PropTypes.string,
    headerMessage: PropTypes.string,
    isPrintFooterLogo: PropTypes.bool,
    footerLogoFile: PropTypes.string,
    footerMessage: PropTypes.string,
    isPrintZeroProducts: PropTypes.bool,
    isPrintPointInfo: PropTypes.bool,
    printProductCodeMode: PropTypes.number,
    onUpdateReceiptAccounting: PropTypes.func,
    onBackupSettings: PropTypes.func
  }

  constructor (props) {
    super(props)
    if (this.props.useShopSetting) {
      this.props.onBackupSettings(this.props.useShopSetting)
    }
  }

  componentWillMount () {
    this.state = {
      headerMessage: this.props.headerMessage,
      footerMessage: this.props.footerMessage,
      modeOptions: this._codeModeOptions(this.props)
    }
  }

  componentWillReceiveProps (nextProps) {
    this.state = {
      headerMessage: nextProps.headerMessage,
      footerMessage: nextProps.footerMessage,
      modeOptions: this._codeModeOptions(nextProps)
    }
  }

  componentWillUnmount () {
    this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE,
      this.state.headerMessage ? this.state.headerMessage : '')
    this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE,
      this.state.footerMessage ? this.state.footerMessage : '')
  }

  _codeModeOptions (props) {
    const codeModeOptions = [
      { label: I18n.t('order.product_code'), value: PRINT_MODE.ITEM_NUMBER },
      { label: I18n.t('order.jan_code'), value: PRINT_MODE.JAN_CODE },
      { label: I18n.t('settings_receipt.no_print'), value: PRINT_MODE.DO_NOT_PRINT }
    ]
    switch (props.printProductCodeMode) {
      case PRINT_MODE.ITEM_NUMBER: codeModeOptions[0].selected = true
        break
      case PRINT_MODE.JAN_CODE: codeModeOptions[1].selected = true
        break
      case PRINT_MODE.DO_NOT_PRINT: codeModeOptions[2].selected = true
        break
    }
    return codeModeOptions
  }

  _backUp (isOn) {
    this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE,
      this.state.headerMessage ? this.state.headerMessage : '')
    this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE,
      this.state.footerMessage ? this.state.footerMessage : '')
    this.props.onBackupSettings(isOn)
  }

  render () {
    return (
      <ScrollView>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_receipt.order_receipt')}</Text>
        <SettingRow label={I18n.t('settings_receipt.use_header_logo')}>
          <OnOffButton
            isOn={this.props.isPrintHeaderLogo}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptAccounting(
                this.props.useShopSetting,
                SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_HEADER_LOGO, isOn)}
          />
        </SettingRow>
        <View style={styles.imgContainer}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={styles.imgContent}
              resizeMode='contain'
              source={this.props.headerLogoFile ? {uri: imageFilePath(this.props.headerLogoFile)} : null}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.formContainer}>
          <Text>{I18n.t('settings_receipt.header_message')}</Text>
        </View>
        <View style={styles.formContainer}>
          <TextInput
            placeholder={I18n.t('settings_receipt.header_message')}
            style={styles.inputContent}
            editable={!this.props.useShopSetting}
            maxLength={40}
            value={this.state.headerMessage}
            onChangeText={(text) => this.setState({headerMessage: text})}
            multiline
          />
        </View>
        <SettingRow label={I18n.t('settings_receipt.use_footer_logo')}>
          <OnOffButton
            isOn={this.props.isPrintFooterLogo}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_FOOTER_LOGO, isOn)}
          />
        </SettingRow>
        <View style={styles.imgContainer}>
          <Image
            style={styles.imgContent}
            resizeMode='contain'
            source={this.props.footerLogoFile ? {uri: imageFilePath(this.props.footerLogoFile)} : null}
          />
        </View>
        <View style={styles.formContainer}>
          <Text>{I18n.t('settings_receipt.footer_message')}</Text>
        </View>
        <View style={styles.formContainer}>
          <TextInput
            placeholder={I18n.t('settings_receipt.footer_message')}
            style={styles.inputContent}
            editable={!this.props.useShopSetting}
            maxLength={40}
            value={this.state.footerMessage}
            onChangeText={(text) => this.setState({footerMessage: text})}
            multiline
          />
        </View>
        <SettingRow label={I18n.t('settings_receipt.print_point_information')}>
          <OnOffButton
            isOn={this.props.isPrintPointInfo}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_POINT_INFO, isOn)}
          />
        </SettingRow>
        <SettingRow label={I18n.t('settings_receipt.print_zero_price_item')}>
          <OnOffButton
            isOn={this.props.isPrintZeroProducts}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptAccounting(this.props.useShopSetting, SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_ZERO_PRODUCTS, isOn)}
          />
        </SettingRow>
      </ScrollView>
    )
  }
}
