'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import SettingMenuButton from './SettingMenuButton'
import { HomeButtons } from 'modules/home/models'
import PropTypes from 'prop-types'

export default class SettingHomeButton extends Component {
  static propTypes = {
    menus: PropTypes.object,
    onUpdateHomeButton: PropTypes.func
  }

  render () {
    let mainMenu = {
      buttonColor: '#a5a5a5',
      activeButtonColor: '#ff9024',
      textColor: '#fff',
      menus: [
        {
          id: 'accounting',
          selected: this.props.menus.accounting,
          name: I18n.t(HomeButtons.newOrder.title_key)
        },
        {
          id: 'checkSales',
          selected: this.props.menus.checkSales,
          name: I18n.t(HomeButtons.checkSales.title_key)
        },
        {
          id: 'closeSales',
          selected: this.props.menus.closeSales,
          name: I18n.t(HomeButtons.closeSales.title_key)
        }
      ]
    }

    return (
      <View style={{ flex: 1 }}>
        <SettingMenuButton
          menus={mainMenu.menus}
          onMenuClick={(item) => this.props.onUpdateHomeButton(this.props.menus, item)}
          buttonColor={mainMenu.buttonColor}
          textColor={mainMenu.textColor}
          activeButtonColor={mainMenu.activeButtonColor}
        />
      </View>
    )
  }
}
