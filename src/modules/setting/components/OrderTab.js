'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import ConsumptionTaxContainer from '../containers/ConsumptionTaxContainer'
import PaymentMethodContainer from '../containers/PaymentMethodContainer'

export default class OrderTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings_order.consumption')}>
          <ConsumptionTaxContainer />
        </VerticalTabChild>
        <VerticalTabChild tabName={I18n.t('settings_order.payment_method')}>
          <PaymentMethodContainer />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
