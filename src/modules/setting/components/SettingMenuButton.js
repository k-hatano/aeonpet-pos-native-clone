'use strict'

import React, { Component } from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import componentStyles from '../styles/SettingMenuButton'
import SettingGetter from 'modules/setting/models/settingGetter'

class MenuButton extends Component {
  constructor (props) {
    super(props)

    this.state = {
      selected: false
    }
  }

  componentWillMount () {
    const { menu } = this.props
    this.setState({ selected: menu.selected })
  }

  _onMenuClick = id => {
    const { onMenuClick } = this.props

    this.setState({ selected: !this.state.selected })
    onMenuClick(id)
  }

  render () {
    const { menu, buttonColor = '#ff9024', activeButtonColor = '#a5a5a5', textColor = '#000' } = this.props
    const { selected } = this.state
    let backgroundColorStyle = { backgroundColor: selected ? activeButtonColor : buttonColor }
    let textStyle = { color: selected ? '#ccc' : textColor }

    return (
      <TouchableOpacity onPress={() => this._onMenuClick(menu.id)}>
        <View style={[componentStyles.button, backgroundColorStyle]}>
          <Text style={textStyle}>{menu.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

/**
 * USAGE
 *
 *  let mainMenu = {
 *    buttonColor: '#ff9024',
 *    activeButtonColor: '#a5a5a5',
 *    menus: [
 *      {
 *        id: 1,
 *        selected: true,
 *        name:'Stock Search'
 *      }
 *    ]
 *  }
 *
 * <View>
 *  <SettingMenuButton
 *    menus={mainMenu.menus}
 *    onMenuClick={this.onMenuClick}
 *    buttonColor={mainMenu.buttonColor}
 *    activeButtonColor={mainMenu.activeButtonColor}
 *  />
 * </View>
 */
export default class SettingMenuButton extends Component {
  render () {
    const { onMenuClick, buttonColor, activeButtonColor, textColor } = this.props
    let menus
    if (!SettingGetter.cashierId) {
      menus = this.props.menus.filter(menu => menu.isAvailableForInventoryManagement)
    } else {
      menus = this.props.menus
    }

    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.containerMenu}>
          {menus.map((menu, index) =>
            <MenuButton
              key={index}
              menu={menu}
              buttonColor={buttonColor}
              activeButtonColor={activeButtonColor}
              textColor={textColor}
              onMenuClick={onMenuClick}
            />
          )}
        </View>
      </View>
    )
  }
}
