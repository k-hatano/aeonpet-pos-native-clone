'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { VerticalTab, VerticalTabChild } from '../../../common/components/widgets/VerticalTab'
import LogFileView from '../containers/LogFileView'
import LogConfiguration from '../containers/LogConfiguration'

export default class LogTab extends Component {
  render () {
    return (
      <VerticalTab>
        <VerticalTabChild tabName={I18n.t('settings_log.log_files')}>
          <LogFileView />
        </VerticalTabChild>
      </VerticalTab>
    )
  }
}
