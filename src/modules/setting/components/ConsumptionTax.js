'use strict'

import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import I18n from 'i18n-js'
import { RadioGroup } from '../../../common/components/elements/Inputs'
import OnOffButton from '../../../common/components/elements/OnOffButton'
import settingStyles from '../styles/Setting'
import componentStyles from '../styles/ConsumptionTax'
import SettingRow from 'common/components/elements/SettingRow'
import PropTypes from 'prop-types'
import { TAX_RULE } from 'common/models/Tax'

export default class ConsumptionTax extends Component {
  static propTypes = {
    useShopSetting: PropTypes.bool,
    rate: PropTypes.number,
    rule: PropTypes.number,
    calculateRule: PropTypes.number,
    isTaxfree: PropTypes.bool
  }

  taxRules () {
    const taxRules = [
      {
        label: I18n.t('settings_order.excluded') + ': ' + I18n.t('settings_order.ceil'),
        value: TAX_RULE.EXCLUDED_CEIL,
        selected: true
      }
    ]

    return taxRules
  }

  calculateRules () {
    const calculateRules = [
      {
        label: I18n.t('settings_order.subtotal'),
        value: 2,
        selected: true
      }
    ]
    return calculateRules
  }

  render () {
    return (
      <View style={settingStyles.container}>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_order.consumption_tax')}</Text>
        <View style={settingStyles.subContainer}>
          <Text style={settingStyles.subContainerHeader}>{I18n.t('settings_order.tax_ratio')}</Text>
          <TouchableOpacity style={componentStyles.taxRatioButton}>
            <Text>{this.props.rate * 100 + '%'}</Text>
          </TouchableOpacity>
        </View>
        <View style={componentStyles.subContainer}>
          <Text style={componentStyles.subContainerHeader}>{I18n.t('settings_order.tax_rule')}：</Text>
          <View style={componentStyles.marginLeftView}>
            <RadioGroup
              options={this.taxRules()}
              wrap
              buttonStyle={componentStyles.groupButton}
            />
          </View>
        </View>
        <View style={componentStyles.subContainer}>
          <Text style={componentStyles.subContainerHeader}>{I18n.t('settings_order.calculation_rule')}：</Text>
          <View style={componentStyles.marginLeftView}>
            <RadioGroup
              options={this.calculateRules()}
              wrap
              buttonStyle={componentStyles.groupButton}
            />
          </View>
        </View>
      </View>
    )
  }
}
