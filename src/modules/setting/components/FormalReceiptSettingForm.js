'use strict'

import React, { Component } from 'react'
import { TextInput, View, Text, StyleSheet, ScrollView } from 'react-native'
import SettingRow from 'common/components/elements/SettingRow'
import OnOffButton from 'common/components/elements/OnOffButton'
import I18n from 'i18n-js'
import settingStyles from '../styles/Setting'
import { RadioGroup } from 'common/components/elements/Inputs'
import PropTypes from 'prop-types'
import { PRINT_STAFF } from '../models'
import SettingKeys from '../models/SettingKeys'

export default class FormalReceiptSettingForm extends Component {
  static propTypes = {
    useShopSetting: PropTypes.bool,
    isPrintStaff: PropTypes.number,
    isPrintTaxStamp: PropTypes.bool,
    taxOfficeName: PropTypes.string,
    onUpdateReceiptCommon: PropTypes.func,
    onBackupSettings: PropTypes.func
  }

  constructor (props) {
    super(props)
    if (this.props.useShopSetting) {
      this.props.onBackupSettings(this.props.useShopSetting)
    }
  }

  componentWillMount () {
    this.state = {
      taxOfficeName: this.props.taxOfficeName
    }
  }

  componentWillReceiveProps (nextProps) {
    this.state = {
      taxOfficeName: this.props.taxOfficeName
    }
  }

  componentWillUnmount () {
    this.props.onUpdateReceiptCommon(this.props.useShopSetting, SettingKeys.RECEIPT.COMMON.TAX_OFFICE_NAME,
      this.state.taxOfficeName ? this.state.taxOfficeName : '')
  }

  _backUp (isOn) {
    this.props.onUpdateReceiptCommon(this.props.useShopSetting, SettingKeys.RECEIPT.COMMON.TAX_OFFICE_NAME,
      this.state.taxOfficeName ? this.state.taxOfficeName : '')
    this.props.onBackupSettings(isOn)
  }

  printStaffOptions () {
    const printStaffOptions = [
      { label: I18n.t('staff.staff_code'), value: PRINT_STAFF.SELECT_CONTACT_PERSON_CODE },
      { label: I18n.t('staff.staff_name'), value: PRINT_STAFF.PRINT_PERSON_IN_CHARGE_NAME }
    ]

    const selectedOption = this.props.isPrintStaff === PRINT_STAFF.SELECT_CONTACT_PERSON_CODE ?  0 : 1
    printStaffOptions[selectedOption].selected = true
    return printStaffOptions
  }

  render () {
    const booleanTrue = true
    return (
      <ScrollView>
        <Text style={settingStyles.componentHeader}>{I18n.t('settings_receipt.formal_receipt')}</Text>

        <View style={{ marginBottom: 10 }}>
          <Text>{I18n.t('settings_receipt.print_staff')}</Text>
        </View>
        <View style={{ marginBottom: 10 }}>
          <RadioGroup
            options={this.printStaffOptions()}
            wrap
            onSelectItem={item =>
              this.props.onUpdateReceiptCommon(this.props.useShopSetting, SettingKeys.RECEIPT.COMMON.IS_PRINT_STAFF, item.value)
            }
            disable={booleanTrue}
          />
        </View>

        <SettingRow label={I18n.t('settings_receipt.is_print_tax_stamp')}>
          <OnOffButton
            isOn={this.props.isPrintTaxStamp}
            onValueChange={(isOn) =>
              this.props.onUpdateReceiptCommon(this.props.useShopSetting, SettingKeys.RECEIPT.COMMON.IS_PRINT_TAX_STAMP, isOn)}
          />
        </SettingRow>

        <SettingRow label={I18n.t('settings_receipt.tax_office_name')}>
          <TextInput
            placeholder={I18n.t('settings_receipt.tax_office_name')}
            style={formalReceiptStyles.inputContent}
            multiline={booleanTrue}
            editable={!this.props.useShopSetting}
            maxLength={40}
            value={this.state.taxOfficeName}
            onChangeText={(text) => this.setState({taxOfficeName: text})} />
        </SettingRow>
      </ScrollView>
    )
  }
}

const formalReceiptStyles = StyleSheet.create({
  imgContainer: {
    width: 280,
    borderWidth: 1,
    borderColor: '#d7d7d7',
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  imgContent: {
    margin: 5,
    width: 270,
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'flex-start'
  },
  button: {
    marginBottom: 10,
    backgroundColor: '#ddd',
    width: 100,
    height: 30,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5
  },
  inputContent: {
    width: '92%',
    borderWidth: 2,
    borderColor: '#828282',
    paddingLeft: 5,
    height: 60,
    borderRadius: 5
  }
})
