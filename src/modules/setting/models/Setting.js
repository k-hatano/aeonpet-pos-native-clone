/**
 * @deprecated
 */
export default class Setting {
  static get currency () {
    return 'jpy'
  }

  static get language () {
    return 'ja'
  }

  static get deviceId () {
    return '23456789012345678901234567890123'
  }

  static get cachierId () {
    return '12345678901234567890123456789012'
  }

  static get cashierCode () {
    return 'dummy cashier code'
  }

  static get shopName () {
    return 'dummy shop name'
  }

  static get shopId () {
    return 'dummy shop id'
  }

  static get staff_id () {
    return 'dummy staff id'
  }

  static get isPrintTaxStamp () {
    return 1
  }

  static get taxOfficeName () {
    return '名古屋中村屋中村'
  }
}
