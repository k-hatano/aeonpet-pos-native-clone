import SettingKeys from './SettingKeys'
import { TAX_RULE } from '../../../common/models/Tax'

export const SETTING_TYPE = {
  STRING: 1,
  INTEGER: 2,
  BOOLEAN: 3,
  OBJECT: 4
}

export default {
  // Common
  [SettingKeys.COMMON.LOGIN_USER_NAME]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.COMMON.SHOP_ID]: {
    type: SETTING_TYPE.STRING
  },
  // ポイント利用基底数
  [SettingKeys.COMMON.USEABLE_BASE_POINTS]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 1
  },
  [SettingKeys.COMMON.CURRENCY]: {
    type: SETTING_TYPE.STRING,
    defaultValue: 'jpy'
  },
  [SettingKeys.COMMON.DEVICE_ID]: {
    type: SETTING_TYPE.STRING,
    isSensitive: true
  },
  [SettingKeys.COMMON.LAST_LOGGED_IN_AT]: {
    type: SETTING_TYPE.INTEGER
  },
  [SettingKeys.COMMON.DEVICE_TOKEN_TTL]: {
    type: SETTING_TYPE.INTEGER
  },
  [SettingKeys.COMMON.DEVICE_TOKEN]: {
    type: SETTING_TYPE.STRING,
    isSensitive: true
  },
  [SettingKeys.COMMON.BASIC_AUTH_TOKEN]: {
    type: SETTING_TYPE.STRING,
    isSensitive: true
  },
  [SettingKeys.COMMON.CASHIER_ID]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.COMMON.IS_LOGGED_IN]: {
    type: SETTING_TYPE.BOOLEAN
  },
  [SettingKeys.COMMON.LOGIN_REFRESHED_AT]: {
    type: SETTING_TYPE.INTEGER
  },
  [SettingKeys.COMMON.REPOSITORY_CONTEXT]: {
    type: SETTING_TYPE.STRING,
    defaultValue: 'standard'
  },
  [SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0
  },
  [SettingKeys.COMMON.TRAINING_ORDER_NUMBER_SEQUENCE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 9999999
  },
  [SettingKeys.COMMON.LAST_SYNCED_AT]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0
  },
  // Staff
  [SettingKeys.STAFF.IS_FIX_STAFF]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: false
  },
  [SettingKeys.STAFF.CURRENT_STAFF]: {
    type: SETTING_TYPE.OBJECT
  },
  [SettingKeys.STAFF.SYNC_DATE]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },

  // network
  [SettingKeys.NETWORK.ACCESS.SERVER_URL]: {
    type: SETTING_TYPE.STRING,
    defaultValue: 'http://'
  },
  [SettingKeys.NETWORK.ACCESS.SHOP_NAME]: {
    type: SETTING_TYPE.STRING,
    defaultValue: 'テスト店舗'
  },
  [SettingKeys.NETWORK.ACCESS.CASHIER_NAME]: {
    type: SETTING_TYPE.STRING,
    defaultValue: 'テストPOS'
  },
  [SettingKeys.NETWORK.ACCESS.REQUEST_TIMEOUT]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 15
  },
  [SettingKeys.NETWORK.DEVICE.UDID]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.NETWORK.DEVICE.VERSION]: {
    type: SETTING_TYPE.STRING
  },

  // menu
  [SettingKeys.MENU.MAIN_MENU]: {
    type: SETTING_TYPE.OBJECT,
    defaultValue: JSON.stringify({
      accounting: true,
      checkSales: true,
      closeSales: true
    })
  },
  [SettingKeys.MENU.SUB_MENU]: {
    type: SETTING_TYPE.OBJECT,
    defaultValue: JSON.stringify({
      orderSearch: true,
      customerSearch: true,
      stockSearch1: true,
      return: true,
      depositsAndWithdrawals: true,
      stockAdjustment1: false,
      stockOut1: false,
      stockIn1: false,
      stockTaking: false,
      closeSalesSummary: false,
      customerRegister: false
    })
  },

  // openSale
  [SettingKeys.OPENSALE.IS_OPEN_SALE]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: false
  },

  // order
  [SettingKeys.ORDER.TAX.USE_SHOP_SETTING]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.ORDER.TAX.RATE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0.08
  },
  [SettingKeys.ORDER.TAX.RULE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: TAX_RULE.EXCLUDED_FLOOR
  },
  [SettingKeys.ORDER.TAX.CALCULATE_RULE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 1
  },
  [SettingKeys.ORDER.TAX.IS_TAXFREE]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.ORDER.PAYMENT_SERVICE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0
  },

  // Receipt
  [SettingKeys.RECEIPT.ACCOUNTING.USE_SHOP_SETTING]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_HEADER_LOGO]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.ACCOUNTING.HEADER_LOGO_FILE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_FOOTER_LOGO]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.ACCOUNTING.FOOTER_LOGO_FILE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_ZERO_PRODUCTS]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_POINT_INFO]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.ACCOUNTING.PRINT_PRODUCT_CODE_MODE]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 1
  },
  [SettingKeys.RECEIPT.BILL.USE_SHOP_SETTING]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.BILL.IS_PRINT_HEADER_LOGO]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.BILL.HEADER_LOGO_FILE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.BILL.HEADER_MESSAGE]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.BILL.PROVISO]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.COMMON.USE_SHOP_SETTING]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.COMMON.IS_PRINT_STAFF]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 1
  },
  [SettingKeys.RECEIPT.COMMON.IS_PRINT_TAX_STAMP]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.COMMON.TAX_OFFICE_NAME]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.RECEIPT.CASHIER_TOTAL.USE_SHOP_SETTING]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.CASHIER_TOTAL.PRINT_NUM]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 2
  },
  [SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_PRODUCT_SEGMENT_TOTAL]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },
  [SettingKeys.RECEIPT.CASHIER_TOTAL.IS_PRINT_STAFF_TOTAL]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },

  // log
  [SettingKeys.LOG.LEVEL]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 2
  },
  [SettingKeys.LOG.ROTATION_DAYS]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 5
  },
  [SettingKeys.LOG.FILE_PATHS]: {
    type: SETTING_TYPE.OBJECT,
    defaultValue: '[]'
  },
  [SettingKeys.LOG.REACT_NATIVE_ERROR]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: false
  },

  // equipment
  [SettingKeys.EQUIPMENT.PRINTER.MAKER]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0
  },
  [SettingKeys.EQUIPMENT.PRINTER.ADDRESS]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.PRINTER.UUID]: {
    type: SETTING_TYPE.STRING
  },
  [SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER]: {
    type: SETTING_TYPE.INTEGER,
    defaultValue: 0
  },
  [SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.ADDRESS]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.TOP_MESSAGE]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.BOTTOM_MESSAGE]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.MAKER]: {
    type: SETTING_TYPE.STRING,
    defaultValue: '未選択'
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ADDRESS]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.PORT]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ACCOUNT_ID]: {
    type: SETTING_TYPE.STRING,
    defaultValue: ''
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ACCOUNT_PASSWORD]: {
    type: SETTING_TYPE.STRING,
    defaultValue: '',
    isSensitive: true
  },
  [SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.MERCHANT_PASSWORD]: {
    type: SETTING_TYPE.STRING,
    defaultValue: '',
    isSensitive: true
  },
  [SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER]: {
    type: SETTING_TYPE.BOOLEAN,
    defaultValue: true
  },

  // backup
  [SettingKeys.BACKUP.ACCOUNTING]: {
    type: SETTING_TYPE.OBJECT
  },
  [SettingKeys.BACKUP.BILL]: {
    type: SETTING_TYPE.OBJECT
  },
  [SettingKeys.BACKUP.COMMON]: {
    type: SETTING_TYPE.OBJECT
  },
  [SettingKeys.BACKUP.CASHIER_TOTAL]: {
    type: SETTING_TYPE.OBJECT
  }
}
