import { AsyncStorage } from 'react-native'
import SettingDefinitions, { SETTING_TYPE } from './SettingDefinitions'
import SettingKeys from './SettingKeys'

export async function getLocalReceiptParameter (receiptType) {
  let values = {}
  for (const i in SettingKeys.RECEIPT[receiptType]) {
    const key = SettingKeys.RECEIPT[receiptType][i]
    const type = SettingDefinitions[key].type
    const value = await SettingStorage.load(key, type)
    values[key] = value
  }
  return values
}

export default class SettingStorage {
  static async save (key, value, type) {
    switch (type) {
      case SETTING_TYPE.STRING:
        return AsyncStorage.setItem(key, value)
      case SETTING_TYPE.INTEGER:
        return AsyncStorage.setItem(key, String(value))
      case SETTING_TYPE.BOOLEAN:
        return AsyncStorage.setItem(key, JSON.stringify(!!value))
      case SETTING_TYPE.OBJECT:
        return AsyncStorage.setItem(key, JSON.stringify(value))
      default:
        console.error('Invalid data type')
        return undefined
    }
  }

  static async load (key, type) {
    let value = await AsyncStorage.getItem(key)
    if (value === null && SettingDefinitions[key].type === SETTING_TYPE.BOOLEAN) {
      value = SettingDefinitions[key].defaultValue
    } else if (!value && SettingDefinitions[key].defaultValue) {
      value = SettingDefinitions[key].defaultValue
    }
    switch (type) {
      case SETTING_TYPE.STRING:
        return value
      case SETTING_TYPE.INTEGER:
        return value === undefined ? undefined : Number(value)
      case SETTING_TYPE.BOOLEAN:
        return value === undefined ? undefined : JSON.parse(value)
      case SETTING_TYPE.OBJECT:
        return value === undefined ? undefined : JSON.parse(value)
      default:
        console.error('Invalid data type')
        return undefined
    }
  }
}
