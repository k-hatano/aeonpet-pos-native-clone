export default {
  COMMON: {
    LOGIN_USER_NAME: 'common.login_user_name',
    SHOP_ID: 'common.shop_id',
    USEABLE_BASE_POINTS: 'common.useable_base_points',
    CURRENCY: 'common.currency',
    DEVICE_ID: 'common.device_id',
    DEVICE_TOKEN: 'common.device_token',
    BASIC_AUTH_TOKEN: 'common.basic_auth_token',
    LAST_LOGGED_IN_AT: 'common.last_logged_in_at',
    DEVICE_TOKEN_TTL: 'common.device_token_ttl',
    CASHIER_ID: 'common.cashier_id',
    IS_LOGGED_IN: 'common.is_logged_in',
    LOGIN_REFRESHED_AT: 'commont.login_refreshed_at',
    REPOSITORY_CONTEXT: 'common.repository_context',
    ORDER_NUMBER_SEQUENCE: 'common.order_number_sequence',
    TRAINING_ORDER_NUMBER_SEQUENCE: 'common.training_order_number_sequence',
    LAST_SYNCED_AT: 'common.last_synced_at'
  },
  NETWORK: {
    ACCESS: {
      SERVER_URL: 'access.server_url',
      SHOP_NAME: 'access.shop_name',
      CASHIER_NAME: 'access.cashier_name',
      REQUEST_TIMEOUT: 'access.request_timeout'
    },
    DEVICE: {
      UDID: 'device.udid',
      VERSION: 'device.version'
    }
  },
  STAFF: {
    IS_FIX_STAFF: 'staff.is_fix_staff',
    CURRENT_STAFF: 'staff.current_staff',
    SYNC_DATE: 'staff.sync_date'
  },
  MENU: {
    MAIN_MENU: 'menu.main_menu',
    SUB_MENU: 'menu.sub_menu'
  },
  OPENSALE: {
    IS_OPEN_SALE: 'open_sale.is_open_sale'
  },
  ORDER: {
    TAX: {
      USE_SHOP_SETTING: 'tax.use_shop_setting',
      RATE: 'tax.rate',
      RULE: 'tax.rule',
      CALCULATE_RULE: 'tax.calculate_rule',
      IS_TAXFREE: 'tax.is_taxfree'
    },
    PAYMENT_SERVICE: 'order.payment_service'
  },
  RECEIPT: {
    ACCOUNTING: {
      USE_SHOP_SETTING: 'accounting.use_shop_setting',
      IS_PRINT_HEADER_LOGO: 'accounting.is_print_header_logo',
      HEADER_LOGO_FILE: 'accounting.header_logo_file',
      HEADER_MESSAGE: 'accounting.header_message',
      IS_PRINT_FOOTER_LOGO: 'accounting.is_print_footer_logo',
      FOOTER_LOGO_FILE: 'accounting.footer_logo_file',
      FOOTER_MESSAGE: 'accounting.footer_message',
      IS_PRINT_POINT_INFO: 'accounting.is_print_point_info',
      IS_PRINT_ZERO_PRODUCTS: 'accounting.is_print_zero_products',
      PRINT_PRODUCT_CODE_MODE: 'accounting.print_product_code_mode'
    },
    BILL: {
      USE_SHOP_SETTING: 'bill.use_shop_setting',
      IS_PRINT_HEADER_LOGO: 'bill.is_print_header_logo',
      HEADER_LOGO_FILE: 'bill.header_logo_file',
      HEADER_MESSAGE: 'bill.header_message',
      PROVISO: 'bill.proviso'
    },
    COMMON: {
      USE_SHOP_SETTING: 'common.use_shop_setting',
      IS_PRINT_STAFF: 'common.is_print_staff',
      IS_PRINT_TAX_STAMP: 'common.is_print_tax_stamp',
      TAX_OFFICE_NAME: 'common.tax_office_name'
    },
    CASHIER_TOTAL: {
      USE_SHOP_SETTING: 'cashier_total.use_shop_setting',
      PRINT_NUM: 'cashier_total.print_num',
      IS_PRINT_PRODUCT_SEGMENT_TOTAL: 'cashier_total.is_print_product_segment_total',
      IS_PRINT_STAFF_TOTAL: 'cashier_total.is_print_staff_total'
    }
  },
  LOG: {
    LEVEL: 'log.level',
    ROTATION_DAYS: 'log.rotation_days',
    REACT_NATIVE_ERROR: 'log.react_native_error',
    FILE_PATHS: 'log.file_pathes'
  },
  EQUIPMENT: {
    PRINTER: {
      MAKER: 'printer.maker',
      ADDRESS: 'printer.address',
      UUID: 'printer.uuid'
    },
    CUSTOMER_DISPLAY: {
      MAKER: 'customer_display.maker',
      ADDRESS: 'customer_display.address',
      TOP_MESSAGE: 'customer_display.top_message',
      BOTTOM_MESSAGE: 'customer_display.bottom_message'
    },
    PAYMENT_TERMINAL: {
      MAKER: 'payment_terminal.maker',
      ADDRESS: 'payment_terminal.address',
      PORT: 'payment_terminal.port',
      ACCOUNT_ID: 'payment_terminal.account_id',
      ACCOUNT_PASSWORD: 'payment_terminal.account_password',
      MERCHANT_PASSWORD: 'payment_terminal.merchant_password'
    },
    CASH_CHANGER: {
      IS_SELECTED_CASH_CHANGER: 'cash_changer.is_selected_cash_changer'
    }
  },
  BACKUP: {
    ACCOUNTING: 'backup.accounting',
    BILL: 'backup.bill',
    COMMON: 'backup.common',
    CASHIER_TOTAL: 'backup.cashier_total'
  }
}
