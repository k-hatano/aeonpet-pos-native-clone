import StoreAccessibleBase from '../../../common/models/StoreAccessibleBase'
import SettingKeys from './SettingKeys'

class SettingGetter extends StoreAccessibleBase {
  get shopId () {
    return this.getSetting(SettingKeys.COMMON.SHOP_ID)
  }
  get cashierId () {
    return this.getSetting(SettingKeys.COMMON.CASHIER_ID)
  }
  get orderNumberSequence () {
    return this.getSetting(SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE)
  }
  get currentStaff () {
    return this.getSetting(SettingKeys.STAFF.CURRENT_STAFF)
  }
  get isOpenSale () {
    return this.getSetting(SettingKeys.OPENSALE.IS_OPEN_SALE)
  }
  get paymentTerminal () {
    return this.getSetting(SettingKeys.ORDER.PAYMENT_SERVICE)
  }
  get paymentTerminalAddress () {
    return this.getSetting(SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ADDRESS)
  }
  get paymentTerminalPort () {
    const port = this.getSetting(SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.PORT)
    return parseInt(port)
  }
}

export default new SettingGetter()
