import SampleCashierSettingRepository from './sample/SampleCashierSettingRepository'
import StandardCashierSettingRepository from './standard/StandardCashierSettingRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierSettingRepository {
  static _implement = new SampleCashierSettingRepository()

  /**
   *
   * @return {Promise.<CashierSettingEntity>}
   */
  static async find () {
    return this._implement.find()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardCashierSettingRepository()
        break

      default:
        this._implement = new SampleCashierSettingRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierSettingRepository', (context) => {
  CashierSettingRepository.switchImplement(context)
})
