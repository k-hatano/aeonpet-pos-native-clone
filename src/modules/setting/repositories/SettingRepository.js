import SampleSettingRepository from './sample/SampleSettingRepository'
import StandardSettingRepository from './standard/StandardSettingRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class SettingRepository {
  static _implement = new SampleSettingRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardSettingRepository()
        break
      default:
        this._implement = new SampleSettingRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('SettingRepository', (context) => {
  SettingRepository.switchImplement(context)
})
