const settings = {
  id: 1,
  is_print_receipt_header_logo: true,
  receipt_header_logo_image: 'assets/images/header_logo.png',
  receipt_header_message: 'ヘッダーメッセージ',
  is_print_zero_products: false,
  print_product_code_mode: 2,
  is_print_point_info: true,
  is_print_tax_stamp: true,
  tax_office_name: 'sample税務署',
  is_print_staff: 1,
  is_print_receipt_footer_logo: true,
  receipt_footer_logo_image: 'assets/images/footer_logo.png',
  receipt_footer_message: 'フッターメッセージ',
  is_print_bill_logo: true,
  bill_logo_image: 'assets/images/bill_logo.png',
  bill_message: '領収証メッセージ',
  bill_proviso: '但し書き',
  total_receipt_print_num: 2,
  is_print_product_segment_total: true,
  is_print_staff_total: true,
  is_taxfree: true
}

export default class SampleSettingRepository {
  _settings = []
  async findAll () {
    return settings
  }
}
