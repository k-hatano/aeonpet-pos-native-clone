export default class SampleCashierSettingRepository {
  async find () {
    return {
      tax_office: '散布留税務署',
      tax_payment_place: 'サンプル納税地',
      company_name: '免税会社名'
    }
  }
}
