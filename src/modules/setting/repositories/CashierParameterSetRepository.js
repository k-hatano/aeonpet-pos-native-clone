import SampleCashierParameterSetRepository from './sample/SampleCashierParameterSetRepository'
import StandardCashierParameterSetRepository from './standard/StandardCashierParameterSetRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierParameterSetRepository {
  static _implement = new SampleCashierParameterSetRepository()

  // static async findAll () {
  //   return this._implement.findAll()
  // }

  static async createSamples () {
    return this._implement.createSamples()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashierParameterSetRepository()
        break

      default:
        this._implement = new SampleCashierParameterSetRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierParameterSetRepository', (context) => {
  CashierParameterSetRepository.switchImplement(context)
})
