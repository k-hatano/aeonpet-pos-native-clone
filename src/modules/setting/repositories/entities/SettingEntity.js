import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const CashierParameterSets = sequelize.define(
  'CashierParameterSets',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    is_print_receipt_header_logo: Sequelize.INTEGER,
    receipt_header_logo_image: Sequelize.STRING,
    receipt_header_message: Sequelize.TEXT,
    is_print_zero_products: Sequelize.INTEGER,
    print_product_code_mode: Sequelize.INTEGER,
    is_print_point_info: Sequelize.INTEGER,
    is_print_tax_stamp: Sequelize.INTEGER,
    tax_office_name: Sequelize.STRING,
    is_print_staff: Sequelize.INTEGER,
    is_print_receipt_footer_logo: Sequelize.INTEGER,
    receipt_footer_logo_image: Sequelize.STRING,
    receipt_footer_message: Sequelize.TEXT,
    is_print_bill_logo: Sequelize.INTEGER,
    bill_logo_image: Sequelize.STRING,
    bill_message: Sequelize.TEXT,
    bill_proviso: Sequelize.STRING,
    total_receipt_print_num: Sequelize.INTEGER,
    is_print_product_segment_total: Sequelize.INTEGER,
    is_print_staff_total: Sequelize.INTEGER,
    is_taxfree: Sequelize.INTEGER
  }, {
    tableName: 'cashier_parameter_sets',
    underscored: true,
    timestamps: false
  }
)

export default CashierParameterSets
