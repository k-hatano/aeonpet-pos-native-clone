import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

/**
 * @typedef {Object} CashierParameterSetEntity
 * @property {string} id
 * @property {string} is_print_receipt_header_logo
 * @property {string} receipt_header_logo_image
 * @property {string} receipt_header_message
 * @property {integer} is_print_zero_products
 * @property {integer} print_product_code_mode
 * @property {integer} is_print_point_info
 * @property {integer} is_print_tax_stamp
 * @property {string} tax_office_name
 * @property {integer} is_print_staff
 * @property {integer} is_print_receipt_footer_logo
 * @property {string} receipt_footer_logo_image
 * @property {string} receipt_footer_message
 * @property {integer} is_print_bill_logo
 * @property {string} bill_logo_image
 * @property {string} bill_message
 * @property {string} bill_proviso
 * @property {integer} total_receipt_print_num
 * @property {integer} is_print_product_segment_total
 * @property {integer} is_print_staff_total
 * @property {integer} is_taxfree
 // * @property {integer} created_at
 // * @property {integer} updated_at
 */

const CashierParameterSet = sequelize.define(
  'CashierParameterSet',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    is_print_receipt_header_logo: Sequelize.INTEGER,
    receipt_header_logo_image: Sequelize.STRING,
    receipt_header_message: Sequelize.TEXT,
    is_print_zero_products: Sequelize.INTEGER,
    print_product_code_mode: Sequelize.INTEGER,
    is_print_point_info: Sequelize.INTEGER,
    is_print_tax_stamp: Sequelize.INTEGER,
    tax_office_name: Sequelize.STRING,
    is_print_staff: Sequelize.INTEGER,
    is_print_receipt_footer_logo: Sequelize.INTEGER,
    receipt_footer_logo_image: Sequelize.STRING,
    receipt_footer_message: Sequelize.TEXT,
    is_print_bill_logo: Sequelize.INTEGER,
    bill_message: Sequelize.TEXT,
    bill_logo_image: Sequelize.STRING,
    bill_proviso: Sequelize.STRING,
    total_receipt_print_num: Sequelize.INTEGER,
    is_print_product_segment_total: Sequelize.INTEGER,
    is_print_staff_total: Sequelize.INTEGER,
    is_taxfree: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'cashier_parameter_sets',
    underscored: true,
    timestamps: false
  }
)

export default CashierParameterSet
