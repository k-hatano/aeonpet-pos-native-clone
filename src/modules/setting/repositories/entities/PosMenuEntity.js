import Sequelize from 'sequelize'
import { sequelize } from 'common/BaseEntity'
import AppEvents from 'common/AppEvents'

const PosMenu = sequelize.define(
  'PosMenu',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    menu_type: Sequelize.INTEGER,
    menu_name: Sequelize.STRING,
    display_menu: Sequelize.INTEGER,
    sort_order: Sequelize.INTEGER
  }, {
    tableName: 'pos_menu_items',
    underscored: true,
    timestamps: false
  }
)

export default PosMenu
