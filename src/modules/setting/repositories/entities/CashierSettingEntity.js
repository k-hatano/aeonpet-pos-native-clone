import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'

/**
 * @typedef {Object} CashierSettingEntity
 * @property {string} tax_office
 * @property {string} tax_payment_place
 * @property {string} company_name
 */

const CashierSetting = sequelize.define(
  'CashierSetting',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    tax_office: Sequelize.STRING,
    tax_payment_place: Sequelize.STRING,
    company_name: Sequelize.STRING
  }, {
    tableName: 'cashier_settings',
    underscored: true,
    timestamps: false
  }
)

export default CashierSetting
