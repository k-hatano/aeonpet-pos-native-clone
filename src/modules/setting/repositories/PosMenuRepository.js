import SamplePosMenuRepository from './sample/SampleSettingRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class PosMenuRepository {
  static _implement = new SamplePosMenuRepository()

  static async findByMenuType (menuType) {
    return this._implement.findByMenuType(menuType)
  }

  static switchImplement (context) {
    switch (context) {
      /*
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardSettingRepository()
        break
*/
      default:
        this._implement = new SamplePosMenuRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('PosMenuRepository', (context) => {
  PosMenuRepository.switchImplement(context)
})
