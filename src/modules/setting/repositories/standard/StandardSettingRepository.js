import AppEvents from 'common/AppEvents'
import CashierParameterSets from '../entities/SettingEntity'
import ShopRepository from '../../../shop/repositories/ShopRepository'

export default class StandardSettingRepository {
  async findAll () {
    const shop = await ShopRepository.find()
    const cashierParameterSet = await CashierParameterSets.findOne({ raw: true })
    return Object.assign({}, cashierParameterSet, shop)
  }
}
