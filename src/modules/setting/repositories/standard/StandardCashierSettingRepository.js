import AppEvents from 'common/AppEvents'
import CashierSettingEntity from '../entities/CashierSettingEntity'

export default class StandardCashierSettingRepository {
  async find () {
    const settings = await CashierSettingEntity.findAll({ raw: true })
    return settings ? settings[0] : null
  }

  async createSamples () {
    if (await CashierSettingEntity.count() === 0) {
      // await CashierSettingEntity.bulkCreate(sampleCashierSetting)
    }
  }
}

AppEvents.onSampleDataCreate('StandardCashierSettingRepository', async () => {
  return (new StandardCashierSettingRepository()).createSamples()
})
