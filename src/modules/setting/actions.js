import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const updateSetting = createAction(`${MODULE_NAME}_updateSettings`)
export const setSettings = createAction(`${MODULE_NAME}_setSettings`)
export const setPaymentMethods = createAction(`${MODULE_NAME}_setPaymentMethods`)
export const setCurrentLog = createAction(`${MODULE_NAME}_setCurrentLog`)
