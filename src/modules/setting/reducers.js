import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  settings: {},
  paymentMethods: null,
  currentLog: ''
}

const handlers = {
  [actions.updateSetting]: (state, { payload: { key, value } }) => ({
    ...state,
    settings: { ...state.settings, [key]: value }
  }),
  [actions.setSettings]: (state, { payload }) => ({
    ...state,
    settings: payload
  }),
  [actions.setPaymentMethods]: (state, action) => ({
    ...state,
    paymentMethods: action.payload
  }),
  [actions.setCurrentLog]: (state, action) => ({
    ...state,
    currentLog: action.payload
  })
}

export default handleActions(handlers, defaultState)
