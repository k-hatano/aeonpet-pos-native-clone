import I18n from 'i18n-js'
import moment from 'moment'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import SettingKeys from '../../setting/models/SettingKeys'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import ShopRepository from '../../shop/repositories/ShopRepository'

/**
 * @typedef {Object} ReceiptElementBase
 * @property {string} element
 * @property {Array.<string>} tags
 */

/**
 * @typedef {ReceiptElementBase} ReceiptTextElement
 * @property {string} text
 * @property {string|undefined} align
 * @property {integer} width
 * @property {integer} height
 */

/**
 * @typedef {ReceiptElementBase} ReceiptCombineElement
 * @property {string} left
 * @property {string} right
 * @property {integer} width
 * @property {integer} height
 */

export default class ReceiptBuilder extends StoreAccessibleBase {
  _cashier = null
  _shop = null

  async initializeAsync () {
    this._cashier = await CashierRepository.find()
    this._shop = await ShopRepository.find()
  }

  get _staffName () {
    return this.getSetting(SettingKeys.STAFF.CURRENT_STAFF).name
  }

  get _cashierName () {
    return this._cashier ? this._cashier.name : '初期化エラー'
  }

  get _shopName () {
    return this._shop ? this._shop.name : '初期化エラー'
  }

  get _issureCode () {
    return this._shop.issuer_code ? this._shop.issuer_code : null
  }

  get _isPrintStaff () {
    return this.getSetting(SettingKeys.RECEIPT.COMMON.IS_PRINT_STAFF)
  }

  get _taxOfficeName () {
    const maxLength = 4
    let name = this.getSetting(SettingKeys.RECEIPT.COMMON.TAX_OFFICE_NAME) || ''
    const length = this.lengthByShiftJisByte(name)
    return name + ' '.repeat(Math.max(0, maxLength - length))
  }

  get _isPrintTaxStamp () {
    return this.getSetting(SettingKeys.RECEIPT.COMMON.IS_PRINT_TAX_STAMP)
  }

  staffElement (staffName, align = 'right') {
    if (align === 'left') {
      return this.combineLeftRightElement(
        I18n.t('receipt.staff') + ':' + (staffName || this._staffName || ''),
        '',
        ['staff']
      )
    } else {
      return this.combineLeftRightElement(
        I18n.t('receipt.staff') + ':',
        (staffName || this._staffName || ''),
        ['staff']
      )
    }
  }

  titleElement (title) {
    return {
      element: 'text',
      align: 'center',
      tags: ['title'],
      text: title
    }
  }

  /**
   *
   * @deprecated shopNameElementsを利用するように
   * @param shopName
   * @returns {{element: string, text: (*|string), align: string, tags: [string]}}
   */
  shopNameElement (shopName) {
    return {
      element: 'text',
      text: shopName || this._shopName || '',
      align: 'left',
      tags: ['shop']
    }
  }

  shopNameElements () {
    return [
      this.leftTextElement(this._shopName || '')
    ]
  }

  cashierNameElement (cashierName) {
    return {
      element: 'text',
      text: cashierName || this._cashierName || '',
      align: 'left',
      tags: ['cashier']
    }
  }

  combineLeftRightElement (left, right, tags) {
    return this.addTagsToElement({
      element: 'combine_left_right',
      left,
      right
    }, tags)
  }

  multiLineTextElements (text, align, tags = undefined) {
    const texts = (text || '').split('\n')
    return texts.map(text => {
      const element = { element: 'text', text, align }
      if (tags) { element.tags = tags }
      return element
    })
  }

  feedElement (lines = 1) {
    return {
      element: 'feed',
      lines
    }
  }

  leftTextElement (text, tags = undefined, options = {}) {
    return this.addTagsToElement({
      element: 'text',
      align: 'left',
      text,
      ...options
    }, tags)
  }

  /**
   *
   * @param text
   * @param tags
   * @returns {ReceiptTextElement}
   */
  centerTextElement (text, tags = undefined, options = {}) {
    return this.addTagsToElement({
      element: 'text',
      align: 'center',
      text,
      ...options
    }, tags)
  }

  rightTextElement (text, tags = undefined, options = {}) {
    return this.addTagsToElement({
      element: 'text',
      align: 'right',
      text,
      ...options
    }, tags)
  }

  barcodeElement (data) {
    return {
      element: 'barcode',
      data,
      height: 80
    }
  }

  addTagsToElement (element, tags) {
    if (tags) {
      return { ...element, tags }
    } else {
      return element
    }
  }

  textLineElement () {
    return {
      element: 'text',
      text: '----------------------',
      align: 'center',
      tags: ['line']
    }
  }

  cutElement () {
    return {
      element: 'cut'
    }
  }

  formatMoney (amount, currency) {
    amount = parseFloat(amount)
    let sign = ''
    if (amount < 0) {
      amount = -amount
      sign = '-'
    }
    return sign + I18n.t('receipt.format_amount.' + currency, { amount: amount.toLocaleString('ja') })
  }

  formatCount (count) {
    return I18n.t('receipt.format_count', { count })
  }

  formatDateTime (date) {
    return moment(date).format('YYYY-MM-DD HH:mm:ss')
  }

  formatDate (date) {
    return moment(date).format('YYYY-MM-DD')
  }

  formatPoint (point) {
    return this.formatNumber(point) + 'pt'
  }

  formatNumber (number) {
    return parseFloat(number).toLocaleString('ja')
  }

  findFirstElementByTag (receipt, tag) {
    for (let index in receipt.content) {
      const element = receipt.content[index]
      if (element.tag === tag) {
        return { index, element }
      }
    }
    return null
  }

  makeSpace (spaceLength) {
    return ' '.repeat(spaceLength)
  }

  lengthByShiftJisByte (text) {
    let length = 0
    for (let count = 0; count < text.length; count++) {
      let char = text.charCodeAt(count)
      if ((char >= 0x0 && char < 0x81) || (char === 0xa0) || (char >= 0xa1 && char < 0xdf) || (char >= 0xfd && char < 0xff)) {
        length += 1
      } else {
        length += 2
      }
    }
    return length
  }
}
