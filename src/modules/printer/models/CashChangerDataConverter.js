import CashChangerStatusEntity from 'modules/printer/repositories/entities/CashChangerStatusEntity.js'

/**
 * 固定長形式のデータ文字列をオブジェクトに変換させる
 */
class CashChangerDataConverter {

  /**
   * 応答電文をオブジェクトにマッピングする
   * @param {String} _dataString 
   * @return {CashChangerStatusEntity}
   */
  convertResponse (_dataString) {
    const resultEntity = this._stringToObject(_dataString, new CashChangerStatusEntity())
    return resultEntity
  }

  /**
   * 固定長形式 → Entity
   * @param {string} _dataString 
   * @param {*} _targetEntity 
   */
  _stringToObject (_dataString, _targetEntity) {
    const keys = Object.keys(_targetEntity)
    let index = 0
    keys.forEach(key => {
      if (_targetEntity[key] && _targetEntity[key].byte) {
        _targetEntity[key].value = _dataString.substr(index, _targetEntity[key].byte)
        index += _targetEntity[key].byte
      }
    })
    return _targetEntity
  }
}

export default new CashChangerDataConverter()
