import { Printer, _PRINTER_SDK, _PRINTER_STD } from './Printer'

// FIX ME: This method is sample for printing receipt
export const printOrder = (successCompletion, errCompletion) => {
  // sample
  const printerProperty = {
    PRINTER_SDK: _PRINTER_SDK.EPSON,
    PRINTER_STD: _PRINTER_STD.IP,
    PRINTER_IP: '192.168.50.160',
    RECEIPT_LINE_LENGTH: 48
  }
  /*
  var printerProperty = {
    PRINTER_SDK: _PRINTER_SDK.EPSON,
    PRINTER_STD: _PRINTER_STD.BLUETOOTH,
    PRINTER_IP: ''
  }
  */
  try {
    const printer = new Printer()
    printer.setPrinterProperty(printerProperty)

    const json = {
      'content': [
        {
          'element': 'text',
          'text': "I'm Sample Receipt",
          'align': 'center',
          'width': '2',
          'height': '2',
          'bold': true
        },
        {
          'element': 'feed',
          'lines': '1'
        },
        {
          'element': 'image'
          // "path": ""
        },
        {
          'element': 'text',
          'text': '2017-05-24 13:12:12',
          'align': 'left',
          'underline': true
        },
        {
          'element': 'feed',
          'lines': '1'
        },
        {
          'element': 'combine_left_right',
          'left': '伝票番号:',
          'right': '81-XXXXXXXXXXXXX'
        },
        {
          'element': 'feed',
          'lines': '1'
        },
        {
          'element': 'text',
          'text': 'エスキュービズム・雪見亭　アラスカ支店',
          'align': 'right'
        },
        {
          'element': 'feed',
          'lines': '1'
        },
        {
          'element': 'text',
          'text': 'かき氷　メロン味',
          'align': 'left'
        },
        {
          'element': 'combine_left_right',
          'left': '    ￥1000 × 1 ',
          'right': '￥1000'
        },
        {
          'element': 'feed',
          'lines': '1'
        },
        {
          'element': 'text',
          'text': ' 小計        1点',
          'align': 'right'
        },
        {
          'element': 'text',
          'text': '----------------------',
          'align': 'center'
        },
        {
          'element': 'text',
          'text': '合計   ￥1000',
          'align': 'right',
          'width': '2'
        },
        {
          'element': 'feed',
          'lines': '2'
        },
        {
          'element': 'barcode',
          'data': '9-81-000000000001'
        },
        {
          'element': 'cut'
        }
      ]
    }

    printer.printReceipt(JSON.stringify(json), successCompletion)
  } catch (error) {
    errCompletion(error)
  }
}
