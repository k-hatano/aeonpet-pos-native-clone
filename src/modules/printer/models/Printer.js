import { NativeModules } from 'react-native'
import { isSuccess, errorMessage, isPaperNearEnd } from './PrinterResult'
import { PRINTER_MAKERS as PRINTER_SDK } from '../../setting/models'

export const _PRINTER_STD = {
  IP: 0,
  BLUETOOTH: 1
}

export const interfaceToSDK = (interfaceValue) => {
  switch (interfaceValue) {
    case 1:
      return PRINTER_SDK.EPSON
    default:
      return null
  }
}

export const PRINTER_STD = 'PRINTER_STD'
export const PRINTER_IP = 'PRINTER_IP'
export const PRINTER_TIMEOUT = 'PRINTER_TIMEOUT'
export const PRINTER_RESULT = 'PRINTER_RESULT'
export const RECEIPT_LINE_LENGTH = 'RECEIPT_LINE_LENGTH'

export default class Printer {
  constructor () {
    this.printerBridge = NativeModules.PrinterBridge
  }

  // brdge interface
  setPrinterProperty (printerProperty) {
    this.printerBridge.setPrinterProperty(printerProperty)
  }

  printReceipt (receiptJsonString, openDrawer, successCompletion, onError) {
    this.printerBridge.printReceipt(
      receiptJsonString,
      openDrawer,
      (bridgeError, result) => {
        const errorMessage = this._getErrorMessage(bridgeError, result[PRINTER_RESULT])
        if (errorMessage) {
          onError(errorMessage)
        }
        if (isSuccess(result[PRINTER_RESULT])) {
          successCompletion(result[PRINTER_RESULT])
        }
      }
    )
  }

  disconnectCasher (successCompletion, onError) {
    this.printerBridge.disconnectCasher(
      (bridgeError, result) => {
        const errorMessage = this._getErrorMessage(bridgeError, result[PRINTER_RESULT])
        if (errorMessage) {
          onError(errorMessage)
        }
        if (isSuccess(result[PRINTER_RESULT])) {
          successCompletion()
        }
      }
    )
  }

  connectCasher (successCompletion, onError) {
    this.printerBridge.connectCasher(
      (bridgeError, result) => {
        const errorMessage = this._getErrorMessage(bridgeError, result[PRINTER_RESULT])
        if (errorMessage) {
          onError(errorMessage)
        }
        if (isSuccess(result[PRINTER_RESULT])) {
          successCompletion()
        }
      }
    )
  }

  readCashCount (successCompletion, onError) {
    this.printerBridge.readCashCount(
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  depositCasher (successCompletion, onError) {
    this.printerBridge.depositCasher(
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  pauseDepositCasher (successCompletion, onError) {
    this.printerBridge.pauseDepositCasher(
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  restartDepositCasher (successCompletion, onError) {
    this.printerBridge.restartDepositCasher(
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  endDepositCasher (type, successCompletion, onError) {
    this.printerBridge.endDepositCasher(
      type,
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  dispenseCasher (coinCountInfo, successCompletion, onError) {
    this.printerBridge.dispenseCasher(JSON.stringify(coinCountInfo),
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  dispenseChange (changeValue, successCompletion, onError) {
    this.printerBridge.dispenseChange(
      changeValue,
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  readCasherStatus (successCompletion, onError) {
    this.printerBridge.readCasherStatus(
      (bridgeError, result) => {
        this._onComplete(bridgeError, result, successCompletion, onError)
      }
    )
  }

  _getErrorMessage (bridgeError, result) {
    if (bridgeError) {
      return Error(bridgeError.toSource())
    } else if (!isSuccess(result)) {
      return errorMessage(result)
    }

    return null
  }

  _onComplete = (bridgeError, result, successCompletion, onError) => {
    const errorMessage = this._getErrorMessage(bridgeError, result[PRINTER_RESULT])
    if (errorMessage) {
      onError(errorMessage)
    }
    if (isSuccess(result[PRINTER_RESULT])) {
      successCompletion()
    }
  }
}
