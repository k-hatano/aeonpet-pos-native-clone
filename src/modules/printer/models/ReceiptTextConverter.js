﻿import * as _ from 'underscore'

export default class ReceiptTextConverter {
  contentToText (content, property) {
    let text = ''
    for (const index in content) {
      text += this.elementToText(content[index], property)
    }
    return text
  }

  elementToText (element, property) {
    switch (element['element']) {
      case 'text':
        return this.text(element, property)
      case 'combine_left_right' :
        return this.combineLeftRight(element, property)
      case 'feed' :
        return this.feed(element)
      default:
        return ''
    }
  }

  text (element, property) {
    const safeText = this._safeText(element['text'])
    let breakedTexts = safeText.split('\n').map(breakedText => {
      return this._breakLongTextByWidth(breakedText, property['RECEIPT_LINE_LENGTH'])
    })
    breakedTexts = _.flatten(breakedTexts)
    return breakedTexts
      .map(line => this._alignText(line, element.align, property['RECEIPT_LINE_LENGTH']))
      .join('\n') + '\n'
  }

  _alignText (text, align, width) {
    let spaceLength = 0
    switch (align) {
      case 'left':
        break
      case 'center':
        spaceLength = (width - this.lengthByShiftJisByte(text)) / 2
        break
      case 'right':
        spaceLength = width - this.lengthByShiftJisByte(text)
        break
    }
    return this.makeSpace(spaceLength) + text
  }

  combineLeftRight (element, property) {
    const spaceLength =
      property['RECEIPT_LINE_LENGTH'] -
      this.lengthByShiftJisByte(element['left']) -
      this.lengthByShiftJisByte(element['right'])
    return element['left'] +
      this.makeSpace(Math.max(0, spaceLength)) +
      element['right'] + '\n'
  }

  feed (element) {
    return '\n'.repeat(element['lines'])
  }

  makeSpace (spaceLength) {
    return spaceLength > 0 ? ' '.repeat(spaceLength) : ''
  }

  lengthByShiftJisByte (text) {
    let length = 0
    for (let count = 0; count < text.length; count++) {
      length += this._isHalfCharAt(text, count) ? 1 : 2
    }
    return length
  }

  _breakLongTextByWidth (text, width) {
    let currentWidth = 0
    let lastBreakedIndex = 0
    const texts = []
    for (let i = 0; i < text.length; i++) {
      const charWidth = this._isHalfCharAt(text, i) ? 1 : 2
      const nextWidth = currentWidth + charWidth
      if (nextWidth > width) {
        texts.push(text.substring(lastBreakedIndex, i))
        currentWidth = charWidth
        lastBreakedIndex = i
      } else {
        currentWidth = nextWidth
      }
    }
    texts.push(text.substring(lastBreakedIndex))
    return texts
  }

  _isHalfCharAt (text, index) {
    const char = text.charCodeAt(index)
    return (
      (char >= 0x0 && char < 0x81) ||
      (char === 0xa0) ||
      (char >= 0xa1 && char < 0xdf) ||
      (char >= 0xfd && char < 0xff)
    )
  }

  _safeText (text) {
    return text === null ? '' : text
  }
}
