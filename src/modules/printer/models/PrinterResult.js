const PRINTER_RESULT_CODE = {
  SUCCESS: 0,
  ERR_PARAM: 1,
  ERR_CONNECT: 2,
  ERR_TIMEOUT: 3,
  ERR_MEMORY: 4,
  ERR_ILLEGAL: 5,
  ERR_PROCESSING: 6,
  ERR_NOT_FOUND: 7,
  ERR_IN_USE: 8,
  ERR_TYPE_INVALID: 9,
  ERR_DISCONNECT: 10,
  ERR_ALREADY_OPENED: 11,
  ERR_ALREADY_USED: 12,
  ERR_BOX_COUNT_OVER: 13,
  ERR_BOX_CLIENT_OVER: 14,
  ERR_UNSUPPORTED: 15,
  ERR_CANCEL: 16,
  ERR_ALREADY_CONNECT: 17,
  ERR_ILLEGAL_DEVICE: 18,
  ERR_CODE_BEHAVIOR: 19,
  ERR_DEVICE_UNPRINTABLE: 20,
  ERR_IOS_VERSION: 21,
  ERR_IMAGE_SIZE: 22,
  ERR_IMAGE_PATH_NOT_FOUND: 23,
  ERR_HEAD_HEAT_ERROR: 24,
  ERR_HEAD_VP_ERR: 25,
  ERR_PAPER_NOT_FOUND: 26,
  ERR_BLE: 27,
  ERR_PRINTER_DISCONNECTED: 28,
  ERR_PRINTER_AUTOCUTTER: 29,
  ERR_PAPER_NEAR_END: 30,
  ERR_PAPER_EMPTY: 31,
  ERR_UNKNOWN: 255
}

export const isSuccess = result => {
  // ニアエンドと用紙切れは成功扱い
  return result === PRINTER_RESULT_CODE.SUCCESS || 
         result === PRINTER_RESULT_CODE.ERR_PAPER_NEAR_END ||
         result === PRINTER_RESULT_CODE.ERR_PAPER_EMPTY
}

export const errorMessage = result => {
  switch (result) {
    case PRINTER_RESULT_CODE.ERR_PARAM:
      return 'パラメーターが適切ではありません。'
    case PRINTER_RESULT_CODE.ERR_CONNECT:
      return '接続に失敗しました。'
    case PRINTER_RESULT_CODE.ERR_TIMEOUT:
      return '接続がタイムアウトになりました。'
    case PRINTER_RESULT_CODE.ERR_MEMORY:
      return '処理に必要なメモリーが確保できませんでした。'
    case PRINTER_RESULT_CODE.ERR_ILLEGAL:
      return '不適切な処理が発生しました。'
    case PRINTER_RESULT_CODE.ERR_PROCESSING:
      return '処理が実行できませんでした。'
    case PRINTER_RESULT_CODE.ERR_NOT_FOUND:
      return 'プリンターが見つかりませんでした。'
    case PRINTER_RESULT_CODE.ERR_IN_USE:
      return 'プリンターが使用中です。'
    case PRINTER_RESULT_CODE.ERR_TYPE_INVALID:
      return 'デバイスの機種が対応していません。'
    case PRINTER_RESULT_CODE.ERR_DISCONNECT:
      return 'プリンターに接続されていません。'
    case PRINTER_RESULT_CODE.ERR_ALREADY_OPENED:
      return 'プリンターが開いています。'
    case PRINTER_RESULT_CODE.ERR_ALREADY_USED:
      return 'プリンター選択に関して、IDの異常が発生しました。'
    case PRINTER_RESULT_CODE.ERR_BOX_COUNT_OVER:
      return '印刷する内容が上限を超えています。不要な内容を削除してください。'
    case PRINTER_RESULT_CODE.ERR_BOX_CLIENT_OVER:
      return '印刷する内容が上限を超えています。不要な内容を削除してください。'
    case PRINTER_RESULT_CODE.ERR_UNSUPPORTED:
      return '接続しようとしているプリンターはサポートされていません。'
    case PRINTER_RESULT_CODE.ERR_CANCEL:
      return 'ペアリング接続のキャンセルが行われました。'
    case PRINTER_RESULT_CODE.ERR_ALREADY_CONNECT:
      return '既にプリンターが接続されています。'
    case PRINTER_RESULT_CODE.ERR_ILLEGAL_DEVICE:
      return '不正なデバイスに接続しようとしています。'
    case PRINTER_RESULT_CODE.ERR_CODE_BEHAVIOR:
      return 'プログラム内で例外が発生しました。メモリご確認ください。'
    case PRINTER_RESULT_CODE.ERR_DEVICE_UNPRINTABLE:
      return 'プリンターが印刷ができない状態です。'
    case PRINTER_RESULT_CODE.ERR_IOS_VERSION:
      return 'iOSのヴァージョンがプリンターのSDKに対応していません。'
    case PRINTER_RESULT_CODE.ERR_UNKNOWN:
      return '不明なエラーが確認されました。\n ヘルプデスクにお問い合わせ下さい。\n03-6452-3038'
    case PRINTER_RESULT_CODE.ERR_IMAGE_SIZE:
      return '画像のサイズが大きすぎます。'
    case PRINTER_RESULT_CODE.ERR_IMAGE_PATH_NOT_FOUND:
      return 'パスが間違っているか、画像が存在しません。'
    case PRINTER_RESULT_CODE.ERR_HEAD_HEAT_ERROR:
      return 'ヘッドが熱くなりすぎているため、自動回復機能が有効になりました。'
    case PRINTER_RESULT_CODE.ERR_HEAD_VP_ERR:
      return 'ヘッドのVP電圧にエラーが生じました。'
    case PRINTER_RESULT_CODE.ERR_PAPER_NOT_FOUND:
      return '紙がセンサーから離れています。'
    case PRINTER_RESULT_CODE.ERR_BLE:
      return 'Ble関連のエラーが起こりました。'
    case PRINTER_RESULT_CODE.ERR_PRINTER_DISCONNECTED:
      return 'プリンターとのコネクトが切断されました。'
    case PRINTER_RESULT_CODE.ERR_PRINTER_AUTOCUTTER:
      return 'オートカッターにエラーが生じました。'
    // case PRINTER_RESULT_CODE.ERR_PAPER_NEAR_END:
    //   return 'レシートロールが少なくなっています。'
    // case PRINTER_RESULT_CODE.ERR_PAPER_EMPTY:
    //   return 'レシートロールが無くなりました。\nレシートロールを交換してください。'
    default:
      return result
  }
}

const CASH_CHANGER_RESULT_CODE = {
  SUCCESS: 0,
  ERR_BUSY: 1,
  ERR_DISCREPANCY: 2,
  ERR_CASH_IN_TRAY: 3,
  ERR_SHORTAGE: 4,
  ERR_REJECT_UNIT: 5,
  ERR_OPOSCODE: 6,
  ERR_UNSUPPORTED: 7,
  ERR_PARAM: 8,
  ERR_COMMAND: 9,
  ERR_DEVICE: 10,
  ERR_SYSTEM: 11,
  ERR_FAILURE: 12,
  OPOS_ERR_CLOSED: 101,
  OPOS_ERR_CLAIMED: 102,
  OPOS_ERR_NOTCLAIMED: 103,
  OPOS_ERR_NOSERVICE: 104,
  OPOS_ERR_DISABLED: 105,
  OPOS_ERR_ILLEGAL: 106,
  OPOS_ERR_NOHARDWARE: 107,
  OPOS_ERR_OFFLINE: 108,
  OPOS_ERR_NOEXIST: 109,
  OPOS_ERR_EXISTS: 110,
  OPOS_ERR_FAILURE: 111,
  OPOS_ERR_TIMEOUT: 112,
  OPOS_ERR_BUSY: 113,
  OPOS_ERR_EXTENDED: 114,
  OPOS_ERR_ECHAN_OVERDISPENSE: 201,
  OPOS_ECHAN_TOTALOVER: 202,
  OPOS_ECHAN_CHANGEERROR: 203,
  OPOS_ECHAN_OVER: 204,
  OPOS_ECHAN_IFERROR: 205,
  OPOS_ECHAN_SETERROR: 206,
  OPOS_ECHAN_ERROR: 207,
  OPOS_ECHAN_CHARGING: 208,
  OPOS_ECHAN_NEAREMPTY: 209,
  OPOS_ECHAN_EMPTY: 210,
  OPOS_ECHAN_NEARFULL: 211,
  OPOS_ECHAN_FULL: 212,
  OPOS_ECHAN_OVERFLOW: 213,
  OPOS_ECHAN_REJECT: 214,
  OPOS_ECHAN_BUSY: 215,
  OPOS_ECHAN_ASYNCBUSY: 216,
  OPOS_ECHAN_CASSETTEWAIT: 217,
  OPOS_ECHAN_COLLECTWAIT: 218,
  OPOS_ECHAN_COUNTERROR: 219,
  OPOS_ECHAN_AMOUNTERROR: 220,
  OPOS_ECHAN_IMPOSSIBLE: 221,
  OPOS_ECHAN_CANNOTPAY: 222,
  OPOS_ECHAN_NOTSTORE: 223,
  OPOS_ECHAN_NEUTRAL: 224,
  OPOS_ECHAN_DEPOSIT: 225,
  OPOS_ECHAN_PAUSEDEPOSIT: 226,
  OPOS_ECHAN_UNMATCH: 227,
  OPOS_ECHAN_DEPOSIT_ELSE_BILL: 228,
  OPOS_ECHAN_DEPOSIT_ELSE_COIN: 229,
  OPOS_ECHAN_DEPOSIT_MOVE_BILL: 230,
  OPOS_ECHAN_DEPOSIT_MOVE_COIN: 231,
  OPOS_ECHAN_DEPOSIT_ERR_BILL: 232,
  OPOS_ECHAN_DEPOSIT_ERR_COIN: 233,
  OPOS_ECHAN_DEPOSIT_RJ_BILL: 234,
  OPOS_ECHAN_DEPOSIT_RJ_COIN: 235,
  OPOS_ECHAN_DEPOSIT_CAS_BILL: 236,
  OPOS_ECHAN_DEPOSIT_OVF_COIN: 237,
  OPOS_ECHAN_DEPOSIT_SET_BILL: 238,
  OPOS_ECHAN_DEPOSIT_SET_COIN: 239,
  OPOS_ECHAN_DEPOSIT_RESET_BILL: 240,
  OPOS_ECHAN_DEPOSIT_RESET_COIN: 241,
  OPOS_OR_ALREADYOPEN: 301,
  OPOS_OR_REGBADNAME: 302,
  OPOS_OR_REGPROGID: 303,
  OPOS_OR_CREATE: 304,
  OPOS_OR_BADIF: 305,
  OPOS_OR_FAILEDOPEN: 306,
  OPOS_OR_BADVERSION: 307,
  OPOS_ORS_NOPORT: 401,
  OPOS_ORS_NOTSUPPORTED: 402,
  OPOS_ORS_CONFIG: 403,
  OPOS_ORS_EVENTCLASS: 451,
  OPOS_ORS_COCREATE: 452,
  OPOS_ORS_PORTCONTROL: 453,
  OPOS_ORS_EVENTTHREAD: 454,
  OPOS_ORS_SENSETHREAD: 455,
  ERR_TIMEOUT: 998,
  ERR_UNKNOWN: 999
}

export const cChangerErrorMessage = result => {
  switch (result) {
    case CASH_CHANGER_RESULT_CODE.ERR_BUSY:
      return '処理が実行できませんでした。'
    case CASH_CHANGER_RESULT_CODE.ERR_DISCREPANCY:
      return '金額が一致していない可能性があります。'
    case CASH_CHANGER_RESULT_CODE.ERR_CASH_IN_TRAY:
      return '現金抜き取り待ち状態です。'
    case CASH_CHANGER_RESULT_CODE.ERR_SHORTAGE:
      return '現金が不足しております。'
    case CASH_CHANGER_RESULT_CODE.ERR_REJECT_UNIT:
      return 'リジェクトユニットの容量オーバーです。'
    case CASH_CHANGER_RESULT_CODE.ERR_OPOSCODE:
      return 'OPOSでエラーが発生しています。'
    case CASH_CHANGER_RESULT_CODE.ERR_UNSUPPORTED:
      return '接続しようとしている自動釣銭機はサポートされていません。'
    case CASH_CHANGER_RESULT_CODE.ERR_PARAM:
      return 'パラメータが不正です。'
    case CASH_CHANGER_RESULT_CODE.ERR_COMMAND:
      return '送信コマンドエラーが発生しました。'
    case CASH_CHANGER_RESULT_CODE.ERR_DEVICE:
      return 'デバイスエラーが発生しました。'
    case CASH_CHANGER_RESULT_CODE.ERR_SYSTEM:
      return 'システムエラーが発生しました。'
    case CASH_CHANGER_RESULT_CODE.ERR_FAILURE:
      return '処理に失敗しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_CLOSED:
      return 'State = OPOS_S_CLOSED の時にアクセスしようとしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_CLAIMED:
      return 'Claimed = TRUE の時にアクセスしようとしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_NOTCLAIMED:
      return 'Claimed = FALSE の時にメソッド゙またはプロパティ設定処理をしようとしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_NOSERVICE:
      return 'COがSOと通信できません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_DISABLED:
      return 'DeviceEnabled = FALSE の時に動作しようとしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_ILLEGAL:
      return 'デバイスに無効な動作か、サポートされていない動作を実行しようとしたか、 無効なパラメータを使用しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_NOHARDWARE:
      return 'デバイスがシステムに接続されていないか、電源 OFF の状態です。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_OFFLINE:
      return 'デバイスがオフラインです。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_NOEXIST:
      return 'ファイル名(他の指定値)が存在しません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_EXISTS:
      return 'ファイル名(他の指定値)がすでに存在しています。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_FAILURE:
      return 'リクエストされた処理をデバイスが実行できません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_TIMEOUT:
      return 'デバイスからの応答を待っているSOがタイムアウトしたか、SOからの応答を待って いるCOがタイムアウトしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_BUSY:
      return 'State = OPOS_S_BUSY の時処理を実行しようとしました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_EXTENDED:
      return 'クラス固有のエラーが発生しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ERR_ECHAN_OVERDISPENSE:
      return '釣銭機内のお金が足りません。\nお金を補充してください。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_TOTALOVER:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_CHANGEERROR:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_OVER:
      return '放出限度額、枚数データが指定されました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_IFERROR:
      return 'データ異常・通信異常。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_SETERROR:
      return 'セットはずれ。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_ERROR:
      return '異常終了・エラー中。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_CHARGING:
      return '補充中。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_NEAREMPTY:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_EMPTY:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_NEARFULL:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_FULL:
      return 'フルを検知しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_OVERFLOW:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_REJECT:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_BUSY:
      return '処理機動作中にコマンド発行した時。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_ASYNCBUSY:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_CASSETTEWAIT:
      return '回収カセット抜取待ち・貨幣抜取待ち。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_COLLECTWAIT:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_COUNTERROR:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_AMOUNTERROR:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_IMPOSSIBLE:
      return 'コマンド受付不可状態。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_CANNOTPAY:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_NOTSTORE:
      return ''
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_NEUTRAL:
      return '待機中(ACK/ETB)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT:
      return '計数計数中(SOH)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_PAUSEDEPOSIT:
      return '計数停止中(EM)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_UNMATCH:
      return 'モード不一致。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_ELSE_BILL:
      return '入金中詳細状態 0:その他(紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_ELSE_COIN:
      return '入金中詳細状態 0:その他(硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_MOVE_BILL:
      return '入金中詳細状態 2:動作中(紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_MOVE_COIN:
      return '入金中詳細状態 2:動作中(硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_ERR_BILL:
      return '入金中詳細状態 3:エラー(紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_ERR_COIN:
      return '入金中詳細状態 3:エラー(硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_RJ_BILL:
      return '入金中詳細状態 4:RJ中(紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_RJ_COIN:
      return '入金中詳細状態 4:RJ中(硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_CAS_BILL:
      return '入金中詳細状態 5:カセットフル(紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_OVF_COIN:
      return '入金中詳細状態 5:収納庫フル(硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_SET_BILL:
      return '入金中詳細状態 6:セット外 (紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_SET_COIN:
      return '入金中詳細状態 6:セット外 (硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_RESET_BILL:
      return '入金中詳細状態 7:リセット (紙幣)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ECHAN_DEPOSIT_RESET_COIN:
      return '入金中詳細状態 7:リセット (硬貨)。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_ALREADYOPEN:
      return 'コントロールはすでにオープンされています。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_REGBADNAME:
      return 'レジストリに指定したデバイス名称キーが存在しません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_REGPROGID:
      return 'デバイス名キーのデフォルト値が読めないか、そこに保持されたプログラマ ティク ID を有効なクラス ID に変換できませんでした。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_CREATE:
      return 'サービスオブジェクトインスタンスを生成できなかったか、IDispatch インター フェースを取得できませんでした。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_BADIF:
      return 'サービスオブジェクトは指定版数で要求されるひとつ、もしくはひとつ以上 のメソッドをサポートしていません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_FAILEDOPEN:
      return 'サービスオブジェクトはオープン呼び出しでエラーのステータスを返しまし た、しかしながらそれ以上の定義されたエラーコードを返しませんでした。'
    case CASH_CHANGER_RESULT_CODE.OPOS_OR_BADVERSION:
      return 'サービスオブジェクトのメジャーバージョンはコントロールオブジェクトのメジ ャーバージョンに適合していません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_NOPORT:
      return 'サービスオブジェクトで使用する COM ポートがオープンできませんでした。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_NOTSUPPORTED:
      return 'サービスオブジェクトは指定されたデバイスをサポートしていません。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_CONFIG:
      return '構成情報エラー。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_EVENTCLASS:
      return 'イベント処理用オブジェクトクラスの作成に失敗しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_COCREATE:
      return 'サービスオブジェクト側からコントロールオブジェクト側のインターフェイスを 取得できませんでした。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_PORTCONTROL:
      return 'COM ポート設定時にエラーが発生しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_EVENTTHREAD:
      return 'イベント処理用スレッドの起動に失敗しました。'
    case CASH_CHANGER_RESULT_CODE.OPOS_ORS_SENSETHREAD:
      return 'センススレッドの起動に失敗しました。'
    case CASH_CHANGER_RESULT_CODE.ERR_UNKNOWN:
      return '不明なエラーが発生しました。'
    default:
      return ''
  }
}

export const isPaperNearEnd = resultCode => {
  return resultCode === PRINTER_RESULT_CODE.ERR_PAPER_NEAR_END
}
export const isPaperEnd = resultCode => {
  return resultCode === PRINTER_RESULT_CODE.ERR_PAPER_EMPTY
}
