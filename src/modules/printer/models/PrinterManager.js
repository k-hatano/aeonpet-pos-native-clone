import logger from 'common/utils/logger'
import Printer, { _PRINTER_STD, interfaceToSDK } from './Printer'
import { printerParameter, PRINTER_MAKERS } from 'modules/setting/models'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import { waitAsync } from '../../../common/utils/waitAsync'

class PrinterManager extends StoreAccessibleBase {
  printerProperty = (printerMaker) => {
    return {
      PRINTER_SDK: printerParameter(printerMaker).sdk,
      PRINTER_STD: _PRINTER_STD[printerParameter(printerMaker).interfaceLabel],
      PRINTER_IP: this.getSetting(SettingKeys.EQUIPMENT.PRINTER.ADDRESS),
      RECEIPT_LINE_LENGTH: printerParameter(printerMaker).paperWidth
    }
  }

  async print (receipt, openDrawer = false) {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      receipt = this._convertReceiptForEpson(receipt)
      const printContent = JSON.stringify(receipt)
      await logger.debug('print content : ', printContent)
      return new Promise((resolve, reject) => {
        printer.printReceipt(printContent, (openDrawer && !this.getSetting(SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER)), (resultCode) => {
          resolve(resultCode)
        }, async (error) => {
          await saveOperationLog(OPERATION_TYPE.PRINT_ERROR, new Date())
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async disconnectCasher () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.disconnectCasher(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async connectCasher () {
    logger.info('connectCasher')
    if (!this.getSetting(SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER)) {
      return
    }
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.connectCasher(() => {
          logger.info('connectCasher succeed')
          resolve()
        }, (error) => {
          logger.info('connectCasher fail')
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async readCashCount () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.readCashCount(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async depositCasher () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.depositCasher(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async pauseDepositCasher () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.pauseDepositCasher(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async restartDepositCasher () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.restartDepositCasher(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  endDepositCasher (type) {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.endDepositCasher(type, () => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async dispenseCasher (jsonCoin) {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.dispenseCasher(jsonCoin, () => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async dispenseChange (changeValue) {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.dispenseChange(changeValue, () => {
          resolve()
        }, (error) => {
          reject(new Error(error))
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  async readCasherStatus () {
    const printerMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER)
    if (printerMaker) {
      const printer = new Printer()
      printer.setPrinterProperty(this.printerProperty(printerMaker))
      return new Promise((resolve, reject) => {
        printer.readCasherStatus(() => {
          resolve()
        }, (error) => {
          reject(error)
        })
      })
    } else {
      // プリンタを使わない設定の場合、何もせず成功したことにする。
    }
  }

  _convertReceiptForEpson (receiptData) {
    return {
      ...receiptData,
      content: receiptData.content.map(element => {
        if (element.element === 'image') {
          let imagePath = ''
          switch (element.key) {
            case 'order_header_logo':
              imagePath = this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.HEADER_LOGO_FILE)
              break
            case 'order_footer_logo':
              imagePath = this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.FOOTER_LOGO_FILE)
              break
            case 'bill_logo':
              imagePath = this.getSetting(SettingKeys.RECEIPT.BILL.HEADER_LOGO_FILE)
              break
          }
          return {
            element: 'image',
            path: imagePath
          }
        } else {
          return element
        }
      })
    }
  }

  isSetPrinter () {
    const _isSetPrinterMaker = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.MAKER) !== PRINTER_MAKERS.NOT_SELECTED
    const _isSetPrinterAddress = this.getSetting(SettingKeys.EQUIPMENT.PRINTER.ADDRESS) !== ''
    return _isSetPrinterMaker && _isSetPrinterAddress
  }

  isSelectedCashChanger () {
    return this.getSetting(SettingKeys.EQUIPMENT.CASH_CHANGER.IS_SELECTED_CASH_CHANGER)
  }
}

const printerManager = new PrinterManager()
export default printerManager
