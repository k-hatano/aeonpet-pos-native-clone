import I18n from 'i18n-js'
import { NativeEventEmitter, NativeModules } from 'react-native'
import logger from 'common/utils/logger'
import PrinterManager from './PrinterManager'
import CashChangerDataConverter from './CashChangerDataConverter'
import { CASH_CHANGER_STATUS } from '../models'
import { isSuccess, cChangerErrorMessage } from 'modules/printer/models/PrinterResult'

const printerBridgeEventEmitter = new NativeEventEmitter(NativeModules.PrinterBridge)

class CashChanger {
  get isEnabled () {
    return PrinterManager.isSelectedCashChanger() && PrinterManager.isSetPrinter()
  }

  /**
   * Return CASH_CHANGER_STATUS
   * @return {Number}
   */
  init () {
    return new Promise(async (resolve, reject) => {
      try {
        logger.info('init CashChanger')
        await PrinterManager.connectCasher()
        await this.beginDepositCashChanger()
        await this.checkCashierStatus()
        resolve()
      } catch (error) {
        if (!(error instanceof Error)) {
          error = new Error(error)
        }
        reject(error)
      }
    })
  }

  final (type) {
    this.endDepositCashChanger(type)
    this.disconnectCashChanger()
  }

  checkCashierStatus () {
    return new Promise(async (resolve, reject) => {
      try {
        await this.cashChangerStatusListener()
        resolve()
      } catch (error) {
        let errorMessage = ''
        switch (error) {
          case CASH_CHANGER_STATUS.OFFLINE:
            errorMessage = I18n.t('check_sale.cashChanger_not_connected')
            break
          case CASH_CHANGER_STATUS.LACK_MONEY:
            errorMessage = I18n.t('check_sale.cashChanger_lack_money')
            break
        }

        reject(new Error(errorMessage))
      }
    })
  }

  cashChangerStatusListener () {
    return new Promise(async (resolve, reject) => {
      printerBridgeEventEmitter.addListener('SendCashChangerStatus', (body) => {
        printerBridgeEventEmitter.removeAllListeners('SendCashChangerStatus')

        if (body.length < 1) {
          reject(CASH_CHANGER_STATUS.OFFLINE)
          return
        }

        const status = CashChangerDataConverter.convertResponse(body)
        if (status.existEmptyMoney()) {
          reject(CASH_CHANGER_STATUS.LACK_MONEY)
          return
        }

        resolve()
      })
      try {
        await PrinterManager.readCasherStatus()
      } catch (error) {
        reject(CASH_CHANGER_STATUS.OFFLINE)
      }
    })
  }

  async endDepositCashChanger (type) {
    try {
      await PrinterManager.endDepositCasher(type)
      logger.debug('endDeposit success')
    } catch (error) {
      logger.debug('endDeposit failed ' + error)
    }
  }

  async disconnectCashChanger () {
    try {
      await PrinterManager.disconnectCasher()
      logger.debug('disconnectCashChanger success')
    } catch (error) {
      logger.debug('disconnectCashChanger failed ' + error)
    }
  }

  beginDepositCashChanger () {
    logger.info('beginDepositCashChanger')
    let onBeginSucceeded
    let onError
    const removeListener = () => {
      printerBridgeEventEmitter.removeListener('SendDepositAmountUpdate', onBeginSucceeded)
      printerBridgeEventEmitter.removeListener('SendCashChangerError', onError)
    }

    return new Promise((resolve, reject) => {
      onBeginSucceeded = (body) => {
        removeListener()
        logger.info('beginDeposit success')
        resolve()
      }
      onError = (body) => {
        removeListener()
        logger.debug('beginDeposit faild ' + body)
        reject(new Error(cChangerErrorMessage(body)))
      }
      printerBridgeEventEmitter.addListener('SendDepositAmountUpdate', onBeginSucceeded)
      printerBridgeEventEmitter.addListener('SendCashChangerError', onError)
      PrinterManager.depositCasher()
    })
  }

  dispenseChange (changeValue) {
    if (!(changeValue instanceof Number)) {
      changeValue = parseInt(changeValue)
    }

    const removeListener = () => {
      printerBridgeEventEmitter.removeAllListeners('SendDispenseComplete')
      printerBridgeEventEmitter.removeAllListeners('SendCashChangerError')
    }

    return new Promise(async (resolve, reject) => {
      printerBridgeEventEmitter.addListener('SendDispenseComplete', (body) => {
        removeListener()
        logger.debug('dispenseChange success')
        resolve()
      })

      printerBridgeEventEmitter.addListener('SendCashChangerError', (body) => {
        removeListener()
        logger.debug('dispenseChange faild ' + body)
        reject(new Error(cChangerErrorMessage(body)))
      })

      try {
        await PrinterManager.dispenseChange(changeValue)
      } catch(error) {
        reject(error)
      }
    })
  }
}

export default new CashChanger()
