import { handleActions } from 'redux-actions'
import * as actions from '../action'

const defaultState = {
  printerErrors: [],
  currentReceipt: {
    data_text: "",
    id: "",
    data: "",
  }
}

const handlers = {
  [actions.listPrinterErrors]: (state, action) => ({
    ...state,
    ...{ printerErrors: action.payload }
  }),
  [actions.setCurrentReceipt]: (state, action) => ({
    ...state,
    ...{currentReceipt: action.payload}
  }),
  [actions.resetCurrentReceipt]: (state, action) => {
    const currentReceipt = {
      data_text: "",
      id: "",
      data: ""
    }
    return {
      ...state,
      ...{currentReceipt}
    }
  }
}

export default handleActions(handlers, defaultState)
