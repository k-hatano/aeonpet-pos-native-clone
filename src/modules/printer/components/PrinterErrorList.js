'use strict'

import React, { Component } from 'react'
import { Text, View, ListView, TouchableOpacity } from 'react-native'
import printerErrorStyles from '../styles/PrinterError'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import {ProceedButton} from '../../../common/components/elements/StandardButton'
import moment from 'moment'

export default class printerErrorList extends Component {
  static propTypes = {
    printerErrors: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  }

  _getClientCreateDateTimeFormat = (timestamp) => {
    return moment.unix(timestamp).format('YYYY-MM-DD HH:mm:ss')
  }

  _renderPrinterErrorItem = (item) => (
    <TouchableOpacity
      onPress={() => this.props.onSelectPrinterError(item.data_text, item.data, item.id)}
    >
      <View style={[printerErrorStyles.listViewContainer]}>
        <View style={[printerErrorStyles.listViewRow]}>
          <Text style={[printerErrorStyles.listViewClientCreatedAt]}>
            {this._getClientCreateDateTimeFormat(item.client_created_at)}
          </Text>
          <Text style={[printerErrorStyles.listViewReceiptTitle]}>{item.receipt_title}</Text>
        </View>
      </View>
    </TouchableOpacity>
    )

  render () {
    const {printerErrors} = this.props
    let printerErrorSource = this.dataSource.cloneWithRows(printerErrors)
    return (
      <View style={printerErrorStyles.container}>
        <View style={printerErrorStyles.listHeader}>
          <View style={printerErrorStyles.listTitleContainer}>
            <Text style={printerErrorStyles.listTitleText}>{I18n.t('printer.printer_error_list')}</Text>
          </View>
          <View style={printerErrorStyles.allDeleteButtonArea}>
            <ProceedButton
              text={I18n.t('printer.delete_all')}
              style={printerErrorStyles.allDeleteButton}
              onPress={() => this.props.onPressAllDetele()}/>
          </View>
        </View>
        <View style={printerErrorStyles.listViewArea}>
          {printerErrorSource && 
            <ListView
             dataSource={printerErrorSource}
             renderRow={item => this._renderPrinterErrorItem(item)}
             enableEmptySections />
          }
        </View>
      </View>
    )
  }
}
