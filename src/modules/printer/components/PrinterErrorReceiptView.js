'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import printerErrorStyles from '../styles/PrinterError'
import PropTypes from 'prop-types'
import ReceiptView from './ReceiptView'
import { ProceedButton, WeakProceedButton } from '../../../common/components/elements/StandardButton'

export default class PrinterErrorReceiptView extends Component {
  static propTypes = {
    receiptDataText: PropTypes.string,
    receiptDataId: PropTypes.string,
    receiptData: PropTypes.string
  }

  render () {
    const {receiptDataText, receiptDataId, receiptData} = this.props
    return (
      <View style={printerErrorStyles.receiptViewArea}>
        <View style={[printerErrorStyles.receiptViewContainer]}>
          {receiptDataText.length > 0 && receiptDataId.length > 0 && 
            <ReceiptView receiptDataText={receiptDataText} />
          }
        </View>
        {receiptDataText.length > 0 && receiptDataId.length > 0 && 
          <View style={[printerErrorStyles.receitViewFooterContainer]}>
            <View style={printerErrorStyles.receiptViewbuttonContainer}>
              <WeakProceedButton
                  style={printerErrorStyles.deleteButton}
                  onPress={() => this.props.onDelete(receiptDataId)}
                  text={I18n.t('printer.delete')} />
              <ProceedButton
                style={printerErrorStyles.reprintButton}
                onPress={() => this.props.onReprint(receiptData, receiptDataId)}
                text={I18n.t('printer.reprint')} />
            </View>
          </View>
        }
      </View>
    )
  }
}
