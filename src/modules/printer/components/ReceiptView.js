'use strict'
import React, { Component } from 'react'
import I18n from 'i18n-js'
import { View, Text, ScrollView } from 'react-native'
import PropTypes from 'prop-types'

export default class ReceiptView extends Component {
  static propTypes = {
    receiptDataText: PropTypes.string
  }
  render () {
    return (
      <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center', margin: 20}}>
        <ScrollView style={{width: 290, height: 650, backgroundColor: '#fff', borderWidth: 1, borderColor: '#bbb'}}>
          <Text style={{margin: 30, fontFamily: 'IPAGothic'}}>{this.props.receiptDataText}</Text>
        </ScrollView>
      </View>
    )
  }
}
