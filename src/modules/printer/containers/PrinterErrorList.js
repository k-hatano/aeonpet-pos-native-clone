import { connect } from 'react-redux'
import PrinterErrorList from '../components/PrinterErrorList'
import {MODULE_NAME} from '../models'
import * as actions from '../action'
import AlertView from 'common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import EJournalRepository from '../repositories/EJournalRepository'
import I18n from 'i18n-js'
import { loading } from 'common/sideEffects'
import { setPrinterErrorsCount } from '../../home/actions'

const mapDispatchToProps = dispatch => ({
    onSelectPrinterError: async (data_text, data, id) => {
      try {
        dispatch(actions.setCurrentReceipt({ data_text, id, data }))
      } catch (error) {
        dispatch(actions.resetCurrentReceipt())
      }
    },
    onPressAllDetele: async () => {
      try {
        await ConfirmView.show(
          I18n.t('printer.delete_all_alert'),
        async () => {
          await loading(dispatch, async () => {
            // 全ての電子ジャーナルに印刷済みフラグを立てることで全て削除の状態とする
            EJournalRepository.saveIsPrintedAll()
            dispatch(actions.resetCurrentReceipt())
            dispatch(actions.listPrinterErrors([]))
            dispatch(setPrinterErrorsCount(0))
          })
         return true },
        () => { return true })
      } catch (error){
        AlertView.show('Error')
      }
    }
})

const mapStateToProps = state => ({
    printerErrors: state[MODULE_NAME].printerErrors,
})

export default connect(mapStateToProps, mapDispatchToProps)(PrinterErrorList)
