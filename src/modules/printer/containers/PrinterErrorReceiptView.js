import { connect } from 'react-redux'
import PrinterErrorReceiptView from '../components/PrinterErrorReceiptView'
import {MODULE_NAME} from '../models'
import * as actions from '../action'
import AlertView from 'common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import EJournalRepository from '../repositories/EJournalRepository'
import PrinterManager from 'modules/printer/models/PrinterManager'
import I18n from 'i18n-js'
import { loading } from 'common/sideEffects'
import { setPrinterErrorsCount } from '../../home/actions'

const mapDispatchToProps = dispatch => ({
  onDelete: async (id) => {
    try {
      await ConfirmView.show(
      I18n.t('printer.delete_alert'),
      async () => {
        await loading(dispatch, async () => {
          await EJournalRepository.saveIsPrintedById(id)
          dispatch(actions.resetCurrentReceipt())
          const printerErrors = await EJournalRepository.findByIsPrinted(0)
          const printerErrorCount = Object.keys(printerErrors).length
          dispatch(actions.listPrinterErrors(printerErrors))
          dispatch(setPrinterErrorsCount(printerErrorCount))
        })
        return true},
      () => { return true })
    } catch (error) {
      AlertView.show('Error')
    }
  },

  onReprint: async (data, id) => {
    try {
      await loading(dispatch, async () => {
        await PrinterManager.print(JSON.parse(data))
        await EJournalRepository.saveIsPrintedById(id)
        dispatch(actions.resetCurrentReceipt())
        const printerErrors = await EJournalRepository.findByIsPrinted(0)
        const printerErrorCount = Object.keys(printerErrors).length
        dispatch(actions.listPrinterErrors(printerErrors))
        dispatch(setPrinterErrorsCount(printerErrorCount))
      })
    } catch (error) {
      if (error) {
        AlertView.show(error)
      } else {
        AlertView.show(I18n.t('printer.reprint_error_alert'))
      }
    }
  }

})

const mapStateToProps = state => ({
  receiptDataText: state[MODULE_NAME].currentReceipt.data_text,
  receiptDataId: state[MODULE_NAME].currentReceipt.id,
  receiptData: state[MODULE_NAME].currentReceipt.data
})

export default connect(mapStateToProps, mapDispatchToProps)(PrinterErrorReceiptView)
