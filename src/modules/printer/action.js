import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listPrinterErrors = createAction(`${MODULE_NAME}_listPrinterErrors`)
export const setCurrentReceipt = createAction(`${MODULE_NAME}_setCurrentReceipt`)
export const resetCurrentReceipt = createAction(`${MODULE_NAME}_resetCurrentReceipt`)