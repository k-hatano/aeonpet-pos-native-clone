import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    overflow: 'hidden'
  },
  receiptViewContainer: {
    backgroundColor: '#f0f0f0',
    flexDirection: 'column',
    width: '100%',
    overflow: 'hidden'
  },
  receitViewFooterContainer: {
    width:'100%',
    height: 104,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#bbb',
    position: 'absolute',
    bottom: 0
  },
  listHeader: {
    height: 64,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#95989a',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 24
  },
  listTitleContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  listTitleText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  listViewArea:{
    alignItems: 'center'
  },
  listViewContainer: {
    width:635,
    height: 64,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#bbb'
  },
  listViewRow: {
    flexDirection:'row',
    justifyContent: 'space-between',
    marginTop: 22
  },
  listViewClientCreatedAt: {
    fontFamily: 'Helvetica',
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29,
    textAlign: 'left'
  },
  listViewReceiptTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    textAlign: 'right'
  },
  allDeleteButtonArea: {
    borderWidth: 2,
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderRadius: 10,
    width: 120,
    height: 50
  },
  allDeleteButton: {
    width: 116,
    height: 46,
    fontSize: 24,
    fontWeight: 'bold'
  },
  receiptViewbuttonContainer: {
    flexDirection: 'row',
    margin: 32,
    justifyContent: 'space-between'
  },
  reprintButton: {
    width: 138,
    height: 50,
    fontSize: 24,
    fontWeight: 'bold'
  },
  deleteButton: {
    width: 138,
    height: 50,
    fontSize: 24,
    fontWeight: 'bold'
  },
  receiptViewArea: {
    position: 'relative',
    height: '100%'
  }
})
