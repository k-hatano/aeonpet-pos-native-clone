import generateUuid from '../../common/utils/generateUuid'
import ReceiptTextConverter from './models/ReceiptTextConverter'
import settingGetter from 'modules/setting/models/settingGetter'
import EjournalRepositry from './repositories/EJournalRepository'
import * as actions from './action'

export const MODULE_NAME = 'printer'

export const RECEIPT_TYPE = {
  OPEN_DRAWER: 1, // ドロアオープン
  ORDER: 2, // 会計レシート
  BILL: 3, // 領収証レシート
  RETURN_ORDER: 4, // 返品レシート
  CANCEL_ORDER: 5, // レジマイナスレシート
  ADD_POINT: 6, // ポイント付与レシート
  INDEPENDENT_RETURN_ORDER: 7, // 取引なし返品レシート
  CHECK_SALES: 8, // 点検レシート
  CLOSE_SALES: 9, // 精算レシート
  TERM_TOTAL_SALES: 10, // 月間レシート
  DEPOSIT: 11, // 入金レシート
  WITHDRAWAL: 12, // 出金レシート
  OPEN_SALES: 13, // 開局レシート
  TAXFREE_SALES: 14, // 輸出免税物品購入記録起票
  TAXFREE_CUSTOMER: 15, // 購入者誓約書
  TAXFREE_SHIPPING: 16, // 梱包用リスト
  CASH_RECORD: 17, // 在高レシート
  CREDIT: 18, // クレジット伝票
  CREDIT_RETURN: 19, // クレジット返品伝票
  RETURN_FOR_SHOP: 20, // 返品レシート(売り場控え)
  BILL_FOR_SHOP: 21, // 領収証レシート(売り場控え)
  CANCELED_BILL: 22 // 領収証レシート(取り消し)
}

export const makeBaseEJournal = (title, receipt, type, createdAt) => {
  if (createdAt instanceof Date) {
    createdAt = Math.floor(createdAt.getTime() / 1000)
  }

  const receiptText = makeReceiptText(receipt)
  return {
    id: generateUuid(),
    data: JSON.stringify(receipt),
    data_text: receiptText,
    receipt_title: title,
    receipt_type: type,
    cashier_id: settingGetter.cashierId,
    is_printed: 0,
    is_pushed: 0,
    is_reprint: 0,
    is_print_tax_stamp: 0,
    client_created_at: createdAt
  }
}

export const makeReceiptText = (receipt) => {
  if (receipt == null) return null
  return (new ReceiptTextConverter())
    .contentToText(receipt.content, { RECEIPT_LINE_LENGTH: 32 })  //2019.6 58mm対応
}

export const initForCreatePrintErrors = async (dispatch) => {
  const printerErrors = await EjournalRepositry.findByIsPrinted(0)
  dispatch(actions.listPrinterErrors(printerErrors))
}

export const clearSelected = async (dispatch) => {
  dispatch(actions.listPrinterErrors([]))
  dispatch(actions.setCurrentReceipt({
    data_text: '',
    id: '',
    data: ''
  }))
}

export const CASH_CHANGER_STATUS = {
  ONLINE: 1,
  OFFLINE: 2,
  LACK_MONEY: 3
}

export const DEPOSIT_TYPE = {
  /** つり銭あり(収納部への収納を保留する。) */
  CHANGE: 0,
  /** つり銭なし(収納部に収納する。) */
  NOCHANGE: 1,
  /** 預かり金返却 */
  REPAY: 2
}