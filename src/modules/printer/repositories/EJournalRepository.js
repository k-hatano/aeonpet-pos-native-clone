import SampleEJournalRepository from './sample/SampleEJournalRepository'
import StandardEJournalRepository from './standard/StandardEJournalRepository'
import TrainingEJournalRepository from './training/TrainingEJournalRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class EJournalRepository {
  static _implement = new SampleEJournalRepository()

  static async save (ejournal, transaction) {
    return this._implement.save(ejournal, transaction)
  }

  /**
   * サーバに保存された電子ジャーナルを取得
   * @param posOrderNumber
   * @param receiptType
   * @returns {Promise}
   */
  static async fetchByOrderNumber (posOrderNumber, receiptType) {
    return this._implement.fetchByOrderNumber(posOrderNumber, receiptType)
  }

  /**
   * クライアント作成日時とレシート種類を指定して電子ジャーナルを検索します。
   * @param fromDate
   * @param toDate
   * @param receiptType
   * @returns {Promise}
   */
  static async fetchByClientCreatedAt (fromDate, toDate, receiptType) {
    return this._implement.fetchByClientCreatedAt(fromDate, toDate, receiptType)
  }

  /**
   * ローカル、サーバーの両方から電子ジャーナルを探します。
   * ローカル優先とし、ローカルに一つ以上条件に合うものがあればサーバーへはアクセスせず、ローカルのものだけ返します。
   * @param posOrderNumber
   * @param {number} receiptType
   * @param {boolean} failToNotFound trueを指定すると、ネットワークエラーが出た場合に例外を投げず、空配列を返します。
   * @return {Promise<*>}
   */
  static async findOrFetchByOrderNumber (posOrderNumber, receiptType, failToNotFound = false) {
    const found = await this._implement.findByOrderNumber(posOrderNumber, receiptType)
    if (found && found.length > 0) {
      return found
    }

    try {
      return await this._implement.fetchByOrderNumber(posOrderNumber, receiptType)
    } catch (error) {
      if (failToNotFound) {
        return []
      } else {
        throw error
      }
    }
  }

  /**
   * ローカルに保存された電子ジャーナルを取得
   * @param posOrderNumber
   * @param receiptType
   * @returns {Promise}
   */
  static async findByOrderNumber (posOrderNumber, receiptType) {
    return this._implement.findByOrderNumber(posOrderNumber, receiptType)
  }

  static async findUnpushedEJournals () {
    return this._implement.findUnpushedEJournals()
  }

  /**
   * ローカルに保存された電子ジャーナルをサーバへ送信する
   * @param eJournalId
   * @returns {Promise}
   */
  static async pushById (eJournalId, isErrorAlert = false) {
    return this._implement.pushById(eJournalId, isErrorAlert)
  }

  /**
   * 送信済みであることを記録する
   * @param eJournalId
   * @returns {Promise}
   */
  static async saveIsPrintedById (eJournalId) {
    return this._implement.saveIsPrintedById(eJournalId)
  }
  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardEJournalRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingEJournalRepository()
        break

      default:
        this._implement = new SampleEJournalRepository()
        break
    }
  }

  /**
   * 印刷済みフラグからレシートの電子ジャーナルを取得
   */
  static async findByIsPrinted (isPrinted) {
    return this._implement.findByIsPrinted(isPrinted)
  }

  /**
   * 全件印刷済み状態にする
   */
  static async saveIsPrintedAll () {
    return this._implement.saveIsPrintedAll()
  }

  /**
   * 条件を指定して検索する。
   * @param query
   * @returns {Promise.<void>}
   */
  static async findAll (query) {
    return this._implement.findAll(query)
  }

  /**
   * 未送信の電子ジャーナルを送信する
   */
  static async sendUnpushEjournals () {
    return this._implement.sendUnpushEjournals()
  }

  /**
   * XX日以上前の送信済みの電子ジャーナルを削除する
   */
  static async clearBeforeDate (timestamp) {
    return this._implement.clearBeforeDate(timestamp)
  }

  /**
   * 全ての会計を強制的に送信済みにする。
   * @return {Promise}
   */
  static async saveIsPushedAll () {
    return this._implement.saveIsPushedAll()
  }

  /**
   * 特定の電子ジャーナルを印刷エラーのステータスにする
   */
  static async saveIsNotPrintedById (id) {
    return this._implement.saveIsNotPrintedById(id)
  }
}

AppEvents.onRepositoryContextChanged('EJournalRepository', (context) => {
  EJournalRepository.switchImplement(context)
})
