import FormalReceiptSettingForm from "../../../setting/components/FormalReceiptSettingForm";

/**
 * 自動釣銭機の状態エンティティ
 */
export default class CashChangerStatusEntity {
  constructor () {
    /**
     * 紙幣部エラーコード
     * 4 Byte
     */
    this._billErrorCode = {
      byte: 4,
      value: '0000'
    }

    /**
     * 紙幣メカセット状態
     * Bit: 内容
     * 7: 予約(0 固定)
     * 6: 投入口残留 (1 で残留検知)
     * 5: 払出部残留 (1 で残留検知)
     * 4: RAD-300 接続の場合:未使用
     * 3: RAD-380 接続の場合:RJ 部扉開検知(1 で開検知) カセットセット外れ(1 でセット外れ)
     * 2: 収納庫扉セット外れ(1 でセット外れ)
     * 1: RAD-300 接続の場合:前カバーセット外れ(1 でセット外れ) RAD-380 接続の場合:未使用
     * 0: ユニットセット外れ(1 でセット外れ)
     * 2 Byte
     */
    this._billMachineStatus = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣部解除手順
     * 00:解除完了
     * 01:リセット待ち
     * 02:リセット中
     * 03:要因解除待ち
     * 04:要因解除中
     * 2 Byte
     */
    this._billCancel = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣部収納状態
     * 0:エンプティ
     * 1:ニアエンプティ
     * 2:適量
     * 3:ニアフル
     * 4:フル
     * 5:収納部無し
     * 1 Byte
     */
    this._billAmountStatusJpy10000 = {
      byte: 1,
      value: '0'
    }

    this._billAmountStatusJpy5000 = {
      byte: 1,
      value: '0'
    }

    this._billAmountStatusJpy2000 = {
      byte: 1,
      value: '0'
    }

    this._billAmountStatusJpy1000 = {
      byte: 1,
      value: '0'
    }

    this._billAmountStatusCassette = {
      byte: 1,
      value: '0'
    }

    this._billAmountStatusRJ = {
      byte: 1,
      value: '0'
    }

    /**
     * 紙幣部在高異常解除方法
     * 0:在高異常の可能性なし
     * 1:回収必要
     * 2:戻し入れ必要
     * 1 Byte
     */
    this._billResidenceCancelJpy5000Over = {
      byte: 1,
      value: '0'
    }

    this._billResidenceCancelJpy1000Over = {
      byte: 1,
      value: '0'
    }

    this._billResidenceCancelCassette = {
      byte: 1,
      value: '0'
    }

    this._billResidenceCancelUnuse = {
      byte: 1,
      value: '0'
    }

    /**
     * 処理機状態フラグ
     * ‘00’:処理機待機中
     * ‘01’:処理機省電力中
     * ‘02’:処理機動作中
     * 2 Byte
     */
    this._billprocessStatus = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣在高状態フラグ
     * ‘00’:在高異常の可能性なし
     * ‘01’:収納部又は収納カセットに在高異常の可能性あり
     * 2 Byte
     */
    this._billResidenceStatusFlg = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣硬貨状態フラグ
     * 2 Byte
     */
    this._billCoinStatusFlg = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣部ローカル操作禁止モード
     * ‘00’:ローカル操作許可
     * ‘01’:ローカル操作禁止
     * 2 Byte
     */
    this._billLocalOperationMode = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣搬送路情報
     * 2 Byte
     */
    this._billPathInfo = {
      byte: 2,
      value: '00'
    }

    /**
     * エラー貨幣の取り扱い(紙幣)
     * ‘0’: 判定不可
     * ‘1’: 機外貨幣
     * ‘2’: 機内貨幣(在高に含む)
     * 1 Byte
     */
    this._billErrorMoney = {
      byte: 1,
      value: '0'
    }

    /**
     * エラー貨幣の取り扱い(貨幣)
     * ‘0’: 判定不可
     * ‘1’: 機外貨幣
     * ‘2’: 機内貨幣(在高に含む)
     * 1 Byte
     */
    this._coinErrorMoney = {
      byte: 1,
      value: '0'
    }

    /**
     * 硬貨部在高異常解除方法
     * ‘0’:在高異常の可能性なし
     * ‘1’:回収必要
     * ‘2’:戻しいれ必要
     * 1 Byte
     */
    this._coinResidenceCancelJPY100Over = {
      byte: 1,
      value: '0'
    }

    this._coinResidenceCancelJPY10Over = {
      byte: 1,
      value: '0'
    }

    this._coinResidenceCancelJPY1Over = {
      byte: 1,
      value: '0'
    }

    this._coinResidenceCancelJPYUnuse = {
      byte: 1,
      value: '0'
    }

    /**
     * 硬貨搬送路情報
     * 2 Byte
     */
    this._coinPathInfo = {
      byte: 2,
      value: '00'
    }

    /**
     * 紙幣部ローカル操作禁止モード
     * ‘00’:ローカル操作許可
     * ‘01’:ローカル操作禁止
     * 2 Byte
     */
    this._coinLocalOperationMode = {
      byte: 2,
      value: '00'
    }

    /**
     * 硬貨メカセット状態
     * Bit: 内容
     * 7: メンテキー(1 でメンテ位置)
     * 6: 投入口残留 (1 で残留検知)
     * 5: トレイフル(1 でトレイフル)
     * 4: トレイ残留(1 で残留検知)
     * 3: 収納庫扉セット外れ(1 でセット外れ)
     * 2: 予約(0 固定)
     * 1: 予約(1 固定)
     * 0: 装置内部セット外れ(1 でセット外れ)
     * 2 Byte
     */
    this._coinMachineStatus = {
      byte: 2,
      value: '00'
    }

    /**
     * 硬貨部エラーコード
     * 4 Byte
     */
    this._coinErrorCode = {
      byte: 4,
      value: '0000'
    }

    /**
     * 硬貨部解除手順
     * 00:解除完了
     * 01:リセット待ち
     * 02:リセット中
     * 03:要因解除待ち
     * 04:要因解除中
     * 2 Byte
     */
    this._coinCancel = {
      byte: 2,
      value: '00'
    }

    /**
     * 硬貨在高状態フラグ
     * ‘00’:在高異常の可能性なし
     * ‘01’:収納部に在高異常の可能性あり
     * 2 Byte
     */
    this._coinResidenceStatusFlg = {
      byte: 2,
      value: '00'
    }

    /**
     * 硬貨包装部状態
     * 0:エンプティ
     * 1:ニアエンプティ
     * 2:適量
     * 1 Byte
     */
    this._coinWrappedStatusJpy100Over = {
      byte: 1,
      value: '0'
    }

    this._coinWrappedStatusJpy10Over = {
      byte: 1,
      value: '0'
    }

    this._coinWrappedStatusJpy1Over = {
      byte: 1,
      value: '0'
    }

    this._coinStatusDrawer = {
      byte: 1,
      value: '0'
    }

    /**
     * 硬貨部収納状態
     * 0:エンプティ
     * 1:ニアエンプティ
     * 2:適量
     * 3:ニアフル
     * 4:フル
     * 1 Byte
     */
    this._coinStatusJpy500 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusJpy100 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusJpy50 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusJpy10 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusJpy5 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusJpy1 = {
      byte: 1,
      value: '0'
    }

    this._coinStatusRJ = {
      byte: 1,
      value: '0'
    }

    this._coinStatusEmpty = {
      byte: 1,
      value: '0'
    }

    this._endString = {
      byte: 4,
      value: 'ffff'
    }

    Object.seal(this)
  }

  get billErrorCode () {
    return this._billErrorCode.value
  }
  set billErrorCode (string) {
    this._billErrorCode.value = string
  }

  get billMachineStatus () {
    return this._billMachineStatus.value
  }
  set billMachineStatus (string) {
    this._billMachineStatus.value = string
  }

  get billCancel () {
    return this._billCancel.value
  }
  set billCancel (string) {
    this._billCancel.value = string
  }

  get billAmountStatusJpy10000 () {
    return this._billAmountStatusJpy10000.value
  }
  set billAmountStatusJpy10000 (string) {
    this._billAmountStatusJpy10000.value = string
  }

  get billAmountStatusJpy5000 () {
    return this._billAmountStatusJpy5000.value
  }
  set billAmountStatusJpy5000 (string) {
    this._billAmountStatusJpy5000.value = string
  }

  get billAmountStatusJpy2000 () {
    return this._billAmountStatusJpy2000.value
  }
  set billAmountStatusJpy2000 (string) {
    this._billAmountStatusJpy2000.value = string
  }

  get billAmountStatusJpy1000 () {
    return this._billAmountStatusJpy1000.value
  }
  set billAmountStatusJpy1000 (string) {
    this._billAmountStatusJpy1000.value = string
  }

  get billAmountStatusCassette () {
    return this._billAmountStatusCassette.value
  }
  set billAmountStatusCassette (string) {
    this._billAmountStatusCassette.value = string
  }

  get billAmountStatusRJ () {
    return this._billAmountStatusRJ.value
  }
  set billAmountStatusRJ (string) {
    this._billAmountStatusRJ.value = string
  }

  get billResidenceCancelJpy5000Over () {
    return this._billResidenceCancelJpy5000Over.value
  }
  set billResidenceCancelJpy5000Over (string) {
    this._billResidenceCancelJpy5000Over.value = string
  }

  get billResidenceCancelJpy1000Over () {
    return this._billResidenceCancelJpy1000Over.value
  }
  set billResidenceCancelJpy1000Over (string) {
    this._billResidenceCancelJpy1000Over.value = string
  }

  get billResidenceCancelCassette () {
    return this._billResidenceCancelCassette.value
  }
  set billResidenceCancelCassette (string) {
    this._billResidenceCancelCassette.value = string
  }

  get billResidenceCancelUnuse () {
    return this._billResidenceCancelUnuse.value
  }
  set billResidenceCancelUnuse (string) {
    this._billResidenceCancelUnuse.value = string
  }

  get billprocessStatus () {
    return this._billprocessStatus.value
  }
  set billprocessStatus (string) {
    this._billprocessStatus.value = string
  }

  get billResidenceStatusFlg () {
    return this._billResidenceStatusFlg.value
  }
  set billResidenceStatusFlg (string) {
    this._billResidenceStatusFlg.value = string
  }

  get billCoinStatusFlg () {
    return this._billCoinStatusFlg.value
  }
  set billCoinStatusFlg (string) {
    this._billCoinStatusFlg.value = string
  }

  get billLocalOperationMode () {
    return this._billLocalOperationMode.value
  }
  set billLocalOperationMode (string) {
    this._billLocalOperationMode.value.value = string
  }

  get billPathInfo () {
    return this._billPathInfo.value
  }
  set billPathInfo (string) {
    this._billPathInfo.value = string
  }

  get billErrorMoney () {
    return this._billErrorMoney.value
  }
  set billErrorMoney (string) {
    this._billErrorMoney.value = string
  }

  get coinErrorMoney () {
    return this._coinErrorMoney.value
  }
  set coinErrorMoney (string) {
    this._coinErrorMoney.value = string
  }

  get coinResidenceCancelJPY100Over () {
    return this._coinResidenceCancelJPY100Over.value
  }
  set coinResidenceCancelJPY100Over (string) {
    this._coinResidenceCancelJPY100Over.value = string
  }

  get coinResidenceCancelJPY10Over () {
    return this._coinResidenceCancelJPY10Over.value
  }
  set coinResidenceCancelJPY10Over (string) {
    this._coinResidenceCancelJPY10Over.value = string
  }

  get coinResidenceCancelJPY1Over () {
    return this._coinResidenceCancelJPY1Over.value
  }
  set coinResidenceCancelJPY1Over (string) {
    this._coinResidenceCancelJPY1Over.value = string
  }

  get coinResidenceCancelJPYUnuse () {
    return this._coinResidenceCancelJPYUnuse.value
  }
  set coinResidenceCancelJPYUnuse (string) {
    this._coinResidenceCancelJPYUnuse.value = string
  }

  get coinPathInfo () {
    return this._coinPathInfo.value
  }
  set coinPathInfo (value) {
    this._coinPathInfo.value = value
  }

  get coinLocalOperationMode () {
    return this._coinLocalOperationMode.value
  }
  set coinLocalOperationMode (string) {
    this._coinLocalOperationMode.value = string
  }

  get coinMachineStatus () {
    return this._coinMachineStatus.value
  }
  set coinMachineStatus (string) {
    this._coinMachineStatus.value = string
  }

  get coinErrorCode () {
    return this._coinErrorCode.value
  }
  set coinErrorCode (string) {
    this._coinErrorCode.value = string
  }

  get coinCancel () {
    return this._coinCancel.value
  }
  set coinCancel (string) {
    this._coinCancel.value = string
  }

  get coinResidenceStatusFlg () {
    return this._coinResidenceStatusFlg.value
  }
  set coinResidenceStatusFlg (string) {
    this._coinResidenceStatusFlg.value = string
  }

  get coinWrappedStatusJpy100Over () {
    return this._coinWrappedStatusJpy100Over.value
  }
  set coinWrappedStatusJpy100Over (string) {
    this._coinWrappedStatusJpy100Over.value = string
  }

  get coinWrappedStatusJpy10Over () {
    return this._coinWrappedStatusJpy10Over.value
  }
  set coinWrappedStatusJpy10Over (string) {
    this._coinWrappedStatusJpy10Over.value = string
  }

  get coinWrappedStatusJpy1Over () {
    return this._coinWrappedStatusJpy1Over.value
  }
  set coinWrappedStatusJpy1Over (string) {
    this._coinWrappedStatusJpy1Over.value = string
  }

  get coinStatusDrawer () {
    return this._coinStatusDrawer.value
  }
  set coinStatusDrawer (string) {
    this._coinStatusDrawer.value = string
  }

  get coinStatusJpy500 () {
    return this._coinStatusJpy500.value
  }
  set coinStatusJpy500 (string) {
    this._coinStatusJpy500.value = string
  }

  get coinStatusJpy100 () {
    return this._coinStatusJpy100.value
  }
  set coinStatusJpy100 (string) {
    this._coinStatusJpy100.value = string
  }

  get coinStatusJpy50 () {
    return this._coinStatusJpy50.value
  }
  set coinStatusJpy50 (string) {
    this._coinStatusJpy50.value = string
  }

  get coinStatusJpy10 () {
    return this._coinStatusJpy10.value
  }
  set coinStatusJpy10 (string) {
    this._coinStatusJpy10.value = string
  }

  get coinStatusJpy5 () {
    return this._coinStatusJpy5.value
  }
  set coinStatusJpy5 (string) {
    this._coinStatusJpy5.value = string
  }

  get coinStatusJpy1 () {
    return this._coinStatusJpy1.value
  }
  set coinStatusJpy1 (string) {
    this._coinStatusJpy1.value = string
  }

  get coinStatusRJ () {
    return this._coinStatusRJ.value
  }
  set coinStatusRJ (string) {
    this._coinStatusRJ.value = string
  }

  get coinStatusEmpty () {
    return this._coinStatusJpy10.value
  }
  set coinStatusEmpty (string) {
    this._coinStatusJpy10.value = string
  }

  toString () {
    const valueArray = Object.entries(this)

    const dataArray = []
    valueArray.forEach((element) => {
      dataArray.push(element.pop().value)
    })

    return dataArray.join('')
  }

  existEmptyMoney () {
    let result = false

    const moneyStatusArray = [
      this.billAmountStatusJpy1000,
      this.coinStatusJpy500,
      this.coinStatusJpy100,
      this.coinStatusJpy50,
      this.coinStatusJpy10,
      this.coinStatusJpy5,
      this.coinStatusJpy1
    ]

    moneyStatusArray.forEach((element) => {
      // TODO: element === '1' (まもなく空状態)のチェックも追加する。
      if (element === '0' || element === '4') {
        result = true
      }
    })

    return result
  }
}
