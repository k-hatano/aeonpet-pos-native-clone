import { waitAsync } from 'common/utils/waitAsync'

export default class SampleEJournalRepository {
  _eJournals = []

  async fetchByOrderNumber (posOrderNumber, receiptType) {
    return this._eJournals.filter(eJournal => (eJournal.pos_order_number === posOrderNumber && eJournal.receipt_type === receiptType))
  }

  async fetchByClientCreatedAt (fromDate, toDate, receiptType) {
    return this._eJournals.filter(eJournal => (eJournal.receipt_type === receiptType && eJournal.client_created_at >= fromDate && eJournal.client_created_at <= toDate))
  }

  async findByOrderNumber (posOrderNumber, receiptType) {
    return this._eJournals.filter(eJournal => (eJournal.pos_order_number === posOrderNumber && eJournal.receipt_type === receiptType))
  }

  async findByIsPrinted (isPrinted) {
    return this._eJournals.filter(eJournal => (eJournal.isPrinted === isPrinted))
  }

  async saveIsPrintedAll () {
    console.log('ejournal is printed. id')
    await waitAsync(30)
  }

  async findUnpushedEJournals () {
    return [
      {
        id: 1,
        client_created_at: 1504593787
      },
      {
        id: 2,
        client_created_at: 1504593789
      }
    ]
  }

  async save (ejournal) {
    console.log('save ejournal', ejournal)
    this._eJournals.push(ejournal)
    await waitAsync(30)
  }

  async pushById (eJournalId, isErrorAlert = false) {
    console.log('push ejournal. id = ', eJournalId)
    await waitAsync(200)
  }

  async saveIsPrintedById (eJournalId) {
    console.log('ejournal is printed. id = ', eJournalId)
    await waitAsync(30)
  }

  async sendUnpushEjournals () {
    console.log('push all unpushed ejournals')
  }

  async clearBeforeDate (timestamp) {
    console.log('Delete pushed ejournals')
  }

  async saveIsNotPrintedById (id) {
    console.log('save completed.')
  }
}
