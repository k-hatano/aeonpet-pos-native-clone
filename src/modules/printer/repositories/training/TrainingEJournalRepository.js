import * as _ from 'underscore'
import { RECEIPT_TYPE } from '../../models'

export default class TrainingEJournalRepository {
  _trainingEjournals = []

  async fetchByOrderNumber (posOrderNumber, receiptType) {
    return []
  }

  async fetchByClientCreatedAt (fromDate, toDate, receiptType) {
    return []
  }

  async findByOrderNumber (posOrderNumber, receiptType) {
    if (receiptType === RECEIPT_TYPE.BILL) {
      return []
    } else {
      return this._trainingEjournals.filter(ejournal => ejournal.pos_order_number === posOrderNumber && ejournal.receipt_type === receiptType)
    }
  }

  async save (ejournal) {
    this._trainingEjournals.push(ejournal)
  }

  async saveIsPrintedById (eJournalId) {
    this._trainingEjournals.forEach(ejournal => {
      if (ejournal.id === eJournalId) {
        ejournal.is_printed = 1
      }
    })
  }

  async pushById (eJournalId, isErrorAlert = false) {
    this._trainingEjournals.forEach(ejournal => {
      if (ejournal.id === eJournalId) {
        ejournal.is_pushed = 1
      }
    })
  }

  async findByIsPrinted (isPrinted) {
    const eJournals = this._trainingEjournals.filter(ejournal => ejournal.is_printed === isPrinted)
    return _.sortBy(eJournals, item => -item.client_created_at)
  }

  async saveIsPrintedAll () {
    this._trainingEjournals.forEach(ejournal => {
      if (ejournal.is_printed === 0) {
        ejournal.is_printed = 1
      }
    })
  }

  async findUnpushedEJournals () {
    return []
  }

  async saveIsNotPrintedById (id) {
    console.log('save completed.')
  }
}
