import logger from 'common/utils/logger'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import EJournalEntity from '../entities/EjournalEntity'
import CashierRepository from 'modules/cashier/repositories/CashierRepository'
import fetcher from 'common/models/fetcher'
import { handleAxiosError, handleDatabaseError, REQUEST_ERROR_CODE } from '../../../../common/errors'
import { OPERATION_TYPE } from 'modules/home/models'
import { saveOperationLog } from 'modules/home/service'
import NetworkError from '../../../../common/errors/NetworkError'
import DbError from '../../../../common/errors/DbError'

export default class StandardEJournalRepository {
  async fetchByOrderNumber (posOrderNumber, receiptType) {
    const searchParams = {
      pos_order_number: posOrderNumber,
      receipt_type: receiptType
    }
    const response = await fetcher.get('ejournals', encodeCriteria(searchParams))
    return response.data
  }

  async fetchByClientCreatedAt (fromDate, toDate, receiptType) {
    const cashier = await CashierRepository.find()
    const searchParams = {
      cashier_id: cashier.id,
      receipt_type: receiptType,
      client_created_at__gte: fromDate,
      client_created_at__lt: toDate
    }
    return fetcher.get('ejournals', encodeCriteria(searchParams))
  }

  async findByOrderNumber (posOrderNumber, receiptType) {
    const query = {
      pos_order_number: posOrderNumber,
      receipt_type: receiptType
    }
    return EJournalEntity.findAll({
      where: query,
      raw: true
    })
  }

  async findUnpushedEJournals () {
    const ejournals = EJournalEntity.findAll({
      where: {
        is_pushed: false
      },
      raw: true
    })
    return ejournals
  }

  async save (ejournal, transaction) {
    const option = transaction ? { transaction } : {}
    const isExistData = await EJournalEntity.find({
      where: {
        id: ejournal.id
      }
    })
    if (isExistData) {
      return EJournalEntity.update(ejournal, {
        where: {
          id: ejournal.id
        },
        ...option
      })
    } else {
      return EJournalEntity.create(ejournal, option)
    }
  }

  async saveIsPrintedById (eJournalId) {
    const eJournal = await EJournalEntity.find({
      where: {
        id: eJournalId
      },
      raw: true
    })

    if (eJournal) {
      eJournal.is_printed = 1
      EJournalEntity.update(eJournal, {
        where: {
          id: eJournalId
        }
      })
    }
  }

  async pushById (eJournalId, isErrorAlert = false, disableOperationLog = false) {
    const eJournal = await EJournalEntity.find({
      where: {
        id: eJournalId
      },
      raw: true
    })
    let isPushed = false
    try {
      await fetcher.post('ejournals', eJournal)
      isPushed = true
    } catch (error) {
      if (this._isDuplicatedError(error)) {
        // 重複データである = 既に送信済みでローカルが認識していなかっただけなので、成功として扱う
        isPushed = true
        logger.warning('Duplicated EJournal ' + JSON.stringify(eJournal))
      } else {
        try {
          handleAxiosError(error)
        } catch (error2) {
          if (error2 instanceof NetworkError) {
            if (error2.type === NetworkError.TYPE.SERVER_ERROR) {
              // サーバに異常がある状態のため、調査用に詳細を記録しておく
              logger.error(
                'StandardEJournalRepository.pushById Server Error ' +
                JSON.stringify(error.response) + ' ' +
                JSON.stringify(eJournal)
              )
            } else if (error2.type === NetworkError.TYPE.CLIENT_ERROR) {
              // 想定外の処理をクライアントが行った可能性が高いため、調査用に詳細を記録しておく
              logger.error(
                'StandardEJournalRepository.pushById Client Error ' +
                JSON.stringify(error.response) + ' ' +
                JSON.stringify(eJournal)
              )
            }
          }
          if (!disableOperationLog) {
            await saveOperationLog(OPERATION_TYPE.UNSET_ITEM, new Date())
          }
          if (isErrorAlert) {
            throw error2
          }
        }
      }
    }

    if (isPushed) {
      try {
        await EJournalEntity.update(
          { is_pushed: 1 },
          { where: { id: eJournalId } })
      } catch (error) {
        throw new DbError(error)
      }
    }
  }

  async findByIsPrinted (isPrinted) {
    return EJournalEntity.findAll({
      where: {
        is_printed: isPrinted
      },
      order: [
        ['client_created_at', 'DESC']
      ]
    })
  }

  async saveIsPrintedAll () {
    return EJournalEntity.update({
      is_printed: 1
    },
    {where: {}}
    )
  }

  async findAll (query) {
    return EJournalEntity.findAll(query)
  }

  _isDuplicatedError (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.DUPLICATED_ENTRY
  }

  async sendUnpushEjournals () {
    let ejournals
    try {
      ejournals = await this.findUnpushedEJournals()
    } catch (error) {
      console.error('Unexpected Error', error)
    }

    const errors = []
    const ejournalResults = []
    for (const i in ejournals) {
      try {
        const ejournal = ejournals[i]
        const result = await this.pushById(ejournal.id, true, true)
        ejournalResults.push(result)
      } catch (error) {
        errors.push(error)
        if (error instanceof NetworkError &&
          (error.type === NetworkError.TYPE.TIMEOUT || error.type === NetworkError.TYPE.OFFLINE)) {
          // タイムアウトとオフラインの場合、次を送信できる見込みがほとんどないので、この時点で終了する
          break
        }
      }
    }

    return { errors, ejournals: ejournalResults }
  }

  async clearBeforeDate (timestamp) {
    try {
      await EJournalEntity.destroy(
        {
          where: {
            is_pushed: true,
            is_printed: true,
            client_created_at: {
              $lte: timestamp
            }
          }
        }
      )
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async saveIsPushedAll () {
    try {
      await EJournalEntity.update({ is_pushed: true }, { where: {} })
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async saveIsNotPrintedById (id) {
    try {
        await EJournalEntity.update(
          { is_printed: 0 },
          { where: { id: id } })
      } catch (error) {
        throw new DbError(error)
      }
  }
}
