import OrderActionButton from '../components/OrderActionButton'
import connectPermissionButton from '../../staff/containers/connectPermissionButton'

export default connectPermissionButton(OrderActionButton)
