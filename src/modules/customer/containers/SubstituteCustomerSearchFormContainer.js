import { connect } from 'react-redux'
import SubstituteCustomerSearchForm from '../components/SubstituteCustomerSearchForm'
import { listCustomers, selectCustomer, listCustomerOrders, setMemoText } from '../actions'
import CustomerRepository from '../repositories/CustomerRepository'
import Modal from 'common/components/widgets/Modal'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from 'common/sideEffects'
import SettingKeys from 'modules/setting/models/SettingKeys'
import I18n from 'i18n-js'
import { getCommonNetworkErrorMessage } from 'common/errors'

const mapDispatchToProps = dispatch => ({
  onFindCustomers: async (firstNameKana, phoneNumber) => {
    dispatch(selectCustomer(null))
    dispatch(setMemoText(null))
    dispatch(listCustomerOrders([]))
    try {
      const customers = await loading(dispatch, async () => {
        return CustomerRepository.fetchCustomers(firstNameKana, phoneNumber)
      })
      if (customers.length > 0) {
        dispatch(listCustomers(customers))
        Modal.close('forgotCardModal')
      } else {
        dispatch(listCustomers([]))
        Modal.close('forgotCardModal')
        AlertView.show(I18n.t('message.C-01-E001'))
      }
    } catch (error) {
      dispatch(listCustomers([]))
      Modal.close('forgotCardModal')
      const message = getCommonNetworkErrorMessage(error, I18n.t('message.C-01-E003', {message: error.message}))
      AlertView.show(message)
    }
  }
})

const mapStateToProps = state => ({
  currentStaff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF]
})

export default connect(mapStateToProps, mapDispatchToProps)(SubstituteCustomerSearchForm)
