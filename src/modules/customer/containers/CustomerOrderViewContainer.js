import { connect } from 'react-redux'
import CustomerOrderView from '../components/CustomerOrderView'
import { selectOrder, selectCustomer, setIsEditable, setMemoText } from '../actions'
import { MODULE_NAME } from '../models'
import CustomerRepository from '../repositories/CustomerRepository'
import OrderRepository from 'modules/order/repositories/OrderRepository'
import AlertView from 'common/components/widgets/AlertView'
import NetworkError from '../../../common/errors/NetworkError'
import { getCommonNetworkErrorMessage } from '../../../common/errors'
import { loading } from '../../../common/sideEffects'
import I18n from 'i18n-js'

const mapDispatchToProps = dispatch => ({
  onSelectOrder: async (selectedOrder) => {
    try {
      const order = await OrderRepository.fetchByOrderId(selectedOrder.id)
      dispatch(selectOrder(order))
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : error.message
      AlertView.show(message)
    }
  },
  onResetOrder: async () => {
    dispatch(selectOrder(null))
  },
  onSaveMemo: async (newMemo, customer) => {
    return await loading(dispatch, async () => {
      try {
        const response = await CustomerRepository.updateCustomerMemo(newMemo, customer.id)
        if (response) {
          dispatch(selectCustomer({...customer, memo: newMemo}))
          AlertView.show(I18n.t('message.C-03-I002'))
          return true
        }
      } catch (error) {
        const message = getCommonNetworkErrorMessage(error)
        AlertView.show(message)
      }
      return false
    })
  },
  onSetIsEditable: async (isEditable) => {
    dispatch(setIsEditable(isEditable))
  },
  onChangeMemoText: async (memoText) => {
    dispatch(setMemoText(memoText))
  }
})

const mapStateToProps = state => ({
  customer: state[MODULE_NAME].selectedCustomer,
  orders: state[MODULE_NAME].orders,
  order: state[MODULE_NAME].order,
  isEditable: state[MODULE_NAME].isEditable,
  memoText: state[MODULE_NAME].memoText
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerOrderView)
