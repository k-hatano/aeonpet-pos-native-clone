import { connect } from 'react-redux'
import CustomerCodeChange from '../components/CustomerCodeChange'
import { selectCustomer, setIsEditable, setMemoText } from '../actions'
import CustomerRepository from '../repositories/CustomerRepository'
import Modal from 'common/components/widgets/Modal'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from 'common/sideEffects'
import SettingKeys from 'modules/setting/models/SettingKeys'
import I18n from 'i18n-js'
import { isHalfWidthAndSymbol } from '../../../common/utils/validations.js'
import MessageError from '../../../common/errors/MessageError'

const mapDispatchToProps = dispatch => ({
  onUpdateCustomerCode: async (customerCode, customer) => {
    if (!isHalfWidthAndSymbol(customerCode)) {
      Modal.close()
      AlertView.show(I18n.t('message.D-01-E002', {
        item: I18n.t('customer.customer_code'),
        format: I18n.t('validation.halfWidthAndSymbol')
      }))
    }

    try {
      await loading(dispatch, async () => CustomerRepository.updateCustomerCode(customerCode, customer.id))
      dispatch(selectCustomer({...customer, customer_code: customerCode}))
      dispatch(setIsEditable(false))
      dispatch(setMemoText(customer.memo))
      Modal.close()
      AlertView.show(I18n.t('message.C-03-I003'))
    } catch (error) {
      dispatch(selectCustomer(null))
      Modal.close()
      AlertView.show(error.message)
    }
  }
})

const mapStateToProps = state => ({
  currentStaff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  customer: state.customer.selectedCustomer
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onUpdateCustomerCode: async (customerCode) => {
    dispatchProps.onUpdateCustomerCode(
      customerCode,
      stateProps.customer)
  }
})

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CustomerCodeChange)
