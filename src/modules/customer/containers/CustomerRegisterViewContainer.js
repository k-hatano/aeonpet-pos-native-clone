import { connect } from 'react-redux'
import { MODULE_NAME, TEL_WHOLE_LENGTH, ZIPCODE1_LENGTH, ZIPCODE2_LENGTH } from '../models'
import CustomerRegisterView from '../components/CustomerRegisterView'
import AlertView from 'common/components/widgets/AlertView'
import * as actions from '../actions'
import I18n from 'i18n-js'
import { loading } from 'common/sideEffects'
import CustomerRepository from '../repositories/CustomerRepository'
import RegionRepository from '../../regions/repositories/RegionRepository'
import ZipAddressRepository from '../../zipAddress/repositories/ZipAddressRepository'
import { Actions } from 'react-native-router-flux'
import NetworkError from 'common/errors/NetworkError'
import Modal from 'common/components/widgets/Modal'
import { getCommonNetworkErrorMessage } from 'common/errors'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    dispatch(actions.resetRegisterCustomer())
    const regions = await RegionRepository.findAll()
    dispatch(actions.listRegions(regions))
  },
  onChangeForm: async (key, value) => {
    const changedValue = {}
    changedValue[key] = value
    dispatch(actions.updateRegisterCustomer(changedValue))
  },
  onInputAddress: async (zip01, zip02, regions) => {
    await loading(dispatch, async () => {
      try {
        const zipCode = zip01 + zip02
        const zipAddress = await ZipAddressRepository.fetchByZipCode(zipCode)
        if (zipAddress.length === 0) {
          AlertView.show(I18n.t('message.D-01-E001'))
          return
        }
        const region = regions.find(item => item.name === zipAddress[0].city)
        const pref = region ? region.region_code : null
        dispatch(actions.updateRegisterCustomer({'city': zipAddress[0].city}))
        dispatch(actions.updateRegisterCustomer({'locality': zipAddress[0].locality}))
        dispatch(actions.updateRegisterCustomer({'pref': pref}))
      } catch (error) {
        AlertView.show(getCommonNetworkErrorMessage(error))
      }
    })
  },
  onSubmit: async (values) => {
    await loading(dispatch, async () => {
      let message = null
      if (!values.customer_code.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_code'), format: I18n.t('validation.halfWidthAndSymbol')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_code'), format: I18n.t('validation.halfWidthAndSymbol')})
      }
      if (!values.last_name_kana.match(/^[\u30a0-\u30ff]+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.name_kana') + '_' + I18n.t('customer.last_name_kana'), format: I18n.t('validation.kana')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.name_kana') + '_' + I18n.t('customer.last_name_kana'), format: I18n.t('validation.kana')})
      }
      if (!values.first_name_kana.match(/^[\u30a0-\u30ff]+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.name_kana') + '_' + I18n.t('customer.first_name_kana'), format: I18n.t('validation.kana')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.name_kana') + '_' + I18n.t('customer.first_name_kana'), format: I18n.t('validation.kana')})
      }
      if (values.zip01 && !values.zip01.match(/^\d+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '01', format: I18n.t('validation.halfWidthNumeric')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '01', format: I18n.t('validation.halfWidthNumeric')})
      }
      if (values.zip02 && !values.zip02.match(/^\d+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '02', format: I18n.t('validation.halfWidthNumeric')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '02', format: I18n.t('validation.halfWidthNumeric')})
      }
      if (!values.tel01.match(/^\d+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '01', format: I18n.t('validation.halfWidthNumeric')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '01', format: I18n.t('validation.halfWidthNumeric')})
      }
      if (!values.tel02.match(/^\d+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '02', format: I18n.t('validation.halfWidthNumeric')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '02', format: I18n.t('validation.halfWidthNumeric')})
      }
      if (!values.tel03.match(/^\d+$/)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '03', format: I18n.t('validation.halfWidthNumeric')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number') + '03', format: I18n.t('validation.halfWidthNumeric')})
      }
      if (values.tel01 && values.tel02 && values.tel03 && values.tel01.length + values.tel02.length + values.tel03.length > TEL_WHOLE_LENGTH) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number'), format: I18n.t('validation.within', { attribute: TEL_WHOLE_LENGTH })})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_phone_number'), format: I18n.t('validation.within', { attribute: TEL_WHOLE_LENGTH })})
      }
      if (values.zip01 && values.zip01.length !== ZIPCODE1_LENGTH) {
        message = message
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '1', format: I18n.t('validation.length', { attribute: ZIPCODE1_LENGTH })})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '1', format: I18n.t('validation.length', { attribute: ZIPCODE1_LENGTH })})
      }
      if (values.zip02 && values.zip02.length !== ZIPCODE2_LENGTH) {
        message = message
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '2', format: I18n.t('validation.length', { attribute: ZIPCODE2_LENGTH })})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.customer_postal_code') + '2', format: I18n.t('validation.length', { attribute: ZIPCODE2_LENGTH })})
      }
      if (values.email && !_checkEmail(values.email)) {
        message = message 
          ? message + '\n' + I18n.t('message.D-01-E002', { item: I18n.t('customer.mailaddress'), format: I18n.t('validation.email')})
          : I18n.t('message.D-01-E002', { item: I18n.t('customer.mailaddress'), format: I18n.t('validation.email')})
      }

      if (message) {
        AlertView.show(message)
        return
      }

      try {
        const customerByCustomerCode = await CustomerRepository.fetchByCustomerCode(values.customer_code)
        if (customerByCustomerCode) {
          AlertView.show(I18n.t('message.D-01-E003'))
          return
        }
        await CustomerRepository.push(values)
        AlertView.show(I18n.t('message.D-01-I001'), () => {
          Modal.close()
          Actions.redirectPage({redirectTo: 'home'})
        })
      } catch (error) {
        let message
        if (error.type === NetworkError.TYPE.OFFLINE) {
          message = I18n.t('message.A-01-E100')
        } else {
          message = error.message
        }
        AlertView.show(message)
      }
    })
  }
})

const _checkEmail = (email) => {
  // 半角英数字記号であること
  if (!email.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/)) {
    return false
  }
  // 1文字目が記号でないこと
  if (email.slice(0,1).match(/^[!-/:-@¥[-`{-~]+$/)) {
    return false
  }
  // 末尾が"."でないこと
  if (email.slice(-1) === '.') {
    return false
  }
  const divided = email.split('@')
  // @が1つだけであること
  if (divided.length !== 2) {
    return false
  }
  // @の前に1文字以上含まれていること
  if (!divided[0]) {
    return false
  }
  // @の後に1文字以上含まれていること
  if (!divided[1]) {
    return false
  }
  // @の後の1文字目が"."でないこと
  if (divided[1].slice(0,1) === '.') {
    return false
  }
  // @の後に"."を含むこと
  if (divided[1].indexOf('.') === -1) {
    return false
  }

  return true
}

const mapStateToProps = state => ({
  registerCustomer: state[MODULE_NAME].registerCustomer,
  regions: state[MODULE_NAME].regions
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerRegisterView)
