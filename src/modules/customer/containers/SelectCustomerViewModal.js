import { connect } from 'react-redux'
import SelectCustomerViewModal from '../components/SelectCustomerViewModal'
import { listCustomers, useCardNumber, setCustomerCode, setPasoriStatus } from '../actions'
import { setCartCustomer } from 'modules/cart/actions'
import { MODULE_NAME } from '../models'
import AlertView from '../../../common/components/widgets/AlertView'
import CustomerRepository from '../repositories/CustomerRepository'
import Modal from 'common/components/widgets/Modal'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { loading } from '../../../common/sideEffects'
import I18n from 'i18n-js'
import * as _ from 'underscore'
import NetworkError from 'common/errors/NetworkError'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    dispatch(listCustomers([]))
  },
  onFindByCode: async (customerCode) => {
    if (!customerCode) {
      return null
    }
    if (!customerCode.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/)) {
      AlertView.show(I18n.t('message.C-01-E002', {
        item: I18n.t('customer.customer_code'),
        format: I18n.t('validation.halfWidthAndSymbol')
      }))
      return null
    }

    try {
      const customer = await loading(dispatch, async () => {
        return CustomerRepository.fetchByCustomerCode(customerCode)
      })
      if (customer) {
        dispatch(setCartCustomer(customer))
        return customer
      } else {
        AlertView.show(I18n.t('message.C-01-E001'))
        return null
      }
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.C-01-E003', {message: error.message})
      AlertView.show(message)
      return null
    }
  },
  onFindByText: async (params) => {
    try {
      const customers = await loading(dispatch, async () => {
        return CustomerRepository.fetchCustomers(params.customerNamePhon, params.customerPhone)
      })
      if (customers.length > 0) {
        dispatch(listCustomers(_.sortBy(customers, member => member.customer_code)))
        return null
      } else {
        dispatch(listCustomers([]))
        return null
      }
    } catch (error) {
      const message = error.type === NetworkError.TYPE.OFFLINE ? I18n.t('message.A-01-E100') : I18n.t('message.C-01-E003', {message: error.message})
      AlertView.show(message)
      return null
    }
  },
  onSelectCustomer: async (customer) => {
    dispatch(setCartCustomer(customer))
    return customer
  },
  onDispose: async () => {
    dispatch(listCustomers([]))
    dispatch(setCustomerCode(''))
  },
  onUseCardNumber: async (useCardNumberFlg) => {
    dispatch(useCardNumber(useCardNumberFlg))
  },
  onChangeCustomerCode: async (customerCode) => {
    dispatch(setCustomerCode(customerCode))
  },
  onChangePasoriStatus: async (pasoriStatus) => {
    dispatch(setPasoriStatus(pasoriStatus))
  }
})

const mapStateToProps = state => ({
  customers: state[MODULE_NAME].customers,
  useCardNumberFlg: state[MODULE_NAME].useCardNumberFlg,
  currentStaff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  customerCode: state[MODULE_NAME].customerCode,
  pasoriStatus: state[MODULE_NAME].pasoriStatus
})

const selectCustomerViewModel = connect(mapStateToProps, mapDispatchToProps)(SelectCustomerViewModal)
export default selectCustomerViewModel

export const showSelectCustomerViewModal = (onSelected) => {
  Modal.open(selectCustomerViewModel, {
    isBackgroundVisible: true,
    enableBackgroundClose: false,
    props: {
      onClose: async (customer) => {
        await onSelected(customer)
        Modal.close()
      }
    }
  })
}
