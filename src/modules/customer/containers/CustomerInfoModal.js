import { connect } from 'react-redux'
import CustomerInfoModalComponent from '../components/CustomerInfoModal'
import { listCustomerOrders, selectOrder } from '../actions'
import Modal from 'common/components/widgets/Modal'
import { MODULE_NAME as CART_MODULE_NAME } from 'modules/cart/models'
import { loading } from 'common/sideEffects'
import CustomerRepository from '../repositories/CustomerRepository'

const mapDispatchToProps = dispatch => ({
  onInit: async (customer) => {
    try {
      await loading(dispatch, async () => {
        if (customer) {
          const orders = await CustomerRepository.fetchCustomerOrders(customer.customer_code)
          dispatch(listCustomerOrders(orders))
        }
      })
    } catch (error) {
      dispatch(listCustomerOrders([]))
    }
  },
  onClearCustomerOrder: async () => {
    dispatch(listCustomerOrders([]))
    dispatch(selectOrder(null))
  }
})

const mapStateToProps = state => ({
  customer: state[CART_MODULE_NAME].cart.customer
})

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onInit: async () => {
    return dispatchProps.onInit(stateProps.customer)
  }
})

const CustomerInfoModal =
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(CustomerInfoModalComponent)
export default CustomerInfoModal

export const showCustomerInfoModal = () => {
  Modal.open(CustomerInfoModal, {
    isBackgroundVisible: true,
    enableBackgroundClose: false,
    props: {
      onClose: () => {
        Modal.close()
      }
    }
  })
}
