import { connect } from 'react-redux'
import CustomerSearchForm from '../components/CustomerSearchForm'
import { selectCustomer, listCustomerOrders, listCustomers, setShopName, setMemoText } from '../actions'
import { MODULE_NAME } from '../models'
import CustomerRepository from '../repositories/CustomerRepository'
import AlertView from 'common/components/widgets/AlertView'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { loading } from 'common/sideEffects'
import { MODULE_NAME as staffModuleName } from '../../staff/models'
import I18n from 'i18n-js'
import NetworkError from '../../../common/errors/NetworkError'
import ShopRepository from 'modules/shop/repositories/ShopRepository'
import { getCommonNetworkErrorMessage } from 'common/errors'

const mapDispatchToProps = dispatch => ({
  onSelectCustomer: async (selectedCustomer) => {
    try {
      await loading(dispatch, async () => {
        const customer = await CustomerRepository.fetchByCustomerCode(selectedCustomer.customer_code)
        if (customer) {
          dispatch(selectCustomer(customer))
          dispatch(setMemoText(customer.memo))
          const orders = await CustomerRepository.fetchCustomerOrders(customer.id)
          dispatch(listCustomerOrders(orders))
          if (customer.shop_id && customer.shop_id != 0) {
            const shop = await ShopRepository.fetchById(customer.shop_id)
            dispatch(setShopName(shop.name))
          }
        } else {
          dispatch(selectCustomer(null))
          dispatch(setMemoText(null))
          dispatch(listCustomerOrders([]))
          dispatch(setShopName(null))
        }
      })
    } catch (error) {
      const message = getCommonNetworkErrorMessage(error, I18n.t('message.C-01-E003', {message: error.message}))
      AlertView.show(message)
      dispatch(selectCustomer(null))
      dispatch(setMemoText(null))
      dispatch(listCustomerOrders([]))
    }
  },
  onEnterCustomerCode: async (selectedCustomerCode) => {
    try {
      await loading(dispatch, async () => {
        const customer = await CustomerRepository.fetchByCustomerCode(selectedCustomerCode)
        if (customer) {
          dispatch(selectCustomer(customer))
          dispatch(setMemoText(customer.memo))
          const orders = await CustomerRepository.fetchCustomerOrders(customer.id)
          dispatch(listCustomerOrders(orders))
          if (customer.shop_id && customer.shop_id != 0) {
            const shop = await ShopRepository.fetchById(customer.shop_id)
            dispatch(setShopName(shop.name))
          }
        } else {
          AlertView.show(I18n.t('message.C-01-E001'))
          dispatch(selectCustomer(null))
          dispatch(setMemoText(null))
          dispatch(listCustomerOrders([]))
          dispatch(listCustomers([]))
          dispatch(setShopName(null))
        }
      })
    } catch (error) {
      const message = getCommonNetworkErrorMessage(error, I18n.t('message.C-01-E003', {message: error.message}))
      AlertView.show(message)
      dispatch(selectCustomer(null))
      dispatch(setMemoText(null))
      dispatch(listCustomerOrders([]))
      dispatch(listCustomers([]))
    }
  }
})

const mapStateToProps = state => ({
  customers: state[MODULE_NAME].customers,
  customer: state[MODULE_NAME].selectedCustomer,
  currentStaff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  permissions: state[staffModuleName].permissions,
  shopName: state[MODULE_NAME].shopName,
  isEditable: state[MODULE_NAME].isEditable
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerSearchForm)
