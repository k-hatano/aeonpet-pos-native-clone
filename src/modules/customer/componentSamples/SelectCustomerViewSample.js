import SelectCustomerView from '../components/SelectCustomerView'

export default {
  name: 'SelectCustomerView',
  component: SelectCustomerView,
  properties: [
    {
      title: 'Default',
      property: {
        customers: [
          {
            last_name_kana: 'ヤマダ',
            first_name_kana: 'タロウ',
            customer_code: 'A1234567890',
            phone_number: 'XXXX-XXXX-1234'
          },
          {
            last_name_kana: 'ヤマダ',
            first_name_kana: 'ジロウ',
            customer_code: 'A2234567890',
            phone_number: 'XXXX-XXXX-1234'
          },
          {
            last_name_kana: 'ヤマダ',
            first_name_kana: 'サブロウ',
            customer_code: 'A3234567890',
            phone_number: 'XXXX-XXXX-1234'
          }
        ],
        onFindByCode: (code) => console.log('Find By Code', code),
        onFindByText: (params) => console.log('Find By Text', params)
      }
    }
  ],
  frames: [
    {
      title: 'W686',
      style: {width: 686}
    }
  ]
}
