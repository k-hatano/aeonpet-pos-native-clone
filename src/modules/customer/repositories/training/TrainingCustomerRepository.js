import * as _ from 'underscore'

export const trainingCustomerMap = {
  training_customer1: {
    accept_dm: 1,
    accept_email: 1,
    birthday: '1974-05-15',
    city: '東京都',
    created_at: 1535554800,
    customer_activity: {
      buy_times: 0,
      buy_totals: 0,
      created_at: 1535554800,
      customer_id: '23ed3bba4533f551c9d6b1662d2678ae',
      first_buy_at: null,
      first_buy_shop_id: null,
      id: '43207734f3c3b306beeec12688ced207',
      last_buy_at: null,
      last_buy_shop_id: null,
      points: 0,
      updated_at: 1535554800
    },
    customer_code: 'test00000',
    customer_property01: null,
    customer_property02: null,
    customer_property03: null,
    customer_property04: null,
    customer_property05: null,
    deleted_at: null,
    email: 'test1@example.com',
    first_buy_shop_name: null,
    first_name: '太郎',
    first_name_kana: 'タロウ',
    gender: 1,
    id: '23ed3bba4533f551c9d6b1662d2678ae',
    last_buy_shop_name: null,
    last_name: '山田',
    last_name_kana: 'ヤマダ',
    locality: '品川区',
    memo: null,
    pref: 13,
    shop_id: '764a75e495bfb2ad660b97750df0dd40',
    street: '1-1-1',
    tel01: '000',
    tel02: '0000',
    tel03: '0000',
    updated_at: 1535554800,
    zip01: '001',
    zip02: '0001'
  },
  training_customer2: {
    accept_dm: 0,
    accept_email: 1,
    birthday: '1987-02-14',
    city: '東京都',
    created_at: 1535641200,
    customer_activity: {
      buy_times: 1,
      buy_totals: 10000,
      created_at: 1535641200,
      customer_id: '8582b5a385d9689df2f2aadd97ba2a47',
      first_buy_at: 1535727600,
      first_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      id: '95e310c0c0f5322eeb5380737010ae62',
      last_buy_at: 1535727600,
      last_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      points: 100,
      updated_at: 1535727600
    },
    customer_code: 'test00001',
    customer_property01: 'test',
    customer_property02: null,
    customer_property03: null,
    customer_property04: null,
    customer_property05: null,
    deleted_at: null,
    email: 'test2@example.com',
    first_buy_shop_name: 'SC店A',
    first_name: '太郎',
    first_name_kana: 'タロウ',
    gender: 1,
    id: '8582b5a385d9689df2f2aadd97ba2a47',
    last_buy_shop_name: 'SC店A',
    last_name: '山田',
    last_name_kana: 'ヤマダ',
    locality: '新宿',
    memo: 'memomemomemo',
    pref: 13,
    shop_id: 'b30bf864d39afcda729e145210bccd18',
    street: '2-2-2',
    tel01: '111',
    tel02: '1111',
    tel03: '1111',
    updated_at: 1535641200,
    zip01: '002',
    zip02: '0002'
  },
  training_customer3: {
    accept_dm: 0,
    accept_email: 0,
    birthday: '1995-07-17',
    city: '東京都',
    created_at: 1535814000,
    customer_activity: {
      buy_times: 3,
      buy_totals: 50000,
      created_at: 1535814000,
      customer_id: 'e7930ce70bf357287930beb014ee1c6e',
      first_buy_at: 1535900400,
      first_buy_shop_id: 'fec00d329a5dae669e04d5f1bc44c0c3',
      id: 'efd2ec00e7280e8a646632f056de7557',
      last_buy_at: 1535986800,
      last_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      points: 1000,
      updated_at: 1535986800
    },
    customer_code: 'test00003',
    customer_property01: 'prop1',
    customer_property02: 'prop2',
    customer_property03: 'prop3',
    customer_property04: 'prop4',
    customer_property05: 'prop5',
    deleted_at: null,
    email: 'test3@example.com',
    first_buy_shop_name: 'SC店B',
    first_name: '一郎',
    first_name_kana: 'イチロウ',
    gender: 1,
    id: 'e7930ce70bf357287930beb014ee1c6e',
    last_buy_shop_name: 'SC店A',
    last_name: '山口',
    last_name_kana: 'ヤマグチ',
    locality: '品川',
    memo: 'memomemomemomemomemomemomemomemomemomemomemomemo',
    pref: 13,
    shop_id: 'b30bf864d39afcda729e145210bccd18',
    street: '3-3-3',
    tel01: '222',
    tel02: '2222',
    tel03: '2222',
    updated_at: 1535814000,
    zip01: '003',
    zip02: '0003'
  }
}

export const trainingCustomers = _.values(trainingCustomerMap)

export default class TrainingCustomerRepository {


  async fetchCustomers (lastNameKana, phoneNumber) {
    try {
      // トレーニングモード時の会員検索について
      // 電話番号と姓（カナ）で検索した場合は、入力制御に引っ掛かりさえしなければ
      // 入力値がどんなものであっても3件全てを結果として出す
      return trainingCustomers
    } catch (error) {
    }
  }

  async fetchByCustomerCode (customerCode) {
    try {
      // トレーニングモード時の会員検索について
      // 会員コードで検索した場合は、入力制御に引っ掛かりさえしなければ
      // 入力値がどんなものであっても何かしらの会員を出す
      const key = Math.floor(Math.random() * 3)
      return trainingCustomers[key]
    } catch (error) {
    }
  }

  async updateCustomerCode (customerCode, id) {
    console.log('cutsomerCode update succeeded.')
  }
}
