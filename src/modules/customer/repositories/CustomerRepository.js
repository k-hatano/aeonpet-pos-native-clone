import StandardCustomerRepository from './standard/StandardCustomerRepository'
import SampleCustomerRepository from './sample/SampleCustomerRepository'
import TrainingCustomerRepository from './training/TrainingCustomerRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CustomerRepository {
  static _implement = new SampleCustomerRepository()

  static async fetchCustomers (lastNameKana, phoneNumber) {
    return this._implement.fetchCustomers(lastNameKana, phoneNumber)
  }

  static async fetchByCustomerCode (customerCode) {
    return this._implement.fetchByCustomerCode(customerCode)
  }

  static async fetchCustomerOrders (customerCode) {
    return this._implement.fetchCustomerOrders(customerCode)
  }

  static async push (customer) {
    return this._implement.push(customer)
  }

  static async updateCustomerCode (customerCode, id) {
    return this._implement.updateCustomerCode(customerCode, id)
  }

  static async updateCustomerMemo (customerMemo, id) {
    return this._implement.updateCustomerMemo(customerMemo, id)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCustomerRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingCustomerRepository()
        break

      default:
        this._implement = new SampleCustomerRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CustomerRepository', (context) => {
  CustomerRepository.switchImplement(context)
})
