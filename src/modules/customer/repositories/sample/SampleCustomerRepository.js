import * as _ from 'underscore'

export const sampleCustomerMap = {
  customer1: {
    accept_dm: 1,
    accept_email: 1,
    birthday: '1974-05-15',
    city: '東京都',
    created_at: 1535554800,
    customer_activity: {
      buy_times: 0,
      buy_totals: 0,
      created_at: 1535554800,
      customer_id: '23ed3bba4533f551c9d6b1662d2678ae',
      first_buy_at: null,
      first_buy_shop_id: null,
      id: '43207734f3c3b306beeec12688ced207',
      last_buy_at: null,
      last_buy_shop_id: null,
      points: 0,
      updated_at: 1535554800
    },
    customer_code: 'test00000',
    customer_property01: null,
    customer_property02: null,
    customer_property03: null,
    customer_property04: null,
    customer_property05: null,
    deleted_at: null,
    email: 'test1@example.com',
    first_buy_shop_name: null,
    first_name: '太郎',
    first_name_kana: 'タロウ',
    gender: 1,
    id: '23ed3bba4533f551c9d6b1662d2678ae',
    last_buy_shop_name: null,
    last_name: '山田',
    last_name_kana: 'ヤマダ',
    locality: '品川区',
    memo: null,
    pref: 13,
    shop_id: '764a75e495bfb2ad660b97750df0dd40',
    street: '1-1-1',
    tel01: '000',
    tel02: '0000',
    tel03: '0000',
    updated_at: 1535554800,
    zip01: '001',
    zip02: '0001'
  },
  customer2: {
    accept_dm: 0,
    accept_email: 1,
    birthday: '1987-02-14',
    city: '東京都',
    created_at: 1535641200,
    customer_activity: {
      buy_times: 1,
      buy_totals: 10000,
      created_at: 1535641200,
      customer_id: '8582b5a385d9689df2f2aadd97ba2a47',
      first_buy_at: 1535727600,
      first_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      id: '95e310c0c0f5322eeb5380737010ae62',
      last_buy_at: 1535727600,
      last_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      points: 100,
      updated_at: 1535727600
    },
    customer_code: 'test00001',
    customer_property01: 'test',
    customer_property02: null,
    customer_property03: null,
    customer_property04: null,
    customer_property05: null,
    deleted_at: null,
    email: 'test2@example.com',
    first_buy_shop_name: 'SC店A',
    first_name: '太郎',
    first_name_kana: 'タロウ',
    gender: 1,
    id: '8582b5a385d9689df2f2aadd97ba2a47',
    last_buy_shop_name: 'SC店A',
    last_name: '山田',
    last_name_kana: 'ヤマダ',
    locality: '新宿',
    memo: 'memomemomemo',
    pref: 13,
    shop_id: 'b30bf864d39afcda729e145210bccd18',
    street: '2-2-2',
    tel01: '111',
    tel02: '1111',
    tel03: '1111',
    updated_at: 1535641200,
    zip01: '002',
    zip02: '0002'
  },
  customer3: {
    accept_dm: 0,
    accept_email: 0,
    birthday: '1995-07-17',
    city: '東京都',
    created_at: 1535814000,
    customer_activity: {
      buy_times: 3,
      buy_totals: 50000,
      created_at: 1535814000,
      customer_id: 'e7930ce70bf357287930beb014ee1c6e',
      first_buy_at: 1535900400,
      first_buy_shop_id: 'fec00d329a5dae669e04d5f1bc44c0c3',
      id: 'efd2ec00e7280e8a646632f056de7557',
      last_buy_at: 1535986800,
      last_buy_shop_id: 'b30bf864d39afcda729e145210bccd18',
      points: 1000,
      updated_at: 1535986800
    },
    customer_code: 'test00003',
    customer_property01: 'prop1',
    customer_property02: 'prop2',
    customer_property03: 'prop3',
    customer_property04: 'prop4',
    customer_property05: 'prop5',
    deleted_at: null,
    email: 'test3@example.com',
    first_buy_shop_name: 'SC店B',
    first_name: '一郎',
    first_name_kana: 'イチロウ',
    gender: 1,
    id: 'e7930ce70bf357287930beb014ee1c6e',
    last_buy_shop_name: 'SC店A',
    last_name: '山口',
    last_name_kana: 'ヤマグチ',
    locality: '品川',
    memo: 'memomemomemomemomemomemomemomemomemomemomemomemo',
    pref: 13,
    shop_id: 'b30bf864d39afcda729e145210bccd18',
    street: '3-3-3',
    tel01: '222',
    tel02: '2222',
    tel03: '2222',
    updated_at: 1535814000,
    zip01: '003',
    zip02: '0003'
  }
}

export const sampleCustomers = _.values(sampleCustomerMap)

const sampleCustomerOrders = [
  {
    'created_at': 1452006000,
    'shop_code': '01550',
    'shop_name': '店舗１',
    'total_paid_taxless': '8359',
    'total_paid_taxed': '8959',
    'staff_name': 'スタッフ１A',
    'account_month': '201601',
    'pos_order': {
      'client_created_at': 1452006000
    },
    'order_items': [
      {
        'sort_order': 1,
        'group_code': '0',
        'product_code': '222222222222',
        'product_name': '【FO12-1~3】 お正月 1万円RED [750ml x 6]',
        'quantity': 1,
        'price': '10000.00',
        'price_taxless': '10000.00',
        'price_taxed': '10800.00',
        'sales_price_taxless': '10000.00',
        'sales_price_taxed': '10800.00'
      }
    ]
  },
  {
    'created_at': 1427554800,
    'shop_code': '01550',
    'shop_name': '店舗2',
    'total_paid_taxless': '9901',
    'total_paid_taxed': '10090',
    'staff_name': 'スタッフ２A',
    'account_month': '201503',
    'pos_order': {
      'client_created_at': 1427554800
    },
    'order_items': [
      {
        'sort_order': 1,
        'group_code': '0',
        'product_code': '222222222222',
        'product_name': 'YOUR CHOICE WINE 3BTLS SET [750ml×3]',
        'quantity': 1,
        'price_taxless': '10000.00',
        'price_taxed': '10800.00',
        'sales_price_taxless': '10000.00',
        'sales_price_taxed': '10900.00'
      },
      {
        'sort_order': 2,
        'group_code': '0',
        'product_code': '0446533913B2',
        'product_name': "CA'MARCANDA PROMIS",
        'quantity': 1,
        'price_taxless': '10.00',
        'price_taxed': '11.00',
        'sales_price_taxless': '1000.00',
        'sales_price_taxed': '1080.00'
      },
      {
        'sort_order': 3,
        'group_code': '0',
        'product_code': '0448332013B2',
        'product_name': 'GUIDALBERTO',
        'quantity': 2,
        'price_taxless': '1000.00',
        'price_taxed': '1000.00',
        'sales_price_taxless': '1000.00',
        'sales_price_taxed': '1000.00'
      },
      {
        'sort_order': 4,
        'group_code': '0',
        'product_code': '0700202913B3',
        'product_name': 'BARDA',
        'quantity': 1,
        'price': '0.00',
        'list_price_taxless': '0.00',
        'sales_price_taxless': '0.00'
      }
    ]
  },
  {
    'created_at': 1420470000,
    'shop_code': '01550',
    'shop_name': '店舗１',
    'total_paid_taxless': '9905',
    'total_paid_taxed': '10905',
    'staff_name': 'スタッフ１B',
    'account_month': '201501',
    'pos_order': {
      'client_created_at': 1420470000
    },
    'order_items': [
      {
        'sort_order': 1,
        'group_code': '0',
        'product_code': '222222222222',
        'product_name': '【FO12-1･12-2･12-3】お正月福袋1万円RED [',
        'quantity': 1,
        'price': '10000.00',
        'price_taxless': '10000.00',
        'price_taxed': '10800.00',
        'sales_price_taxless': '10000.00',
        'sales_price_taxed': '10800.00'
      }
    ]
  }
]

export default class SampleCustomerRepository {
  /**
   *
   * @param lastNameKana
   * @param phoneNumber
   * @returns {Promise.<Array.<CustomerDto>>}
   */
  async fetchCustomers (lastNameKana, phoneNumber) {
    return sampleCustomers.filter(customer => {
      return (lastNameKana && customer.last_name_kana.indexOf(lastNameKana) !== -1) ||
        (phoneNumber && customer.phone_number.indexOf(phoneNumber) !== -1)
    })
  }

  /**
   *
   * @param customerCode
   * @returns {Promise.<CustomerDetailDto>}
   */
  async fetchByCustomerCode (customerCode) {
    return sampleCustomers.find(customer => customer.customer_code === customerCode)
  }

  async fetchCustomerOrders () {
    return sampleCustomerOrders.map(order => ({
      ...order,
      payments: []
    }))
  }

  async push () {
    console.log('cutsomer push succeeded.')
  }
  async updateCustomerCode (customerCode, id) {
    console.log('cutsomerCode update succeeded.')
  }
}
