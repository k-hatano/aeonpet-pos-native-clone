import fetcher from 'common/models/fetcher'
import { handleAxiosError,REQUEST_ERROR_CODE } from '../../../../common/errors'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import MessageError from '../../../../common/errors/MessageError'

export default class StandardCustomerRepository {
  async fetchCustomers (lastNameKana, phoneNumber) {
    try {
      const response = await fetcher.get('customers/', {
        ...encodeCriteria({last_name_kana: lastNameKana, phone_number: phoneNumber}),
        with: 'customerActivity'
      })
      return response.data
    } catch (error) {
      if (error.response) {
        throw error.response.data
      } else {
        handleAxiosError(error)
      }
    }
  }

  async fetchByCustomerCode (customerCode) {
    try {
      const response = await fetcher.get('customers/', {
        ...encodeCriteria({customer_code: customerCode}),
        with: 'customerActivity'
      })
      const customers = response.data
      return customers[0]
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchCustomerOrders (customerId) {
    try {
      const response = await fetcher.get('customers/' + customerId + '/orders')
      return (response.data || []).map(order => ({
        ...order,
        payments: []
      }))
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async push (customer) {
    try {
      await fetcher.post('/customers', customer)
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async updateCustomerCode (customerCode, id) {
    try {
      await fetcher.put('customers/' + id, {customer_code: customerCode})
    } catch (error) {
      if (this._isNotExistCustomer(error)) {
        throw new MessageError('message.C-03-E001')
      } else if (this._isCustomerCodeDuplicate(error)) {
        throw new MessageError('message.C-03-E002')
      } else {
        handleAxiosError(error)
      }
    }
  }

  async updateCustomerMemo (customerMemo, id) {
    try {
      return await fetcher.put('customers/' + id, {memo: customerMemo})
    } catch (error) {
      if (this._isNotExistCustomer(error)) {
        throw new MessageError('message.C-03-E001')
      } else {
        handleAxiosError(error)
      }
    }
  }

  _isNotExistCustomer (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.CUSTOMER_NOT_EXIST
  }

  _isCustomerCodeDuplicate (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.CUSTOMER_CODE_DUPLICATE
  }
}