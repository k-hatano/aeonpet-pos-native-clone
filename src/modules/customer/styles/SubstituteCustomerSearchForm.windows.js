import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  modalHeader: {
    marginBottom: 0,
    paddingTop: 3,
    paddingRight: 4,
    paddingLeft: 32
  },
  contentContainer: {
    paddingLeft: 32,
    paddingRight: 32,
    paddingBottom: 24,
    paddingTop: 48
  },
  rowWrapper: {
    flexDirection: 'row',
    marginBottom: 32
  },
  rowTitleWrapper: {
    flex: 2,
    justifyContent: 'center'
  },
  rowInputWrapper: {
    flex: 3,
    justifyContent: 'center'
  },
  rowTitleText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  rowInput: {
    height: 48,
    backgroundColor: '#f0f0f0',
    paddingLeft: 23,
    paddingTop: 12,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: '#979797',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  rowButtonWrapper: {
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowButtonContainerStyle: {
    width: 96,
    height: 48,
    fontSize: 24
  },
  rowButtonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  }
})
