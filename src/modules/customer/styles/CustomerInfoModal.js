import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  layoutRoot: {
    width: 1200,
    height: 900,
    backgroundColor: 'white'
  },
  container: {
    flexDirection: 'row',
    alignItems: 'stretch',
    flex: 1
  }
})
