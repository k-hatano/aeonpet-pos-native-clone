import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    padding: 32
  },
  searchByCodeArea: {
    height: 48,
    flexDirection: 'row',
    marginBottom: 30
  },
  searchTextInput: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    height: 46,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 15
  },
  searchByCodeTextArea: {
    flex: 1
  },
  searchByTextArea: {
    height: 100,
    flexDirection: 'row',
    marginBottom: 52
  },
  searchButton: {
    width: 96,
    height: 48,
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 16
  },
  searchByTextInputArea: {
    height: '100%',
    marginBottom: 52,
    flex: 1,
    justifyContent: 'space-between'
  },
  searchByTextButtonArea: {
    height: '100%',
    justifyContent: 'flex-end'
  },
  list: {
    height: '75%'
  },
  listItemLayout: {
    height: 86,
    backgroundColor: 'white'
  },
  listItemMainArea: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 21,
    marginTop: 6
  },
  listItemSubArea: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 21
  },
  phoneNumberText: {
    marginLeft: 30
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  switchUseCardNumberFlgButton: {
    height: 38,
    width: 204,
    fontSize: 24
  },
  customerRegisterButton: {
    height: 38,
    width: 153,
    fontSize: 24
  },
  searchRowWrapper: {
    flexDirection: 'row'
  },
  searchRowTitle: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  searchRowTitleWrapper: {
    flex: 2,
    justifyContent: 'center'
  },
  searchRowFormWrapper: {
    flex: 3,
    justifyContent: 'center'
  }
})
