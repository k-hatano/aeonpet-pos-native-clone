import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContent: {
    flex: 1,
    alignItems: 'center'
  },
  rowContentOneItem: {
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowContentTwoItems: {
    flexDirection: 'row'
  },
  rowContentTwoItemsFirstItem: {
    flex: 1
  },
  rowContentTwoItemsFirstItemText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#9b9b9b',
    letterSpacing: -0.38
  },
  rowContentTwoItemsSecondItem: {
    flex: 1,
    alignItems: 'flex-end'
  },
  rowText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  rowTextPrice: {
    flex: 1,
    flexDirection: 'row'
  },
  rowTextPriceNumber: {
    flex: 1
  },
  rowTextPriceIcon: {
    flex: 1
  },
  separator: {
    backgroundColor: '#ccc'
  },
  listMoreInformation: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    height: 152,
    paddingTop: 17,
    paddingLeft: 23,
    paddingRight: 16,
    paddingBottom: 21
  },
  watchControlContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0
  },
  rowTitle: {
    flex: 1,
    alignItems: 'flex-start'
  },
  rowIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowPrice: {
    flex: 1,
    justifyContent: 'center'
  },
  rowPointWrapper: {
    flex: 1,
    flexDirection: 'row'
  },
  rowPointTitle: {
    flex: 2,
    alignItems: 'flex-end'
  },
  rowPoint: {
    flex: 1,
    alignItems: 'flex-end'
  }
})
