import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  layoutRoot: {
    backgroundColor: 'white',
    width: 640,
    height: 340
  },
  container: {
    margin: 15
  },
  customerSearchArea: {
    position: 'relative',
    height: '100%'
  },
  customerSearchContainer: {
    minHeight: 80
  },
  searchTitle: {
    fontSize: 18,
    marginBottom: 0,
    marginLeft: 5,
    marginTop: 10
  },
  listHeader: {
    width: undefined,
    height: 30,
    marginRight: 10,
    flexDirection: 'row'
  },
  headerBreadcrumbs: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: '#bbb',
    flexDirection: 'row'
  },
  breadcrumbsItem: {
    margin: 5
  },
  currentBreadcrumbsItem: {
    fontWeight: '500'
  },
  headerTitle: {
    marginRight: 0,
    width: 32,
    height: 32
  },
  recordListContainer: {
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  recordItem: {
    height: 90,
    backgroundColor: '#f3f3f3',
    marginTop: 2,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 2,
    borderRadius: 5,
    padding: 15,
    borderWidth: 1,
    borderColor: '#bbb',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  recordItemTitle: {
    backgroundColor: 'transparent'
  },
  productList: {
    marginTop: 10,
    width: undefined,
    flex: 1
  },
  recordItemRight: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  recordItemLeft: {
    flex: 3,
    alignItems: 'flex-start',
    height: 70
  },
  searchPanelTopWrapper: {
    marginBottom: 10,
    marginTop: 0,
    height: 150,
    borderWidth: 1,
    borderColor: '#a2a2a2',
    backgroundColor: '#d8d8d8'
  },
  searchInputWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20
  },
  searchInputContainerLeft: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginLeft: 5
  },
  searchInputContainerRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  searchButton: {
    width: 70,
    height: 34,
    margin: 5,
    borderWidth: 1,
    borderColor: '#b2b2b2',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5
  },
  buttonText: {
    color: '#000'
  },
  searchResultContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  searchResultHeaderRow: {
    backgroundColor: '#f0f0f0',
    height: 48,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12
  },
  searchResultHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  searchResultListViewData: {
    flex: 1,
    paddingBottom: 5
  },
  searchResultPagingRow: {
    height: 100,
    justifyContent: 'flex-start',
    alignItems: 'flex-end'
  },
  searchInputContainer: {
    paddingRight: 20
  },
  searchInputStyle: {
    height: 42,
    width: '100%',
    backgroundColor: '#f0f0f0',
    borderColor: '#979797',
    borderWidth: 1,
    fontSize: 24,
    color: '#d8d8d8',
    letterSpacing: -0.38
  },
  buttonStandard: {
    fontSize: 24,
    height: 40,
    fontWeight: 'bold',
    width: 96
  },
  buttonStandardContainer: {
    flex: 1,
    height: 40,
    marginLeft: 12
  },
  borderedButton: {
    fontSize: 24,
    height: 40,
    fontWeight: 'bold'
  },
  borderedButtonContainer: {
    flex: 2,
    height: 40,
    marginLeft: 8
  },
  customerSearchFooterContainer: {
    position: 'absolute',
    height: 104,
    width: '100%',
    bottom: 0,
    borderTopWidth: 1,
    borderColor: '#979797',
    backgroundColor: 'white',
    alignItems: 'center'
  },
  customerSearchFooterButtonContainer: {
    height: 56,
    marginTop: 24,
    width: 266
  },
  customerSearchFooterButton: {
    fontSize: 24,
    height: 56,
    fontWeight: 'bold'
  },
  customerListContainer: {
    height: '80%'
  },
  customerDetailContainer: {
    height: '70%'
  },
  customerCodeForm: {
    height: 40,
    width: 306,
    borderColor: 'gray',
    borderWidth: 1,
    paddingLeft: 7
  }
})
