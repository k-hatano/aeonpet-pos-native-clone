import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  listContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: '#fff',
    paddingHorizontal: 24,
    paddingVertical: 12
  },
  customerItem: {
    flex: 1,
    flexDirection: 'column'
  },
  customerItemTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  customerItemBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  customerItemBottomLeft: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    height: 30
  },
  customerItemBottomRight: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: 30
  },
  rowText: {
    fontSize: 24,
    height: 36,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  informationListRow: {
    width: '100%',
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  informationListText: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    lineHeight: 26,
    height: 26,
    color: '#4a4a4a'
  },
  informationListTextLeft: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    lineHeight: 26,
    width: '50%',
    height: 26,
    color: '#4a4a4a',
    textAlign: 'left'
  },
  informationListTextRight: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    lineHeight: 26,
    width: '50%',
    height: 26,
    color: '#4a4a4a',
    textAlign: 'right'
  },
  informationListItem: {
  }
})
