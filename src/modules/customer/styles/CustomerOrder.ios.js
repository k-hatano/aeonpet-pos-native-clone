import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  watchControlContainer: {
    flex: 1,
    marginLeft: 40,
    marginRight: 40,
    marginTop: 5
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8e8e8e'
  },
  headerTitle: {
    marginLeft: 20,
    flex: 1,
    fontWeight: '500'
  },
  sectionHeaderText: {
    marginLeft: 10,
    color: '#fff',
    fontWeight: '400'
  },
  rowText: {
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headPicker: {
    height: 35,
    backgroundColor: '#3c3c3c'
  },
  listHeader: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ccc',
    justifyContent: 'space-between'
  },
  sectionHeader: {
    backgroundColor: '#777',
    flex: 1,
    height: 30
  },
  rowView: {
    flex: 1,
    height: 40
  },
  calendarView: {
    position: 'absolute'
  },
  orderDetail: {
    height: 48,
    width: '100%',
    backgroundColor: '#f0f0f0',
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12
  },
  orderDetailButton: {
    flexDirection: 'row'
  },
  orderContainer: {
    height: '95%'
  },
  searchResultContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  searchResultHeaderRow: {
    backgroundColor: '#f0f0f0',
    height: 48,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12
  },
  searchResultHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  container: {
    height: '100%',
    backgroundColor: '#fff',
  },
  tabStyle: {
    width: 136
  }
})
