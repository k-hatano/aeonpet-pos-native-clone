import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  customerMemoArea: {
    position: 'relative',
    height: '100%',
  },
  customerMemoContainer: {
    minHeight: 80
  },
  customerMemoHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20
  },
  customerMemoHeaderLeft: {
    width: 150,
    marginLeft: 5
  },
  customerMemoHeaderRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: 300
  },
  customerMemoLengthContainer: {
    height: 40,
    width: 150,
    marginLeft: 12
  },
  customerMemoLengthLabel: {
    fontSize: 20,
    textAlign: 'right',
    width: 150,
    backgroundColor: '#fff'
  },
  rightButtonContainer: {
    justifyContent: 'space-between',
    height: 40,
    width: 100,
    marginLeft: 8
  },
  customerMemoCancelButton: {
    fontSize: 24,
    height: 40,
    fontWeight: 'bold'
  },
  customerMemoButton: {
    fontSize: 24,
    height: 40
  },
  inputContentEditable: {
    fontSize: 24,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: '#828282',
    padding: 5,
    height: '55%',
    borderRadius: 5
  },
  inputContent: {
    fontSize: 24,
    marginHorizontal: 10,
    padding: 5,
    height: '95%',
    borderRadius: 5
  }

})
