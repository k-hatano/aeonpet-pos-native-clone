import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  CustomerOrder: {
    flex: 1,
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  container: {
    height: '100%',
    width: '100%',
    flexDirection: 'column'
  },
  rowHeader: {
    backgroundColor: '#ddd',
    height: 45,
    justifyContent: 'center',
    paddingLeft: 20,
    width: '100%'
  },
  row: {
    flexDirection: 'row',
    height: 64,
    paddingLeft: 20,
    paddingRight: 20,
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: '#fff'
  },
  rowName: {
    flex: 6,
    marginRight: 10,
    justifyContent: 'center'
  },
  rowId: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowIcon: {
    marginRight: 5
  },
  btnCheckoutWrapper: {
    width: '100%',
    height: 104,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: '#979797',
    paddingHorizontal: 15
  },
  btnCheckout: {
    backgroundColor: '#ff9024',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    padding: 5
  },
  rowText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  customerInformationContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    flexDirection: 'column'
  },
  fontColor: {
    color: '#fff'
  },
  customerInformationInner: {
    flex: 1,
    width: '100%'
  },
  searchResultContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  searchResultHeaderRow: {
    backgroundColor: '#f0f0f0',
    height: 48,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  searchResultHeaderText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  buttonDeleteWrapper: {
    flex: 3,
    height: 56,
    paddingHorizontal: 8
  },
  buttonPointWrapper: {
    flex: 2,
    height: 56,
    paddingHorizontal: 8
  },
  buttonLabel: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 24,
    letterSpacing: -0.38
  },
  memberStatusContainer: {
    width: 80,
    height: 30,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    marginRight: 5
  },
  memberStatusText: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 3
  }
})
