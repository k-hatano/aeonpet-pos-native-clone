import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  rowView: {
    width: undefined,
    height: 90,
    paddingTop: 5,
    paddingLeft: 10,
    paddingRight: 20,
    paddingBottom: 5,
    backgroundColor: '#fff',
    flexDirection: 'row'
  },
  rowViewLeft: {
    flex: 2,
    flexDirection: 'column'
  },
  rowViewCenter: {
    flex: 3,
    flexDirection: 'row',
    paddingRight: 10
  },
  rowViewRight: {
    minWidth: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowTextTitle: {
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 5,
    fontSize: 18
  },
  rowTextRightPanel: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: undefined
  },
  rowTextPrice: {
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 4,
    fontWeight: '500',
    fontSize: 25
  },
  rowTextQuantity: {
    fontSize: 20
  },
  separator: {
    backgroundColor: '#ccc'
  },
  rowWrapper: {
    flex: 2,
    flexDirection: 'column'
  },
  watchControlContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    height: '100%'
  },
  tabPanelActiveBackgroundStyle: {
    borderBottomWidth: 6,
    borderColor: '#ff9024'
  },
  tabPanelActiveTextStyle: {
    color: '#000000',
    marginTop: 6
  },
  tabPanelTabStyle: {
    paddingLeft: 0,
    height: 80,
    flex: 1,
    backgroundColor: '#ffffff'
  },
  tabPanelTabWrapperStyle: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 67,
    marginBottom: 13
  },
  particularTabStyle: {
    height: 67,
    backgroundColor: 'transparent',
    paddingLeft: 50,
    paddingRight: 50
  },
  particularTabNameStyle: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  summaryContainer: {
    flexDirection: 'column',
    paddingLeft: 24,
    paddingRight: 24
  },
  summaryOrderDate: {
    flexDirection: 'row',
    paddingTop: 26,
    paddingBottom: 26,
    borderColor: '#d8d8d8',
    borderBottomWidth: 1
  },
  summaryTitleWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  summaryTitleText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  summaryValueWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  summaryValueText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  summaryExtraValueText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    marginRight: 20
  },
  childRowIcon: {
    width: 12,
    height: 11,
    marginLeft: 15,
    marginRight: 9
  }
})
