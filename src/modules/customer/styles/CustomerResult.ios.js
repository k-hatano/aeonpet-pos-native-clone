import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  CustomerOrder: {
    flex: 1,
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  container: {
    height: '100%',
    width: '100%',
    flexDirection: 'column'
  },
  rowHeader: {
    backgroundColor: '#ddd',
    height: 45,
    justifyContent: 'center',
    paddingLeft: 20,
    width: '100%'
  },
  row: {
    flexDirection: 'row',
    padding: 7,
    height: 64,
    paddingLeft: 20,
    paddingRight: 20,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#fff'
  },
  rowName: {
    flex: 2,
    marginRight: 10,
    justifyContent: 'center'
  },
  rowId: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowIcon: {
    marginRight: 5
  },
  btnCheckoutWrapper: {
    width: '100%',
    height: 104,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: '#979797',
    paddingHorizontal: 15
  },
  btnCheckout: {
    backgroundColor: '#ff9024',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    padding: 5
  },
  customerInformationContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.8,
    marginHorizontal: 24,
    minHeight: 64,
    alignItems: 'center'
  },
  customerInformationLabel: {
    flex: 1,
    fontSize: 20,
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: '#fff'
  },
  customerInformationValue: {
    flex: 2,
    backgroundColor: '#fff',
    paddingTop: 12,
    paddingBottom: 12,
    fontSize: 20,
    textAlign: 'right'
  },
  searchResultContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  searchResultHeaderRow: {
    backgroundColor: '#f0f0f0',
    height: 48,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  searchResultHeaderText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  buttonDeleteWrapper: {
    flex: 3,
    height: 56,
    paddingHorizontal: 8
  },
  buttonPointWrapper: {
    flex: 2,
    height: 56,
    paddingHorizontal: 8
  },
  buttonLabel: {
    fontSize: 18,
    letterSpacing: -0.38
  },
  memberStatusContainer: {
    width: 80,
    height: 30,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    marginRight: 5
  },
  memberStatusText: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 3
  },
  customerCodeChangeButton: {
    fontSize: 24,
    height: 40,
    fontWeight: 'bold',
    width: 96
  },
  customerCodeChangeModal: {
    backgroundColor: 'white',
    width: 640,
    height: 250
  },
  customerCodeChangeButtonHidden: {
    opacity: 0,
    fontSize: 24,
    height: 40,
    fontWeight: 'bold',
    width: 96
  },
})
