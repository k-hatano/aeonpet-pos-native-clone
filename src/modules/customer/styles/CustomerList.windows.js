import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  listContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: '#fff',
    height: 152,
    paddingHorizontal: 24,
    paddingVertical: 22
  },
  customerItem: {
    flex: 1,
    flexDirection: 'column'
  },
  customerItemTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 13
  },
  customerItemBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  customerItemBottomLeft: {
    flex: 3,
    flexDirection: 'row'
  },
  customerItemBottomRight: {
    flex: 2,
    flexDirection: 'row'
  },
  rowText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    marginRight: 15
  }
})
