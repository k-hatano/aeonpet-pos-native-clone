import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  row: {
    flexDirection: 'row'
  },
  mainArea: {
    flex: 1,
    flexDirection: 'row'
  },
  leftArea: {
    flex: 1,
    marginTop: 104,
    marginLeft: 24,
    marginRight: 12
  },
  rightArea: {
    flex: 1,
    marginLeft: 12,
    marginRight: 24,
    marginBottom: 24,
    justifyContent: 'center'
  },
  leftTextContainerOfDouble: {
    width: '50%',
    paddingRight: 8
  },
  rightTextContainerOfDoufle: {
    width: '50%',
    paddingLeft: 8
  },
  leftTextContainerOfTriple: {
    width: '33%',
    paddingRight: 8
  },
  centerTextContainerOfTriple: {
    width: '33%',
    paddingLeft: 8,
    paddingRight: 8
  },
  rightTextContainerOfTriple: {
    width: '33%',
    paddingLeft: 8
  },
  textInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: '100%',
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  rowLayout: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 22
  },
  rowLayoutRequiredContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rowLayoutRequiredText: {
    fontSize: 24,
    height: 24,
    color: 'red',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  rowLayoutLabel: {
    flex: 7,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  rowLayoutText: {
    fontSize: 24,
    height: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  rowLayoutContents: {
    flex: 11,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  datePickerContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  datePickerTitleLabel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  datePickerConfirmLabel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#44bcb9',
    color: '#ffffff',
    padding: 15
  },
  datePickerCancelLabel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#ffffff',
    color: '#44bcb9'
  },
  addressInputButton: {
    fontSize: 24,
    fontWeight: 'bold',
    height: 56
  },
  selectInput: {
    width: '100%'
  },
  submitButtonContainer: {
    width: '100%',
    height: 82,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  submitButton: {
    fontSize: 24,
    fontWeight: 'bold',
    width: 144,
    height: 56
  }
})
