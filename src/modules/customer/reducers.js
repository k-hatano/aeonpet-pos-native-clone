import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  customers: [],
  selectedCustomer: null,
  orders: [],
  order: null,
  useCardNumberFlg: true,
  customerCode: '',
  registerCustomer: {
    customer_code: null,
    first_name: null,
    last_name: null,
    first_name_kana: null,
    last_name_kana: null,
    tel01: null,
    tel02: null,
    tel03: null,
    birthday: null,
    gender: 3,
    zip01: null,
    zip02: null,
    pref: null,
    city: null,
    locality: null,
    street: null,
    accept_dm: 1,
    email: null,
    accept_email: 1
  },
  regions: [],
  shopName: null,
  isEditable: false,
  memoText: null,
  pasoriStatus: ''
}

const handlers = {
  [actions.listCustomers]: (state, action) => {
    return {
      ...state,
      ...{
        customers: action.payload,
        selectedCustomer: null,
        orders: []
      }
    }
  },
  [actions.selectCustomer]: (state, action) => ({
    ...state,
    ...{ selectedCustomer: action.payload }
  }),
  [actions.listCustomerOrders]: (state, action) => ({
    ...state,
    ...{
      orders: action.payload,
      order: null
    }
  }),
  [actions.selectOrder]: (state, action) => ({
    ...state,
    ...{ order: action.payload }
  }),
  [actions.useCardNumber]: (state, action) => ({
    ...state,
    ...{ useCardNumberFlg: action.payload }
  }),
  [actions.setCustomerCode]: (state, action) => ({
    ...state,
    customerCode: action.payload
  }),
  [actions.updateRegisterCustomer]: (state, action) => ({
    ...state,
    registerCustomer: {
      ...state.registerCustomer,
      ...action.payload
    }
  }),
  [actions.resetRegisterCustomer]: (state, action) => ({
    ...state,
    registerCustomer: Object.assign({}, defaultState.registerCustomer)
  }),
  [actions.listRegions]: (state, action) => ({
    ...state,
    regions: action.payload
  }),
  [actions.setShopName]: (state, action) => ({
    ...state,
    shopName: action.payload
  }),
  [actions.setIsEditable]: (state, action) => ({
    ...state,
    isEditable: action.payload
  }),
  [actions.setMemoText]: (state, action) => ({
    ...state,
    memoText: action.payload
  }),
  [actions.setPasoriStatus]: (state, action) => ({
    ...state,
    pasoriStatus: action.payload
  })
}
export default handleActions(handlers, defaultState)
