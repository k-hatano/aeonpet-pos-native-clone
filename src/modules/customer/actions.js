import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listCustomers = createAction(`${MODULE_NAME}_listCustomers`)
export const selectCustomer = createAction(`${MODULE_NAME}_selectCustomer`)
export const listCustomerOrders = createAction(`${MODULE_NAME}_listCustomerOrders`)
export const selectOrder = createAction(`${MODULE_NAME}_selectOrder`)
export const useCardNumber = createAction(`${MODULE_NAME}_useCardNumber`)
export const setCustomerCode = createAction(`${MODULE_NAME}_setCustomerCode`)
export const updateRegisterCustomer = createAction(`${MODULE_NAME}_updateRegisterCustomer`)
export const resetRegisterCustomer = createAction(`${MODULE_NAME}_resetRegisterCustomer`)
export const listRegions = createAction(`${MODULE_NAME}_listRegions`)
export const setShopName = createAction(`${MODULE_NAME}_setShopName`)
export const setIsEditable = createAction(`${MODULE_NAME}_setIsEditable`)
export const setMemoText = createAction(`${MODULE_NAME}_setMemoText`)
export const setPasoriStatus = createAction(`${MODULE_NAME}_setPasoriStatus`)
