import React, { Component } from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/CustomerInfoModal'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import Modal from 'common/components/widgets/Modal'
import CustomerDetail from './CustomerDetail'
import CustomerOrderViewContainer from '../containers/CustomerOrderViewContainer'

export default class CustomerInfoModal extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    onClearCustomerOrder: PropTypes.func
  }

  componentWillMount () {
    this.props.onInit()
  }

  componentWillUnmount () {
    this.props.onClearCustomerOrder()
  }

  render () {
    const { customer } = this.props

    return (
      <ModalFrame
        style={styles.layoutRoot}
        onClose={() => Modal.close()} >
        <View style={styles.container}>
          <View style={{flex: 1}}>
            <CustomerDetail customer={customer} />
          </View>
          <View style={{flex: 1, height: '98%'}}>
            <CustomerOrderViewContainer />
          </View>
        </View>
      </ModalFrame>
    )
  }
}
