'use strict'

import React, { Component } from 'react'
import { Text, View, ScrollView, ListView } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/CustomerResult'
import moment from 'moment'
import { formatUnit } from '../../../common/utils/formats'
import { WeakProceedButton } from 'common/components/elements/StandardButton'
import Modal from 'common/components/widgets/Modal'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import CustomerCodeChangeContainer from '../containers/CustomerCodeChangeContainer'
import KeyboardSpace from '../../../common/components/layout/KeyboardSpace'

class ModalContent extends Component {
  render () {
    return (
      <View>
        <ModalFrame
          style={styles.customerCodeChangeModal}
          title={I18n.t('customer.customer_code') + I18n.t('common.change')}
          onClose={() => this.props.onClose()}>
          <CustomerCodeChangeContainer
            onClose={() => this.props.onClose()}/>
        </ModalFrame>
        <KeyboardSpace />
      </View>
    )
  }
}

export default class CustomerDetail extends Component {
  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  _onChangeCardNum () {
    // バーコードリーダーの処理が重複してしまうため、一旦消す
    const {searchObj} = this.props
    searchObj.eventBarcodeReader.remove()
    Modal.open(ModalContent, {
      isBackgroundVisible: true,
      props: {
        onClose: () => {
          Modal.close()
          searchObj._setBarcodeReaderEvent()
        }
      }
    })
  }

  _getCustomerName (lastName, firstName) {
    const name = [lastName, firstName].filter(v => v).join(' ')
    return name ? name + ' ' + I18n.t('customer.honorific') : ''
  }

  render () {
    const { customer, shopName } = this.props
    const lastBuyAt = customer.customer_activity.last_buy_at !== null ? moment.unix(customer.customer_activity.last_buy_at).format('YYYY-MM-DD') : ''

    return (
      <View>
        <View style={styles.searchResultHeaderRow}>
          <Text style={styles.searchResultHeaderText}>{I18n.t('customer.customer_info')}</Text>
        </View>
        <View>
          <ScrollView>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('customer.customer_code') + '：　'}</Text>
              <WeakProceedButton
                onPress={() => this._onChangeCardNum()}
                text={I18n.t('common.change')}
                style={styles.customerCodeChangeButtonHidden}
                disabled={true}
              />
              <Text style={styles.customerInformationValue}>
                {customer.customer_code}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('customer.name') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {this._getCustomerName(customer.last_name, customer.first_name)}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('customer.name_kana') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {this._getCustomerName(customer.last_name_kana, customer.first_name_kana)}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('customer.customer_phone_number') + '： '}</Text>
              <Text style={styles.customerInformationValue}>
                {customer.tel01 + '-' + customer.tel02 + '-' + customer.tel03}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('customer.customer_holding_points') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {formatUnit(customer.customer_activity.points, 'pt')}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('order.first_point_limit') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {customer.customer_activity.exchange_points_effective_date ? customer.customer_activity.exchange_points_effective_date : '-'}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('order.first_invalidate_points') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {formatUnit(customer.customer_activity.exchange_points ? customer.customer_activity.exchange_points : 0, 'pt')}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('order.second_point_limit') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {customer.customer_activity.deposit_points_effective_date ? customer.customer_activity.deposit_points_effective_date : '-'}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{I18n.t('order.second_invalidate_points') + '：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {formatUnit(customer.customer_activity.deposit_points ? customer.customer_activity.deposit_points : 0, 'pt')}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{'登録店舗：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {shopName}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{'購入回数：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {formatUnit(customer.customer_activity.buy_times, '回')}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{'前回購入日：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {lastBuyAt}
              </Text>
            </View>
            <View style={styles.customerInformationContainer}>
              <Text style={styles.customerInformationLabel}>{'前回購入店舗：　'}</Text>
              <Text style={styles.customerInformationValue}>
                {customer.last_buy_shop_name}
              </Text>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}
