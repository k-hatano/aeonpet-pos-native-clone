import React from 'react'
import { Text, View, ListView, TextInput } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/SelectCustomerView'
import baseStyle from 'common/styles/baseStyle'
import { CommandButton, OptionalCommandButton, WeakProceedButton } from 'common/components/elements/StandardButton'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import { CUSTOMER_CODE_LENGTH, SEARCH_TEL_LENGTH } from '../models'

class CustomerListItem extends React.Component {
  static propTypes = {
    customer: PropTypes.shape({
      last_name_kana: PropTypes.string,
      first_name_kana: PropTypes.string,
      customer_code: PropTypes.string,
      postal_code: PropTypes.string,
      porint: PropTypes.number
    }),
    onSelect: PropTypes.func
  }

  render () {
    const { customer } = this.props
    return (
      <TouchableOpacitySfx onPress={this.props.onSelect}>
        <View style={styles.listItemLayout}>
          <View style={styles.listItemMainArea}>
            <Text style={baseStyle.mainText}>
              {customer.last_name_kana + '  ' + customer.first_name_kana + I18n.t('customer.honorific')}
            </Text>
          </View>
          <View style={styles.listItemSubArea}>
            <Text style={baseStyle.subText}>
              {customer.customer_code}
            </Text>
            <Text style={[baseStyle.subText, styles.phoneNumberText]}>
              {'***-****-' + customer.tel03}
            </Text>
          </View>
          <View style={styles.separator} />
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export default class SelectCustomerView extends React.Component {
  static propTypes = {
    customerCode: PropTypes.string,
    customers: PropTypes.array,
    onChangeCustomerCode: PropTypes.func,
    onFindByCode: PropTypes.func,
    onFindByText: PropTypes.func,
    onSelectCustomer: PropTypes.func,
    onSwitchUseCardNumberFlg: PropTypes.func,
    onUseCardNumber: PropTypes.func,
    useCardNumber: PropTypes.bool
  }

  static defaultProps = {
    customers: []
  }

  constructor () {
    super()
    this.state = {
      customerNamePhon: '',
      customerPhone: ''
    }
  }

  get _searchByTextParamater () {
    return {
      customerNamePhon: this.state.customerNamePhon,
      customerPhone: this.state.customerPhone
    }
  }

  render () {
    const dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const { customerCode, useCardNumberFlg, onChangeCustomerCode, onFindByCode, onUseCardNumber, onFindByText, customers, onSelectCustomer, onConfirmCustomerRegister } = this.props
    const { customerPhone, customerNamePhon } = this.state
    const cardNumberDisableFlg = !(customerCode && customerCode.length <= CUSTOMER_CODE_LENGTH && customerCode.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/))
    const nameAndPhoneDisableFlg = !(customerNamePhon.length > 0 && customerNamePhon.match(/^[\u30A0-\u30FF]+$/) && customerPhone.length === SEARCH_TEL_LENGTH && customerPhone.match(/^\d*$/))
    return (
      <View style={styles.container}>
        {useCardNumberFlg &&
          <View>
            <View style={styles.searchByCodeArea}>
              <View style={styles.searchByCodeTextArea}>
                <TextInput
                  maxLength={CUSTOMER_CODE_LENGTH}
                  style={styles.searchTextInput}
                  value={customerCode}
                  onChangeText={(text) => onChangeCustomerCode(text)}
                  placeholder={I18n.t('customer.customer_code')} />
              </View>
              <CommandButton
                text={I18n.t('common.search')}
                style={styles.searchButton}
                onPress={() => onFindByCode(customerCode)}
                disabled={cardNumberDisableFlg} />
            </View>
            <View style={styles.buttonsContainer}>
              <OptionalCommandButton
                style={styles.switchUseCardNumberFlgButton}
                onPress={() => onUseCardNumber(false)}
                text={I18n.t('customer.forgotten_card')} />
              <WeakProceedButton
                style={styles.customerRegisterButton}
                onPress={() => onConfirmCustomerRegister(customerCode)}
                text={I18n.t('page.customer_register')} />
            </View>
          </View>
        }
        {!useCardNumberFlg &&
          <View style={styles.searchByTextArea}>
            <View style={styles.searchByTextInputArea}>
              <View style={styles.searchRowWrapper}>
                <View style={styles.searchRowTitleWrapper}>
                  <Text style={styles.searchRowTitle}>{I18n.t('customer.family_name_phonetic')}</Text>
                </View>
                <View style={styles.searchRowFormWrapper}>
                  <TextInput
                    style={styles.searchTextInput}
                    value={customerNamePhon}
                    autoCapitalize='none'
                    autoCorrect={false}
                    onChangeText={(text) => {
                      const inputText = text.slice(0, 10) || ''
                      this.setState({ customerNamePhon: inputText })}
                    }
                    returnKeyType='next'
                    placeholder={I18n.t('customer.family_name_phonetic')} />
                </View>
              </View>
              <View style={styles.searchRowWrapper}>
                <View style={styles.searchRowTitleWrapper}>
                  <Text style={styles.searchRowTitle}>{I18n.t('customer.phone_number_last_4')}</Text>
                </View>
                <View style={styles.searchRowFormWrapper}>
                  <TextInput
                    maxLength={SEARCH_TEL_LENGTH}
                    style={styles.searchTextInput}
                    value={customerPhone}
                    autoCapitalize='none'
                    autoCorrect={false}
                    onChangeText={(text) => this.setState({ customerPhone: text })}
                    returnKeyType='next'
                    placeholder={I18n.t('customer.phone_number_last_4')} />
                  </View>
              </View>
            </View>
            <View style={styles.searchByTextButtonArea}>
              <CommandButton
                text={I18n.t('common.search')}
                style={styles.searchButton}
                onPress={() => onFindByText(this._searchByTextParamater)}
                disabled={nameAndPhoneDisableFlg} />
            </View>
          </View>
        }
        <ListView
          enableEmptySections
          style={styles.list}
          dataSource={dataSource.cloneWithRows(this.props.customers)}
          renderRow={(customer) => (
            <CustomerListItem customer={customer} onSelect={() => onSelectCustomer(customer)} />)} />
      </View>
    )
  }
}
