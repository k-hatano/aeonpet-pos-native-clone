'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/CustomerOrder'
import PropTypes from 'prop-types'
import CustomerMemo from './CustomerMemo'
import CustomerOrderList from './CustomerOrderList'
import OrderedCart from '../../order/models/OrderedCart'
import CartItemFormatter from '../../cart/models/CartItemFormatter'
import orderStyles from 'modules/order/styles/Page'
import TabPanel from 'common/components/widgets/TabPanel'
import { STATUS } from 'modules/order/models'
import OrderedCartItemList from '../../order/components/OrderedCartItemList'

export default class CustomerOrderView extends Component {
  static propTypes = {
    orders: PropTypes.array,
    customer: PropTypes.object,
    selectedOrder: PropTypes.object,
    onSelectOrder: PropTypes.func,
    onResetOrder: PropTypes.func,
    onSaveMemo: PropTypes.func,
    isEditable: PropTypes.bool,
    onSetIsEditable: PropTypes.func,
    memoText: PropTypes.string,
    onChangeMemoText: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      selectedTab: 0
    }
  }

  _renderCustomerOrder () {
    const {orders, order, onSelectOrder} = this.props
    if (order === null) {
      return (
        <CustomerOrderList
          orders={orders}
          onSelectOrder={onSelectOrder} />
      )
    } else {
      const cart = new OrderedCart(order)
      const items = (new CartItemFormatter()).format(cart)
      return (
        <View style={orderStyles.tabContainer}>
          <TabPanel tabWrapperStyle={orderStyles.tabWrapperStyle} tabStyle={{width: 136}}>
            <View tabName={I18n.t('order.detail')}>
              <OrderedCartItemList
                items={items}
                currency={cart.currency} />
            </View>
            {order.status === STATUS.RETURNED &&
            <View
              tabName={I18n.t('order.return_detail')}
              tabNameStyle={{color: 'red'}}>
              <OrderedCartItemList
                items={items}
                currency={cart.currency} />
            </View>}
          </TabPanel>
        </View>
      )
    }
  }

  render () {
    const {order, customer, onResetOrder, onSaveMemo, isEditable, onSetIsEditable, memoText, onChangeMemoText} = this.props
    if (customer === null) {
      return null
    }

    const tabClickFunc = order === null || this.state.selectedTab === 1
      ? tabId => this.setState({selectedTab: tabId})
      : tabId => onResetOrder()

    return (
      <View style={styles.container}>
        <TabPanel selectedTab={this.state.selectedTab} tabStyle={styles.tabStyle}>
          <View
            tabName={(order !== null ? '＜' : '') + I18n.t('order.orders')}
            onTabClick={tabClickFunc}
            style={styles.orderContainer}>
            {this._renderCustomerOrder()}
          </View>

          <View
            tabName={I18n.t('customer.note')}
            onTabClick={tabId => this.setState({selectedTab: tabId})}
            style={styles.orderContainer}>
            <CustomerMemo
              customer={customer}
              onSaveMemo={onSaveMemo}
              isEditable={isEditable}
              onSetIsEditable={onSetIsEditable}
              memoText={memoText}
              onChangeMemoText={onChangeMemoText}/>
          </View>
        </TabPanel>
      </View>
    )
  }
}
