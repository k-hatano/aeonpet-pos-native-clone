'use strict'

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, View } from 'react-native'
import componentStyles from '../styles/CustomerList'
import I18n from 'i18n-js'
import { formatPriceWithCurrency, formatUnit } from '../../../common/utils/formats'
import moment from 'moment'

class ItemRow extends React.Component {
  static propTypes = {
    leftLabel: PropTypes.string,
    leftValue: PropTypes.string,
    rightLabel: PropTypes.string,
    rightValue: PropTypes.string,
    leftTextAlign: PropTypes.string
  }

  render () {
    const leftSeparator = this.props.leftLabel ? ' : ' : ''
    const rightSeparator = this.props.rightLabel ? ' : ' : ''
    const leftTextAlign = this.props.leftTextAlign || 'left'
    return (
      <View style={componentStyles.informationListRow}>
        <Text style={
          leftTextAlign === 'left'
            ? componentStyles.informationListTextLeft
            : componentStyles.informationListTextRight
        }>
          {this.props.leftLabel}{leftSeparator}{this.props.leftValue}
        </Text>
        <Text style={componentStyles.informationListTextRight}>
          {this.props.rightLabel}{rightSeparator}{this.props.rightValue}
        </Text>
      </View>
    )
  }
}

export default class CustomerOrderListItem extends Component {
  static propTypes = {
    order: PropTypes.object
  }

  render () {
    const order = this.props.order
    const totalPaid = formatPriceWithCurrency(parseInt(order.total_paid_taxed), order.cueency)
    const clientCreatedAt = order.client_created_at ? moment.unix(order.client_created_at).format('YYYY-MM-DD HH:mm:ss') : ''
    return (
      <View style={componentStyles.informationListItem}>
        <ItemRow
          leftValue={clientCreatedAt}
          rightValue={order.shop_name}
        />
        <ItemRow
          rightLabel={I18n.t('customer.used_points')}
          rightValue={formatUnit(order.used_points, 'pt')}
        />
        <ItemRow
          leftValue={totalPaid}
          leftTextAlign={'right'}
          rightLabel={I18n.t('customer.got_points')}
          rightValue={formatUnit(order.obtained_points, 'pt')}
        />
      </View>
    )
  }
}
