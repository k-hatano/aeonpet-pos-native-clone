import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import { Text, View, NativeModules, NativeAppEventEmitter, Platform, NativeEventEmitter, AlertIOS } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/SelectCustomerViewModal'
import Modal from 'common/components/widgets/Modal'
import { showSelectStaffViewModal } from '../../../modules/staff/containers/SelectStaffViewModal'
import baseStyle from 'common/styles/baseStyle'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import SelectCustomerView from './SelectCustomerView'
import PasoriManager from '../../pasori/PasoriManager'
import AlertView from '../../../common/components/widgets/AlertView'
import ConfirmView from '../../../common/components/widgets/ConfirmView'
import { PERMISSION_CODES } from 'modules/staff/models'

let EventName = 'barcode'

export default class SelectCustomerViewModal extends Component {
  nativeEventManager = NativeModules.PasoriBridge
  nativeEventManageEmitter = new NativeEventEmitter(this.nativeEventManager)
  bluetoothStatusListener = null

  static propTypes = {
    customers: PropTypes.array,
    customerCode: PropTypes.string,
    onInit: PropTypes.func,
    onFindByCode: PropTypes.func,
    onFindByText: PropTypes.func,
    onSelectCustomer: PropTypes.func,
    onChangeCustomerCode: PropTypes.func,
    onClose: PropTypes.func,
    onDispose: PropTypes.func,
    onUseCardNumber: PropTypes.func,
    useCardNumber: PropTypes.bool,
    onChangePasoriStatus: PropTypes.func,
    pasoriStatus: PropTypes.string
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
    this.props.onChangePasoriStatus(I18n.t('customer.pasori_connecting'))
    this.bluetoothStatusListener = this.nativeEventManageEmitter.addListener('ConfirmBluetoothStatus', (body) => {
      AlertIOS.alert(body)
    })
  }

  async componentDidMount () {
    this.eventBarcodeReader = NativeAppEventEmitter.addListener(
      EventName,
      async body => {
        const customerDetail = await this.props.onFindByCode(body.trim())
        if (customerDetail) {
          this.props.onClose(customerDetail)
        }
      }
    )

    if (Platform.OS === 'ios') {
      NativeModules.BarcodeReaderBridge.setBarcodeReaderProperty({
        BARCODE_READER_SDK: 0,
        BARCODE_READER_STD: 1,
        BARCODE_READER_IP: 'empty'
      })
    }

    await this._connectPasori()

  }

  async _connectPasori () {
    try {
      await PasoriManager.startInitialize()
      this.props.onChangePasoriStatus(I18n.t('customer.pasori_connected'))
      await this._readCardDataByPasori()
    } catch (error) {
      this.props.onChangePasoriStatus(I18n.t('customer.pasori_unconnecting'))
    }
  }

  async _readCardDataByPasori () {
    try {
      const result = await PasoriManager.start()
      const customerDetail = await this.props.onFindByCode(result['data'])
      if (customerDetail) {
        this.props.onClose(customerDetail)
      } else {
        this._readCardDataByPasori()
      }
    } catch (error) {
      this.props.onChangePasoriStatus(I18n.t('customer.pasori_unconnecting'))
    }
  }

  _closePasori () {
    PasoriManager.close()
  }

  componentWillUnmount () {
    this.nativeEventManageEmitter.removeSubscription(this.bluetoothStatusListener)
    this.props.onDispose && this.props.onDispose()
    this.eventBarcodeReader.remove()
    this._closePasori()
  }

  render () {
    return (
      <ModalFrame
        style={styles.layoutRoot}
        title={I18n.t('customer.customer_selector_title')}
        onClose={() => this.props.onClose(null)} >
        <Text style={[{marginLeft: 32}, baseStyle.subText]}>
          {this.props.pasoriStatus}
        </Text>
        <SelectCustomerView
          onFindByCode={async (code) => {
            const customerDetail = await this.props.onFindByCode(code)
            if (customerDetail) {
              this.props.onClose(customerDetail)
            }
          }}
          onFindByText={async (params) => {
              const customerDetail = await this.props.onFindByText(params)
              if (customerDetail) {
                this.props.onClose(customerDetail)
              }
            }
          }
          onSelectCustomer={async (customer) => {
            const customerDetail = await this.props.onSelectCustomer(customer)
            if (customerDetail) {
              this.props.onClose(customerDetail)
            }
          }}
          onConfirmCustomerRegister={async (customerCode) => {
            ConfirmView.show(I18n.t('cart.go_to_customer_register_confirm'), async (customerCode) => {
              await Modal.close()
              showSelectStaffViewModal(PERMISSION_CODES.CUSTOMER_REGISTER, async (staff) => {
                if (staff) {
                  Actions.customerRegisterPage()
                }
              })
            })
          }}
          customers={this.props.customers}
          onUseCardNumber={this.props.onUseCardNumber}
          useCardNumberFlg={this.props.useCardNumberFlg}
          customerCode={this.props.customerCode}
          onChangeCustomerCode={this.props.onChangeCustomerCode} />
      </ModalFrame>
    )
  }
}
