'use strict'

import React, { Component } from 'react'
import { TouchableOpacity, ListView } from 'react-native'
import componentStyles from '../styles/CustomerList'
import PropTypes from 'prop-types'
import CustomerOrderListItem from './CustomerOrderListItem'

export default class CustomerOrderList extends Component {
  static propTypes = {
    orders: PropTypes.array,
    onSelectOrder: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  render () {
    const { orders, onSelectOrder } = this.props
    const source = this.dataSource.cloneWithRows(orders)
    return (
      <ListView
        style={componentStyles.listContainer}
        dataSource={source}
        renderRow={(order, index) =>
          <TouchableOpacity
            key={index}
            style={componentStyles.row}
            onPress={() => {
              onSelectOrder(order)
            }}
          >
            <CustomerOrderListItem
              order={order} />
          </TouchableOpacity>}
        enableEmptySections />
    )
  }
}
