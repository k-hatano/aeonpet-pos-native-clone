import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/CustomerRegister'
import I18n from 'i18n-js'
import Moment from 'moment'
import KeyboardSpace from 'common/components/layout/KeyboardSpace'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { RadioGroup } from 'common/components/elements/Inputs'
import { 
  CUSTOMER_GENDER,
  ACCEPT,
  CUSTOMER_CODE_LENGTH,
  NAME_LENGTH,
  TEL_LENGTH,
  ZIPCODE1_LENGTH,
  ZIPCODE2_LENGTH,
  LOCALITY_LENGTH,
  STREET_LENGTH,
  EMAIL_LENGTH
} from '../models'
import { OptionalCommandButton, CommandButton } from 'common/components/elements/StandardButton'
import Select from 'common/components/elements/Select'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import CardNumberBox from 'common/components/elements/CardNumberBox'

function RowLayout (props) {
  return (
    <View style={styles.rowLayout}>
      <View style={styles.rowLayoutRequiredContainer}>
        { props.required &&
          <Text style={styles.rowLayoutRequiredText}>*</Text>
        }
      </View>
      <View style={styles.rowLayoutLabel}>
        {props.title && <Text style={styles.rowLayoutText}>{props.title} : </Text>}
      </View>
      <View style={styles.rowLayoutContents}>
        {props.children}
      </View>
    </View>
  )
}

export default class CustomerRegisterView extends Component {
  static propTypes = {
    registerCustomer: PropTypes.object,
    regions: PropTypes.array,
    onInit: PropTypes.func,
    onChangeForm: PropTypes.func,
    onInputAddress: PropTypes.func,
    onSubmit: PropTypes.func
  }

  componentWillMount () {
    this.props.onInit()
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      if (this.props.isInputCustomerCode) {
        this.props.onChangeForm('customer_code', body.trim())
      }
    })
  }

  async componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  constructor (props) {
    super(props)
    this.state = {
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  _showDateTimePicker (title, date, key) {
    this.setState({
      isDatePickerVisible: true,
      date: date ? new Date(date) : new Date(),
      datePickerTitle: title,
      dateKey: key
    })
  }

  _hideDateTimePicker = () => this.setState({ isDatePickerVisible: false })

  _handleDatePicked (date) {
    this.setState({ isDatePickerVisible: false})
    this.props.onChangeForm('birthday', Moment(date).format('YYYY-MM-DD'))
  }

  _genderOptions () {
    const { registerCustomer } = this.props
    const genderOptions = [
      { label: I18n.t('customer.male'), value: CUSTOMER_GENDER.MALE },
      { label: I18n.t('customer.female'), value: CUSTOMER_GENDER.FEMALE },
      { label: I18n.t('customer.unselected'), value: CUSTOMER_GENDER.UNSELECTED }
    ]

    const optionIndex = genderOptions.findIndex(option => option.value === registerCustomer.gender)
    if (optionIndex >= 0) {
      genderOptions[optionIndex].selected = true
    }
    return genderOptions
  }

  _acceptOptions (value) {
    const acceptOptions = [
      { label: I18n.t('customer.accept'), value: ACCEPT.TRUE},
      { label: I18n.t('customer.not_accept'), value: ACCEPT.FALCE}
    ]
    const selectedOption = value === ACCEPT.TRUE ? 0 : 1
    acceptOptions[selectedOption].selected = true
    return acceptOptions
  }

  _isDisableSubmitButton () {
    const { customer_code, last_name_kana, first_name_kana, tel01, tel02, tel03, zip01, zip02 } = this.props.registerCustomer
    return !(customer_code && last_name_kana && first_name_kana && tel01 && tel02 && tel03 && !!((zip01 && zip02) || (!zip01 && !zip02)))
  }

  _isDisableAutoFillAddressButton () {
    const { zip01, zip02 } = this.props.registerCustomer
    return !(zip01 && zip01.length === ZIPCODE1_LENGTH && zip02 && zip02.length === ZIPCODE2_LENGTH)
  }

  _sliceInputText (input, maxLength) {
    return input.slice(0, maxLength) || null
  }

  render () {
    const { registerCustomer, regions } = this.props
    return (
      <View style={styles.mainArea}>
        <View style={styles.leftArea}>
          <RowLayout title={I18n.t('customer.customer_code')} required >
            <TextInput
              style={styles.textInput}
              onChangeText={(input) => this.props.onChangeForm('customer_code', input)}
              value={registerCustomer.customer_code}
              maxLength={CUSTOMER_CODE_LENGTH}/>
          </RowLayout>
          <RowLayout title={I18n.t('customer.name')}>
            <View style={styles.row}>
              <View style={styles.leftTextContainerOfDouble}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(input) => {
                    const inputText = this._sliceInputText(input, NAME_LENGTH)
                    this.props.onChangeForm('last_name', inputText)}
                  }
                  value={registerCustomer.last_name}
                  placeholder={I18n.t('customer.last_name')} />
              </View>
              <View style={styles.rightTextContainerOfDoufle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(input) => {
                    const inputText = this._sliceInputText(input, NAME_LENGTH)
                    this.props.onChangeForm('first_name', inputText)}
                  }
                  value={registerCustomer.first_name}
                  placeholder={I18n.t('customer.first_name')} />
              </View>
            </View>
          </RowLayout>
          <RowLayout title={I18n.t('customer.name_kana')} required >
            <View style={styles.row}>
              <View style={styles.leftTextContainerOfDouble}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(input) => {
                    const inputText = this._sliceInputText(input, NAME_LENGTH)
                    this.props.onChangeForm('last_name_kana', inputText)}
                  }
                  value={registerCustomer.last_name_kana}
                  placeholder={I18n.t('customer.last_name_kana')} />
              </View>
              <View style={styles.rightTextContainerOfDoufle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(input) => {
                    const inputText = this._sliceInputText(input, NAME_LENGTH)
                    this.props.onChangeForm('first_name_kana', inputText)}
                  }
                  value={registerCustomer.first_name_kana}
                  placeholder={I18n.t('customer.first_name_kana')} />
              </View>
            </View>
          </RowLayout>
          <RowLayout title={I18n.t('customer.customer_phone_number')} required >
            <View style={styles.row}>
              <View style={styles.leftTextContainerOfTriple}>
                <CardNumberBox
                  maxLength={TEL_LENGTH}
                  style={styles.textInput}
                  value={registerCustomer.tel01}
                  onChange={(input) => this.props.onChangeForm('tel01', input)} />
              </View>
              <View style={styles.centerTextContainerOfTriple}>
                <CardNumberBox
                  maxLength={TEL_LENGTH}
                  style={styles.textInput}
                  value={registerCustomer.tel02}
                  onChange={(input) => this.props.onChangeForm('tel02', input)} />
              </View>
              <View style={styles.rightTextContainerOfTriple}>
                <CardNumberBox
                  maxLength={TEL_LENGTH}
                  style={styles.textInput}
                  value={registerCustomer.tel03}
                  onChange={(input) => this.props.onChangeForm('tel03', input)} />
              </View>
            </View>
          </RowLayout>
          <RowLayout title={I18n.t('customer.customer_birthday')}>
            <TextInput
              ref="birthday"
              style={styles.textInput}
              onChangeText={(input) => {}}
              value={registerCustomer.birthday} />
            <TouchableOpacity
              style={styles.datePickerContainer}
              onPress={() => {
                this._showDateTimePicker(
                  I18n.t('customer.customer_birthday'),
                  registerCustomer.birthday,
                  'birthday')
              }}
            />
          </RowLayout>
          <RowLayout title={I18n.t('customer.gender')}>
            <RadioGroup
              options={this._genderOptions()}
              wrap
              onSelectItem={(input) => this.props.onChangeForm('gender', input.value)}
              useProceedButton
            />
          </RowLayout>
        </View>
        <View style={styles.rightArea}>
          <RowLayout title={I18n.t('customer.customer_postal_code')}>
            <View style={styles.row}>
              <View style={styles.leftTextContainerOfTriple}>
                <CardNumberBox
                  maxLength={ZIPCODE1_LENGTH}
                  style={styles.textInput}
                  value={registerCustomer.zip01}
                  onChange={(input) => this.props.onChangeForm('zip01', input)} />
              </View>
              <View style={styles.centerTextContainerOfTriple}>
                <CardNumberBox
                  maxLength={ZIPCODE2_LENGTH}
                  style={styles.textInput}
                  value={registerCustomer.zip02}
                  onChange={(input) => this.props.onChangeForm('zip02', input)} />
              </View>
              <View style={styles.rightTextContainerOfTriple}>
                <OptionalCommandButton
                  style={styles.addressInputButton}
                  text={I18n.t('customer.autofill_address')}
                  onPress={() => this.props.onInputAddress(registerCustomer.zip01, registerCustomer.zip02, regions)}
                  disabled={this._isDisableAutoFillAddressButton()}/>
              </View>
            </View>
          </RowLayout>
          <RowLayout title={I18n.t('customer.city')}>
            <Select
              style={styles.selectInput}
              items={regions}
              selectedItem={{'name': registerCustomer.city || I18n.t('common.select_one')}}
              displayProperty="name"
              onItemChange={item => {
                const pref = item.id ? item.region_code : null
                const city = item.id ? item.name : null
                this.props.onChangeForm('pref', pref)
                this.props.onChangeForm('city', city)}
              }
            />
          </RowLayout>
          <RowLayout title={I18n.t('customer.locality')}>
            <TextInput
              style={styles.textInput}
              onChangeText={(input) => {
                const inputText = this._sliceInputText(input, LOCALITY_LENGTH)
                this.props.onChangeForm('locality', inputText)}
              }
              value={registerCustomer.locality} />
          </RowLayout>
          <RowLayout title={I18n.t('customer.street')}>
            <TextInput
              style={styles.textInput}
              onChangeText={(input) => {
                const inputText = this._sliceInputText(input, LOCALITY_LENGTH)
                this.props.onChangeForm('street', input)}
              }
              value={registerCustomer.street}
              maxLength={STREET_LENGTH} />
          </RowLayout>
          <RowLayout title={I18n.t('customer.mailaddress')}>
            <TextInput
              style={styles.textInput}
              onChangeText={(input) => this.props.onChangeForm('email', input)}
              value={registerCustomer.email}
              maxLength={EMAIL_LENGTH} />
          </RowLayout>
          <RowLayout title={I18n.t('customer.accept_dm')}>
            <RadioGroup
              options={this._acceptOptions(registerCustomer.accept_dm)}
              wrap
              onSelectItem={(input) => this.props.onChangeForm('accept_dm', input.value)}
              useProceedButton
            />
          </RowLayout>
          <RowLayout title={I18n.t('customer.accept_email')}>
            <RadioGroup
              options={this._acceptOptions(registerCustomer.accept_email)}
              wrap
              onSelectItem={(input) => this.props.onChangeForm('accept_email', input.value)}
              useProceedButton
            />
          </RowLayout>
          <View style={styles.submitButtonContainer}>
            <CommandButton
              style={styles.submitButton}
              text={I18n.t('customer.register')}
              onPress={() => this.props.onSubmit(registerCustomer)}
              disabled={this._isDisableSubmitButton()}/>
          </View>
          <KeyboardSpace toHalf />
        </View>
        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          mode='date'
          date={this.state.date}
          titleIOS={this.state.datePickerTitle}
          titleStyle={styles.datePickerTitleLabel}
          confirmTextIOS={I18n.t('common.ok')}
          confirmTextStyle={styles.datePickerConfirmLabel}
          cancelTextIOS={I18n.t('common.cancel')}
          cancelTextStyle={styles.datePickerCancelLabel}
          minimumDate={new Date('1900/01/01')}
          maximumDate={new Date()}
          onConfirm={(date) => { this._handleDatePicked(date) }}
          onCancel={this._hideDateTimePicker}
          neverDisableConfirmIOS />
      </View>
    )
  }
}
