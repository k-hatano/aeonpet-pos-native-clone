'use strict'

import React, { Component } from 'react'
import { View, TextInput } from 'react-native'
import I18n from 'i18n-js'
import CustomerList from './CustomerList'
import CustomerDetail from './CustomerDetail'
import SubstituteCustomerSearchFormContainer from '../containers/SubstituteCustomerSearchFormContainer'
import Modal from 'common/components/widgets/Modal'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import { OptionalCommandButton, CommandButton } from 'common/components/elements/StandardButton'
import styles from '../styles/CustomerSearch'
import PropTypes from 'prop-types'
import { Actions } from 'react-native-router-flux'
import { PERMISSION_CODES } from '../../staff/models'
import OrderActionButton from '../containers/OrderActionButton'
import KeyboardSpace from '../../../common/components/layout/KeyboardSpace'
import { CUSTOMER_CODE_LENGTH } from '../models'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import ConfirmView from 'common/components/widgets/ConfirmView'

class ModalContent extends Component {
  render () {
    return (
      <View>
        <ModalFrame
          style={styles.layoutRoot}
          title={I18n.t('customer.forgotten_card')}
          onClose={() => this.props.onClose()} >
          <SubstituteCustomerSearchFormContainer />
        </ModalFrame>
        <KeyboardSpace />
      </View>
    )
  }
}

export default class CustomerSearchForm extends Component {
  static propTypes = {
    customers: PropTypes.array || PropTypes.object,
    customer: PropTypes.object,
    onSelectCustomer: PropTypes.func,
    onEnterCustomerCode: PropTypes.func,
    shopName: PropTypes.string,
    isEditable: PropTypes.bool,
    isStaffModalShowing: PropTypes.bool
  }
  constructor (props) {
    super(props)
    this.state = {customerCode: ''}
  }

  async componentDidMount () {
    this._setBarcodeReaderEvent()
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  _setBarcodeReaderEvent () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(body => {
      if (this.props.isStaffModalShowing) {
        Modal.close()
        this.setState({
          customerCode: ''
        })
        this.props.onEnterCustomerCode(body.trim())
      }
    })
  }

  _onForgotCard () {
    Modal.open(ModalContent, {
      isBackgroundVisible: true,
      props: {
        onClose: () => {
          Modal.close()
        }
      }
    })
  }

  _renderCustomerSearchResult () {
    if (this.props.customer === null) {
      return (
        <View style={styles.customerListContainer}>
          <CustomerList
            customers={this.props.customers}
            onSelectCustomer={this.props.onSelectCustomer} />
        </View>
      )
    } else {
      return (
        <View style={styles.customerDetailContainer}>
          <CustomerDetail
            customer={this.props.customer}
            searchObj={this}
            shopName={this.props.shopName} />
        </View>
      )
    }
  }

  _onPressNewOrderButton (customer) {
    if (!this.props.isEditable) {
      Actions.cartPage({ customer: customer, mode: 'order' })
    } else {
      ConfirmView.show(I18n.t('message.C-03-I001'), () => {
        Actions.cartPage({ customer: customer, mode: 'order' })
      })
    }
  }

  render () {
    const customer = this.props.customer
    const disable = !this.state.customerCode || this.state.customerCode.length === 0
    return (
      <View style={styles.customerSearchArea}>
        <View style={styles.customerSearchContainer}>
          <View style={styles.searchInputWrap}>
            <View style={styles.searchInputContainerLeft}>
              <TextInput
                maxLength={CUSTOMER_CODE_LENGTH}
                style={styles.customerCodeForm}
                onChangeText={(customerCode) => this.setState({customerCode})}
                value={this.state.customerCode}
                placeholder={I18n.t('customer.customer_code')} />
            </View>
            <View style={styles.searchInputContainerRight}>
              <View style={styles.buttonStandardContainer}>
                <CommandButton
                  onPress={() => this.props.onEnterCustomerCode(this.state.customerCode)}
                  text={I18n.t('common.search')}
                  style={styles.buttonStandard}
                  disabled={disable}
                />
              </View>
              <View style={styles.borderedButtonContainer}>
                <OptionalCommandButton
                  isBorderButton
                  text={I18n.t('customer.forgotten_card')}
                  style={styles.borderedButton}
                  onPress={() => this._onForgotCard()}
                />
              </View>
            </View>
          </View>
        </View>
        {this._renderCustomerSearchResult()}
        { customer !== null &&
          <View style={styles.customerSearchFooterContainer}>
            <View style={styles.customerSearchFooterButtonContainer}>
              <OrderActionButton
                style={styles.customerSearchFooterButton}
                title={I18n.t('customer.new_order')}
                onPress={() => this._onPressNewOrderButton(customer)}
                permissionCode={PERMISSION_CODES.NEW_ORDER}
                skipSelectStaff />
            </View>
          </View>
        }
      </View>
    )
  }
}
