'use strict'

import React, { Component } from 'react'
import { Text, View, TextInput } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import { isHalfWidthAndSymbol } from '../../../common/utils/validations.js'
import styles from '../styles/CustomerCodeChange'
import PropTypes from 'prop-types'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import { CUSTOMER_CODE_LENGTH } from '../models'

export default class ChangeCustomerCode extends Component {
  static propTypes = {
    onFindCustomers: PropTypes.func,
    onClose: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      customerCode: '',
    }
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(body => {
      this.setState({customerCode: body.trim()})
    })
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  render () {
    const { onUpdateCustomerCode, onClose } = this.props
    const disable = this.state.customerCode.length === 0
      || this.state.customerCode.length > CUSTOMER_CODE_LENGTH
      || !isHalfWidthAndSymbol(this.state.customerCode)

    return (
      <View>
        <View style={styles.layoutRoot}>
          <View style={styles.searchFormArea}>
            <View style={styles.searchInputWrapper}>
              <TextInput
                maxLength={CUSTOMER_CODE_LENGTH}
                style={styles.searchInput}
                value={this.state.customerCode}
                onChangeText={(customerCode) => this.setState({customerCode})}/>
            </View>
            <CommandButton
              text={I18n.t('customer.register')}
              style={styles.searchButton}
              onPress={() => {
                  onUpdateCustomerCode(this.state.customerCode)
                  onClose()
                }
              }
              disabled={disable}/>
          </View>
          <Text style={styles.messageText}>
            {I18n.t('customer.validate_customer_code')}
          </Text>
        </View>
      </View>
    )
  }
}
