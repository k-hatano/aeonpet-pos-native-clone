'use strict'

import React, { Component } from 'react'
import { Text, View, ListView } from 'react-native'
import I18n from 'i18n-js'
import CustomerList from './CustomerList'
import styles from '../styles/CustomerSearch'

export default class CustomerSearchResult extends Component {
  constructor (props) {
    super(props)

    this.state = {
      perPage: 10,
      totalPage: 6,
      source: []
    }
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  _goToPage = ({ page }) => {
    // todo get data for page
    const { data } = this.props
    let newData = data
    if (page === this.state.totalPage) {
      newData = newData.filter((item, index) => index < 5)
    }
    this.setState({
      source: this.dataSource.cloneWithRows(newData)
    })
  }

  componentWillMount () {
    const { data } = this.props
    this.setState({
      source: this.dataSource.cloneWithRows(data)
    })
  }

  render () {
    const { onItemChoose, data } = this.props

    return (
      <View style={styles.searchResultContainer}>
        <View style={styles.searchResultHeaderRow}>
          <Text style={styles.searchResultHeaderText}>
            {I18n.t('common.search_result')}
          </Text>
        </View>
        <View style={styles.searchResultListViewData}>
          <CustomerList data={data} onItemChoose={customer => onItemChoose(customer)} />
        </View>
      </View>
    )
  }
}
