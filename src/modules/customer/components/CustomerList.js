'use strict'

import React, { Component } from 'react'
import { Text, View, ListView, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/CustomerSearch'
import componentStyles from '../styles/CustomerList'
import PropTypes from 'prop-types'

export default class CustomerList extends Component {
  static propTypes = {
    onSelectCustomer: PropTypes.func,
    customers: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
  }

  render () {
    return (
      <View>
        <View style={styles.searchResultHeaderRow}>
          <Text style={styles.searchResultHeaderText}>
            {I18n.t('common.search_result')}
          </Text>
        </View>
        <View>
          {this._renderCustomerList()}
        </View>
      </View>
    )
  }

  _renderCustomerList () {
    const { onSelectCustomer, customers } = this.props
    const source = this.dataSource.cloneWithRows(customers)
    return (
      <ListView
        enableEmptySections
        style={componentStyles.listContainer}
        dataSource={source}
        renderRow={(customer, index) =>
          <TouchableOpacity
            key={index}
            style={componentStyles.row}
            onPress={() => {
              onSelectCustomer(customer)
            }}
          >
            <View style={componentStyles.customerItem}>
              <View style={componentStyles.customerItemTop}>
                <Text style={componentStyles.rowText}>
                  {I18n.t('customer.customer_code')} : {customer.customer_code}
                </Text>
              </View>
              <View style={componentStyles.customerItemTop}>
                <Text style={componentStyles.rowText}>
                  {I18n.t('customer.name_kana')} : {customer.last_name_kana + '  ' + customer.first_name_kana + I18n.t('customer.honorific')}
                </Text>
              </View>
              <View style={componentStyles.customerItemBottom}>
                <View style={componentStyles.customerItemBottomLeft}>
                  <Text style={componentStyles.rowText}>
                    {I18n.t('customer.customer_phone_number')} : {'***-****-' + customer.tel03}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>} />
    )
  }
}
