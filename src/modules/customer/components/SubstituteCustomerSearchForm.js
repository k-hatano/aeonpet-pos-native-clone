'use strict'

import React, { Component } from 'react'
import { Text, View, TextInput } from 'react-native'
import I18n from 'i18n-js'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import componentStyles from '../styles/SubstituteCustomerSearchForm'
import PropTypes from 'prop-types'
import { SEARCH_TEL_LENGTH } from '../models'

export default class SubstituteCustomerSearchForm extends Component {
  static propTypes = {
    onFindCustomers: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      lastNameKana: '',
      phoneNumber: ''
    }
  }

  render () {
    const { onFindCustomers } = this.props
    const { lastNameKana, phoneNumber } = this.state
    const disable = !(lastNameKana.length > 0 && lastNameKana.match(/^[\u30A0-\u30FF]+$/) && phoneNumber.length === SEARCH_TEL_LENGTH && phoneNumber.match(/^\d*$/))

    return (
      <View>
        <View style={componentStyles.contentContainer}>
          <View style={componentStyles.rowWrapper}>
            <View style={componentStyles.rowTitleWrapper}>
              <Text style={componentStyles.rowTitleText}>{I18n.t('customer.family_name_phonetic')}</Text>
            </View>
            <View style={componentStyles.rowInputWrapper}>
              <TextInput
                style={componentStyles.rowInput}
                placeholder={I18n.t('customer.family_name_phonetic')}
                value={lastNameKana}
                onChangeText={(input) => {
                  const inputName = input.substr(0, 10)
                  this.setState({lastNameKana: inputName})
                }} />
            </View>
          </View>
          <View style={componentStyles.rowWrapper}>
            <View style={componentStyles.rowTitleWrapper}>
              <Text style={componentStyles.rowTitleText}>{I18n.t('customer.phone_number_last_4')}</Text>
            </View>
            <View style={componentStyles.rowInputWrapper}>
              <TextInput
                maxLength={SEARCH_TEL_LENGTH}
                style={componentStyles.rowInput}
                placeholder={I18n.t('customer.phone_number_last_4')}
                onChangeText={(phoneNumber) => this.setState({phoneNumber})} />
            </View>
          </View>
          <View style={componentStyles.rowButtonWrapper}>
            <CommandButton
              text={I18n.t('common.search')}
              style={componentStyles.rowButtonContainerStyle}
              onPress={() => onFindCustomers(lastNameKana, phoneNumber)}
              disabled={disable} />
          </View>
        </View>
      </View>
    )
  }
}
