'use strict'

import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import { CUSTOMER_MEMO_MAX_LENGTH } from '../models'
import styles from '../styles/CustomerMemo'
import { CommandButton, WeakProceedButton } from '../../../common/components/elements/StandardButton'
import ConfirmView from '../../../common/components/widgets/ConfirmView'

export default class CustomerMemo extends Component {
  static propTypes = {
    customer: PropTypes.object,
    onSaveMemo: PropTypes.func,
    isEditable: PropTypes.bool,
    onSetIsEditable: PropTypes.func,
    memoText: PropTypes.string,
    onChangeMemoText: PropTypes.func
  }

  _onCancel () {
    ConfirmView.show(
      I18n.t('message.C-03-I001'), () => {
        this.props.onChangeMemoText(this.props.customer.memo)
        this.props.onSetIsEditable(false)
      }
    )
  }

  _onSave = async() => {
    const {customer, onSaveMemo} = this.props
    const isSuccess = await onSaveMemo(this.props.memoText, customer)
    if (isSuccess) {
      this.props.onSetIsEditable(false)
    }
  }

  render () {
    const memoLength = this.props.memoText === null ? 0 : this.props.memoText.length
    const buttonText = this.props.isEditable ? I18n.t('common.save') : I18n.t('common.edit')
    return (
      <View style={styles.customerMemoArea}>
        <View style={styles.customerMemoContainer}>
          <View style={styles.customerMemoHeader}>
            <View style={styles.customerMemoHeaderLeft}>
              {
                this.props.isEditable &&
                <WeakProceedButton
                  onPress={() => this._onCancel()}
                  text={I18n.t('common.cancel')}
                  style={styles.customerMemoCancelButton} />
              }
            </View>
            <View style={styles.customerMemoHeaderRight}>
              <View style={styles.customerMemoLengthContainer}>
                <Text style={styles.customerMemoLengthLabel}>
                  {memoLength + '/' + CUSTOMER_MEMO_MAX_LENGTH + I18n.t('customer.chara')}
                </Text>
              </View>
              <View style={styles.rightButtonContainer}>
                <CommandButton
                  onPress={() => { this.props.isEditable ? this._onSave() : this.props.onSetIsEditable(true) }}
                  text={buttonText}
                  style={styles.customerMemoButton} />
              </View>
            </View>
          </View>
        </View>
        <View>
          <TextInput
            ref='customerMemo'
            style={this.props.isEditable ? styles.inputContentEditable : styles.inputContent}
            multiline={true}
            editable={this.props.isEditable}
            maxLength={CUSTOMER_MEMO_MAX_LENGTH}
            value={this.props.memoText}
            onChangeText={(text) => this.props.onChangeMemoText(text)}
          />
        </View>
      </View>
    )
  }
}

