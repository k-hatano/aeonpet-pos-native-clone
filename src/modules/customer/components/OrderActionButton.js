import React from 'react'
import PropTypes from 'prop-types'
import CustomPropTypes from 'common/components/CustomPropTypes'
import { ProceedButton } from '../../../common/components/elements/StandardButton'

export default class OrderActionButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    style: CustomPropTypes.Style,
    onPress: PropTypes.func
  }

  render () {
    return (
      <ProceedButton
        style={this.props.style}
        text={this.props.title}
        onPress={this.props.onPress}/>
    )
  }
}