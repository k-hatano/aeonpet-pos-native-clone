export const MODULE_NAME = 'customer'

export const CUSTOMER_CODE_LENGTH = 20
export const NAME_LENGTH = 10
export const TEL_LENGTH = 10
export const TEL_WHOLE_LENGTH = 12
export const ZIPCODE1_LENGTH = 3
export const ZIPCODE2_LENGTH = 4
export const LOCALITY_LENGTH = 50
export const STREET_LENGTH = 50
export const EMAIL_LENGTH = 255
export const CUSTOMER_MEMO_MAX_LENGTH = 1000
export const SEARCH_TEL_LENGTH = 4

export const CUSTOMER_GENDER = {
  MALE: 1,
  FEMALE: 2,
  UNSELECTED: 3
}

export const ACCEPT = {
  FALCE: 0,
  TRUE: 1
}

/**
 * @typedef {Object} CustomerDto
 * @property {string} customer_code
 * @property {string} first_name_kana
 * @property {string} last_name_kana
 * @property {string} phone_number
 * @property {string} postal_code
 */

/**
 * @typedef {CustomerDto} CustomerDetailDto
 * @property {string} card_type_name
 * @property {string} customer_rank
 * @property {string} first_name
 * @property {string} last_name
 * @property {string} region
 * @property {string} locality
 * @property {Array.<Array>} expire_points - ex. [['2017-08-15', '100'], ['2017-09-01', '200']]
 * @property {string} birthday
 * @property {string} phone_number
 * @property {string} point
 * @property {string} buy_times
 * @property {string} last_buy_date - ex. '2017-07-12'
 * @property {string} last_buy_shop_name
 */
