import { handleActions } from 'redux-actions'
import {
  listOrders,
  selectOrder,
  listPaymentMethods,
  setOrderProperty,
  setSelectedProperties,
  updateIsReturned,
  setBillEJournal,
  setIsBillPrinted,
  setReturnedOrder,
  setCashirDeposit,
  getReadyCashChanger,
  resetReadyCashChanger,
  setProgress,
  setCashierError
} from './actions'
import Decimal from 'decimal.js'

const defaultState = {
  orders: [],
  selectedOrder: null,
  paymentMethods: [],
  orderProperty: [],
  selectedProperties: [],
  isReturned: false,
  billEJournal: null,
  isBillPrinted: false,
  returnedOrder: null,
  cashirDeposit: Decimal(0),
  progress: false,
  cashierError: null,
  isCashChangerReady: false,
}

const handlers = {
  [listOrders]: (state, action) => ({
    ...state,
    orders: action.payload,
    selectedOrder: null
  }),
  [selectOrder]: (state, action) => ({
    ...state,
    selectedOrder: action.payload
  }),
  [listPaymentMethods]: (state, action) => ({
    ...state,
    paymentMethods: action.payload
  }),
  [setOrderProperty]: (state, action) => ({
    ...state,
    orderProperty: action.payload
  }),
  [setSelectedProperties]: (state, action) => ({
    ...state,
    selectedProperties: action.payload
  }),
  [updateIsReturned]: (state, action) => ({
    ...state,
    isReturned: action.payload
  }),
  [setBillEJournal]: (state, action) => ({
    ...state,
    billEJournal: action.payload
  }),
  [setIsBillPrinted]: (state, action) => ({
    ...state,
    isBillPrinted: action.payload
  }),
  [setReturnedOrder]: (state, action) => ({
    ...state,
    returnedOrder: action.payload
  }),
  [setCashirDeposit]: (state, action) => ({
    ...state,
    cashirDeposit: action.payload
  }),
  [getReadyCashChanger]: (state) => ({
    ...state,
    isCashChangerReady: true
  }),
  [resetReadyCashChanger]: (state) => ({
    ...state,
    isCashChangerReady: false
  }),
  [setProgress]: (state, action) => ({
    ...state,
    progress: action.payload
  }),
  [setCashierError]: (state, action) => ({
    ...state,
    cashierError: action.payload
  })
}
export default handleActions(handlers, defaultState)
