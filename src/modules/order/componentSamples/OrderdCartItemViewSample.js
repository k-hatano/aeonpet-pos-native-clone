import * as _ from 'underscore'
import OrderdCartItemView from '../components/OrderedCartItemView'
import CartDataConverter from '../../cart/models/CartDataConverter'
import { addProductToSampleCart, completeSampleCart, createSampleCart, payByPaymentMethodType } from '../samples'
import { sampleProductMap } from '../../product/samples'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'
import OrderedCart from '../models/OrderedCart'

const properties = []

const exp = {
  name: 'OrderdCartItemView',
  component: OrderdCartItemView,
  properties: properties,
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}

export default exp

const init = async () => {
  let createOrderedCart = (cart) => {
    cart = cart.complete({}, 1, [], new Date())
    const order = (new CartDataConverter(cart)).convert()
    const orderedCart = new OrderedCart(order)
    return orderedCart
  }
  await (async () => {
    let cart = createSampleCart()
    cart = await addProductToSampleCart(cart, sampleProductMap.standardA, 1)
    cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
    cart = completeSampleCart(cart, new Date())
    const orderedCart = createOrderedCart(cart)
    properties.push({
      title: 'Simple',
      property: {
        item: orderedCart.items.get(0)
      }
    })
  })()
  await (async () => {
    let cart = createSampleCart()
    cart = await addProductToSampleCart(cart, sampleProductMap.sale_a_1, 2)
    cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
    cart = completeSampleCart(cart, new Date())
    const orderedCart = createOrderedCart(cart)
    properties.push({
      title: 'With Sale Amount Discount',
      property: {
        item: orderedCart.items.get(0)
      }
    })
  })()
  await (async () => {
    let cart = createSampleCart()
    cart = await addProductToSampleCart(cart, sampleProductMap.sale_b_1, 2)
    cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
    cart = completeSampleCart(cart, new Date())
    const orderedCart = createOrderedCart(cart)
    properties.push({
      title: 'With Sale Rate Discount',
      property: {
        item: orderedCart.items.get(0)
      }
    })
  })()
}
init()
