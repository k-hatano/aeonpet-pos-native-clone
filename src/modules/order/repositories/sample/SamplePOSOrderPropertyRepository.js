import logger from 'common/utils/logger'
import NetworkError from 'common/errors/NetworkError'
import { waitAsync } from '../../../../common/utils/waitAsync'
import generateUuid from '../../../../common/utils/generateUuid'

export default class SamplePOSOrderPropertyRepository {

  async findAll () {
    return [
      {
        id: generateUuid(),
        name: '性別',
        options: [{
          "status":1,
          "code":"M",
          "name":"男性"
        },{
          "status":1,
          "code":"F",
          "name":"女性"
        },{
          "status":0,
          "code":"U",
          "name":"不明"
        }],
        required: 1,
        sort_order: 1
      },
      {
        id: generateUuid(),
        name: '年齢',
        options: [{
          "status":1,
          "code":"20-30",
          "name":"20〜30歳"
        },{
          "status":1,
          "code":"30-40",
          "name":"30〜40歳"
        },{
          "status":1,
          "code":"40-50",
          "name":"40〜50歳"
        },{
          "status":1,
          "code":"50-60",
          "name":"50〜60歳"
        },{
          "status":1,
          "code":"60-",
          "name":"60〜歳"
        }],
        required: 0,
        sort_order: 2,
      },
      {
        id: generateUuid(),
        "name": "来店頻度",
        options: [{
          "status":1,
          "code":"new",
          "name":"新規"
        },{
          "status":1,
          "code":"repeater",
          "name":"リピーター"
        }],
        required: 0,
        sort_order: 3
      }
    ]
  }
}
