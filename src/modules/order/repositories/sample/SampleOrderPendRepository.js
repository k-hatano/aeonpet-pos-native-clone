export default class SampleOrderPendRepository {
  _pendedOrders = []

  async findAll () {
    return this._pendedOrders.map(SampleOrderPendRepository._toReturnDto)
  }

  async findById (id) {
    return SampleOrderPendRepository._toReturnDto(
      this._pendedOrders.find(pendedOrder => pendedOrder.id === id))
  }

  async save (order) {
    this._pendedOrders.push(JSON.parse(JSON.stringify(order)))
  }

  async clearById (id) {
    const index = this._pendedOrders.findIndex(pendedOrder => pendedOrder.id === id)
    if (index > 0) {
      this._pendedOrders.splice(index, 1)
    }
  }

  async updateById (order) {
    const index = this._pendedOrders.findIndex(pendedOrder => pendedOrder.id === order.id)
    if (index > 0) {
      this._pendedOrders[index] = JSON.parse(JSON.stringify(order))
    }
  }

  async clearAll () {
    this._pendedOrders = []
  }

  static _toReturnDto (dto) {
    if (dto) {
      return {
        ...dto,
        status: 0,
        is_pended: true
      }
    }
  }
}
