import logger from 'common/utils/logger'
import {
  createSampleOrders,
  sampleOrders,
  samplePromotionOrders,
  samplePromotionOrderItems, createSampleServerOrders
} from '../../samples'
import NetworkError from 'common/errors/NetworkError'
import { waitAsync } from '../../../../common/utils/waitAsync'
import { STATUS } from '../../models'

export default class SampleOrderRepository {
  /** @type {Array.<OrderDto>} */
  _orders = []

  _localOrders = []

  _initialized = false

  async _init () {
    if (!this._initialized) {
      const localOrders = await createSampleOrders()
      this._localOrders = localOrders.map(order => {
        return {
          data: order,
          is_pushed: false
        }
      })
      this._orders = await createSampleServerOrders()
      this._initialized = true
    }
  }

  async fetchByConditions (conditions) {
    await this._init()
    return this._orders
  }

  async fetchByOrderId (orderId) {
    return this._orders.find(order => order.id === orderId)
  }

  async fetchPromotionOrderByOrderId (orderId) {
    return samplePromotionOrders.filter(promotionOrder => (promotionOrder.order_id === orderId))
  }

  async fetchPromotionOrderItemsByOrderId (orderId) {
    return samplePromotionOrderItems.filter(promotionOrderItem => (promotionOrderItem.order_id === orderId))
  }

  async fetchOrdersByCustomerCode (customerCode) {
    return [...this._orders]
  }

  async push (order) {
    console.log('SampleOrderRepository.push', order)
    this._orders.push(order)
    return {}
  }

  async save (order) {
    console.log('SampleOrderRepository.save', order)
    this._localOrders.push({
      data: order,
      is_pushed: false
    })
  }

  async findByOrderNumber (posOrderNumber) {
    await this._init()
    const order = {...this._orders[0]}
    console.log(order)
    order.pos_order.pos_order_number = posOrderNumber
    return order
  }

  async findById (orderId) {
    await this._init()
    return this._localOrders.find(order => order.data.id === orderId).data
  }

  async findByDate (fromDate, toDate) {
    await this._init()
    return this._localOrders.map(order => order.data)
  }

  /**
   *
   * @return {Promise.<Array.<OrderDto>>}
   */
  async findUnpushedOrders () {
    await this._init()
    return this._localOrders.filter(order => !order.is_pushed).map(order => order.data)
  }

  async syncLocalToRemoteById (orderId) {
    await this._init()
    await waitAsync(1000)
    const order = this._localOrders.find(order => order.data.id === orderId)
    if (!order) {
      await logger.error('Order not found')
    }
    if (order.is_pushed) {
      return
    }
    const randomNumber = Math.random()
    console.log({randomNumber})
    if (randomNumber > 0.33) { // 1/3の確率で送信に失敗する
      order.is_pushed = true
      this._orders.push(order)
      return {}
    } else {
      throw new NetworkError()
    }
  }

  async syncLocalToRemote () {
    await this._init()
    this._localOrders.forEach(order => {
      if (!order.is_pushed) {
        order.is_pushed = true
        this._orders.push(order)
      }
    })
    await waitAsync(1000)
  }

  async updateLocal (order) {
    try {
      order.status = STATUS.RETURNED
      return order
    } catch (error) {
      //
    }
  }

  async clearBeforeDate (timestamp) {
    try {
      console.log('Delete pushed order')
    } catch (error) {
      //
    }
  }
}
