import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const Order = sequelize.define(
  'Order',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    data: Sequelize.STRING,
    client_created_at: Sequelize.INTEGER,
    is_pushed: Sequelize.BOOLEAN,
    is_minus: Sequelize.BOOLEAN,
    status: Sequelize.INTEGER,
    updated_at: Sequelize.INTEGER
  }, {
    tableName: 'orders',
    underscored: true,
    timestamps: false,
    scopes: {
    }
  }
)

export default Order
