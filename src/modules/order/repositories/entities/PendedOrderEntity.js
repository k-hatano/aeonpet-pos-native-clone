import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const PendedOrder = sequelize.define(
  'PendedOrder',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    data: Sequelize.STRING,
    client_created_at: Sequelize.INTEGER,
    is_pended: Sequelize.BOOLEAN,
    is_minus: Sequelize.BOOLEAN,
    status: Sequelize.INTEGER,
    updated_at: Sequelize.INTEGER
  }, {
    tableName: 'pended_orders',
    underscored: true,
    timestamps: false,
    scopes: {
    }
  }
)

export default PendedOrder
