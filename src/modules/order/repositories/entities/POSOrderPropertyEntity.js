import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const POSOrderProperty = sequelize.define(
  'POSOrderProperty',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    options: Sequelize.TEXT,
    default_value: Sequelize.STRING,
    required: Sequelize.INTEGER,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'pos_order_properties',
    underscored: true,
    timestamps: false
  }
)

export default POSOrderProperty
