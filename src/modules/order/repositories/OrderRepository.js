import SampleOrderRepository from './sample/SampleOrderRepository'
import StandardOrderRepository from './standard/StandardOrderRepository'
import TrainingOrderRepository from './training/TrainingOrderRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OrderRepository {
  static _implement = new SampleOrderRepository()

  static async fetchByConditions (condition) {
    return this._implement.fetchByConditions(condition)
  }

  static async fetchByOrderId (orderId) {
    return this._implement.fetchByOrderId(orderId)
  }

  static async fetchLastPosOrderByCashierId (cashierId) {
    return this._implement.fetchLastPosOrderByCashierId(cashierId)
  }

  static async fetchPromotionOrderByOrderId (orderId) {
    // TODO Refactoring PromotionOrderRepositoryへ
    return this._implement.fetchPromotionOrderByOrderId(orderId)
  }

  static async fetchPromotionOrderItemsByOrderId (orderId) {
    return this._implement.fetchPromotionOrderItemsByOrderId(orderId)
  }

  static async fetchOrdersByCustomerCode (customerCode) {
    return this._implement.fetchOrdersByCustomerCode(customerCode)
  }

  static async save (order, isPushed = false, transaction = undefined) {
    return this._implement.save(order, isPushed, transaction)
  }

  static async findByOrderNumber (posOrderNumber) {
    return this._implement.findByOrderNumber(posOrderNumber)
  }

  static async push (order) {
    return this._implement.push(order)
  }

  static async cancelRemoteOrder (orderId, params) {
    return this._implement.cancelRemoteOrder(orderId, params)
  }

  static async syncLocalToRemote () {
    return this._implement.syncLocalToRemote()
  }

  static async syncLocalToRemoteById (orderId) {
    return this._implement.syncLocalToRemoteById(orderId)
  }

  static async findById (orderId) {
    return this._implement.findById(orderId)
  }

  static async findByDate (fromDate, toDate) {
    return this._implement.findByDate(fromDate, toDate)
  }

  static async findUnpushedOrders () {
    return this._implement.findUnpushedOrders()
  }

  static async updateLocal (order) {
    return this._implement.updateLocal(order)
  }

  /**
   * XX日以上前の送信済みの受注を削除する
   */
  static async clearBeforeDate (timestamp) {
    return this._implement.clearBeforeDate(timestamp)
  }

  /**
   * 全ての会計を強制的に送信済みにする。
   * @return {Promise}
   */
  static async saveIsPushedAll () {
    return this._implement.saveIsPushedAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardOrderRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingOrderRepository()
        break

      default:
        this._implement = new SampleOrderRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('OrderRepository', (context) => {
  OrderRepository.switchImplement(context)
})
