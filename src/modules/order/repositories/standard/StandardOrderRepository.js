import logger from 'common/utils/logger'
import OrderEntity from '../entities/OrderEntity'
import AppEvents from 'common/AppEvents'
import { handleAxiosError, handleDatabaseError } from 'common/errors'
import fetcher from 'common/models/fetcher'
import { encodeCriteria } from 'common/utils/searchUtils'
import DbError from '../../../../common/errors/DbError'
import NetworkError from '../../../../common/errors/NetworkError'
import { REQUEST_ERROR_CODE } from '../../../../common/errors'
import { INIT_STATUS } from 'modules/order/models'
import MessageError from '../../../../common/errors/MessageError'

export default class StandardOrderRepository {
  async fetchByConditions (conditions) {
    try {
      const query = ({
        ...encodeCriteria(conditions),
        with: 'posOrder;orderItems;orderPayments',
        limit: 300
      })
      const orders = await fetcher.get('orders', query)
      return orders.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchByOrderId (orderId) {
    try {
      const order = await fetcher.get('orders/accessibles/' + orderId)
      return order.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchLastPosOrderByCashierId (cashierId) {
    try {
      const result = await fetcher.get(`pos-orders`, {
        limit: 1,
        orderBy: 'pos_order_number_sequence',
        sortedBy: 'desc',
        search: `cashier_id:${cashierId}`
      })
      if (Array.isArray(result.data) && result.data.length === 1) {
        return result.data[0]
      } else {
        return null
      }
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchPromotionOrderByOrderId (orderId) {
    try {
      const promotionOrders = await fetcher.get('promotion-orders', encodeCriteria({ order_id: orderId }))
      return promotionOrders.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchPromotionOrderItemsByOrderId (orderId) {
    try {
      const promotionOrderItems = await fetcher.get('promotion-order-items', encodeCriteria({ order_id: orderId }))
      return promotionOrderItems.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async save (order, isPushed = false, transaction = undefined) {
    try {
      const options = transaction ? { transaction } : {}
      await OrderEntity.create({
        id: order.id,
        data: JSON.stringify(order),
        client_created_at: order.pos_order.client_created_at,
        pos_order_number: order.pos_order.pos_order_number,
        pos_order_number_sequence: order.pos_order_number_sequence,
        is_pushed: isPushed,
        is_minus: order.is_minus,
        status: order.status,
        updated_at: order.updated_at
      }, options)
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async findByDate (fromDate, toDate) {
    try {
      const orders = await OrderEntity.findAll({
        where: {
          client_created_at: {
            $gte: fromDate,
            $lte: toDate
          }
        },
        order: [
          ['client_created_at', 'DESC']
        ]
      })
      return orders.map(orderEntity => this._mapEntityToDto(orderEntity))
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async fetchOrdersByCustomerCode (customerCode) {
    try {
      const orders = await fetcher.get('orders/accessibles', {
        ...encodeCriteria({
          customer_code: customerCode,
          status: INIT_STATUS
        }),
        with: 'posOrder;orderItems;orderPayments',
        limit: 300
      })
      return orders.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async findUnpushedOrders () {
    try {
      const orders = await OrderEntity.findAll({
        where: {
          is_pushed: false
        }
      })
      return orders.map(orderEntity => this._mapEntityToDto(orderEntity))
    } catch (error) {
      handleDatabaseError(error, 'orders')
    }
  }

  async push (order) {
    try {
      const response = await fetcher.post('orders', order)
      this._assingResponseToOrder(order, response)
      return order
    } catch (error) {
      try {
        handleAxiosError(error, 'orders')
      } catch (networkError) {
        if (networkError instanceof NetworkError) {
          if (networkError.type !== NetworkError.TYPE.OFFLINE) {
            // オフライン以外のエラーの場合、会計の重複など調査が必要になる可能性があるので、
            // 送信したデータの詳細を記録しておく
            logger.error(
              'StandardOrderRepository.push Network Error ' +
              JSON.stringify(order)
            )
          }
        }
        throw networkError
      }
    }
  }

  async cancelRemoteOrder (orderId, params) {
    try {
      await fetcher.put(`orders/${orderId}/cancel`, params)
    } catch (error) {
      if (this._isCannotCancelError(error)) {
        throw new MessageError('message.F-02-E013')
      } else {
        handleAxiosError(error)
      }
    }
  }

  async syncLocalToRemoteById (orderId) {
    let order = null
    try {
      order = await OrderEntity.find({
        where: {
          id: orderId
        }
      })
      if (order.is_pushed) {
        return // 既に送信済みならなにもしない
      }
    } catch (error) {
      throw new DbError(error)
    }

    let data
    try {
      logger.debug('StandardOrderRepository.syncLocalToRemoteById ' + order.data)
      data = JSON.parse(order.data)
      const response = await fetcher.post('orders', data)
      this._assingResponseToOrder(data, response)
    } catch (error) {
      if (this._isDuplicatedError(error)) {
        // 重複データである = 既に送信済みでローカルが認識していなかっただけなので、成功として扱う
        logger.warning('Duplicated Order ' + order.data)
      } else {
        try {
          handleAxiosError(error)
        } catch (error2) {
          if (error2 instanceof NetworkError) {
            if (error2.type === NetworkError.TYPE.SERVER_ERROR) {
              // サーバに異常がある状態のため、調査用に詳細を記録しておく
              logger.error(
                'StandardOrderRepository.syncLocalToRemoteById Server Error ' +
                JSON.stringify(error.response) + ' ' +
                order.data
              )
            } else if (error2.type === NetworkError.TYPE.CLIENT_ERROR) {
              // 想定外の処理をクライアントが行った可能性が高いため、調査用に詳細を記録しておく
              logger.error(
                'StandardOrderRepository.syncLocalToRemoteById Client Error ' +
                JSON.stringify(error.response) + ' ' +
                order.data
              )
            }
          }
          throw error2
        }
      }
    }

    try {
      await OrderEntity.update(
        { is_pushed: true, data: JSON.stringify(data) },
        { where: { id: order.id } }
      )
      return data
    } catch (error) {
      throw new DbError(error)
    }
  }

  async syncLocalToRemote () {
    let orders
    try {
      orders = await this.findUnpushedOrders()
    } catch (error) {
      handleDatabaseError(error)
    }

    const errors = []
    const orderResults = []
    for (const i in orders) {
      try {
        const order = orders[i]
        const result = await this.syncLocalToRemoteById(order.id)
        orderResults.push(result)
      } catch (error) {
        errors.push(error)
        if (error instanceof NetworkError &&
          (error.type === NetworkError.TYPE.TIMEOUT || error.type === NetworkError.TYPE.OFFLINE)) {
          // タイムアウトとオフラインの場合、次を送信できる見込みがほとんどないので、この時点で終了する
          break
        }
      }
    }

    return { errors, orders: orderResults }
  }

  async findById (orderId) {
    try {
      let orderEntity = await OrderEntity.find({
        where: {
          id: orderId
        }
      })
      return orderEntity && this._mapEntityToDto(orderEntity)
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async updateLocal (selectedOrder) {
    try {
      await OrderEntity.update(
        {
          data: JSON.stringify(selectedOrder),
          status: selectedOrder.status
        },
        { where: { id: selectedOrder.id } }
      )

      let orderEntity = await OrderEntity.find({
        where: {
          id: selectedOrder.id
        }
      })
      return this._mapEntityToDto(orderEntity)
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async clearBeforeDate (timestamp) {
    try {
      await OrderEntity.destroy(
        {
          where: {
            is_pushed: true,
            client_created_at: {
              $lte: timestamp
            }
          }
        }
      )
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async saveIsPushedAll () {
    try {
      await OrderEntity.update({ is_pushed: true }, { where: {} })
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  _mapEntityToDto (orderEntity) {
    try {
      const dto = JSON.parse(orderEntity.data)
      dto.status = orderEntity.status
      dto.is_pushed = orderEntity.is_pushed
      return dto
    } catch (error) {
      console.log(error)
      logger.fatal('StandardOrderRepository._mapEntityToDto failed ', error)
    }
  }

  _assingResponseToOrder (order, response) {
    order.obtained_points = response.data.obtained_points
  }

  _isDuplicatedError (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.DUPLICATED_ENTRY
  }

  _isCannotCancelError (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.ORDER_CANCEL
  }

  async createSamples () {
    // const existingCount = await OrderEntity.count()
    // if (existingCount === 0) {
    //   // TODO
    // }
  }
}

AppEvents.onSampleDataCreate('StandardOrderRepository', async () => {
  return (new StandardOrderRepository()).createSamples()
})
