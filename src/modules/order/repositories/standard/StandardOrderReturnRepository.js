import { handleAxiosError } from 'common/errors'
import fetcher from 'common/models/fetcher'
import { STATUS } from '../../models'
import { encodeCriteria } from 'common/utils/searchUtils'

export default class StandardOrderRepository {
  async fetchReturnOrder (cart) {
    try {
      const returnOrder = await fetcher.put('orders/' + cart.id, {
        status: STATUS.RETURNED,
        shop_code: cart.shop.shop_code,
        cashier_id: cart.cashier.id,
        cashier_code: cart.cashier.cashier_code,
        staff_id: cart.staff.id,
        staff_name: cart.staff.name,
        staff_code: cart.staff.staff_code,
        pos_order: {
          pos_order_number: cart.posOrderNumber,
          pos_order_number_sequence: cart.orderNumberSequence
        }
      })
      return returnOrder.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchOrderPointTransactionByOrderId (orderId) {
    try {
      const orderPointTransactions = await fetcher.get('order-point-transactions', encodeCriteria({ order_id: orderId }))
      return orderPointTransactions.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
