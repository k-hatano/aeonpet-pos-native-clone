import POSOrderPropertyEntity from '../entities/POSOrderPropertyEntity'
import AppEvents from 'common/AppEvents'

export default class StandardPOSOrderPropertyRepository {
  async findAll () {
    const properties = await POSOrderPropertyEntity.findAll({
      raw: true
    })
    return properties.map(property => {
      return {
        ...property,
        options: JSON.parse(property.options)
      }
    })
  }

  async createSamples () {

  }
}

AppEvents.onSampleDataCreate('StandardPOSOrderPropertyRepository', async () => {
  return (new StandardPOSOrderPropertyRepository()).createSamples()
})
