import PendedOrderEntity from '../entities/PendedOrderEntity'
import { handleAxiosError, handleDatabaseError } from 'common/errors'

export default class StandardOrderPendRepository {
  async findAll () {
    try {
      const pendedOrders = await PendedOrderEntity.findAll({
        order: [
          ['client_created_at', 'DESC']
        ]
      })
      return pendedOrders.map(pendedOrderEntity => {
        let order = JSON.parse(pendedOrderEntity.data)
        order.status = pendedOrderEntity.status
        order.is_pended = pendedOrderEntity.is_pended
        return order
      })
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async findById (id) {
    try {
      const pendedOrder = await PendedOrderEntity.find({
        where: {
          id: id
        }
      })
      return pendedOrder
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async save (order) {
    try {
      await PendedOrderEntity.create({
        id: order.id,
        data: JSON.stringify(order),
        client_created_at: order.pos_order.client_created_at,
        is_pended: true,
        is_minus: 0,
        status: 0,
        updated_at: order.pos_order.client_created_at
      })
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async clearById (id) {
    try {
      await PendedOrderEntity.destroy(
        { where: { id: id } }
      )
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async updateById (order) {
    try {
      await PendedOrderEntity.update(
        {
          data: JSON.stringify(order),
          client_created_at: order.pos_order.client_created_at,
          updated_at: order.pos_order.client_created_at
        },
        { where: { id: order.id } }
      )
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async clearAll () {
    try {
      await PendedOrderEntity.destroy({ where: {} })
    } catch (error) {
      handleDatabaseError(error)
    }
  }
}
