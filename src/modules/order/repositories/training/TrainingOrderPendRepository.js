export default class TrainingOrderPendRepository {
  _pendedOrders = []

  async findAll () {
    return this._pendedOrders.map(pendOrderEntity => {
      let order = pendOrderEntity.data
      order.status = pendOrderEntity.status
      order.is_pended = pendOrderEntity.is_pended
      return order
    })
  }

  async findById (id) {
    const pendedOrder = this._pendedOrders.find(order => order.id === id)
    return pendedOrder
  }

  async save (order) {
    this._pendedOrders.push({
      id: order.id,
      data: order,
      client_created_at: order.pos_order.client_created_at,
      is_pended: true,
      is_minus: 0,
      status: 0,
      updated_at: order.pos_order.client_created_at
    })
  }

  async clearById (id) {
    this._pendedOrders = this._pendedOrders.filter(order =>
      order.id !== id
    )
  }

  async updateById (order) {
    this._pendedOrders.forEach(pendedOrder => {
      if (pendedOrder.id === order.id) {
        pendedOrder.data = order
        pendedOrder.client_created_at = order.pos_order.client_created_at
        pendedOrder.updated_at = order.pos_order.client_created_at
      }
    })
  }

  async clearAll () {
    this._pendedOrders = []
  }
}
