import { IS_MINUS, STATUS } from '../../models'
import Decimal from 'decimal.js'
import { toUnixTimestamp } from 'common/utils/dateTime'
import { waitAsync } from '../../../../common/utils/waitAsync'

export default class TrainingOrderRepository {
  _trainingOrders = []

  async fetchByConditions (conditions) {
    return this._trainingOrders
  }

  async fetchByOrderId (orderId) {
    const order = this._trainingOrders.find(order => order.id === orderId)
    return order.data
  }

  async fetchPromotionOrderByOrderId (orderId) {
    const order = this._trainingOrders.find(order => order.id === orderId)
    return order.data.promotion_orders
  }

  async save (order) {
    const date = new Date()
    order.created_at = toUnixTimestamp(date)
    order.updated_at = toUnixTimestamp(date)
    this._trainingOrders.push({
      id: order.id,
      data: order,
      client_created_at: order.pos_order.client_created_at,
      is_pushed: true,
      is_minus: order.is_minus,
      status: order.is_minus === IS_MINUS.ORDER ? STATUS.COMPLETED : STATUS.NEW,
      updated_at: order.pos_order.client_created_at
    })
  }

  async findByDate (fromDate, toDate) {
    return this._trainingOrders.map(order => order.data)
  }

  async findById (orderId) {
    const found = this._trainingOrders.find(order => order.id === orderId)
    return found && found.data
  }

  async syncLocalToRemoteById (orderId) {
    const order = this._trainingOrders.find(order => order.id === orderId)
    // if (!order) {
    //   await logger.error('Order not found')
    // }

    let data
    try {
      data = order.data
      data.obtained_points = data.obtained_points || Decimal(data.total_paid_taxless).mul(0.02)
      return data
    } catch (error) {
    }
  }

  async updateLocal (orderId) {
    this._trainingOrders.forEach(order => {
      if (order.id === orderId) {
        order.status = STATUS.RETURNED
        order.data.status = STATUS.RETURNED
        order.data.updated_at = toUnixTimestamp(new Date())
      }
    })
  }

  async push (order) {
    try {
      order.obtained_points = Decimal(order.total_paid_taxless).mul(0.02)
      return order
    } catch (error) {
    }
  }

  async cancelRemoteOrder (orderId, params) {
    const order = this._trainingOrders.find(order => order.id === orderId)
    order.data = {
      ...order.data,
      pos_order: {
        ...order.data.pos_order,
        ...params,
        canceled_at: toUnixTimestamp(new Date())
      },
      status: STATUS.CANCELED
    }
    order.status = STATUS.CANCELED
    await waitAsync(500)
  }

  async findUnpushedOrders () {
    return []
  }
}
