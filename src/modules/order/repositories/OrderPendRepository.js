import SampleOrderPendRepository from './sample/SampleOrderPendRepository'
import StandardOrderPendRepository from './standard/StandardOrderPendRepository'
import TrainingOrderPendRepository from './training/TrainingOrderPendRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OrderPendRepository {
  static _implement = new SampleOrderPendRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static async findById (id) {
    return this._implement.findById(id)
  }

  static async save (order) {
    return this._implement.save(order)
  }

  static async clearById (id) {
    return this._implement.clearById(id)
  }

  static async updateById (order) {
    return this._implement.updateById(order)
  }

  static async clearAll () {
    return this._implement.clearAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardOrderPendRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingOrderPendRepository()
        break

      default:
        this._implement = new SampleOrderPendRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('OrderPendRepository', (context) => {
  OrderPendRepository.switchImplement(context)
})
