import SamplePOSOrderPropertyRepository from './sample/SamplePOSOrderPropertyRepository'
import StandardPOSOrderPropertyRepository from './standard/StandardPOSOrderPropertyRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OrderPropertyRepository {
  static _implement = new StandardPOSOrderPropertyRepository()

  static async findAll() {
    return this._implement.findAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardPOSOrderPropertyRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardPOSOrderPropertyRepository()
        break

      default:
        this._implement = new SamplePOSOrderPropertyRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('OrderPropertyRepository', (context) => {
  OrderPropertyRepository.switchImplement(context)
})
