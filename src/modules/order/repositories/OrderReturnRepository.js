import SampleOrderReturnRepository from './sample/SampleOrderReturnRepository'
import StandardOrderReturnRepository from './standard/StandardOrderReturnRepository'
import TrainingOrderReturnRepository from './training/TrainingOrderReturnRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OrderReturnRepository {
  static _implement = new SampleOrderReturnRepository()

  static async fetchReturnOrder (cart) {
    return this._implement.fetchReturnOrder(cart)
  }

  static async fetchOrderPointTransactionByOrderId (orderId) {
    return this._implement.fetchOrderPointTransactionByOrderId(orderId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardOrderReturnRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingOrderReturnRepository()
        break

      default:
        this._implement = new SampleOrderReturnRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('OrderReturnRepository', (context) => {
  OrderReturnRepository.switchImplement(context)
})
