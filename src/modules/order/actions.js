import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listOrders = createAction(`${MODULE_NAME}_listOrders`)
export const selectOrder = createAction(`${MODULE_NAME}_selectOrder`)
export const listPaymentMethods = createAction(`${MODULE_NAME}_listPaymentMethod`)
export const setOrderProperty = createAction(`${MODULE_NAME}_setOrderProperty`)
export const setSelectedProperties = createAction(`${MODULE_NAME}_setSelectedProperties`)
export const updateIsReturned = createAction(`${MODULE_NAME}_updateIsReturned`)
export const setBillEJournal = createAction(`${MODULE_NAME}_setBillEJournal`)
export const setIsBillPrinted = createAction(`${MODULE_NAME}_setIsBillPrinted`)
export const setReturnedOrder = createAction(`${MODULE_NAME}_setReturnedOrder`)
export const setCashirDeposit = createAction(`${MODULE_NAME}_setCashirDeposit`)
export const getReadyCashChanger = createAction(`${MODULE_NAME}_getReadyCashChanger`)
export const resetReadyCashChanger = createAction(`${MODULE_NAME}_resetReadyCashChanger`)
export const setProgress = createAction(`${MODULE_NAME}_setProgress`)
export const setCashierError  = createAction(`${MODULE_NAME}_setCashierError`)
