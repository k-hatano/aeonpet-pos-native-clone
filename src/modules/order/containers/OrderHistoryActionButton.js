import OrderHistoryActionButton from '../components/OrderHistoryActionButton'
import connectPermissionButton from '../../staff/containers/connectPermissionButton'

export default connectPermissionButton(OrderHistoryActionButton)
