import OrderPropertyCashierModalView from '../components/OrderPropertyCashierModalView'
import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import Modal from 'common/components/widgets/Modal'

const mapDispatchToProps = dispatch => ({
  onUpdateDepositAmount: async (depositAmount) => {
    dispatch(actions.setCashirDeposit(depositAmount))
  },
  onGetReadyCashChanger: () => {
    dispatch(actions.getReadyCashChanger())
  },
  onEmdCashChanger: () => {
    dispatch(actions.resetReadyCashChanger())
  },
  onError: async (error) => {
    dispatch(actions.setCashierError(error))
  },
})

const mapStateToProps = state => ({
  cashirDeposit: state[MODULE_NAME].cashirDeposit,
  cashierError: state[MODULE_NAME].cashierError,
  isCashChangerReady: state[MODULE_NAME].isCashChangerReady,
})

const showSelectOrderPropertyCashierViewModel = connect(mapStateToProps, mapDispatchToProps)(
  OrderPropertyCashierModalView
)
export default showSelectOrderPropertyCashierViewModel

export const showSelectOrderPropertyCashierModal = (cashPaymentAmount, currency, onCompleted, onCancel) => {
  Modal.open(showSelectOrderPropertyCashierViewModel, {
    enableBackgroundClose: false,
    isBackgroundVisible: true,
    props: {
      cashPaymentAmount: cashPaymentAmount,
      currency: currency,
      onClose: (isCancel, newPaymentAmount) => {
        Modal.close().then(() => {
          if (isCancel) {
            onCancel()
          } else {
            onCompleted(newPaymentAmount)
          }
        })
      }
    }
  })
}
