import I18n from 'i18n-js'
import { connect } from 'react-redux'
import { MODULE_NAME, STATUS } from '../models'
import OrderListView from '../components/OrderListView'
import { selectOrder, setReturnedOrder } from '../actions'
import AlertView from '../../../common/components/widgets/AlertView'
import OrderRepository from '../repositories/OrderRepository'
import * as _ from 'underscore'
import PaymentRepository from 'modules/payment/repositories/PaymentRepository'

const mapDispatchToProps = dispatch => ({
  onSelect: async (order) => {
    try {
      const selectedOrder = await OrderRepository.fetchByOrderId(order.id)
      const promotionOrders = await OrderRepository.fetchPromotionOrderByOrderId(order.id)
      const promotionOrderItems = await OrderRepository.fetchPromotionOrderItemsByOrderId(order.id)
      selectedOrder.promotion_orders = promotionOrders
      selectedOrder.promotion_order_items = promotionOrderItems
      selectedOrder.order_payments = _.sortBy(selectedOrder.order_payments, item => item.payment_method_type)
      dispatch(selectOrder(selectedOrder))

      let returnedOrder = null
      if (selectedOrder.status === STATUS.RETURNED) {
        const conditions = {
          is_minus: 1,
          origin_order_id: selectedOrder.id
        }
        const fetchedReturnedOrder = await OrderRepository.fetchByConditions(conditions)
        returnedOrder = fetchedReturnedOrder.length > 0 ? fetchedReturnedOrder[0] : null
        dispatch(setReturnedOrder(returnedOrder))
      }
    } catch (error) {
      AlertView.show(I18n.t('message.F-01-E002', { process: I18n.t('order.get_order_detail'), error: error.message }))
    }
  }
})

const mapStateToProps = state => ({
  orders: state[MODULE_NAME].orders
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderListView)
