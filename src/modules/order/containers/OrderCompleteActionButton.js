import OrderCompleteActionButton from '../components/OrderCompleteActionButton'
import connectPermissionButton from '../../staff/containers/connectPermissionButton'

export default connectPermissionButton(OrderCompleteActionButton)
