import { connect } from 'react-redux'
import ReturnCompleteView from '../components/ReturnCompleteView'
import {} from '../actions'
import { MODULE_NAME } from '../models'
import { MODULE_NAME as CART_MODULE_NAME } from '../../cart/models'
import AlertView from '../../../common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  // onAction: async () => {
  //   try {
  //     const models = await ModelRepository.findAll()
  //     dispatch(listModels(models))
  //   } catch (error) {
  //     AlertView.show('Error')
  //   }
  // }
})

const mapStateToProps = state => ({
  cart: state[CART_MODULE_NAME].cart
})

export default connect(mapStateToProps, mapDispatchToProps)(ReturnCompleteView)
