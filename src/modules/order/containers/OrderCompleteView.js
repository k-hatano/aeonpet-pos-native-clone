import { connect } from 'react-redux'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'
import OrderCompleteView from '../components/OrderCompleteView'
import { updateIsReturned, setBillEJournal } from '../actions'
import { MODULE_NAME, STATUS } from '../models'
import { MODULE_NAME as CART_MODULE_NAME } from '../../cart/models'
import { cancelOrder, printOrderBill, reprintOrderReceipt } from '../services'
import CartDataConverter from '../../cart/models/CartDataConverter'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { Actions } from 'react-native-router-flux'
import {TaxFreeReceiptPanelModal} from 'modules/taxFree/containers/TaxFreeReceiptPanel'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { RECEIPT_TYPE } from 'modules/printer/models'
import CartManager from 'modules/cart/models/CartManager'
import { formatPriceWithCurrency } from 'common/utils/formats'
import Decimal from 'decimal.js'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { canCreateTaxfreeReceipt, fetchOrderStatus, fetchTaxFreeEJournals } from '../../taxFree/services'
import { PAYMENT_METHOD_TYPE } from 'modules/payment/models'
import { orderStatusToLabel } from 'modules/order/models'
import { getIsTrainingModeFromState, getSettingFromState } from '../../setting/models'

const mapDispatchToProps = dispatch => ({
  onInit: () => {
    dispatch(updateIsReturned(false))
    dispatch(setBillEJournal(null))
  },
  onPressReprintReceipt: async (cart) => {
    const converter = new CartDataConverter(cart)
    const order = converter.convert()
    await reprintOrderReceipt(dispatch, order, cart.isTraining)
  },
  onPressPrintIssueBill: async (cart) => {
    await printOrderBill(dispatch, cart)
    const bills = await EJournalRepository.findOrFetchByOrderNumber(cart.posOrderNumber, RECEIPT_TYPE.BILL, true)
    if (bills.length > 0) {
      dispatch(setBillEJournal(bills[0]))
    }
  },
  onPressReturn: async (cart) => {
    const converter = new CartDataConverter(cart)
    const order = converter.convert()
    const options = {
      total: formatPriceWithCurrency(new Decimal(order.total_paid_taxed), order.currency),
      payment: order.order_payments.map(payment => {
        if (payment.payment_method_type !== PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT &&
          payment.payment_method_type !== PAYMENT_METHOD_TYPE.GIFT_SURPLUS) {
          return payment.payment_method_name
        }
      }).join('\n　　　　　')
    }

    if (await ConfirmView.showAsync(I18n.t('message.F-02-I002', options))) {
      let result
      const cartManager = new CartManager()
      result = await cartManager.returnOrder(dispatch, order, cart.isTraining)

      if (result) {
        dispatch(updateIsReturned(true))
        Actions.redirectPage({redirectTo: 'cart'})
      }
    }
    return true
  },
  onPressPrintTaxFreeSheets: async (order) => {
    const taxFreeEJournals = await fetchTaxFreeEJournals(order)
    // ローカルに免税帳票があれば再印刷にする。
    if (taxFreeEJournals.isComplete) {
      TaxFreeReceiptPanelModal(taxFreeEJournals.sales[0], taxFreeEJournals.customer[0], taxFreeEJournals.shipping[0])
    } else {
      if (await canCreateTaxfreeReceipt()) {
        const orderStatus = await fetchOrderStatus(order.id)
        // 最新の売上の状態をチェックして、返品もしくはレジマイナス済みならエラーにする。
        if (orderStatus === STATUS.RETURNED || orderStatus === STATUS.CANCELED) {
          await AlertView.showAsync(I18n.t('message.F-02-E004', { process: orderStatusToLabel(orderStatus) }))
        } else {
          Actions.taxFree({ order })
        }
      } else {
        await AlertView.showAsync(I18n.t('message.K-01-E003'))
      }
    }
  },
  onCancel: async (cart, isTrainingMode, currentStaff) => {
    const converter = new CartDataConverter(cart)
    const order = converter.convert()
    const result = await cancelOrder(order, currentStaff, isTrainingMode, dispatch)
    if (result) {
      Actions.redirectPage({redirectTo: 'cart'})
    }
  }
})

const mapStateToProps = state => ({
  cart: state[CART_MODULE_NAME].cart,
  isReturned: state[MODULE_NAME].isReturned,
  isTrainingMode: getIsTrainingModeFromState(state),
  billEJournal: state[MODULE_NAME].billEJournal,
  currentStaff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onPressReprintReceipt: async () => {
      if (stateProps.isReturned) {
        await AlertView.showAsync(I18n.t('message.F-02-E004', {process: I18n.t('order.process_returned')})) // TODO 翻訳対応
      } else {
        return dispatchProps.onPressReprintReceipt(stateProps.cart)
      }
    },
    onPressPrintIssueBill: async () => {
      return dispatchProps.onPressPrintIssueBill(stateProps.cart)
    },
    onPressReturn: async () => {
      return dispatchProps.onPressReturn(stateProps.cart)
    },
    onPressPrintTaxFreeSheets: async () => {
      const converter = new CartDataConverter(stateProps.cart)
      const order = converter.convert()
      const taxFreeSalesEjournal = await EJournalRepository.findByOrderNumber(order.pos_order.pos_order_number, RECEIPT_TYPE.TAXFREE_SALES)
      const taxFreeCustomerEjournal = await EJournalRepository.findByOrderNumber(order.pos_order.pos_order_number, RECEIPT_TYPE.TAXFREE_CUSTOMER)
      const taxFreeShippingEjournal = await EJournalRepository.findByOrderNumber(order.pos_order.pos_order_number, RECEIPT_TYPE.TAXFREE_SHIPPING)
      if (taxFreeSalesEjournal && taxFreeSalesEjournal.length > 0 && taxFreeCustomerEjournal && taxFreeCustomerEjournal.length > 0) {
        // TODO 配列長の判定が不十分 or 不要な気がするので、要確認
        TaxFreeReceiptPanelModal(taxFreeSalesEjournal[0], taxFreeCustomerEjournal[0], taxFreeShippingEjournal[0])
      } else {
        dispatchProps.onPressPrintTaxFreeSheets(order)
      }
    },
    onCancel: async (cart) => {
      return dispatchProps.onCancel(cart, stateProps.isTrainingMode, stateProps.currentStaff)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(OrderCompleteView)
