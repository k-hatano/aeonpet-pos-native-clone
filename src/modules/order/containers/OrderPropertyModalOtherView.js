import OrderPropertyOtherModalView from '../components/OrderPropertyOtherModalView'
import { connect } from 'react-redux'
import Modal from 'common/components/widgets/Modal'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'

const mapDispatchToProps = dispatch => ({
  setProgress: (progress) => {
    dispatch(actions.setProgress(progress))
  }
})

const mapStateToProps = state => ({
  progress: state[MODULE_NAME].progress
})

const showSelectOrderPropertyOtherViewModel = connect(mapStateToProps, mapDispatchToProps)(OrderPropertyOtherModalView)
export default showSelectOrderPropertyOtherViewModel

export const showSelectOrderPropertyOtherModal = (cart, onCompleted, onCancel) => {
  Modal.open(showSelectOrderPropertyOtherViewModel, {
    enableBackgroundClose: false,
    isBackgroundVisible: true,
    props: {
      cart: cart,
      onClose: async (result, isFailed) => {
        Modal.close().then(() => {
          if (isFailed) {
            onCancel(result)
          } else {
            onCompleted(result)
          }
        })
      }
    }
  })
}
