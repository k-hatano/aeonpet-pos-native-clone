import OrderPropertyModalView from '../components/OrderPropertyModalView'
import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import Modal from 'common/components/widgets/Modal'

const mapDispatchToProps = dispatch => ({
  onSelectedOptions: async (key, value, currentSelectedOptions) => {
    currentSelectedOptions[key] = value
    dispatch(actions.setSelectedProperties(currentSelectedOptions))
  }
})

const mapStateToProps = state => ({
  orderProperty: state[MODULE_NAME].orderProperty,
  selectedProperties: state[MODULE_NAME].selectedProperties
})

const showSelectOrderPropertyViewModel = connect(mapStateToProps, mapDispatchToProps)(OrderPropertyModalView)
export default showSelectOrderPropertyViewModel

export const showSelectOrderPropertyModal = (cart, onCompleted) => {
  Modal.open(showSelectOrderPropertyViewModel, {
    enableBackgroundClose: false,
    isBackgroundVisible: true,
    props: {
      cart: cart,
      onClose: orderProperties => {
        Modal.close().then(() => {
          onCompleted(orderProperties)
        })
      }
    }
  })
}
