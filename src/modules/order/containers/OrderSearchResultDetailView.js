import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import OrderSearchResultDetailView from '../components/OrderSearchResultDetailView'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  selectedOrder: state[MODULE_NAME].selectedOrder
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderSearchResultDetailView)
