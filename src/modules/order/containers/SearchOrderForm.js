import I18n from 'i18n-js'
import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import { listOrders, selectOrder, listPaymentMethods } from '../actions'
import SearchOrderForm from '../components/SearchOrderForm'
import { loading } from 'common/sideEffects'
import OrderRepository from '../repositories/OrderRepository'
import AlertView from '../../../common/components/widgets/AlertView'
import PaymentMethodRepository from '../../payment/repositories/PaymentMethodRepository'
import * as _ from 'underscore'
import CustomerRepository from '../../customer/repositories/CustomerRepository'

const mapDispatchToProps = dispatch => ({
  onFind: async (conditions, customerCode, init) => {
    await loading(dispatch, async () => {
      try {
        if (customerCode) {
          const customer = await CustomerRepository.fetchByCustomerCode(customerCode)
          if (!customer) {
            dispatch(listOrders([]))
            AlertView.show(I18n.t('message.F-01-E001'))
            return
          }
          conditions.customer_id = customer.id
        }
        const orders = await OrderRepository.fetchByConditions(conditions)
        orders.forEach(order => {
          order.order_payments = _.sortBy(order.order_payments, item => item.payment_method_type)
        })
        dispatch(listOrders(orders))
        if (!init && orders.length === 0) {
          AlertView.show(I18n.t('message.F-01-E001'))
        }
      } catch (error) {
        dispatch(listOrders([]))
        AlertView.show(I18n.t('message.F-01-E002', { process: I18n.t('order.search_order'), error: error.message }))
      }
    })
  },
  onFindByPosOrderNumber: async (posOrderNumber) => {
    await loading(dispatch, async () => {
      try {
        const conditions = {
          'posOrder.pos_order_number': posOrderNumber
        }

        const orders = await OrderRepository.fetchByConditions(conditions)

        dispatch(listOrders(orders))
        if (orders.length === 0) {
          AlertView.show(I18n.t('message.F-01-E001'))
          return
        }

        if (orders.length === 1) {
          let selectedOrder = null
          let promotionOrders = null
          let promotionOrderItems = null
          await loading(dispatch, async () => {
            selectedOrder = await OrderRepository.fetchByOrderId(orders[0].id)
            promotionOrders = await OrderRepository.fetchPromotionOrderByOrderId(orders[0].id)
            promotionOrderItems = await OrderRepository.fetchPromotionOrderItemsByOrderId(orders[0].id)
          })
          selectedOrder.promotion_orders = promotionOrders
          selectedOrder.promotion_order_items = promotionOrderItems
          selectedOrder.order_payments = _.sortBy(selectedOrder.order_payments, item => item.payment_method_type)
          dispatch(selectOrder(selectedOrder))
        }
      } catch (error) {
        dispatch(listOrders([]))
        AlertView.show(I18n.t('message.F-01-E002', { process: I18n.t('order.search_order'), error: error.message }))
      }
    })
  },
  onFindPaymentMethods: async () => {
    try {
      const paymentMethods = await PaymentMethodRepository.findAll()
      dispatch(listPaymentMethods(paymentMethods))
    } catch (error) {
      return []
    }
  },
  onInit: async () => {
    await dispatch(listOrders([]))
  }
})

const mapStateToProps = state => ({
  orders: state[MODULE_NAME].orders,
  paymentMethods: state[MODULE_NAME].paymentMethods,
  selectedOrder: state[MODULE_NAME].selectedOrder
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchOrderForm)
