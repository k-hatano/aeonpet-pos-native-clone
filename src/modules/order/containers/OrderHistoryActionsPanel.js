import { connect } from 'react-redux'
import I18n from 'i18n-js'
import {MODULE_NAME, makeBillEJournal, makeProcessName, STATUS, IS_MINUS} from '../models'
import { MODULE_NAME as HOME_MODULE_NAME } from 'modules/home/models'
import OrderHistoryActionsPanel from '../components/OrderHistoryActionsPanel'
import OrderRepository from '../repositories/OrderRepository'
import OrderPendRepository from '../repositories/OrderPendRepository'
import AlertView from 'common/components/widgets/AlertView'
import OrderBillBuilder from '../models/OrderBillBuilder'
import { loading } from 'common/sideEffects'
import PrinterManager from 'modules/printer/models/PrinterManager'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { RECEIPT_TYPE } from 'modules/printer/models'
import { Actions } from 'react-native-router-flux'
import { selectOrder, setIsBillPrinted, setReturnedOrder } from '../actions'
import {
  cancelOrder, reprintCancelOrderReceipt,
  reprintCreditReceipt, reprintCreditRefundReceipt, reprintOrderReceipt,
  reprintReturnOrderReceipt
} from '../services'
import OrderedCart from '../models/OrderedCart'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { formatPriceWithCurrency } from 'common/utils/formats'
import Decimal from 'decimal.js'
import { listPendedOrders } from 'modules/home/actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import {TaxFreeReceiptPanelModal} from 'modules/taxFree/containers/TaxFreeReceiptPanel'
import CartManager from 'modules/cart/models/CartManager'
import { canCreateTaxfreeReceipt, fetchOrderStatus, fetchTaxFreeEJournals } from '../../taxFree/services'
import * as _ from 'underscore'
import { PAYMENT_METHOD_TYPE } from 'modules/payment/models'
import { orderStatusToLabel } from 'modules/order/models'
import ReceiptForShopBuilder from '../models/ReceiptForShopBuilder'
import { getIsTrainingModeFromState, getSettingFromState } from '../../setting/models'
import { isPaperNearEnd, isPaperEnd } from 'modules/printer/models/PrinterResult'

const mapDispatchToProps = dispatch => ({
  onPrintTaxFreeSheet: async (selectedOrder) => {
    const taxFreeEJournals = await fetchTaxFreeEJournals(selectedOrder)
    // ローカルに免税帳票があれば再印刷にする。
    if (taxFreeEJournals.isComplete) {
      TaxFreeReceiptPanelModal(taxFreeEJournals.sales[0], taxFreeEJournals.customer[0], taxFreeEJournals.shipping[0])
    } else {
      if (await canCreateTaxfreeReceipt()) {
        const orderStatus = await fetchOrderStatus(selectedOrder.id)
        // 最新の売上の状態をチェックして、返品もしくはレジマイナス済みならエラーにする。
        if (orderStatus === STATUS.RETURNED || orderStatus === STATUS.CANCELED) {
          await AlertView.showAsync(I18n.t('message.F-02-E004', { process: orderStatusToLabel(orderStatus) }))
        } else {
          Actions.taxFree({ order: selectedOrder })
        }
      } else {
        await AlertView.showAsync(I18n.t('message.K-01-E003'))
      }
    }
  },
  onReprintReceipt: async (selectedOrder, repositoryContext) => {
    const isTrainingMode = repositoryContext === REPOSITORY_CONTEXT.TRAINING
    if (selectedOrder.status === STATUS.CANCELED) {
      return reprintCancelOrderReceipt(selectedOrder, isTrainingMode, dispatch)
    } else if (selectedOrder.status === STATUS.RETURNED || selectedOrder.is_minus === IS_MINUS.RETURNED) {
      return reprintReturnOrderReceipt(dispatch, selectedOrder, isTrainingMode)
    } else {
      return reprintOrderReceipt(dispatch, selectedOrder, isTrainingMode)
    }
  },
  onReprintCreditReceipt: async (selectedOrder, repositoryContext) => {
    const isTrainingMode = repositoryContext === REPOSITORY_CONTEXT.TRAINING
    if (isTrainingMode) {
      // トレーニングモードの際はクレカ支払いがないので、フロー上ここは実行されないはず
    } else {
      if (selectedOrder.status === STATUS.RETURNED || selectedOrder.is_minus === IS_MINUS.RETURNED) {
        await reprintCreditRefundReceipt(dispatch, selectedOrder)
      } else {
        await reprintCreditReceipt(dispatch, selectedOrder)
      }
    }
  },
  onPrintBill: async (selectedOrder, repositoryContext) => {
    const messages = []
    const result = await loading(dispatch, async () => {
      let ejournals = null
      let bill = null
      let cart = null
      let ejournal = null
      let ejournalForShop = null
      const isTrainingMode = repositoryContext === REPOSITORY_CONTEXT.TRAINING
      try {
        const isSetPrinter = await PrinterManager.isSetPrinter()
        if (!isSetPrinter) {
          AlertView.show(I18n.t('message.F-02-E009'))
          return
        }
        await loading(dispatch, async () => {
          let order = null
          try {
            order = await OrderRepository.fetchByOrderId(selectedOrder.id)
          } catch (error) {
            order = selectedOrder
          }

          if (order.status === STATUS.RETURNED || selectedOrder.status === STATUS.CANCELED) {
            AlertView.show(I18n.t('message.F-02-E004', {process: makeProcessName(order)}))
            return false
          }
          ejournals = await EJournalRepository
            .findByOrderNumber(selectedOrder.pos_order.pos_order_number, RECEIPT_TYPE.BILL)
          if (ejournals && ejournals.length > 0) {
            AlertView.show(I18n.t('message.F-02-E008'))
            return false
          }
          try {
            ejournals = await EJournalRepository
              .fetchByOrderNumber(selectedOrder.pos_order.pos_order_number, RECEIPT_TYPE.BILL)
            if (ejournals && ejournals.length > 0) {
              AlertView.show(I18n.t('message.F-02-E008'))
              return false
            }
          } catch (error) {
            // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
          }

          const builder = new OrderBillBuilder()
          await builder.initializeAsync()
          cart = new OrderedCart(selectedOrder)
          cart = cart.setIsTrainingMode(isTrainingMode)
          bill = builder.buildOrderBill(cart, new Date())
          const printResult = await PrinterManager.print(bill)
          if (isPaperNearEnd(printResult)) {
            messages.push(I18n.t('printer.is_paper_near_end'))
          } else if(isPaperEnd(printResult)) {
            messages.push(I18n.t('printer.is_paper_end'))
          }
        })
      } catch (error) {
        messages.push(I18n.t('message.F-02-E003'))

        return false
      }
      try {
        ejournal = await makeBillEJournal(bill, cart, new Date())
        await EJournalRepository.save(ejournal)
        await EJournalRepository.pushById(ejournal.id)
      } catch (error) {
        // ユーザーへのエラー通知は行わない。
        return false
      }
      if (ejournal) {
        const builderForShop = new ReceiptForShopBuilder()
        await builderForShop.initializeAsync()
        const billForShop = builderForShop.buildReceiptForShop(JSON.parse(ejournal.data))
        await PrinterManager.print(billForShop)
        try {
          ejournalForShop = makeBillEJournal(billForShop, cart, new Date(), RECEIPT_TYPE.BILL_FOR_SHOP)
          await EJournalRepository.save(ejournalForShop)
          await EJournalRepository.pushById(ejournalForShop.id)
        } catch (error) {
          // ユーザーへのエラー通知は行わない
          return true
        } 
      }
      return true
    })
    for (const message of messages) {
      await AlertView.showAsync(message)
    }
    return result
  },
  onReturn: async (selectedOrder, moduleName, repositoryContext) => {
    const isTrainingMode = repositoryContext === REPOSITORY_CONTEXT.TRAINING
    const options = {
      total: formatPriceWithCurrency(new Decimal(selectedOrder.total_paid_taxed), selectedOrder.currency),
      payment: selectedOrder.order_payments.map(payment => {
        if (payment.payment_method_type !== PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT &&
          payment.payment_method_type !== PAYMENT_METHOD_TYPE.GIFT_SURPLUS) {
          return payment.payment_method_name
        }
      }).join('\n　　　　　')
    }
    ConfirmView.show(I18n.t('message.F-02-I002', options), async () => {
      let returnedOrder = null
      const cartManager = new CartManager()
      returnedOrder = await cartManager.returnOrder(dispatch, selectedOrder, isTrainingMode)

      if (returnedOrder) {
        returnedOrder.order_payments = _.sortBy(returnedOrder.order_payments, item => item.payment_method_type)
        dispatch(selectOrder(returnedOrder))
      }

      const conditions = {
        is_minus: 1,
        origin_order_id: selectedOrder.id
      }
      try {
        const fetchedReturnedOrder = await OrderRepository.fetchByConditions(conditions)
        if (fetchedReturnedOrder.length > 0) {
          dispatch(setReturnedOrder(fetchedReturnedOrder[0]))
        }
      } catch (error) {
        // 返品後に返品の情報取得に失敗しただけなので、メッセージを出して再度表示してもらう
        AlertView.show(I18n.t('message.F-02-E012'))
      }

      if (moduleName === HOME_MODULE_NAME) {
        Actions.home()
      } else if (moduleName === MODULE_NAME) {
        Actions.orderSearchPage()
      }
      return true
    }, async () => {
      return true
    })
  },
  onCancel: async (selectedOrder, isTrainingMode, currentStaff) => {
    const canceledOrder = await cancelOrder(selectedOrder, currentStaff, isTrainingMode, dispatch)
    if (canceledOrder) {
      dispatch(selectOrder(canceledOrder))
    }
  },
  onReorder: async (selectedOrder) => {
    try {

    } catch (error) {

    }
  },
  onFindEjournal: async (posOrderNumber) => {
    try {
      let isBillPrinted = false
      let billEjournals
      billEjournals = await EJournalRepository
        .findByOrderNumber(posOrderNumber, RECEIPT_TYPE.BILL)
      if (billEjournals && billEjournals.length > 0) {
        if (billEjournals[0].is_printed === 1) {
          isBillPrinted = true
        }
      }
      if ((!billEjournals || billEjournals.length === 0) && !isBillPrinted) {
        try {
          billEjournals = await EJournalRepository
            .fetchByOrderNumber(posOrderNumber, RECEIPT_TYPE.BILL)
          if (billEjournals && billEjournals.length > 0) {
            isBillPrinted = true
          }
        } catch (error) {
          // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
        }
      }
      dispatch(setIsBillPrinted(isBillPrinted))
    } catch (error) {
      dispatch(setIsBillPrinted(false))
    }
  },
  onDeletePendedOrder: async (order) => {
    ConfirmView.show(I18n.t('message.A-07-I001'), async () => {
      await OrderPendRepository.clearById(order.id)
      const pendedOrders = await OrderPendRepository.findAll()
      dispatch(listPendedOrders(pendedOrders))
      dispatch(selectOrder(null))
    })
  },
  onReturnPendedOrder: async (order) => {
    Actions.cartPage({ mode: 'order', pendedOrder: order })
  }
})

const mapStateToProps = state => ({
  selectedOrder: state[MODULE_NAME].selectedOrder,
  repositoryContext: state['setting'].settings[SettingKeys.COMMON.REPOSITORY_CONTEXT],
  isTrainingMode: getIsTrainingModeFromState(state),
  isBillPrinted: state[MODULE_NAME].isBillPrinted,
  currentStaff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF),
  cashierId: getSettingFromState(state, SettingKeys.COMMON.CASHIER_ID)
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...dispatchProps,
    ...stateProps,
    onReprintReceipt: async (selectedOrder) => {
      dispatchProps.onReprintReceipt(selectedOrder, stateProps.repositoryContext)
    },
    onPrintBill: async (selectedOrder) => {
      return dispatchProps.onPrintBill(selectedOrder, stateProps.repositoryContext)
    },
    onReturn: async (selectedOrder, moduleName) => {
      dispatchProps.onReturn(selectedOrder, moduleName, stateProps.repositoryContext)
    },
    onReprintCreditReceipt: async (selectedOrder) => {
      dispatchProps.onReprintCreditReceipt(selectedOrder, stateProps.repositoryContext)
    },
    onCancel: async (selectedOrder) => {
      return dispatchProps.onCancel(selectedOrder, stateProps.isTrainingMode, stateProps.currentStaff)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(OrderHistoryActionsPanel)
