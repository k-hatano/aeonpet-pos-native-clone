import I18n from 'i18n-js'
import _ from 'underscore'
import { makeBaseEJournal, RECEIPT_TYPE } from '../printer/models'
import POSOrderPropertyRepository from './repositories/POSOrderPropertyRepository'

export const MODULE_NAME = 'order'

export const PROMOTION_TYPE = {
  VALUE: 1,
  PERCENT: 2
}

export const PRINT_TITLE = {
  ORDER: 'receipt.title.order',
  BILL: 'order.receipt_title_bill',
  RETURN: 'order.receipt_title_return',
  VOID: 'order.receipt_title_void',
  REPRINT: 'order.receipt_title_reprint'
}

export const ORDER_TYPE = {
  ORDER: 1,
  RETURN: 2,
  CANCELED: 3
}

export const STATUS = {
  STATUS_FAILED: -1,
  NEW: 0,
  CONTRACTED: 1,
  STATUS_PAID: 2,
  SHIPPED: 3,
  COMPLETED: 4,
  CANCELED: 5,
  RETURNED: 6,
  RETURN_COMPLETED: 7
}

export const IS_MINUS = {
  ORDER: 0,
  RETURNED: 1
}

export const ORDER_EXTRA_DATA_KEY = {
  PAYMENT: 'vega',
  CART_ITEMS: 'before_prorate_cart_items'
}

export const INIT_STATUS = [STATUS.STATUS_PAID, STATUS.COMPLETED, STATUS.RETURNED]

export const IS_TAX_FREE = 1

export const makeProcessName = (selectedOrder) => {
  return orderStatusToLabel(selectedOrder.status)
}

export const orderStatusToLabel = (orderStatus) => {
  return orderStatus === STATUS.RETURNED
    ? I18n.t('order.process_returned')
    : I18n.t('order.process_canceled')
}

export const makeBillEJournal = (receipt, cart, createdAt, receiptType = RECEIPT_TYPE.BILL) => {
  const ejournal = makeBaseEJournal(I18n.t(PRINT_TITLE.BILL), receipt, receiptType, createdAt)
  ejournal.is_printed = 1
  ejournal.order_id = cart.id
  ejournal.staff_id = cart.staff.id
  ejournal.is_print_tax_stamp = cart.needsTaxStamp
  ejournal.pos_order_number = cart.posOrderNumber
  ejournal.amount = cart.totalTaxed.toFixed(4)
  return ejournal
}

export const makeReprintEJournal = (receipt, originEjournal, createdAt, receiptType = RECEIPT_TYPE.ORDER) => {
  const ejournal = makeBaseEJournal(I18n.t(PRINT_TITLE.REPRINT), receipt, receiptType, createdAt)
  ejournal.is_printed = 1
  ejournal.is_reprint = 1
  ejournal.order_id = originEjournal.order_id
  ejournal.staff_id = originEjournal.staff_id
  ejournal.is_print_tax_stamp = originEjournal.is_print_tax_stamp
  ejournal.pos_order_number = originEjournal.pos_order_number
  ejournal.amount = originEjournal.amount
  return ejournal
}

export const makeReturnEjournal = (receipt, returnCart, createdAt, receiptType = RECEIPT_TYPE.RETURN_ORDER) => {
  const ejournal = makeBaseEJournal(I18n.t(PRINT_TITLE.RETURN), receipt, receiptType, createdAt)
  ejournal.is_printed = 0
  ejournal.order_id = returnCart.id
  ejournal.staff_id = returnCart.staff.id
  ejournal.is_print_tax_stamp = false
  ejournal.pos_order_number = returnCart.posOrderNumber
  ejournal.amount = returnCart.totalTaxed.toFixed(4)
  return ejournal
}

/**
 *
 * @param receipt
 * @param {OrderedCart} canceledCart
 * @param createdAt
 */
export function makeCancelOrderEJournal (receipt, canceledCart, createdAt) {
  const ejournal = makeBaseEJournal(I18n.t(PRINT_TITLE.VOID), receipt, RECEIPT_TYPE.CANCEL_ORDER, createdAt)
  ejournal.is_printed = 0
  ejournal.order_id = canceledCart.id
  ejournal.staff_id = canceledCart.canceledStaffId
  ejournal.is_print_tax_stamp = false
  ejournal.pos_order_number = canceledCart.posOrderNumber
  ejournal.amount = canceledCart.totalTaxed.toFixed(4)
  return ejournal
}

export const makeReprintReturnEjournal = (receipt, returnCart, createdAt, receiptType = RECEIPT_TYPE.RETURN_ORDER) => {
  const ejournal = makeBaseEJournal(I18n.t(PRINT_TITLE.REPRINT), receipt, receiptType, createdAt)
  ejournal.is_printed = 1
  ejournal.is_reprint = 1
  ejournal.order_id = returnCart.id
  ejournal.staff_id = returnCart.staff.id
  ejournal.is_print_tax_stamp = false
  ejournal.pos_order_number = returnCart.posOrderNumber
  ejournal.amount = returnCart.totalTaxed.toFixed(4)
  return ejournal
}

export const getOrderPropertyInfo = async () => {
  const orderProperty = await POSOrderPropertyRepository.findAll()
  var selectedProperties = []
  for (var i = 0; i < orderProperty.length; i++) {
    selectedProperties[i] = null
  }
  return {
    orderProperty: orderProperty,
    selectedProperties: selectedProperties
  }
}

export const orderHasPaymentMethodType = (order, paymentMethodType) => {
  let payments = null
  if (Array.isArray(order.payments)) {
    payments = order.payments
  } else if (Array.isArray(order.order_payments)) {
    payments = order.order_payments
  }
  return !!payments && payments.some(payment => payment.payment_method_type === paymentMethodType)
}

const orderTypeToStatusesMap = {
  [ORDER_TYPE.ORDER]: [STATUS.STATUS_PAID, STATUS.COMPLETED, STATUS.RETURNED],
  [ORDER_TYPE.RETURN]: [STATUS.RETURN_COMPLETED],
  [ORDER_TYPE.CANCELED]: [STATUS.CANCELED]
}

/**
 *
 * @param {OrderDto} order
 */
export function getTypeOfOrder (order) {
  switch (order.status) {
    case STATUS.STATUS_PAID:
    case STATUS.RETURNED:
      return ORDER_TYPE.ORDER
    case STATUS.CANCELED:
      return ORDER_TYPE.CANCELED
    case STATUS.RETURN_COMPLETED:
      return ORDER_TYPE.RETURN
  }

  if (order.is_minus) {
    return ORDER_TYPE.RETURN
  }

  if (order.pos_order && order.pos_order.canceled_at) {
    return ORDER_TYPE.CANCELED
  }

  return ORDER_TYPE.ORDER
}

export function orderTabTitles (order) {
  switch (getTypeOfOrder(order)) {
    case ORDER_TYPE.ORDER:
    case ORDER_TYPE.CANCELED:
      return {
        total: I18n.t('order.total'),
        detail: I18n.t('order.detail')
      }
    case ORDER_TYPE.RETURN:
      return {
        total: I18n.t('order.return_total'),
        detail: I18n.t('order.return_detail')
      }
  }
}

/**
 *
 * @param {number[]} orderTypes
 */
export function orderTypesToSearchCondition (orderTypes) {
  return {
    status: _.chain(orderTypes)
      .map(orderType => orderTypeToStatusesMap[orderType])
      .flatten().uniq().value()
  }
}

/**
 * @typedef {Object} OrderDto
 * @property {string} id
 * @property {string} shop_id
 * @property {string} shop_name
 * // @property {string} user_id 本案件では使わない
 * @property {string} customer_code - 顧客コード
 * @property {integer} order_number
 * @property {integer} is_minus
 * @property {string} origin_order_id
 * @property {integer} status
 * @property {string} currency
 * @property {string} total_items_taxless
 * @property {string} total_items_taxed
 * @property {string} total_taxable_items_taxless
 * @property {string} total_taxable_items_taxed
 * @property {string} total_items_discount
 * @property {string} total_paid_taxless
 * @property {string} total_paid_taxed
 * @property {integer} used_point
 * @property {integer} obtained_points
 * @property {string} extra_data
 * // @property {integer} contracted_at
 * @property {integer} finalized_at
 * // @property {integer} deleted_at
 * // @property {integer} completed_at
 * @property {integer} created_at
 * @property {integer} updated_at
 * @property {integer} shop_mode
 * @property {Array<PromotionOrderDto>} promotion_orders
 * @property {Array<OrderItemDto>} order_items
 * @property {PosOrderDto} pos_order
 * @property {Array<PaymentDto>} payments
 * @property {Array<PosPaymentDto>} pos_payments
 */

/**
 * @typedef {Object} OrderItemDto
 * @property {string} id
 * @property {string} order_id
 * @property {string} product_id
 * @property {string} product_variant_id
 * // @property {string} bundle_no
 * @property {string} product_name
 * @property {string} product_short_name
 * @property {string} product_variant_name
 * @property {string} product_code
 * @property {string} sku
 * @property {string} article_number
 * @property {string} price_taxless
 * @property {string} price_taxed
 * @property {string} sales_price_taxless
 * @property {string} sales_price_taxed
 * @property {integer} tax_rule
 * @property {string} tax_rate
 * @property {integer} taxfree_type
 * @property {integer} tax_code
 * @property {integer} quantity
 * @property {integer} sort_order
 * @property {string} extra_data
 * @property {string} child_products_data
 * @property {integer} can_grant_points
 * @property {integer} is_sales_excluded
 * @property {string} created_at
 * @property {string} updated_at
 * @property {Array<PromotionOrderItemDto>} promotion_order_items
 */

/**
 * @typedef {Object} PromotionOrderDto
 * @property {string} id
 * @property {string} order_id
 * @property {number} tax_rate
 * @property {string} promotion_id
 * @property {string} promotion_name
 * @property {integer} is_minus
 * @property {integer} discount_type
 * @property {number} discount_percent
 * @property {string} subtotal_discounts_taxless
 * @property {string} subtotal_discounts_taxed
 * @property {string} discount_reason_id
 * @property {string} extra_data
 * @property {integer} created_at
 * @property {string} created_by
 * @property {integer} updated_at
 * @property {string} updated_by
 * @property {string} approved_by
 * @property {integer} approval_status
 */

/**
 * @typedef {Object} PromotionOrderItemDto
 * @property {string} id
 * @property {string} order_id
 * @property {string} product_id
 * @property {string} product_variant_id
 * @property {string} promotion_id
 * @property {string} promotion_name
 * @property {integer} promotion_type
 * @property {string} promotion_code
 * @property {integer} bundle_group
 * @property {integer} bundle_type
 * @property {integer} discount_type
 * @property {string} discount_percent
 * @property {string} discount_taxless
 * @property {string} discount_taxed
 * @property {string} discount_reason_id
 * @property {string} discount_reason_name
 * @property {string} discount_reason_note
 * @property {integer} created_at
 * @property {integer} updated_at
 */

/**
 * @typedef {Object} PosOrderDto
 * @property {string} id
 * @property {string} order_id
 * @property {string} pos_order_number
 * @property {integer} pos_order_number_sequence
 * @property {string} origin_pos_order_number
 * @property {number|string} total_general_items_taxless
 * @property {number|string} total_general_items_tax
 * @property {integer} is_general_items_taxfree
 * @property {number|string} total_expendable_items_taxless
 * @property {number|string} total_expendable_items_tax
 * @property {integer} is_expendable_items_taxfree
 * @property {number|string} total_exclude_items_taxless
 * @property {number|string} total_exclude_items_tax
 * @property {number|string} total_point_cut_items_taxless
 * @property {number|string} total_point_cut_items_taxed
 * @property {integer} is_tax_free
 * @property {string} device_id
 * @property {string} cashier_id
 * @property {string} cashier_code
 * @property {string} staff_id
 * @property {string} staff_name
 * @property {string} staff_code
 * @property {string} canceled_device_id
 * @property {string} canceled_cashier_id
 * @property {string} canceled_cashier_code
 * @property {string} canceled_staff_id
 * @property {string} canceled_staff_name
 * @property {string} canceled_staff_code
 * @property {string} canceled_at
 * @property {integer} client_created_at
 */
