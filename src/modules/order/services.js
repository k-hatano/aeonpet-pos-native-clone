import I18n from 'i18n-js'
import Decimal from 'decimal.js'
import AlertView from '../../common/components/widgets/AlertView'
import {
  makeBillEJournal,
  makeProcessName,
  makeReprintEJournal,
  STATUS,
  makeReprintReturnEjournal,
  IS_MINUS,
  makeCancelOrderEJournal
} from './models'
import EJournalRepository from '../printer/repositories/EJournalRepository'
import { makeBaseEJournal, RECEIPT_TYPE } from '../printer/models'
import ReprintOrderReceiptBuilder from './models/ReprintOrderReceiptBuilder'
import { loading } from 'common/sideEffects'
import PrinterManager from 'modules/printer/models/PrinterManager'
import OrderRepository from './repositories/OrderRepository'
import OrderBillBuilder from './models/OrderBillBuilder'
import CartReceiptBuilder from 'modules/cart/models/CartReceiptBuilder'
import OrderedCart from 'modules/order/models/OrderedCart'
import CartDataConverter from 'modules/cart/models/CartDataConverter'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import ReceiptForShopBuilder from './models/ReceiptForShopBuilder'
import CashierRepository from '../cashier/repositories/CashierRepository'
import ConfirmView from '../../common/components/widgets/ConfirmView'
import logger from '../../common/utils/logger'
import MessageError from '../../common/errors/MessageError'
import CartManager from '../cart/models/CartManager'
import CustomerRepository from '../customer/repositories/CustomerRepository'
import PaymentServiceManager from 'modules/payment/models/PaymentServiceManager'
import { ORDER_EXTRA_DATA_KEY } from 'modules/order/models'
import CashChanger from 'modules/printer/models/CashChanger'
import { isPaperNearEnd, isPaperEnd } from 'modules/printer/models/PrinterResult'

export const reprintOrderReceipt = async (dispatch, order, isTrainingMode = false) => {
  try {
    const isSetPrinter = await PrinterManager.isSetPrinter()
    if (!isSetPrinter) {
      AlertView.show(I18n.t('message.F-02-E009'))
      return
    }

    if (order.status === STATUS.RETURNED || order.status === STATUS.CANCELED) {
      AlertView.show(I18n.t('message.F-02-E004', {process: makeProcessName(order)}))
      return
    }

    let message = null
    await loading(dispatch, async () => {
      let ejournals

      try {
        ejournals = await EJournalRepository
          .fetchByOrderNumber(order.pos_order.pos_order_number, RECEIPT_TYPE.ORDER)
      } catch (error) {
        // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
      }
      if (!ejournals || ejournals.length === 0) {
        ejournals = await EJournalRepository
          .findByOrderNumber(order.pos_order.pos_order_number, RECEIPT_TYPE.ORDER)
      }

      if (!ejournals || ejournals.length === 0) {
        AlertView.show(I18n.t('message.F-02-E001'))
        return
      }

      const builder = new ReprintOrderReceiptBuilder()
      await builder.initializeAsync()
      const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournals[0].data), ejournals[0].is_reprint === 0)
      const ejournal = makeReprintEJournal(receipt, ejournals[0], new Date())
      await EJournalRepository.save(ejournal)
      try {
        const printResult = await PrinterManager.print(receipt)
        if (isPaperNearEnd(printResult)) {
          message = I18n.t('printer.is_paper_near_end')
        } else if(isPaperEnd(printResult)) {
          message = I18n.t('printer.is_paper_end')
        }
      } catch (error) {
        message = I18n.t('message.F-02-E003')
      }
      await EJournalRepository.pushById(ejournal.id)
    })
    if (message) {
      AlertView.show(message)
    }
  } catch (error) {
    AlertView.show(I18n.t('message.F-02-E003'))
  }
}

export const reprintReturnOrderReceipt = async (dispatch, order, isTrainingMode = false) => {
  let returnedCart
  let ejournal
  let ejournalForShop
  let ejournals
  let isError = false
  try {
    const isSetPrinter = await PrinterManager.isSetPrinter()
    if (!isSetPrinter) {
      AlertView.show(I18n.t('message.F-02-E009'))
      return
    }

    if (order.status === STATUS.CANCELED) {
      AlertView.show(I18n.t('message.F-02-E004', {process: makeProcessName(order)}))
      return
    }

    await loading(dispatch, async () => {
      let returnOrder
      if (order.status === STATUS.RETURNED) {
        const conditions = {
          'origin_order_id': order.id,
          'is_minus': 1
        }
        const returnOrders = await OrderRepository.fetchByConditions(conditions)
        returnOrder = returnOrders[0]
        if (isTrainingMode) {
          returnOrder = returnOrder.data
        }
      } else {
        returnOrder = order
      }

      try {
        ejournals = await EJournalRepository
          .fetchByOrderNumber(returnOrder.pos_order.pos_order_number, RECEIPT_TYPE.RETURN_ORDER)
      } catch (error) {
        // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
      }
      if (!ejournals || ejournals.length === 0) {
        ejournals = await EJournalRepository
          .findByOrderNumber(returnOrder.pos_order.pos_order_number, RECEIPT_TYPE.RETURN_ORDER)
      }

      if (!ejournals || ejournals.length === 0) {
        // 注文レシートの場合と異なり、サーバ側にもローカルDB側にも電子ジャーナルデータが存在しない場合、電子ジャーナルを生成する
        returnedCart = new OrderedCart(order)
        returnedCart = returnedCart.createReturnWithReturnedOrder(returnOrder)
        if (order.is_minus === IS_MINUS.RETURNED) {
          // 特殊返品の場合は金額に-1をかけた状態になっているので、再度対象のデータに-1をかける為にデータを再構築する
          // レシートの金額にマイナスが付与された状態で表示されてしまう為
          const orderDto = (new CartDataConverter(returnedCart)).convert()
          returnedCart = new OrderedCart(orderDto)
          returnedCart = returnedCart.createReturnWithReturnedOrder(returnOrder)
        }
        returnedCart = returnedCart.setIsTrainingMode(isTrainingMode)
        if (order.customer_code) {
          // TODO: customerRankを保持しておいてここで使えるようにしたい。
        }
        // returnedCart = returnedCart.setReturnResult(customerRank, order)
        const returnBuilder = new CartReceiptBuilder()
        await returnBuilder.initializeAsync()
        const returnReceipt = returnBuilder.buildCartReceipt(returnedCart)

        const builder = new ReprintOrderReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildReprintOrderReceipt(returnReceipt)
        ejournal = makeReprintReturnEjournal(receipt, returnedCart, new Date())
        await EJournalRepository.save(ejournal)
        await PrinterManager.print(receipt)
        await EJournalRepository.pushById(ejournal.id)
      } else {
        const builder = new ReprintOrderReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournals[0].data), ejournals[0].is_reprint === 0)
        ejournal = makeReprintEJournal(receipt, ejournals[0], new Date(), RECEIPT_TYPE.RETURN_ORDER)
        await EJournalRepository.save(ejournal)
        await PrinterManager.print(receipt)
      }
    })
  } catch (error) {
    isError = true
  }

  try {
    await loading(dispatch, async () => {
      // 売り場控え返品レシート
      const builderForShop = new ReceiptForShopBuilder()
      await builderForShop.initializeAsync()
      const receiptForShop = builderForShop.buildReceiptForShop(JSON.parse(ejournal.data))
      const ejournalForShop = !ejournals || ejournals.length === 0
        ? makeReprintReturnEjournal(receiptForShop, returnedCart, new Date(), RECEIPT_TYPE.RETURN_FOR_SHOP)
        : makeReprintEJournal(receiptForShop, ejournal, new Date(), RECEIPT_TYPE.RETURN_FOR_SHOP)
      await EJournalRepository.save(ejournalForShop)
      await PrinterManager.print(receiptForShop)
    })
  } catch (error) {
    isError = true
  }

  try {
    if (ejournal) {
      await EJournalRepository.pushById(ejournal.id)
    }
    if (ejournalForShop) {
      await EJournalRepository.pushById(ejournalForShop.id)
    }
  } catch (error) {
    // 失敗してもユーザーへ通知はしない。
  }

  if (isError) {
    AlertView.show(I18n.t('message.F-02-E003'))
  }
}

async function makeCancelOrderParams (selectedStaff) {
  const cashier = await CashierRepository.find()
  return {
    canceled_cashier_id: cashier.id,
    canceled_cashier_code: cashier.cashier_code,
    canceled_staff_id: selectedStaff.id,
    canceled_staff_code: selectedStaff.staff_code,
    canceled_staff_name: selectedStaff.name
  }
}

/**
 *
 * @param {OrderDto} order
 * @return {Promise<void>}
 */
async function validateCanCancelOrder (order) {
  const isValidStatus = (order.status !== STATUS.STATUS_PAID && order.status !== null && order.status !== undefined)
  // 会計であること かつ 返品済みでないこと かつ 精算済みでないこと
  if (order.is_minus || isValidStatus || order.finalized_at) {
    AlertView.show(I18n.t('message.F-02-E013'))
    return false
  }

  // 特定の伝票が印字済みでないことを確認
  try {
    // 領収証印字済みでないこと
    let checkReceiptTypes = [RECEIPT_TYPE.BILL]

    // 免税会計の場合は免税帳票発行済でないこと
    if (order.pos_order.is_tax_free) {
      checkReceiptTypes = checkReceiptTypes.concat([
        RECEIPT_TYPE.TAXFREE_SALES,
        RECEIPT_TYPE.TAXFREE_CUSTOMER,
        RECEIPT_TYPE.TAXFREE_SHIPPING
      ])
    }

    for (const receiptType of checkReceiptTypes) {
      const ejournals = await EJournalRepository.findByOrderNumber(
        order.pos_order.pos_order_number, receiptType)
      if (ejournals.length > 0) {
        AlertView.show(I18n.t('message.F-02-E013'))
        return false
      }
    }
  } catch (error) {
    const reasonMessage = getCommonNetworkErrorMessage(error, '')
    const message = I18n.t('message.F-01-E002', { error: reasonMessage, process: I18n.t('order.process_canceled') })
    AlertView.show(message)
    return false
  }
  return true
}

async function updateLocalOrderForCancel (newRemoteOrder) {
  const oldLocal = await OrderRepository.findById(newRemoteOrder.id)
  if (!oldLocal) { return }
  oldLocal.pos_order.canceled_at = newRemoteOrder.pos_order.canceled_at
  oldLocal.pos_order.canceled_cashier_id = newRemoteOrder.pos_order.canceled_cashier_id
  oldLocal.pos_order.canceled_cashier_code = newRemoteOrder.pos_order.canceled_cashier_code
  oldLocal.pos_order.canceled_staff_id = newRemoteOrder.pos_order.canceled_staff_id
  oldLocal.pos_order.canceled_staff_name = newRemoteOrder.pos_order.canceled_staff_name
  oldLocal.pos_order.canceled_staff_code = newRemoteOrder.pos_order.canceled_staff_code
  oldLocal.status = newRemoteOrder.status
  await OrderRepository.updateLocal(oldLocal)
}

export async function cancelOrder (order, selectedStaff, isTrainingMode, dispatch) {
  if (await ConfirmView.showAsync(I18n.t('message.F-02-I003'))) {
    const messages = []

    if (!await validateCanCancelOrder(order)) { return }
    // 現金で返金するのはクレジットとイオンペットポイント以外
    const cancelCart = new OrderedCart(order)
    const cancelCashAmount = cancelCart.activePayments
      .filter(payment => !payment.isCreditPaid)
      .reduce((sum, payment) => sum + payment.amount.toNumber(), 0) - cancelCart.change - cancelCart.usePoint
    const needsDispenseChange = (CashChanger.isEnabled && cancelCashAmount > 0 && !isTrainingMode)

    if (needsDispenseChange) {
      const checkResult = await loading(dispatch, async () => {
        try {
          await CashChanger.checkCashierStatus()
        } catch (error) {
          return error.message
        }
      })
      if(checkResult) {
        await AlertView.showAsync(checkResult)

        return
      }
    }
    let result = await loading(dispatch, async () => {
      try {
        await OrderRepository.cancelRemoteOrder(
          order.id,
          await makeCancelOrderParams(selectedStaff))
      } catch (error) {
        await logger.warning('Void request failed for ' + order.pos_order.pos_order_number)
        if (error instanceof MessageError) {
          messages.push(error.message)
        } else {
          const reasonMessage = getCommonNetworkErrorMessage(error, '')
          messages.push(I18n.t('message.F-01-E002', { error: reasonMessage, process: I18n.t('order.process_canceled') }))
        }
      }
    })

    let { receipt, ejournal, canceledOrder, orderedCart, paymentExtraData } =
    await buildCancelOrderReceipt(order, messages, isTrainingMode)
    if (!orderedCart) { return result }

    /** クレジット支払額合計 */
    let creditAmount = new Decimal(0)
    if (paymentExtraData) {
      result = await loading(dispatch, async () => {
        try {
          const payments = orderedCart.activePayments.toArray()
          for (let key of Object.keys(payments)) {
            if (payments[key].isCreditPaid) {
              payments[key].extra_data = paymentExtraData
              creditAmount = creditAmount.add(payments[key].amount)
            }
            const paymentServiceManager = new PaymentServiceManager()
            const result = await paymentServiceManager.cancel(payments[key])
            if (result) {
              orderedCart = orderedCart.updatePayment(payments[key], result)
            }
          }
          return {
            isSuccess: true
          }
        } catch (error) {
          logger.warning('credit cart transaction error ' + error.message)
          messages.push(error.message)
          return {
            isSuccess: false,
            messages: messages,
            isUserCancel: error.isUserCancel
          }
        }
      })

      if (!result.isSuccess) {
        if (result.isUserCancel) {
          messages.push(I18n.t('message.F-02-E019'))
          for (const i in result.messages) {
            await AlertView.showAsync(result.messages[i])
          }
          return result
        } else {
          if (!await ConfirmView.showAsync(I18n.t('message.F-02-E018'))) {
            messages.push(I18n.t('message.F-02-E017', {creditOrderId: order.id}))
            for (const i in result.messages) {
              await AlertView.showAsync(result.messages[i])
            }
            return result
          }
        }
      }
    }

    result = await loading(dispatch, async () => {
      const manager = new CartManager()
      manager._updateQuantity(orderedCart)

      await EJournalRepository.save(ejournal)
      try {
        if (needsDispenseChange) {
          try {
            await CashChanger.dispenseChange(cancelCashAmount)
          } catch (error) {
            isError = true
            messages.push(I18n.t('message.G-02-E012'))
          }
        }
        const printResult = await PrinterManager.print(receipt)
        if (isPaperNearEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_near_end'))
        } else if(isPaperEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_end'))
        }
        await EJournalRepository.saveIsPrintedById(ejournal.id)
      } catch (error) {
        messages.push(I18n.t('message.F-02-E014'))
      }
      await EJournalRepository.pushById(ejournal.id, false)
      await updateLocalOrderForCancel(canceledOrder)
      return canceledOrder
    })
    for (const message of messages) {
      await AlertView.showAsync(message)
    }
    return result
  }
}

/**
 *
 * @param order
 * @param messages
 * @param isTrainingMode
 * @param isReprint
 * @return {Promise<{receipt: {content}, ejournal, canceledOrder: *, paymentExtraData}>}
 */
export async function buildCancelOrderReceipt (order, messages, isTrainingMode, isReprint = false) {
  let canceledOrder
  try {
    canceledOrder = await OrderRepository.fetchByOrderId(order.id)
    canceledOrder.promotion_orders = await OrderRepository.fetchPromotionOrderByOrderId(order.id)
  } catch (error) {
    messages.push(I18n.t('message.F-01-E002', { error: '', process: I18n.t('order.process_canceled') }))
    return {}
  }
  let orderedCart = (new OrderedCart(canceledOrder)).setIsTrainingMode(isTrainingMode)
  if (order.customer_code) {
    const customer = await CustomerRepository.fetchByCustomerCode(order.customer_code)
    if (customer) {
      orderedCart = orderedCart.setCustomer(customer)
    }
  }

  orderedCart = await orderedCart.convertPaymentMethodType()

  const receiptBuilder = new CartReceiptBuilder()
  await receiptBuilder.initializeAsync()
  let receipt = receiptBuilder.buildCartReceipt(orderedCart)
  let ejournal = makeCancelOrderEJournal(receipt, orderedCart, new Date())
  if (isReprint) {
    const reprintBuilder = new ReprintOrderReceiptBuilder()
    await reprintBuilder.initializeAsync()
    receipt = reprintBuilder.buildReprintOrderReceipt(receipt, true)
    ejournal = makeReprintEJournal(receipt, ejournal, new Date(), RECEIPT_TYPE.CANCEL_ORDER)
  }

  const orderExtraData = JSON.parse(order.extra_data)
  return { receipt, ejournal, canceledOrder, orderedCart, paymentExtraData: orderExtraData[ORDER_EXTRA_DATA_KEY.PAYMENT] }
}

/**
 *
 * @param {OrderDto} order
 * @param {boolean} isTrainingMode
 * @param dispatch
 * @return {Promise<void>}
 */
export async function reprintCancelOrderReceipt (order, isTrainingMode, dispatch) {
  const messages = []
  await loading(dispatch, async () => {
    if (!await PrinterManager.isSetPrinter()) {
      AlertView.show(I18n.t('message.F-02-E009'))
      return null
    }

    const originalEjournals = await EJournalRepository.findOrFetchByOrderNumber(
      order.pos_order.pos_order_number, RECEIPT_TYPE.CANCEL_ORDER, true)

    let ejournal, receipt

    if (originalEjournals && originalEjournals.length > 0) {
      const originalEjournal = originalEjournals[0]
      const builder = new ReprintOrderReceiptBuilder()
      await builder.initializeAsync()
      receipt = builder.buildReprintOrderReceipt(
        JSON.parse(originalEjournal.data), originalEjournal.is_reprint === 0)
      ejournal = makeReprintEJournal(receipt, originalEjournal, new Date(), RECEIPT_TYPE.CANCEL_ORDER)
    } else {
      const built = await buildCancelOrderReceipt(order, messages, isTrainingMode, true)
      ejournal = built.ejournal
      receipt = built.receipt
    }
    await EJournalRepository.save(ejournal)
    try {
      const printResult = await PrinterManager.print(receipt)
      if (isPaperNearEnd(printResult)) {
        messages.push(I18n.t('printer.is_paper_near_end'))
      } else if(isPaperEnd(printResult)) {
        messages.push(I18n.t('printer.is_paper_end'))
      }
    } catch (error) {
      messages.push(I18n.t('message.F-02-E003'))
    }
    await EJournalRepository.pushById(ejournal.id)
  })
  for (const message of messages) {
    AlertView.showAsync(message)
  }
}

export const reprintCreditRefundReceipt = async (dispatch, returnOrder) => {
  if (returnOrder.status === STATUS.RETURNED) {
    try {
      // 返品済みの会計レコードから、返品レコードを検索する
      await loading(dispatch, async () => {
        const conditions = {
          'origin_order_id': returnOrder.id,
          'is_minus': 1
        }
        const returnOrders = await OrderRepository.fetchByConditions(conditions)
        returnOrder = returnOrders[0]
      })
    } catch (error) {
      const message = getCommonNetworkErrorMessage(error, I18n.t('message.F-02-E011'))
      await AlertView.showAsync(message)
      return
    }
  }

  return reprintCreditReceipt(dispatch, returnOrder, RECEIPT_TYPE.CREDIT_RETURN)
}

export const reprintCreditReceipt = async (dispatch, order, receiptType = RECEIPT_TYPE.CREDIT) => {
  const isSetPrinter = await PrinterManager.isSetPrinter()
  if (!isSetPrinter) {
    await AlertView.showAsync(I18n.t('message.F-02-E009'))
    return
  }

  let message = null

  await loading(dispatch, async () => {
    let ejournals
    try {
      ejournals = await EJournalRepository
        .fetchByOrderNumber(order.pos_order.pos_order_number, receiptType)
    } catch (error) {
      // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
    }
    if (!ejournals || ejournals.length === 0) {
      ejournals = await EJournalRepository
        .findByOrderNumber(order.pos_order.pos_order_number, receiptType)
    }

    if (!ejournals || ejournals.length === 0) {
      message = I18n.t('message.F-02-E011')
    } else {
      // TODO リファクタリング ReprintOrderReceiptBuilderは会計レシートに限らず使えるので、ReprintReceiptBuilderに変更する
      const builder = new ReprintOrderReceiptBuilder()
      await builder.initializeAsync()
      const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournals[0].data), ejournals[0].is_reprint === 0)
      const ejournal = makeReprintEJournal(receipt, ejournals[0], new Date(), receiptType)
      await EJournalRepository.save(ejournal)
      try {
        const printResult = await PrinterManager.print(receipt)
        if (isPaperNearEnd(printResult)) {
          message = I18n.t('printer.is_paper_near_end')
        } else if(isPaperEnd(printResult)) {
          message = I18n.t('printer.is_paper_end')
        }
      } catch (error) {
        message = I18n.t('message.F-02-E003')
      }
      try {
        await EJournalRepository.pushById(ejournal.id)
      } catch (error) {
        // ユーザーへの通知は行わない
      }
    }
  })
  if (message) {
    await AlertView.showAsync(message)
  }
}

export const printOrderBill = async (dispatch, cart) => {
  let ejournals = null
  let bill = null
  let ejournal = null
  let ejournalForShop = null
  try {
    const isSetPrinter = await PrinterManager.isSetPrinter()
    if (!isSetPrinter) {
      AlertView.show(I18n.t('message.F-02-E009'))
      return
    }
    await loading(dispatch, async () => {
      const order = await OrderRepository.fetchByOrderId(cart.id)
      if (!order) {
        AlertView.show(I18n.t('message.F-01-E001'))
        return
      }
      if (order.status === STATUS.RETURNED || order.status === STATUS.CANCELED) {
        AlertView.show(I18n.t('message.F-02-E004', {process: makeProcessName(order)}))
        return
      }

      try {
        ejournals = await EJournalRepository.fetchByOrderNumber(cart.posOrderNumber, RECEIPT_TYPE.BILL)
        if (ejournals && ejournals.length > 0) {
          AlertView.show(I18n.t('message.F-02-E008'))
          return
        }
      } catch (error) {
        // この場合の例外は「見つからなかっただけ」の扱いなので、特別な処理は行わない
      }
      ejournals = await EJournalRepository.findByOrderNumber(cart.posOrderNumber, RECEIPT_TYPE.BILL)
      if (ejournals && ejournals.length > 0) {
        AlertView.show(I18n.t('message.F-02-E008'))
        return
      }

      const builder = new OrderBillBuilder()
      await builder.initializeAsync()
      bill = builder.buildOrderBill(cart, new Date())
      await PrinterManager.print(bill)

      try {
        ejournal = makeBillEJournal(bill, cart, new Date())
        await EJournalRepository.save(ejournal)
        await EJournalRepository.pushById(ejournal.id)
      } catch (error) {
        console.log({error})
        // ユーザーへのエラー通知は行わない。
      }

      if (ejournal) {
        const builderForShop = new ReceiptForShopBuilder()
        await builderForShop.initializeAsync()
        const billForShop = builderForShop.buildReceiptForShop(JSON.parse(ejournal.data))
        await PrinterManager.print(billForShop)
        try {
          ejournalForShop = makeBillEJournal(billForShop, cart, new Date(), RECEIPT_TYPE.BILL_FOR_SHOP)
          await EJournalRepository.save(ejournalForShop)
          await EJournalRepository.pushById(ejournalForShop.id)
        } catch (error) {
          // ユーザーへのエラー通知は行わない。
        }
      }
    })
  } catch (error) {
    AlertView.show(I18n.t('message.F-02-E003'))
  }
}

export const makeCreditEjournal = (receipt, amount, cart) => {
  const ejournal = makeBaseEJournal(
    I18n.t('receipt.title.credit'),
    receipt,
    RECEIPT_TYPE.CREDIT,
    cart.createdAt)
  ejournal.currency = cart.currency
  ejournal.amount = Decimal(amount).toFixed(4)
  ejournal.order_id = cart.id
  ejournal.pos_order_number = cart.posOrderNumber
  ejournal.staff_id = cart.staff.id
  return ejournal
}

export const makeCreditReturnEjournal = (receipt, amount, cart, createdAt) => {
  const ejournal = makeBaseEJournal(
    I18n.t('receipt.title.credit'),
    receipt,
    RECEIPT_TYPE.CREDIT_RETURN,
    createdAt)
  ejournal.is_printed = 0
  ejournal.currency = cart.currency
  ejournal.amount = Decimal(amount).toFixed(4)
  ejournal.order_id = cart.id
  ejournal.pos_order_number = cart.posOrderNumber
  ejournal.staff_id = cart.staff.id
  return ejournal
}
