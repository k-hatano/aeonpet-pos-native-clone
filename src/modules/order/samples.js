/**
 *
 * @return {Cart}
 */
import generateUuid from '../../common/utils/generateUuid'
import { IS_MINUS, STATUS } from './models'
import * as _ from 'underscore'
import Cart from '../cart/models/Cart'
import Tax, { TAX_RULE } from 'common/models/Tax'
import { sampleCashier } from '../cashier/samples'
import { sampleShops } from '../shop/samples'
import { samplePaymentMethods } from '../payment/samples'
import { sampleProductMap } from '../product/samples'
import { PAYMENT_METHOD_TYPE } from '../payment/models'
import { sampleStaffMap } from '../staff/samples'
import CartDataConverter from '../cart/models/CartDataConverter'
import CartPayment from '../cart/models/CartPayment'

let orderNumberSequence = 1000

export const createSampleCart = (resetOrderNumberSequence = false) => {
  if (resetOrderNumberSequence) {
    orderNumberSequence = 1000
  }
  let cart = new Cart({
    tax: new Tax({ tax_rule: TAX_RULE.EXCLUDED_FLOOR, tax_rate: 0.08 }),
    id: generateUuid(),
    cashier: sampleCashier,
    shop: sampleShops[0]
  })
  for (const i in samplePaymentMethods) {
    cart = cart.addPayment(CartPayment.createFromPaymentMethodEntity(samplePaymentMethods[i]))
  }
  return cart
}

export const completeSampleCart = (cart, createdAt) => {
  return cart.complete(sampleStaffMap.staff1, orderNumberSequence++, [], createdAt)
}

/**
 *
 * @param {Cart} cart
 * @param product
 * @param quantity
 * @return {Promise.<Cart>}
 */
export const addProductToSampleCart = async (cart, product, quantity) => {
  return cart.addProductAsync(product, product.product_variants[0], quantity)
}

/**
 *
 * @param {Cart} cart
 * @param paymentMethodType
 * @param amount
 * @return {Cart}
 */
export const payByPaymentMethodType = (cart, paymentMethodType, amount) => {
  const payment = cart.payments.find(payment => payment.paymentMethodType === paymentMethodType)
  if (amount === undefined) {
    return cart.updatePayment(payment, payment.updateAmount(cart.justAmountForPayment(payment)))
  } else {
    return cart.updatePayment(payment, payment.updateAmount(amount))
  }
}

export const createSampleOrders = async () => {
  return [
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.standardA, 1)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
      cart = completeSampleCart(cart, new Date())
      return (new CartDataConverter(cart)).convert()
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_s_1_1, 3)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
      cart = completeSampleCart(cart, new Date())
      return (new CartDataConverter(cart)).convert()
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.setA, 2)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 10000)
      cart = completeSampleCart(cart, new Date())
      return (new CartDataConverter(cart)).convert()
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = cart.setIsTaxfree(true)
      cart = await addProductToSampleCart(cart, sampleProductMap.standardB, 5)
      cart = await addProductToSampleCart(cart, sampleProductMap.taxfreeExpentable1, 2)
      cart = await addProductToSampleCart(cart, sampleProductMap.taxfreeNotApplicable2, 2)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 10000)
      cart = completeSampleCart(cart, new Date(new Date().getTime() - 10000))
      return (new CartDataConverter(cart)).convert()
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_xy_1_1, 3)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 10000)
      cart = completeSampleCart(cart, new Date())
      return (new CartDataConverter(cart)).convert()
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_s_1_1, 1)
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_s_1_2, 2)
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_flat_1_1, 2)
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_flat_1_2, 1)
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_xy_1_1, 2)
      cart = await addProductToSampleCart(cart, sampleProductMap.bundle_xy_1_2, 1)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 10000)
      cart = completeSampleCart(cart, new Date())
      return (new CartDataConverter(cart)).convert()
    })()
  ]
}

export const createSampleServerOrders = async () => {
  return [
    await (async () => {
      let cart = createSampleCart()
      cart = await addProductToSampleCart(cart, sampleProductMap.standardA, 1)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
      cart = completeSampleCart(cart, new Date())
      return convertToServerOrder(
        (new CartDataConverter(cart)).convert())
    })(),
    await (async () => {
      let cart = createSampleCart()
      cart = cart.setIsTaxfree(true)
      cart = await addProductToSampleCart(cart, sampleProductMap.standardB, 5)
      cart = await addProductToSampleCart(cart, sampleProductMap.taxfreeExpentable1, 2)
      cart = await addProductToSampleCart(cart, sampleProductMap.taxfreeNotApplicable2, 2)
      cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 10000)
      cart = completeSampleCart(cart, new Date(new Date().getTime() - 10000))
      return convertToServerOrder(
        (new CartDataConverter(cart)).convert())
    })()
  ]
}

export function convertToServerOrder (order) {
  return {
    ...order,
    status: STATUS.COMPLETED
  }
}

export const sampleServerOrders = [
  {
    id: 1,
    order_type: 1,
    status: 2,
    created_at: 1504593788,
    order_payments: [
      {
        payment_method_type: 1
      },
      {
        payment_method_type: 2
      },
      {
        payment_method_type: 3
      }
    ],
    currency: 'jpy',
    total_paid: 10000,
    pos_order: {
      client_created_at: 1504593788
    }
  },
  {
    id: 2,
    order_type: 2,
    status: 3,
    created_at: 1504593788,
    order_payments: [
      {
        payment_method_type: 4
      },
      {
        payment_method_type: 5
      },
      {
        payment_method_type: 6
      }
    ],
    currency: 'jpy',
    total_paid: 11000,
    pos_order: {
      client_created_at: 1504593788
    }
  },
  {
    id: 3,
    order_type: 3,
    status: 4,
    created_at: 1504593788,
    order_payments: [
      {
        payment_method_type: 99
      }
    ],
    currency: 'jpy',
    total_paid: 12000,
    pos_order: {
      client_created_at: 1504593788
    }
  }
]

const orderIds = {
  id1: '0283890260D50599767D97511B8E90A5',
  id2: '08C6BF4A27D09AE2407789830FD0F8CF',
  id3: '28CA72D187E50E566D74965DC1566381',
  id4: '343E15A347742A96A561C0983A4EA9D5',
  id5: '45318F45F7C340D56A38B2A616BD78F1'
}

const orderNumbers = {
  number1: 'ORDER000000000000000000000000001',
  number2: 'ORDER000000000000000000000000002',
  number3: 'ORDER000000000000000000000000003',
  number4: 'ORDER000000000000000000000000004',
  number5: 'ORDER000000000000000000000000005'
}

const posOrderNumbers = {
  number1: 'POS00000000000000000000000000001',
  number2: 'POS00000000000000000000000000002',
  number3: 'POS00000000000000000000000000003',
  number4: 'POS00000000000000000000000000004',
  number5: 'POS00000000000000000000000000005'
}

const promotionIds = {
  id1: generateUuid(),
  id2: generateUuid(),
  id3: generateUuid(),
  id4: generateUuid(),
  id5: generateUuid()
}

const promotionOrderItemIds = {
  id1: {
    productId1: generateUuid(),
    variantId1: generateUuid()
  },
  id2: {
    productId2: generateUuid(),
    variantId2: generateUuid()
  },
  id3: {
    productId3: generateUuid(),
    variantId3: generateUuid()
  },
  id4: {
    productId4: generateUuid(),
    variantId4: generateUuid()
  },
  id5: {
    productId5: generateUuid(),
    variantId5: generateUuid()
  }
}

const sampleOrderMap = {
  order1: {
    id: orderIds.id1,
    shop_id: generateUuid(),
    shop_name: '店舗１',
    user_id: generateUuid(),
    customer_code: 'code1',
    order_number: orderNumbers.number1,
    is_minus: 0,
    origin_order_id: null,
    status: 2, // STATUS.STATUS_PAID 参照ループ対策で直接数値指定
    email: 'order1@example.com',
    currency: 'jpy',
    total_items_taxless: 1000,
    total_items_taxed: 1080,
    total_taxable_items_taxless: 1000,
    total_taxable_items_taxed: 1080,
    total_items_discount: 0,
    total_paid_taxless: 1000,
    total_paid_taxed: 1080,
    used_points: 0,
    obtained_points: 0,
    extra_data: '',
    contracted_at: Math.floor(new Date().getTime() / 1000),
    finalized_at: Math.floor(new Date().getTime() / 1000),
    deleted_at: Math.floor(new Date().getTime() / 1000),
    completed_at: Math.floor(new Date().getTime() / 1000),
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000),
    order_items: [
      {
        id: generateUuid(),
        order_id: orderIds.id1,
        product_id: promotionOrderItemIds.id1.productId1,
        product_variant_id: promotionOrderItemIds.id1.variantId1,
        bundle_no: 0,
        product_name: '商品名１',
        product_variant_name: '資格名１',
        product_code: 'PCODE00000001',
        sku: 'SKU1',
        article_number: null,
        price_taxless: 1000,
        price_taxed: null,
        sales_price_taxless: 1000,
        sales_price_taxed: null,
        tax_rule: 0,
        tax_rate: 8,
        tax_type: null,
        taxfree_type: null,
        quantity: 1,
        sort_order: 1,
        extra_data: null,
        child_products_data: null,
        can_grant_points: null,
        is_sales_excluded: null
      }
    ],
    order_payments: [
      {
        id: generateUuid(),
        order_id: orderIds.id1,
        payment_method_id: generateUuid(),
        payment_method_name: '現金',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        currency: 'jpy',
        amount: 1080
      }
    ],
    pos_order: {
      order_id: orderIds.id1,
      pos_order_number: posOrderNumbers.number1,
      pos_order_number_sequence: 1,
      origin_pos_order_number: null,
      total_general_items_taxless: 1000,
      total_general_items_tax: 80,
      is_general_items_taxfree: 0,
      total_expendable_items_taxless: null,
      total_expendable_items_tax: null,
      is_expendable_items_taxfree: null,
      total_exclude_items_taxless: null,
      total_exclude_items_tax: null,
      total_point_cut_items_taxless: null,
      total_point_cut_items_taxed: null,
      is_tax_free: 0,
      device_id: generateUuid(),
      cashier_id: generateUuid(),
      cashier_code: 'CCODE00000001',
      staff_id: generateUuid(),
      staff_name: 'スタッフ１',
      staff_code: 'SCODE00000001',
      canceled_device_id: null,
      canceled_cashier_id: null,
      canceled_staff_id: null,
      canceled_staff_name: null,
      client_created_at: Math.floor(new Date().getTime() / 1000)
    }
  },
  order2: {
    id: orderIds.id2,
    shop_id: generateUuid(),
    shop_name: '店舗１',
    user_id: generateUuid(),
    customer_code: 'code1',
    order_number: orderNumbers.number2,
    is_minus: 1,
    origin_order_id: null,
    status: 6, // STATUS.RETURNED 参照ループ対策で直接数値指定
    email: 'order1@example.com',
    currency: 'jpy',
    total_items_taxless: 1000,
    total_items_taxed: 1080,
    total_taxable_items_taxless: 0,
    total_taxable_items_taxed: 0,
    total_items_discount: 0,
    total_paid_taxless: 1000,
    total_paid_taxed: 1080,
    used_points: 0,
    obtained_points: 0,
    extra_data: '',
    contracted_at: Math.floor(new Date().getTime() / 1000),
    finalized_at: Math.floor(new Date().getTime() / 1000),
    deleted_at: Math.floor(new Date().getTime() / 1000),
    completed_at: Math.floor(new Date().getTime() / 1000),
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000),
    order_items: [
      {
        id: generateUuid(),
        order_id: orderIds.id2,
        product_id: promotionOrderItemIds.id2.productId2,
        product_variant_id: promotionOrderItemIds.id2.variantId2,
        bundle_no: 0,
        product_name: '商品名１',
        product_variant_name: '資格名１',
        product_code: 'PCODE00000001',
        sku: 'SKU1',
        article_number: null,
        price_taxless: 1000,
        price_taxed: 1080,
        sales_price_taxless: 1000,
        sales_price_taxed: 1080,
        tax_rule: 0,
        tax_rate: 8,
        tax_type: null,
        taxfree_type: null,
        quantity: 1,
        sort_order: 1,
        extra_data: null,
        child_products_data: null,
        can_grant_points: null,
        is_sales_excluded: null
      }
    ],
    order_payments: [
      {
        id: generateUuid(),
        order_id: orderIds.id2,
        payment_method_id: generateUuid(),
        payment_method_name: '現金',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        currency: 'jpy',
        amount: 1080
      }
    ],
    pos_order: {
      order_id: orderIds.id2,
      pos_order_number: posOrderNumbers.number2,
      pos_order_number_sequence: 1,
      origin_pos_order_number: null,
      total_general_items_taxless: 1000,
      total_general_items_tax: 80,
      is_general_items_taxfree: 0,
      total_expendable_items_taxless: null,
      total_expendable_items_tax: null,
      is_expendable_items_taxfree: null,
      total_exclude_items_taxless: null,
      total_exclude_items_tax: null,
      total_point_cut_items_taxless: null,
      total_point_cut_items_taxed: null,
      is_tax_free: 0,
      device_id: generateUuid(),
      cashier_id: generateUuid(),
      cashier_code: 'CCODE00000001',
      staff_id: generateUuid(),
      staff_name: 'スタッフ１',
      staff_code: 'SCODE00000001',
      canceled_device_id: null,
      canceled_cashier_id: null,
      canceled_staff_id: null,
      canceled_staff_name: null,
      client_created_at: Math.floor(new Date().getTime() / 1000)
    }
  },
  order3: {
    id: orderIds.id3,
    shop_id: generateUuid(),
    shop_name: '店舗１',
    user_id: generateUuid(),
    customer_code: 'code1',
    order_number: orderNumbers.number3,
    is_minus: 0,
    origin_order_id: null,
    status: 5, // STATUS.CANCELED 参照ループ対策で直接数値指定
    email: 'order1@example.com',
    currency: 'jpy',
    total_items_taxless: 1000,
    total_items_taxed: 1080,
    total_taxable_items_taxless: 1000,
    total_taxable_items_taxed: 1080,
    total_items_discount: 0,
    total_paid_taxless: 1000,
    total_paid_taxed: 1080,
    used_points: 0,
    obtained_points: 0,
    extra_data: '',
    contracted_at: Math.floor(new Date().getTime() / 1000),
    finalized_at: Math.floor(new Date().getTime() / 1000),
    deleted_at: Math.floor(new Date().getTime() / 1000),
    completed_at: Math.floor(new Date().getTime() / 1000),
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000),
    order_items: [
      {
        id: generateUuid(),
        order_id: orderIds.id3,
        product_id: promotionOrderItemIds.id3.productId3,
        product_variant_id: promotionOrderItemIds.id3.variantId3,
        bundle_no: 0,
        product_name: '商品名１',
        product_variant_name: '資格名１',
        product_code: 'PCODE00000001',
        sku: 'SKU1',
        article_number: null,
        price_taxless: 1000,
        price_taxed: 1080,
        sales_price_taxless: 1000,
        sales_price_taxed: 1080,
        tax_rule: 0,
        tax_rate: 8,
        taxfree_type: null,
        quantity: 1,
        sort_order: 1,
        extra_data: null,
        child_products_data: null,
        can_grant_points: null,
        is_sales_excluded: null
      }
    ],
    order_payments: [
      {
        id: generateUuid(),
        order_id: orderIds.id3,
        payment_method_id: generateUuid(),
        payment_method_name: '現金',
        payment_method_type: PAYMENT_METHOD_TYPE.CASH,
        currency: 'jpy',
        amount: 1080
      }
    ],
    pos_order: {
      order_id: orderIds.id3,
      pos_order_number: posOrderNumbers.number3,
      pos_order_number_sequence: 1,
      origin_pos_order_number: null,
      total_general_items_taxless: 1000,
      total_general_items_tax: 80,
      is_general_items_taxfree: 0,
      total_expendable_items_taxless: null,
      total_expendable_items_tax: null,
      is_expendable_items_taxfree: null,
      total_exclude_items_taxless: null,
      total_exclude_items_tax: null,
      total_point_cut_items_taxless: null,
      total_point_cut_items_taxed: null,
      is_tax_free: 0,
      device_id: generateUuid(),
      cashier_id: generateUuid(),
      cashier_code: 'CCODE00000001',
      staff_id: generateUuid(),
      staff_name: 'スタッフ１',
      staff_code: 'SCODE00000001',
      canceled_device_id: null,
      canceled_cashier_id: null,
      canceled_staff_id: null,
      canceled_staff_name: null,
      client_created_at: Math.floor(new Date().getTime() / 1000)
    }
  }
}

const samplePromotionOrderMap = {
  promotion1: {
    id: promotionIds.id1,
    order_id: orderIds.id1,
    tax_rate: 8,
    promotion_id: generateUuid(),
    promotion_name: 'テストプロモーション',
    is_minus: 0,
    discount_type: 1,
    discount_percent: 10,
    subtotal_discounts_taxless: 100,
    subtotal_discounts_taxed: 50,
    discount_reason_id: generateUuid(),
    extra_data: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    created_by: generateUuid(),
    updated_at: Math.floor(new Date().getTime() / 1000),
    updated_by: generateUuid(),
    approved_by: generateUuid(),
    approval_status: 1
  },
  promotion2: {
    id: promotionIds.id2,
    order_id: orderIds.id2,
    tax_rate: 8,
    promotion_id: generateUuid(),
    promotion_name: 'テストプロモーション',
    is_minus: 0,
    discount_type: 1,
    discount_percent: 10,
    subtotal_discounts_taxless: 100,
    subtotal_discounts_taxed: 150,
    discount_reason_id: generateUuid(),
    extra_data: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    created_by: generateUuid(),
    updated_at: Math.floor(new Date().getTime() / 1000),
    updated_by: generateUuid(),
    approved_by: generateUuid(),
    approval_status: 1
  },
  promotion3: {
    id: promotionIds.id3,
    order_id: orderIds.id3,
    tax_rate: 8,
    promotion_id: generateUuid(),
    promotion_name: 'テストプロモーション',
    is_minus: 0,
    discount_type: 1,
    discount_percent: 10,
    subtotal_discounts_taxless: 100,
    subtotal_discounts_taxed: 150,
    discount_reason_id: generateUuid(),
    extra_data: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    created_by: generateUuid(),
    updated_at: Math.floor(new Date().getTime() / 1000),
    updated_by: generateUuid(),
    approved_by: generateUuid(),
    approval_status: 1
  }
}

const samplePromotionOrderItemMap = {
  item1: {
    id: generateUuid(),
    order_id: orderIds.id1,
    product_id: promotionOrderItemIds.id1.productId1,
    product_variant_id: promotionOrderItemIds.id1.variantId1,
    promotion_id: promotionIds.id1,
    promotion_name: 'テストプロモーション',
    promotion_type: null,
    promotion_code: null,
    bundle_group: null,
    bundle_type: null,
    discount_type: 1,
    discount_percent: 10,
    discount_taxless: 50,
    discount_taxed: 100,
    discount_reason_id: null,
    discount_reason_name: null,
    discount_reason_note: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000)
  },
  item2: {
    id: generateUuid(),
    order_id: orderIds.id2,
    product_id: promotionOrderItemIds.id2.productId2,
    product_variant_id: promotionOrderItemIds.id2.variantId2,
    promotion_id: promotionIds.id2,
    promotion_name: 'テストプロモーション',
    promotion_type: null,
    promotion_code: null,
    bundle_group: null,
    bundle_type: null,
    discount_type: 2,
    discount_percent: null,
    discount_taxless: 50,
    discount_taxed: 100,
    discount_reason_id: null,
    discount_reason_name: null,
    discount_reason_note: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000)
  },
  item3: {
    id: generateUuid(),
    order_id: orderIds.id3,
    product_id: promotionOrderItemIds.id3.productId3,
    product_variant_id: promotionOrderItemIds.id1.variantId3,
    promotion_id: promotionIds.id3,
    promotion_name: 'テストプロモーション',
    promotion_type: null,
    promotion_code: null,
    bundle_group: null,
    bundle_type: null,
    discount_type: 1,
    discount_percent: 10,
    discount_taxless: 50,
    discount_taxed: 100,
    discount_reason_id: null,
    discount_reason_name: null,
    discount_reason_note: null,
    created_at: Math.floor(new Date().getTime() / 1000),
    updated_at: Math.floor(new Date().getTime() / 1000)
  }
}

export const sampleOrders = _.values(sampleOrderMap)
export const samplePromotionOrders = _.values(samplePromotionOrderMap)
export const samplePromotionOrderItems = _.values(samplePromotionOrderItemMap)
