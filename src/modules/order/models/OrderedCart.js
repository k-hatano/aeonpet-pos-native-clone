import { Record, List } from 'immutable'
import * as _ from 'underscore'
import Cart from '../../cart/models/Cart'
import OrderedCartItem from './OrderedCartItem'
import OrderedCartBundle from './OrderedCartBundle'
import Tax, { TAXFREE_TYPE } from '../../../common/models/Tax'
import moment from 'moment'
import Decimal from 'decimal.js'
// import logger from 'common/utils/logger'
import CartPayment from '../../cart/models/CartPayment'
import CartDiscount from '../../cart/models/CartDiscount'
import { DISCOUNT_TYPE, OWNER_DISCOUNT } from '../../promotion/models'
import { isGiftChangeOrSurplusPaymentMethodType, PAYMENT_METHOD_TYPE } from '../../payment/models'
import { ORDER_EXTRA_DATA_KEY } from 'modules/order/models'
import PaymentMethodRepository from 'modules/payment/repositories/PaymentMethodRepository'

const OrderField = Record({
  posOrderNumber: null,
  totalTaxless: Decimal(0),
  totalTaxed: Decimal(0),
  standardTotalTaxableItemsTaxless: Decimal(0),
  standardTotalTaxableItemsTaxed: Decimal(0),
  reducedTotalTaxableItemsTaxless: Decimal(0),
  reducedTotalTaxableItemsTaxed: Decimal(0),
  originPosOrderNumber: '',
  taxfreeExpendablesTotalTax: Decimal(0),
  taxfreeGeneralsTotalTax: Decimal(0),
  taxfreeNotApplicablesTotalTax: Decimal(0),
  canceledCashierId: null,
  canceledCashierCode: null,
  canceledStaffId: null,
  canceledStaffName: null,
  canceledStaffCode: null,
  canceledAt: null,
  obtainedPoint: null
})

export default class OrderedCart extends Cart {
  /**
   *
   * @param {OrderDto} order
   */
  constructor (order) {
    order = JSON.parse(JSON.stringify(order)) // Deep Copy
    if (order.promotion_order_items) {
      OrderedCart._dispatchPromotionToItems(order.order_items, order.promotion_order_items)
    }
    const orderExtraData = JSON.parse(order.extra_data)
    const orderItems = orderExtraData[ORDER_EXTRA_DATA_KEY.CART_ITEMS]
    let items = List(orderItems.map(orderItem => (
      new OrderedCartItem(orderItem, order.currency))))
      .sortBy(orderItem => orderItem.instanceUniqueKey)

    items = items.map(item => {
      switch (item.product.taxfreeType) {
        case TAXFREE_TYPE.EXPENDABLE:
          return item.setIsTaxfreeApplied(!!order.pos_order.is_expendable_items_taxfree)
        case TAXFREE_TYPE.GENERAL:
          return item.setIsTaxfreeApplied(!!order.pos_order.is_general_items_taxfree)
        case TAXFREE_TYPE.NOT_APPLICABLE:
        default:
          return item
      }
    })

    const groupedItems = OrderedCart._itemsGroupedByBundle(items)
    const bundles = []
    const bundledItems = []
    _.values(groupedItems).forEach(itemsByNumber => {
      _.values(itemsByNumber).forEach(items => {
        const bundle = new OrderedCartBundle(List(items))
        bundle.items.forEach(bundledItem => bundledItems.push(bundledItem))
        bundles.push(bundle)
      })
    })

    const paymentEntities = order.payments || order.order_payments
    const payments = List(paymentEntities
      .filter(payment => !isGiftChangeOrSurplusPaymentMethodType(payment.payment_method_type))
      .map(payment => CartPayment.createFromPaymentEntity(payment))
    )

    let discount = null
    let ownerDiscount = null
    let discountOrder = 0
    if (order.promotion_orders && order.promotion_orders.length > 0) {
      const orderExtraData = JSON.parse(order.promotion_orders[0].extra_data)
      discountOrder = orderExtraData.discount_order
      for (let i = 0; i < order.promotion_orders.length; i++) {
        const promotionOrder = order.promotion_orders[i]
        // オーナー割引
        if (promotionOrder.discount_reason_name === OWNER_DISCOUNT.NAME) {
          ownerDiscount = new CartDiscount({
            discountType: DISCOUNT_TYPE.RATE,
            value: new Decimal(OWNER_DISCOUNT.RATE)
          })
        // 小計割引or値引
        } else {
          let value = promotionOrder.discount_type === DISCOUNT_TYPE.AMOUNT
            ? Decimal(promotionOrder.subtotal_discounts_taxless || promotionOrder.subtotal_discounts_taxed)
            : Decimal(promotionOrder.discount_percent).div(100)
          discount = new CartDiscount({
            discountType: promotionOrder.discount_type,
            value,
            discountReasonId: promotionOrder.discount_reason_id
          })
        }
      }
    }

    super({
      items: List([...bundledItems, ...items.filter(item => !item.isBundled)]),
      currency: order.currency,
      appliedBundles: List(bundles).sortBy(bundle => bundle.items.get(0).sortOrder),
      orderStatus: order.status,
      cashier: {
        id: order.pos_order.cashier_id,
        code: order.pos_order.cashier_code
      },
      createdAt: new Date(order.pos_order.client_created_at * 1000),
      id: order.id,
      obtainedPoint: order.obtained_points,
      usePoint: order.used_points,
      shop: {
        id: order.shop_id,
        mode: order.shop_mode,
        name: order.shop_name
      },
      staff: {
        id: order.pos_order.staff_id,
        code: order.pos_order.staff_code,
        name: order.pos_order.staff_name
      },
      isTaxfree: order.is_taxfree,
      isBundleDisabled: !!order.is_bundle_disabled,
      tax: new Tax(order),
      payments,
      extendsData: new OrderField({
        posOrderNumber: order.pos_order.pos_order_number,
        totalTaxless: Decimal(order.total_paid_taxless || 0),
        totalTaxed: Decimal(order.total_paid_taxed || 0),
        standardTotalTaxableItemsTaxless: Decimal(order.standard_total_taxable_items_taxless || 0),
        standardTotalTaxableItemsTaxed: Decimal(order.standard_total_taxable_items_taxed || 0),
        reducedTotalTaxableItemsTaxless: Decimal(order.reduced_total_taxable_items_taxless || 0),
        reducedTotalTaxableItemsTaxed: Decimal(order.reduced_total_taxable_items_taxed || 0),
        originPosOrderNumber: order.pos_order.origin_pos_order_number,
        taxfreeExpendablesTotalTax: order.pos_order.total_expendable_items_tax,
        taxfreeGeneralsTotalTax: order.pos_order.total_general_items_tax,
        taxfreeNotApplicablesTotalTax: order.pos_order.total_exclude_items_tax,
        canceledAt: order.pos_order.canceled_at && new Date(order.pos_order.canceled_at * 1000),
        canceledCashierId: order.pos_order.canceled_cashier_id,
        canceledCashierCode: order.pos_order.canceled_cashier_code,
        canceledStaffId: order.pos_order.canceled_staff_id,
        canceledStaffName: order.pos_order.canceled_staff_name,
        canceledStaffCode: order.pos_order.canceled_staff_code,
        obtainedPoint: order.obtained_points
      }),
      discount,
      ownerDiscount: ownerDiscount,
      discountOrder: discountOrder
    })
  }

  createReturn (orderNumberSequence, staff, cashier, shop) {
    return this
      .set('isReturn', true)
      .set('orderNumberSequence', orderNumberSequence)
      .set('staff', staff)
      .set('cashier', cashier)
      .set('shop', shop)
      .set('extendsData', new OrderField({
        posOrderNumber: null,
        totalTaxless: this.totalTaxless,
        totalTaxed: this.totalTaxed,
        standardTotalTaxableItemsTaxless: this.standardTaxableTotalTaxless,
        standardTotalTaxableItemsTaxed: this.standardTaxableTotalTaxed,
        reducedTotalTaxableItemsTaxless: this.reducedTaxableTotalTaxless,
        reducedTotalTaxableItemsTaxed: this.reducedTaxableTotalTaxed,
        originPosOrderNumber: this.originPosOrderNumber,
        taxfreeExpendablesTotalTax: this.taxfreeExpendablesTotalTax,
        taxfreeGeneralsTotalTax: this.taxfreeGeneralsTotalTax,
        taxfreeNotApplicablesTotalTax: this.taxfreeNotApplicablesTotalTax,
        obtainedPoint: this.obtainedPoint
      }))
  }

  createReturnWithReturnedOrder (returnOrder) {
    const staff = {
      id: returnOrder.pos_order.staff_id,
      name: returnOrder.pos_order.staff_name,
      staff_code: returnOrder.pos_order.staff_code
    }
    const cashier = {
      id: returnOrder.pos_order.cashier_id,
      cashier_code: returnOrder.pos_order.cashier_code
    }
    const shop = {
      id: returnOrder.shop_id,
      name: returnOrder.shop_name
    }
    return this
      .set('isReturn', true)
      .set('orderNumberSequence', returnOrder.pos_order.pos_order_number)
      .set('staff', staff)
      .set('cashier', cashier)
      .set('shop', shop)
      .set('extendsData', new OrderField({
        posOrderNumber: returnOrder.pos_order.pos_order_number,
        totalTaxless: this.totalTaxless,
        totalTaxed: this.totalTaxed,
        standardTotalTaxableItemsTaxless: this.standardTaxableTotalTaxless,
        standardTotalTaxableItemsTaxed: this.standardTaxableTotalTaxed,
        reducedTotalTaxableItemsTaxless: this.reducedTaxableTotalTaxless,
        reducedTotalTaxableItemsTaxed: this.reducedTaxableTotalTaxed,
        originPosOrderNumber: returnOrder.pos_order.origin_pos_order_number,
        taxfreeExpendablesTotalTax: this.taxfreeExpendablesTotalTax,
        taxfreeGeneralsTotalTax: this.taxfreeGeneralsTotalTax,
        taxfreeNotApplicablesTotalTax: this.taxfreeNotApplicablesTotalTax,
        obtainedPoint: this.obtainedPoint
      }))
  }

  setReturnResult (returnOrder) {
    return this
      .set('extendsData', new OrderField({
        posOrderNumber: returnOrder.pos_order.pos_order_number,
        totalTaxless: this.totalTaxless,
        totalTaxed: this.totalTaxed,
        standardTotalTaxableItemsTaxless: this.standardTaxableTotalTaxless,
        standardTotalTaxableItemsTaxed: this.standardTaxableTotalTaxed,
        reducedTotalTaxableItemsTaxless: this.reducedTaxableTotalTaxless,
        reducedTotalTaxableItemsTaxed: this.reducedTaxableTotalTaxed,
        originPosOrderNumber: returnOrder.pos_order.origin_pos_order_number,
        taxfreeExpendablesTotalTax: returnOrder.pos_order.total_expendable_items_tax * (-1),
        taxfreeGeneralsTotalTax: returnOrder.pos_order.total_general_items_tax * (-1),
        taxfreeNotApplicablesTotalTax: returnOrder.pos_order.total_exclude_items_tax * (-1),
        obtainedPoint: this.obtainedPoint
      }))
      .set('createdAt', moment.unix(returnOrder.pos_order.client_created_at)._d)
      .set('id', returnOrder.id)
  }

  async convertPaymentMethodType () {
    let newOrderedCart = this.clearPayments()
    let totalReturnAmount = this.totalTaxed

    const creditCartPayment = this.activePayments.find(payment => payment.isCreditPaid)
    if (creditCartPayment) {
      totalReturnAmount = totalReturnAmount.sub(creditCartPayment.amount)
      newOrderedCart = newOrderedCart.addPayment(creditCartPayment)
    }

    const internalPointCartPayment = this.activePayments.find(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.INTERNAL_POINT)
    if (internalPointCartPayment) {
      totalReturnAmount = totalReturnAmount.sub(internalPointCartPayment.amount)
      newOrderedCart = newOrderedCart.addPayment(internalPointCartPayment)
    }

    const cashCartPayment = this.activePayments.find(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH)
    if (cashCartPayment) {
      return newOrderedCart.addPayment(cashCartPayment.updateAmount(totalReturnAmount))
    }

    const cashPayment = await PaymentMethodRepository.findByPaymentMethodType(PAYMENT_METHOD_TYPE.CASH)
    const newCartPayment = CartPayment.createFromPaymentMethodEntity(cashPayment[0], 0)
    return newOrderedCart.addPayment(newCartPayment.updateAmount(totalReturnAmount))
  }

  async toCart (additionalProps) {
    const items = this.items
    const props = this.toObject()
    delete props.extendsData
    delete props.items
    let cart = new Cart({ ...props, ...additionalProps })
    for (let i = 0; i < items.size; i++) {
      const item = await items.get(i).toCartItemAsync()
      cart = cart.addItem(item)
    }
    return cart
  }

  /**
   *
   * @param {List<OrderedCartItem>} items
   * @private
   */
  static _itemsGroupedByBundle (items) {
    const groupedItems = {}
    items.forEach(item => {
      if (!item.isBundled) return
      /** @type {OrderedCartItemDiscount} */
      const discount = item.discounts.get(0)
      groupedItems[discount.promotionId] = groupedItems[discount.promotionId] || {}
      const itemsByBundleNumber = groupedItems[discount.promotionId]
      itemsByBundleNumber[discount.bundleNumber] = itemsByBundleNumber[discount.bundleNumber] || []
      itemsByBundleNumber[discount.bundleNumber].push(item)
    })
    return groupedItems
  }

  /**
   *
   * @param {Array.<PromotionOrderItemDto>} promotionOrderItems
   * @private
   */
  static _makeGroupedBundle (promotionOrderItems) {
    const bundles = {}
    promotionOrderItems.forEach(item => {
      bundles[item.promotion_id] = bundles[item.promotion_id] || {}
      const bundlesByNumber = bundles[item.promotion_id]
      bundlesByNumber[item.bundle_number] = bundlesByNumber[item.bundle_number] || []
      bundlesByNumber[item.bundle_number].push(item)
    })
    return bundles
  }

  static _dispatchPromotionToItems (orderItems, promotionOrderItems) {
    const itemIdMap = {}
    orderItems.forEach(item => {
      itemIdMap[item.id] = item
      item.promotion_order_items = []
    })
    promotionOrderItems.forEach(promotionOrderItem => {
      const item = itemIdMap[promotionOrderItem.order_item_id]
      item.promotion_order_items.push(promotionOrderItem)
    })
  }

  /**
   * @inheritDoc
   */
  get posOrderNumber () {
    if (this.extendsData.posOrderNumber) {
      return this.extendsData.posOrderNumber
    } else {
      return super.posOrderNumber
    }
  }

  /**
   * @inheritDoc
   */
  get originPosOrderNumber () {
    return this.extendsData.originPosOrderNumber
  }

  /**
   * @inheritDoc
   */
  get totalTaxless (): Decimal {
    return this.extendsData.totalTaxless
  }

  /**
   * @inheritDoc
   */
  get totalTax (): Decimal {
    return this.totalTaxed.sub(this.totalTaxless)
  }

  /**
   * @inheritDoc
   */
  get totalTaxed (): Decimal {
    return this.extendsData.totalTaxed
  }

  /**
   * @inheritDoc
   */
  get standardTaxableTotalTaxless (): Decimal {
    return this.extendsData.standardTotalTaxableItemsTaxless
  }

  /**
   * @inheritDoc
   */
  get standardTaxableTotalTaxed (): Decimal {
    return this.extendsData.standardTotalTaxableItemsTaxed
  }

  /**
   * @inheritDoc
   */
  get reducedTaxableTotalTaxless (): Decimal {
    return this.extendsData.reducedTotalTaxableItemsTaxless
  }

  /**
   * @inheritDoc
   */
  get reducedTaxableTotalTaxed (): Decimal {
    return this.extendsData.reducedTotalTaxableItemsTaxed
  }

  get taxfreeGeneralsTotalTax (): Decimal {
    return this.extendsData.taxfreeGeneralsTotalTax
  }

  get taxfreeExpendablesTotalTax (): Decimal {
    return this.extendsData.taxfreeExpendablesTotalTax
  }

  get taxfreeNotApplicablesTotalTax () {
    return this.extendsData.taxfreeNotApplicablesTotalTax
  }

  /**
   * @return {Date|null}
   */
  get canceledAt () {
    return this.extendsData.canceledAt
  }

  get canceledStaffName () {
    return this.extendsData.canceledStaffName
  }

  get canceledStaffId () {
    return this.extendsData.canceledStaffId
  }

  get isCancel () {
    return !!this.canceledAt
  }

  get obtainedPoint () {
    return this.extendsData.obtainedPoint
  }
}
