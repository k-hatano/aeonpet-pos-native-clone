import * as _ from 'underscore'
import I18n from 'i18n-js'
import SettingKeys from '../../setting/models/SettingKeys'
import CartReceiptBuilder from '../../cart/models/CartReceiptBuilder'

export default class OrderBillBuilder extends CartReceiptBuilder {
  get _headerMessage () {
    return this.getSetting(SettingKeys.RECEIPT.BILL.HEADER_MESSAGE)
  }

  get _headerLogoEnabled () {
    return !!this.getSetting(SettingKeys.RECEIPT.BILL.IS_PRINT_HEADER_LOGO)
  }

  /**
   * 領収証但し書き
   * @return {string}
   * @private
   */
  get _proviso () {
    return this.getSetting(SettingKeys.RECEIPT.BILL.PROVISO) || ''
  }

  /**
   *
   * @param {Cart} cart
   * @return {Array.<ReceiptElementBase>}
   */
  buildOrderBill (cart) {
    return {
      content: _.compact([
        ...this.cartHeaderElements(this._headerMessage, cart),
        this.feedElement(),
        this._underLineElemet('', '様'),
        this.rightTextElement(this.formatMoney(cart.totalTaxed, cart.currency), null, { width: 2 }),
        ...this.taxContent(this.formatMoney(cart.discountedStandardTaxableTotalTaxless, cart.currency)
          , this.formatMoney(cart.standardTotalTax, cart.currency), cart.standardTaxRate),
        ...this.taxContent(this.formatMoney(cart.discountedReducedTaxableTotalTaxless, cart.currency)
          , this.formatMoney(cart.reducedTotalTax, cart.currency), cart.reducedTaxRate),
        this._displayIssuerCode(this._issureCode),
        this.feedElement(),
        this._underLineElemet('但し ' + this._proviso, ''),
        this.feedElement(),
        this.leftTextElement('お支払内訳'),
        ...this.paymentsElements(cart),
        this.feedElement(),
        this.leftTextElement('上記正に領収致しました。'),
        this.feedElement(),
        ...this.taxStampElements(cart),
        this.cutElement()
      ]),
      check_error_strictly: true
    }
  }

  _receiptTitle (cart) {
    return I18n.t('order.receipt_title_bill')
  }

  _headerLogoElement (key) {
    if (this._headerLogoEnabled) {
      return {
        element: 'image',
        key: 'bill_logo'
      }
    }
    return undefined
  }

  _underLineElemet (left, right) {
    return {
      element: 'combine_left_right',
      left: left,
      right: right,
      underline: true
    }
  }

  _combineElemet (left, right) {
    return {
      element: 'combine_left_right',
      left: left,
      right: right
    }
  }
}
