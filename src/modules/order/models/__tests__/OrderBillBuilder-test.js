import OrderBillBuilder from '../OrderBillBuilder'
import ReceiptTextConverter from '../../../printer/models/ReceiptTextConverter'
import I18n from 'i18n-js'

import { propertyA } from '../../../printer/samples'
import initialize from 'common/initialize'
import { BALANCE_TYPE } from '../../models'
import OrderedCart from '../OrderedCart'
I18n.locale = 'ja'

const targetReceiptText = `

              OrangeShop 芝公園店
             東京都港区芝公園2-4-1
                  03-6430-6730
             営業時間 10：00-18：00
                        
         ご来店ありがとうございました。
       またのお越しをお待ちしております。
         クリスマスセール　全品20％OFF

                     領収証

テスト店舗A
取引ID:10000001
2017-09-11 16:26:33
担当者:スタッフ１                               


                                              様
                                     ￥5,001,000
                              (内消費税 ￥1,000)

但し                                            

お支払内訳
現金                                     ￥1,000
クレジット                               ￥2,000
電子マネー                                 ￥100

上記正に領収致しました。


                                ┏━━━━━━┓
                                ┃            ┃
                                ┃  収    入  ┃
                                ┃            ┃
                                ┃  印    紙  ┃
                                ┃            ┃
                                ┗━━━━━━┛
`.slice(1)



describe('Balance Receipt Builder test', () => {
  it('Order bill text build', async () => {
    const orderBillBulder = new OrderBillBuilder()
    await orderBillBulder.initializeAsync()
    const selectedOrder = {
      id: 1,
      title: 'order1',
      client_created_at: '2017-04-05 00:00:00',
      total_paid_taxed: 5001000,
      total_paid_taxless: 5000000,
      total_discount_items_rate: 10,
      total_discount_items_taxless: 2000,
      total_point_cut_items_taxless: 30000,
      total_general_items_taxless: 40000,
      total_general_items_tax: 5000,
      total_expendable_items_taxless: 6000,
      total_expendable_items_tax: 7000,
      total_exclude_items_taxless: 8000,
      total_exclude_items_tax: 9000,
      is_minus: 0,
      status: 4,
      currency: 'jpy',
      order_items: [],
      order_payments: [
        {
          payment_method_type: 1,
          payment_method_name: '現金',
          amount: 1000
        },
        {
          payment_method_type: 2,
          payment_method_name: 'クレジット',
          amount: 2000
        },
        {
          payment_method_type: 3,
          payment_method_name: '電子マネー',
          amount: 100
        }
      ],
      pos_order: {
        pos_order_number: 10000001,
        is_tax_free: 1,
        client_created_at: 1505114793
      },
      used_points: 2000,
      obtained_points: 1000,
      order_member_code: 'CD001',
      origin_pos_order_number: null,
      staff_id: 20000002,
      staff_name: '佐藤',
      shop_name: 'test店舗',
    }
    const orderedCart = new OrderedCart(selectedOrder)
    const receiptObj = orderBillBulder.buildOrderBill(orderedCart)
    const receiptTextConverter = new ReceiptTextConverter()
    const orderBillText = receiptTextConverter.contentToText(receiptObj.content, propertyA)
    expect(orderBillText).toBe(targetReceiptText)
  })
})
