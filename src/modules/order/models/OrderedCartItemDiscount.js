// import { Record } from 'immutable'
import Decimal from 'decimal.js'
import CartItemDiscount from '../../cart/models/CartItemDiscount'
import { BUNDLE_TYPE, DISCOUNT_TYPE, PROMOTION_TYPE } from '../../promotion/models'

// const OrderedCartItemDiscountField = Record({
// })

export default class OrderedCartItemDiscount extends CartItemDiscount {
  /**
   *
   * @param {PromotionOrderItemDto} promotionOrderItem
   */
  constructor (promotionOrderItem) {
    super(OrderedCartItemDiscount._createConstructorParameters(promotionOrderItem))
  }

  setBundleSurplus (surplus) {
    return this.set('bundleSurplus', surplus)
  }

  /**
   *
   * @param {PromotionOrderItemDto} promotionOrderItem
   */
  static _createConstructorParameters (promotionOrderItem) {
    const commonParams = {
      promotionId: promotionOrderItem.promotion_id,
      promotionType: promotionOrderItem.promotion_type,
      promotionName: promotionOrderItem.promotion_name,
      promotionCode: promotionOrderItem.promotion_code,
      discountType: promotionOrderItem.discount_type
    }

    switch (promotionOrderItem.promotion_type) {
      case PROMOTION_TYPE.BUNDLE:
        switch (promotionOrderItem.bundle_type) {
          case BUNDLE_TYPE.SELECTION:
          case BUNDLE_TYPE.BUY_X_GET_Y:
          case BUNDLE_TYPE.FLATPRICE:
            return {
              ...commonParams,
              bundleDiscount: Decimal(promotionOrderItem.discount_taxless || promotionOrderItem.discount_taxed),
              value: Decimal(promotionOrderItem.discount_taxless || promotionOrderItem.discount_taxed),
              bundleType: promotionOrderItem.bundle_type,
              bundleNumber: promotionOrderItem.bundle_group,
              bundlePriceForDisplay:
                promotionOrderItem.bundle_display_price && Decimal(promotionOrderItem.bundle_display_price)
            }

          default:
            return {
              ...commonParams,
              bundleDiscount: Decimal(promotionOrderItem.discount_taxless || promotionOrderItem.discount_taxed),
              value: Decimal(promotionOrderItem.discount_taxless || promotionOrderItem.discount_taxed),
              bundleType: promotionOrderItem.bundle_type,
              bundleNumber: promotionOrderItem.bundle_group
            }
        }

      case PROMOTION_TYPE.SALE:
      default:
        switch (promotionOrderItem.discount_type) {
          case DISCOUNT_TYPE.RATE:
            return {
              ...commonParams,
              value: Decimal(promotionOrderItem.discount_percent).div(100)
            }
          case DISCOUNT_TYPE.AMOUNT:
            return {
              ...commonParams,
              value: Decimal(promotionOrderItem.discount_taxless || promotionOrderItem.discount_taxed)
            }
        }
    }
  }
}
