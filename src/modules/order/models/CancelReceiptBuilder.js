import ReceiptBuilder from '../../printer/models/ReceiptBuilder'
import I18n from 'i18n-js'

const RIGHT_ALIGN = 'right'

export default class CancelReceiptBuilder extends ReceiptBuilder {
  buildCancelReceipt (receipt) {
    return {
      content: [
        ...this._isCancelElement(),
        ...receipt.content
      ]
    }
  }

  _textElement (text, align) {
    return {
      element: 'text',
      text: text,
      align: align
    }
  }

  _isCancelElement () {
    var content = []
    const forShopTitle = '(' + I18n.t('order.cancel') + ')'
    content.push(this._textElement(forShopTitle, RIGHT_ALIGN))
    content.push(this.feedElement())
    return content
  }
}
