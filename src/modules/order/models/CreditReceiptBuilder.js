import * as _ from 'underscore'
import ReceiptBuilder from '../../printer/models/ReceiptBuilder'

export default class CreditReceiptBuilder extends ReceiptBuilder {
  /**
   * @param {MPosResponse} creditResult
   * @param {string} paymentMethodName
   * @param {Cart} cart
   * @return {{content}}
   */
  buildCreditReceipt (creditResult, paymentMethodName, cart) {
    // TODO 翻訳対応
    return {
      content: _.compact([
        ...this.shopNameElements(),
        this.feedElement(),
        {
          element: 'text',
          text: 'クレジット売上票',
          align: 'center',
          tags: ['title'],
          width: 2
        },
        this.centerTextElement('(お客様控え)'),
        this.feedElement(),
        this.combineLeftRightElement('お取扱日', this._formatCreditTime(creditResult.paymentTime)),
        this.combineLeftRightElement('伝票NO', creditResult.orderId),
        this.combineLeftRightElement('クレジット会社', paymentMethodName || ''),
        this.combineLeftRightElement('カードNO', creditResult.cardNumber),
        this.combineLeftRightElement('有効期限', 'XX/XX'),
        this.combineLeftRightElement('承認番号', creditResult.authCode),
        this.combineLeftRightElement('カードタイプ', creditResult.cardType),
        // this.combineLeftRightElement('端末番号', ''), // mPosでは取得できない
        this.combineLeftRightElement('処理通番', creditResult.orderId),
        this.combineLeftRightElement('商品区分', '990'), // 990固定
        this.combineLeftRightElement('取扱区分', this._jpoToLabel(creditResult.jpo)),
        this.combineLeftRightElement('取引内容', creditResult.originOrderId ? '返品' : 'お買上'),
        this._strongTextElement('合計', this.formatMoney(creditResult.amount, cart.currency)),
        creditResult.originOrderId ? this.combineLeftRightElement('元伝票NO', creditResult.originOrderId) : null,
        this.feedElement(),
        this.combineLeftRightElement('取引ID :', cart.posOrderNumber),
        this.staffElement(),
        this.cutElement()
      ])
    }
  }

  _jpoToLabel (jpo) {
    if (!jpo || jpo.length < 2) {
      return ''
    }
    switch (jpo.slice(0, 2)) {
      case '10':
        return '一括払い'
      case '21':
        return 'ボーナス一括払い'
      case '23':
        return 'ボーナス(ボーナス月指定)'
      case '61':
        let count = ''
        if (jpo.slice(2, 3) === 'C') {
          count = jpo.slice(3, 5)
        } else if (jpo.slice(5, 6) === 'C') {
          count = jpo.slice(6, 8)
        }
        return '分割' + count
      case '80':
        return 'リボルビング払い'
      default:
        return ''
    }
  }

  _formatCreditTime (time) {
    if (!time || time.length !== 14) {
      return '----'
    }
    const year = time.slice(0, 4)
    const month = time.slice(4, 6)
    const day = time.slice(6, 8)
    const hour = time.slice(8, 10)
    const minute = time.slice(10, 12)
    const second = time.slice(12, 14)
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`
  }

  _strongTextElement (left, right) {
    return {
      element: 'combine_left_right',
      left,
      right,
      width: 2,
      height: 1
    }
  }
}
