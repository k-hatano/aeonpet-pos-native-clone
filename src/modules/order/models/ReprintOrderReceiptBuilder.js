import ReceiptBuilder from '../../printer/models/ReceiptBuilder'
import I18n from 'i18n-js'

const RIGHT_ALIGN = 'right'
const RE_PRINT_KEY = 'order.receipt_title_reprint'

export default class ReprintOrderReceiptBuilder extends ReceiptBuilder {
  buildReprintOrderReceipt (orderReceipt, rePrintFlg = true) {
    return {
      content: [
        ...rePrintFlg && this._isReprintElement(),
        ...orderReceipt.content
      ]
    }
  }

  _textElement (text, align) {
    return {
      element: 'text',
      text: text,
      align: align
    }
  }

  _isReprintElement () {
    var content = []
    const reprintTitle = '(' + I18n.t(RE_PRINT_KEY) + ')'
    content.push(this._textElement(reprintTitle, RIGHT_ALIGN))
    content.push(this.feedElement())
    return content
  }
}
