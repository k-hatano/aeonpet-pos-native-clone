import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import logger from 'common/utils/logger'
import CartItem from '../../cart/models/CartItem'
import CartItemProduct from '../../cart/models/CartItemProduct'
import OrderedCartItemDiscount from './OrderedCartItemDiscount'
import { PROMOTION_TYPE } from '../../promotion/models'

const OrderItemField = Record({
  salesPrice: Decimal(0),
  price: Decimal(0),
  discountAmount: Decimal(0),
  sortOrder: 0
})

export default class OrderedCartItem extends CartItem {
  // 税込み額と税抜き額でnullでない方を選択する
  static _selectTaxableAmount (taxless, taxed) {
    if (taxless === null) {
      return new Decimal(taxed)
    } else if (taxed === null) {
      return new Decimal(taxless)
    }
  }

  /**
   *
   * @param {OrderItemDto} item
   */
  constructor (item, currency) {
    let sets = null
    try {
      let children = []
      if (item.child_products_data && item.child_products_data !== '') {
        children = JSON.parse(item.child_products_data)
        if (!Array.isArray(children)) {
          throw Error('type error')
        }
      }

      sets = children.map(child => {
        return {
          id: child.product_id,
          name: child.product_name,
          product_name_sys21: child.product_name_sys21,
          product_code: child.product_code,
          tax_rule: child.tax_rule,
          tax_rate: child.tax_rate,
          tax_type: child.tax_type,
          taxfree_type: child.taxfree_type,
          tax_code: child.tax_code,
          quantity: Number(child.quantity),
          is_discountable: 1,
          variant: {
            id: child.product_variant_id,
            sku: child.sku,
            article_number: child.article_number,
            price: child.price,
            cost_price: child.cost_price,
            can_grant_points: child.can_grant_points,
            is_sales_excluded: child.is_sales_excluded,
            stock_mode: child.stock_mode
          }
        }
      })
    } catch (error) {
      logger.error('Invalid Set Product ' + error + ' ' + item.child_products_data)
    }

    const product = {
      ...item,
      id: item.product_id,
      name: item.product_name,
      short_name: item.product_short_name,
      is_parent_product: sets.length > 0 ? 1 : 0,
      is_discountable: 1, // order_itemsに保存項目がないので、常に割引可能扱い
      tax_code: item.tax_code
    }

    const productVariant = {
      ...item,
      id: item.product_variant_id,
      price: item.price_taxless === null ? item.price_taxed : item.price_taxless,
      stock_mode: item.stock_mode
    }

    const totalDiscountAmount = item.promotion_order_items
      .reduce((sum, promotion) => sum.add(OrderedCartItem._selectTaxableAmount(
        promotion.discount_taxless, promotion.discount_taxed
      )), Decimal(0))

    let cartItemProduct = new CartItemProduct(product, productVariant, item.tax_rate, [], item.sales || [], [], sets)
    cartItemProduct = cartItemProduct.changePrice(
      totalDiscountAmount.add(OrderedCartItem._selectTaxableAmount(item.sales_price_taxless, item.sales_price_taxed)))

    // 保留会計の再会計時に(ズバリ価格による)変更前の価格を設定
    const immutablePrice = item.immutable_price_taxless === null ? item.immutable_price_taxed : item.immutable_price_taxless
    if (immutablePrice !== cartItemProduct.immutableOriginalPrice.toNumber() && !!immutablePrice) {
      cartItemProduct = cartItemProduct.overwriteImmutableOriginalPrice(immutablePrice)
    }

    super({
      product: cartItemProduct,
      discounts: List((item.promotion_order_items || []).map(promotionItem => {
        return new OrderedCartItemDiscount(promotionItem)
      })),
      extendsData: new OrderItemField({
        salesPrice: OrderedCartItem._selectTaxableAmount(item.sales_price_taxless, item.sales_price_taxed),
        price: OrderedCartItem._selectTaxableAmount(item.price_taxless, item.price_taxed),
        discountAmount:
          (item.promotion_order_items || []).reduce((sum, item) => {
            const discountAmount = OrderedCartItem._selectTaxableAmount(item.discount_taxless, item.discount_taxed)
            return sum.add(discountAmount)
          }, new Decimal(0))
      }),
      quantity: Number(item.quantity),
      currency: currency,
      isBundled: OrderedCartItem._isBundled(item),
      instanceUniqueKey: 'ordered-cart-item' + ('0000000000' + item.sort_order).slice(-11),
      sortOrder: item.sort_order
    })
  }

  async toCartItemAsync () {
    const changedPrice = this.isChangedPrice ? this.salesPrice : undefined
    const cartItemProduct = await CartItemProduct.createByPendedOrderProduct(this.product, changedPrice)

    return new CartItem({
      product: cartItemProduct,
      discounts: this.discounts,
      quantity: this.quantity,
      currency: this.currency,
      instanceUniqueKey: CartItem.generateInstanceUniqueKey()
    })
  }

  static _isBundled (item) {
    return item.promotion_order_items &&
      item.promotion_order_items.length === 1 &&
      item.promotion_order_items[0].promotion_type === PROMOTION_TYPE.BUNDLE
  }

  /**
   * @inheritDoc
   */
  get salesPrice (): Decimal {
    return this.extendsData.salesPrice
  }

  /**
   * @inheritDoc
   */
  get price () {
    return this.isBundled
      ? this.extendsData.price
      : this.salesPrice.add(this.discountAmount)
  }

  get discountAmount () {
    return this.extendsData.discountAmount
  }
}
