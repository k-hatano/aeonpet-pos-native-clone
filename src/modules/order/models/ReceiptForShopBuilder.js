import ReceiptBuilder from '../../printer/models/ReceiptBuilder'
import I18n from 'i18n-js'

const RIGHT_ALIGN = 'right'

export default class ReceiptForShopBuilder extends ReceiptBuilder {
  buildReceiptForShop (receipt) {
    return {
      content: [
        ...this._isForShopElement(),
        ...receipt.content
      ]
    }
  }

  _textElement (text, align) {
    return {
      element: 'text',
      text: text,
      align: align
    }
  }

  _isForShopElement () {
    var content = []
    const forShopTitle = '(' + I18n.t('order.receipt_for_shop') + ')'
    content.push(this._textElement(forShopTitle, RIGHT_ALIGN))
    content.push(this.feedElement())
    return content
  }
}
