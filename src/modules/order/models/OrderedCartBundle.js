import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import logger from 'common/utils/logger'
import CartBundle from '../../cart/models/CartBundle'
import { BUNDLE_TYPE } from '../../promotion/models'

const BundleExtends = Record({
  bundleDiscountTotal: new Decimal(0)
})

export default class OrderedCartBundle extends CartBundle {
  constructor (items) {
    /** @type {CartItemDiscount} */
    const discount = items.get(0).discounts.get(0)

    // バンドル内は他の値割引、売価変更がないので、単純に売価と商品価格の差の合計で全体の値引き額が求められる
    const bundleDiscountTotal = items.reduce((sum, item) => {
      return sum.add(item.listPrice.sub(item.salesPrice).mul(item.quantity))
    }, new Decimal(0))

    // サーバに送る際に按分のために明細が分かれることがあるので、ここで結合する
    let joinedItems = null
    switch (discount.bundleType) {
      case BUNDLE_TYPE.SELECTION:
        joinedItems = items
          .groupBy(item => item.product.productVariantId)
          .valueSeq()
          .map(items => {
            switch (items.count()) {
              case 1:
                return items.get(0)
              case 2:
                items = items.sortBy(item => Math.abs(item.salesPrice.toNumber()))
                const item1 = items.get(0)
                const item2 = items.get(1)
                const surplus = item2.salesPrice.sub(item1.salesPrice).abs()
                let discount = item2.discounts.get(0)
                discount = discount.setBundleSurplus(surplus)
                return item2
                  .increase(item1.quantity)
                  .clearDiscounts()
                  .applyDiscount(discount)

              default:
                logger.error('Invalid bundled item count')
            }
          })
        break

      case BUNDLE_TYPE.BUY_X_GET_Y:
        joinedItems = items
          .groupBy(item => List([item.product.productVariantId, item.salesPrice]))
          .valueSeq()
          .map(items => {
            const totalQuantity = items.reduce((sum, item) => item.quantity + sum, 0)
            return items.first().changeQuantity(totalQuantity)
          })
        break

      case BUNDLE_TYPE.FLATPRICE:
        joinedItems = items
          .groupBy(item => item.product.productVariantId)
          .valueSeq()
          .map(items => {
            const totalQuantity = items.reduce((sum, item) => item.quantity + sum, 0)
            return items.first().changeQuantity(totalQuantity)
          })
        break

      case BUNDLE_TYPE.CASES:
        // CASESはあまりが発生しないので、くっつける必要なし
        joinedItems = items
        break
    }

    super(
      { id: discount.promotionId, name: discount.promotionName },
      joinedItems,
      discount.bundleNumber,
      new BundleExtends({ bundleDiscountTotal })
    )
  }

  get bundleDiscountTotal () {
    return this.extendsData.bundleDiscountTotal
  }
}
