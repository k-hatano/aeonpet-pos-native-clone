import React from 'react'
import { Text, View, ListView } from 'react-native'
import styles from '../styles/PendedOrderModalView'
import PropTypes from 'prop-types'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import Modal from 'common/components/widgets/Modal'
import ModalFrame from 'common/components/widgets/ModalFrame'
import moment from 'moment'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'

class PendedOrderModalItemView extends React.Component {
  static propTypes = {
    pendedOrder: PropTypes.object,
    onSelect: PropTypes.func
  }

  _renderOtherItem () {
    return `他${this.props.pendedOrder.order_items.length - 1}点`
  }

  render () {
    const pendedOrder = this.props.pendedOrder
    return (
      <TouchableOpacitySfx onPress={this.props.onSelect}>
        <View style={styles.rowView}>
          <View style={styles.TextTimeWraper}>
            <Text style={styles.rowTextTime}>
              {moment.unix(pendedOrder.pos_order.client_created_at).format('HH:mm:ss')}
            </Text>
          </View>
          <View style={styles.colTextItem}>
            <Text style={styles.colTextMainItem}>
              {pendedOrder.order_items[0].product_name}
            </Text>
            {pendedOrder.order_items.length > 1 && <Text style={styles.colTextOtherItem}>
              {this._renderOtherItem()}
            </Text>}
          </View>
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export default class PendedOrderModalView extends React.Component {
  static propTypes = {
    pendedOrders: PropTypes.array,
    onSelect: PropTypes.func,
    beforeShowMore: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  async _onShowMore () {
    Modal.close()
    if (await this.props.beforeShowMore()) {
      Actions.home({ initialTab: 'pend' })
    }
  }

  render () {
    const pendedOrders = this._slicePendedOrders()
    const modalHeight = this.props.pendedOrders.length > 5 ? 460 : 70 + (64 * this.props.pendedOrders.length)
    return (
      <ModalFrame
        style={[styles.container, {height: modalHeight}]}
        title={I18n.t('order_pended.pending_order')}
        onClose={() => Modal.close()} >
        <ListView
          dataSource={this.dataSource.cloneWithRows(pendedOrders)}
          renderRow={(item) => (
            <PendedOrderModalItemView
              pendedOrder={item}
              onSelect={() => { this.props.onSelect(item) }} />)}
          renderSeparator={(sectionId, rowId) => (<View key={rowId} style={styles.separator} />)} />
        {this.props.pendedOrders.length > 5 &&
          <TouchableOpacitySfx onPress={() => this._onShowMore()}>
            <View style={styles.rowView}>
              <Text style={styles.moreText}>
                {I18n.t('order_pended.more')}
              </Text>
            </View>
          </TouchableOpacitySfx>
        }
      </ModalFrame>

    )
  }

  _slicePendedOrders () {
    return this.props.pendedOrders.length > 5 ? this.props.pendedOrders.slice(0, 5) : this.props.pendedOrders
  }

  static openModalBy (pendedOrders, ref, onSelect, beforeShowMore) {
    class ModalContent extends React.Component {
      render () {
        return (
          <PendedOrderModalView
            pendedOrders={pendedOrders}
            onSelect={(pendedOrder) => onSelect(pendedOrder)}
            beforeShowMore={beforeShowMore} />
        )
      }
    }

    Modal.openBy(ModalContent, ref, {
      direction: Modal.DIRECTION.BOTTOM,
      adjust: Modal.ADJUST.RIGHT,
      isBackgroundVisible: false
    })
  }
}