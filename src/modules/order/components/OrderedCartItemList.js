import React from 'react'
import { View } from 'react-native'
import CartItemList from '../../cart/components/CartItemList'
import OrderedCartBundledItemView from './OrderedCartBundledItemView'
import OrderedCartItemView from './OrderedCartItemView'
import styles from 'modules/cart/styles/CartItemList'

export default class OrderedCartItemList extends CartItemList {
  _renderBundledItem (item) {
    return (
      <OrderedCartBundledItemView
        bundle={item.bundle}
        currency={this.props.currency}
        items={item.items}
        onItemChanged={this.props.onItemChanged} />
    )
  }

  _renderNormalItem (item) {
    return (
      <View>
        <OrderedCartItemView
          item={item.item}
          onItemChanged={this.props.onItemChanged} />
        <View style={styles.separator} />
      </View>
    )
  }
}
