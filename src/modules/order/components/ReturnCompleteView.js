import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/ReturnCompleteView'
import baseStyle from 'common/styles/baseStyle'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import { CommandButton } from '../../../common/components/elements/StandardButton'

export default class ReturnCompleteView extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    onNext: PropTypes.func,
    cart: PropTypes.object
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  render () {
    /** @type {Cart} */
    const cart = this.props.cart

    return (
      <View style={styles.layoutRoot}>
        <View style={styles.titleArea}>
          <Text style={styles.completeText}>{I18n.t('return_complete.return_completed')}</Text>
        </View>
        <View style={styles.amountArea}>
          <View style={styles.amountInnerArea}>
            <View style={styles.amountLine}>
              <Text style={styles.label}>{I18n.t('return_complete.return_amount')}</Text>
              <Text style={styles.valueText}>
                {formatPriceWithCurrency(cart.totalTaxed, cart.currency)}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.nextArea}>
          <CommandButton
            onPress={this.props.onNext}
            text={I18n.t('return_complete.completed')}
            style={styles.nextButton}
          />
        </View>
      </View>
    )
  }
}
