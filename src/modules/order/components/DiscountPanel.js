'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'
import I18n from 'i18n-js'

export default class DiscountPanel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false,
      discountType: [
        {
          title: `${I18n.t('order.price_discount')} （-${I18n.t('common.japanese_yen')}）`,
          type: 'absolute',
          value: false
        },
        {
          title: `${I18n.t('order.percent_discount')} （-％）`,
          type: 'relative',
          value: false
        },
        {
          title: I18n.t('order.change_price_discount'),
          type: 'change_price',
          value: false
        }
      ],
      presets: [
        {
          title: I18n.t('order.rainy_day_discount'),
          type: 'relative',
          value: 10
        },
        {
          title: I18n.t('order.friend_discount'),
          type: 'relative',
          value: 5
        },
        {
          title: I18n.t('order.child_discount'),
          type: 'absolute',
          value: 100
        },
        {
          title: I18n.t('order.early_morning_discount'),
          type: 'absolute',
          value: 50
        },
        {
          title: I18n.t('order.late_night_discount'),
          type: 'absolute',
          value: 50
        }
      ]
    }
  }

  toggle () {
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen
    })
  }

  render () {
    const { discountType, presets, isOpen } = this.state
    const { setDiscount } = this.props

    if (!isOpen) {
      return <View />
    }

    return (
      <View style={styles.container}>
        <View style={styles.discountTypeContainer}>
          {discountType &&
            discountType.map((item, index) =>
              <View key={index} style={styles.discountTypeItem}>
                <TouchableOpacity onPress={() => setDiscount(item)} style={styles.discountItemTouchable}>
                  <Text style={styles.discountItemText}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
        </View>
        <View style={styles.presetTitle}>
          <Text style={styles.presetTitleText}>
            {I18n.t('order.preset_discount')}
          </Text>
        </View>
        <View style={styles.presetContainer}>
          {presets &&
            <ScrollView style={styles.contents} ref='list' contentContainerStyle={{ alignItems: 'center' }}>
              {presets &&
                presets.map((item, index) => {
                  const type = item.type === 'relative'
                    ? `（-${item.value}%）`
                    : `（-${I18n.t('common.amount_of_money', { amount: item.value })}）`
                  return (
                    <View key={index} style={styles.discountTypeItem}>
                      <TouchableOpacity onPress={() => setDiscount(item)} style={styles.discountItemTouchable}>
                        <Text style={styles.discountItemText}>
                          {`${item.title} ${type}`}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                })}
            </ScrollView>}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: 398,
    height: 564,
    borderWidth: 1,
    borderColor: '#d8d8d8',
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    position: 'absolute',
    left: '100%',
    zIndex: 2
  },
  contents: {
    width: '100%',
    height: '100%'
  },
  discountTypeContainer: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center'
  },
  discountTypeItem: {
    width: 359,
    height: 64,
    borderColor: '#d8d8d8',
    borderBottomWidth: 1
  },
  discountItemTouchable: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'center'
  },
  discountItemText: {
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29
  },
  presetTitle: {
    backgroundColor: '#f0f0f0',
    height: 38,
    justifyContent: 'center'
  },
  presetTitleText: {
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29,
    paddingLeft: 10
  },
  presetContainer: {
    flex: 1,
    width: '100%'
  }
})
