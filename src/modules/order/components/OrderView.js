'use strict'

import React, { Component } from 'react'
import { ScrollView, View, Text } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import ImageButton from '../../../common/components/elements/ImageButton'
import { formatPriceWithCurrency, formatUnit } from '../../../common/utils/formats'
import componentStyles from '../styles/OrderView'
import { IS_MINUS, IS_TAX_FREE, PROMOTION_TYPE } from '../models'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'
import moment from 'moment'
import Decimal from 'decimal.js'
import { OWNER_DISCOUNT } from '../../promotion/models'

export default class OrderView extends Component {
  static propTypes = {
    selectedOrder: PropTypes.object,
    returnedOrder: PropTypes.object
  }

  _formatAmount = (amount, currency) => {
    if (amount && !Decimal(0).equals(amount)) {
      return formatPriceWithCurrency(amount, currency)
    }
    return false
  }

  _formatAmountWithTaxSwitch (taxlessValue, taxedValue, isAbs = true) {
    const { currency } = this.props.selectedOrder
    if (taxlessValue === null) {
      return this._formatAmount(isAbs ? new Decimal(taxedValue).abs() : taxedValue, currency)
    } else if (taxedValue === null) {
      return this._formatAmount(isAbs ? new Decimal(taxlessValue).abs() : taxlessValue, currency)
    }
    return '!!!!'
  }

  _renderSummaryRow = (title, value, icon, extraValue) => {
    if ((!title && !value) || (value !== null && value === false)) {
      return <View />
    }

    if (value === '') {
      value = null
    }

    const randomKey = Math.floor(1000 * Math.random()).toString(16)
    return (
      <View key={title + value + randomKey} style={componentStyles.summaryOrderDate}>
        <View style={componentStyles.summaryTitleWrapper}>
          {icon &&
            <ImageButton
              style={componentStyles.childRowIcon}
              appearance={{
                normal: icon,
                highlight: icon
              }}
              onPress={() => {}}
            />}
          {title && <Text style={componentStyles.summaryTitleText}>{title}</Text>}
        </View>
        <View style={componentStyles.summaryValueWrapper}>
          {extraValue && <Text style={componentStyles.summaryExtraValueText}>{extraValue}</Text>}
          {value && <Text style={componentStyles.summaryValueText}>{value}</Text>}
        </View>
      </View>
    )
  }

  _renderOrderTypeToTitleLabel = (selectedOrder) => {
    switch (selectedOrder.is_minus) {
      case IS_MINUS.RETURNED:
        return (
          this._renderSummaryRow(
            I18n.t('order.returned_at'),
            moment.unix(selectedOrder.pos_order.client_created_at).format('YYYY-MM-DD HH:mm:ss')
          )
        )
      default:
        return (
          this._renderSummaryRow(
            I18n.t('order.ordered_at'),
            moment.unix(selectedOrder.pos_order.client_created_at).format('YYYY-MM-DD HH:mm:ss')
          )
        )
    }
  }

  _getAmountLabel = (selectedOrder) => {
    switch (selectedOrder.is_minus) {
      case IS_MINUS.RETURNED:
        return I18n.t('order.return_amount')
      default:
        return I18n.t('order.amount')
    }
  }

  _renderPromotionType = (selectedOrder) => {
    if (selectedOrder.promotion_orders && selectedOrder.promotion_orders.length > 0) {
      return selectedOrder.promotion_orders.map((promotion) => {
        const amount = new Decimal(promotion.subtotal_discounts_taxless || promotion.subtotal_discounts_taxed).abs()
        if (amount.eq(0)) {
          return null
        }
        // オーナー割引表示
        if (promotion.discount_reason_name === OWNER_DISCOUNT.NAME) {
          return this._renderSummaryRow(
            I18n.t('cart.owner_discount'),
            formatPriceWithCurrency(amount, selectedOrder.currency)
          )
        }
        switch (promotion.discount_type) {
          case PROMOTION_TYPE.VALUE:
            return this._renderSummaryRow(
              I18n.t('order.subtotal_value_reduce'),
              formatPriceWithCurrency(amount, selectedOrder.currency)
            )
          case PROMOTION_TYPE.PERCENT:
            return this._renderSummaryRow(
              I18n.t('order.subtotal_percent_discount'),
              formatPriceWithCurrency(amount, selectedOrder.currency),
              null,
              promotion.discount_percent ? `${Decimal(promotion.discount_percent).toFixed(0)}%OFF` : null
            )
          default:
            return null
        }
      })
    } else {
      return null
    }
  }

  _renderTaxFree = (selectedOrder) => {
    const posOrder = selectedOrder.pos_order

    if (posOrder.is_tax_free !== IS_TAX_FREE) {
      return null
    }

    const generalItemsTaxLabel = posOrder.is_general_items_taxfree ? I18n.t('order.total_general_items_taxfree') : I18n.t('order.total_general_items_tax')
    const expendableItemsTaxLabel = posOrder.is_expendable_items_taxfree ? I18n.t('order.total_expendable_items_taxfree') : I18n.t('order.total_expendable_items_tax')

    return (
      <View>
        {posOrder.total_general_items_taxless && this._renderSummaryRow(
          I18n.t('order.total_general_items_taxless'),
          this._formatAmount(Decimal(posOrder.total_general_items_taxless).abs(), selectedOrder.currency)
        )}
        {posOrder.total_general_items_tax && this._renderSummaryRow(
          generalItemsTaxLabel,
          this._formatAmount(Decimal(posOrder.total_general_items_tax).abs(), selectedOrder.currency),
          require('../../../assets/images/homepage/enter.png')
        )}
        {posOrder.total_expendable_items_taxless && this._renderSummaryRow(
          I18n.t('order.total_expendable_items_taxless'),
          this._formatAmount(Decimal(posOrder.total_expendable_items_taxless).abs(), selectedOrder.currency)
        )}
        {posOrder.total_expendable_items_tax && this._renderSummaryRow(
          expendableItemsTaxLabel,
          this._formatAmount(Decimal(posOrder.total_expendable_items_tax).abs(), selectedOrder.currency),
          require('../../../assets/images/homepage/enter.png')
        )}
        {posOrder.total_exclude_items_taxless && this._renderSummaryRow(
          I18n.t('order.total_exclude_items_taxless'),
          this._formatAmount(Decimal(posOrder.total_exclude_items_taxless).abs(), selectedOrder.currency)
        )}
        {posOrder.total_exclude_items_tax && this._renderSummaryRow(
          I18n.t('order.total_exclude_items_tax'),
          this._formatAmount(Decimal(posOrder.total_exclude_items_tax).abs(), selectedOrder.currency),
          require('../../../assets/images/homepage/enter.png')
        )}
      </View>
    )
  }

  _renderPaymentMethodType = (selectedOrder) => {
    let paymentMethodTitleLabel = I18n.t('order.payment_methods')

    if (selectedOrder.is_minus === IS_MINUS.RETURNED) {
      paymentMethodTitleLabel = I18n.t('order.return_payment_methods')
    }

    return (
      <View>
        {this._renderSummaryRow(paymentMethodTitleLabel)}
        {selectedOrder.order_payments.map((payment) => {
          return payment.payment_method_type !== PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT &&
            payment.payment_method_type !== PAYMENT_METHOD_TYPE.GIFT_SURPLUS &&
            this._renderSummaryRow(payment.payment_method_name || '-',
              formatPriceWithCurrency(this._calcAmountWithChange(payment), selectedOrder.currency),
              require('../../../assets/images/homepage/enter.png'))
        })}
      </View>
    )
  }

  _calcAmountWithChange (payment) {
    if (payment.payment_method_type === PAYMENT_METHOD_TYPE.CASH) {
      return Decimal.sub(Decimal(payment.amount).abs(), payment.change).abs()
    } else {
      return Decimal(payment.amount).abs()
    }
  }

  _renderOrderNumber= (selectedOrder) => {
    if (selectedOrder.is_minus === IS_MINUS.RETURNED) {
      return (
        <View>
          {this._renderSummaryRow(I18n.t('order.origin_order_id'), selectedOrder.pos_order.origin_pos_order_number)}
          {this._renderSummaryRow(I18n.t('order.order_code'), selectedOrder.pos_order.pos_order_number)}
        </View>
      )
    } else {
      return this._renderSummaryRow(I18n.t('order.order_code'), selectedOrder.pos_order.pos_order_number)
    }
  }

  _renderOrderStaff= (selectedOrder) => {
    if (selectedOrder.is_minus === IS_MINUS.RETURNED) {
      return this._renderSummaryRow(I18n.t('order.return_staff'), selectedOrder.pos_order.staff_name)
    } else {
      return this._renderSummaryRow(I18n.t('order.staff'), selectedOrder.pos_order.staff_name)
    }
  }

  _renderCustomer= (selectedOrder) => {
    if (selectedOrder.customer_code) {
      let pointLabel = selectedOrder.is_minus === IS_MINUS.RETURNED
        ? I18n.t('order.subtracted_point')
        : I18n.t('order.obtained_points')
      return (
        <View>
          {(!selectedOrder.hasOwnProperty('is_pushed') || selectedOrder.is_pushed) &&
            this._renderSummaryRow(pointLabel, formatUnit(Decimal(selectedOrder.obtained_points || 0).abs(), 'pt'))}
          {this._renderSummaryRow(I18n.t('order.customer_code'), selectedOrder.customer_code)}
        </View>
      )
    } else {
      return null
    }
  }

  /**
   *
   * @param {OrderDto} order
   * @private
   */
  _renderCancelInfo (order) {
    if (!order.pos_order.canceled_at) {
      return null
    }
    const canceledAt = moment.unix(order.pos_order.canceled_at).format('YYYY-MM-DD HH:mm:ss')
    return (
      <View>
        {this._renderSummaryRow(I18n.t('order.canceled_at'), canceledAt)}
        {this._renderSummaryRow(I18n.t('order.canceled_staff'), order.pos_order.canceled_staff_name)}
      </View>
    )
  }

  /**
   * 返品会計に小計割引データがないため、
   * 返品会計表示においても、小計割引表示は
   * 購入会計データを利用(_renderPromotionType)
   */
  _renderOrder= (selectedOrder, returnedOrder) => {
    const useOrder = returnedOrder === null ? selectedOrder : returnedOrder
    return (
      <ScrollView>
        {useOrder && <View style={componentStyles.summaryContainer}>
          {this._renderOrderTypeToTitleLabel(useOrder)}
          {this._renderSummaryRow(
            this._getAmountLabel(useOrder),
            this._formatAmount(new Decimal(useOrder.total_paid_taxed).abs(), useOrder.currency)
          )}
          {this._renderSummaryRow(
            I18n.t('order.tax'),
            formatPriceWithCurrency(Decimal.sub(
              useOrder.total_paid_taxed, useOrder.total_paid_taxless).abs(), useOrder.currency)
          )}
          {this._renderPromotionType(selectedOrder)}
          {this._renderTaxFree(useOrder)}
          {this._renderPaymentMethodType(useOrder)}
          {this._renderCustomer(useOrder)}
          {this._renderOrderNumber(useOrder)}
          {this._renderOrderStaff(useOrder)}
          {this._renderCancelInfo(useOrder)}
        </View>}
      </ScrollView>
    )
  }

  _renderPendedOrder= (selectedOrder) => {
    return (
      <ScrollView>
        {selectedOrder && <View style={componentStyles.summaryContainer}>
          {this._renderSummaryRow(
            I18n.t('order.pended_at'),
            moment.unix(selectedOrder.pos_order.client_created_at).format('YYYY-MM-DD HH:mm:ss'))}
          {this._renderSummaryRow(
            I18n.t('order.amount'),
            this._formatAmount(Math.abs(selectedOrder.total_paid_taxed), selectedOrder.currency)
          )}
          {this._renderSummaryRow(
            I18n.t('order.tax'),
            formatPriceWithCurrency(Decimal.sub(selectedOrder.total_paid_taxed, selectedOrder.total_paid_taxless).abs(), selectedOrder.currency)
          )}
          {selectedOrder.customer_code ? this._renderSummaryRow(I18n.t('order.customer_code'), selectedOrder.customer_code) : null}
          {selectedOrder.customer_name ? this._renderSummaryRow(I18n.t('order.customer_name'), selectedOrder.customer_name) : null}
          {this._renderOrderStaff(selectedOrder)}
        </View>}
      </ScrollView>
    )
  }

  render () {
    const { selectedOrder, returnedOrder } = this.props
    return (
      <View style={componentStyles.container}>
        {selectedOrder.is_pended ? this._renderPendedOrder(selectedOrder) : this._renderOrder(selectedOrder, returnedOrder)}
      </View>
    )
  }
}
