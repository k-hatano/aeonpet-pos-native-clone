import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/OrderCompleteView'
// import baseStyle from 'common/styles/baseStyle'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import OrderCompleteActionButton from '../containers/OrderCompleteActionButton'
import { PERMISSION_CODES } from '../../staff/models'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OrderCompleteView extends Component {
  constructor () {
    super()
    this.state = {
      isDisabled: false
    }
  }

  static propTypes = {
    onInit: PropTypes.func,
    onNext: PropTypes.func,
    cart: PropTypes.object,
    repositoryContext: PropTypes.string,
    billEJournal: PropTypes.object,
    onCancel: PropTypes.func
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
    this._isMounted = true
  }

  componentWillUnmount () {
    this._isMounted = false
  }

  _onPress = async (callback) => {
    this.setState({isDisabled: true})
    try {
      await callback()
    } finally {
      if (this._isMounted) {
        this.setState({isDisabled: false})
      }
    }
  }

  /**
   *
   * @param {Cart} cart
   * @param {boolean} isTraining
   * @return {*[]}
   */
  buttonDefinitions (cart, isTraining) {
    const { billEJournal } = this.props
    return [
      {
        titleKey: 'order_completed.reprint_receipt',
        onPress: this.props.onPressReprintReceipt,
        disabled: !cart.isPushed,
        permissionCode: null
      },
      {
        titleKey: 'order_completed.issue_bill',
        onPress: this.props.onPressPrintIssueBill,
        disabled: !cart.isPushed || !!billEJournal,
        permissionCode: null
      },
      {
        titleKey: 'order_completed.cancel_order',
        onPress: () => this.props.onCancel(cart),
        disabled: !cart.isPushed || !!billEJournal,
        permissionCode: isTraining ? null : PERMISSION_CODES.CANCEL_ORDER
      },
      {
        titleKey: 'order_completed.return_order',
        onPress: this.props.onPressReturn,
        disabled: !cart.isPushed,
        permissionCode: isTraining ? null : PERMISSION_CODES.RETURN_ORDER
      },
      {
        titleKey: 'order_completed.issue_tax_free_sheets',
        disabled: !cart.isTaxfreeApplied,
        onPress: this.props.onPressPrintTaxFreeSheets,
        permissionCode: isTraining ? null : PERMISSION_CODES.PRINT_TAXFREE_RECEIPT
      }
    ]
  }

  render () {
    /** @type {Cart} */
    const cart = this.props.cart
    const isTraining = this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING

    return (
      <View style={styles.layoutRoot}>
        <View style={styles.titleArea}>
          <Text style={styles.completeText}>{I18n.t('order_completed.order_completed')}</Text>
        </View>
        <View style={styles.amountArea}>
          <View style={styles.amountInnerArea}>
            <View style={styles.amountLine1}>
              <Text style={styles.label1}>{I18n.t('order_completed.total_paid')}</Text>
              <Text style={styles.valueText1}>
                {formatPriceWithCurrency(cart.totalTaxed, cart.currency)}
              </Text>
            </View>
            <View style={styles.amountLine2}>
              <Text style={styles.label1}>{I18n.t('order_completed.deposit')}</Text>
              <Text style={styles.valueText2}>
                {formatPriceWithCurrency(cart.deposit, cart.currency)}
              </Text>
            </View>
            <View style={styles.amountLine3}>
              <Text style={styles.label2}>{I18n.t('order_completed.change')}</Text>
              <Text style={styles.valueText3}>
                {formatPriceWithCurrency(cart.change, cart.currency)}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.nextArea}>
          <CommandButton
            onPress={this.props.onNext}
            text={I18n.t('order_completed.new_order')}
            style={styles.nextButton}
          />
        </View>
        <View style={styles.separator} />
        <View style={styles.buttonArea}>
          <View style={{ flex: 35 }} />
          <View style={styles.buttonAreaContainer}>
            {this.buttonDefinitions(cart, isTraining).map((button, index) => {
              return (
                <View style={styles.buttonLayout} key={index}>
                  {button.disabled ? null : (
                    <OrderCompleteActionButton
                      title={I18n.t(button.titleKey)}
                      onPress={() => this._onPress(button.onPress)}
                      permissionCode={button.permissionCode}
                      disabled={this.state.isDisabled}
                    />
                  )}
                </View>
              )
            })}
          </View>
          <View style={{flex: 35}} />
        </View>
      </View>
    )
  }
}
