import React, { Component } from 'react'
import { View, Text, ScrollView } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OrderPropertyModalView'
import I18n from 'i18n-js'
import { RadioGroup } from '../../../common/components/elements/Inputs'
import Cart from '../../cart/models/Cart'
import Modal from 'common/components/widgets/Modal'
import { ProceedButton, CommandButton, UnSelectedButton } from '../../../common/components/elements/StandardButton'

export default class OrderPropertyModalViewItems extends Component {
  static propTypes = {
    orderProperty: PropTypes.array,
    selectedProperty: PropTypes.array,
    cart: React.PropTypes.instanceOf(Cart)
  }

  constructor (props) {
    super(props)
    this.state = {
      selectedProperties: this.props.selectedProperties
    }
  }

  _close (selectedProperties, orderProperty) {
    let orderProperties = []
    for (let i = 0; i < orderProperty.length; i++) {
      if (orderProperty[i].options[selectedProperties[i]] !== undefined) {
        orderProperties.push({
          ...orderProperty[i].options[selectedProperties[i]],
          pos_order_propery_id: orderProperty[i].id
        })
      }
    }
    this.props.onClose(orderProperties)
  }

  _setBool (orderProperty, selectedProperties) {
    let bool = false
    for (let i = 0; i < orderProperty.length; i++) {
      for (let j = 0; j < orderProperty[i].options.length; j++) {
        if (orderProperty[i].required === 1 && orderProperty[i].options[j].status === 1 && selectedProperties[i] == null) {
          bool = true
        }
      }
    }
    return bool
  }

  render () {
    const orderProperty = this.props.orderProperty
    const selectedProperties = this.state.selectedProperties
    const bool = this._setBool(orderProperty, selectedProperties)

    let properties = []
    for (let i = 0; i < orderProperty.length; i++) {
      let propertyButtons = []
      let optionCount = 0
      if (orderProperty[i].options) {
        for (let j = 0; j < orderProperty[i].options.length; j++) {
          if (orderProperty[i].options[j].status === 0) {
            continue
          }
          optionCount++
          propertyButtons.push(
            <View style={styles.itemButtonContainer} key={j}>
              {selectedProperties[i] === j &&
                <CommandButton
                  style={styles.itemButton}
                  text={orderProperty[i].options[j].name}
                  onPress={() => {
                    this.props.onSelectedOptions(i, j, selectedProperties)
                    this.setState({selectedProperties: this.state.selectedProperties})
                  }} />
              }
              {selectedProperties[i] !== j &&
                <UnSelectedButton
                  style={styles.itemButton}
                  text={orderProperty[i].options[j].name}
                  onPress={() => {
                    this.props.onSelectedOptions(i, j, selectedProperties)
                    this.setState({selectedProperties: this.state.selectedProperties})
                  }} />
              }
            </View>
          )
        }
      }
      if (optionCount > 0) {
        properties.push(
          <View style={styles.itemRoot} key={i}>
            <View style={styles.itemTitleContainer}>
              <Text style={styles.itemTitleText}>{orderProperty[i].name}</Text>
            </View>
            <View style={styles.itemButtonsArea}>
              {propertyButtons}
            </View>
          </View>
        )
      }
    }
    return (
      <View style={styles.itemRoot}>
        <ScrollView>
          {properties}
        </ScrollView>
        <View style={styles.submitButtonContainer}>
          <ProceedButton
            text={I18n.t('order.confirm')}
            style={styles.submitButton}
            onPress={() => this._close(selectedProperties, orderProperty)}
            disabled={bool} />
        </View>
      </View>
    )
  }
}
