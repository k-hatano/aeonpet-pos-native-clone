import React from 'react'
import PropTypes from 'prop-types'
import CustomPropTypes from 'common/components/CustomPropTypes'
import { OptionalCommandButton, ProceedButton, WeakProceedButton } from '../../../common/components/elements/StandardButton'

export default class OrderHistoryActionButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    buttonComponent: PropTypes.any,
    style: CustomPropTypes.Style,
    textStyle: CustomPropTypes.Style,
    onPress: PropTypes.func,
    disabled: PropTypes.bool
  }

  render () {
    // TODO リファクタリング switch文を使わず、buttonComponentをそのまま描画に使えば良い
    // TODO リファクタリング {...this.props}で全てのプロパティを渡しても良いかも
    switch (this.props.buttonComponent) {
      case WeakProceedButton:
        return (
          <WeakProceedButton text={this.props.title} {...this.props} />
        )
      case ProceedButton:
        return (
          <ProceedButton text={this.props.title} {...this.props} />
        )
      default:
        return (
          <OptionalCommandButton text={this.props.title} {...this.props} />
        )
    }
  }
}