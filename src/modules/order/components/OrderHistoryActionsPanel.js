import React, { Component } from 'react'
import { View, NativeEventEmitter, NativeModules } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/OrderHistoryActionsPanel'
import { STATUS, IS_MINUS, orderHasPaymentMethodType } from '../models'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'
import OrderHistoryActionButton from '../containers/OrderHistoryActionButton'
import { PERMISSION_CODES } from 'modules/staff/models'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import { OptionalCommandButton, ProceedButton, WeakProceedButton } from '../../../common/components/elements/StandardButton'

export default class OrderHistoryActionsPanel extends Component {
  static propTypes = {
    selectedOrder: PropTypes.object,
    onPrintTaxFreeSheet: PropTypes.func,
    onReprintReceipt: PropTypes.func,
    onPrintBill: PropTypes.func,
    onCancel: PropTypes.func,
    onReturn: PropTypes.func,
    onReorder: PropTypes.func,
    onFindEjournal: PropTypes.func,
    moduleName: PropTypes.string,
    repositoryContext: PropTypes.string,
    onDeletePendedOrder: PropTypes.func,
    onReturnPendedOrder: PropTypes.func,
    onReprintCreditReceipt: PropTypes.func,
    isBillPrinted: PropTypes.bool,
    cashierId: PropTypes.string
  }

  async componentDidMount () {
    await this._onDisplayBillButton() 
  }

  _onDisplayBillButton = async () => {
    if (!this.props.selectedOrder) {
      return
    }
    await this.props.onFindEjournal(this.props.selectedOrder.pos_order.pos_order_number)
  }

  _onPrintBill = async (order) => {
    const result = await this.props.onPrintBill(order)
    if (result) {
      await this._onDisplayBillButton()
    }
  }

  _onRenderOrderButton = (order) => {
    if (!order || order.is_pended) {
      return []
    }

    const { isBillPrinted } = this.props
    const hasAccountsReceivablePayment = orderHasPaymentMethodType(order, PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE)
    const isReturnOrReturnedOrCanceled =
      order.status === STATUS.RETURNED || order.is_minus === IS_MINUS.RETURNED || order.status === STATUS.CANCELED
    const isOwnOrder = order.pos_order.cashier_id === this.props.cashierId

    let buttons = [
      {
        title: I18n.t('order_history_action.reprint_receipt'),
        permissionCode: null,
        display: null,
        buttonComponent: OptionalCommandButton,
        onPress: (order) => {
          this.props.onReprintReceipt(order)
        },
        disabled: false
      },
      {
        title: I18n.t('order_history_action.issue_bill'),
        permissionCode: null,
        style: null,
        buttonComponent: OptionalCommandButton,
        onPress: (order) => {
          this._onPrintBill(order)
        },
        disabled: isBillPrinted || hasAccountsReceivablePayment || isReturnOrReturnedOrCanceled
      },
      {
        title: I18n.t('order_history_action.cancel_order'),
        permissionCode: PERMISSION_CODES.CANCEL_ORDER, // TODO CANCEL_ORDERに変更
        style: null,
        buttonComponent: OptionalCommandButton,
        onPress: (order) => {
          this.props.onCancel(order, this.props.moduleName)
        },
        disabled: isReturnOrReturnedOrCanceled || isBillPrinted || !!order.finalized_at || !isOwnOrder
      },
      {
        title: I18n.t('order_history_action.return_order'),
        permissionCode: PERMISSION_CODES.RETURN_ORDER,
        style: null,
        buttonComponent: OptionalCommandButton,
        onPress: (order) => {
          this.props.onReturn(order, this.props.moduleName)
        },
        disabled: isReturnOrReturnedOrCanceled
      },
      {
        title: I18n.t('order_history_action.issue_tax_free_sheets'),
        permissionCode: PERMISSION_CODES.PRINT_TAXFREE_RECEIPT,
        style: null,
        buttonComponent: OptionalCommandButton,
        onPress: (order) => {
          this.props.onPrintTaxFreeSheet(order)
        },
        disabled: !order.pos_order.is_tax_free || isReturnOrReturnedOrCanceled
      }
      // {
      //   title: I18n.t('order_completed.reprint_credit_receipt'),
      //   permissionCode: null,
      //   style: null,
      //   buttonComponent: OptionalCommandButton,
      //   onPress: (order) => {
      //     this.props.onReprintCreditReceipt(order)
      //   },
      //   disabled: !orderHasPaymentMethodType(order, PAYMENT_METHOD_TYPE.CREDIT)
      // }
    ]

    return buttons
  }

  _onRenderPendedButton () {
    const pendButtons = [
      {
        title: I18n.t('order_pended.delete_pended'),
        permissionCode: null,
        style: null,
        buttonComponent: WeakProceedButton,
        onPress: (order) => {
          this.props.onDeletePendedOrder(order)
        }
      },
      {
        title: null,
        permissionCode: null,
        buttonComponent: null,
        disabled: true
      },
      {
        title: null,
        permissionCode: null,
        buttonComponent: null,
        disabled: true
      },
      {
        title: null,
        permissionCode: null,
        buttonComponent: null,
        disabled: true
      },
      {
        title: I18n.t('order_pended.return_pended'),
        permissionCode: PERMISSION_CODES.NEW_ORDER,
        style: {paddingRight: 7},
        buttonComponent: ProceedButton,
        onPress: (order) => {
          this.props.onReturnPendedOrder(order)
        }
      }
    ]
    return pendButtons
  }

  _onRenderButton = (order) => {
    const buttons = order.is_pended ? this._onRenderPendedButton() : this._onRenderOrderButton(order)
    const pendedOrderStyle = order.is_pended ? componentStyles.pendedOrderButton : null
    return (
      <View style={componentStyles.buttonContainer}>
        <View style={[componentStyles.container, pendedOrderStyle]}>
          {buttons.map((button, index) =>
            <View style={[componentStyles.orderDetailButtonContainer, button.disabled ? {opacity: 0} : {}]} key={index}>
              <OrderHistoryActionButton
                style={componentStyles.orderDetailButton}
                title={button.title}
                disabled={button.disabled}
                onPress={() => button.onPress(order)}
                buttonComponent={button.buttonComponent}
                permissionCode={this.props.repositoryContext !== REPOSITORY_CONTEXT.TRAINING ? button.permissionCode : null}
              />
            </View>)}
        </View>
      </View>
    )
  }

  render () {
    return (
      this._onRenderButton(this.props.selectedOrder)
    )
  }
}
