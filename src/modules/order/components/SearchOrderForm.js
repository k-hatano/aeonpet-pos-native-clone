'use strict'

import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import CheckBox from '../../../common/components/elements/CheckBox'
import Select from '../../../common/components/elements/Select'
import { CommandButton, ProceedButton } from '../../../common/components/elements/StandardButton'
import componentStyles from '../styles/SearchOrderForm'
import { STATUS, ORDER_TYPE, orderTypesToSearchCondition } from '../models'
import Decimal from 'decimal.js'
import { formatNumberZeroNotNull } from '../../../common/utils/formats'
import NumberBox from 'common/components/elements/NumberBox'
import DateInput from 'common/components/elements/DateInput'
import moment from 'moment'
import { toUnixTimestamp, getToday, getEndOfToday } from 'common/utils/dateTime'
import IconButton, { ICON_BUTTON_TYPE } from 'common/components/elements/IconButton'
import { CASH_AMOUNT_MAX_LENGTH } from 'common/models'
import CardNumberBox from 'common/components/elements/CardNumberBox'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'

function RowLayout (props) {
  return (
    <View style={[componentStyles.sampleRowContainer, props.style && props.style]}>
      <View style={componentStyles.sampleRowTitle}>
        {props.title && <Text style={componentStyles.commonText}>{props.title} : </Text>}
      </View>
      <View style={componentStyles.sampleRowTextContainer}>
        {props.children}
      </View>
    </View>
  )
}

const orderTypeOptions = [
  {
    labelKey: 'order.order',
    value: ORDER_TYPE.ORDER,
    selected: true
  },
  {
    labelKey: 'order_completed.return_order',
    value: ORDER_TYPE.RETURN
  },
  {
    labelKey: 'order_completed.cancel_order',
    value: ORDER_TYPE.CANCELED
  }
]

export default class SearchOrderForm extends Component {
  static propTypes = {
    onClear: PropTypes.func,
    onFind: PropTypes.func,
    onFindByPosOrderNumber: PropTypes.func,
    onInit: PropTypes.func,
    orders: PropTypes.array,
    paymentMethods: PropTypes.array,
    selectedOrder: PropTypes.object,
    isSearchOrder: PropTypes.bool
  }

  constructor (props) {
    super(props)
    const fromDate = getToday()
    const untilDate = getEndOfToday()
    this.state = {
      posOrderNumber: null,
      customerCode: null,
      productName: null,
      totalPaidFrom: null,
      totalPaidUntil: null,
      calendarFrom: fromDate,
      calendarUntil: untilDate,
      fromCompare: true,
      toCompare: true,
      paymentMethodArray: [],
      statusCheckBox: [...orderTypeOptions]
    }
  }

  async componentWillUnmount () {
    await this.props.onInit()
    this.eventBarcodeReader.remove()
  }

  async componentWillMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(body => {
      if (this.props.isSearchOrder) {
        this.setState({
          posOrderNumber: null
        })
        this.props.onFindByPosOrderNumber(body.trim())
      }
    })

    await this.props.onFindPaymentMethods()
    await this._onInitPaymentMethodSelects()
    this._handleSearch(true)
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.selectedOrder && this.props.selectedOrder.status !== STATUS.RETURNED && !nextProps.selectedOrder) {
      this._handleSearch(true)
    }
  }

  _onInitPaymentMethodSelects () {
    let paymentMethodArray = this.state.paymentMethodArray
    paymentMethodArray.push({ value: null })
    this.setState({ paymentMethodArray: paymentMethodArray })
  }

  _onRenderPaymentMethodSelects () {
    return (
      this.state.paymentMethodArray.map((payment, keyIndex) => (
        <View style={componentStyles.selectContainer} key={keyIndex}>
          <Select
            style={componentStyles.paymentMethod}
            selectedItem={payment.value}
            items={this.props.paymentMethods}
            displayProperty='name'
            onItemChange={paymentMethod => this._onChangePaymentValue(keyIndex, paymentMethod)}
            placeholder={I18n.t('common.select_one')}
          />
          {keyIndex !== 0 && <View style={componentStyles.buttonTouchableArea}>
            <IconButton
              icon={'minus'}
              type={ICON_BUTTON_TYPE.FONTAWESOME}
              style={componentStyles.button}
              onPress={() => { this._onMinusPaymentSelect(keyIndex) }} />
          </View>}
          {keyIndex < 5 && keyIndex === this.state.paymentMethodArray.length - 1 && <View style={componentStyles.buttonTouchableArea}>
            <IconButton
              icon={'plus'}
              type={ICON_BUTTON_TYPE.FONTAWESOME}
              style={componentStyles.button}
              onPress={() => { this._onPlusPaymentSelect() }} />
          </View>}
        </View>
      )
      ))
  }

  _onChangePaymentValue (key, value) {
    let paymentMethodArray = this.state.paymentMethodArray
    if (paymentMethodArray.length > key) {
      // 未選択の選択肢を選んだ場合の考慮を追加
      const paymentMethodValue = value.id ? value : null
      paymentMethodArray[key].value = paymentMethodValue
    }
    this.setState({ paymentMethodArray: paymentMethodArray })
  }

  _onPlusPaymentSelect () {
    let paymentMethodArray = this.state.paymentMethodArray
    paymentMethodArray.push({ value: null })
    this.setState({ paymentMethodArray: paymentMethodArray })
  }

  _onMinusPaymentSelect (key) {
    let paymentMethodArray = this.state.paymentMethodArray
    paymentMethodArray.splice(key, 1)
    this.setState({ paymentMethodArray: paymentMethodArray })
  }

  _getPaymentSelect () {
    let paymentMethods = []
    this.state.paymentMethodArray.map((payment, keyIndex) => {
      if (payment.value) {
        paymentMethods.push(payment.value.payment_method_type)
      }
    })

    if (paymentMethods.length > 0) {
      return paymentMethods
    } else {
      return null
    }
  }

  _orderTypeCheckBox (value) {
    let statusCheckBox = [...this.state.statusCheckBox]
    statusCheckBox[value] = {...statusCheckBox[value], selected: !statusCheckBox[value].selected}
    this.setState({statusCheckBox})
  }

  async _handleClear () {
    const fromDate = getToday()
    const untilDate = getEndOfToday()
    await this.setState({
      posOrderNumber: null,
      customerCode: null,
      productName: null,
      totalPaidFrom: null,
      totalPaidUntil: null,
      calendarFrom: fromDate,
      calendarUntil: untilDate,
      fromCompare: false,
      toCompare: false,
      paymentMethodArray: [],
      statusCheckBox: [...orderTypeOptions]
    })
    this._onInitPaymentMethodSelects()
  }

  async _setCondition () {
    const selectedOrderType = this.state.statusCheckBox
      .filter(checkBox => checkBox.selected)
      .map(checkBox => checkBox.value)
    const condition = orderTypesToSearchCondition(selectedOrderType)

    if (this.state.posOrderNumber) {
      condition['posOrder.pos_order_number'] = this.state.posOrderNumber
    }

    if (this.state.productName) {
      condition['orderItems.product_name'] = this.state.productName
    }

    const from = this.state.calendarFrom
    const until = this.state.calendarUntil

    condition['posOrder.client_created_at__gte'] = toUnixTimestamp(moment(from)._d)
    condition['posOrder.client_created_at__lte'] = toUnixTimestamp(moment(until)._d)
    if (moment(from)._d.getTime() > moment(until)._d.getTime()) {
      condition['posOrder.client_created_at__gte'] = toUnixTimestamp(moment(until)._d)
      condition['posOrder.client_created_at__lte'] = toUnixTimestamp(moment(from)._d)
      this.setState({
        calendarFrom: until,
        fromCompare: false,
        calendarUntil: from,
        toCompare: false
      })
    }

    const paymentMethods = this._getPaymentSelect()
    if (paymentMethods && paymentMethods.length > 0) {
      condition['orderPayments.payment_method_type'] = paymentMethods
    }

    const totalAmountFrom = this.state.totalPaidFrom === 0 || this.state.totalPaidFrom ? Decimal(this.state.totalPaidFrom) : null
    const totalAmountUntil = this.state.totalPaidUntil === 0 || this.state.totalPaidUntil ? Decimal(this.state.totalPaidUntil) : null

    if (totalAmountFrom && !isNaN(totalAmountFrom)) {
      condition.total_paid__gte = totalAmountFrom
    }

    if (totalAmountUntil && !isNaN(totalAmountUntil)) {
      condition.total_paid__lte = totalAmountUntil
    }

    if (totalAmountFrom && totalAmountUntil && (totalAmountFrom.comparedTo(totalAmountUntil) > 0)) {
      condition.total_paid__gte = totalAmountUntil
      condition.total_paid__lte = totalAmountFrom
      this.setState({ totalPaidFrom: totalAmountUntil })
      this.setState({ totalPaidUntil: totalAmountFrom })
    }
    return condition
  }

  async _handleSearch (init = false) {
    const condition = await this._setCondition()
    this.props.onFind(condition, this.state.customerCode, init)
  }

  _setFromDate (date) {
    this.setState({calendarFrom: date, fromCompare: true})
  }

  _setToDate (date) {
    this.setState({calendarUntil: date, toCompare: true})
  }

  render () {
    const statusCheckBox = this.state.statusCheckBox
    const disabled = !statusCheckBox.some(e => e.selected)
    return (
      <View style={componentStyles.container}>
        <ScrollView>
          <View style={componentStyles.innerContainer}>
            <View style={componentStyles.formSearchLayout}>
              <RowLayout style={{marginBottom: 50}} title={I18n.t('order.order_type')}>
                <View style={componentStyles.checkboxesContainer}>
                  {statusCheckBox.map((checkBox, index) => (
                    <CheckBox
                      key={`${index}-${checkBox.selected}`}
                      label={I18n.t(checkBox.labelKey)}
                      labelStyle={componentStyles.checkboxOptionText}
                      onChange={() => this._orderTypeCheckBox(index)}
                      mutableChecked={checkBox.selected} />
                  ))}
                </View>
              </RowLayout>
              <RowLayout title={I18n.t('order.order_code')}>
                <TextInput
                  style={componentStyles.searchInput}
                  onChangeText={(posOrderNumber) => this.setState({ posOrderNumber: posOrderNumber })}
                  value={this.state.posOrderNumber}
                  placeholder={I18n.t('order.order_code')} />
              </RowLayout>
              <RowLayout title={I18n.t('customer.customer_code')}>
                <TextInput
                  style={componentStyles.searchInput}
                  onChangeText={(customerCode) => this.setState({ customerCode: customerCode })}
                  value={this.state.customerCode}
                  placeholder={I18n.t('customer.customer_code')} />
              </RowLayout>
              <RowLayout title={I18n.t('product.product_name')}>
                <TextInput
                  style={componentStyles.searchInput}
                  onChangeText={(productName) => this.setState({ productName: productName })}
                  value={this.state.productName}
                  placeholder={I18n.t('product.product_name')} />
              </RowLayout>
              <RowLayout title={I18n.t('order.ordered_at')}>
                <View style={componentStyles.dateWrapper}>
                  <View style={componentStyles.inputContainer}>
                    <DateInput
                      compare={this.state.fromCompare}
                      fromDateTime={moment(this.state.calendarFrom)}
                      textInputStyle={componentStyles.searchInput}
                      useTimePicker={true}
                      onChangeDateTime={date => this._setFromDate(date)} />
                  </View>
                  <View style={componentStyles.dateTextWrapper}>
                    <Text style={componentStyles.commonText}>{I18n.t('common.from')}</Text>
                  </View>
                </View>
              </RowLayout>
              <RowLayout style={componentStyles.rowLayout}>
                <View style={componentStyles.dateWrapper}>
                  <View style={componentStyles.inputContainer}>
                    <DateInput
                      compare={this.state.toCompare}
                      toDateTime={moment(this.state.calendarUntil)}
                      textInputStyle={componentStyles.searchInput}
                      useTimePicker={true}
                      onChangeDateTime={date => this._setToDate(date)} />
                  </View>
                  <View style={componentStyles.dateTextWrapper}>
                    <Text style={componentStyles.commonText}>{I18n.t('common.until')}</Text>
                  </View>
                </View>
              </RowLayout>
              <RowLayout title={I18n.t('payment.payment_method')} style={componentStyles.paymentMethodWrapper}>
                {this._onRenderPaymentMethodSelects()}
              </RowLayout>
              <RowLayout title={I18n.t('order.total_amount')} style={componentStyles.inlineRowLayout}>
                <View style={componentStyles.dateWrapper}>
                  <NumberBox
                    style={componentStyles.searchInputTotal}
                    onChange={(amount) => this.setState({ totalPaidFrom: amount })}
                    amount={this.state.totalPaidFrom}
                    formatAmount={amount => (formatNumberZeroNotNull(amount))}
                    maxLength={CASH_AMOUNT_MAX_LENGTH}
                  />
                  <View style={componentStyles.dateTextWrapper}>
                    <Text style={componentStyles.commonText}>
                      {I18n.t('common.japanese_yen')} {I18n.t('common.from')}
                    </Text>
                  </View>
                </View>
              </RowLayout>
              <RowLayout style={componentStyles.inlineRowLayout}>
                <View style={componentStyles.dateWrapper}>
                  <NumberBox
                    style={componentStyles.searchInputTotal}
                    onChange={(amount) => this.setState({ totalPaidUntil: amount })}
                    amount={this.state.totalPaidUntil}
                    formatAmount={amount => (formatNumberZeroNotNull(amount))}
                    maxLength={CASH_AMOUNT_MAX_LENGTH}
                  />
                  <View style={componentStyles.dateTextWrapper}>
                    <Text style={componentStyles.commonText}>
                      {I18n.t('common.japanese_yen')} {I18n.t('common.until')}
                    </Text>
                  </View>
                </View>
              </RowLayout>
              <View style={componentStyles.btnClearWrapper}>
                <ProceedButton
                  style={componentStyles.btnClear}
                  onPress={() => this._handleClear()}
                  text={I18n.t('common.clear')} />
              </View>
            </View>
          </View>
          <View>
            <View style={componentStyles.btnSearchWrapper}>
              <CommandButton
                style={componentStyles.btnSearch}
                onPress={() => this._handleSearch()}
                text={I18n.t('common.search')}
                disabled={disabled} />
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}
