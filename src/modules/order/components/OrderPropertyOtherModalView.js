import React, { Component } from 'react'
import { View, Text } from 'react-native'
import styles from '../styles/OrderPropertyOtherModalView'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'
import { ProceedButton, WeakProceedButton } from '../../../common/components/elements/StandardButton'
import Cart from '../../cart/models/Cart'
import CartManager from '../../cart/models/CartManager'

export default class OrderPropertyOtherModalView extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart),
    setProcessed: PropTypes.func,
    progress: PropTypes.bool
  }

  manager = new CartManager()

  state = {
    showInterruptConfirm: false,
    showInterruptError: false
  }

  componentDidMount () {
    this.progress()
  }

  _setInterruptConfirm (bool) {
    this.setState({ showInterruptConfirm: bool })
  }

  _setInterruptError (bool) {
    this.setState({ showInterruptError: bool })
  }

  // 決済処理進行中
  progress () {
    if (!this.props.progress) {
      this.props.setProgress(true)
      this.manager.payAsync(this.props.cart).then(result => {
        this._close(result, !result.canContinue)
      })
    }
  }

  // 処理完了
  _close (result, isInterrupt = false) {
    if (this.props.progress) this.props.setProgress(false)
    this.props.onClose(result, isInterrupt)
  }

  // 処理を停止して終了
  async _interrupt () {
    try {
      await this.manager.payInterrupt()
      this._close({}, true)
    } catch (error) {
      this._setInterruptConfirm(false)
      this._setInterruptError(true)
    }
  }

  renderInterruptError () {
    return (
      <View style={styles.hover}>
        <View style={styles.hoverBg} />
        <View style={styles.hoverInner}>
          <View style={styles.hoverTextArea}>
            <Text style={styles.hoverText}>
              {I18n.t('message.C-99-I003')}
            </Text>
          </View>
          <View style={styles.hoverSubmitButtonArea}>
            <ProceedButton
              text={I18n.t('common.ok')}
              style={styles.hoverSubmitButton}
              onPress={() => this._setInterruptError(false)}
            />
          </View>
        </View>
      </View>
    )
  }

  renderInterruptConfirm () {
    return (
      <View style={styles.hover}>
        <View style={styles.hoverBg} />
        <View style={styles.hoverInner}>
          <View style={styles.hoverTextArea}>
            <Text style={styles.hoverText}>
              {I18n.t('message.C-99-I002')}
            </Text>
          </View>
          <View style={styles.hoverSubmitButtonArea}>
            <WeakProceedButton
              text={I18n.t('common.cancel')}
              style={[styles.hoverSubmitButton, styles.hoverSubmitButtonFirst]}
              onPress={() => this._setInterruptConfirm(false)}
            />
            <ProceedButton
              text={I18n.t('common.ok')}
              style={styles.hoverSubmitButton}
              onPress={() => this._interrupt()}
            />
          </View>
        </View>
      </View>
    )
  }

  render () {
    // 会計方法チェック
    let payment = null
    const paymentsObj = this.props.cart.payments.toJS()
    for (let i = 0, len = paymentsObj.length; i < len; i++) {
      if ((paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT ||
        paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY ||
        paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY) &&
        paymentsObj[i].amount.toNumber() !== 0) {
        payment = paymentsObj[i]
        break
      }
    }
    return (
      <View style={styles.layoutRoot}>
        {
          this.state.showInterruptError &&
          this.renderInterruptError()
        }
        {
          this.state.showInterruptConfirm &&
          this.renderInterruptConfirm()
        }
        <View style={styles.headerAreaPayment}>
          <Text style={styles.titleText}>
            {I18n.t('common.processing')}
          </Text>
        </View>
        <View style={styles.mainArea}>
          <View style={styles.textAreaPayment}>
            <Text style={styles.text}>
              {I18n.t('message.C-99-I001')}
            </Text>
          </View>
          <View style={styles.addTextArea}>
            <View style={styles.addTextAreaInner}>
              <Text style={styles.addText}>
                {I18n.t('cart.howto')}:
              </Text>
              <Text style={styles.addText}>
                {payment.name}
                {I18n.t('home.settlement')}
              </Text>
            </View>
          </View>
          <View style={styles.buttonArea}>
            <ProceedButton text={I18n.t('common.interrupt')} style={styles.submitButton} onPress={() => this._setInterruptConfirm(true)} />
          </View>
        </View>
      </View>
    )
  }
}
