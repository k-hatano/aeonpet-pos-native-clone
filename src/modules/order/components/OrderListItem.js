'use strict'

import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import componentStyles from '../styles/OrderListItem'
import { getOrderPaymentString } from '../../payment/models'
import { getTypeOfOrder, ORDER_TYPE } from '../models'
import moment from 'moment'

export default class OrderListItem extends Component {
  static propTypes = {
    item: PropTypes.object,
    onSelectItem: PropTypes.func
  }

  _getStatusIcon = (order) => {
    switch (getTypeOfOrder(order)) {
      case ORDER_TYPE.RETURN:
        return (
          <Image
            source={require('../../../assets/images/homepage/return_black.png')}
            style={componentStyles.iconOrder}
          />
        )
      case ORDER_TYPE.CANCELED:
        return (
          <Image
            source={require('../../../assets/images/homepage/Void.png')}
            style={componentStyles.iconOrder}
          />
        )
      default:
        return (
          <Image
            source={require('../../../assets/images/homepage/Accounting.png')}
            style={componentStyles.iconOrder}
          />
        )
    }
  }

  render () {
    const {
      item,
      onSelectItem
    } = this.props

    return (
      <TouchableOpacity
        style={componentStyles.container}
        onPress={() => onSelectItem(item)}
      >
        <View style={componentStyles.rowView}>
          <View style={componentStyles.orderStatus}>
            {this._getStatusIcon(item)}
          </View>
          <View style={componentStyles.dateContainer}>
            <Text style={componentStyles.rowTextDate}>
              {moment.unix(item.pos_order.client_created_at).format('YYYY-MM-DD HH:mm:ss')}
            </Text>
            <Text style={componentStyles.rowTextTitle}>
              {getOrderPaymentString(item.order_payments)}
            </Text>
          </View>
          <View style={componentStyles.rowTextWrapper}>
            <Text style={componentStyles.rowTextAmount}>
              {formatPriceWithCurrency(Math.abs(item.total_paid_taxed), item.currency)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
