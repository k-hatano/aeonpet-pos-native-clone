import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import CustomPropTypes from 'common/components/CustomPropTypes'
import styles from '../styles/OrderCompleteView'

export default class OrderCompleteActionButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    style: CustomPropTypes.Style,
    onPress: PropTypes.func,
    disabled: PropTypes.bool
  }

  render () {
    return (
      <TouchableOpacity
        onPress={() => this.props.onPress()}
        disabled={this.props.disabled}>
        <View style={this.props.disabled ? styles.processButtonDisabled : styles.processButton}>
          <View style={styles.processButtonTextContainer}>
            <Text style={styles.processButtonText}>
              {this.props.title}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}