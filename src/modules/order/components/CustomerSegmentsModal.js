import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import I18n from 'i18n-js'
import Modal from 'react-native-modalbox'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import styles from '../styles/CustomerSegmentsModal'

export default class CustomerSegmentsModal extends Component {
  constructor (props) {
    super(props)
    this.progressList = {}
    this.progressBarName = ''
    this.state = {
      groups: [
        {
          type: 'gender',
          name: I18n.t('order.customer_segment_gender'),
          selected: null,
          values: [
            {
              name: I18n.t('order.customer_segment_gender_male'),
              value: 'male'
            },
            {
              name: I18n.t('order.customer_segment_gender_female'),
              value: 'feMale'
            },
            {
              name: I18n.t('order.customer_segment_gender_unknown'),
              value: 'unknown'
            }
          ]
        },
        {
          type: 'age',
          name: I18n.t('order.customer_segment_age'),
          selected: null,
          values: [
            {
              name: `１０${I18n.t('order.customer_segment_customer_generation')}`,
              value: 10
            },
            {
              name: `２０${I18n.t('order.customer_segment_customer_generation')}`,
              value: 20
            }
          ]
        },
        {
          type: 'visitFrequency',
          name: I18n.t('order.customer_segment_visit_frequency'),
          selected: null,
          values: [
            {
              name: I18n.t('order.customer_segment_visit_new'),
              value: 1
            },
            {
              name: I18n.t('order.customer_segment_visit_repeater'),
              value: 2
            }
          ]
        }
      ]
    }
  }

  open () {
    this.refs.syncPopup && this.refs.syncPopup.open()
  }

  selectGroup (group, selectedItem) {
    const { groups } = this.state

    const selectedGroup = groups.map(item => {
      if (item.type === group.type) {
        item.selected = selectedItem.value
      }
      return item
    })
    this.setState({
      groups: selectedGroup
    })
  }

  renderItems (group, item, index) {
    const selected = item.value === group.selected
    return (
      <TouchableOpacity
        key={index}
        style={[styles.itemContainer, selected ? styles.selectedItem : {}]}
        onPress={() => this.selectGroup(group, item)}
      >
        <Text style={[styles.boldText, selected ? styles.itemSelectedText : styles.itemText]}>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  renderGroups (group, index) {
    return (
      <View key={index} style={styles.groupItem}>
        <View style={styles.groupItemHeader}>
          <Text style={styles.boldText}>{group.name}</Text>
        </View>
        <View style={styles.groupListItemContainer}>
          <ScrollView style={styles.groupScrollview} contentContainerStyle={styles.contentContainer}>
            {group.values && group.values.map((item, index) => this.renderItems(group, item, index))}
          </ScrollView>
        </View>
      </View>
    )
  }

  render () {
    const { groups } = this.state

    return (
      <Modal style={styles.modalPopup} ref={'syncPopup'}>
        <View style={styles.modalContainer}>
          <View style={styles.popupHeader}>
            <Icon name='account-box' size={40} />
            <Text style={styles.headerTitle}>{I18n.t('order.customer_segment_title')}</Text>
          </View>
          <View style={styles.groupContainer}>
            <ScrollView ref='syncList' style={styles.syncScrollViewContainer}>
              {groups && groups.map((group, index) => this.renderGroups(group, index))}
            </ScrollView>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttonPrinter}>
              <Text style={styles.buttonPrinterText}>{I18n.t('order.customer_segment_print_button')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}
