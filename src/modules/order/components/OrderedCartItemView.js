import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/OrderedCartItemText'
import Decimal from 'decimal.js'
// import baseStyle from 'common/styles/baseStyle'
import CartItemView from '../../cart/components/CartItemView'
import OrderedCartItem from '../models/OrderedCartItem'

export default class OrderedCartItemView extends CartItemView {
  static propTypes = {
    item: PropTypes.instanceOf(OrderedCartItem)
  }

  _renderQuantity () {
    /** @var {OrderedCartItem} **/
    const item = this.props.item
    return (
      <View style={styles.quantityTextLayout}>
        <Text style={styles.quantityText}>
          {I18n.t('cart.item_count_format', { count: Decimal(item.quantity).abs() })}
        </Text>
      </View>
    )
  }

  get _canAdditionalOperation () {
    return false
  }
}
