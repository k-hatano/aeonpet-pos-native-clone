import React, { Component } from 'react'
import { View, Text, NativeEventEmitter, NativeModules } from 'react-native'
import Decimal from 'decimal.js'
import styles from '../styles/OrderPropertyCashierModalView'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import { formatPriceWithCurrency } from 'common/utils/formats'
import { ProceedButton, WeakProceedButton } from 'common/components/elements/StandardButton'
import CashChanger from 'modules/printer/models/CashChanger'
import { DEPOSIT_TYPE } from 'modules/printer/models'

const printerBridgeEventEmitter = new NativeEventEmitter(NativeModules.PrinterBridge)

export default class OrderPropertyCashierModalViewItems extends Component {
  static propTypes = {
    cashPaymentAmount: PropTypes.number,
    currency: PropTypes.string,
    onError: PropTypes.func,
    onGetReadyCashChanger: PropTypes.func,
    isCashChangerReady: PropTypes.bool,
  }

  async complete (isCancel = false, newPaymentAmount) {
    if (!isCancel) {
      try {
        await CashChanger.checkCashierStatus()
      } catch (error) {
        CashChanger.final(DEPOSIT_TYPE.REPAY)
        this.props.onError(error)
        return
      }
    }

    if (this.getChange() > 0) {
      CashChanger.endDepositCashChanger(DEPOSIT_TYPE.CHANGE)
    } else {
      CashChanger.endDepositCashChanger(DEPOSIT_TYPE.NOCHANGE)
    }

    this.props.onClose(isCancel, newPaymentAmount)
  }

  cancel () {
    CashChanger.endDepositCashChanger(DEPOSIT_TYPE.REPAY)
    this.props.onClose(true)
  }

  getChange () {
    // お釣り計算
    const dpositAmount = new Decimal(this.props.cashPaymentAmount)
    const change = this.props.cashirDeposit.minus(dpositAmount).toNumber()
    return change < 0 ? 0 : change
  }

  async componentDidMount () {
    this.props.onUpdateDepositAmount(new Decimal(0))

    try {
      await CashChanger.init()

      printerBridgeEventEmitter.addListener('SendDepositAmountUpdate', (body) => {
        this.props.onUpdateDepositAmount(Decimal(body))
      })

      this.props.onGetReadyCashChanger()
    } catch (error) {
      CashChanger.final(DEPOSIT_TYPE.REPAY)
      this.props.onError(error)
    }
  }

  componentWillUnmount () {
    printerBridgeEventEmitter.removeAllListeners('SendDepositAmountUpdate')
  }

  render () {
    const payments = new Decimal(this.props.cashPaymentAmount)
    const currency = this.props.currency
    const cashirDeposit = this.props.cashirDeposit
    const checkShortageMoneyBool = payments.minus(cashirDeposit).toNumber() > 0
    // 不足金の項目で使用するcssを値段によって振り分け
    const shortageMoney = (() => {
      const mon = payments.minus(cashirDeposit)
      return {
        style1: mon.toNumber() > 0 ? styles.textWrapItemRed : styles.textWrapItem,
        style2: mon.toNumber() > 0 ? styles.textWrapItem2Red : styles.textWrapItem2,
        money: mon.toNumber() < 0 ? new Decimal(0) : mon
      }
    })()

    const cancel = () => this.cancel()
    const complete = () => this.complete(false, cashirDeposit)
    return (
      <View style={styles.itemRoot}>
        <View style={styles.parentTextWrap}>
          <View style={styles.submitCashirTextArea}>
            <View style={styles.textWrap}>
              <Text style={styles.textWrapItem}>
                {I18n.t('order.summary')}
              </Text>
              <Text style={styles.textWrapItem2}>
                {formatPriceWithCurrency(payments, currency)}
              </Text>
            </View>
            <View style={styles.textWrap}>
              <Text style={styles.textWrapItem}>
                {I18n.t('order.deposit')}
              </Text>
              <Text style={styles.textWrapItem2}>
                {formatPriceWithCurrency(cashirDeposit, currency)}
              </Text>
            </View>
            <View style={styles.textWrap}>
              <Text style={shortageMoney.style1}>
                {I18n.t('order.underpayment')}
              </Text>
              <Text style={shortageMoney.style2}>
                {formatPriceWithCurrency(shortageMoney.money, currency)}
              </Text>
            </View>
            <View style={styles.textWrap}>
              <Text style={styles.textWrapItem}>
                {I18n.t('order.change')}
              </Text>
              <Text style={styles.textWrapItem2}>
                {formatPriceWithCurrency(this.getChange(), currency)}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.itemWrapp}>
          <View style={styles.submitButtonCashirContainer}>
            <WeakProceedButton
              style={styles.submitCashirButton}
              onPress={cancel}
              text={I18n.t('common.cancel')}
              disabled={!this.props.isCashChangerReady}
            />
            <ProceedButton
              text={I18n.t('order.confirm')}
              style={styles.submitCashirButton}
              onPress={complete}
              disabled={checkShortageMoneyBool}
            />
          </View>
        </View>
      </View>
    )
  }
}
