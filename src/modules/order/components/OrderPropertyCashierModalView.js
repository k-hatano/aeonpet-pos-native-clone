import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OrderPropertyModalView'
import I18n from 'i18n-js'
import OrderPropertyCashierModalViewItems from './OrderPropertyCashierModalViewItems'
import { ProceedButton } from 'common/components/elements/StandardButton'

export default class OrderPropertyCashirModalView extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    onUpdateDepositAmount: PropTypes.func,
    cashPaymentAmount: PropTypes.number,
    currency: PropTypes.string,
    onError: PropTypes.func,
    cashierError: PropTypes.object,
    onGetReadyCashChanger: PropTypes.func,
    onClose: PropTypes.func,
    isCashChangerReady: PropTypes.bool
  }

  componentWillUnmount () {
    this.props.onEmdCashChanger()
  }

  renderError () {
    const errorMessage = this.props.cashierError.message

    const close = () => {
      this.props.onError(null)
      this.props.onClose(true)
    }
    return (
      <View style={styles.hover}>
        <View style={styles.hoverInner}>
          <View style={styles.hoverTitleArea}>
            <Text style={styles.titleTextCashir}>
              {I18n.t('order.deposit_cashir_error_title')}
            </Text>
          </View>
          <View style={styles.hoverTextArea}>
            <Text style={styles.hoverText}>
              {errorMessage}
            </Text>
          </View>
          <View style={styles.hoverSubmitButtonArea}>
            <ProceedButton
              text={I18n.t('common.ok')}
              style={styles.hoverSubmitButton}
              onPress={close}
            />
          </View>
        </View>
      </View>
    )
  }

  render () {
    const { onUpdateDepositAmount, cashirDeposit, cashPaymentAmount, currency, onClose, onError } = this.props
    return (
      <View style={[styles.layoutRoot, styles.layoutRootCashir]}>
        {
          this.props.cashierError &&
          this.renderError()
        }
        <View style={styles.headerAreaCashir}>
          <Text style={styles.titleTextCashir}>
            {I18n.t('order.deposit_cashir_title')}
          </Text>
        </View>
        <View style={styles.mainArea}>
          <OrderPropertyCashierModalViewItems
            onUpdateDepositAmount={onUpdateDepositAmount}
            cashirDeposit={cashirDeposit}
            cashPaymentAmount={cashPaymentAmount}
            currency={currency}
            onClose={onClose}
            onError={onError}
            onGetReadyCashChanger={this.props.onGetReadyCashChanger}
            isCashChangerReady={this.props.isCashChangerReady}
          />
        </View>
      </View>
    )
  }
}
