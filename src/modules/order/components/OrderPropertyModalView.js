import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OrderPropertyModalView'
import I18n from 'i18n-js'
import OrderPropertyModalViewItems from './OrderPropertyModalViewItems'
import Cart from '../../cart/models/Cart'

export default class OrderPropertyModalView extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    onSelectedOptions: PropTypes.func,
    orderProperty: PropTypes.array,
    selectedProperty: PropTypes.array,
    cart: React.PropTypes.instanceOf(Cart)
  }

  render () {
    const orderProperty = this.props.orderProperty
    const selectedProperties = this.props.selectedProperties
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.headerArea}>
          <Text style={styles.titleText}>{I18n.t('order.customer_segment_title')}</Text>
        </View>
        <View style={styles.mainArea}>
          <OrderPropertyModalViewItems
            onSelectedOptions={this.props.onSelectedOptions}
            orderProperty={orderProperty}
            selectedProperties={selectedProperties}
            cart={this.props.cart}
            onClose={this.props.onClose} />
        </View>
      </View>
    )
  }
}
