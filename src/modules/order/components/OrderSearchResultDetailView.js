'use strict'

import React, { Component } from 'react'
import { ListView, View } from 'react-native'
import PropTypes from 'prop-types'
import componentStyles from '../styles/OrderSearchResultDetailView'
import OrderedCart from '../models/OrderedCart'
import CartItemFormatter from '../../cart/models/CartItemFormatter'
import OrderedCartItemList from './OrderedCartItemList'

export default class OrderSearchResultDetailView extends Component {
  static propTypes = {
    selectedOrder: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  render () {
    const order = this.props.selectedOrder
    const cart = new OrderedCart(order)
    const items = (new CartItemFormatter()).format(cart)

    return (
      <View style={componentStyles.container}>
        <OrderedCartItemList
          items={items}
          currency={cart.currency} />
      </View>
    )
  }
}
