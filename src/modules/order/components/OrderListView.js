'use strict'

import React, { Component } from 'react'
import { ListView, View } from 'react-native'
import PropTypes from 'prop-types'
import OrderListItem from './OrderListItem'
import componentStyles from '../styles/OrderListView'

export default class OrderListView extends Component {
  static propTypes = {
    orders: PropTypes.array,
    selectedOrder: PropTypes.object,
    onSelect: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  _onSelect (item) {
    this.props.onSelect(item)
  }
  render () {
    let source = this.dataSource.cloneWithRows(this.props.orders)
    return (
      <View style={componentStyles.container}>
        <ListView
          enableEmptySections
          dataSource={source}
          renderSeparator={(sectionId, rowId) => <View
            key={`${sectionId}-${rowId}`}
            style={componentStyles.separator}
          />}
          renderRow={(item, index) => (
            <OrderListItem
              key={index}
              item={item}
              onSelectItem={(item) => this._onSelect(item)}
            />
          )}
        />
      </View>
    )
  }
}
