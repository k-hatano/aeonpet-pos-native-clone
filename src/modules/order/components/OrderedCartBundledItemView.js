import React from 'react'
import CartBundledItemView from '../../cart/components/CartBundledItemView'
import OrderedCartItemView from './OrderedCartItemView'

export default class OrderedCartBundledItemView extends CartBundledItemView {
  _renderChildItem (item) {
    return (
      <OrderedCartItemView
        item={item}
        onItemChanged={this.props.onItemChanged}
        canDiscount={false} />
    )
  }
}
