import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  itemRoot: {
    flex: 1,
    marginRight: 20,
    marginLeft: 20,
    alignItems: 'center'
  },
  parentTextWrap: {
    alignItems: 'center',
    width: '100%',
    paddingBottom: 50,
    marginBottom: 50,
    borderBottomWidth: 1,
    borderBottomColor: '#000'
  },

  itemWrapp: {
    alignItems: 'center'
  },
  submitCashirTextArea: {
    width: 576
  },
  submitButtonCashirContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  submitCashirButton: {
    width: 300,
    height: 64,
    marginBottom: 34,
    marginLeft: 50,
    marginRight: 50,
    fontSize: 36
  },
  textWrap: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  textWrapItem: {
    fontSize: 36
  },
  textWrapItemRed: {
    fontSize: 36,
    color: 'red'
  },
  textWrapItem2: {
    fontSize: 36,
    lineHeight: 36
  },
  textWrapItem2Red: {
    fontSize: 36,
    lineHeight: 36,
    color: 'red'
  }
})

export default styles
