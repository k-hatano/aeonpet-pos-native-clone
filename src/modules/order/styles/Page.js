import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    flex: 0,
    height: 72
  },
  searchOrderFormContainer: {
    zIndex: 2
  },
  listHeader: {
    paddingLeft: 10,
    paddingRight: 10,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9b9b9b'
  },
  headerText: {
    fontSize: 24,
    height: 24,
    fontFamily: 'HiraginoSans-W6',
    color: '#ffffff',
    letterSpacing: -0.38,
    fontWeight: '500'
  },
  orderList: {
    flex: 1
  },
  orderDetail: {
    flex: 1
  },
  orderDetailContainer: {
    height: 64,
    width: '100%',
    backgroundColor: '#95989a',
    flexDirection: 'row',
    paddingLeft: 16
  },
  backButtonContainer: {
    position: 'absolute',
    left: 16,
    top: 16,
    width: 32,
    height: 32,
    zIndex: 1
  },
  backButton: {
    width: 32,
    height: 32
  },
  headerContainer: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabContainer: {
    flex: 1
  },
  tabWrapperStyle: {
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  orderHistoryContainer: {
    height: 104,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: '#979797'
  }
})
