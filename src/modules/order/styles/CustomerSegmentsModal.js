import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  modalContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'column'
  },
  syncScrollViewContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'column'
  },
  popupHeader: {
    height: 80,
    width: '100%',
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerTitle: {
    fontSize: 22,
    color: 'black',
    fontWeight: '500',
    paddingLeft: 20
  },
  modalPopup: {
    width: '80%',
    height: '80%'
  },
  groupContainer: {
    flex: 1,
    width: '100%'
  },
  groupItem: {
    width: '100%',
    flex: 1,
    flexDirection: 'column'
  },
  groupItemHeader: {
    height: 30,
    width: '100%',
    backgroundColor: '#f0f0f0',
    paddingLeft: 10,
    justifyContent: 'center'
  },
  groupListItemContainer: {
    width: '100%',
    flex: 1
  },
  itemContainer: {
    width: 200,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f0f0f0',
    margin: 15,
    borderColor: '#dadada',
    borderWidth: 2
  },
  selectedItem: {
    backgroundColor: '#ff9024',
    borderColor: '#ff9024'
  },
  itemText: {
    color: 'black'
  },
  itemSelectedText: {
    color: '#fff'
  },
  buttonContainer: {
    width: '100%',
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#dddddd',
    borderTopWidth: 1
  },
  buttonPrinter: {
    width: 300,
    height: 60,
    backgroundColor: '#43bbb9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonPrinterText: {
    fontWeight: '500',
    fontSize: 20,
    color: '#fff'
  },
  boldText: {
    fontWeight: '500'
  },
  contentContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  groupScrollview: {
    marginHorizontal: 10
  }
})

export default styles
