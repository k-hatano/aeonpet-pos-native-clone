import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

const buttonBase = {
  width: 200,
  height: 86
}

export default StyleSheet.create({
  layoutRoot: {
    height: '100%'
  },
  titleArea: {
    flex: 148,
    justifyContent: 'center',
    alignItems: 'center'
  },
  amountArea: {
    flex: 280,
    backgroundColor: baseStyleValues.subBackgroundColor,
    alignItems: 'center'
  },
  amountInnerArea: {
    width: 576
  },
  amountLine1: {
    marginTop: 50,
    height: 35,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  amountLine2: {
    marginTop: 40,
    height: 35,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  amountLine3: {
    marginTop: 40,
    height: 35,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  nextArea: {
    flex: 175,
    justifyContent: 'center',
    alignItems: 'center'
  },
  separator: {
    height: 1,
    backgroundColor: baseStyleValues.separatorColor,
    borderWidth: 1,
    borderColor: '#979797'
  },
  buttonArea: {
    flex: 345
  },
  buttonAreaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 22,
    marginLeft: '6%',
    marginRight: '6%'
  },
  processButton: {
    ...buttonBase,
    borderWidth: 1,
    borderColor: '#979797',
    backgroundColor: '#f0f0f0'
  },
  processButtonDisabled: {
    ...buttonBase,
    borderWidth: 0,
    backgroundColor: baseStyleValues.disabledColor
  },
  buttonLayout: {
    ...buttonBase
  },
  processButtonTextContainer: {
    marginTop: 29
  },
  processButtonText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'center'
  },
  completeText: {
    fontSize: 36,
    color: baseStyleValues.mainTextColor
  },
  label1: {
    fontSize: 36,
    color: baseStyleValues.mainTextColor
  },
  label2: {
    fontSize: 36,
    color: baseStyleValues.mainTextColor
  },
  valueText1: {
    fontSize: 36,
    lineHeight: 36,
    color: baseStyleValues.mainTextColor,
    fontFamily: 'Courier'
  },
  valueText2: {
    fontSize: 36,
    lineHeight: 36,
    color: baseStyleValues.mainTextColor,
    fontFamily: 'Courier'
  },
  valueText3: {
    fontSize: 36,
    lineHeight: 36,
    color: baseStyleValues.mainTextColor,
    fontFamily: 'Courier'
  },
  nextButton: {
    fontSize: 36,
    width: 480,
    height: 64
  }
})
