import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    width: 500,
    height: 460,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:1, width: 1}
  },
  rowView: {
    height: 64,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8',
    marginHorizontal: 20
  },
  rowTextTime: {
    color: '#9b9b9b',
    fontFamily: 'Helvetica',
    fontSize: 18,
    letterSpacing: -0.29,
    paddingRight: 15,
    height: 22
  },
  TextTimeWraper: {
    width: 88
  },
  colTextItem: {
    width: 350,
    flexDirection: 'column'
  },
  colTextMainItem: {
    color: '#4a4a4a',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    letterSpacing: -0.29,
    textAlign: 'left',
    height: 22
  },
  colTextOtherItem: {
    color: '#9b9b9b',
    fontFamily: 'Helvetica',
    fontSize: 18,
    letterSpacing: -0.29,
    textAlign: 'right',
    height: 22
  },
  moreText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29,
    textAlign: 'center'
  }
})
