import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    paddingTop: 32,
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#f0f0f0'
  },
  headPicker: {
    width: 100,
    height: 30,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: '#DDDDDD'
  },
  headPickerText: {
    color: '#373737'
  },
  calendarView: {
    position: 'absolute',
    zIndex: 999
  },
  formSearchLayout: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  dateWrapper: {
    flexDirection: 'row',
    margin: 0,
    width: '100%'
  },
  dateTextWrapper: {
    flex: 2,
    marginLeft: 10,
    justifyContent: 'center'
  },
  btnClearWrapper: {
    alignItems: 'flex-end',
    padding: 10,
    marginBottom: 48
  },
  btnClear: {
    width: 144,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    fontSize: 24,
    fontWeight: 'bold'
  },
  btnSearchWrapper: {
    alignItems: 'center',
    padding: 10,
    paddingTop: 0
  },
  btnSearch: {
    height: 60,
    width: '80%',
    fontSize: 32,
    fontWeight: 'bold'
  },
  buttonText: {
    fontSize: 32,
    fontWeight: 'bold'
  },
  searchInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: '100%',
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  touchableContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  inputContainer: {
    height: 50,
    flex: 8
  },
  searchInputTotal: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: 250,
    fontSize: 24,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  commonText: {
    fontSize: 24,
    height: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  innerContainer: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 20
  },
  scrollView: {
    width: '100%',
    height: '100%'
  },
  sampleRowContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 22
  },
  sampleRowTextContainer: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sampleRowTitle: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  rowLayout30: {
    marginBottom: 30
  },
  rowLayout: {
    marginBottom: 20
  },
  selectContainer: {
    width: '100%',
    flexDirection: 'row'
  },
  select: {
    width: '100%',
    marginTop: 10
  },
  calendarContainer: {
    zIndex: 2,
    paddingHorizontal: 15
  },
  paymentMethodWrapper: {
    zIndex: 1000
  },
  paymentMethod: {
    width: '78%',
    marginTop: 5
  },
  inlineRowLayout: {
    marginBottom: 25
  },
  button: {
    width: 36,
    height: 36,
    color: 'white',
    fontSize: 26,
    backgroundColor: baseStyleValues.mainColor
  },
  buttonTouchableArea: {
    width: 42,
    height: 36,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 15
  },
  checkboxesContainer: {
    height: 24,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  checkboxOptionText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  }
})
