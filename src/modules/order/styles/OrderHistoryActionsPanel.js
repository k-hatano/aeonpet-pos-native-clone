import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    height: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingRight: 12,
    alignSelf: 'center'
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center'
  },
  orderDetailButtonContainer: {
    paddingVertical: 14,
    paddingLeft: 12
  },
  pendedOrderButton: {
    justifyContent: 'space-between'
  },
  orderDetailButton: {
    width: 114,
    height: 63,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold'
  }
})
