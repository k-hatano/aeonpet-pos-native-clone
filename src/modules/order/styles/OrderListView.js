import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    width: '100%',
    height: '90%',
    overflow: 'hidden'
  },
  separator: {
    flex: 1,
    height: 1,
    backgroundColor: '#d8d8d8',
    marginLeft: 24,
    marginRight: 24
  }
})
