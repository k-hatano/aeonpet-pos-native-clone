import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff'
  },
  separator: {
    flex: 1,
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
