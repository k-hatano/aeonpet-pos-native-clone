import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    marginLeft: 24,
    marginRight: 24
  },
  rowView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    width: 635,
    height: 64
  },
  dateContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  orderStatus: {
    width: 45,
    alignItems: 'center'
  },
  rowTextDate: {
    color: '#9b9b9b',
    fontFamily: 'Helvetica',
    fontSize: 18,
    letterSpacing: -0.29,
    paddingRight: 24,
    height: 22
  },
  rowTextTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29,
    height: 22,
    paddingTop: 2
  },
  rowTextAmount: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    height: 24,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    textAlign: 'right'
  },
  iconYenCoin: {
    width: 22,
    height: 22,
    marginRight: 14
  },
  iconOrder: {
    width: 20,
    height: 17,
    marginRight: 16
  },
  rowTextWrapper: {
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
