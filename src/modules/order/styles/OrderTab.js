import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderColor: '#979797',
    flexDirection: 'column'
  },
  header: {
    paddingTop: 20,
    paddingBottom: 20,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#979797',
    backgroundColor: '#fff'
  },
  headerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  userContent: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 0.5
  },
  userIcon: {
    marginRight: 0,
    width: 18,
    height: 21
  },
  circleCloseContent: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  circleCloseIcon: {
    marginRight: 0,
    width: 32,
    height: 32
  },
  headerText: {
    fontSize: 16,
    color: '#4a4a4a'
  },
  tabContainer: {
    flex: 1
  },
  subtotalContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  subtotalViewTop: {
    flex: 2 / 5,
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderColor: '#979797'
  },
  subtotalDetail: {
    flex: 3 / 5,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#d8d8d8',
    marginLeft: 20,
    marginRight: 10
  },
  subtotalScrollView: {
    width: '100%',
    height: '100%'
  },
  totalDetail: {
    flex: 2 / 5,
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 10
  },
  commonDetail: {
    paddingTop: 10,
    flexDirection: 'column'
  },
  leftDetail: {
    flex: 3 / 10
  },
  textDetailCommon: {
    fontSize: 20,
    fontFamily: 'Helvetica'
  },
  textDetailTitle: {
    fontSize: 18,
    fontFamily: 'HiraginoSans-W3'
  },
  textDetailDeposit: {
    fontSize: 24,
    fontFamily: 'HiraginoSans-W3'
  },
  textDetailPriceBold: {
    fontSize: 30,
    fontFamily: 'Helvetica-Bold'
  },
  paddingCommon: {
    paddingTop: 10
  },
  padding5: {
    paddingTop: 5
  },
  colorCommonRed: {
    color: '#ff0000'
  },
  middleDetail: {
    flex: 4 / 10,
    alignItems: 'flex-end',
    paddingRight: 15
  },
  rightDetail: {
    flex: 3 / 10
  },
  marginTop20: {
    marginTop: 20
  },
  discountButton: {
    marginTop: 10,
    padding: 5,
    height: 50
  },
  discountButtonText: {
    fontSize: 16,
    fontFamily: 'Helvetica'
  },
  subtotalViewBottom: {
    flex: 3 / 5,
    flexDirection: 'column'
  },
  paymentViewContainer: {
    flex: 4 / 5,
    flexDirection: 'row'
  },
  paymentView: {
    flex: 1,
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderColor: '#979797',
    paddingBottom: 5
  },
  submitView: {
    flex: 1 / 5
  },
  cashViewLeft: {
    backgroundColor: '#fff',
    flex: 3 / 5,
    flexDirection: 'column',
    marginLeft: 10,
    marginRight: 10
  },
  cashViewRight: {
    flex: 2 / 5,
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#979797',
    paddingBottom: 5
  },
  itemView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  borderItem: {
    borderBottomWidth: 1,
    borderColor: '#d8d8d8'
  },
  itemText: {
    fontSize: 18,
    fontFamily: 'HiraginoSans-W3',
    paddingLeft: 10,
    color: '#4a4a4a'
  },
  itemViewText: {
    flex: 3 / 10,
    alignItems: 'flex-start'
  },
  itemViewInput: {
    flex: 6 / 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  rowNumberInput: {
    borderWidth: 2,
    borderColor: '#ddd',
    width: '60%',
    height: 34,
    paddingRight: 10,
    paddingLeft: 10
  },
  pinItem: {
    flex: 1 / 10,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  pinIcon: {
    marginRight: 0,
    width: '20%',
    height: '40%'
  },
  checkoutButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 15,
    marginRight: 15,
    height: 60
  },
  checkoutButtonText: {
    fontSize: 30
  },
  tabPanelActiveBackgroundStyle: {
    borderBottomWidth: 6,
    borderColor: '#ff9024'
  },
  tabPanelActiveTextStyle: {
    color: '#000000',
    marginTop: 6
  },
  tabPanelTabStyle: {
    paddingLeft: 0,
    height: 80,
    flex: 1,
    backgroundColor: '#ffffff'
  },
  tabPanelTabWrapperStyle: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 67,
    marginBottom: 13
  },
  tabContentWrapper: {
    backgroundColor: 'transparent',
    flex: 1
  },
  particularTabStyle: {
    height: 67,
    backgroundColor: 'transparent'
  },
  particularTabNameStyle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerName: {
    flex: 1.5
  }
})

export default styles
