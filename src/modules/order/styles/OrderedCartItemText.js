import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  quantityTextLayout: {
    width: 80,
    height: 24,
    alignItems: 'flex-end'
  },
  quantityText: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    fontWeight: 'bold'
  }
})
