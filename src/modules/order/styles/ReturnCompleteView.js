import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: '100%'
  },
  titleArea: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  amountArea: {
    flex: 6,
    backgroundColor: baseStyleValues.subBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center'
  },
  amountInnerArea: {
    width: 576
  },
  amountLine: {
    height: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  nextArea: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonArea: {
    flex: 345
  },
  completeText: {
    fontSize: 36,
    color: baseStyleValues.mainTextColor
  },
  label: {
    fontSize: 30,
    color: baseStyleValues.mainTextColor
  },
  valueText: {
    fontSize: 48,
    color: baseStyleValues.mainTextColor,
    fontWeight: 'bold'
  },
  nextButton: {
    fontSize: 36,
    width: 480,
    height: 64
  }
})
