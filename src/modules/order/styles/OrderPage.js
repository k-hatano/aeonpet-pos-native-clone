import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  pageLayout: {
    backgroundColor: '#fff'
  },
  cartView: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    flex: 1,
    width: undefined
  },
  cartTotal: {
    width: undefined,
    height: 100,
    backgroundColor: '#fff',
    margin: 10
  },
  cartTotalLeftPanel: {
    flex: 1
  },
  cartTotalRightPanel: {
    flex: 1,
    alignItems: 'flex-end'
  },
  textbox: {
    flex: 1,
    fontSize: 20
  },
  textboxTotal: {
    fontWeight: '500',
    fontSize: 20
  },
  checkoutButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9024',
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  }
})
