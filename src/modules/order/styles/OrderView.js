import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff'
  },
  summaryContainer: {
    flexDirection: 'column',
    paddingLeft: 24,
    paddingRight: 24
  },
  summaryOrderDate: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderColor: '#d8d8d8',
    borderBottomWidth: 1,
    width: 635,
    height: 48
  },
  summaryTitleWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  summaryTitleText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    height: 20
  },
  summaryValueWrapper: {
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  summaryValueText: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    height: 24
  },
  summaryExtraValueText: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32,
    marginRight: 20,
    height: 24
  },
  childRowIcon: {
    width: 12,
    height: 11,
    marginLeft: 15,
    marginRight: 9
  }
})
