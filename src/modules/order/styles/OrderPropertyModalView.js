import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  layoutRoot: {
    width: 1077,
    height: 782,
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: { height: 1, width: 1 }
  },
  layoutRootCashir: {
    height: 600
  },
  headerArea: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingBottom: 13,
    flex: 1
  },
  headerAreaCashir: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flex: 1,
    marginBottom: 60
  },
  titleText: {
    flex: 1,
    fontSize: 24,
    fontWeight: 'bold',
    color: '#4a4a4a',
    alignSelf: 'flex-end',
    marginLeft: 42,
    paddingBottom: 40
  },
  titleTextCashir: {
    flex: 1,
    fontSize: 36,
    fontWeight: 'bold',
    color: '#4a4a4a',
    alignSelf: 'flex-end',
    textAlign: 'center'
  },
  mainArea: {
    flex: 5
  },
  footerArea: {
    flex: 1
  },
  submitCashirTextArea: {
    width: '100%'
  },
  submitCashirText: {
    fontSize: 36,
    textAlign: 'center'
  },
  submitButtonContainer: {
    alignSelf: 'center'
  },
  submitButtonCashirContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  submitButton: {
    width: 480,
    height: 64,
    marginBottom: 34,
    fontSize: 36
  },
  submitCashirButton: {
    width: 300,
    height: 64,
    marginBottom: 34,
    marginLeft: 50,
    marginRight: 50,
    fontSize: 36
  },
  itemRoot: {
    flex: 1,
    marginRight: 20,
    marginLeft: 20
  },
  itemTitleContainer: {
    height: 45,
    backgroundColor: '#f0f0f0',
    marginBottom: 24,
    justifyContent: 'center'
  },
  itemTitleText: {
    fontSize: 24,
    fontFamily: 'HiraginoSans-W3',
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left',
    height: 24
  },
  itemButtonsArea: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  itemButtonContainer: {
    width: '25%',
    height: 68,
    alignItems: 'center'
  },
  itemButton: {
    height: 54,
    width: 240
  },
  hover: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(128, 128, 128, 0.5)',
    zIndex: 2
  },
  hoverInner: {
    width: '80%',
    height: '80%',
    alignItems: 'center',
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: { height: 1, width: 1 }
  },
  hoverTitleArea: {
    flex: 1,
    marginTop: 50
  },
  hoverTextArea: {
    flex: 1,
    alignItems: 'center'
  },
  hoverText: {
    marginBottom: 10,
    fontSize: 26,
    lineHeight: 30,
    textAlign: 'center'
  },
  hoverSubmitButtonArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  hoverSubmitButton: {
    width: 220,
    height: 40,
    marginBottom: 34,
    fontSize: 30
  }
})

export default styles
