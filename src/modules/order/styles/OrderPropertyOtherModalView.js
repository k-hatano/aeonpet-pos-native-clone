import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  layoutRoot: {
    position: 'relative',
    width: 800,
    height: 340,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: { height: 1, width: 1 }
  },
  headerArea: {
    height: 70,
    flex: 1,
    alignSelf: 'center',
    marginTop: 90,
    marginBottom: 40
  },
  headerAreaPayment: {
    height: 70,
    flex: 1,
    alignSelf: 'center',
    marginTop: 50
  },
  titleText: {
    flex: 1,
    fontSize: 28,
    fontWeight: 'bold',
    color: '#4a4a4a'
  },
  mainArea: {
    flex: 4
  },
  textArea: {
    marginBottom: 60
  },
  textAreaPayment: {
    marginBottom: 40
  },
  text: {
    fontSize: 22,
    textAlign: 'center'
  },
  addTextArea: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60
  },
  addTextAreaInner: {
    width: '60%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  addText: {
    fontSize: 22
  },
  buttonArea: {
    width: '100%',
    paddingTop: 20,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#4a4a4a'
  },
  submitButton: {
    width: 480,
    height: 40,
    marginBottom: 34,
    fontSize: 30
  },
  hover: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(128, 128, 128, 0.5)',
    zIndex: 2
  },
  hoverInner: {
    width: '80%',
    height: '80%',
    alignItems: 'center',
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: { height: 1, width: 1 }
  },
  hoverTextArea: {
    marginTop: 70,
    marginBottom: 50,
    alignItems: 'center'
  },
  hoverText: {
    marginBottom: 10,
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center'
  },
  hoverSubmitButtonArea: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  hoverSubmitButton: {
    width: 220,
    height: 40,
    marginBottom: 34,
    fontSize: 30
  },
  hoverSubmitCancelButton: {
    width: 220,
    height: 40,
    fontSize: 30,
    backgroundColor: 'white',
    borderColor: '#44bcb9',
    color: '#44bcb9',
    borderWidth: 3
  },
  hoverSubmitButtonFirst: {
    marginRight: 100
  }
})
/*
  hoverSubmitCancelButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: '#44bcb9',
    borderWidth: 3
  }, */
export default styles
