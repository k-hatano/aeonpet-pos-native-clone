import { NativeAppEventEmitter, NativeModules, Platform } from 'react-native'
import { isDevMode } from '../../common/utils/environment'
import { setDebugModal } from '../debug/actions'

const managers = {}

let managerCount = 0;

class DebugBarcodeReaderManager {
  _nativeEvent
  _callback
  _id

  constructor (nativeEvent, callback) {
    this._nativeEvent = nativeEvent
    this._id = managerCount++
    this._callback = callback
    managers[this._id] = this
  }

  input (text) {
    this._callback(text)
  }

  remove () {
    this._nativeEvent.remove()
    delete managers[this._id]
    this._callback = null
  }
}

export function registerBarcodeReaderEvent (callback) {
  const event = NativeAppEventEmitter.addListener('barcode', callback)

  if (Platform.OS === 'ios') {
    NativeModules.BarcodeReaderBridge.setBarcodeReaderProperty({
      BARCODE_READER_SDK: 0,
      BARCODE_READER_STD: 1,
      BARCODE_READER_IP: 'empty'
    })
  }

  return isDevMode() ? new DebugBarcodeReaderManager(event, callback) : event
}

export function showDebugBarcodeInput (dispatch) {
  dispatch(setDebugModal({
    title: 'バーコード手動入力',
    options: [
      {
        type: 'text',
        key: 'barcode',
        title: 'バーコード'
      }
    ],
    commands: [
      {
        label: 'Cancel',
        onPress: () => {
          dispatch(setDebugModal(null))
        }
      },
      {
        label: 'OK',
        onPress: (input) => {
          for (const key of Object.keys(managers)) {
            const manager = managers[key]
            manager.input(input.barcode)
          }
          dispatch(setDebugModal(null))
        }
      },
    ]
  }))
}
