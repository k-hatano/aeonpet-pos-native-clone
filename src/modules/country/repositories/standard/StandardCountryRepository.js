import fetcher from 'common/models/fetcher'
import { handleAxiosError } from '../../../../common/errors'
import CountriesEntity from 'modules/geo/repositories/entities/CountriesEntity'

export default class StandardCountryRepository {
  async fetchAll () {
    try {
      const response = await fetcher.get('countries')
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async findAll () {
    return CountriesEntity.findAll({
      order: [
        ['country_code', 'ASC']
      ],
      raw: true
    })
  }

  async findByCountryCode (code) {
    const result = await CountriesEntity.find({
      where: {
        country_code: code
      }
    })
    const country = result.dataValues
    if (country) {
      return {
        label: country.country_code,
        value: country.id
      }
    } else {
      return null
    }
  }
}
