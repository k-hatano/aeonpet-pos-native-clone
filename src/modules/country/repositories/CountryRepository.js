import SampleCountryRepository from './sample/SampleCountryRepository'
import StandardCountryRepository from './standard/StandardCountryRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CountryRepository {
  static _implement = new SampleCountryRepository()

  static async fetchAll () {
    return this._implement.fetchAll()
  }

  static async findAll () {
    return this._implement.findAll()
  }

  static async findByCountryCode (code) {
    return this._implement.findByCountryCode(code)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCountryRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardCountryRepository()
        break

      default:
        this._implement = new SampleCountryRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CountryRepository', context => {
  CountryRepository.switchImplement(context)
})
