export default class SampleCountryRepository {
  async fetchAll () {
    return [
      {
        id: 'A0000000000000000000000000000001',
        country_code: 'JPN'
      },
      {
        id: 'A0000000000000000000000000000002',
        country_code: 'USA'
      },
      {
        id: 'A0000000000000000000000000000003',
        country_code: 'GBR'
      }
    ]
  }

  async findAll () {
    return [
      {
        id: 'A0000000000000000000000000000001',
        country_code: 'JPN'
      },
      {
        id: 'A0000000000000000000000000000002',
        country_code: 'USA'
      },
      {
        id: 'A0000000000000000000000000000003',
        country_code: 'GBR'
      }
    ]
  }

  async findByCountryCode (code) {
    return {
      id: 'A0000000000000000000000000000002',
      country_code: 'USA'
    }
  }
}
