import { connect } from 'react-redux'
import I18n from 'i18n-js'
import CartCustomerView from '../components/CartCustomerView'
import { MODULE_NAME } from '../models'
import { setCartCustomer } from '../actions'
import { useCardNumber } from 'modules/customer/actions'
import AlertView from 'common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  onSelectCustomer: async (customer) => {
    dispatch(setCartCustomer(customer))
  },
  onClearCustomer: async () => {
    dispatch(setCartCustomer(null))
  },
  onShowSelectCustomerModal: async (useCardNumberFlg) => {
    dispatch(useCardNumber(useCardNumberFlg))
  }
})

const mapStateToProps = state => ({
  customer: state[MODULE_NAME].cart.customer,
  disabled: !state[MODULE_NAME].cart.canSetCustomer
})

export default connect(mapStateToProps, mapDispatchToProps)(CartCustomerView)
