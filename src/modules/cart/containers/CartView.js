import CartView from '../components/CartView'
import { connect } from 'react-redux'
import { MODULE_NAME, getCustomerName } from '../models'
import { initilizeCart, proceedUpdateCart } from '../services'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'
import CartDataConverter from '../models/CartDataConverter'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import SettingGetter from 'modules/setting/models/settingGetter'
import { listPendedOrders } from '../../home/actions'

const mapDispatchToProps = dispatch => ({
  onPendOrder: async cart => {
    if (cart.items.size === 0) {
      AlertView.show(I18n.t('message.B-02-E001'))
      return false
    } else {
      cart = cart.completePend(SettingGetter.currentStaff, new Date())
      const converter = new CartDataConverter(cart)
      const order = converter.convert()
      if (cart.customer) {
        order.customer_name = getCustomerName(cart.customer)
      }
      if (await OrderPendRepository.findById(order.id)) {
        await OrderPendRepository.updateById(order)
      } else {
        const pendedOrders = await OrderPendRepository.findAll()
        if (pendedOrders && pendedOrders.length >= 20) {
          await AlertView.showAsync(I18n.t('message.B-02-E002'))
          return false
        } else {
          await OrderPendRepository.save(order)
        }
      }
      await AlertView.showAsync(I18n.t('message.B-02-I001'))
      await initilizeCart('order', null, dispatch)
      const pendedOrders = await OrderPendRepository.findAll()
      dispatch(listPendedOrders(pendedOrders))
      return true
    }
  }
})

const mapStateToProps = state => ({
  cart: state[MODULE_NAME].cart
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onPendOrder: async (...args) => {
      if (await dispatchProps.onPendOrder(...args)) {
        await ownProps.onPendOrder(...args)
      }
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CartView)
