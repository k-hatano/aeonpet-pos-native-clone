import { connect } from 'react-redux'
import I18n from 'i18n-js'
import AlertView from 'common/components/widgets/AlertView'
import logger from 'common/utils/logger'
import DiscountOptionsView from 'modules/promotion/components/DiscountOptionsView'
import PromotionPresetRepository from 'modules/promotion/repositories/PromotionPresetRepository'
import { PROMOTION_PRESET_TYPE } from 'modules/promotion/models'
import { MODULE_NAME } from '../models'
import { listCartPromotionSet } from '../actions'
import { DISCOUNT_TARGET_TYPE } from '../../promotion/models'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    try {
      const presets = await PromotionPresetRepository.findByPromotionPresetType(PROMOTION_PRESET_TYPE.SUBTOTAL)
      dispatch(listCartPromotionSet(presets))
    } catch (error) {
      logger.error('Cannot find promotion_presets for cart')
      AlertView.show(I18n.t('errors.unexpected_error'))
    }
  }
})

const mapStateToProps = state => {
  return {
    promotionPresets: state[MODULE_NAME].cartPromotionPresets,
    discountTargetType: DISCOUNT_TARGET_TYPE.SUBTOTAL
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DiscountOptionsView)
