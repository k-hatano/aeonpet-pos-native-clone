import configureMockStore from 'redux-mock-store'
import cartReducer from '../cartReducer'
import Cart from '../../models/Cart'
import { MODULE_NAME } from '../../models'
import { updateCart } from '../../actions'

const middlewares = []
const mockStore = configureMockStore(middlewares)
const initialState = { cart: { cartHistory: [] } }
const store = mockStore(initialState)

describe('[Cart] Cart Reducer Test', () => {
  it('Should dispatch updateCart action', () => {
    const cart = new Cart()
    store.dispatch(updateCart(cart))
    const expectedPayload = {
      payload: cart,
      type: `${MODULE_NAME}_updateCart`
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle updateCart', () => {
    const oldState = {
      cart: new Cart(),
      cartHistory: []
    }
    const newCart = new Cart()
    const newState = cartReducer(oldState, updateCart(newCart))
    expect(newState.cart).toEqual(newCart)
  })
})
