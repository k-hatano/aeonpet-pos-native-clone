import { handleActions } from 'redux-actions'
import * as actions from '../actions'
import Cart from '../models/Cart'

const defaultState = {
  cart: new Cart(),
  cartPromotionPresets: [],
  cartItemPromotionPresets: [],
  cartHistory: []
}

const handlers = {
  [actions.updateCart]: (state, action) => ({
    ...state,
    cart: action.payload,
    cartHistory: [...state.cartHistory, action.payload].splice(-5)
  }),
  [actions.setCartCustomer]: (state, action) => {
    /** @type {Cart} */
    const cart = state.cart
    return {
      ...state,
      cart: cart.setCustomer(action.payload)
    }
  },
  [actions.listCartPromotionSet]: (state, action) => ({
    ...state,
    ...{ cartPromotionPresets: action.payload }
  }),
  [actions.listCartItemPromotionSet]: (state, action) => ({
    ...state,
    ...{ cartItemPromotionPresets: action.payload }
  })
}

export default handleActions(handlers, defaultState)
