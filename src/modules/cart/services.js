import I18n from 'i18n-js'
import { makeBaseEJournal, RECEIPT_TYPE } from '../printer/models'
import CartManager from './models/CartManager'
import AlertView from '../../common/components/widgets/AlertView'
import { updateCart } from './actions'
import { setCustomerDisplayText } from '../customerDisplay/actions'
import * as productActions from 'modules/product/actions'
import * as cartActions from 'modules/cart/actions'
import Cart from 'modules/cart/models/Cart'
import CategoryRepository from 'modules/product/repositories/CategoryRepository'
import { loading } from 'common/sideEffects'
import { PRINT_TITLE } from '../order/models'
import { PREV_SEARCH_COND } from '../product/models'

/**
 * @function makeOrderEJournal
 * @param receipt
 * @param {Cart} cart
 */
export const makeOrderEJournal = (receipt, cart, id = undefined, isForShop = false) => {
  const receiptType = cart.isReturn 
                    ? isForShop ? RECEIPT_TYPE.RETURN_FOR_SHOP : RECEIPT_TYPE.RETURN_ORDER
                    : RECEIPT_TYPE.ORDER
  const receiptTitle = cart.isReturn ? I18n.t(PRINT_TITLE.RETURN) : I18n.t(PRINT_TITLE.ORDER)
  const ejournal = makeBaseEJournal(
    receiptTitle,
    receipt,
    receiptType,
    cart.createdAt)
  ejournal.currency = cart.currency
  ejournal.amount = cart.totalTaxed.toFixed(4)
  ejournal.order_id = cart.id
  ejournal.pos_order_number = cart.posOrderNumber
  ejournal.staff_id = cart.staff.id
  ejournal.is_print_tax_stamp = cart.needsTaxStamp ? 1 : 0
  if (id) {
    ejournal.id = id
  }
  return ejournal
}

export const initilizeCart = async (mode, customer, dispatch) => {
  const messages = []
  await loading(dispatch, async () => {
    // 前回の表示を消す目的でひとまず空のカートを入れる
    const initialCart = new Cart()
    dispatch(cartActions.updateCart(initialCart))

    const manager = new CartManager()
    let cart = await manager.createInitialCartAsync(mode)
    await proceedUpdateCart(initialCart, cart, dispatch)
    dispatch(cartActions.setCartCustomer(customer))
  })
  for (const message of messages) {
    await AlertView.showAsync(message)
  }
}

export const restorePendedCart = async (pendedOrder, dispatch) => {
  let cart, messages
  await loading(dispatch, async () => {
    const manager = new CartManager()
    ;({ cart, messages } = await manager.initializeCartWithPendedOrder(pendedOrder))
  })
  for (const i in messages) {
    await AlertView.showAsync(messages[i])
  }
  dispatch(cartActions.updateCart(cart))
  await proceedUpdateCart(new Cart(), cart, dispatch)
}

export const proceedUpdateCart = async (oldCart, newCart, dispatch, updateMessage = true) => {
  const manager = new CartManager()
  const { isValid, messages } = manager.checkUpdatedCart(oldCart, newCart)
  if (messages.length > 0) {
    await AlertView.showAsync(messages.join('\n'))
  }
  if (!updateMessage) {
    return
  }
  const message = manager.getCustomerDisplayMessage(oldCart, newCart)
  if (message) {
    dispatch(setCustomerDisplayText(message))
  }
  if (isValid) {
    dispatch(updateCart(newCart))
  }
}

export const initForCategoryProductView = async (dispatch, stockConditionFlag = true) => {
  const categories = await CategoryRepository.findByParentId(null)
  dispatch(productActions.listCategories(categories))
  dispatch(productActions.listProducts([]))
  dispatch(productActions.listOriginProducts([]))
  dispatch(productActions.setSearchWord(''))
  dispatch(productActions.setClosureCond(''))
  dispatch(productActions.setVintageCond(''))
  dispatch(productActions.setLabelCond(''))
  dispatch(productActions.setBoxCond(''))
  dispatch(productActions.setPrevCategory({}))
  dispatch(productActions.setClosureData([]))
  dispatch(productActions.setLabelData([]))
  dispatch(productActions.setBoxData([]))
  dispatch(productActions.setVintageData([]))
  dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
  dispatch(productActions.setSearchLayoutFlg(false))
  dispatch(productActions.setStockConditionFlg(stockConditionFlag))
}
