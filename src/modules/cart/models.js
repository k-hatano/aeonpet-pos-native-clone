import I18n from 'i18n-js'
import DiscountReasonRepository from 'modules/promotion/repositories/DiscountReasonRepository'

export const MODULE_NAME = 'cart'

/**
 * 一度の会計で利用可能な最大金額
 * @type {number}
 */
export const MAX_SUBTOTAL_AMOUNT = 99999999

/**
 * 一度の会計で購入可能な最大点数
 * @type {number}
 */
export const MAX_CART_ITEM_QUANTITY = 999

/**
 * 免税会系の際、一般品の合計がこの金額を超えるとアラートを出す
 * @type {number}
 */
export const ALERT_THRESHOLD_FOR_TAXFREE_GENERAL = 1000000

/**
 * 免税適用の基準金額。この金額以上であれば免税適用する
 */
export const TAXFREE_THRESHOLD = 5000

/**
 * 一度の会計で使用可能な支払い方法の最大数
 */
export const MAX_ACTIVE_PAYMENT = 4

export const TAXFREE_MAX_THRESHOLD_FOR_EXPENDABLE = 500000
export const MAX_PAYMENT_AMOUNT = 99999999

/**
 * ポイント付与対象外フラグ
 */
export const GRANT_POINT = {
  CAN: 1,
  CANNOT: 2
}

export const getCustomerName = customer => {
  return customer.last_name_kana + ' ' + customer.first_name_kana
}

export const getDiscountReasonName = async discountReasonId => {
  const discountReason = await DiscountReasonRepository.findById(discountReasonId)
  return discountReason ? discountReason.name : ''
}
