import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartSelectProductFooter'
import baseStyle from 'common/styles/baseStyle'
import Cart from '../models/Cart'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import { ProceedButton } from '../../../common/components/elements/StandardButton'

export default class CartSelectProductFooter extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart),
    onSubtotal: PropTypes.func
  }

  render () {
    /** @type Cart */
    const cart = this.props.cart

    return (
      <View style={styles.root}>
        <View style={styles.separator} />
        <View style={styles.contentArea}>
          <Text style={[baseStyle.strongTextStyle, styles.summaryText]}>
            {I18n.t('cart.item_count_format', {count: cart.totalQuantity})}
            {'　／　'}
            {formatPriceWithCurrency(cart.totalItemsBase, cart.currency)}
          </Text>
          <ProceedButton
            onPress={this.props.onSubtotal}
            style={styles.subtotalButton}
            text={I18n.t('cart.subtotal')} />
        </View>
      </View>
    )
  }
}
