import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import Decimal from 'decimal.js'
import styles from '../styles/CartTotalView'
// import baseStyle from 'common/styles/baseStyle'
import Cart from '../models/Cart'
import { formatPriceWithCurrency } from '../../../common/utils/formats'

class DepositAmountLine extends Component {
  static propTypes = {
    titleKey: PropTypes.string,
    amount: PropTypes.instanceOf(Decimal),
    currency: PropTypes.string
  }

  render () {
    return (
      <View style={styles.depositAmountLine} >
        <Text style={styles.depositAmountTitle}>
          {I18n.t(this.props.titleKey)}{' : '}
        </Text>
        <Text style={styles.depositAmountText}>
          {formatPriceWithCurrency(this.props.amount, this.props.currency)}
        </Text>
      </View>
    )
  }
}

export default class CartTotalView extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart)
  }

  render () {
    /** @type Cart */
    const cart = this.props.cart

    const isRemaining = cart.remainDeposit.gt(0)

    return (
      <View style={styles.layoutRoot}>
        <View style={styles.totalArea}>
          <Text style={styles.amountTitleText}>{I18n.t('cart.total')}</Text>
          <Text style={styles.amountText}>
            {formatPriceWithCurrency(cart.totalTaxed, cart.currency)}
          </Text>
          <Text style={styles.amountSubInfoText}>
            {I18n.t('cart.item_count_format', {count: cart.totalQuantity})}
          </Text>
        </View>
        <View style={styles.depositArea}>
          <DepositAmountLine
            titleKey={cart.isReturn ? 'cart.return' : 'cart.deposit'}
            amount={cart.deposit}
            currency={cart.currency} />
          {cart.isReturn ? null : (
            <DepositAmountLine
              titleKey={isRemaining ? 'cart.remaining' : 'cart.change'}
              amount={isRemaining ? cart.remainDeposit : cart.change}
              currency={cart.currency} />
          )}
        </View>
      </View>
    )
  }
}
