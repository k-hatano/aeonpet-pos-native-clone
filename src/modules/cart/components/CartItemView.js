import React, { Component, PureComponent } from 'react'
import { Text, View, Image } from 'react-native'
import I18n from 'i18n-js'
import { formatPriceWithCurrency } from 'common/utils/formats'
import CartItem from '../models/CartItem'
import styles from '../styles/CartItemView'
import Label from 'common/components/elements/Label'
import baseStyle from 'common/styles/baseStyle'
import CartInputNumberForm from './CartInputNumberForm'
import PropTypes from 'prop-types'
import { List } from 'immutable'
import OpenCloseButton from 'common/components/widgets/OpenCloseButton'
import CartItemDiscountOptionsView from '../containers/CartItemDiscountOptionsView'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import Modal from '../../../common/components/widgets/Modal'
import CartItemDiscount from '../models/CartItemDiscount'
import { MAX_CART_ITEM_QUANTITY, getDiscountReasonName } from '../models'

class ProductSetArea extends Component {
  static propTypes = {
    childProducts: PropTypes.instanceOf(List)
  }

  render() {
    return (
      <View style={styles.productSetContainer}>
        {this.props.childProducts.map((setProduct, i) => {
          return (
            <Text key={i} style={[baseStyle.subText, styles.productSetText]}>
              ・{setProduct.name}　×{setProduct.quantity}
            </Text>
          )
        })}
      </View>
    )
  }
}

class DiscountOptionsModal extends Component {
  static propTypes = {
    item: PropTypes.instanceOf(CartItem).isRequired,
    onItemChanged: PropTypes.func
  }

  async _applyPromotionPreset(promotionPreset) {
    const discountReasonName = await getDiscountReasonName(
      promotionPreset.discount_reason_id
    )
    const discount = CartItemDiscount.createWithPromotionPreset(
      promotionPreset,
      discountReasonName
    )
    Modal.close()
    this.props.onItemChanged(
      this.props.item,
      this.props.item.applyDiscount(discount)
    )
  }

  _applyDiscount(discount) {
    Modal.close()
    this.props.onItemChanged(
      this.props.item,
      this.props.item.applyDiscount(discount)
    )
  }

  _clearDiscount() {
    Modal.close()
    this.props.onItemChanged(this.props.item, this.props.item.clearDiscounts())
  }

  _changePrice(amount) {
    Modal.close()
    this.props.onItemChanged(
      this.props.item,
      this.props.item.changePrice(amount)
    )
  }

  _changeOriginalPrice(amount) {
    Modal.close()
    this.props.onItemChanged(
      this.props.item,
      this.props.item.changeOriginalPrice(amount)
    )
  }

  _cancelDiscount(discountType) {
    Modal.close()
    this.props.onItemChanged(
      this.props.item,
      this.props.item.cancelDiscountByDiscountType(discountType)
    )
  }

  render() {
    /** @type {CartItem} **/
    const item = this.props.item

    return (
      <CartItemDiscountOptionsView
        currency={item.currency}
        discountAmount={item.manualAmountDiscountAmount.toNumber()}
        discountRate={item.manualRateDiscountRate.toNumber()}
        maxDiscountAmount={item.maxManualDiscountAmount.toNumber()}
        maxDiscountRate={item.maxManualDiscountRate.mul(100).toNumber()}
        style={styles.discountOptionsModal}
        onSelectPromotionPreset={preset => this._applyPromotionPreset(preset)}
        onApplyDiscount={discount => this._applyDiscount(discount)}
        onCancelDiscount={discountType => this._cancelDiscount(discountType)}
        minChangePrice={item.product.immutableOriginalPrice}
        onChangePrice={amount => this._changePrice(amount)}
        onChangeOriginalPrice={amount => this._changeOriginalPrice(amount)}
        onClearDiscount={() => this._clearDiscount()}
        isDiscountable={item.validateCanApplyManualDiscount.isValid}
        disableDiscountReasonMessage={
          item.validateCanApplyManualDiscount.message
        }
        isChangedPrice={item.isChangedPrice}
        isChangedOriginalPrice={item.isChangedOriginalPrice}
        disableChangePrice={!item.validateCanChangePrice.isValid}
        disableChangePriceReasonMessage={item.validateCanChangePrice.message}
      />
    )
  }
}

export default class CartItemView extends PureComponent {
  constructor() {
    super()
    this.state = {
      isProductSetAreaOpen: false
    }
  }

  shouldComponentUpdate(nextProps) {
    return !nextProps.item.equals(this.props.item)
  }

  static propTypes = {
    item: PropTypes.instanceOf(CartItem),
    onItemChanged: PropTypes.func,
    canDiscount: PropTypes.bool,
    canIncreaseDecrease: PropTypes.bool,
    showLineNumber: PropTypes.bool,
    isCartPage: PropTypes.bool
  }

  static defaultProps = {
    canDiscount: true,
    canIncreaseDecrease: true
  }

  _openDiscountOptionsModal() {
    Modal.openBy(DiscountOptionsModal, this.refs.layoutRoot, {
      direction: Modal.DIRECTION.RIGHT,
      adjust: Modal.ADJUST.CENTER,
      isBackgroundVisible: false,
      height: 450,
      props: {
        item: this.props.item,
        onItemChanged: this.props.onItemChanged
      }
    })
  }

  _renderDiscounts() {
    return (
      <View style={styles.discountArea}>
        {this.props.item.discounts
          .map((discount, index) => (
            <Text key={index} style={styles.discountText}>
              {this._discountToText(discount)}
            </Text>
          ))
          .toArray()}
      </View>
    )
  }

  _renderQuantity() {
    /** @var {CartItem} **/
    const item = this.props.item
    return (
      <CartInputNumberForm
        number={item.quantity}
        onIncrease={() => this.props.onItemChanged(item, item.increase())}
        onDecrease={() => this.props.onItemChanged(item, item.decrease())}
        maxNumber={MAX_CART_ITEM_QUANTITY}
        onChange={quantity =>
          this.props.onItemChanged(item, item.changeQuantity(quantity))
        }
        canIncreaseDecrease={this.props.canIncreaseDecrease}
      />
    )
  }

  get _canAdditionalOperation() {
    return this.props.canDiscount
  }

  /**
   *
   * @param {CartItem} item
   * @protected
   */
  _salesPrice(item) {
    if (item.bundlePriceForDisplayTotal === null) {
      // 意図的に bundlePriceForDisplayTotal がnullにされている場合は、価格の非表示を表す（よりどり構成品の価格など）
      return ''
    } else {
      return formatPriceWithCurrency(
        item.bundlePriceForDisplayTotal || item.salesTotal.abs(),
        item.currency
      )
    }
  }

  render() {
    /** @var {CartItem} **/
    const item = this.props.item

    return (
      <TouchableOpacitySfx
        onPress={() => this._openDiscountOptionsModal()}
        disabled={!this._canAdditionalOperation}
      >
        <View style={styles.root} ref='layoutRoot'>
          <View style={styles.headerLabelsArea}>
            <Label
              baseStyle={styles.taxFreeLabelBase}
              textStyle={styles.taxFreeLabelText}
              isVisible={item.isTaxfreeApplied}
            >
              {I18n.t('cart.taxfree_label')}
            </Label>
          </View>
          <View style={styles.headerArea}>
            <View style={[styles.productLabelArea, { flex: 3 }]}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'middle'}
                style={baseStyle.subText}
              >
                {item.product.productCode}
              </Text>
            </View>
            <View style={[styles.productLabelArea, { flex: 3 }]}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'middle'}
                style={baseStyle.subText}
              >
                {item.product.articleNumber}
              </Text>
            </View>
            <View style={styles.discountLabelArea}>
              {item.product.isDiscountable ? null : (
                <Image
                  source={require('../../../assets/images/cannot_discount.png')}
                  style={styles.cannotDiscountIcon}
                />
              )}
            </View>
            {!this.props.showLineNumber ? null : (
              <View style={styles.lineNumberArea}>
                <Text style={[baseStyle.commonText]}>
                  {this.props.item.sortOrder}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.mainArea}>
            <Text
              numberOfLines={1}
              style={[baseStyle.mainText, styles.productNameText]}
            >
              {item.product.name}
            </Text>
          </View>
          <View style={styles.subArea}>
            <View style={styles.productSubInfoArea}>
              <Text style={[baseStyle.subText, styles.unitPriceText]}>
                {I18n.t('cart.unit_price')} :{' '}
                {formatPriceWithCurrency(item.price.abs(), item.currency)}
              </Text>
            </View>
            <View style={styles.productSubInfoArea}>
              <Text style={[baseStyle.subText, styles.unitPriceText]}>
                税率：{String(item.product.tax.taxRate * 100)}％
              </Text>
            </View>
            <View style={styles.operationArea}>
              <Text style={[baseStyle.strongTextStyle, styles.priceText]}>
                {this._salesPrice(item)}
              </Text>
              {this._renderQuantity()}
            </View>
          </View>
          {!item.discountsLabel ? null : (
            <View style={styles.discountArea}>
              <Text style={styles.discountText}>{item.discountsLabel}</Text>
            </View>
          )}
          {!item.product.isParentProduct ? null : (
            <OpenCloseButton
              style={styles.openSetButton}
              text={'セット販売'}
              source={require('../../../assets/images/set_product.png')}
              onIsOpenChanged={isOpen => {
                this.setState({ ...this.state, isProductSetAreaOpen: isOpen })
              }}
            />
          )}
          {!this.state.isProductSetAreaOpen ? null : (
            <ProductSetArea childProducts={item.product.childProducts} />
          )}
          <View style={styles.footer} />
        </View>
      </TouchableOpacitySfx>
    )
  }
}
