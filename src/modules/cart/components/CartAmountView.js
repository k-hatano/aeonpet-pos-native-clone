import React, { Component } from 'react'
import { Text, View, ListView, Button } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartAmountView'
import baseStyle from 'common/styles/baseStyle'
import CartSubtotalView from './CartSubtotalView'
import CartTotalView from './CartTotalView'
import Cart from '../models/Cart'
import { ProceedButton } from '../../../common/components/elements/StandardButton'
import CartPaymentListItem from './CartPaymentListItem'

export default class CartAmountView extends Component {
  constructor() {
    super()
    this.state = {
      displayPayments: false
    }
    this.togglePayments = this.togglePayments.bind(this)
  }
  static propTypes = {
    cart: PropTypes.instanceOf(Cart),
    onUpdateCart: PropTypes.func,
    onPaymentChange: PropTypes.func,
    onDeposit: PropTypes.func,
    depositDisabled: PropTypes.bool,
    useableBasePoints: PropTypes.number,
    queueLength: PropTypes.number
  }

  togglePayments() {
    this.setState({ displayPayments: !this.state.displayPayments })
  }

  render() {
    /** @type Cart */
    const cart = this.props.cart
    const displayPayments = this.state.displayPayments
    const queueLength = this.props.queueLength
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const isReady = queueLength === 0
    console.log(displayPayments)
    return (
      <View style={styles.layoutRoot}>
        <CartSubtotalView
          cart={cart}
          onUpdateCart={this.props.onUpdateCart}
          useableBasePoints={this.props.useableBasePoints}
        />

        <View style={styles.subSeparator} />
        <CartTotalView cart={cart} />

        <View style={styles.separator} />
        <ProceedButton
          text={displayPayments ? I18n.t('cart.back') : I18n.t('cart.select_payment')}
          style={styles.submitButton}
          onPress={this.togglePayments}
        />

        {displayPayments && (
          <View style={styles.paymentMethodList}>
            <ListView
              initialListSize={7}
              enableEmptySections
              dataSource={dataSource.cloneWithRows(cart.payments.toArray())}
              renderRow={payment => {
                return (
                  <CartPaymentListItem
                    payment={payment}
                    canUse={cart.canUsePayment(payment)}
                    justAmount={cart.justAmountForPayment(payment).toNumber()}
                    maxAmount={cart.maxAmountForPayment(payment).toNumber()}
                    onChange={this.props.onPaymentChange}
                    maxUsePoint={cart.maxUsePoint}
                    useableBasePoints={cart.shop.useable_base_points}
                  />
                )
              }}
              renderSeparator={(sectionId, rowId) => (
                <View key={rowId} style={styles.paymentsSeparator} />
              )}
            />
          </View>
        )}
        <View style={styles.separator} />
        <ProceedButton
          text={I18n.t(cart.isReturn ? 'cart.do_return' : 'cart.deposit')}
          style={styles.submitButton}
          onPress={this.props.onDeposit}
          disabled={this.props.depositDisabled || !this.props.cart.canComplete}
        />
      </View>
    )
  }
}
