import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartBundledItemView'
import baseStyle from '../../../common/styles/baseStyle'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import CartItemView from './CartItemView'
import OpenCloseButton from '../../../common/components/widgets/OpenCloseButton'
import Label from '../../../common/components/elements/Label'

export default class CartBundledItemView extends Component {
  static propTypes = {
    bundle: PropTypes.object,
    items: PropTypes.array,
    currency: PropTypes.string,
    onItemChanged: PropTypes.func,
    isCartPage: PropTypes.bool
  }

  constructor () {
    super()
    this.state = {
      isItemsOpen: false
    }
  }

  _renderChildItem (item) {
    return (
      <CartItemView
        isCartPage={this.props.isCartPage}
        item={item}
        onItemChanged={this.props.onItemChanged}
        canDiscount={false}
        canIncreaseDecrease={false} />
    )
  }

  render () {
    return (
      <View style={styles.root}>
        <View style={styles.headerLabelsArea}>
          <Label
            baseStyle={styles.taxFreeLabelBase}
            textStyle={styles.taxFreeLabelText}
            isVisible={this.props.bundle.isTaxfreeApplied}>
            {I18n.t('cart.taxfree_label')}
          </Label>
        </View>
        <View style={styles.header}>
          <Text numberOfLines={2} style={[baseStyle.mainText, styles.bundleName]}>
            {this.props.bundle.name}
          </Text>
          <View style={styles.headerSubArea}>
            <OpenCloseButton
              style={styles.openListButton}
              source={require('../../../assets/images/bundle.png')}
              text={'バンドル販売'}
              onIsOpenChanged={(isOpen) => this.setState({isItemsOpen: isOpen})} />
            <Text style={[baseStyle.strongTextStyle, styles.priceText]}>
              {formatPriceWithCurrency(this.props.bundle.amount, this.props.currency)}
            </Text>
          </View>
          <View style={styles.footer} />
        </View>
        <View style={styles.separator} />
        {!this.state.isItemsOpen ? null : (
          <View style={styles.listArea}>
            <View style={styles.groupIndex} />
            <View style={styles.items}>
              {(this.props.items || []).map(item => (
                <View key={item.hashCode()}>
                  {this._renderChildItem(item)}
                  <View style={styles.separator} />
                </View>
              ))}
            </View>
          </View>
        )}
      </View>
    )
  }
}
