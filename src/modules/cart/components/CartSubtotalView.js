import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import Decimal from 'decimal.js'
import styles from '../styles/CartSubtotalView'
import { OptionalCommandButton } from 'common/components/elements/StandardButton'
import { formatPriceWithCurrency } from 'common/utils/formats'
import Cart from '../models/Cart'
import CartDiscountOptionsView from '../containers/CartDiscountOptionsView'
import Modal from 'common/components/widgets/Modal'
import CartDiscount from '../models/CartDiscount'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import { getDiscountReasonName } from '../models'
import AlertView from 'common/components/widgets/AlertView'
import { OWNER_DISCOUNT } from '../../promotion/models'

class DiscountText extends Component {
  static propTypes = {
    titleKey: PropTypes.string,
    amount: PropTypes.instanceOf(Decimal),
    currency: PropTypes.string
  }

  render () {
    if (this.props.amount.lte(0)) {
      return <View style={styles.discountLine} />
    } else {
      return (
        <View style={styles.discountLine}>
          <Text style={styles.discountText}>
            {I18n.t(this.props.titleKey)}
          </Text>
          <Text style={styles.discountTextSeparator}>
            {' : '}
          </Text>
          <Text style={styles.discountText}>
            {formatPriceWithCurrency(this.props.amount, this.props.currency)}
          </Text>
        </View>
      )
    }
  }
}

class DiscountOptionsModal extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart).isRequired,
    onUpdateCart: PropTypes.func
  }

  async _applyPromotionPreset (promotionPreset) {
    const discountReasonName = promotionPreset.promotion_preset_code === OWNER_DISCOUNT.CODE
      ? OWNER_DISCOUNT.NAME : await getDiscountReasonName(promotionPreset.discount_reason_id)
    const discount = CartDiscount.createWithPromotionPreset(promotionPreset, discountReasonName)
    Modal.close()
    this.props.onUpdateCart(this.props.cart.applySubtotalDiscount(discount))
  }

  _applyDiscount (discount) {
    /** @type {Cart} **/
    const cart = this.props.cart
    Modal.close()
    this.props.onUpdateCart(cart.applySubtotalDiscount(discount))
  }

  _cancelDiscount (discountType) {
    /** @type {Cart} **/
    const cart = this.props.cart
    Modal.close()
    this.props.onUpdateCart(cart.cancelDiscount(discountType))
  }

  render () {
    /** @type {Cart} **/
    const cart = this.props.cart

    return (
      <CartDiscountOptionsView
        discountAmount={cart.subtotalAmountDiscountAmount.toNumber()}
        maxDiscountAmount={cart.maxAmountDiscountAmount.toNumber()}
        discountRate={cart.subtotalDiscountRate.toNumber()}
        ownerDiscountAmount={cart.ownerDiscountAmount.toNumber()}
        maxDiscountRate={cart.maxRateDiscountRate.mul(100).toNumber()}
        currency={cart.currency}
        style={styles.discountOptionsModal}
        onSelectPromotionPreset={preset => this._applyPromotionPreset(preset)}
        onApplyDiscount={discount => this._applyDiscount(discount)}
        onCancelDiscount={discountType => this._cancelDiscount(discountType)}
      />
    )
  }
}

export default class CartSubtotalView extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart),
    onUpdateCart: PropTypes.func,
    onDiscountByPoint: PropTypes.func,
    useableBasePoints: PropTypes.number
  }

  _openDiscountOptionsModal () {
    Modal.openBy(DiscountOptionsModal, this.refs.subtotalOperation, {
      direction: Modal.DIRECTION.LEFT,
      adjust: Modal.ADJUST.TOP,
      isBackgroundVisible: false,
      props: {
        cart: this.props.cart,
        onUpdateCart: this.props.onUpdateCart
      }
    })
  }

  _openPointPadModal () {
    /** @type Cart */
    const cart = this.props.cart

    AmountPadModal.open({
      onComplete: point => {
        if (point < 1) {
          AlertView.show(I18n.t('message.B-01-E017', { useableBasePoints: this.props.useableBasePoints }))
          return false
        } else {
          this.props.onUpdateCart(cart.setUsePoint(point))
        }
        return true
      },
      mode: AMOUNT_PAD_MODE.POINT,
      maxValue: cart.maxUsePoint,
      justValue: cart.maxUsePoint,
      disableClearPoint: cart.usePoint === 0,
      onClearPoint: () => {
        this.props.onUpdateCart(cart.setUsePoint(0))
        return true
      },
      useableBasePoints: this.props.useableBasePoints
    })
  }

  render () {
    /** @type Cart */
    const cart = this.props.cart
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.amountArea}>
          <Text style={styles.amountTitleText}>{I18n.t('cart.subtotal')}</Text>
          <Text style={styles.amountText}>
            {formatPriceWithCurrency(cart.totalBase, cart.currency)}
          </Text>
          <Text style={styles.amountSubInfoText}>
            {I18n.t('cart.tax')}
            {'  :  '}
            {formatPriceWithCurrency(cart.totalTax, cart.currency)}
          </Text>
          {/* <Text style={styles.amountSubInfoText}> */}
          {/* {I18n.t('cart.add_point')} */}
          {/* {'  :  '} */}
          {/* {I18n.t('cart.format_point', {point: formatNumber(cart.pointDiscountTaxless)})} */}
          {/* </Text> */}
        </View>
        <View style={styles.discountArea}>
          <DiscountText
            currency={cart.currency}
            amount={cart.subtotalAmountDiscountAmount}
            titleKey='cart.subtotal_amount_discount'
          />
          <DiscountText
            currency={cart.currency}
            amount={cart.subtotalRateDiscountAmount}
            titleKey='cart.subtotal_rate_discount'
          />
          <Text style={styles.discountPercentText}>
            {cart.subtotalDiscountPercent.lte(0) ? '' : '(' + cart.subtotalDiscountPercent.toNumber() + '%)'}
          </Text>
          <DiscountText
            currency={cart.currency}
            amount={cart.ownerDiscountAmount}
            titleKey='cart.owner_discount'
          />
          <Text style={styles.discountPercentText}>
            {cart.ownerDiscountAmount.lte(0) ? '' : '(' + cart.ownerDiscountPercent.toNumber() + '%)'}
          </Text>
        </View>
        <View style={styles.operationArea}>
          <OptionalCommandButton
            disabled={!cart.canDiscount}
            ref='subtotalOperation'
            text={I18n.t('cart.subtotal_operation')}
            style={styles.button}
            onPress={() => this._openDiscountOptionsModal()}
          />
        </View>
      </View>
    )
  }
}
