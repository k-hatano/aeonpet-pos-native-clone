import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartCustomerView'
// import baseStyle from 'common/styles/baseStyle'
import { OptionalCommandButton } from 'common/components/elements/StandardButton'
import ImageButton from 'common/components/elements/ImageButton'
import { showSelectCustomerViewModal } from '../../customer/containers/SelectCustomerViewModal'
import { formatUnit } from 'common/utils/formats'
import { getCustomerName } from '../models'
import { showCustomerInfoModal } from 'modules/customer/containers/CustomerInfoModal'

export default class CartCustomerView extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    customer: PropTypes.shape({
      last_name_kana: PropTypes.string,
      first_name_kana: PropTypes.string,
      customer_code: PropTypes.string,
      point: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    }),
    onSelectCustomer: PropTypes.func,
    onClearCustomer: PropTypes.func,
    setIsSearchProduct: PropTypes.func
  }

  _showSelectCustomerModal () {
    this.props.setIsSearchProduct(false)
    this.props.onShowSelectCustomerModal(true)
    showSelectCustomerViewModal(async (customer) => {
      if (customer) {
        this.props.onSelectCustomer && await this.props.onSelectCustomer(customer)
      }
      this.props.setIsSearchProduct(true)
    })
  }

  _renderEmpty () {
    return (
      <View style={[styles.emptyViewLayout, this.props.disabled ? {opacity: 0} : {}]}>
        <OptionalCommandButton
          text={I18n.t('customer.customer_selector_title')}
          style={styles.selectCustomerButton}
          onPress={() => this._showSelectCustomerModal()}
          disabled={this.props.disabled} />
      </View>
    )
  }

  _renderCustomer () {
    const { customer } = this.props
    const customerName = getCustomerName(customer)
    return (
      <View style={styles.customerViewLayout}>
        <TouchableOpacity style={styles.customerViewLayout} onPress={() => { showCustomerInfoModal() }}>
          <ImageButton
            style={styles.userIcon}
            toggle
            appearance={{
              normal: require('../../../assets/images/user.png'),
              highlight: require('../../../assets/images/user.png')
            }}
            onPress={() => {}}
          />
          <Text style={styles.customerCodeText}>
            {customer.customer_code}
          </Text>
          <Text style={styles.customerNameText}>
            {customerName + I18n.t('customer.honorific')}
          </Text>
          <Text style={styles.pointText}>
            {formatUnit(customer.customer_activity.points, 'pt')}
          </Text>
        </TouchableOpacity>
        <ImageButton
          style={styles.circleCloseIcon}
          toggle
          appearance={{
            normal: require('../../../assets/images/close.png'),
            highlight: require('../../../assets/images/close.png')
          }}
          onPress={() => this.props.onClearCustomer()}
        />
      </View>
    )
  }

  render () {
    return (
      <View style={styles.layoutRoot}>
        {this.props.customer
          ? this._renderCustomer()
          : this._renderEmpty()
        }
        <View style={styles.separator} />
      </View>
    )
  }
}
