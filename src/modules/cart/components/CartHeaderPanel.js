import React, { Component } from 'react'
import { Text, View, Switch } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartHeaderPanel'
import baseStyle from 'common/styles/baseStyle'
import { WeakProceedButton, ProceedButton } from 'common/components/elements/StandardButton'
import Cart from '../models/Cart'

export default class CartHeaderPanel extends Component {
  static propTypes = {
    message: PropTypes.string,
    cart: PropTypes.instanceOf(Cart),
    onUpdateCart: PropTypes.func,
    onPendOrder: PropTypes.func,
    isTraining: PropTypes.bool
  }

  render () {
    /** @type {Cart} */
    const { cart, isTraining } = this.props
    const isVisible = !(cart.isReturn || isTraining)
    return (
      <View style={styles.root}>
        <View style={styles.messageArea}>
          <Text style={[baseStyle.subText, styles.message]}>
            {this.props.message}
          </Text>
        </View>
        <View style={styles.controlArea}>
          <View style={styles.controlLeftArea}>
            <View style={styles.disableBundleLayout} >
              <Text style={[baseStyle.mainText]}>
                {I18n.t('cart.disable_bundle')}
              </Text>
              <Switch
                style={styles.switch}
                onValueChange={value => this.props.onUpdateCart(cart.setIsBundleDisabled(value))}
                value={cart.isBundleDisabled} />
            </View>
            {!cart.canApplyTaxfree ? null : (
              <View style={styles.taxfreeSwitchWrapper} >
                <Text style={[baseStyle.mainText]}>
                  {I18n.t('cart.taxfree')}
                </Text>
                <Switch
                  style={styles.switch}
                  onValueChange={value => this.props.onUpdateCart(cart.setIsTaxfree(value))}
                  value={cart.isTaxfree} />
              </View>
            )}
          </View>
          <View style={styles.separator} />
          <View style={styles.controlRightArea}>
            {cart.isReturn ? null : (
              <WeakProceedButton
                style={styles.resumeButton}
                text={I18n.t('cart.resume_order')}
                onPress={() => this.props.onPendOrder(cart)}
                disabled={!isVisible} />
            )}
            <ProceedButton
              style={styles.clearButton}
              text={I18n.t('common.clear')}
              onPress={() => this.props.onUpdateCart(cart.clearItems())} />
          </View>
        </View>
      </View>
    )
  }
}
