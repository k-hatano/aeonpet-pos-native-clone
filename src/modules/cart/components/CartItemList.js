import React, { Component } from 'react'
import { View, ListView } from 'react-native'
// import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartItemList'
// import baseStyle from 'common/styles/baseStyle'
import CartBundledItemView from './CartBundledItemView'
import CartItemView from './CartItemView'
import { CART_LIST_ITEM_TYPE } from '../models/CartItemFormatter'
import CustomPropTypes from 'common/components/CustomPropTypes'

export default class CartItemList extends Component {
  static propTypes = {
    items: PropTypes.array,
    currency: PropTypes.string,
    onItemChanged: PropTypes.func,
    style: CustomPropTypes.Style,
    isScrollScheduled: PropTypes.bool,
    endScroll: PropTypes.func,
    isCartPage: PropTypes.bool
  }

  _renderRow(item) {
    switch (item.type) {
      case CART_LIST_ITEM_TYPE.NORMAL:
        return this._renderNormalItem(item)

      case CART_LIST_ITEM_TYPE.BUNDLE:
        return this._renderBundledItem(item)

      default:
        console.warn('Invalid item type')
    }
  }

  _scrollToEnd() {
    if (this.props.items.length > 5 && this.props.isScrollScheduled) {
      this.refs.listView.scrollToEnd()
    }
    this.props.endScroll()
  }

  _renderNormalItem(item) {
    return (
      <View
        onLayout={() => {
          this._scrollToEnd()
        }}
      >
        <CartItemView
          isCartPage={this.props.isCartPage}
          item={item.item}
          onItemChanged={this.props.onItemChanged}
        />
        <View style={styles.separator} />
      </View>
    )
  }

  _renderBundledItem(item) {
    return (
      <View
        onLayout={() => {
          this._scrollToEnd()
        }}
      >
        <CartBundledItemView
          isCartPage={this.props.isCartPage}
          bundle={item.bundle}
          currency={this.props.currency}
          items={item.items}
          onItemChanged={this.props.onItemChanged}
        />
      </View>
    )
  }

  render() {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    return (
      <ListView
        initialListSize={30}
        style={this.props.style}
        enableEmptySections
        dataSource={dataSource.cloneWithRows(this.props.items)}
        renderRow={item => this._renderRow(item)}
        ref={'listView'}
      />
    )
  }
}
