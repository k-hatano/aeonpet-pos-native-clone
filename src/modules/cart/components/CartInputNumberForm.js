import React, { Component } from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/CartInputNumberForm'
import IconButton, { ICON_BUTTON_TYPE } from '../../../common/components/elements/IconButton'
import { NumberInputLabel } from '../../../common/components/elements/labels'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import TouchableOpacitySfx from '../../../common/components/elements/TouchableOpacitySfx'
import Modal from '../../../common/components/widgets/Modal'

export default class CartItemNumberForm extends Component {
  static propTypes = {
    onIncrease: PropTypes.func,
    onDecrease: PropTypes.func,
    onChange: PropTypes.func,
    number: PropTypes.number,
    maxNumber: PropTypes.number,
    canIncreaseDecrease: PropTypes.bool
  }

  _onChangeAmount () {
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.NUMBER,
      maxValue: this.props.maxNumber,
      onComplete: (amount) => {
        // 本当は onChange の結果を返すようにしないと、
        // エラー時に再度入力パッドを出すということができないが、
        // 今は影響範囲がおおきいので暫定対応として、onChange の前で閉じるようにしておく。
        // → onChangeでエラーが出るため、その対処
        Modal.close()
        this.props.onChange && this.props.onChange(amount)
      }
    })
  }

  render () {
    return (
      <View style={styles.layoutRoot}>
        <TouchableOpacitySfx onPress={this.props.onDecrease} disabled={!this.props.canIncreaseDecrease}>
          <View style={styles.buttonTouchableArea}>
            {!this.props.canIncreaseDecrease ? null : <IconButton
              icon={'minus'}
              type={ICON_BUTTON_TYPE.FONTAWESOME}
              style={styles.button}
              onPress={this.props.onDecrease} />}
          </View>
        </TouchableOpacitySfx>
        {/* 当たり判定を見た目より広くとるため、NumberInputLabelの周りにViewを置く */}
        <TouchableOpacitySfx onPress={() => this._onChangeAmount()}>
          <View style={styles.numberTouchableArea}>
            <NumberInputLabel
              style={styles.number}
              value={this.props.number}
              onPress={() => this._onChangeAmount()} />
          </View>
        </TouchableOpacitySfx>
        <TouchableOpacitySfx onPress={this.props.onIncrease} disabled={!this.props.canIncreaseDecrease}>
          <View style={styles.buttonTouchableArea}>
            {!this.props.canIncreaseDecrease ? null : <IconButton
              icon={'plus'}
              type={ICON_BUTTON_TYPE.FONTAWESOME}
              style={styles.button}
              onPress={this.props.onIncrease} />}
          </View>
        </TouchableOpacitySfx>
      </View>
    )
  }
}
