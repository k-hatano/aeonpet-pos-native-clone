import React, { PureComponent } from 'react'
import { Text, View, Image } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/CartPaymentListItem'
import baseStyle from 'common/styles/baseStyle'
import { OptionalCommandButton } from 'common/components/elements/StandardButton'
import NumberBox from 'common/components/elements/NumberBox'
import CartPayment from '../models/CartPayment'
import { formatNumber } from '../../../common/utils/formats'
import {
  paymentMethodTypeToImage,
  isCreditCardOrEmoney,
  PAYMENT_METHOD_TYPE
} from 'modules/payment/models'
import { hasSettingPayment } from 'modules/setting/models'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'

export default class CartPaymentListItem extends PureComponent {
  static propTypes = {
    /** @type CartPayment */
    payment: PropTypes.instanceOf(CartPayment),
    canUse: PropTypes.bool,
    onChange: PropTypes.func,
    justAmount: PropTypes.number,
    maxAmount: PropTypes.number,
    maxUsePoint: PropTypes.number,
    useableBasePoints: PropTypes.number
  }

  _renderNumberBox() {
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.iconArea}>
          <Image
            source={paymentMethodTypeToImage(
              this.props.payment.paymentMethodType
            )}
          />
        </View>
        <Text style={[baseStyle.mainText, styles.nameText]}>
          {this.props.payment.name}
        </Text>
        <NumberBox
          disabled={!this.props.canUse}
          style={styles.numberBox}
          onChange={amount =>
            this.props.onChange(
              this.props.payment,
              this.props.payment.updateAmount(amount)
            )
          }
          justAmount={this.props.justAmount}
          maxAmount={this.props.maxAmount}
          amount={this.props.payment.amount}
          mode={
            this.props.payment.paymentMethodType ===
            PAYMENT_METHOD_TYPE.INTERNAL_POINT
              ? AMOUNT_PAD_MODE.POINT
              : AMOUNT_PAD_MODE.CASH
          }
          maxUsePoint={this.props.maxUsePoint}
          useableBasePoints={this.props.useableBasePoints}
          formatAmount={amount => formatNumber(amount)}
        />
        <Text style={styles.unitText}>{I18n.t('cart.amount_suffix')}</Text>
        <OptionalCommandButton
          disabled={!this.props.canUse}
          onPress={() =>
            this.props.onChange(
              this.props.payment,
              this.props.payment.updateAmount(this.props.justAmount)
            )
          }
          text={I18n.t('cart.just')}
          style={styles.button}
        />
      </View>
    )
  }

  _renderUnavailable() {
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.iconArea}>
          <Image
            source={paymentMethodTypeToImage(
              this.props.payment.paymentMethodType
            )}
          />
        </View>
        <Text style={[baseStyle.mainText, styles.nameText, styles.disableText]}>
          {this.props.payment.name}
        </Text>
        <View>
          <Text style={styles.unavailableText}>
            {I18n.t('cart.unavailable')}
          </Text>
        </View>
      </View>
    )
  }

  _renderMethods() {
    if (
      isCreditCardOrEmoney(this.props.payment.paymentMethodType) &&
      !this.props.payment.is_offline
    ) {
      if (hasSettingPayment()) {
        return this._renderNumberBox()
      } else {
        return this._renderUnavailable()
      }
    }
    return this._renderNumberBox()
  }

  render() {
    return this._renderMethods()
  }
}
