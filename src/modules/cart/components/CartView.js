import React, { Component } from 'react'
import { View } from 'react-native'
import styles from '../styles/CartView'
import Cart from '../models/Cart'
import CartHeaderPanel from './CartHeaderPanel'
import CartItemList from './CartItemList'
import CartItemFormatter from '../models/CartItemFormatter'
import PropTypes from 'prop-types'

export default class CartView extends Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Cart),
    onCartItemSelected: PropTypes.func,
    onUpdateCart: PropTypes.func,
    onItemChanged: PropTypes.func,
    onPendOrder: PropTypes.func,
    isTraining: PropTypes.bool
  }

  render () {
    /** @var {Cart} **/
    const { cart, isTraining } = this.props
    const items = (new CartItemFormatter()).format(cart)

    return (
      <View style={styles.root}>
        <CartHeaderPanel
          cart={cart}
          message={cart.bundleRiichiMessage}
          onUpdateCart={this.props.onUpdateCart}
          isTraining={isTraining}
          onPendOrder={this.props.onPendOrder} />
        <CartItemList
          style={styles.itemList}
          items={items}
          currency={'jpy'}
          onItemChanged={(oldItem, newItem) => this.props.onItemChanged(cart, oldItem, newItem)}
          isScrollScheduled={this.props.isScrollScheduled}
          endScroll={this.props.endScroll}
          isCartPage={true} />
      </View>
    )
  }
}
