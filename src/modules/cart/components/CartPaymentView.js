import React, { Component } from 'react'
import { Text, View } from 'react-native'
import componentStyles from '../../order/styles/OrderTab'
import StandardButton from '../../../common/components/elements/StandardButton'
import { NumberInputLabel } from 'common/components/elements/labels'
import CartPayment from 'modules/cart/models/CartPayment'
import { List } from 'immutable'
import I18n from 'i18n-js'
import ImageButton from '../../../common/components/elements/ImageButton'

class PaymentMethodView extends Component {
  static propTypes = {
    onSelected: React.PropTypes.func,
    paymentMethod: React.PropTypes.instanceOf(CartPayment),
    currency: React.PropTypes.string
  }

  static defaultProps = {
    currency: 'jpy'
  }

  render () {
    /** @var {CartPayment} */
    const paymentMethod = this.props.paymentMethod

    return (
      <View style={[componentStyles.itemView, componentStyles.borderItem]}>
        <View style={componentStyles.itemViewText}>
          <Text style={componentStyles.itemText}>{paymentMethod.name}</Text>
        </View>
        <NumberInputLabel
          value={paymentMethod.amount.toNumber()}
          onPress={() => this.props.onSelected(paymentMethod)} />
        <Text style={componentStyles.itemText}>{I18n.t('common.japanese_yen')}</Text>
        <View style={componentStyles.pinItem}>
          <ImageButton
            style={componentStyles.pinIcon}
            toggle
            appearance={{
              normal: require('../../../assets/images/pin@3x.png'),
              highlight: require('../../../assets/images/pin@3x.png')
            }}
            onPress={() => {}}
          />
        </View>
      </View>
    )
  }
}

export default class CartPaymentView extends Component {
  static propTypes = {
    payments: React.PropTypes.instanceOf(List),
    onSelectPaymentMethod: React.PropTypes.func,
    onSubmitPress: React.PropTypes.func
  }

  static defaultProps = {
    onSelectPaymentMethod: () => {}
  }

  render () {
    return (
      <View style={componentStyles.subtotalViewBottom}>
        <View style={componentStyles.paymentViewContainer}>
          <View style={componentStyles.paymentView}>
            <View style={componentStyles.cashViewLeft}>
              {this.props.payments.map((item, index) => {
                return (
                  <PaymentMethodView
                    key={item.hashCode()}
                    paymentMethod={item}
                    onSelected={this.props.onSelectPaymentMethod} />
                )
              })}
            </View>
          </View>
        </View>
        <View style={componentStyles.submitView}>
          <View style={componentStyles.checkoutButton}>
            <StandardButton
              isBorderButton={false}
              text={I18n.t('order.deposit')}
              textStyle={componentStyles.checkoutButtonText}
              color={'#44bcb9'}
              textColor={'#ffffff'}
              onPress={this.props.onSubmitPress}
            />
          </View>
        </View>
      </View>
    )
  }
}
