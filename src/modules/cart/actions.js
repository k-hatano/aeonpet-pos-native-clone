import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const updateCart = createAction(`${MODULE_NAME}_updateCart`)
export const setCartCustomer = createAction(`${MODULE_NAME}_setCartCustomer`)
export const listCartPromotionSet = createAction(`${MODULE_NAME}_listCartPromotionSet`)
export const listCartItemPromotionSet = createAction(`${MODULE_NAME}_listCartItemPromotionSet`)
export const setCartItemPromotion = createAction(`${MODULE_NAME}_setCartItemPromotion`)
