import CartItem from '../models/CartItem'
import { TAX_RULE } from 'common/models/Tax'
import CartItemProduct from '../models/CartItemProduct'
import { sampleProductMap } from 'modules/product/samples'
import generateUuid from '../../../common/utils/generateUuid'

export const defaultTax = { tax_rule: TAX_RULE.NONE, tax_rate: 300 }

/**
 * @param product
 * @param quantity
 * @param currency
 * @return {CartItem}
 */
export const createCartItem = (product, quantity = 1, currency = 'jpy') => {
  return new CartItem({
    product,
    currency,
    instanceUniqueKey: CartItem.generateInstanceUniqueKey(),
    quantity
  })
}

export const taxNone = {
  tax_rule: TAX_RULE.NONE,
  tax_rate: 0
}

export const taxExcludedFloor8 = {
  tax_rule: TAX_RULE.EXCLUDED_FLOOR,
  tax_rate: 0.08
}

export const taxIncludedFloor8 = {
  tax_rule: TAX_RULE.INCLUDED_FLOOR,
  tax_rate: 0.08
}

export const createCartItemProduct = (price, tax, productProperties = {}, productVariantProperties = {}) => {
  if (!tax) tax = taxNone
  return new CartItemProduct({
    id: generateUuid(),
    ...tax,
    ...productProperties
  }, {
    id: generateUuid(),
    price,
    ...productVariantProperties
  })
}

export const simpleProduct = new CartItemProduct({
  id: sampleProductMap.standardA.id,
  tax_rule: TAX_RULE.NONE,
  tax_rate: 0
}, {
  id: generateUuid(),
  price: 1000.0000
})

export const simpleProduct2 = new CartItemProduct({
  id: sampleProductMap.standardB.id,
  tax_rule: TAX_RULE.NONE,
  tax_rate: 0
}, {
  id: generateUuid(),
  price: 1000.0000
})
