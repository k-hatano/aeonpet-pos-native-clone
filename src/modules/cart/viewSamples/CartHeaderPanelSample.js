import CartHeaderPanel from '../components/CartHeaderPanel'
import Cart from '../models/Cart'
import { SimpleCartItemSample, SetCartItemSample } from './CartItemViewSample'

let cart = new Cart()
cart = cart.addItem(SimpleCartItemSample)
cart = cart.addItem(SetCartItemSample)

export default {
  name: 'CartHeaderPanel',
  component: CartHeaderPanel,
  properties: [
    {
      title: 'Default',
      property: {
        message: '',
        cart: cart
      }
    },
    {
      title: 'Message',
      property: {
        message: 'あと靴下１点でバンドル成立',
        cart: cart
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: {width: 683}
    }
  ]
}
