import Decimal from 'decimal.js'
import CartItemView from '../components/CartItemView'
import CartItemProduct from '../models/CartItemProduct'
import CartItem from '../models/CartItem'
import { TAX_RULE } from 'common/models/Tax'
import CartItemDiscount from '../models/CartItemDiscount'
import { DISCOUNT_TYPE, PROMOTION_TYPE } from '../../promotion/models'

export const SimpleCartItemSample = new CartItem({
  product: new CartItemProduct(
    {
      name: '商品名',
      product_code: 'product-code1',
      tax_rule: TAX_RULE.NONE,
      tax_rate: 0,
      is_discountable: true
    },
    {
      product_variant_name: 'ブルー／M／規格３',
      price: 1000.000
    }
  ),
  currency: 'jpy'
})

const rateDiscount = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.1)
})

const saleAmountDiscount = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(100),
  promotionType: PROMOTION_TYPE.SALE
})

export const SetCartItemSample = new CartItem({
  product: new CartItemProduct(
    {
      name: 'セット商品名',
      product_code: 'product-code4',
      tax_rule: TAX_RULE.NONE,
      tax_rate: 0,
      is_parent_product: true,
      child_products: [
        {
          name: 'セット商品内容1'
        },
        {
          name: 'セット商品内容3'
        }
      ]
    },
    {
      product_variant_name: 'ブルー／M／規格３',
      price: 1000.000
    }
  ),
  currency: 'jpy'
})

export default {
  name: 'CartItem',
  component: CartItemView,
  properties: [
    {
      title: 'Test',
      property: {
        item: SimpleCartItemSample,
        onItemChanged: () => { console.log('onItemChanged') }
      }
    },
    {
      title: 'Long Product Name',
      property: {
        item: new CartItem({
          product: new CartItemProduct(
            {
              name: '商品名です商品名です商品名です商品名です商品名です商品名です',
              product_code: 'product-code2',
              tax_rule: TAX_RULE.NONE,
              tax_rate: 0
            },
            {
              product_variant_name: 'ブルー／M／規格３',
              price: 3000.000
            }
          ),
          currency: 'jpy'
        }),
        onItemChanged: () => { console.log('onItemChanged') }
      }
    },
    {
      title: 'Taxfree',
      property: {
        item: new CartItem({
          product: new CartItemProduct(
            {
              name: '商品名',
              product_code: 'product-code3',
              tax_rule: TAX_RULE.NONE,
              tax_rate: 0
            },
            {
              product_variant_name: 'ブルー／M／規格３',
              price: 1000.000
            }
          ),
          currency: 'jpy',
          isTaxfreeApplied: true
        }),
        onItemChanged: () => { console.log('onItemChanged') }
      }
    },
    {
      title: 'Set',
      property: {
        item: SetCartItemSample,
        onItemChanged: () => { console.log('onItemChanged') }
      }
    },
    {
      title: 'Cannot Change',
      property: {
        item: SimpleCartItemSample,
        onItemChanged: null
      }
    },
    {
      title: 'With Sale Discount',
      property: {
        item: SimpleCartItemSample.applyDiscount(rateDiscount).applyDiscount(saleAmountDiscount),
        onItemChanged: null
      }
    },
    {
      title: 'Enoteca Info',
      property: {
        item: new CartItem({
          product: new CartItemProduct(
            {
              name: '商品名',
              product_code: 'product-code3',
              tax_rule: TAX_RULE.NONE,
              tax_rate: 0
            },
            {
              product_variant_name: 'ブルー／M／規格３',
              price: 1000.000
            }
          ),
          currency: 'jpy',
          isTaxfreeApplied: true
        }),
        onItemChanged: () => { console.log('onItemChanged') }
      }
    },
    {
      title: 'Enoteca Info and Line number',
      property: {
        item: new CartItem({
          product: new CartItemProduct(
            {
              name: '商品名',
              product_code: 'product-code3',
              tax_rule: TAX_RULE.NONE,
              tax_rate: 0
            },
            {
              product_variant_name: 'ブルー／M／規格３',
              price: 1000.000
            }
          ),
          currency: 'jpy',
          isTaxfreeApplied: true
        }).setSortOrder(12),
        onItemChanged: () => { console.log('onItemChanged') },
        showLineNumber: 1
      }
    }
  ],
  frames: [
    {
      title: 'W600',
      style: {width: 600}
    },
    {
      title: 'W683',
      style: {width: 683}
    }
  ]
}
