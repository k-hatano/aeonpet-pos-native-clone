import CartItemList from '../components/CartItemList'
import CartItemProduct from '../models/CartItemProduct'
import CartItem from '../models/CartItem'
import { TAX_RULE } from 'common/models/Tax'
import { BundleItemSample } from './CartBundledItemViewSample'
import { SetCartItemSample } from './CartItemViewSample'
import { CART_LIST_ITEM_TYPE } from '../models/CartItemFormatter'

export default {
  name: 'CartItemList',
  component: CartItemList,
  properties: [
    {
      title: 'Default',
      property: {
        items: [
          {
            type: CART_LIST_ITEM_TYPE.NORMAL,
            item: new CartItem({
              product: new CartItemProduct(
                {
                  name: '商品名',
                  product_code: 'product-code1',
                  tax_rule: TAX_RULE.NONE,
                  tax_rate: 0
                },
                {
                  product_variant_name: 'ブルー／M／規格３',
                  price: 1000.000
                }
              ),
              currency: 'jpy'
            })
          },
          {
            type: CART_LIST_ITEM_TYPE.BUNDLE,
            ...BundleItemSample
          },
          {
            type: CART_LIST_ITEM_TYPE.NORMAL,
            item: SetCartItemSample
          }
        ],
        onItemChanged: () => { console.log('onItemChanged') }
      },
      currency: 'jpy'
    }
  ],
  frames: [
    {
      title: 'W600',
      style: {width: 600}
    }
  ]
}
