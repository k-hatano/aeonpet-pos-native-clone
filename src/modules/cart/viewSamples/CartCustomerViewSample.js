import CartCustomerView from '../components/CartCustomerView'

export default {
  name: 'CartCustomerView',
  component: CartCustomerView,
  properties: [
    {
      title: 'Empty',
      property: {
        customer: null,
        onSelectCustomer: (customer) => console.log('onSelectCustomer', customer),
        onClearCustomer: () => console.log('onClearCustomer')
      }
    },
    {
      title: 'Selected',
      property: {
        customer: {
          last_name_kana: 'ヤマダ',
          first_name_kana: 'タロウ',
          customer_code: 'A1234567890',
          point: 1000
        },
        onSelectCustomer: () => console.log('onSelectCustomer'),
        onClearCustomer: () => console.log('onClearCustomer')
      }
    }
  ],
  frames: [
    {
      title: 'W686',
      style: {width: 686}
    }
  ]
}
