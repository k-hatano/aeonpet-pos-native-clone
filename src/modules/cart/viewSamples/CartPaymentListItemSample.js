import CartPaymentListItem from '../components/CartPaymentListItem'
import CartPayment from '../models/CartPayment'

export default {
  name: 'CartPaymentListItem',
  component: CartPaymentListItem,
  properties: [
    {
      title: 'Default',
      property: {
        payment: CartPayment.createFromPaymentMethodEntity({
          id: 'id1',
          name: '現金'
        }),
        onChange: (oldPayment, newPayment) => {
          console.log('Payment Changed', oldPayment, newPayment)
        }
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
