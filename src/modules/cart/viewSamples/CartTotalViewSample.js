import CartTotalView from '../components/CartTotalView'
import Decimal from 'decimal.js'

export const CartTotalDefaultSample = {
  total: new Decimal(1000),
  totalQuantity: 99,
  deposit: new Decimal(20000),
  change: new Decimal(1600)
}

export default {
  name: 'CartTotalView',
  component: CartTotalView,
  properties: [
    {
      title: 'PropertySample1',
      property: {
        cart: CartTotalDefaultSample
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
