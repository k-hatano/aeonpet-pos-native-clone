import CartInputNumberForm from '../components/CartInputNumberForm'

export default {
  name: 'CartInputNumberForm',
  component: CartInputNumberForm,
  properties: [
    {
      title: 'Default',
      property: {
        number: 1
      }
    },
    {
      title: 'Big Number',
      property: {
        number: 9999
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}

