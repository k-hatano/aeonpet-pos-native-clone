import CartSubtotalView from '../components/CartSubtotalView'
import Decimal from 'decimal.js'

export const CartSubtotalDefaultSample = {
  totalItems: new Decimal(108000),
  totalItemsTax: new Decimal(8000),
  addingPoint: new Decimal(1000),
  subtotalAmountDiscount: new Decimal(0),
  subtotalRateDiscount: new Decimal(0),
  subtotalRateDiscountRate: new Decimal(0),
  pointDiscount: new Decimal(0),
  currency: 'jpy'
}

export const CartSubtotalDiscountedSample = {
  totalItems: new Decimal(108000),
  totalItemsTax: new Decimal(8000),
  addingPoint: new Decimal(1000),
  subtotalAmountDiscount: new Decimal(10),
  subtotalRateDiscount: new Decimal(10),
  subtotalRateDiscountPercent: new Decimal(10),
  pointDiscount: new Decimal(10),
  currency: 'jpy'
}

export default {
  name: 'CartSubtotalView',
  component: CartSubtotalView,
  properties: [
    {
      title: 'Default',
      property: {
        cart: CartSubtotalDefaultSample
      }
    },
    {
      title: 'Discounted',
      property: {
        cart: CartSubtotalDiscountedSample
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: {width: 683}
    }
  ]
}
