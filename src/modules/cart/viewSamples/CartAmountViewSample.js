import CartAmountView from '../components/CartAmountView'
import { CartSubtotalDefaultSample, CartSubtotalDiscountedSample } from './CartSubtotalViewSample'
import { CartTotalDefaultSample } from './CartTotalViewSample'
import CartPayment from '../models/CartPayment'
import { List } from 'immutable'

export default {
  name: 'CartAmountView',
  component: CartAmountView,
  properties: [
    {
      title: 'PropertySample1',
      property: {
        cart: {
          ...CartSubtotalDefaultSample,
          ...CartTotalDefaultSample,
          canComplete: true,
          paymentMethods: new List([
            CartPayment.createFromPaymentMethodEntity({
              id: 'id1',
              name: '現金'
            }),
            CartPayment.createFromPaymentMethodEntity({
              id: 'id2',
              name: '電子マネー'
            }),
            CartPayment.createFromPaymentMethodEntity({
              id: 'id3',
              name: 'クレジットカード'
            })
          ])
        },
        onPaymentChange: (oldPayment, newPayment) => {
          console.log('onPaymentChange', oldPayment, newPayment)
        }
      }
    },
    {
      title: 'PropertySample2',
      property: {
        cart: {
          ...CartSubtotalDiscountedSample,
          ...CartTotalDefaultSample,
          canComplete: false,
          paymentMethods: new List([
            CartPayment.createFromPaymentMethodEntity({
              id: 'id1',
              name: '現金'
            })
          ])
        }
      }
    }
  ],
  frames: [
    {
      title: '683 x 888',
      style: {width: 683, height: 888}
    }
  ]
}
