import CartSelectProductFooter from '../components/CartSelectProductFooter'

export default {
  name: 'CartSelectProductFooter',
  component: CartSelectProductFooter,
  properties: [
    {
      title: 'PropertySample1',
      property: {
        cart: {
          itemsTotal: 1200,
          totalQuantity: 4
        }
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
