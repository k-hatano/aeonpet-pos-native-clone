import CartBundledItemView from '../components/CartBundledItemView'
import CartItemProduct from '../models/CartItemProduct'
import CartItem from '../models/CartItem'
import { TAX_RULE } from 'common/models/Tax'

export const BundleItemSample = {
  bundle: {
    name: '靴下よりどり３足1,000円',
    amount: 1000
  },
  items: [
    new CartItem({
      product: new CartItemProduct(
        {
          name: '靴下A',
          product_code: 'product-code1',
          tax_rule: TAX_RULE.NONE,
          tax_rate: 0
        },
        {
          product_variant_name: 'ブルー／M／規格３',
          price: 1000.000
        }
      ),
      currency: 'jpy'
    }),
    new CartItem({
      product: new CartItemProduct(
        {
          name: '靴下B',
          product_code: 'product-code1',
          tax_rule: TAX_RULE.NONE,
          tax_rate: 0
        },
        {
          product_variant_name: 'グレー／M／規格３',
          price: 1000.000
        }
      ),
      currency: 'jpy'
    }),
    new CartItem({
      product: new CartItemProduct(
        {
          name: '靴下C',
          product_code: 'product-code1',
          tax_rule: TAX_RULE.NONE,
          tax_rate: 0
        },
        {
          product_variant_name: 'ブラック／M／規格３',
          price: 1000.000
        }
      ),
      currency: 'jpy'
    })
  ]
}

export default {
  name: 'CartBundledItemView',
  component: CartBundledItemView,
  properties: [
    {
      title: 'Default',
      property: BundleItemSample
    }
  ],
  frames: [
    {
      title: 'W600',
      style: {width: 600}
    },
    {
      title: 'W683',
      style: {width: 683}
    }
  ]
}

