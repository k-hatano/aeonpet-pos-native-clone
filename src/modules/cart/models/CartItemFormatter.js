import * as _ from 'underscore'

export const CART_LIST_ITEM_TYPE = {
  NORMAL: 1,
  BUNDLE: 2
}

export default class CartItemFormatter {
  format (cart) {
    const items = []
    cart.appliedBundles.forEach(bundle => {
      items.push({
        type: CART_LIST_ITEM_TYPE.BUNDLE,
        bundle,
        items: bundle.items.toArray()
      })
    })
    cart.items.forEach(item => {
      if (!item.isBundled) {
        items.push({
          type: CART_LIST_ITEM_TYPE.NORMAL,
          item
        })
      }
    })
    return items
  }
}
