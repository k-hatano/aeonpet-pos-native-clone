import CartBundle from '../CartBundle'
import { List } from 'immutable'
import Decimal from 'decimal.js'
import generateUuid from 'common/utils/generateUuid'
import { TAX_RULE } from 'common/models/Tax'
import CartItem from '../CartItem'
import CartItemProduct from '../CartItemProduct'
import { ESTABLISHMENT_CONDITION } from 'modules/promotion/models'
import { BUNDLE_TYPE } from '../../../promotion/models'

const defaultTax = { tax_rule: TAX_RULE.NONE, tax_rate: 0.08 }

const productTag1 = {
  id: generateUuid()
}

const productTag2 = {
  id: generateUuid()
}

const bundleA = {
  name: 'BundleA',
  bundle_type: BUNDLE_TYPE.FLATPRICE,
  bundle_riichi_message: 'BundleA Riichi',
  bundle_patterns: [
    {
      product_tags: [
        productTag1.id
      ],
      threshold_quantity: 3,
      establishment_condition: ESTABLISHMENT_CONDITION.EQUAL,
      bundle_price: 1000
    }
  ]
}

const bundleGte1 = {
  name: 'bundleGte1',
  bundle_type: BUNDLE_TYPE.FLATPRICE,
  bundle_riichi_message: 'bundleGte1 Riichi',
  bundle_patterns: [
    {
      product_tags: [
        productTag1.id
      ],
      threshold_quantity: 2,
      establishment_condition: ESTABLISHMENT_CONDITION.GREATER_EQUAL,
      bundle_price: 1000
    }
  ]
}

const productForAll1 = {
  product: {
    id: generateUuid(),
    ...defaultTax
  },
  variant: {
    price: 1000
  },
  tags: [
    productTag1
  ],
  bundleCandidates: [
    bundleA,
    bundleGte1
  ]
}

const productForAll2 = {
  product: {
    id: generateUuid(),
    ...defaultTax
  },
  variant: {
    price: 2000
  },
  tags: [
    productTag1
  ],
  bundleCandidates: [
    bundleA,
    bundleGte1
  ]
}

const product_1_1 = {
  product: {
    id: generateUuid(),
    ...defaultTax
  },
  variant: {
    price: 1000
  },
  tags: [
    productTag1
  ],
  bundleCandidates: [
    bundleA
  ]
}

const product_1_2 = {
  product: {
    id: generateUuid(),
    ...defaultTax
  },
  variant: {
    price: 1100
  },
  tags: [
    productTag1
  ],
  bundleCandidates: [
    bundleA
  ]
}

const createCartItemProduct = (sample) => {
  return new CartItemProduct(
    sample.product,
    sample.variant,
    sample.bundleCandidates,
    [],
    sample.tags
  )
}

const createCartItem = (productSample, quantity) => {
  return new CartItem({
    product: createCartItemProduct(productSample),
    quantity,
    currency: 'jpy'
  })
}

describe('Apply Bundle Test', () => {
  it ('Simple', () => {
    let items = new List([
      createCartItem(product_1_1, 3)
    ])
    const { bundles } = CartBundle.applyToCartItems(items)
    expect(bundles.count()).toBe(1)
  })

  it ('Simple Riich', () => {
    let items = new List([
      createCartItem(product_1_1, 2)
    ])
    const { bundles, riichiMessage } = CartBundle.applyToCartItems(items)
    expect(bundles.count()).toBe(0)
    expect(riichiMessage).toBe(bundleA.bundle_riichi_message)
  })

  it ('Price Order1', () => {
    let items = new List([
      createCartItem(product_1_1, 2),
      createCartItem(product_1_2, 2)
    ])
    const { unbundledItems } = CartBundle.applyToCartItems(items)
    expect(unbundledItems.get(0).product.product_id).toBe(product_1_1.id)
  })

  it ('Price Order2', () => {
    let items = new List([
      createCartItem(product_1_2, 2),
      createCartItem(product_1_1, 2)
    ])
    const { unbundledItems } = CartBundle.applyToCartItems(items)
    expect(unbundledItems.get(0).product.product_id).toBe(product_1_1.id)
  })

  it ('Two Apply', () => {
    let items = new List([
      createCartItem(product_1_1, 6)
    ])
    const { bundles } = CartBundle.applyToCartItems(items)
    expect(bundles.count()).toBe(2)
  })

  it ('Two Items', () => {
    let items = new List([
      createCartItem(product_1_1, 1),
      createCartItem(product_1_2, 2)
    ])
    const { bundles } = CartBundle.applyToCartItems(items)
    expect(bundles.count()).toBe(1)
  })

  it ('Two Items Two Apply', () => {
    let items = new List([
      createCartItem(product_1_1, 2),
      createCartItem(product_1_2, 4)
    ])
    const { bundles } = CartBundle.applyToCartItems(items)
    expect(bundles.count()).toBe(2)
  })

  describe ('For GREATER_EQUAL', () => {
    it ('Apply One Item & Just Count', () => {
      let items = new List([
        createCartItem(productForAll1, 2)
      ])
      const { bundles } = CartBundle.applyToCartItems(items)
      expect(bundles.count()).toBe(1)
    })

    it ('Apply One Item & Larger Count', () => {
      let items = new List([
        createCartItem(productForAll1, 3)
      ])
      const { bundles, unbundledItems } = CartBundle.applyToCartItems(items)
      expect(bundles.count()).toBe(1)
      expect(bundles.get(0).items.get(0).quantity).toBe(3)
      expect(unbundledItems.count()).toBe(0)
    })
  })

  describe ('_distributeDiscounts', () => {
    const createBundleDiscount = (price, quantity, sku) => {
      return {
        item: new CartItem({
          product: new CartItemProduct({}, {
            sku,
            price
          }),
          quantity,
          currency: 'jpy',
          instanceUniqueKey: CartItem.generateInstanceUniqueKey()
        })
      }
    }

    it('Simple', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1000, 1, '01'),
        createBundleDiscount(3000, 1, '10')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(2000))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(500))
      expect(bundleDiscounts.get(1).bundleDiscount).toEqual(Decimal(1500))
    })

    it('2Quantity', () => {
      let bundleDiscounts = List([
        createBundleDiscount(2000, 2, '01')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(2000))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(1000))
    })

    it('2Quantity and one quantity', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1000, 1, '01'),
        createBundleDiscount(2000, 2, '10')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(2000))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(400))
      expect(bundleDiscounts.get(1).bundleDiscount).toEqual(Decimal(800))
    })

    it('Surplus', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1000, 3, '01', 0)
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(1000))
      expect(bundleDiscounts.count()).toBe(1)
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(333))
      expect(bundleDiscounts.get(0).bundleSurplus).toEqual(Decimal(1))
    })

    // https://docs.google.com/spreadsheets/d/1nyNGMNxvR8m596PEcaw2iufHdZ77mBwg5uMFA22ilC0/edit#gid=1072453104
    // 価格が高いものにあまりをつける
    it('Designed Test Pattern 1', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1555, 1, '4999999999955'),
        createBundleDiscount(1550, 1, '4999999999944'),
        createBundleDiscount(1545, 1, '4999999999933')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(552 + 550 + 548))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(551))
      expect(bundleDiscounts.get(0).bundleSurplus).toEqual(Decimal(1))
      expect(bundleDiscounts.get(1).bundleDiscount).toEqual(Decimal(550))
      expect(bundleDiscounts.get(2).bundleDiscount).toEqual(Decimal(548))
    })

    /** 価格が同じであれば、JANコードの若い方にあまりをつける */
    it('Designed Test Pattern 2', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1555, 1, '4999999999955'),
        createBundleDiscount(1555, 1, '4999999999944'),
        createBundleDiscount(1545, 1, '4999999999933')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(355 + 355 + 1545))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(753))
      expect(bundleDiscounts.get(1).bundleDiscount).toEqual(Decimal(753))
      expect(bundleDiscounts.get(1).bundleSurplus).toEqual(Decimal(1))
      expect(bundleDiscounts.get(2).bundleDiscount).toEqual(Decimal(748))
    })

    it('Designed Test Pattern 3', () => {
      let bundleDiscounts = List([
        createBundleDiscount(1555, 1, '4999999999955'),
        createBundleDiscount(1550, 1, '4999999999944'),
        createBundleDiscount(1545, 1, '4999999999933')
      ])
      bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, Decimal(555 + 550 + 545))
      expect(bundleDiscounts.get(0).bundleDiscount).toEqual(Decimal(551))
      expect(bundleDiscounts.get(0).bundleSurplus).toEqual(Decimal(1))
      expect(bundleDiscounts.get(1).bundleDiscount).toEqual(Decimal(550))
      expect(bundleDiscounts.get(2).bundleDiscount).toEqual(Decimal(548))
    })
  })

  describe('_applyBundleDiscount', () => {
    const createCartItem = (price, quantity, articleNumber) => {
      return new CartItem({
        product: new CartItemProduct({}, {
          article_number: articleNumber,
          price
        }),
        quantity,
        currency: 'jpy',
        instanceUniqueKey: CartItem.generateInstanceUniqueKey()
      })
    }

    it ('Simple Flat', () => {
      const result = CartBundle._applyBundleDiscount({
          bundle_type: BUNDLE_TYPE.FLATPRICE
        },
        0,
        1000,
        List([
          createCartItem(1200, 1, '01'),
          createCartItem(1500, 1, '10')
        ])
      )
      expect(result.get(0).bundlePrice).toEqual(Decimal(1000))
      expect(result.get(1).bundlePrice).toEqual(Decimal(1000))
    })

    it ('Simple BuyX GetY', () => {
      const result = CartBundle._applyBundleDiscount({
          bundle_type: BUNDLE_TYPE.BUY_X_GET_Y
        },
        0,
        0,
        List([
          createCartItem(1200, 1, '01'),
          createCartItem(1500, 1, '10')
        ])
      )
      expect(result.get(1).bundlePrice).toEqual(Decimal(0))
      expect(result.get(0).bundlePrice).toEqual(Decimal(1500))
    })

    it ('XY same item', () => {
      const result = CartBundle._applyBundleDiscount({
          bundle_type: BUNDLE_TYPE.BUY_X_GET_Y
        },
        0,
        0,
        List([
          createCartItem(1000, 3, '01')
        ])
      )
      expect(result.count()).toBe(2)
      expect(result.get(0).bundlePrice).toEqual(Decimal(1000))
      expect(result.get(0).quantity).toBe(2)
      expect(result.get(1).bundlePrice).toEqual(Decimal(0))
      expect(result.get(1).quantity).toBe(1)
    })

    it ('Simple Selection', () => {
      const result = CartBundle._applyBundleDiscount({
          bundle_type: BUNDLE_TYPE.SELECTION
        },
        0,
        2000,
        List([
          createCartItem(1200, 1, '01'),
          createCartItem(1800, 1, '10')
        ])
      )
      expect(result.get(0).salesPrice).toEqual(Decimal(800))
      expect(result.get(1).salesPrice).toEqual(Decimal(1200))
    })

    it ('Selection remain', () => {
      const result = CartBundle._applyBundleDiscount({
          bundle_type: BUNDLE_TYPE.SELECTION
        },
        0,
        2000,
        List([
          createCartItem(1000, 3, '01')
        ])
      )
      expect(result.get(0).salesPrice).toEqual(Decimal(667))
      expect(result.get(0).salesTotal).toEqual(Decimal(2000))
    })
  })
})
