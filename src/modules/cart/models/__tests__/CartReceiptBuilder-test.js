import CartReceiptBuilder from '../CartReceiptBuilder'
import ReceiptTextConverter from '../../../printer/models/ReceiptTextConverter'
import {
  addProductToSampleCart, completeSampleCart, createSampleCart,
  payByPaymentMethodType
} from '../../../order/samples'
import { sampleProductMap } from '../../../product/samples'
import { PAYMENT_METHOD_TYPE } from '../../../payment/models'

import I18n from 'i18n-js'
import initialize from 'common/initialize'
import SampleShopRepository from '../../../shop/repositories/sample/SampleShopRepository'
import { shopIds } from '../../../shop/samples'
I18n.locale = 'ja'


describe('CartReceiptBuilder Test Sample', () => {

  const getCartReceiptText = async (cart) => {
    const builder = new CartReceiptBuilder()
    await builder.initializeAsync()
    const receipt = builder.buildCartReceipt(cart)
    return (new ReceiptTextConverter()).contentToText(receipt.content, {RECEIPT_LINE_LENGTH: 48})
  }

  it('Simple', async () => {
    const expectText = `
              OrangeShop 芝公園店
             東京都港区芝公園2-4-1
                  03-6430-6730
             営業時間 10：00-18：00
                        
         ご来店ありがとうございました。
       またのお越しをお待ちしております。
         クリスマスセール　全品20％OFF


テスト店舗A
取引ID:0001-0001000
2017-09-11 16:26:33
担当者:スタッフ１                               

             ----------------------

00000000001
S基本商品A
           ￥1,000       1点             ￥1,000

小計                     1点             ￥1,000
消費税                                      ￥80
             ----------------------

合計                                     ￥1,080
お預かり                                 ￥2,000
おつり                                     ￥920

現金                                     ￥1,080



       ご返品は未使用未洗濯でご購入日より
      １ヶ月以内にレシートをご持参ください

`

    let cart = createSampleCart(true)
    cart = await addProductToSampleCart(cart, sampleProductMap.standardA, 1)
    cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH, 2000)
    cart = completeSampleCart(cart, 1505114793 * 1000)
    const receiptText = await getCartReceiptText(cart)
    expect(receiptText).toBe(expectText)
  })

  it('With Bundle', async () => {
    const expectText = `
              OrangeShop 芝公園店
             東京都港区芝公園2-4-1
                  03-6430-6730
             営業時間 10：00-18：00
                        
         ご来店ありがとうございました。
       またのお越しをお待ちしております。
         クリスマスセール　全品20％OFF


テスト店舗A
取引ID:0001-0001000
2017-09-11 16:26:33
担当者:スタッフ１                               

             ----------------------

よりどり1
  00001000001
  Sよりどり1対象1
           ￥1,000       1点             ￥1,000
  00001000002
  Sよりどり1対象2
           ￥1,200       2点             ￥2,400
              バンドル値引き              -￥400

小計                     3点             ￥3,000
消費税                                     ￥240
             ----------------------

合計                                     ￥3,240
お預かり                                 ￥3,240
おつり                                       ￥0

現金                                     ￥3,240



       ご返品は未使用未洗濯でご購入日より
      １ヶ月以内にレシートをご持参ください

`

    let cart = createSampleCart(true)
    cart = await addProductToSampleCart(cart, sampleProductMap.bundle_s_1_1, 1)
    cart = await addProductToSampleCart(cart, sampleProductMap.bundle_s_1_2, 2)
    cart = payByPaymentMethodType(cart, PAYMENT_METHOD_TYPE.CASH)
    cart = completeSampleCart(cart, 1505114793 * 1000)
    const receiptText = await getCartReceiptText(cart)
    expect(receiptText).toBe(expectText)
  })
})
