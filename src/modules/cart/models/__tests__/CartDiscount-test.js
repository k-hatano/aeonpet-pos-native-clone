import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import CartDiscount from '../CartDiscount'
import { DISCOUNT_TYPE } from 'modules/promotion/models'

// TODO Uncomment following line if use db
// import Migrator from 'common/utils/migrator'

const discount100Yen = new CartDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(100)
})

const discount300Yen = new CartDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(300)
})

const discount10Percent = new CartDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.1)
})

const discount5Percent = new CartDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.05)
})

describe('CartDiscount Test', () => {
  it('discountAmount for amount discount', () => {
    const discountAmount = CartDiscount.discountAmount(discount100Yen, new Decimal(333), 'jpy')
    expect(discountAmount).toEqual(new Decimal(100))
  })
  it('discountAmount for amount discount', () => {
    const discountAmount = CartDiscount.discountAmount(discount10Percent, new Decimal(339), 'jpy')
    expect(discountAmount).toEqual(new Decimal(33))
  })
})
