import { TAX_RULE, TAXFREE_TYPE } from '../../../../common/models/Tax'

jest.mock('modules/promotion/repositories/PromotionRepository')
jest.mock('modules/product/repositories/ProductTagRepository')

import { List } from 'immutable'
import Decimal from 'decimal.js'
import CartItemTaxfree from '../CartItemTaxfree'
import { createCartItem, createCartItemProduct, taxExcludedFloor8, taxIncludedFloor8 } from '../../test/CartTestData'
import Tax from '../../../../common/models/Tax'

const findItemByOldItem = (items, oldItem) => {
  return items.find(item => item.product.productVariantId === oldItem.product.productVariantId)
}

describe('CartItemTaxfree Test', () => {
  describe('For tax excluded', () => {
    const tax = new Tax(taxExcludedFloor8)
    const product_1000yen_general =
      createCartItemProduct(1000, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })
    const product_999yen_general =
      createCartItemProduct(999, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })
    const product_2000yen_general =
      createCartItemProduct(2000, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })

    const product_1000yen_expendable =
      createCartItemProduct(1000, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.EXPENDABLE })
    const product_2000yen_expendable =
      createCartItemProduct(2000, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.EXPENDABLE })

    const product_1000yen_notapplicable =
      createCartItemProduct(1000, taxExcludedFloor8, { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE })

    it('Apply To General', () => {
      const items = List([
        createCartItem(product_1000yen_general, 5)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
    })

    it('Apply To Generals', () => {
      const items = List([
        createCartItem(product_1000yen_general, 3),
        createCartItem(product_2000yen_general, 1)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
      expect(applied.get(1).isTaxfreeApplied).toBe(true)
    })

    it('Do Not Apply To General', () => {
      const items = List([
        createCartItem(product_1000yen_general, 4),
        createCartItem(product_999yen_general, 1)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Apply To Expendable', () => {
      const items = List([
        createCartItem(product_1000yen_expendable, 5)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
    })

    it('Apply To Expendables', () => {
      const items = List([
        createCartItem(product_1000yen_expendable, 3),
        createCartItem(product_2000yen_expendable, 1)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
      expect(applied.get(1).isTaxfreeApplied).toBe(true)
    })

    it('Do Not Apply To Expendable', () => {
      const items = List([
        createCartItem(product_1000yen_expendable, 4)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Do Not Apply To Not Applicable', () => {
      const items = List([
        createCartItem(product_1000yen_notapplicable, 5)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Apply To General with others', () => {
      const general = createCartItem(product_1000yen_general, 5)
      const expendable = createCartItem(product_1000yen_expendable, 4)
      const notApplicable = createCartItem(product_1000yen_notapplicable, 5)
      const items = List([general, expendable, notApplicable])

      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(findItemByOldItem(applied, general).isTaxfreeApplied).toBe(true)
      expect(findItemByOldItem(applied, expendable).isTaxfreeApplied).toBe(false)
      expect(findItemByOldItem(applied, notApplicable).isTaxfreeApplied).toBe(false)
    })

    it('Apply To Expendable with others', () => {
      const general = createCartItem(product_1000yen_general, 4)
      const expendable = createCartItem(product_1000yen_expendable, 5)
      const notApplicable = createCartItem(product_1000yen_notapplicable, 5)
      const items = List([general, expendable, notApplicable])

      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(findItemByOldItem(applied, general).isTaxfreeApplied).toBe(false)
      expect(findItemByOldItem(applied, expendable).isTaxfreeApplied).toBe(true)
      expect(findItemByOldItem(applied, notApplicable).isTaxfreeApplied).toBe(false)
    })
  })

  describe('For tax included', () => {
    const tax = new Tax(taxIncludedFloor8)
    const product_1080yen_general =
      createCartItemProduct(1080, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })
    const product_1079yen_general =
      createCartItemProduct(1079, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })
    const product_2160yen_general =
      createCartItemProduct(2160, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.GENERAL })

    const product_1080yen_expendable =
      createCartItemProduct(1080, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.EXPENDABLE })
    const product_2160yen_expendable =
      createCartItemProduct(2160, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.EXPENDABLE })

    const product_1000yen_notapplicable =
      createCartItemProduct(1000, taxIncludedFloor8, { taxfree_type: TAXFREE_TYPE.NOT_APPLICABLE })

    it('Apply To General', () => {
      const items = List([
        createCartItem(product_1080yen_general, 5)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
    })

    it('Apply To Generals', () => {
      // 税額が切り捨てになる関係で、5,800円ではなく、5,799円以上の場合に免税適用となる
      const items = List([
        createCartItem(product_1080yen_general, 2),
        createCartItem(product_2160yen_general, 1),
        createCartItem(product_1079yen_general, 1)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
      expect(applied.get(1).isTaxfreeApplied).toBe(true)
      expect(applied.get(2).isTaxfreeApplied).toBe(true)
    })

    it('Do Not Apply To General', () => {
      const items = List([
        createCartItem(product_1080yen_general, 3),
        createCartItem(product_1079yen_general, 2)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Apply To Expendable', () => {
      const items = List([
        createCartItem(product_1080yen_expendable, 5)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
    })

    it('Apply To Expendables', () => {
      const items = List([
        createCartItem(product_1080yen_expendable, 3),
        createCartItem(product_2160yen_expendable, 1)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(true)
      expect(applied.get(1).isTaxfreeApplied).toBe(true)
    })

    it('Do Not Apply To Expendable', () => {
      const items = List([
        createCartItem(product_1080yen_expendable, 4)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Do Not Apply To Not Applicable', () => {
      const items = List([
        createCartItem(product_1000yen_notapplicable, 10)
      ])
      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(applied.get(0).isTaxfreeApplied).toBe(false)
    })

    it('Apply To General with others', () => {
      const general = createCartItem(product_1080yen_general, 5)
      const expendable = createCartItem(product_1080yen_expendable, 4)
      const notApplicable = createCartItem(product_1000yen_notapplicable, 10)
      const items = List([general, expendable, notApplicable])

      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(findItemByOldItem(applied, general).isTaxfreeApplied).toBe(true)
      expect(findItemByOldItem(applied, expendable).isTaxfreeApplied).toBe(false)
      expect(findItemByOldItem(applied, notApplicable).isTaxfreeApplied).toBe(false)
    })

    it('Apply To Expendable with others', () => {
      const general = createCartItem(product_1080yen_general, 4)
      const expendable = createCartItem(product_1080yen_expendable, 5)
      const notApplicable = createCartItem(product_1000yen_notapplicable, 10)
      const items = List([general, expendable, notApplicable])

      const applied = CartItemTaxfree.applyToItems(items, tax)
      expect(findItemByOldItem(applied, general).isTaxfreeApplied).toBe(false)
      expect(findItemByOldItem(applied, expendable).isTaxfreeApplied).toBe(true)
      expect(findItemByOldItem(applied, notApplicable).isTaxfreeApplied).toBe(false)
    })
  })
})
