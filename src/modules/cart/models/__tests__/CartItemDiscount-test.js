import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import CartItemDiscount from '../CartItemDiscount'
import { DISCOUNT_TYPE } from 'modules/promotion/models'
import { PROMOTION_TYPE } from '../../../promotion/models'

// TODO Uncomment following line if use db
// import Migrator from 'common/utils/migrator'

const discount100Yen = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(100)
})

const discount300Yen = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(300)
})

const discount10Percent = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.1)
})

const discount5Percent = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.05)
})

const saleDiscount1000Yen = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(1000),
  promotionType: PROMOTION_TYPE.SALE
})

const saleDiscount8Percent = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: Decimal(0.08),
  promotionType: PROMOTION_TYPE.SALE
})

const bundleDiscount300Yen = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(300),
  promotionType: PROMOTION_TYPE.BUNDLE
})

describe('CartItemDiscount Test', () => {
  describe('CartItemDiscount amountDiscountTotalAmount', () => {
    it('One Amount Discount', () => {
      const actual = CartItemDiscount.amountDiscountTotalAmount(List([
        discount100Yen
      ]))
      expect(actual).toEqual(Decimal(100))
    })

    it('Two Amount Discount', () => {
      const actual = CartItemDiscount.amountDiscountTotalAmount(List([
        discount100Yen,
        discount100Yen
      ]))
      expect(actual).toEqual(Decimal(200))
    })

    it('One Rate Discount', () => {
      const actual = CartItemDiscount.amountDiscountTotalAmount(List([
        discount10Percent
      ]))
      expect(actual).toEqual(Decimal(0))
    })

    it('Mixed Discount', () => {
      const actual = CartItemDiscount.amountDiscountTotalAmount(List([
        discount100Yen,
        discount10Percent
      ]))
      expect(actual).toEqual(Decimal(100))
    })
  })

  describe('CartItemDiscount rateDiscountTotalAmount', () => {
    it('One Amount Discount', () => {
      const actual = CartItemDiscount.rateDiscountTotalAmount(List([
        discount100Yen
      ]), Decimal(2000), 'jpy')
      expect(actual).toEqual(Decimal(0))
    })

    it('One Rate Discount', () => {
      const actual = CartItemDiscount.rateDiscountTotalAmount(List([
        discount10Percent
      ]), Decimal(2000), 'jpy')
      expect(actual).toEqual(Decimal(200))
    })

    it('Two Rate Discount', () => {
      const actual = CartItemDiscount.rateDiscountTotalAmount(List([
        discount10Percent,
        discount10Percent
      ]), Decimal(2000), 'jpy')
      expect(actual).toEqual(Decimal(400))
    })

    it('Mixed Discount', () => {
      const actual = CartItemDiscount.rateDiscountTotalAmount(List([
        discount100Yen,
        discount10Percent
      ]), Decimal(2000), 'jpy')
      expect(actual).toEqual(Decimal(200))
    })
  })

  describe('CartItemDiscount applyToDiscounts', () => {
    it ('New Discounts to Empty', () => {
      const old = List([])
      const applied = CartItemDiscount.applyToDiscounts(discount100Yen, old)
      expect(applied.count()).toBe(1)
    })

    it ('Overwrite Sale Discount', () => {
      const old = List([saleDiscount1000Yen])
      const applied = CartItemDiscount.applyToDiscounts(saleDiscount8Percent, old)
      expect(applied.count()).toBe(1)
      expect(applied.get(0)).toEqual(saleDiscount8Percent)
    })

    it ('Overwrite Manual Discount', () => {
      const old = List([discount10Percent])
      const applied = CartItemDiscount.applyToDiscounts(discount5Percent, old)
      expect(applied.count()).toBe(1)
      expect(applied.get(0)).toEqual(discount5Percent)
    })

    it ('Can Use Together', () => {
      const old = List([discount10Percent])
      const applied = CartItemDiscount.applyToDiscounts(saleDiscount8Percent, old)
      expect(applied.count()).toBe(2)
      expect(applied.get(0)).toEqual(saleDiscount8Percent)
      expect(applied.get(1)).toEqual(discount10Percent)
    })
  })

  describe('Discount Label', () => {
    it ('Item Rate Discount', () => {
      const discounts = List([discount5Percent])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('個 : -500円(5%)')
    })

    it ('Item Amount Discount', () => {
      const discounts = List([discount100Yen])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('個 : -100円')
    })

    it ('Sale Rate Discount', () => {
      const discounts = List([saleDiscount8Percent])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セール : -800円(8%)')
    })

    it ('Sale Amount Discount', () => {
      const discounts = List([saleDiscount1000Yen])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セール : -1,000円')
    })

    it ('Sale Amount And Item Amount', () => {
      const discounts = List([saleDiscount1000Yen, discount100Yen])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セール : -1,000円　　個 : -100円')
    })

    it ('Sale Rate And Item Amount', () => {
      const discounts = List([saleDiscount8Percent, discount100Yen])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セール : -800円(8%)　　個 : -100円')
    })

    it ('Sale Amount And Item Rate', () => {
      const discounts = List([saleDiscount1000Yen, discount5Percent])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セール : -1,000円　　個 : -500円(5%)')
    })

    it ('Sale Rate And Item Rate', () => {
      const discounts = List([saleDiscount8Percent, discount5Percent])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('セ/個 : -1,300円(13%)')
    })

    it ('Ignore Bundle Discount', () => {
      const discounts = List([bundleDiscount300Yen])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('')
    })

    it ('No Discounts', () => {
      const discounts = List([])
      const label = CartItemDiscount.discountsToLabel(discounts, Decimal(10000), 'jpy')
      expect(label).toBe('')
    })
  })
})
