// jest.mock('modules/xxxx/repositories/PromotionRepository')

import Cart from '../Cart'
import {TAX_RULE} from 'common/models/Tax'
import { sampleProductMap } from 'modules/product/samples'
import CartItem from '../CartItem'
import { createCartItem, defaultTax, simpleProduct } from '../../test/CartTestData'
import { List } from 'immutable'
import { DISCOUNT_TYPE } from '../../../promotion/models'
import generateUuid from '../../../../common/utils/generateUuid'
import { sampleBundleMap } from '../../../promotion/samples'

describe('Cart Test', () => {
  describe('Amount', () => {
    it('Initial', () => {
      const cart = new Cart()
      expect(cart.totalItemsTaxless.toFixed(4)).toBe("0.0000")
    })

    it('Single item', async () => {
      const cart = await (new Cart()).addProductAsync(defaultTax, { price: 100 }, 1)
      expect(cart.totalItemsTaxless.toFixed(4)).toBe("100.0000")
    })

    it('Single item quantity', async () => {
      const cart = await (new Cart()).addProductAsync(defaultTax, { price: 100 }, 3)
      expect(cart.totalItemsTaxless.toFixed(4)).toBe("300.0000")
    })
  })

  describe('Cart operation test', () => {
    it('Add bundle target product', async () => {
      const cart = await (new Cart()).addProductAsync({ id: sampleProductMap.bundle_s_1_1.id, ...defaultTax}, { price: 100 }, 3)
      expect(cart.items.get(0).product.bundleCandidates.count()).toBe(3)
      expect(cart.items.get(0).product.tags.count()).toBe(1)
    })

    it('Increase item quantity', async () => {
      const cart = await (new Cart()).addProductAsync(defaultTax, { price: 100 }, 1)
      const oldItem = cart.items.get(0)
      const increasedItem = oldItem.increase()
      const newCart = await cart.updateItemAsync(oldItem, increasedItem)
      expect(newCart.totalItemsTaxless.toFixed(4)).toBe("200.0000")
    })
  })

  describe('Cart discount test', () => {
  })

  describe('Bundle', () => {
    const bundleTargetAProduct = {
      id: sampleProductMap.bundle_s_2_A_1.id,
      ...defaultTax
    }

    const bundleTargetBProduct = {
      id: sampleProductMap.bundle_s_2_B_1.id,
      ...defaultTax
    }

    it ('Simple Applied Bundles', async () => {
      let cart = new Cart()
      cart = await cart.addProductAsync(bundleTargetAProduct, { id: generateUuid(), price: 100 }, 2)
      cart = await cart.addProductAsync(bundleTargetBProduct, { id: generateUuid(), price: 100 }, 3)
      const bundles = cart.appliedBundles
      expect(bundles.count()).toBe(1)
      expect(bundles.get(0).id).toBe(sampleBundleMap.selection2.id)
      expect(bundles.get(0).items.count()).toBe(2)
      expect(bundles.get(0).items.get(0).quantity).toBe(2)
      expect(bundles.get(0).items.get(1).quantity).toBe(3)
    })

    it ('Simple not Applied Bundles 1', async () => {
      let cart = new Cart()
      cart = await cart.addProductAsync(bundleTargetAProduct, { id: generateUuid(), price: 100 }, 2)
      cart = await cart.addProductAsync(bundleTargetBProduct, { id: generateUuid(), price: 100 }, 2)
      const bundles = cart.appliedBundles
      expect(bundles.count()).toBe(0)
    })

    it ('Simple not Applied Bundles 2', async () => {
      let cart = new Cart()
      cart = await cart.addProductAsync(bundleTargetAProduct, { id: generateUuid(), price: 100 }, 1)
      cart = await cart.addProductAsync(bundleTargetBProduct, { id: generateUuid(), price: 100 }, 3)
      const bundles = cart.appliedBundles
      expect(bundles.count()).toBe(0)
    })

    it ('Two Applied Bundles', async () => {
      let cart = new Cart()
      cart = await cart.addProductAsync(bundleTargetAProduct, { id: generateUuid(), price: 100 }, 4)
      cart = await cart.addProductAsync(bundleTargetBProduct, { id: generateUuid(), price: 100 }, 6)
      const bundles = cart.appliedBundles
      expect(bundles.count()).toBe(2)
    })
  })
})

