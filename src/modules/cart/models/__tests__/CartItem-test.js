jest.mock('modules/promotion/repositories/PromotionRepository')
jest.mock('modules/product/repositories/ProductTagRepository')

import { List } from 'immutable'
import Decimal from 'decimal.js'
import CartItem from '../CartItem'
import CartItemDiscount from '../CartItemDiscount'
import CartItemProduct from '../CartItemProduct'
import Tax, { TAX_RULE } from 'common/models/Tax'
import {
  createCartItem, simpleProduct, simpleProduct2, createCartItemProduct,
  taxExcludedFloor8, taxNone, taxIncludedFloor8
} from '../../test/CartTestData'
import { DISCOUNT_TYPE } from '../../../promotion/models'

const simpleCartItem = createCartItem(simpleProduct)

const product_1000yen_tax0 = createCartItemProduct(1000, taxNone)
const product_1000yen_extax8 = createCartItemProduct(1000, taxExcludedFloor8)
const product_1000yen_intax8 = createCartItemProduct(1000, taxIncludedFloor8)

const discount10percent = new CartItemDiscount({
  discountType: DISCOUNT_TYPE.RATE,
  value: 0.1
})

describe('CartItem Test', () => {
  describe('Calculation', () => {
    it('Default values', () => {
      const cartItem = createCartItem(product_1000yen_tax0)
      expect(simpleCartItem.quantity).toBe(1)
      expect(simpleCartItem.currency).toBe('jpy')
      expect(simpleCartItem.discountAmount).toEqual(new Decimal(0))
      expect(cartItem.listPrice).toEqual(new Decimal(1000))
      expect(cartItem.salesPrice).toEqual(new Decimal(1000))
    })

    it('Simple Rate Discount', () => {
      const cartItem = createCartItem(product_1000yen_extax8).applyDiscount(discount10percent)
      expect(cartItem.amountDiscountAmount).toEqual(Decimal(0))
      expect(cartItem.rateDiscountAmount).toEqual(Decimal(100))
      expect(cartItem.discountAmount).toEqual(new Decimal(100))
      expect(cartItem.listPrice).toEqual(Decimal(1000))
      expect(cartItem.salesPrice).toEqual(Decimal(900))
    })
  })

  describe('Operation', () => {
    it('Increase', () => {
      const cartItem = createCartItem(simpleProduct).increase()
      expect(cartItem.quantity).toBe(2)
    })

    it('Decrease', () => {
      const cartItem = createCartItem(simpleProduct).decrease()
      expect(cartItem.quantity).toBe(0)
    })

    describe('takeByQuantity', () => {
      it('quantity', () => {
        const cartItem = createCartItem(simpleProduct, 5)
        const { take, remain } = cartItem.takeByQuantity(2)
        expect(take.quantity).toBe(2)
        expect(remain.quantity).toBe(3)
      })

      it('uniqueKey', () => {
        const cartItem = createCartItem(simpleProduct, 5)
        const { take, remain } = cartItem.takeByQuantity(2)
        expect(take.instanceUniqueKey).not.toBe(cartItem.instanceUniqueKey)
        expect(remain.instanceUniqueKey).toBe(cartItem.instanceUniqueKey)
      })
    })
  })

  describe('Utility Function for Item List', () => {
    it('takeItemsByQuantity default', () => {
      const items = List([
        createCartItem(simpleProduct, 2),
        createCartItem(simpleProduct, 3),
        createCartItem(simpleProduct, 4)
      ])
      const [takes, remains] = CartItem.takeItemsByQuantity(items, 5)
      expect(takes.count()).toBe(2)
      expect(remains.count()).toBe(1)
      expect(takes.get(0).quantity).toBe(2)
      expect(remains.get(0).quantity).toBe(4)
    })

    it('takeItemsByQuantity split item', () => {
      const items = List([
        createCartItem(simpleProduct, 2),
        createCartItem(simpleProduct, 3)
      ])
      const [takes, remains] = CartItem.takeItemsByQuantity(items, 3)
      expect(takes.count()).toBe(2)
      expect(remains.count()).toBe(1)
      expect(takes.get(0).quantity).toBe(2)
      expect(takes.get(1).quantity).toBe(1)
      expect(remains.get(0).quantity).toBe(2)
    })
  })

  describe('Merge', () => {
    it ('Simple Merge', () => {
      const item1 = createCartItem(simpleProduct, 2)
      const item2 = createCartItem(simpleProduct, 3)
      const merged = item1.merge(item2)
      expect(merged.quantity).toBe(5)
    })

    // instanceUniqueKeyは古い方を採用すること
    it ('Simple Merge key', () => {
      const item1 = createCartItem(simpleProduct, 2)
      const item2 = createCartItem(simpleProduct, 3)
      expect(item1.merge(item2).instanceUniqueKey).toBe(item1.instanceUniqueKey)
      expect(item2.merge(item1).instanceUniqueKey).toBe(item1.instanceUniqueKey)
    })

    it ('Compact 2 Items', async () => {
      const item1 = createCartItem(simpleProduct, 2)
      const item2 = createCartItem(simpleProduct, 3)
      const items = CartItem.compactItems(List([item1, item2]))
      expect(items.count()).toBe(1)
      expect(items.get(0).quantity).toBe(5)
    })

    it ('Compact 3 Items into 2', async () => {
      const item1 = createCartItem(simpleProduct, 2)
      const item2 = createCartItem(simpleProduct2, 11)
      const item3 = createCartItem(simpleProduct, 3)
      const items = CartItem.compactItems(List([item1, item2, item3]))
      expect(items.count()).toBe(2)
      expect(items.get(0).quantity).toBe(5)
    })

    it ('Compact 3 Items into 1 item', async () => {
      const item1 = createCartItem(simpleProduct, 2)
      const item2 = createCartItem(simpleProduct, 4)
      const item3 = createCartItem(simpleProduct, 3)
      const items = CartItem.compactItems(List([item1, item2, item3]))
      expect(items.count()).toBe(1)
      expect(items.get(0).quantity).toBe(9)
    })
  })
})
