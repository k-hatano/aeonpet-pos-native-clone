// import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import logger from 'common/utils/logger'
import { TAX_RULE, TAXFREE_TYPE, TAX_CODE } from '../../../common/models/Tax'
import { TAXFREE_MAX_THRESHOLD_FOR_EXPENDABLE, TAXFREE_THRESHOLD, MAX_SUBTOTAL_AMOUNT } from '../models'

export default class CartItemTaxfree {
  /**
   * 一般品の合計金額(税抜)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static generalsTotalTaxless (items, tax) {
    return this.standardGeneralsTotalTaxless(items, tax).plus(this.reducedGeneralsTotalTaxless(items, tax))
  }

  /**
   * 標準税率対象の一般品の合計金額(税抜)
   */
  static standardGeneralsTotalTaxless (items, tax) {
    return this.standardSalesTotalTaxless(items, tax, TAXFREE_TYPE.GENERAL)
  }

  /**
   * 軽減税率対象の一般品の合計金額(税抜)
   */
  static reducedGeneralsTotalTaxless (items, tax) {
    return this.reducedSalesTotalTaxless(items, tax, TAXFREE_TYPE.GENERAL)
  }

  /**
   * 消耗品の合計金額(税抜)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static expendablesTotalTaxless (items, tax) {
    return this.standardExpendablesTotalTaxless(items, tax).plus(this.reducedExpendablesTotalTaxless(items, tax))
  }

  /**
   * 標準税率対象の一般品の合計金額(税抜)
   */
  static standardExpendablesTotalTaxless (items, tax) {
    return this.standardSalesTotalTaxless(items, tax, TAXFREE_TYPE.EXPENDABLE)
  }

  /**
   * 軽減税率対象の一般品の合計金額(税抜)
   */
  static reducedExpendablesTotalTaxless (items, tax) {
    return this.reducedSalesTotalTaxless(items, tax, TAXFREE_TYPE.EXPENDABLE)
  }

  /**
   * 免税対象外の合計金額(税抜)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static notApplicablesTotalTaxless (items, tax) {
    return this.standardNotApplicablesTotalTaxless(items, tax).plus(this.reducedNotApplicablesTotalTaxless(items, tax))
  }

  /**
   * 標準税率対象の免税対象外の合計金額(税抜)
   */
  static standardNotApplicablesTotalTaxless (items, tax) {
    return this.standardSalesTotalTaxless(items, tax, TAXFREE_TYPE.NOT_APPLICABLE)
  }

  /**
   * 軽減税率対象の免税対象外の合計金額(税抜)
   */
  static reducedNotApplicablesTotalTaxless (items, tax) {
    return this.reducedSalesTotalTaxless(items, tax, TAXFREE_TYPE.NOT_APPLICABLE)
  }

  /**
   * 一般品の合計金額(税額)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static generalsTotalTax (items, tax) {
    return this.standardGeneralsTotalTax(items, tax).plus(this.reducedGeneralsTotalTax(items, tax))
  }

  /**
   * 標準税率対象の一般品の合計金額(税額)
   */
  static standardGeneralsTotalTax (items, tax) {
    return this.standardTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.GENERAL)
  }

  /**
   * 軽減税率対象の一般品の合計金額(税額)
   */
  static reducedGeneralsTotalTax (items, tax) {
    return this.reducedTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.GENERAL)
  }

  /**
   * 消耗品の合計金額(税額)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static expendablesTotalTax (items, tax) {
    return this.standardExpendablesTotalTax(items, tax).plus(this.reducedExpendablesTotalTax(items, tax))
  }

  /**
   * 標準税率対象の消耗品の合計金額(税額)
   */
  static standardExpendablesTotalTax (items, tax) {
    return this.standardTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.EXPENDABLE)
  }

  /**
   * 軽減税率対象の消耗品の合計金額(税額)
   */
  static reducedExpendablesTotalTax (items, tax) {
    return this.reducedTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.EXPENDABLE)
  }

  /**
   * 免税対象外の合計金額(税額)
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {Decimal}
   */
  static notApplicablesTotalTax (items, tax) {
    return this.standardNotApplicablesTotalTax(items, tax).plus(this.reducedNotApplicablesTotalTax(items, tax))
  }

  /**
   * 標準税率対象の免税対象外の合計金額(税額)
   */
  static standardNotApplicablesTotalTax (items, tax) {
    return this.standardTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.NOT_APPLICABLE)
  }

  /**
   * 軽減税率対象の免税対象外の合計金額(税額)
   */
  static reducedNotApplicablesTotalTax (items, tax) {
    return this.reducedTotalTaxByTaxfreeType(items, tax, TAXFREE_TYPE.NOT_APPLICABLE)
  }

  static firstGeneral (items) {
    return items.find(item => item.product.taxfreeType === TAXFREE_TYPE.GENERAL)
  }

  static firstExpendable (items) {
    return items.find(item => item.product.taxfreeType === TAXFREE_TYPE.EXPENDABLE)
  }

  static firstNotApplicable (items) {
    return items.find(item => item.product.taxfreeType === TAXFREE_TYPE.NOT_APPLICABLE)
  }

  /**
   * 標準税率対象の免税種類別の合計税額を計算する
   */
  static standardTotalTaxByTaxfreeType (items, tax, taxfreeType) {
    if (items.count() === 0) {
      return Decimal(0)
    }
    const standardSalesTotal = this.standardSalesTotal(items, taxfreeType)
    return tax.calculateStandardTax(standardSalesTotal, items.first().currency)
  }

  /**
   * 軽減税率対象の免税種類別の合計税額を計算する
   */
  static reducedTotalTaxByTaxfreeType (items, tax, taxfreeType) {
    if (items.count() === 0) {
      return Decimal(0)
    }
    const reducedSalesTotal = this.reducedSalesTotal(items, taxfreeType)
    return tax.calculateReducedTax(reducedSalesTotal, items.first().currency)
  }

  // 免税種類別の標準税率対象小計
  static standardSalesTotal (items, taxfreeType) {
    return items
      .filter(item => item.product.taxfreeType === taxfreeType)
      .filter(item => item.product.tax.taxRule !== TAX_RULE.NONE)
      .filter(item => item.product.taxCode === TAX_CODE.STANDARD)
      .reduce((sum, item) => {
        return sum.add(item.salesTotal)
      }, new Decimal(0))
  }

  // 免税種類別の軽減税率対象小計
  static reducedSalesTotal (items, taxfreeType) {
    return items
      .filter(item => item.product.taxfreeType === taxfreeType)
      .filter(item => item.product.tax.taxRule !== TAX_RULE.NONE)
      .filter(item => item.product.taxCode === TAX_CODE.REDUCED)
      .reduce((sum, item) => {
        return sum.add(item.salesTotal)
      }, new Decimal(0))
  }

  // 免税種類別の標準税率対象小計(税抜)
  static standardSalesTotalTaxless (items, tax, taxfreeType) {
    if (items.count() === 0) {
      return Decimal(0)
    }
    const standardSalesTotal = this.standardSalesTotal(items, taxfreeType)
    return tax.standardCalculateTaxless(standardSalesTotal, items.first().currency)
  }

  // 免税種類別の軽減税率対象小計(税抜)
  static reducedSalesTotalTaxless (items, tax, taxfreeType) {
    if (items.count() === 0) {
      return Decimal(0)
    }
    const reducedSalesTotal = this.reducedSalesTotal(items, taxfreeType)
    return tax.reducedCalculateTaxless(reducedSalesTotal, items.first().currency)
  }

  /**
   * CartItem一覧に免税を適用したものを返します。
   * @param {List<CartItem>} items
   * @param {Tax} tax
   * @return {List<CartItem>}
   */
  static applyToItems (items, tax) {
    let generals = items.filter(item => item.product.taxfreeType === TAXFREE_TYPE.GENERAL)
    let expendables = items.filter(item => item.product.taxfreeType === TAXFREE_TYPE.EXPENDABLE)
    let notApplicables = items.filter(item => item.product.taxfreeType === TAXFREE_TYPE.NOT_APPLICABLE)
    if (generals.count() + expendables.count() + notApplicables.count() !== items.count()) {
      console.log(generals.toJSON(), expendables.toJSON(), notApplicables.toJSON(), items.toJSON())
      logger.fatal('CartItemTaxfree.applyTaxfree Contains invalid taxfreeType')
    }
    let upperLimitAmount = MAX_SUBTOTAL_AMOUNT
    const generalsTotalTaxless = this.generalsTotalTaxless(generals, tax)
    const expendablesTotalTaxless = this.expendablesTotalTaxless(expendables, tax)
    // 消耗品が会計される場合、上限値設定
    if (!expendablesTotalTaxless.equals(Decimal(0))) upperLimitAmount = TAXFREE_MAX_THRESHOLD_FOR_EXPENDABLE

    const generalsExpendablesTotalTaxless = generalsTotalTaxless.add(expendablesTotalTaxless)
    const isTaxfreeApply = generalsExpendablesTotalTaxless.gte(TAXFREE_THRESHOLD) &&
      generalsExpendablesTotalTaxless.lte(upperLimitAmount)
    generals = generals.map(item => item.setIsTaxfreeApplied(isTaxfreeApply))
    expendables = expendables.map(item => item.setIsTaxfreeApplied(isTaxfreeApply))
    notApplicables = notApplicables.map(item => item.setIsTaxfreeApplied(false))

    return generals.concat(expendables).concat(notApplicables)
  }
}
