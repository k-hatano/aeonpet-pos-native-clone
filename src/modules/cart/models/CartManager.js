import I18n from 'i18n-js'
import * as _ from 'underscore'
import { ALERT_THRESHOLD_FOR_TAXFREE_GENERAL, MAX_CART_ITEM_QUANTITY, MAX_SUBTOTAL_AMOUNT } from '../models'
import CartItemTaxfree from './CartItemTaxfree'
import StoreAccessibleBase from '../../../common/models/StoreAccessibleBase'
import PaymentMethodRepository from '../../payment/repositories/PaymentMethodRepository'
import SettingKeys from '../../setting/models/SettingKeys'
import logger from 'common/utils/logger'
import { formatNumber } from '../../../common/utils/formats'
import generateUuid from '../../../common/utils/generateUuid'
import Cart from './Cart'
import Tax from '../../../common/models/Tax'
import CashierRepository from '../../cashier/repositories/CashierRepository'
import ShopRepository from '../../shop/repositories/ShopRepository'
import AlertView from '../../../common/components/widgets/AlertView'
import PaymentServiceManager from 'modules/payment/models/PaymentServiceManager'
import PaymentServiceError from '../../payment/errors/PaymentServiceError'
import CartDataConverter from './CartDataConverter'
import OrderRepository from '../../order/repositories/OrderRepository'
import CartReceiptBuilder from './CartReceiptBuilder'
import { proceedUpdateCart, makeOrderEJournal } from '../services'
import EJournalRepository from '../../printer/repositories/EJournalRepository'
import NetworkError from '../../../common/errors/NetworkError'
import PrinterManager from 'modules/printer/models/PrinterManager'
import { saveOperationLog } from '../../home/service'
import { OPERATION_TYPE } from '../../home/models'
import CreditReceiptBuilder from '../../order/models/CreditReceiptBuilder'
import { makeCreditEjournal, makeCreditReturnEjournal } from '../../order/services'
import CustomerRepository from '../../customer/repositories/CustomerRepository'
import ProductRepository from '../../product/repositories/ProductRepository'
import UnassociatedReturnCart from './UnassociatedReturnCart'
import CartPayment from './CartPayment'
import { PAYMENT_METHOD_TYPE, isNotIgnorePayment } from 'modules/payment/models'
import PaymentRepository from 'modules/payment/repositories/PaymentRepository'
import OrderReturnRepository from 'modules/order/repositories/OrderReturnRepository'
import { loading } from 'common/sideEffects'
import { makeReturnEjournal, makeProcessName, STATUS } from 'modules/order/models'
import OrderedCart from 'modules/order/models/OrderedCart'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import { STOCK_MODE } from 'modules/product/models'
import { sequelize } from 'common/DB'
import ReceiptForShopBuilder from '../../order/models/ReceiptForShopBuilder'
import { RECEIPT_TYPE } from '../../printer/models'
import { getCommonNetworkErrorMessage } from '../../../common/errors'
import { waitAsync } from '../../../common/utils/waitAsync'
import DeviceLogRepository from '../../log/repositories/DeviceLogRepository'
import PromotionRepository from '../../promotion/repositories/PromotionRepository'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { POINT_TYPE } from 'modules/promotion/models'
import Decimal from 'decimal.js'
import CashChanger from 'modules/printer/models/CashChanger'
import { isPaperNearEnd, isPaperEnd } from 'modules/printer/models/PrinterResult'

class CartOperationResult {
  _messages = []
  _canContinue = false
  _cart = null
  _ejournal = null
  _receipt = null
  _creditEjournal = null
  _creditReceipt = null
  _isCreditReceiptPrinted = false

  constructor ({ cart, canContinue, messages, ejournal, receipt, creditEjournal, creditReceipt, isCreditReceiptPrinted }) {
    this._cart = cart
    this._messages = messages
    this._canContinue = canContinue
    this._ejournal = ejournal
    this._receipt = receipt
    this._creditEjournal = creditEjournal
    this._creditReceipt = creditReceipt
    this._isCreditReceiptPrinted = isCreditReceiptPrinted
  }

  static unexpectedError (cart) {
    return new CartOperationResult({
      cart,
      messages: [I18n.t('errors.unexpected_error')],
      canContinue: false
    })
  }

  static append (lastResult, newResult) {
    return new CartOperationResult({
      cart: newResult.cart || lastResult.cart,
      canContinue: lastResult.canContinue && newResult.canContinue,
      messages: lastResult.messages.concat(newResult.messages || []),
      ejournal: newResult.ejournal || lastResult.ejournal,
      receipt: newResult.receipt || lastResult.receipt,
      creditEjournal: newResult.creditEjournal || lastResult.creditEjournal,
      creditReceipt: newResult.creditReceipt || lastResult.creditReceipt,
      isCreditReceiptPrinted: newResult.isCreditReceiptPrinted || lastResult.isCreditReceiptPrinted
    })
  }

  get cart () { return this._cart }
  get messages () { return this._messages }
  get canContinue () { return this._canContinue }
  get ejournal () { return this._ejournal }
  get receipt () { return this._receipt }
  get creditEjournal () { return this._creditEjournal }
  get creditReceipt () { return this._creditReceipt }
  get isCreditReceiptPrinted () { return this._isCreditReceiptPrinted }
}

export default class CartManager extends StoreAccessibleBase {
  _paymentServiceManager = new PaymentServiceManager()

  /**
   *
   * @return {Promise.<Cart>}
   */
  async createInitialCartAsync (mode) {
    try {
      let cart
      const paymentMethods = await PaymentMethodRepository.findAll()
      const shop = await ShopRepository.find()
      const shopPointRatio = await PromotionRepository.findTodaysShopPointRatio(POINT_TYPE.AEON_PET_POINT)
      shop.point_rate = Decimal(1)
      if (shopPointRatio !== null) {
        shop.point_rate = shopPointRatio.assigned_ratio
      }
      const cartParams = {
        tax: new Tax({ tax_rule: this._taxRule, tax_rate: this._taxRate }),
        id: generateUuid(),
        cashier: await CashierRepository.find(),
        shop: shop
      }
      switch (mode) {
        case 'order':
          cart = new Cart(cartParams)
          cart = await this._initializePaymentMethods(cart)
          break

        case 'unassociated_return':
          cart = new UnassociatedReturnCart(cartParams)
          paymentMethods.forEach(paymentMethod => {
            if (paymentMethod.payment_method_type !== PAYMENT_METHOD_TYPE.CREDIT &&
              paymentMethod.payment_method_type !== PAYMENT_METHOD_TYPE.EMONEY &&
              paymentMethod.payment_method_type !== PAYMENT_METHOD_TYPE.UNION_PAY &&
              paymentMethod.payment_method_type !== PAYMENT_METHOD_TYPE.INTERNAL_POINT &&
              isNotIgnorePayment(paymentMethod.payment_method_type)) {
              cart = cart.addPayment(CartPayment.createFromPaymentMethodEntity(paymentMethod))
            }
          })
          break
      }
      return cart
    } catch (error) {
      logger.error('CartManager.createInitialCartAsync ' + error.toString())
      await AlertView.showAsync(I18n.t('errors.unexpected_error'))
    }
  }

  /**
   *
   * @return {Promise.<Cart>}
   */
  async initializeCartWithPendedOrder (pendedOrder) {
    let cart = null
    let messages = []
    try {
      let pendedCart = new OrderedCart(pendedOrder)
      const cartParams = {
        tax: new Tax({ tax_rule: this._taxRule, tax_rate: this._taxRate }),
        cashier: await CashierRepository.find(),
        shop: await ShopRepository.find()
      }
      cartParams.shop.point_rate = Decimal(1)

      const shopPointRatio = await PromotionRepository.findTodaysShopPointRatio(POINT_TYPE.AEON_PET_POINT)
      if (shopPointRatio !== null) {
        cartParams.shop.point_rate = shopPointRatio.assigned_ratio
      }
      pendedCart = await pendedCart.toCart(cartParams)
      cart = await this._initializePaymentMethods(pendedCart)
      if (pendedOrder.customer_code) {
        try {
          const customer = await CustomerRepository.fetchByCustomerCode(
            pendedOrder.customer_code)
          if (customer) {
            cart = cart.setCustomer(customer)
          } else {
            messages.push(I18n.t('message.B-07-E001'))
            cart = cart.setCustomer(null)
          }
        } catch (error) {
          messages.push(I18n.t('message.B-07-E001'))
          cart = cart.setCustomer(null)
        }
      } else {
        cart = cart.setCustomer(null)
      }
      return {
        cart,
        messages
      }
    } catch (error) {
      logger.error('CartManager.initializeCartWithPendedOrder ' + error.toString())
      messages.push(I18n.t('errors.unexpected_error'))
      return {
        cart: new Cart(),
        messages
      }
    }
  }

  /**
   *
   * @return {Promise.<Cart>}
   */
  async _initializePaymentMethods (cart) {
    cart = cart.initializePayments()
    const paymentMethods = await PaymentMethodRepository.findAll()
    const waonPointRatio = await PromotionRepository.findTodaysShopPointRatio(POINT_TYPE.WAON_POINT)
    const onlineCreditMethods = []
    const payments = []
    paymentMethods.forEach(paymentMethod => {
      if (paymentMethod.is_offline === 0 && paymentMethod.payment_method_type === PAYMENT_METHOD_TYPE.CREDIT) {
        onlineCreditMethods.push(paymentMethod)
      } else if (paymentMethod.payment_method_type === PAYMENT_METHOD_TYPE.EMONEY) {
        if (waonPointRatio) {
          payments.push(CartPayment.createFromPaymentMethodEntity(paymentMethod, waonPointRatio.assigned_ratio))
        } else {
          payments.push(CartPayment.createFromPaymentMethodEntity(paymentMethod, 0))
        }
      } else if (isNotIgnorePayment(paymentMethod.payment_method_type)) {
        payments.push(CartPayment.createFromPaymentMethodEntity(paymentMethod))
      }
    })
    if (onlineCreditMethods.length > 0) {
      payments.push(CartPayment.createFromCreditPaymentMethods(onlineCreditMethods))
    }
    _.sortBy(payments, payment => payment.sortOrder).forEach(payment => {
      cart = cart.addPayment(payment)
    })
    return cart
  }

  get _taxRule () {
    const rule = this.getSetting(SettingKeys.ORDER.TAX.RULE)
    logger.debug('Use tax rule ' + rule)
    return rule
  }

  get _taxRate () {
    const rate = this.getSetting(SettingKeys.ORDER.TAX.RATE)
    logger.debug('Use tax rate ' + rate)
    return rate
  }

  get _staff () {
    return this.getSetting(SettingKeys.STAFF.CURRENT_STAFF)
  }

  /**
   *
   * @param {Cart} oldCart
   * @param {Cart} newCart
   * @returns {{isValid: boolean, messages: Array}}
   */
  checkUpdatedCart (oldCart, newCart) {
    // TODO 翻訳対応
    let isValid = true
    let messages = []

    if (newCart.totalQuantity > MAX_CART_ITEM_QUANTITY || newCart.totalItemsBase.gt(MAX_SUBTOTAL_AMOUNT)) {
      isValid = false
      messages.push('購入限度を超えました。')
    }

    const newGeneralsTotal = newCart.isTaxfree
      ? CartItemTaxfree.generalsTotalTaxless(newCart.items, newCart.tax).toNumber() : 0
    const oldGeneralsTotal = oldCart.isTaxfree
      ? CartItemTaxfree.generalsTotalTaxless(oldCart.items, oldCart.tax).toNumber() : 0
    if (newGeneralsTotal >= ALERT_THRESHOLD_FOR_TAXFREE_GENERAL &&
      oldGeneralsTotal < ALERT_THRESHOLD_FOR_TAXFREE_GENERAL) {
      messages.push('一般品の購入額が100万円を超えました。パスポートの写しを保管してください')
    }

    if (oldCart.usePoint > 0 && newCart.usePoint === 0 &&
      newCart.discount && !newCart.discount.equals(oldCart.discount)) {
      // 小計値引き起因で利用ポイントがリセットされた場合の警告
      messages.push('ポイント利用がリセットされました')
    }

    if (newCart.totalTaxed.lt(0) || newCart.totalTax.lt(0) ||
      newCart.items.some(item => item.salesPrice.lt(0))) {
      isValid = false
      messages.push('割引、値引きが上限を超えています。')
    }
    if (isValid) {
      const surplus = newCart.deposit.sub(newCart.totalTaxed)
      const checkUpdatedCartPayment = CartPayment.checkUpdatedCartPayment(
        oldCart.activePayments, newCart.activePayments, surplus)
      isValid = checkUpdatedCartPayment.isValid
      messages = messages.concat(checkUpdatedCartPayment.messages)
    }

    return {
      isValid,
      messages
    }
  }

  /**
   * 会計データの整合性チェックを行う  
   * ・免税会計チェック  
   * ・会員チェック  
   * ・会計データ保存ダミー処理
   * @param {Cart} cart 
   * @param {CartOperationResult} cartOrderProperties 
   */
  async depositAsync (cart, cartOrderProperties) {
    if (this._isTaxfreeOrder(cart)) {
      return new CartOperationResult({
        cart,
        canContinue: false,
        messages: [I18n.t('message.B-06-E003')]
      })
    }

    if (cart.usePoint > 0) {
      try {
        await CustomerRepository.fetchByCustomerCode(cart.customer.customer_code)
      } catch (error) {
        const message = error.type === NetworkError.TYPE.OFFLINE
          ? I18n.t('message.A-01-E100') : I18n.t('message.B-01-E013')
        return new CartOperationResult({
          cart,
          canContinue: false,
          messages: [message]
        })
      }
    }

    const orderNumberSequence = this._generateOrderNumberSequence(cart.isTraining)
    // 取引IDの重複を防ぐため、お預かりボタンを押した直後に保存するように変更する
    this._saveOrderNumberSequence(orderNumberSequence, cart.isTraining)
    try {
      // 決済連携後に変換失敗することがあったので、念のため決済前に変換可能であることを確かめる。変換結果は特に利用しない。
      let dummyCart = cart.complete(this._staff, orderNumberSequence, cartOrderProperties, new Date())
      dummyCart = dummyCart.assignSubTotalDiscountToItemForOrder()
      const dummyConverter = new CartDataConverter(dummyCart)
      dummyConverter.convert()
    } catch (error) {
      logger.error('CartManager.depositAsync unexpected error on pre convert cart data ' + error)
      return new CartOperationResult({
        cart,
        canContinue: false,
        messages: [I18n.t('message.B-01-E014')]
      })
    }

    return new CartOperationResult({ cart: cart, canContinue: true })
  }

  async completeAsync (cart, cartOrderProperties) {
    const orderNumberSequence = this.getSetting(SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE)
    cart = cart.complete(this._staff, orderNumberSequence, cartOrderProperties, new Date())

    // NOTE: 決済端末側で伝票印刷を行うため無効化
    // if (!cart.isTraining) {
    //   result = CartOperationResult.append(result, await this._makeCreditReceiptAsync(cart))
    // }
    let result = new CartOperationResult({
      cart,
      canContinue: true,
      messages: []
    })
    try {
      await sequelize.transaction(async (transaction) => {
        result = CartOperationResult.append(result, await this._updateQuantity(cart, transaction))
        if (!result.canContinue) {
          throw new Error()
        }

        result = CartOperationResult.append(result, await this._sendOrderAsync(cart, transaction))
        result = CartOperationResult.append(result, await this._printOrderReceiptAsync(result))
        // NOTE: 決済端末側で伝票印刷を行うため無効化
        // result = CartOperationResult.append(result, await this._refundCreditPayment(result))
        if (!result.canContinue) {
          throw new Error()
        }
      })
    } catch (error) {
      // エラー内容はresultに記録されているので、ここでは何もしない。
    }

    return result
  }

  async returnOrder (dispatch, selectedOrder, isTrainingMode = false) {
    let order = null
    let orderedCart = null
    let returnOrder = null
    let returnedCart = null
    let returnCashAmount = 0
    let needsDispenseChange = false
    let newOrderNumberSequence = 0
    let creditEjournal = null
    let creditAmount = new Decimal(0)
    const messages = []

    let result = await loading(dispatch, async () => {
      try {
        order = await OrderRepository.fetchByOrderId(selectedOrder.id)
        if (!isTrainingMode) {
          const promotionOrders = await OrderRepository.fetchPromotionOrderByOrderId(order.id)
          const promotionOrderItems = await OrderRepository.fetchPromotionOrderItemsByOrderId(order.id)
          let payments = await PaymentRepository.fetchByOrderId(order.id)
          let paymentIdMaps = {}
          // payments と order_payments の並び順は一致しないので、Id で Map にして突合する。
          for (let i = 0; i < order.order_payments.length; i++) {
            paymentIdMaps[order.order_payments[i].payment_method_id] = order.order_payments[i].payment_method_name
          }
          for (let i = 0; i < payments.length; i++) {
            payments[i].payment_method_name = paymentIdMaps[payments[i].payment_method_id]
          }
          order.promotion_orders = promotionOrders
          order.promotion_order_items = promotionOrderItems
          order.payments = payments
          if (!order || !payments) {
            return {
              returnedOrder: false,
              messages: [I18n.t('message.F-01-E001')]
            }
          }
        }
        if (order.status === STATUS.RETURNED || order.status === STATUS.CANCELED) {
          return {
            returnedOrder: false,
            messages: [I18n.t('message.F-02-E004', {process: makeProcessName(order)})]
          }
        }
      } catch (error) {
        return {
          returnedOrder: false,
          messages: [I18n.t('message.F-01-E002', {
            process: I18n.t('order.process_returned'),
            error: error.message
          })]
        }
      }
      orderedCart = new OrderedCart(order)
      // 現金で返金するのはクレジットとイオンペットポイント以外
      returnCashAmount = orderedCart.activePayments
        .filter(payment => !payment.isCreditPaid)
        .reduce((sum, payment) => sum + payment.amount.toNumber(), 0) - orderedCart.change - orderedCart.usePoint
      needsDispenseChange = (CashChanger.isEnabled && returnCashAmount > 0 && !isTrainingMode)
      
      if (needsDispenseChange) {
        const checkResult = await loading(dispatch, async () => {
          try {
            await CashChanger.checkCashierStatus()
          } catch (error) {
            return error.message
          }
        })
        if (checkResult) {
          return {
            returnedOrder: false,
            messages: [checkResult],
            isUserCancel: true
          }
        }
      }

      try {
        // 取引IDの重複を防ぐため、お預かりボタンを押した直後に保存するように変更する
        newOrderNumberSequence = this._generateOrderNumberSequence(isTrainingMode)
        this._saveOrderNumberSequence(newOrderNumberSequence, isTrainingMode)

        const currentStaff = this._staff
        const shop = await ShopRepository.find()
        const cashier = await CashierRepository.find()
        orderedCart = orderedCart.setIsTrainingMode(isTrainingMode)
        orderedCart = orderedCart.createReturn(
          newOrderNumberSequence,
          currentStaff,
          cashier,
          shop
        )

        if (order.customer_code) {
          const customer = await CustomerRepository.fetchByCustomerCode(order.customer_code)
          if (customer) {
            orderedCart = orderedCart.setCustomer(customer)
          }
        }
      } catch (error) {
        logger.fatal('returnOrder unexpected error ' + error.message)
        return {
          returnedOrder: false,
          messages: [I18n.t('errors.unexpected_error')]
        }
      }

      if (!isTrainingMode) {
        try {
          const payments = orderedCart.activePayments.toArray()
          for (let key of Object.keys(payments)) {
            if (payments[key].isCreditPaid) {
              payments[key].extra_data = JSON.parse(order.extra_data).vega
              creditAmount = creditAmount.add(payments[key].amount)
            }
            const result = await this._paymentServiceManager.refund(payments[key])
            if (result) {
              orderedCart = orderedCart.updatePayment(payments[key], result)
            }
          }
        } catch (error) {
          logger.warning('credit cart transaction error ' + error.message)

          return {
            returnedOrder: false,
            messages: messages,
            isUserCancel: error.isUserCancel
          }

          // NOTE: 決済端末側で伝票印刷を行うため無効化
          // if (payment) {
          //   // クレカレシートの印字が不要であるため、extra_dataは取り除く
          //   orderedCart = orderedCart.updatePayment(payment, payment.setExtraData(null))
          // }
        }

        // NOTE: 決済端末側で伝票印刷を行うため無効化
        // try {
        //   creditEjournal = await this._printCreditReturnReceipt(orderedCart, date)
        //   if (creditEjournal) {
        //     await EJournalRepository.saveIsPrintedById(creditEjournal.id)
        //   }
        // } catch (error) {
        //   messages.push(I18n.t('message.K-03-E002', {
        //     receipt: I18n.t('receipt.title.credit')
        //   }))
        //   logger.warning('print credit return receipt error ' + error.message)
        // }

        // NOTE: 決済端末側で伝票印刷を行うため無効化
        // try {
        //   if (creditEjournal) {
        //     await EJournalRepository.pushById(creditEjournal.id)
        //   }
        // } catch (error) {
        //   logger.warning('Push order ejournal failed. ' + error.toString())
        //   // 失敗してもユーザーへ通知はしない。
        // }

        return {
          returnedOrder: true,
          isUserCancel: false
        }
      }
    })

    if (!isTrainingMode && !result.returnedOrder) {
      if (result.isUserCancel) {
        messages.push(I18n.t('message.F-02-E015'))
        for (const i in result.messages) {
          await AlertView.showAsync(result.messages[i])
        }
        return result.returnedOrder
      } else {
        if (!await ConfirmView.showAsync(I18n.t('message.F-02-E018'))) {
          messages.push(I18n.t('message.F-02-E016', {creditOrderId: order.id}))
          for (const i in result.messages) {
            await AlertView.showAsync(result.messages[i])
          }
          return result.returnedOrder
        }
      }
    }

    result = await loading(dispatch, async () => {
      orderedCart = await orderedCart.convertPaymentMethodType()

      if (isTrainingMode) {
        try {
          await OrderRepository.updateLocal(orderedCart.id)
        } catch (error) {
          messages.push(I18n.t('message.F-02-E007'))
          return {
            returnedOrder: false,
            messages: messages
          }
        }
      } else {
        try {
          await OrderReturnRepository.fetchReturnOrder(orderedCart)
        } catch (error) {
          messages.push(I18n.t('message.F-02-E007'))
          return {
            returnedOrder: false,
            messages: messages
          }
        }
      }

      const date = new Date()
      let ejournal
      let ejournalForShop
      let isError = false
      try {
        // 取引IDの重複を防ぐため、お預かりボタンを押した直後に保存するように変更する
        // this._saveOrderNumberSequence(orderedCart.orderNumberSequence, isTrainingMode)
        const conditions = {
          status: STATUS.RETURN_COMPLETED,
          'posOrder.pos_order_number': orderedCart.posOrderNumber
        }
        returnOrder = await OrderRepository.fetchByConditions(conditions)
        returnedCart = orderedCart.setReturnResult(isTrainingMode ? order : returnOrder[0])
        const result = await this._updateQuantity(returnedCart)
        await proceedUpdateCart(orderedCart, returnedCart, dispatch)
        if (!result.canContinue) {
          return {
            returnedOrder: null,
            messages: messages.concat(result.messages)
          }
        }
        const builder = new CartReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildCartReceipt(returnedCart)
        ejournal = makeReturnEjournal(receipt, returnedCart, date)
        await EJournalRepository.save(ejournal)
        if (needsDispenseChange) {
          try {
            await CashChanger.dispenseChange(returnCashAmount)
          } catch (error) {
            isError = true
            messages.push(I18n.t('message.G-02-E011'))
          }
        }
        await PrinterManager.print(receipt, !isTrainingMode)
        await EJournalRepository.saveIsPrintedById(ejournal.id)
      } catch (error) {
        isError = true
        messages.push(I18n.t('message.G-02-E001'))
      }

      try {
        // 売り場控え返品レシート
        const builderForShop = new ReceiptForShopBuilder()
        await builderForShop.initializeAsync()
        const receiptForShop = builderForShop.buildReceiptForShop(JSON.parse(ejournal.data))
        ejournalForShop = makeReturnEjournal(receiptForShop, returnedCart, date, RECEIPT_TYPE.RETURN_FOR_SHOP)
        await EJournalRepository.save(ejournalForShop)
        const printResult = await PrinterManager.print(receiptForShop)
        // ニアエンドと用紙切れのチェックは２枚目の印刷時のみ行う
        if (isPaperNearEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_near_end'))
        } else if(isPaperEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_end'))
        }
        await EJournalRepository.saveIsPrintedById(ejournalForShop.id)
      } catch (error) {
        if (!isError) {
          messages.push(I18n.t('message.G-02-E001'))
        }
      }

      try {
        if (ejournal) {
          await EJournalRepository.pushById(ejournal.id)
        }
        if (ejournalForShop) {
          await EJournalRepository.pushById(ejournalForShop.id)
        }
      } catch (error) {
        // 失敗してもユーザーへ通知はしない。
      }
      order = JSON.parse(JSON.stringify(order)) // Deep copy
      order.status = STATUS.RETURNED
      return {
        returnedOrder: order,
        messages: messages
      }
    })
    for (const i in result.messages) {
      await AlertView.showAsync(result.messages[i])
    }
    return result.returnedOrder
  }

  async payAsync (cart) {
    try {
      if (!cart.isTraining) {
        const payments = cart.activePayments
        for (let i = 0; i < payments.count(); i++) {
          const payment = payments.get(i)
          const result = await this._paymentServiceManager.pay(payment)
          if (result) {
            cart = cart.updatePayment(payment, result)
          }
        }
      }
      return new CartOperationResult({
        cart,
        canContinue: true,
        messages: []
      })
    } catch (error) {
      if (error instanceof PaymentServiceError) {
        // 複数の決済サービスは併用できない仕様なので、ロールバックの考慮はなし
        if (error.isUserCancel) {
          return new CartOperationResult({
            cart,
            canContinue: false,
            messages: []
          })
        } else {
          return new CartOperationResult({
            cart,
            canContinue: false,
            messages: ['決済サービスエラー\n' + error.message]
          })
        }
      } else if (error instanceof NetworkError) {
        return new CartOperationResult({
          cart,
          canContinue: false,
          messages: ['決済通信エラー\n' + error.message]
        })
      } else {
        logger.error('CartManager.payAsync unexpected error ' + error.toString())
        return new CartOperationResult({
          cart,
          canContinue: false,
          messages: [I18n.t('errors.unexpected_error')]
        })
      }
    }
  }

  /**
   * 処理中断
   */
  payInterrupt () {
    return this._paymentServiceManager.interrupt()
  }

  /**
   * 会計データの生成、保存、送信を行う
   * 処理の都合上、transactionは本関数内部でcommitする。rolebackは行わない
   * @param cart
   * @param transaction
   * @return {Promise.<*>}
   * @private
   */
  async _sendOrderAsync (cart, transaction) {
    let orderDto = null
    try {
      cart = cart.assignSubTotalDiscountToItemForOrder()
      orderDto = (new CartDataConverter(cart)).convert()
    } catch (error) {
      logger.error('CartManager._sendOrderAsync unexpected error on convert cart data ' + error)
      return CartOperationResult.unexpectedError(null)
    }

    // 案件カスタマイズ : ポイント利用をしていた場合、外部システムとの連携の関係上、サーバ側で受け付けられない場合があるので、
    // ポイント利用の有無によって登録フローが異なる
    if (cart.usePoint === 0) {
      return this._sendOrderWithoutPoint(cart, orderDto, transaction)
    } else {
      return this._sendOrderWithPoint(cart, orderDto, transaction)
    }
  }

  /**
   *
   * @param cart
   * @param orderDto
   * @param transaction
   * @return {Promise.<CartOperationResult>}
   * @private
   */
  async _sendOrderWithoutPoint (cart, orderDto, transaction) {
    let ejournal = null
    let receipt = null
    try {
      await OrderRepository.save(orderDto, false, transaction)
      const pendedOrder = await OrderPendRepository.findById(cart.id)
      if (pendedOrder && pendedOrder.is_pended) {
        await OrderPendRepository.clearById(cart.id)
      }
      // 取引IDの重複を防ぐため、お預かりボタンを押した直後に保存するように変更する
      // this._saveOrderNumberSequence(cart.orderNumberSequence, cart.isTraining)
    } catch (error) {
      logger.error('CartManager._sendOrderAsync unexpected error on save order data ' + error)
      return CartOperationResult.unexpectedError(null)
    }

    try {
      ({ ejournal, receipt } = await this._makeOrderReceiptAsync(cart, transaction))
    } catch (error) {
      logger.error('CartManager._sendOrderAsync unexpected error on make receipt ' + error)
      return CartOperationResult.unexpectedError(null)
    }

    await transaction.commit()

    try {
      const responseOrder = await OrderRepository.syncLocalToRemoteById(orderDto.id)
      cart = cart.setIsPushed()
      if (cart.customer) {
        // 顧客がセットされていた場合、ValueFrontからのレスポンスによってレシート内容に顧客情報が追加されるため、上書き
        ({ ejournal, receipt } = await this._makeOrderReceiptAsync(cart, null, ejournal.id))
      }
    } catch (error) {
      if (error instanceof NetworkError) {
        await saveOperationLog(OPERATION_TYPE.UNSET_ITEM, new Date())
        return new CartOperationResult({
          cart,
          canContinue: true,
          messages: ['会計を送信できませんでした。'],
          ejournal,
          receipt
        })
      } else {
        logger.error('CartManager._sendOrderAsync unexpected error on push ' + error)
        return CartOperationResult.unexpectedError(null)
      }
    }

    return new CartOperationResult({
      cart,
      ejournal,
      receipt,
      canContinue: true,
      messages: []
    })
  }

  async _handleErrorForSendOrderWithPoint (cart: Cart, error): CartOperationResult {
    await logger.warning('push order @_sendOrderWithPoint failed ' + error.message)
    let orderNotRegisterd = false
    if (error instanceof NetworkError) {
      // サーバー側で成功している可能性がある場合、一度会計データを検索して登録されていないか確かめる。
      if (error.hasSuccessPossibility) {
        for (let i = 0; i < 5; i++) {
          // サーバーの処理時間やネットワークの回復等考慮し、少しだけ待つ
          await logger.info('Start to verify that the order has been registered. try count = ' + i)
          await waitAsync(3000)
          try {
            await OrderRepository.fetchByOrderId(cart.id)
            await logger.info('Order found. order_id = ' + cart.id)
            // 正常にfetchできる = サーバーに存在していたということなので、成功扱いとする
            return null
          } catch (error) {
            if (error.type === NetworkError.TYPE.CLIENT_ERROR) {
              await logger.warning('Order not found. order_id = ' + cart.id)
              orderNotRegisterd = true
              break
            } else {
              await logger.warning('Verify order failed with network error ' + error.message)
            }
          }
        }
      }

      if (!orderNotRegisterd) {
        // 成功したかどうか判定不可能の場合、受注IDを再発行して別会計として上げてもらう。
        return new CartOperationResult({
          cart: cart.setId(generateUuid()),
          canContinue: false,
          messages: [I18n.t('message.B-01-E015')]
        })
      }

      // サーバーに会計が上がっていない時、ポイント利用失敗扱いで会計送信を中止し、再度オペレーションを行ってもらう。
      let message = ''
      const creditPaidPayment = cart.payments.find(payment => payment.isCreditPaid)
      if (creditPaidPayment) {
        message = I18n.t('message.B-01-E012', {paymentOrderId: creditPaidPayment.extraData.orderId})
      } else {
        message = I18n.t('message.B-01-E013')
      }
      message = getCommonNetworkErrorMessage(error, message)

      return new CartOperationResult({
        cart,
        canContinue: false,
        messages: [message]
      })
    } else {
      logger.error('CartManager._sendOrderAsync unexpected error on push using point order ' + error)
      return CartOperationResult.unexpectedError(null)
    }
  }

  async _sendOrderWithPoint (cart, orderDto, transaction) {
    let ejournal = null
    let receipt = null
    let responseOrder = {}
    try {
      responseOrder = await OrderRepository.push(orderDto)
    } catch (error) {
      const result = await this._handleErrorForSendOrderWithPoint(cart, error)
      // resultが帰ってこなかったときのみ、正常に会計登録ができたということなので、処理を続行。
      if (result) { return result }
    }

    cart = cart.setIsPushed()
    orderDto = (new CartDataConverter(cart)).convert()
    await OrderRepository.save(orderDto, true, transaction)
    const pendedOrder = await OrderPendRepository.findById(cart.id)
    if (pendedOrder && pendedOrder.is_pended) {
      await OrderPendRepository.clearById(cart.id)
    }

    try {
      ({ ejournal, receipt } = await this._makeOrderReceiptAsync(cart, transaction))
    } catch (error) {
      logger.error('CartManager._sendOrderAsync unexpected error on make receipt ' + error)
      return CartOperationResult.unexpectedError(null)
    }

    transaction.commit()

    return new CartOperationResult({
      cart,
      ejournal,
      receipt,
      canContinue: true,
      messages: []
    })
  }

  /**
   *
   * @param {CartOperationResult} lastResult
   * @return {Promise.<void>}
   * @private
   */
  async _printOrderReceiptAsync (lastResult) {
    const messages = []
    const isForShop = true
    let ejournalForShop
    let isError = false
    let isCreditReceiptPrinted = false
    if (lastResult.receipt) {
      try {
        const payments = lastResult.cart.activePayments
        const isCreditPaymentMethod = payments.size === 1 && payments.get(0).paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT
        const printResult = await PrinterManager.print(lastResult.receipt, !lastResult.cart.isTraining && !isCreditPaymentMethod)
        if (isPaperNearEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_near_end'))
        } else if(isPaperEnd(printResult)) {
          messages.push(I18n.t('printer.is_paper_end'))
        }
        await EJournalRepository.saveIsPrintedById(lastResult.ejournal.id)
      } catch (error) {
        isError = true

        messages.push(I18n.t('message.G-02-E001'))
      }
    }

    if (lastResult.cart.isReturn) {
      try {
        const builderForShop = new ReceiptForShopBuilder()
        await builderForShop.initializeAsync()
        const receiptForShop = builderForShop.buildReceiptForShop(JSON.parse(lastResult.ejournal.data))
        ejournalForShop = makeOrderEJournal(receiptForShop, lastResult.cart, undefined, isForShop)

        await EJournalRepository.save(ejournalForShop)
        await PrinterManager.print(receiptForShop)
        await EJournalRepository.saveIsPrintedById(ejournalForShop.id)
      } catch (error) {
        if (!isError) {
          messages.push(I18n.t('message.G-02-E001'))
        }
      }
    }

    if (lastResult.creditEjournal) {
      try {
        await PrinterManager.print(lastResult.creditReceipt)
        isCreditReceiptPrinted = true
        await EJournalRepository.saveIsPrintedById(lastResult.creditEjournal.id)
      } catch (error) {
        messages.push(I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.credit')
        }))
      }
    }

    if (lastResult.receipt) {
      try {
        await EJournalRepository.pushById(lastResult.ejournal.id)
        if (lastResult.cart.isReturn) {
          await EJournalRepository.pushById(ejournalForShop.id)
        }
      } catch (error) {
        logger.warning('Push order ejournal failed. ' + error.toString())
        // 失敗してもユーザーへ通知はしない。
      }
    }

    if (lastResult.creditEjournal) {
      try {
        await EJournalRepository.pushById(lastResult.creditEjournal.id)
      } catch (error) {
        logger.warning('Push credit ejournal failed. ' + error.toString())
        // 失敗してもユーザーへ通知はしない。
      }
    }

    return new CartOperationResult({
      canContinue: true,
      isCreditReceiptPrinted,
      messages
    })
  }

  /**
   * クレジット伝票が出なかった時に、二重決済にならないように決済を取り消す
   * @param {CartOperationResult} lastResult
   * @return {Promise.<CartOperationResult>}
   * @private
   */
  async _refundCreditPayment (lastResult) {
    if (!lastResult.creditReceipt || lastResult.isCreditReceiptPrinted || lastResult.cart.isTraining) {
      return new CartOperationResult({ canContinue: true })
    }

    let cart = lastResult.cart
    const payments = cart.activePayments.toArray()
    logger.info('Start refund credit')
    try {
      await DeviceLogRepository.push({
        log_text: 'start',
        device_log_code: 'pos.payment.rollback',
        extra_data: JSON.stringify({
          payments: cart.activePayments.toJS(),
          order_id: cart.id,
          pos_order_number: cart.posOrderNumber
        })
      }, true)
    } catch (error) {
      // デバイスログの未送信機能は無いので、ログファイルに書くだけ書いて終わる。
      logger.warning('Push device log for pos.payment.rollback.start failed.')
    }
    const messages = []

    const date = new Date()
    let payment
    let creditOrderId = ''
    try {
      for (const i in payments) {
        payment = payments[i]
        if (payment.extraData) {
          creditOrderId = payment.extraData.orderId
        }
        const result = await this._paymentServiceManager.refund(payment)
        if (result) {
          cart = cart.updatePayment(payment, result)
        }
      }
      try {
        await DeviceLogRepository.push({
          log_text: 'success',
          device_log_code: 'pos.payment.rollback',
          extra_data: JSON.stringify({
            payments: cart.activePayments.toJS(),
            order_id: cart.id,
            pos_order_number: cart.posOrderNumber
          })
        }, true)
      } catch (error) {
        // デバイスログの未送信機能は無いので、ログファイルに書くだけ書いて終わる。
        logger.warning('Push device log for pos.payment.rollback.success failed.')
      }
    } catch (error) {
      let response = {}
      if (error instanceof PaymentServiceError) {
        response = error.response
      }
      messages.push(I18n.t('message.B-01-E016', {
        order_number: cart.posOrderNumber,
        credit_order_id: creditOrderId
      }))
      logger.warning('credit cart transaction error ' + error.message)

      try {
        await DeviceLogRepository.push({
          log_text: 'failure',
          device_log_code: 'pos.payment.rollback',
          extra_data: JSON.stringify({
            response,
            order_id: cart.id,
            pos_order_number: cart.posOrderNumber
          }, true)
        })
      } catch (error) {
        // デバイスログの未送信機能は無いので、ログファイルに書くだけ書いて終わる。
        logger.warning('Push device log for pos.payment.rollback.failure failed.')
      }

      if (payment) {
        // クレカレシートの印字が不要であるため、extra_dataは取り除く
        cart = cart.updatePayment(payment, payment.setExtraData(null))
      }

      return new CartOperationResult({
        messages: messages,
        canContinue: true
      })
    }

    let creditEjournal
    try {
      creditEjournal = await this._printCreditReturnReceipt(cart, date)
      if (creditEjournal) {
        await EJournalRepository.saveIsPrintedById(creditEjournal.id)
      }
    } catch (error) {
      logger.warning('print credit return receipt error ' + error.message)
    }

    try {
      if (creditEjournal) {
        await EJournalRepository.pushById(creditEjournal.id)
      }
    } catch (error) {
      logger.warning('Push order ejournal failed. ' + error.toString())
      // 失敗してもユーザーへ通知はしない。
    }
    return new CartOperationResult()
  }

  /**
   *
   * @param cart
   * @param id
   * @return {Promise.<{ejournal, receipt: ({content}|*)}>}
   * @private
   */
  async _makeOrderReceiptAsync (cart, transaction, id = undefined) {
    const receiptBuilder = new CartReceiptBuilder()
    await receiptBuilder.initializeAsync()
    const receipt = receiptBuilder.buildCartReceipt(cart)
    const ejournal = makeOrderEJournal(receipt, cart, id)
    await EJournalRepository.save(ejournal, transaction)
    return { ejournal, receipt }
  }

  async _makeCreditReceiptAsync (cart) {
    const creditPayment = cart.activePayments.find(payment => payment.extraData)
    if (!creditPayment) {
      return new CartOperationResult({
        canContinue: true,
        messages: []
      })
    }
    const extraData = creditPayment.extraData
    const creditReceiptBuilder = new CreditReceiptBuilder()
    await creditReceiptBuilder.initializeAsync()
    const creditReceipt = creditReceiptBuilder.buildCreditReceipt(extraData, creditPayment.name, cart)
    const creditEjournal = makeCreditEjournal(creditReceipt, creditPayment.amount, cart)
    await EJournalRepository.save(creditEjournal)
    return new CartOperationResult({
      canContinue: true,
      messages: [],
      creditReceipt,
      creditEjournal
    })
  }

  async _printCreditReturnReceipt (cart, date) {
    const creditPayment = cart.activePayments.find(payment => payment.extraData)
    if (!creditPayment) {
      return null
    }
    const extraData = creditPayment.extraData
    const creditReceiptBuilder = new CreditReceiptBuilder()
    await creditReceiptBuilder.initializeAsync()
    const creditReceipt = creditReceiptBuilder.buildCreditReceipt(extraData, creditPayment.name, cart)
    const creditEjournal = makeCreditReturnEjournal(creditReceipt, creditPayment.amount, cart, date)
    await EJournalRepository.save(creditEjournal)
    await PrinterManager.print(creditReceipt)
    return creditEjournal
  }

  getCustomerDisplayMessage (oldCart, newCart) {
    const isPaymentInputed =
      newCart.deposit.gt(0) && !oldCart.payments.equals(newCart.payments)
    if (isPaymentInputed && !newCart.isReturn) {
      let bottom = {}
      if (newCart.remainDeposit.gt(0)) {
        bottom = {
          left: '残額',
          right: '￥' + formatNumber(newCart.remainDeposit)
        }
      } else {
        bottom = {
          left: 'おつり',
          right: '￥' + formatNumber(newCart.change)
        }
      }
      return {
        top: {
          left: 'お預かり',
          right: '￥' + formatNumber(newCart.deposit)
        },
        bottom
      }
    } else {
      return {
        top: {
          left: newCart.isReturn ? 'ご返金' : '合計',
          right: newCart.totalQuantity + '点'
        },
        bottom: {
          right: '￥' + formatNumber(newCart.totalTaxed)
        }
      }
    }
  }

  _generateOrderNumberSequence (isTraining = false) {
    const number = isTraining
      ? this.getSetting(SettingKeys.COMMON.TRAINING_ORDER_NUMBER_SEQUENCE) + 'T'
      : this.getSetting(SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE) + 1
    logger.debug('_generateOrderNumberSequence ' + number)
    return number
  }

  _saveOrderNumberSequence (number, isTraining = false) {
    if (isTraining) {
      const trainingNumber = this.getSetting(SettingKeys.COMMON.TRAINING_ORDER_NUMBER_SEQUENCE) - 1
      this.updateSetting(SettingKeys.COMMON.TRAINING_ORDER_NUMBER_SEQUENCE, trainingNumber)
    } else {
      this.updateSetting(SettingKeys.COMMON.ORDER_NUMBER_SEQUENCE, number)
    }
  }

  async _updateQuantity (cart) {
    const cartItemQuantities = {}
    const quantitySign = cart.isReturn || cart.isCancel ? -1 : 1
    await cart.items.forEach(async item => {
      if (item.product.stockMode === STOCK_MODE.USE) {
        if (cartItemQuantities[item.product.productId]) {
          cartItemQuantities[item.product.productId].quantity += item.quantity * quantitySign
        } else {
          cartItemQuantities[item.product.productId] = {
            product_id: item.product.productId,
            product_variant_id: item.product.productVariantId,
            quantity: item.quantity * quantitySign
          }
        }
      }

      if (item.product.isParentProduct) {
        await item.product.childProducts.forEach(async childItem => {
          if (childItem.variant.stock_mode === STOCK_MODE.USE) {
            if (cartItemQuantities[childItem.id]) {
              cartItemQuantities[childItem.id].quantity += item.quantity * childItem.quantity * quantitySign
            } else {
              cartItemQuantities[childItem.id] = {
                product_id: childItem.id,
                product_variant_id: childItem.variant.id,
                quantity: item.quantity * childItem.quantity * quantitySign
              }
            }
          }
        })
      }
    })
    await Object.keys(cartItemQuantities).forEach(async key => {
      try {
        await ProductRepository.updateQuantityByProductId(
          cartItemQuantities[key].quantity,
          cartItemQuantities[key].product_id,
          cartItemQuantities[key].product_variant_id)
      } catch (error) {
        return new CartOperationResult({
          cart,
          canContinue: false,
          messages: ['在庫の更新に失敗しました。']
        })
      }
    })
    return new CartOperationResult({
      cart,
      canContinue: true,
      messages: []
    })
  }

  async updateCart (dispatch, cart, product, amount = undefined) {
    const productDetail = await ProductRepository.findWithDetail(product.id)
    const nextCartState = await cart.addProductAsync(productDetail, productDetail.product_variants[0], 1, amount)
    await proceedUpdateCart(cart, nextCartState, dispatch)
  }

  _isTaxfreeOrder (cart) {
    if (cart.isTaxfreeApplied) {
      return cart.items.find(item => !item.isTaxfreeApplied)
    }
    return false
  }
}
