import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import logger from 'common/utils/logger'
import { ceilByCurrency } from 'common/models/Currency'
import { DISCOUNT_TYPE, PROMOTION_TYPE } from '../../promotion/models'
import { formatPriceWithCurrency } from '../../../common/utils/formats'

const CartItemDiscountRecord = Record({
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(0),
  discountReasonId: null,
  discountReasonName: null,
  promotionId: null,
  promotionType: null,
  promotionName: '',
  promotionCode: '',
  bundleNumber: 0,
  bundleType: 0,
  bundleSurplus: Decimal(0),
  bundlePrice: null,
  bundlePriceForDisplay: null,
  extendsData: null
})

/**
 * 明細値割引
 * @class CartItemDiscount
 * @property {integer} discountType
 * @property {Decimal} value
 * @property {Object} discountReason
 * @property {string|null} discountReasonId
 * @property {string|null} discountReasonName
 * @property {string|null} promotionId
 * @property {integer|null} promotionType
 * @property {string} promotionName
 * @property {string} promotionCode
 * @property {integer} bundleNumber
 * @property {integer} bundleType
 * @property {Decimal} bundleSurplus - バンドル成立時に割引の按分にあまりが出た場合の調整値
 * @property {Decimal|null} bundlePrice - バンドル成立後の価格。合計金額の計算に利用、order_itemsの金額に入れる（bundleSurplusと足し合わせて）
 * @property {Decimal|null} bundlePriceForDisplay - バンドル成立後の見せかけの価格。主に表示用
 * @property extendsData
 */
export default class CartItemDiscount extends CartItemDiscountRecord {
  get isSale () {
    return this.promotionType === PROMOTION_TYPE.SALE
  }

  get isSubtotal () {
    return this.promotionType === PROMOTION_TYPE.SUBTOTAL || this.promotionType === PROMOTION_TYPE.COUPON
  }

  get isBundle () {
    return this.promotionType === PROMOTION_TYPE.BUNDLE
  }

  get isManual () {
    return !this.isSale && !this.isSubtotal && !this.isBundle
  }

  /**
   * 割引率 (値引きの場合はnull)
   * @returns {Decimal|null}
   */
  get discountRate () {
    if (this.discountType === DISCOUNT_TYPE.RATE) {
      return this.value
    } else {
      return null
    }
  }

  /**
   * 割引率 % (値引きの場合はnull)
   * @returns {Decimal|null}
   */
  get discountPercent () {
    if (this.discountType === DISCOUNT_TYPE.RATE) {
      return this.value.mul(100)
    } else {
      return null
    }
  }

  /**
   *
   * @returns {CartItemDiscount}
   */
  clearBundleSurplus () {
    return this.set('bundleSurplus', Decimal(0))
  }

  /**
   *
   * @returns {CartItemDiscount}
   */
  updateValue (newValue) {
    return this.set('value', newValue)
  }

  /**
   * 小計値割引一覧から小計値引額を算出する。(按分額を含まない)
   * @param {List<CartItemDiscount>} discounts
   * @return Decimal
   */
  static amountDiscountTotalAmount (discounts) {
    return discounts
      .filter(discount => discount.discountType === DISCOUNT_TYPE.AMOUNT && !discount.isSubtotal)
      .reduce((sum, discount) => {
        return sum.add(discount.value)
      }, Decimal(0))
  }

  /**
   * 小計値割引一覧から小計値引額を算出する。(按分額を含む)
   * @param {List<CartItemDiscount>} discounts
   * @return Decimal
   */
  static amountDiscountTotalAmountWithProrate (discounts) {
    return discounts
      .filter(discount => discount.discountType === DISCOUNT_TYPE.AMOUNT)
      .reduce((sum, discount) => {
        return sum.add(discount.value)
      }, Decimal(0))
  }

  /**
   * 小計値割引一覧から小計割引額を算出する。
   * @param {List<CartItemDiscount>} discounts
   * @param {Decimal} totalItems
   * @param {string} currency
   * @return Decimal
   */
  static rateDiscountTotalAmount (discounts, totalItems, currency) {
    const totalRate = this.discountRateTotal(discounts)
    return ceilByCurrency(totalItems.mul(totalRate), currency)
  }

  /**
   * 小計値割引一覧から小計割引額を算出する。（按分含む）
   * @param {List<CartItemDiscount>} discounts
   * @param {Decimal} totalItems
   * @param {string} currency
   * @return Decimal
   */
  static rateDiscountTotalAmountWithProrate (discounts, totalItems, currency) {
    const totalRate = this.discountRateTotalWithProrate(discounts)
    return ceilByCurrency(totalItems.mul(totalRate), currency)
  }

  /**
   * 小計値割引一覧から小計値割引額を算出する。
   * @param {List<CartItemDiscount>} discounts
   * @param {Decimal} totalItems
   * @param {string} currency
   * @return Decimal
   */
  static discountTotalAmount (discounts, totalItems, currency) {
    return this.rateDiscountTotalAmount(discounts, totalItems, currency)
      .add(this.amountDiscountTotalAmount(discounts))
  }

  /**
   * 値割引一覧から割引率合計を算出する（按分額を含まない）
   * @param {List<CartItemDiscount>} discounts
   * @return Decimal
   */
  static discountRateTotal (discounts) {
    return discounts
      .filter(discount => discount.discountType === DISCOUNT_TYPE.RATE && !discount.isSubtotal)
      .reduce((sum, discount) => {
        return sum.add(discount.value)
      }, Decimal(0))
  }

  /**
   * 値割引一覧から割引率合計を算出する（按分額を含む）
   * @param {List<CartItemDiscount>} discounts
   * @return Decimal
   */
  static discountRateTotalWithProrate (discounts) {
    return discounts
      .filter(discount => discount.discountType === DISCOUNT_TYPE.RATE)
      .reduce((sum, discount) => {
        return sum.add(discount.value)
      }, Decimal(0))
  }

  /**
   * 既存の値割引一覧を考慮して、新しい割引を適用した後の値割引一覧を導出する
   * @param {CartItemDiscount} newDiscount
   * @param {List<CartItemDiscount>} oldDiscounts
   * @return List<CartItemDiscount>
   */
  static applyToDiscounts (newDiscount, oldDiscounts) {
    if (oldDiscounts.count() > 4) {
      logger.fatal('[CartItemDiscount.applyToDiscounts] discounts too meny count')
    }
    let sale = this.getSaleDiscount(oldDiscounts)
    let manual = this.getManualDiscount(oldDiscounts)
    let subtotal = this.getSubtotalDiscount(oldDiscounts)
    if (newDiscount.isSale) {
      sale = newDiscount
    } else if (newDiscount.isSubtotal) {
      subtotal = newDiscount
    } else {
      manual = newDiscount
    }

    const bundle = this.getBundleDiscount(oldDiscounts)
    return List([bundle, sale, manual, subtotal]).filter(discount => !!discount)
  }

  /**
   * プリメモリを元に個別値割引を作成する
   * @param promotionPreset
   * @return {CartItemDiscount}
   */
  static createWithPromotionPreset (promotionPreset, discountReasonName) {
    return new CartItemDiscount({
      discountType: promotionPreset.discount_type,
      value: Decimal(promotionPreset.discount_value),
      discountReasonId: promotionPreset.discount_reason_id,
      discountReasonName: discountReasonName
    })
  }

  static createWithDiscountReason (amount, discountType, discountReasonId, discountReasonName) {
    return new CartItemDiscount({
      discountType,
      discountReasonId,
      discountReasonName,
      value: Decimal(amount)
    })
  }

  static createSaleDiscount (sale) {
    let discountType = 0
    let value = 0
    if (sale.discount_rate > 0) {
      discountType = DISCOUNT_TYPE.RATE
      value = sale.discount_rate
    } else if (sale.discount_price > 0) {
      discountType = DISCOUNT_TYPE.AMOUNT
      value = sale.discount_price
    } else {
      console.error('invalid discount value')
    }

    return new CartItemDiscount({
      discountType,
      value: Decimal(value),
      promotionId: sale.id,
      promotionType: PROMOTION_TYPE.SALE,
      promotionName: sale.name,
      promotionCode: sale.code
    })
  }

  static createBundleDiscount (bundle, discountAmount, bundleSurplus, bundlePrice, bundlePriceForDisplay, number) {
    return new CartItemDiscount({
      discountType: DISCOUNT_TYPE.AMOUNT,
      value: discountAmount,
      promotionId: bundle.id,
      promotionType: PROMOTION_TYPE.BUNDLE,
      promotionName: bundle.name,
      promotionCode: bundle.code,
      bundleNumber: number,
      bundleType: bundle.bundle_type,
      bundleSurplus,
      bundlePrice,
      bundlePriceForDisplay
    })
  }

  /**
   * 小計値引きから単品値引きに変換（按分のため）
   * 
   * @param {CartDiscount} cartDiscount 
   * @param {Decimal} value 
   * @return {CartItemDiscount}
   */
  static convertCartDiscountToCartItemDiscount (cartDiscount, value = cartDiscount.value) {
    const isSubtotal = cartDiscount.promotionType !== PROMOTION_TYPE.SALE && cartDiscount.promotionType !== PROMOTION_TYPE.BUNDLE
    return new CartItemDiscount({
      discountType: isSubtotal ? DISCOUNT_TYPE.AMOUNT : cartDiscount.discountType,
      value: value,
      discountReasonId: cartDiscount.discountReasonId,
      discountReasonName: cartDiscount.discountReasonName,
      promotionType: cartDiscount.promotionType,
      promotionName: cartDiscount.discountReasonName
    })
  }

  static getSaleDiscount (discounts) {
    return discounts.find(discount => discount.isSale)
  }

  static hasSaleDiscount (discounts) {
    return !!this.getSaleDiscount(discounts)
  }

  static getManualDiscount (discounts) {
    return discounts.find(discount => discount.promotionType === null)
  }

  static hasManualDiscount (discounts) {
    return !!this.getManualDiscount(discounts)
  }

  static getSubtotalDiscount (discounts) {
    return discounts.find(discount => discount.isSubtotal)
  }

  static getBundleDiscount (discounts) {
    return discounts.find(discount => discount.isBundle)
  }

  /**
   * 値割引一覧を表示用のラベルに変換する
   */
  static discountsToLabel (discounts, totalItems, currency) {
    const sale = this.getSaleDiscount(discounts)
    const manual = this.getManualDiscount(discounts)

    if (
      sale && sale.discountType === DISCOUNT_TYPE.RATE &&
      manual && manual.discountType === DISCOUNT_TYPE.RATE
    ) {
      // 個別割引とセール割引が併用された場合のみ特別表記
      const rate = this.discountRateTotal(discounts)
      const amount = this.rateDiscountTotalAmount(discounts, totalItems, currency)
      return 'セ/個 : ' + this._formatDiscountValueForLabel(DISCOUNT_TYPE.RATE, amount.abs(), rate, currency)
    }

    let label = ''
    if (sale) {
      const amount = this.discountTotalAmount(List([sale]), totalItems, currency)
      label += 'セール : ' + this._formatDiscountValueForLabel(
        sale.discountType, amount.abs(), sale.value, currency)
    }

    if (manual) {
      if (sale) label += '　　' // Separator
      const amount = this.discountTotalAmount(List([manual]), totalItems, currency)
      label += '個 : ' + this._formatDiscountValueForLabel(
        manual.discountType, amount.abs(), manual.value, currency)
    }

    return label
  }

  static _formatDiscountValueForLabel (discountType, amount, rate, currency) {
    if (discountType === DISCOUNT_TYPE.RATE) {
      return '-' + formatPriceWithCurrency(amount, currency) +
        '(' + rate.mul(100).toNumber() + '%)'
    } else {
      return '-' + formatPriceWithCurrency(amount, currency)
    }
  }
}
