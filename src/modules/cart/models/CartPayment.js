import { Record, List } from 'immutable'
import { Decimal } from 'decimal.js'
import logger from 'common/utils/logger'
import {
  PAYMENT_METHOD_TYPE,
  isCredit,
  isGiftPaymentMethodType,
  isCreditCardOrEmoney,
  isExternalPoint
} from '../../payment/models'
import I18n from 'i18n-js'

const CartPaymentRecord = Record({
  paymentMethodId: null,
  name: '',
  paymentMethodType: 1,
  paymentMethodCode: null,
  mediaType: 0,
  acquirerCode: '',
  amount: new Decimal(0),
  sortOrder: 0,
  creditList: null,
  isOffline: 0,
  extraData: null,
  waonPointRate: 0
})

/**
 * @property {string|null} paymentMethodId
 * @property {string|null} name
 * @property {int} paymentMethodType
 * @property {int} mediaType
 * @property {string} acquirerCode
 * @property {string|null} paymentMethodCode
 * @property {Decimal} amount
 * @property {int} sortOrder
 * @property {List} creditList
 * @property {Object} extraData
 */
export default class CartPayment extends CartPaymentRecord {
  static createFromPaymentMethodEntity (paymentMethod, pointRate) {
    return new CartPayment({
      paymentMethodId: paymentMethod.id,
      name: paymentMethod.name,
      paymentMethodType: paymentMethod.payment_method_type,
      paymentMethodCode: paymentMethod.payment_method_code,
      mediaType: paymentMethod.media_type,
      acquirerCode: paymentMethod.acquirer_code,
      sortOrder: paymentMethod.sort_order,
      isOffline: paymentMethod.is_offline,
      waonPointRate: pointRate
    })
  }

  static createFromCreditPaymentMethods (creditPaymentList) {
    const creditList = List(creditPaymentList.map(payment => {
      return CartPayment.createFromPaymentMethodEntity(payment)
    }))
    return CartPayment.createFromPaymentMethodEntity(creditPaymentList[0])
      .set('name', I18n.t('cart.credit_card'))
      .set('creditList', creditList)
  }

  static createFromPaymentEntity (payment) {
    let extraData = null
    if (payment.extra_data && payment.extra_data !== '') {
      try {
        extraData = JSON.parse(payment.extra_data)
      } catch (error) {
        logger.error(error)
      }
    }

    return new CartPayment({
      id: payment.payment_method_id,
      name: payment.payment_method_name,
      paymentMethodType: payment.payment_method_type,
      paymentMethodCode: payment.payment_method_code,
      mediaType: payment.media_type,
      acquirerCode: payment.acquirer_code,
      amount: Decimal(payment.amount),
      extraData: extraData
    })
  }

  static checkUpdatedCartPayment (oldPayments, newPayments, surplus) {
    let isValid = true
    const messages = []
    // 現金の入力金額
    const cashAmount = newPayments.filter(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH).reduce((sum, payment) => sum.add(payment.amount), Decimal(0))
    // 金券(釣なし or 釣あり)の使用数
    const giftCount = newPayments.filter(payment => isGiftPaymentMethodType(payment.paymentMethodType)).reduce((sum) => sum + 1, 0)
    // oldCartにおける金券(釣なし or 釣あり)の入力金額(配列)
    const giftAmountsOld = oldPayments.filter(e => isGiftPaymentMethodType(e.paymentMethodType)).map(payment => { return payment.amount })
    // newCartにおける金券(釣なし or 釣あり)の入力金額(配列)
    const giftAmountsNew = newPayments.filter(e => isGiftPaymentMethodType(e.paymentMethodType)).map(payment => { return payment.amount })
    // giftAmountsNewをListからArrayに変換しておく
    //  => 入力した券支払額の最小額を取得する際にgiftAmountsNew.min()は時折正しくない結果を返すことがあり、
    //     Math.min.apply()で算出する方針としたため
    const giftAmountsNewArray = giftAmountsNew.toArray()
    // クレカあるいは電子マネーが使われているかどうか
    const isUsedCreditCardOrEmoney = newPayments.some(payment => isCreditCardOrEmoney(payment.paymentMethodType))
    // 外部ポイントが使われているかどうか
    const isUsedExternalPoint = newPayments.some(payment => isExternalPoint(payment.paymentMethodType))

    // 現金支払額以上の余剰金額が発生するように金額を入力された場合にアラートし、処理を中断する。
    if (surplus.gte(cashAmount) && cashAmount.gt(0)) {
      isValid = false
      messages.push(I18n.t('message.B-01-E002'))
    }

    // 複数の券を併用した場合、余剰金額が券釣り有、券釣り無の最小金額以上になった場合にアラートし、処理を中断する。
    if (giftCount >= 2 && surplus.gte(Math.min.apply(null, giftAmountsNewArray))) {
      isValid = false
      messages.push(I18n.t('message.B-01-E002'))
    }

    // クレカ/電子マネーを入力済みかつ現金未入力の状態で券つり有/券つり無の金額を入力し、余剰金額が発生した時点でアラートし、処理を中断する。
    // 外部ポイントを入力済みかつ券つり有/券つり無の金額を入力し、余剰金額が発生した時点でアラートし、処理を中断する。
    if ((!giftAmountsOld.equals(giftAmountsNew) && cashAmount.eq(0) && isUsedCreditCardOrEmoney && surplus.gt(0)) ||
        (!giftAmountsOld.equals(giftAmountsNew) && isUsedExternalPoint && surplus.gt(0))) {
      isValid = false
      messages.push(I18n.t('message.B-01-E003'))
    }

    return {isValid, messages}
  }

  static calculateChange (cart, payment, countGiftWithChange, countGiftWithoutChange) {
    switch (payment.paymentMethodType) {
      case PAYMENT_METHOD_TYPE.CASH:
        return cart.change
      case PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE:
        return countGiftWithChange >= 2 ? Decimal(0) : cart.giftChange
      case PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE:
        return countGiftWithoutChange >= 2 ? Decimal(0) : cart.giftSurplus
      default:
        return Decimal(0)
    }
  }

  /**
   *
   * @param {Decimal|float|int} amount
   * @return {CartPayment}
   */
  updateAmount (amount) {
    if (!(amount instanceof Decimal)) {
      amount = new Decimal(amount)
    }
    return this.set('amount', amount)
  }

  setExtraData (data) {
    // NOTE: VEGA3000では仕向け先コードが無いため、仕向け先コードの突き合わせは行わない
    // if (this.creditList && data && data.acquirerCode) {
    //   const matched = this.creditList.find(payment => payment && payment.acquirerCode === data.acquirerCode)
    //   if (matched) {
    //     return this
    //       .set('paymentMethodId', matched.paymentMethodId)
    //       .set('name', matched.name)
    //       .set('paymentMethodType', matched.paymentMethodType)
    //       .set('paymentMethodCode', matched.paymentMethodCode)
    //       .set('mediaType', matched.mediaType)
    //       .set('acquirerCode', data ? data.acquirerCode : '')
    //       .set('extraData', data)
    //   } else {
    //     const otherCard = this.creditList.find(payment => payment.paymentMethodId === OTHER_CARD.id)
    //     if (otherCard) {
    //       return this
    //         .set('paymentMethodId', otherCard.paymentMethodId)
    //         .set('name', otherCard.name)
    //         .set('paymentMethodType', otherCard.paymentMethodType)
    //         .set('paymentMethodCode', otherCard.paymentMethodCode)
    //         .set('mediaType', otherCard.mediaType)
    //         .set('acquirerCode', data ? data.acquirerCode : '')
    //         .set('extraData', data)
    //     }
    //     logger.error('paymentMethodId of otherCard is not match ', OTHER_CARD.id)
    //   }
    // }
    return this.set('extraData', data)
  }

  get isCreditPaid () {
    return isCredit(this.paymentMethodType) || this.paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY
  }
}
