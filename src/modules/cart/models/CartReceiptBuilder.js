/**
 * TODO 翻訳対応
 */
import * as _ from 'underscore'
import I18n from 'i18n-js'
import CartReceiptBuilderBase from './CartReceiptBuilderBase'
import SettingKeys from '../../setting/models/SettingKeys'
import { DISCOUNT_TYPE, DISCOUNT_ORDER } from '../../promotion/models'
import { formatUnit } from '../../../common/utils/formats'
import CartPayment from './CartPayment'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'

export default class CartReceiptBuilder extends CartReceiptBuilderBase {
  get _headerMessage () {
    return this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.HEADER_MESSAGE)
  }

  get _headerLogoEnabled () {
    return !!this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_HEADER_LOGO)
  }

  get _footerMessage () {
    return this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.FOOTER_MESSAGE)
  }
  get _footerLogoEnabled () {
    return !!this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_FOOTER_LOGO)
  }

  get _isPrintZeroProduct () {
    return this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_ZERO_PRODUCTS)
  }

  /**
   *
   * @param {Cart} cart
   * @param {CashierParameterSetEntity} cashierParameterSet
   */
  buildCartReceipt (cart, cashierParameterSet) {
    cart = cart.numberSortOrderToItems()
    return {
      content: _.compact([
        ...this.cartHeaderElements(this._headerMessage, cart),
        this.textLineElement(), // ----------------------------
        this._originNumberElement(cart),

        this.feedElement(),
        // バンドル一覧 bundles
        ...this.bundlesElement(cart.appliedBundles, cart.currency),
        // 明細一覧(バンドルを除く) Items(not bundled)
        ..._.flatten(cart.items.filter(item => !item.isBundled).map(item => {
          return this.cartItemElement(item)
        }).toArray()),

        ...this._subTotalElement(cart),
        this.feedElement(),

        ...this._totalElement(cart),

        this.feedElement(),
        this._paymentTitleElement(cart),
        ...this.paymentsElements(cart),
        ...this._customerElements(cart),
        this.feedElement(),
        ...this._lastElement(cart),
        this.cutElement()
      ])
    }
  }

  cartHeaderElements (message, cart) {
    const idLabel = cart.isReturn ? '返品ID' : '取引ID'
    return [
      this._headerLogoElement(),
      this.feedElement(),
      ...this.multiLineTextElements(message, 'center'),
      this.feedElement(),
      ...this._titleElements(cart),
      this.feedElement(),
      ...this.shopNameElements(),
      this.leftTextElement(`${idLabel}:${cart.posOrderNumber}`),
      this.leftTextElement(this.formatDateTime(cart.createdAt)),
      this.staffElement(cart.staff.name, 'left'),
      this.feedElement()
    ]
  }

  _receiptTitle (cart) {
    if (cart.isReturn) {
      return '返品レシート'
    } else if (cart.isCancel) {
      return 'レジマイナスレシート'
    }
    return undefined
  }

  _titleElements (cart) {
    let title = this._receiptTitle(cart)
    return [
      this._trainingModeTitle(cart),
      title && {
        element: 'text',
        text: title,
        align: 'center',
        tags: ['title']
      }
    ]
  }

  _headerLogoElement () {
    if (this._headerLogoEnabled) {
      return {
        element: 'image',
        key: 'order_header_logo'
      }
    }
    return undefined
  }

  _footerLogoElement () {
    if (this._footerLogoEnabled) {
      return {
        element: 'image',
        key: 'order_footer_logo'
      }
    }
    return undefined
  }

  _subtotalElements (cart) {
    if (cart.isTaxfreeApplied) {
      const elements = [
        this._3columnElement(
          '小計',
          this.formatCount(this._isPrintZeroProduct
            ? cart.totalQuantity.toString()
            : cart.totalQuantityWithoutZeroProduct.toString()),
          this.formatMoney(cart.totalItemsBase, cart.currency)),
        this.feedElement(),
        this.combineLeftRightElement(
          '一般品 合計', this.formatMoney(cart.taxfreeGeneralsTotalTaxless, cart.currency)),
        this.combineLeftRightElement(
          cart.isTaxfreeAppliedForGeneral ? '   (免税額' : '   消費税',
          this.formatMoney(cart.taxfreeGeneralsTotalTax, cart.currency) +
          (cart.isTaxfreeAppliedForGeneral ? ')' : '')
        ),
        this.combineLeftRightElement(
          '消耗品 合計', this.formatMoney(cart.taxfreeExpendablesTotalTaxless, cart.currency)),
        this.combineLeftRightElement(
          cart.isTaxfreeAppliedForExpendable ? '   (免税額' : '   消費税',
          this.formatMoney(cart.taxfreeExpendablesTotalTax, cart.currency) +
          (cart.isTaxfreeAppliedForExpendable ? ')' : '')
        ),
        this.combineLeftRightElement(
          '免税対象外 合計', this.formatMoney(cart.taxfreeNotApplicablesTotalTaxless, cart.currency)),
        this.combineLeftRightElement(
          '   消費税', this.formatMoney(cart.taxfreeNotApplicablesTotalTax, cart.currency))
      ]
      return elements
    } else {
      return [
        this._3columnElement(
          '小計',
          this.formatCount(this._isPrintZeroProduct
            ? cart.totalQuantity.toString()
            : cart.totalQuantityWithoutZeroProduct.toString()),
          this.formatMoney(cart.totalItemsBase, cart.currency)),
        ...this.discountElements(cart),
        cart.tax.isTaxIncluded
          ? null : this.combineLeftRightElement('消費税', this.formatMoney(cart.totalTax, cart.currency)),
        ...this.taxContent(this.formatMoney(cart.discountedStandardTaxableTotalTaxless, cart.currency)
          , this.formatMoney(cart.standardTotalTax, cart.currency), cart.standardTaxRate),
        ...this.taxContent(this.formatMoney(cart.discountedReducedTaxableTotalTaxless, cart.currency)
          , this.formatMoney(cart.reducedTotalTax, cart.currency), cart.reducedTaxRate),
        this._displayIssuerCode(this._issureCode),
        this.combineLeftRightElement(
          '', this.receiptComment)
      ]
    }
  }

  _displayIssuerCode (issuerCode) {
    if (issuerCode !== null) {
      return this.combineLeftRightElement(
        '', '事業者番号 ' + issuerCode)
    }
  }

  /**
   * 小計割引・オーナー割引の表示
   * @param {*} cart 
   */
  discountElements (cart) {
    if (cart.discountOrder === DISCOUNT_ORDER.OWNER_DISCOUNT_FISRT) {
      return [
        this._ownerDiscountElement(cart),
        this._subtotalDiscountElement(cart)
      ]
    }
    return [
      this._subtotalDiscountElement(cart),
      this._ownerDiscountElement(cart)
    ]
  }

  /**
   *
   * @param {Cart} cart
   * @returns {null}
   * @private
   */
  _subtotalDiscountElement (cart) {
    if (!cart.discount) {
      return null
    }

    const discount = cart.discount
    if (discount.discountType === DISCOUNT_TYPE.RATE) {
      return this._3columnElement(
        '小計割引',
        discount.value.mul(100).toString() + I18n.t('promotion.percent_off'),
        this.formatMoney(cart.subtotalDiscountAmount.mul(-1), cart.currency))
    } else {
      return this.combineLeftRightElement(
        '小計値引',
        this.formatMoney(cart.subtotalDiscountAmount.mul(-1), cart.currency))
    }
  }

  _ownerDiscountElement (cart) {
    if (!cart.ownerDiscount) {
      return null
    }
    const discount = cart.ownerDiscount
    return this._3columnElement(
      I18n.t('promotion.owner_discount'),
      discount.value.mul(100).toString() + I18n.t('promotion.percent_off'),
      this.formatMoney(cart.ownerDiscountAmount.mul(-1), cart.currency))
  }

  /**
   *
   * @param {Cart} cart
   * @returns {null|*|Array|List}
   * @protected
   */
  paymentsElements (cart) {
    let countGiftWithChange = 0
    let countGiftWithoutChange = 0
    return cart.activePayments.map(payment => {
      if (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT &&
        payment.paymentMethodType !== PAYMENT_METHOD_TYPE.GIFT_SURPLUS) {
        if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE) {
          countGiftWithChange++
        } else if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE) {
          countGiftWithoutChange++
        }
        let amount = payment.amount
        // 現金の場合のみおつりを引く
        if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH) {
          amount = payment.amount.sub(
            CartPayment.calculateChange(cart, payment, countGiftWithChange, countGiftWithoutChange)
          )
        }
        return this.combineLeftRightElement(
          payment.name,
          this.formatMoney(amount, cart.currency)
        )
      }
    })
  }

  /**
   *
   * @param {Cart} cart
   * @private
   * ポイントは下記2種類に分けて管理
   * exchangePoints : 交換期ポイント => 入会月を基準にした昨年度に取得したポイント数
   * depositPoints : 積立期ポイント => 入会月を基準にした今年度に取得したポイント数
   * exchangeEffectiveDate : 交換期ポイント有効期限
   * depositEffectiveDate : 積立期ポイント有効期限
   */
  _customerElements (cart) {
    let elements = []
    if (cart.customer) {
      let ownedPoints
      let exchangePoints
      let depositPoints

      const currentPoints = cart.customer.customer_activity.points ? cart.customer.customer_activity.points : 0
      const currentExchangePoints = cart.customer.customer_activity.exchange_points ? cart.customer.customer_activity.exchange_points : 0
      const currentDepositPoints = cart.customer.customer_activity.deposit_points ? cart.customer.customer_activity.deposit_points : 0
      const exchangeEffectiveDate = cart.customer.customer_activity.exchange_points_effective_date ? cart.customer.customer_activity.exchange_points_effective_date : ''
      const depositEffectiveDate = cart.customer.customer_activity.deposit_points_effective_date ? cart.customer.customer_activity.deposit_points_effective_date : ''

      if (cart.isCancel) {
        ownedPoints = currentPoints

        exchangePoints = currentExchangePoints
        depositPoints = currentDepositPoints
      } else if (cart.isReturn) {
        ownedPoints = currentPoints + cart.usePoint - cart.obtainedPoint

        // 返品時、利用ポイントは積立ポイントに戻される(加算される)
        exchangePoints = currentExchangePoints
        depositPoints = currentDepositPoints + cart.usePoint - cart.obtainedPoint
      } else {
        ownedPoints = currentPoints - cart.usePoint + cart.obtainedPoint

        if (currentExchangePoints < cart.usePoint) {
          exchangePoints = 0
          depositPoints = currentDepositPoints - (cart.usePoint - currentExchangePoints) + cart.obtainedPoint
        } else {
          exchangePoints = currentExchangePoints - cart.usePoint
          depositPoints = currentDepositPoints + cart.obtainedPoint
        }
      }
      elements.push(this.feedElement())
      elements.push(this.combineLeftRightElement(I18n.t('order.customer_code'), this.maskingCustomerCode(cart.customer.customer_code) || ''))
      elements.push(this.combineLeftRightElement(this._pointTitle(cart), formatUnit(cart.obtainedPoint, 'pt')))
      elements.push(this.combineLeftRightElement(I18n.t('order.owned_points'), formatUnit(ownedPoints, 'pt')))
      elements.push(this.combineLeftRightElement('', ''))
      this.invalidatePointElements(elements, exchangePoints, depositPoints, exchangeEffectiveDate, depositEffectiveDate)
    }
    return elements
  }

  _pointTitle (cart) {
    if (cart.isReturn) {
      return I18n.t('order.subtracted_point')
    } else {
      return I18n.t('order.obtained_points')
    }
  }

  /**
   * 
   * @param {String} customerCode 
   */
  maskingCustomerCode (customerCode) {
    /** 会員番号下４桁 */
    const displayCode = customerCode.substr(-4, 4)
    const CUSTOMER_CODE_LENGTH = 16
    return displayCode.padStart(CUSTOMER_CODE_LENGTH, '*')
  }

  /**
   * 交換期ポイント・積立ポイントの表示(各ポイントの有無によってレシート表示を変更)
   * 原則:交換期ポイント(exchangePoints)を失効ポイント1、積立ポイント(depositPoints)を失効ポイント2として表示
   * 例外:交換期ポイントが0の場合は,積立ポイント(depositPoints)を失効ポイント1として表示
   */
  invalidatePointElements (elements, exchangePoints, depositPoints, exchangeEffectiveDate, depositEffectiveDate) {
    if (exchangePoints > 0) {
      this.setFirstInvalidatePoint(elements, formatUnit(exchangePoints, 'pt'), exchangeEffectiveDate)
      if (depositPoints > 0) {
        this.setSecondInvalidatePoint(elements, formatUnit(depositPoints, 'pt'), depositEffectiveDate)
      }
    } else if (depositPoints > 0) {
      this.setFirstInvalidatePoint(elements, formatUnit(depositPoints, 'pt'), depositEffectiveDate)
    }
  }

  /**
   * 引数のポイント/日時をレシート上の失効ポイント1/有効期限1として表示
   */
  setFirstInvalidatePoint (elements, points, effectiveDate) {
    const isFirstPoint = true
    this.setInvalidatePoint(isFirstPoint, elements, points, effectiveDate)
  }

  /**
   * 引数のポイント/日時をレシート上の失効ポイント2/有効期限2として表示
   */
  setSecondInvalidatePoint (elements, points, effectiveDate) {
    const isFirstPoint = false
    this.setInvalidatePoint(isFirstPoint, elements, points, effectiveDate)
  }

  /**
   * 失効ポイント/有効期限をレシート表示
   */
  setInvalidatePoint (isFirst, elements, points, effectiveDate) {
    const effectiveDateTitle = isFirst ? I18n.t('order.first_point_limit') : I18n.t('order.second_point_limit')
    const pointTitle = isFirst ? I18n.t('order.first_invalidate_points') : I18n.t('order.second_invalidate_points')
    const frontSpace = ' '.repeat((effectiveDateTitle.length - pointTitle.length) * 2)

    elements.push(this.combineLeftRightElement(effectiveDateTitle, effectiveDate))
    elements.push(this.combineLeftRightElement(frontSpace + pointTitle, points))
  }

  taxStampElements (cart) {
    if (!cart.needsTaxStamp) {
      return []
    }
    if (this._isPrintTaxStamp) {
      return [
        this.feedElement(),
        ...this.multiLineTextElements(
          this._stampWithTaxofficeText, 'right', ['stamp'])
      ]
    } else {
      return [
        this.feedElement(),
        ...this.multiLineTextElements(this._stampText, 'right', ['stamp'])
      ]
    }
  }

  get _stampText () {
    return `
┏━━━━━━┓
┃            ┃
┃  収    入  ┃
┃            ┃
┃  印    紙  ┃
┃            ┃
┗━━━━━━┛`.slice(1)
  }

  get _stampWithTaxofficeText () {
    return `
┏━━━━━━┓
┃印紙税申告納┃
┃            ┃
┃付につき${this._taxOfficeName}┃
┃            ┃
┃税務署承認済┃
┗━━━━━━┛`.slice(1)
  }

  _strongTextElement (left, right) {
    return {
      element: 'combine_left_right',
      left,
      right,
      width: 2,
      height: 1
    }
  }

  _originNumberElement (cart) {
    if (cart.isReturn) {
      return this.leftTextElement(`元取引ID: ${cart.originPosOrderNumber || ''}`)
    }
  }

  _subTotalElement (cart) {
    return [
      this.feedElement(),
      ...this._subtotalElements(cart),
      this.textLineElement() // ----------------------------
    ]
  }

  /**
   * 合計金額欄
   * @param {Cart} cart
   * @return {*[]}
   * @private
   */
  _totalElement (cart) {
    const includingTaxElement = cart.tax.isTaxIncluded
      ? this.combineLeftRightElement(' (うち消費税', this.formatMoney(cart.totalTax, cart.currency) + ') ')
      : null
    if (cart.isReturn) {
      return [
        this._strongTextElement('返金合計', this.formatMoney(cart.totalTaxed, cart.currency)),
        includingTaxElement
      ]
    } else {
      return [
        this._strongTextElement('合計', this.formatMoney(cart.totalTaxed, cart.currency)),
        includingTaxElement,
        this.combineLeftRightElement('お預かり', this.formatMoney(cart.deposit, cart.currency)),
        this._strongTextElement('おつり', this.formatMoney(cart.change, cart.currency))
      ]
    }
  }

  _paymentTitleElement (cart) {
    if (cart.isReturn) {
      return this.leftTextElement('ご返金内訳')
    }
  }

  /**
   *
   * @param {Cart | OrderedCart} cart
   * @return {*[]}
   * @private
   */
  _lastElement (cart) {
    const isCancelOrReturn = cart.isReturn || cart.isCancel
    return [
      ...this._cancelInfoElements(cart),
      ...(isCancelOrReturn ? [] : this.taxStampElements(cart)),
      this.feedElement(),
      this.barcodeElement(cart.posOrderNumber || ''),
      ...(isCancelOrReturn ? [] : [
        this.feedElement(),
        ...this.multiLineTextElements(this._footerMessage, 'center'),
        this.feedElement(),
        this._footerLogoElement()
      ])
    ]
  }

  _cancelInfoElements (cart) {
    if (!cart.isCancel) { return [] }
    return [
      this.rightTextElement('レジマイナス担当者:' + cart.canceledStaffName),
      this.rightTextElement(this.formatDateTime(cart.canceledAt))
    ]
  }
}
