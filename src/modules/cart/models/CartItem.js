import { Record, List } from 'immutable'
import * as _ from 'underscore'
import Decimal from 'decimal.js'
import logger from 'common/utils/logger'
import CartItemDiscount from './CartItemDiscount'
import OperationError from '../../../common/errors/OperationError'
import { BUNDLE_TYPE, DISCOUNT_TYPE, PROMOTION_TYPE } from '../../promotion/models'
import { floor } from '../../../common/models/MoneyAmount'
import I18n from 'i18n-js'

/**
 * @class CartItem
 * @property {CartItemProduct} product
 * @property {int} quantity
 * @property {Immutable.List<CartItemDiscount>} discounts
 * @property {string} currency
 * @property {boolean} isTaxfreeApplied
 * @property {string} instanceUniqueKey
 * @property {boolean} isBundled
 * @property {Record} extendsData
 * @property {integer|null} sortOrder
 * @property {boolean|null} isProrated
 */

const CartItemRecord = Record({
  product: null,
  quantity: 1,
  discounts: List(),
  currency: 'jpy',
  isTaxfreeApplied: false,
  instanceUniqueKey: null,
  isBundled: false,
  extendsData: null,
  sortOrder: null,
  isProrated: false
})

export const CART_ITEM_MAX_QUANTITY = 9999

export default class CartItem extends CartItemRecord {
  /**
   * 明細単価(売価変更後、割引前)
   * @return {*}
   */
  get price () {
    return this.product.price
  }

  /**
   * 販売価格
   * Sales price
   * @return {Decimal}
   */
  get salesPrice () {
    return this.price.sub(this.discountAmount)
  }

  /**
   * 販売価格（値割引按分額を含む）
   * Sales price
   * @return {Decimal}
   */
  get salesPriceWithProrate () {
    return this.price.sub(this.discountAmountWithProrate)
  }

  /**
   * 商品小計
   * @return {Decimal}
   */
  get salesTotal () {
    return this.salesPrice.mul(this.quantity).sub(this.bundleSurplus)
  }

  /**
   * 商品小計(税抜)
   */
  get salesTotalTaxless () {
    return this.product.tax.calculateTaxless(this.salesTotal, this.currency)
  }

  /**
   * マスタ売価(ズバリ価格によって可変)
   * List price
   * @return {Decimal}
   */
  get listPrice () {
    return this.product.originalPrice
  }

  /**
   * 当初のマスタ価格(不変)
   */
  get immutableOriginalPrice () {
    return this.product.immutableOriginalPrice
  }

  /**
   * 販売価格変更(価変(無))したか
   * @return {boolean}
   */
  get isChangedPrice () {
    return this.product.priceIsChanged
  }

  /**
   * 大元の価格変更(ズバリ価格)されたか
   */
  get isChangedOriginalPrice () {
    return this.product.originalPriceIsChanged
  }

  /**
   * 値引額（按分額を含まない）
   * @return {Decimal}
   */
  get amountDiscountAmount () {
    return CartItemDiscount.amountDiscountTotalAmount(this.discounts)
  }

  /**
   * 値引額（値割引き按分額を含む）
   * @return {Decimal}
   */
  get amountDiscountAmountWithProrate () {
    return CartItemDiscount.amountDiscountTotalAmountWithProrate(this.discounts)
  }

  /**
   * 個別値引額
   * @return {Decimal}
   */
  get manualAmountDiscountAmount () {
    return CartItemDiscount.amountDiscountTotalAmount(this.discounts.filter(discount => discount.isManual))
  }

  /**
   * 割引額（按分額を含まない）
   * @return {Decimal}
   */
  get rateDiscountAmount () {
    return CartItemDiscount.rateDiscountTotalAmount(this.discounts, this.price, this.currency)
  }

  /**
   * 割引額（値割引き按分額を含む）
   * @return {Decimal}
   */
  get rateDiscountAmountWithProrate () {
    return CartItemDiscount.rateDiscountTotalAmountWithProrate(this.discounts, this.price, this.currency)
  }

  /**
   * 割引率合計
   * @return {Decimal}
   */
  get rateDiscountRate () {
    return CartItemDiscount.discountRateTotal(this.discounts)
  }

  /**
   * 割引率合計(%)
   * @return {Decimal}
   */
  get rateDiscountPercent () {
    return this.rateDiscountRate.mul(100)
  }

  get manualRateDiscountRate () {
    return CartItemDiscount.discountRateTotal(this.discounts.filter(discount => !discount.isSale))
  }

  /**
   * 値割引額合計（按分額を含まない）
   * @return {Decimal}
   */
  get discountAmount () {
    return this.amountDiscountAmount.add(this.rateDiscountAmount)
  }

  /**
   * 値割引額合計（値割引き按分額を含む）
   * @return {Decimal}
   */
  get discountAmountWithProrate () {
    return this.amountDiscountAmountWithProrate.add(this.rateDiscountAmountWithProrate)
  }

  /**
   * 入力可能な手動値引きの最大値
   * @returns {Decimal}
   */
  get maxManualDiscountAmount () {
    return this.salesPrice.add(this.manualAmountDiscountAmount)
  }

  /**
   * 入力可能な手動割引の最大値
   */
  get maxManualDiscountRate () {
    const sale = this.discounts.find(discount => discount.isSale)
    if (sale) {
      if (sale.discountType === DISCOUNT_TYPE.RATE) {
        return Decimal(1).sub(sale.value)
      } else {
        const maxDiscountAmount = this.listPrice.sub(sale.value)
        return floor(maxDiscountAmount.div(this.listPrice), 2)
      }
    } else {
      return Decimal(1)
    }
  }

  get validateCanApplyManualDiscount (): ValidationResult {
    if (!this.product.isDiscountable) {
      return {
        isValid: false
      }
    }
    if (this.isChangedPrice || this.isChangedOriginalPrice) {
      return {
        isValid: false,
        message: I18n.t('message.B-02-E008')
      }
    }
    return {
      isValid: true
    }
  }

  /**
   * バンドルの按分値あまり
   */
  get bundleSurplus () {
    const bundleDiscount = this.discounts.find(discount => {
      return discount.promotionType === PROMOTION_TYPE.BUNDLE
    })
    return bundleDiscount ? bundleDiscount.bundleSurplus : Decimal(0)
  }

  /**
   * バンドルが成立した場合の記録用の価格
   */
  get bundlePrice () {
    const bundleDiscount = this.discounts.find(discount => {
      return discount.promotionType === PROMOTION_TYPE.BUNDLE
    })
    if (bundleDiscount) {
      if (bundleDiscount.bundleType === BUNDLE_TYPE.SELECTION) {
        return bundleDiscount.bundlePrice.sub(bundleDiscount.bundleSurplus)
      } else {
        return bundleDiscount.bundlePrice
      }
    } else {
      return null
    }
  }

  /**
   * バンドルが成立した場合の記録用の価格（値割引き按分額を含む）
   */
  get bundlePriceWithProrate () {
    const bundleDiscount = this.discounts.find(discount => {
      return discount.isBundle
    })

    const prorateDiscounts = this.discounts.filter(discount => {
      return discount.isSubtotal
    })

    if (bundleDiscount) {
      let bundlePrice = new Decimal(0)
      if (bundleDiscount.bundleType === BUNDLE_TYPE.SELECTION) {
        bundlePrice = bundleDiscount.bundlePrice.sub(bundleDiscount.bundleSurplus)
      } else {
        bundlePrice = bundleDiscount.bundlePrice
      }

      if (prorateDiscounts.size > 0) {
        // 小計値割引が按分されていれば按分額も引く
        prorateDiscounts.forEach(prorateDiscount => {
          bundlePrice = bundlePrice.sub(prorateDiscount.value)
        })
        return bundlePrice
      }
    } else {
      return null
    }
  }

  /**
   * バンドルが成立した場合の、見せかけの価格
   */
  get bundlePriceForDisplay () {
    const bundleDiscount = this.discounts.find(discount => {
      return discount.promotionType === PROMOTION_TYPE.BUNDLE
    })
    return bundleDiscount ? bundleDiscount.bundlePriceForDisplay : undefined
  }

  /**
   * バンドルが成立した場合の、見せかけの価格
   */
  get bundlePriceForDisplayTotal () {
    return this.bundlePriceForDisplay && this.bundlePriceForDisplay.mul(this.quantity)
  }

  /**
   * 小計値割引按分によって追加された値割引データ
   */
  get subtotalDiscount () {
    return CartItemDiscount.getSubtotalDiscount(this.discounts)
  }

  /**
   * オーナー割引按分によって追加された値割引データ
   */
  get ownerDiscount () {
    return CartItemDiscount.getOwnerDiscount(this.discounts)
  }

  /**
   * Check if it is possible to decrease the quantity
   * @param {int} quantity
   * @return {boolean}
   */
  canDecrease (quantity = 1) {
    if (quantity < 0) throw new RangeError()
    return this.quantity - quantity >= 0
  }

  /**
   * Increase the quantity
   * @param {int} quantity
   * @return {CartItem}
   */
  increase (quantity = 1) {
    return this.set('quantity', this.quantity + quantity)
  }

  /**
   * Decrease the quantity
   * @param {int} quantity
   * @return {CartItem}
   */
  decrease (quantity = 1) {
    if (!this.canDecrease(quantity)) throw new OperationError()
    return this.set('quantity', this.quantity - quantity)
  }

  changeQuantity (quantity) {
    return this.set('quantity', quantity)
  }

  /**
   * @param {CartItemDiscount} discount
   * @return {CartItem}
   */
  applyDiscount (discount) {
    return this.set('discounts', CartItemDiscount.applyToDiscounts(discount, this.discounts))
  }

  /**
   * 小計値引・オーナー割引を按分された額(商品小計への按分額)
   */
  assignSubTotalDiscount (discountAmout) {
    return this.set('assignedSubTotalDiscount', this.assignedSubTotalDiscount.add(discountAmout))
  }

  /**
   * 自身の持っている商品情報から、適用可能なセール情報があれば適用する。
   * @param {List<string> scannedPromotionBarcodes}
   * @return {CartItem}
   */
  applySaleDiscount (scannedPromotionBarcodes = List()) {
    const filteredSales = this.product.sales.filter(sale => !sale.barcode || sale.barcode.length == 0 || scannedPromotionBarcodes.includes(sale.barcode))
    if (filteredSales.count() > 0) {
      return this.applyDiscount(CartItemDiscount.createSaleDiscount(filteredSales.get(0)))
    } else {
      return this.clearSaleDiscounts()
    }
  }

  clearSaleDiscounts () {
    return this.set('discounts', this.discounts.filter(discount => discount.promotionType !== PROMOTION_TYPE.SALE))
  }

  cancelDiscountByDiscountType (discountType) {
    return this.set('discounts', this.discounts
      .filter(discount => discount.discountType !== discountType))
  }

  clearDiscounts () {
    return this.set('discounts', List())
  }

  /**
   * バンドル適用するにあたって、不要な割引を取り除く
   * @return {CartItem}
   */
  resetForBundle () {
    return this
      .set('discounts', this.discounts.filter(discount => discount.promotionType === PROMOTION_TYPE.BUNDLE))
      .set('product', this.product.resetPrice())
  }

  /**
   * バンドル適用解除する。バンドル未適用であれば何もしない
   * @return {CartItem}
   */
  resetForUnbundle () {
    if (this.isBundled) {
      return this
        .set('isBundled', false)
        .set('discounts', List())
        .set('product', this.product.resetTaxfreeType())
    } else {
      return this
    }
  }

  /**
   * 売価変更(価変(無))
   * @return {CartItem}
   */
  changePrice (price) {
    return this.set('product', this.product.changePrice(price))
  }

  /**
   * 大元の売価変更(ズバリ価格)
   */
  changeOriginalPrice (price) {
    return this.set('product', this.product.changeOriginalPrice(price))
  }

  /**
   * 免税適用されたか否かをセットする
   * @param value
   * @return {CartItem}
   */
  setIsTaxfreeApplied (value) {
    return this.set('isTaxfreeApplied', value)
  }

  overwriteTaxfreeType (taxfreeType) {
    return this.set('product', this.product.overwriteTaxfreeType(taxfreeType))
  }

  setSortOrder (sortOrder) {
    return this.set('sortOrder', sortOrder)
  }

  setIsProrated (isProrated) {
    return this.set('isProrated', isProrated)
  }

  get discountsLabel () {
    return CartItemDiscount.discountsToLabel(this.discounts, this.price, this.currency)
  }

  /**
   * 個数でCartItemを分割します。takeには新しいinstanceUniqueKeyが割り当てられます。
   * @param {integer} quantity
   * @returns {{take: CartItem, remain: CartItem}}
   */
  takeByQuantity (quantity) {
    if (quantity > this.quantity) {
      console.error('Inputted quantity is too big.')
    }
    let take = this.set('quantity', quantity)
    if (this.instanceUniqueKey) {
      take = take.set('instanceUniqueKey', CartItem.generateInstanceUniqueKey())
    }
    return {
      take,
      remain: this.set('quantity', this.quantity - quantity)
    }
  }

  /**
   * CartItemのリストの最初の方から、目的のquantity分のitemを取り出し、残りとともに返す
   * @param {List.<CartItem>} items
   * @param {integer} quantity
   * @returns {{take: List.<CartItem>, remain: List.<CartItem>}}
   */
  static takeItemsByQuantity (items, quantity) {
    let takes = List()
    let remains = items
    let currentQuantity = 0
    while (remains.count() > 0) {
      const item = remains.first()
      remains = remains.shift()
      const surplusCount = currentQuantity + item.quantity - quantity
      currentQuantity = currentQuantity + item.quantity
      if (surplusCount > 0) {
        const { take: take_, remain: remain_ } = item.takeByQuantity(item.quantity - surplusCount)
        takes = takes.push(take_)
        remains = remains.unshift(remain_)
        break
      } else if (surplusCount === 0) {
        takes = takes.push(item)
        break
      } else {
        takes = takes.push(item)
      }
    }
    return [takes, remains]
  }

  /**
   * 一つにまとめられる明細をまとめた状態の明細一覧を返す。
   * まとめられるかどうかは、CartItem.canMergeメソッドで判定に依存する。
   * @param items
   * @return {*|List<T>|List<any>}
   */
  static compactItems (items) {
    const itemArray = items.toArray()
    const itemsWithIndex = itemArray.map((item, index) => ({item, index}))
    const itemsByProductVariantId =
      _.groupBy(itemsWithIndex, item => item.item.product.productVariantId)

    for (const i in itemsByProductVariantId) {
      const groupedItems = itemsByProductVariantId[i]
      if (groupedItems.length > 1) {
        const checkedItems = [groupedItems.shift()]
        groupedItems.forEach(item => {
          const mergeableItem = checkedItems.find(checkedItem => checkedItem.item.canMerge(item.item))
          if (mergeableItem) {
            itemArray[item.index] = null
            mergeableItem.item = mergeableItem.item.merge(item.item)
            itemArray[mergeableItem.index] = mergeableItem.item
          } else {
            checkedItems.push(item)
          }
        })
      }
    }

    return List(_.compact(itemArray))
  }

  /**
   * @type {number}
   * @private
   */
  static _currentUniqueKeyCount = 0

  /**
   *
   * @returns {string}
   */
  static generateInstanceUniqueKey () {
    return 'cart-item-key-' + ('00000000000' + CartItem._currentUniqueKeyCount++).slice(-11)
  }

  /**
   *
   * @param {Array<CartItem>} items
   * @returns {string}
   * @private
   */
  static _newestInstanceUniqueKey (items) {
    if (items.length === 0) {
      console.error('items is empty')
    }
    return items.map(item => item.instanceUniqueKey).reduce((key1, key2) => (
      key1 > key2 ? key1 : key2
    ))
  }

  /**
   *
   * @param {Array<CartItem>} items
   * @returns {string}
   * @private
   */
  static _oldestInstanceUniqueKey (items) {
    if (items.length === 0) {
      console.error('items is empty')
    }
    return items.map(item => item.instanceUniqueKey).reduce((key1, key2) => (
      key1 < key2 ? key1 : key2
    ))
  }

  /**
   * カート値割引情報から按分用値割引情報を取得
   * 
   * @param {CartItem} item 
   * @param {CartDiscount} cartDiscount 
   * @return {CartItemDiscount}
   */
  static getProrateDiscount (item) {
    return CartItemDiscount.getSubtotalDiscount(item.discounts)
  }

  /**
   * anotherItemとまとめることが可能か検査する。 Check if can merge the anotherItem
   * @param {CartItem} anotherItem
   * @returns {boolean}
   */
  canMerge (anotherItem) {
    return this.product.productVariantId === anotherItem.product.productVariantId &&
      this.product.price.equals(anotherItem.product.price) &&
      (!this.isBundled && !anotherItem.isBundled) &&
      this.discounts.equals(anotherItem.discounts)
    // TODO 値割引など考慮
  }

  /**
   *
   * @param {CartItem} anotherItem
   * @returns {CartItem}
   */
  merge (anotherItem) {
    if (!this.canMerge(anotherItem)) {
      logger.error('[CartItem.merge] cannot merge')
    }
    return this
      .set('quantity', this.quantity + anotherItem.quantity)
      .set('instanceUniqueKey', CartItem._oldestInstanceUniqueKey([this, anotherItem]))
  }

  /**
   *
   * @return ValidationResult
   */
  get validateCanChangePrice () {
    if (CartItemDiscount.getManualDiscount(this.discounts)) {
      return {
        isValid: false,
        message: I18n.t('message.B-02-E008')
      }
    }
    if (CartItemDiscount.getSaleDiscount(this.discounts)) {
      return {
        isValid: false,
        message: I18n.t('message.B-02-E009')
      }
    }
    return {
      isValid: true
    }
  }
}
