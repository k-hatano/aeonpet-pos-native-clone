import ProductRepository from 'modules/product/repositories/ProductRepository'
import AlertView from 'common/components/widgets/AlertView'
import I18n from 'i18n-js'
import { PREV_SEARCH_COND, CHANGE_PRICE_TYPE } from 'modules/product/models'
import * as productActions from 'modules/product/actions'
import logger from 'common/utils/logger'
import Modal from 'common/components/widgets/Modal'
import AmountPadModal from 'common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from 'common/components/elements/AmountPad'
import CartManager from './CartManager'
class BooksProductSearcher {
  constructor () {
    this.MAX_PRODUCT_COUNT = 100
  }

  async searchProducts (dispatch, cart, barcode, conditionFlg, isBarcode) {
    let productArray = await ProductRepository.findProductsBySearchWord(barcode, conditionFlg, isBarcode)
    if (!productArray.length) {
      productArray = await ProductRepository.findVariantsBySearchWord(barcode, conditionFlg)
    }

    const searchResult = {
      isChangedTabIndex: false,
      isUpdateCart: false,
      lastScannedBarcode: undefined
    }
    switch (productArray.length) {
      case 0:
        this._showProductNotFound(barcode)
        break
      case 1:
        const product = productArray.shift()
        if (this._isPriceRequiered(product)) {
          Modal.close()
          await this._openAmountPadModal(dispatch, cart, product)
        } else {
          searchResult.lastScannedBarcode = barcode
        }
        break
      default:
        if (productArray.length > this.MAX_PRODUCT_COUNT) {
          productArray = this._cutOver100Items(productArray)
        }
        searchResult.isChangedTabIndex = true
        this._showProductList(dispatch, productArray)
    }
    return searchResult
  }

  _openAmountPadModal (dispatch, cart, product) {
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.CASH,
      onComplete: async amount => {
        Modal.close()
        await new CartManager().updateCart(dispatch, cart, product, amount)
      },
      title: product.name
    })
  }

  _cutOver100Items (productArray) {
    productArray = productArray.slice(0, 100)
    AlertView.show(I18n.t('common.over_hundred_items'))
    return productArray
  }

  _showProductList (dispatch, productArray) {
    dispatch(productActions.listProducts(productArray))
    dispatch(productActions.listOriginProducts(productArray))
    dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
    dispatch(productActions.setSearchLayoutFlg(true))
  }

  _isPriceRequiered (product) {
    return product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED
  }

  _showProductNotFound (barcode) {
    AlertView.show(I18n.t('product.not_exist'))
    logger.warning(I18n.t('product.not_affected_scan') + '：' + barcode)
    logger.debug('[_searchProductVariants] not find product')
  }
}

export default new BooksProductSearcher()
