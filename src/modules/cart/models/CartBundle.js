import { Record, List } from 'immutable'
import Decimal from 'decimal.js'
import * as _ from 'underscore'
import logger from 'common/utils/logger'
import CartItem from './CartItem'
import { BUNDLE_TYPE, ESTABLISHMENT_CONDITION } from '../../promotion/models'
import { floorByCurrency } from '../../../common/models/Currency'
import CartItemDiscount from './CartItemDiscount'
import { TAXFREE_TYPE } from '../../../common/models/Tax'

const CartBundleRecord = Record({
  id: '',
  name: '',
  items: List(),
  priority: 0,
  bundleType: 0,
  number: 0,
  extendsData: null
})

/**
 * @class CartBundle
 * @property {string} id
 * @property {string} name
 * @property {List<CartItem>} items
 * @property {integer} priority
 * @property {integer} bundleType
 * @property {integer} number
 */
export default class CartBundle extends CartBundleRecord {
  /**
   *
   * @param {BundleEntity} bundle
   * @param items
   * @param number
   * @param extendsData
   */
  constructor (bundle, items, number, extendsData = null) {
    super({
      id: bundle.id,
      name: bundle.name,
      items: List(items),
      bundleType: bundle.bundle_type,
      number,
      priority: bundle.priority,
      extendsData
    })
  }

  /**
   *
   * @return {Decimal}
   */
  get amount () {
    return this.items.reduce((sum, item) => item.salesTotal.add(sum), Decimal(0))
  }

  get bundleDiscountTotal () {
    return this.items.reduce((sum, item) =>
      item.discountAmount.mul(item.quantity).add(item.bundleSurplus).add(sum), Decimal(0))
  }

  /**
   * @return {BundleEntity}
   */
  get entity () {
    return {
      id: this.id,
      name: this.name,
      priority: this.priority,
      bundle_type: this.bundleType
    }
  }

  addItem (item) {
    return this.set('items', this.items.push(item))
  }

  get isTaxfreeApplied () {
    // バンドル内は免税区分が揃えられるため、最初の要素を見るだけで良い
    return this.items.first().isTaxfreeApplied
  }

  /**
   *
   * @param {List<CartBundle>} cartItems
   * @param {List<string>} scannedPromotionBarcodes
   * @returns {{bundles: (List<CartBundle>), unbundledItems: (List<CartItem>), riichiMessage: (string)}}
   */
  static applyToCartItems (cartItems, scannedPromotionBarcodes = List()) {
    let items = this._sortItemsForBundle(cartItems)
    let bundles = List()
    const bundleCandidates = CartBundle._summaryBundleCandidatesFromCartItems(items)
      .filter(bundle => (!bundle.barcode || bundle.barcode.length == 0 || scannedPromotionBarcodes.includes(bundle.barcode)))
    bundleCandidates.forEach(bundle => {
      let number = 1
      let isApplied = true
      while (isApplied) {
        let remainingItems = items
        let appliedPatterns = List()
        bundle.bundle_patterns.forEach(pattern => { // パターンの適用順序は現状未定義
          const [takes, remains] = CartBundle._takeItemsForPattern(remainingItems, pattern)
          if (takes) {
            appliedPatterns = appliedPatterns.push({
              pattern,
              items: takes.map(item => item.resetForBundle())
            })
            remainingItems = remains
          } else {
            isApplied = false
          }
        })
        if (isApplied) {
          let appliedItems = List(_.flatten(appliedPatterns.map(pattern => pattern.items.toArray()).toArray()))
          appliedItems = CartItem.compactItems(
            appliedItems.map(item => item.set('isBundled', false).clearDiscounts()))
          const taxfreeType = this._taxfreeTypeFromItems(appliedItems)
          appliedItems = CartBundle._applyBundleDiscount(
            bundle, number, bundle.bundle_patterns[0].bundle_price, appliedItems)
            .map(item => item.set('isBundled', true).overwriteTaxfreeType(taxfreeType))
          bundles = bundles.push(new CartBundle(
            bundle,
            appliedItems,
            number))
          items = this._sortItemsForBundle(remainingItems)
          number++
        }
      }
    })

    const unbundledItems = items.map(item => item.resetForUnbundle())
    return {
      bundles,
      unbundledItems,
      riichiMessage: CartBundle._getBundleRiichMessage(bundles, unbundledItems, bundleCandidates)
    }
  }

  /**
   * バンドルパターンに必要なだけCartItemを取り出す
   * 数が足りない場合は、一つも取り出さない
   * @param items
   * @param pattern
   * @return {[null|List<CartItem>,List<CartItem>]} 取り出したCartItem(第１要素)、残りのCartItem(第２要素)のListの配列。数が足りない場合は第1要素がnullとなる。
   * @private
   */
  static _takeItemsForPattern (items, pattern) {
    const { targets, others } = CartBundle._getPatternTargetItems(items, pattern)
    if (!targets) {
      return [null, items]
    }

    const targetQuantity = targets.reduce((sum, item) => sum + item.quantity, 0)
    if (targetQuantity < pattern.threshold_quantity) {
      return [null, items]
    }

    let takes = null
    let remains = List()
    switch (pattern.establishment_condition) {
      case ESTABLISHMENT_CONDITION.EQUAL:
        [takes, remains] = CartItem.takeItemsByQuantity(targets, pattern.threshold_quantity)
        remains = remains.concat(others)
        break

      case ESTABLISHMENT_CONDITION.GREATER_EQUAL:
        takes = targets
        remains = others
        break

      default:
        logger.error('Invalid EstablishmentCondition')
    }
    return [takes, remains]
  }

  /**
   *
   * @param {Immutable.List<CartItem>} items
   * @param {BundlePatternEntity} pattern
   * @return {{targets: (Immutable.List<CartItem>), others: (Immutable.List<CartItem>)}}
   * @private
   */
  static _getPatternTargetItems (items, pattern) {
    const splittedItems = items.groupBy(item => {
      return item.product.tags.some(tag => tag.product_tag_id === pattern.product_tag_id)
        ? 'targets' : 'others'
    })
    const targets = splittedItems.get('targets')
    const others = splittedItems.get('others') || List()
    return { targets, others }
  }

  static _sortItemsForBundle (items) {
    return items.sort((a, b) => {
      const priceOrder = b.product.originalPrice.toNumber() - a.product.originalPrice.toNumber() // 価格の高い順
      if (priceOrder !== 0) return priceOrder
      if (a.product.sku < b.product.sku) { // 価格が同じならJANの若い方が優先
        return -1
      } else if (a.product.sku > b.product.sku) {
        return 1
      }

      // 既にバンドルが組まれているものを優先
      if (a.isBundled && !b.isBundled) {
        return -1
      } else if (!a.isBundled && b.isBundled) {
        return 1
      }

      // 価格変更されていないものを優先
      if (!a.isChangedPrice && b.isChangedPrice) {
        return -1
      } else if (a.isChangedPrice && !b.isChangedPrice) {
        return 1
      }

      // 割引がないものを優先
      if (a.discounts.size !== 0 && b.discounts.size === 0) {
        return -1
      } else if (a.discounts.size === 0 && b.discounts.size !== 0) {
        return 1
      }

      return 0
    })
  }

  static _summaryBundleCandidatesFromCartItems (cartItems) {
    const bundleCandidates = {}
    cartItems.forEach(item => {
      item.product.bundleCandidates.forEach(bundle => {
        bundleCandidates[bundle.id] = bundle
      })
    })

    const bundleCandidateArray = _.values(bundleCandidates)
    bundleCandidateArray.sort(this._compareBundlePriority)
    return bundleCandidateArray
  }

  /**
   *
   * @param {BundleEntity} bundle
   * @param {integer} number
   * @param {number} amount
   * @param {List<CartItem>} cartItems
   * @private
   */
  static _applyBundleDiscount (bundle, number, amount, cartItems) {
    switch (bundle.bundle_type) {
      case BUNDLE_TYPE.SELECTION:
        return CartBundle._applyBundleDiscountForSelection(bundle, number, Decimal(amount), cartItems)
      case BUNDLE_TYPE.BUY_X_GET_Y:
        return CartBundle._applyBundleDiscountForXY(bundle, number, null, cartItems)
      case BUNDLE_TYPE.FLATPRICE:
        return CartBundle._applyBundleDiscountForFlatplice(bundle, number, Decimal(amount), cartItems)
      case BUNDLE_TYPE.CASES:
        return CartBundle._applyBundleDiscountForCases(bundle, number, cartItems)
      default:
        logger.error('Invalid Bundle Type')
    }
  }

  /**
   *
   * @param {BundleEntity} bundle
   * @param {integer} number
   * @param {number} amount
   * @param {List<CartItem>} cartItems
   * @private
   */
  static _applyBundleDiscountForSelection (bundle, number, amount, cartItems) {
    let bundleDiscounts = cartItems.map(item => {
      return { item }
    })
    const totalPrice = cartItems.reduce((sum, item) => {
      return item.listPrice.mul(item.quantity).add(sum)
    }, Decimal(0))
    bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, totalPrice.sub(amount))
    return bundleDiscounts.map(bundleDiscount => {
      return bundleDiscount.item.applyDiscount(
        CartItemDiscount.createBundleDiscount(
          bundle,
          bundleDiscount.bundleDiscount,
          bundleDiscount.bundleSurplus || Decimal(0),
          bundleDiscount.item.product.originalPrice.sub(bundleDiscount.bundleDiscount),
          null,
          number
        )
      )
    })
  }

  static _compareBundlePriority (bundleA, bundleB) {
    return bundleA.priority - bundleB.priority // bundleTypeが同じならpriority昇順
  }

  static _compareCartItemPriorityForXY (a, b) {
    const priceOrder = a.product.originalPrice.toNumber() - b.product.originalPrice.toNumber() // 価格の安いものが優先
    if (priceOrder !== 0) return priceOrder
    if (a.product.sku < b.product.sku) { // 価格が同じならJANの若い方が優先
      return -1
    } else if (a.product.sku > b.product.sku) {
      return 1
    } else {
      return 0
    }
  }

  /**
   *
   * @param {BundleEntity} bundle
   * @param {integer} number
   * @param {number} amount
   * @param {List<CartItem>} cartItems
   * @private
   */
  static _applyBundleDiscountForXY (bundle, number, amount, cartItems) {
    let sortedItems = cartItems.sort(this._compareCartItemPriorityForXY).reverse()
    let zeroTargetItem = sortedItems.last()
    sortedItems = sortedItems.pop()
    if (zeroTargetItem.quantity !== 1) {
      const { take, remain } = zeroTargetItem.takeByQuantity(1)
      zeroTargetItem = take
      sortedItems = sortedItems.push(remain)
    }

    let bundleDiscounts = sortedItems.push(zeroTargetItem).map(item => ({ item }))
    bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, zeroTargetItem.listPrice)
    bundleDiscounts = bundleDiscounts.sort((a, b) => -this._compareCartItemPriorityForXY(a.item, b.item))
    let zeroTargetBundleDiscount = bundleDiscounts.last()
    bundleDiscounts = bundleDiscounts.pop()

    return bundleDiscounts.map(bundleDiscount => {
      return bundleDiscount.item.applyDiscount(
        CartItemDiscount.createBundleDiscount(
          bundle,
          bundleDiscount.bundleDiscount,
          bundleDiscount.bundleSurplus || Decimal(0),
          bundleDiscount.item.listPrice,
          bundleDiscount.item.listPrice,
          number
        )
      )
    }).push(zeroTargetBundleDiscount.item.applyDiscount(
      CartItemDiscount.createBundleDiscount(
        bundle,
        zeroTargetBundleDiscount.bundleDiscount,
        zeroTargetBundleDiscount.bundleSurplus || Decimal(0),
        Decimal(0),
        Decimal(0),
        number
      )
    ))
  }

  /**
   *
   * @param {BundleEntity} bundle
   * @param {integer} number
   * @param {number} amount
   * @param {List<CartItem>} cartItems
   * @private
   */
  static _applyBundleDiscountForFlatplice (bundle, number, amount, cartItems) {
    let bundleDiscounts = cartItems.map(item => {
      return { item }
    })
    const totalPrice = cartItems.reduce((sum, item) => {
      return item.listPrice.mul(item.quantity).add(sum)
    }, Decimal(0))
    const totalQuantity = cartItems.reduce((sum, item) => {
      return sum + item.quantity
    }, 0)
    const total = amount.mul(totalQuantity)
    bundleDiscounts = CartBundle._distributeDiscounts(bundleDiscounts, totalPrice.sub(total))
    return bundleDiscounts.map(bundleDiscount => {
      return bundleDiscount.item.applyDiscount(
        CartItemDiscount.createBundleDiscount(
          bundle,
          bundleDiscount.bundleDiscount,
          bundleDiscount.bundleSurplus || Decimal(0),
          Decimal(amount),
          Decimal(amount),
          number
        )
      )
    })
  }

  static _applyBundleDiscountForCases (bundle, number, cartItems) {
    return cartItems.map(item => {
      const discountRate = bundle.bundle_patterns[0].discount_rate
      const discount = floorByCurrency(item.listPrice.mul(discountRate), item.currency)
      const amount = item.listPrice.sub(discount)
      return item.applyDiscount(CartItemDiscount.createBundleDiscount(
        bundle,
        discount,
        Decimal(0),
        amount,
        amount,
        number
      ))
    }).toArray()
  }

  /**
   * 基幹向けにバンドル割引額を按分する
   * @param bundleDiscounts
   * @param totalDiscount
   * @return {*}
   * @private
   */
  static _distributeDiscounts (bundleDiscounts, totalDiscount) {
    const totalPrice = bundleDiscounts.reduce((sum, bundleDiscount) => {
      const item = bundleDiscount.item
      return item.listPrice.mul(item.quantity).add(sum)
    }, Decimal(0))
    bundleDiscounts = bundleDiscounts.map((bundleDiscount) => {
      const item = bundleDiscount.item
      return {
        ...bundleDiscount,
        bundleDiscount: floorByCurrency(
          totalDiscount.mul(item.listPrice).div(totalPrice),
          item.currency)
      }
    })

    const totalBundleDiscount = bundleDiscounts.reduce((sum, bundleDiscount) => {
      return bundleDiscount.bundleDiscount.mul(bundleDiscount.item.quantity).add(sum)
    }, Decimal(0))
    const surplus = totalDiscount.sub(totalBundleDiscount)

    if (surplus.gt(0)) {
      const sortedbundleDiscounts = bundleDiscounts.sort((a, b) => {
        if (!a.item.listPrice.equals(b.item.listPrice)) {
          return b.item.listPrice.sub(a.item.listPrice).toNumber() // 価格の高いものが優先
        } else {
          if (a.item.product.sku > b.item.product.sku) {
            return 1
          } else if (a.item.product.sku < b.item.product.sku) {
            return -1
          } else {
            return 0
          }
        }
      })
      const target = sortedbundleDiscounts.get(0)
      return sortedbundleDiscounts
        .set(0, {
          ...target,
          bundleSurplus: surplus })
        .sortBy(bundleDiscount => bundleDiscount.item.instanceUniqueKey)
    } else {
      return bundleDiscounts
    }
  }

  /**
   *
   * @param {List<CartBundle>} bundles
   * @param {List<CartItem>} remainItems
   * @param {Array<BundleEntity>} bundleCandidates 候補となるバンドル一覧 _summaryBundleCandidatesFromCartItemsで優先度順にソートされている前提
   * @return {string|null}
   * @private
   */
  static _getBundleRiichMessage (bundles, remainItems, bundleCandidates) {
    for (let i = 0; i < bundleCandidates.length; i++) {
      const bundle = bundleCandidates[i]
      const lowPriorityBundles = bundles.filter(otherBundle => {
        return this._compareBundlePriority(otherBundle.entity, bundle) > 0
      })
      let candidateItemsForBundle = remainItems
      lowPriorityBundles.forEach(lowPriorityBundle => {
        candidateItemsForBundle = candidateItemsForBundle.concat(lowPriorityBundle.items)
      })
      let remainingCount = 0
      bundle.bundle_patterns.forEach(pattern => {
        const { targets } = CartBundle._getPatternTargetItems(candidateItemsForBundle, pattern)
        if (targets) {
          const targetCount = targets.reduce((sum, target) => target.quantity + sum, 0)
          if (targetCount < pattern.threshold_quantity) {
            remainingCount += pattern.threshold_quantity - targetCount
          }
        } else {
          remainingCount += pattern.threshold_quantity
        }
      })

      if (remainingCount === 1) {
        return bundle.bundle_riichi_message
      }
    }

    return null
  }

  static _taxfreeTypeFromItems (items) {
    if (items.some(item => item.product.originalTaxfreeType === TAXFREE_TYPE.NOT_APPLICABLE)) {
      return TAXFREE_TYPE.NOT_APPLICABLE
    }
    if (items.some(item => item.product.originalTaxfreeType === TAXFREE_TYPE.EXPENDABLE)) {
      return TAXFREE_TYPE.EXPENDABLE
    }
    return TAXFREE_TYPE.GENERAL
  }

  /**
   * 明細一覧が更新された場合に、バンドル配下にあるitemsとの整合性が取れなくなるので、instanceUniqueKeyをベースに更新する。
   * @param bundle
   */
  static refreshBundledItems (bundle, items) {
    const itemsByUniqueKey = {}
    items.forEach(item => {
      itemsByUniqueKey[item.instanceUniqueKey] = item
    })
    return bundle.set('items', bundle.items.map(oldItem => {
      const newItem = itemsByUniqueKey[oldItem.instanceUniqueKey]
      if (!newItem) {
        logger.error('Bundled item is not found in items.')
      }
      return newItem
    }))
  }

  /**
   * 明細一覧にバンドル按分の余りが含まれていたら、余りを適切にサーバに送信出来るように分割する
   * @param items
   */
  static splitSurplusBundleItems (items) {
    let splitedItems = List()
    items.forEach(item => {
      if (item.quantity > 1 && item.bundleSurplus.gt(0)) {
        let { take, remain } = item.takeByQuantity(1)
        const remainDiscount = remain.discounts.get(0)
        remain = remain.clearDiscounts().applyDiscount(remainDiscount.clearBundleSurplus())
        splitedItems = splitedItems.push(remain).push(take)
      } else {
        splitedItems = splitedItems.push(item)
      }
    })
    return splitedItems
  }
}
