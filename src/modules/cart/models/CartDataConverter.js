﻿import Decimal from 'decimal.js'
import logger from '../../../common/utils/logger'
import { DISCOUNT_TYPE, PROMOTION_TYPE, OWNER_DISCOUNT } from '../../promotion/models'
import CartBundle from './CartBundle'
import { PAYMENT_METHOD_TYPE, isCreditCardOrEmoney } from '../../payment/models'
import I18n from 'i18n-js'
import CartPayment from './CartPayment'
import { BALANCE_TYPE } from '../../../modules/balance/models'
import { ORDER_EXTRA_DATA_KEY } from 'modules/order/models'

export default class CartDataConverter {
  /** @type {Cart} */
  _cart = null

  constructor (cart) {
    this._cart = cart
  }

  convert () {
    return this._createOrderDto()
  }

  _formatAmount (amount) {
    if (amount instanceof Decimal) {
      if (this._cart.isReturn) {
        amount = amount.mul(-1)
      }
      return amount.toFixed(4)
    }
    if (amount === undefined || amount === null) {
      return null
    }
    return this._cart.isReturn ? -amount : amount
  }

  /**
   * 内税ならnullを、外税または非課税なら_formatAmountと同じ値を返す
   * @param {Decimal} amount
   * @private
   */
  _formatTaxlessAmount (amount) {
    // 内税か外税かは商品毎ではなく店舗設定を利用する仕様のため、ここではcartの税ルールを見る
    // 内外混在が許容された場合は要修正
    if (this._cart.tax.isTaxIncluded) {
      return null
    } else {
      return this._formatAmount(amount)
    }
  }

  /**
   * 外税ならnullを、内税または非課税なら_formatAmountと同じ値を返す
   * @param {Decimal} amount
   * @private
   */
  _formatTaxedAmount (amount) {
    // 内税か外税かは商品毎ではなく店舗設定を利用する仕様のため、ここではcartの税ルールを見る
    // 内外混在が許容された場合は要修正
    if (this._cart.tax.isTaxExcluded) {
      return null
    } else {
      return this._formatAmount(amount)
    }
  }

  /**
   *
   * @returns {OrderDto}
   * @private
   */
  _createOrderDto () {
    const cart = this._cart.numberSortOrderToItems()
    const payments = this._createPayments(cart)
    const cctPayment = payments.find(payment => isCreditCardOrEmoney(payment.payment_method_type))
    return {
      id: cart.id,
      // shop_id: '', // Header
      shop_name: cart.shop.name,
      // user_id: '', // Orangeの顧客はエノテカでは使わない
      customer_code: cart.customer && cart.customer.customer_code,
      customer_id: cart.customer && cart.customer.id,
      // order_number: '', // サーバで発行される
      is_minus: cart.isReturn ? 1 : 0,
      // origin_order_id: null,
      // status: 0,
      currency: cart.currency,
      total_items_taxless: this._formatAmount(cart.totalItemsTaxless), // 複数税率対応後は未使用
      total_items_taxed: this._formatAmount(cart.totalItemsTaxed), // 複数税率対応後は未使用
      total_taxable_items_taxless: this._formatAmount(cart.taxableTotalTaxless), // 複数税率対応後は未使用
      total_taxable_items_taxed: this._formatAmount(cart.taxableTotalTaxed), // 複数税率対応後は未使用
      // total_items_discount: 0, // EC用
      total_paid_taxless: this._formatAmount(cart.totalTaxless),
      total_paid_taxed: this._formatAmount(cart.totalTaxed),
      tax_rule: cart.tax.taxRule, // 複数税率対応後は未使用
      tax_rate: cart.tax.taxRate.toFixed(4), // 複数税率対応後は未使用
      standard_total_items_taxless: this._formatAmount(cart.standardTotalItemsTaxless),
      standard_total_items_taxed: this._formatAmount(cart.standardTotalItemsTaxed),
      standard_total_taxable_items_taxless: this._formatAmount(cart.standardTaxableTotalTaxless),
      standard_total_taxable_items_taxed: this._formatAmount(cart.standardTaxableTotalTaxed),
      standard_tax_rule: cart.tax.taxRule,
      standard_tax_rate: cart.tax.standardTaxRate,
      reduced_total_items_taxless: this._formatAmount(cart.reducedTotalItemsTaxless),
      reduced_total_items_taxed: this._formatAmount(cart.reducedTotalItemsTaxed),
      reduced_total_taxable_items_taxless: this._formatAmount(cart.reducedTaxableTotalTaxless),
      reduced_total_taxable_items_taxed: this._formatAmount(cart.reducedTaxableTotalTaxed),
      reduced_tax_rule: cart.tax.taxRule,
      reduced_tax_rate: cart.tax.reducedTaxRate,
      used_points: this._formatAmount(cart.usePoint),
      obtained_points: this._formatAmount(cart.obtainedPoint),
      extra_data: this._createOrderExtraData(cart),
      // contracted_at: null,
      // finalized_at: null,
      // deleted_at: null,
      // completed_at: null,
      // created_at: null,
      // updated_at: null,
      shop_mode: 2,
      cashier_code: cart.cashier.cashier_code,
      staff_code: cart.staff.staff_code,
      shop_code: cart.shop.shop_code,
      promotion_orders: this._createPromotions(cart),
      order_items: CartBundle.splitSurplusBundleItems(cart.proratedItems).toArray()
        .map((item, index) => this._createOrderItemDto(item)),
      pos_order: this._createPosOrderDto(cart),
      payments,
      order_payments: payments,
      pos_order_property_values: cart.orderProperties.map(
        (properties, index) => this._createOrderPropertiesDto(cart, properties, index)),
      cashier_balances: this._createCashierBalanceDtos(cart),
      is_taxfree: cart.isTaxfree,
      is_bundle_disabled: cart.isBundleDisabled
    }
  }

  /**
   *
   * @param {CartItem} item
   * @returns {Object}
   * @private
   */
  _createOrderItemDto (item, index) {
    const cart = this._cart
    const childProducts = this._createChildProductsDto(item.product.childProducts)
    return {
      // id
      // order_id
      product_id: item.product.productId,
      product_variant_id: item.product.productVariantId,
      // bundle_no promotionOrderItemsに記録する
      product_name: item.product.name,
      product_short_name: item.product.shortName,
      product_variant_name: item.product.productVariantName,
      product_code: item.product.productCode,
      sku: item.product.sku,
      own_company_code: item.product.ownCompanyCode,
      maker_code: item.product.makerCode,
      article_number: item.product.articleNumber,
      warehouse_code: item.product.warehouseCode,
      immutable_price_taxless: this._formatTaxlessAmount(item.immutableOriginalPrice),
      immutable_price_taxed: this._formatTaxedAmount(item.immutableOriginalPrice),
      price_taxless: this._formatTaxlessAmount(item.listPrice),
      price_taxed: this._formatTaxedAmount(item.listPrice),
      sales_price_taxless: this._getSalesPriceTaxless(item),
      sales_price_taxed: this._getSalesPriceTaxled(item),
      tax_rule: item.product.tax.taxRule,
      tax_rate: item.product.tax.taxRate.toFixed(4),
      tax_type: item.product.taxType,
      taxfree_type: item.product.taxfreeType,
      tax_code: item.product.taxCode,
      price: item.product.price,
      cost_price: item.product.costPrice,
      quantity: this._formatAmount(item.quantity),
      sort_order: item.sortOrder,
      // extra_data
      child_products_data: childProducts && JSON.stringify(childProducts),
      can_grant_points: item.product.canGrantPoint ? 1 : 0,
      is_sales_excluded: item.product.isSalesExcluded ? 1 : 0,
      is_discountable: item.product.isDiscountable ? 1 : 0,
      product_group_id: item.product.productGroupId,
      // created_at // サーバで入る
      // updated_at // サーバで入る
      promotion_order_items: this._createPromotionOrderItemDtos(cart, item),
      sales: item.product.sales,
      stock_mode: item.product.stockMode
    }
  }

  /**
   *
   * @param {Immutable.List<SetProductEntity>} childProducts
   * @private
   */
  _createChildProductsDto (childProducts) {
    if (!childProducts || childProducts.count() === 0) {
      return null
    }
    return childProducts.map(child => {
      return {
        // id
        // order_id
        product_id: child.id,
        product_variant_id: child.variant.id,
        // bundle_no promotionOrderItemsに記録する
        product_name: child.name,
        // product_variant_name: '', // エノでは不要？
        product_code: child.product_code,
        sku: child.variant.sku,
        // own_company_code: child.ownCompanyCode, // エノでは不要
        // maker_code: child.makerCode, // エノでは不要
        article_number: child.variant.article_number,
        // warehouse_code: child.warehouseCode, // エノでは不要
        // 金額系はセット商品には不要
        // cost_price_taxless: item.costPriceTaxless.toFixed(4),
        // cost_price_tax: item.costPriceTax.toFixed(4),
        // list_price_taxless: item.listPriceTaxless.toFixed(4),
        // list_price_tax: item.listPriceTax.toFixed(4),
        // sales_price_taxless: item.salesPriceTaxless.toFixed(4),
        // sales_price_tax: item.salesPriceTax.toFixed(4),
        price: child.variant.price, // マスタ売価だけは必要
        cost_price: child.variant.cost_price,
        tax_rule: child.tax_rule,
        tax_rate: child.tax_rate,
        tax_type: child.tax_type,
        taxfree_type: child.taxfree_type,
        tax_code: child.tax_code,
        quantity: child.quantity,
        // sort_order: index,
        // extra_data
        // child_products_data: ''
        can_grant_points: child.variant.can_grant_points ? 1 : 0,
        is_sales_excluded: child.is_sales_excluded ? 1 : 0,
        is_discountable: child.is_discountable ? 1 : 0,
        product_group_id: child.product_group_id,
        stock_mode: child.variant.stock_mode
      }
    }).toArray()
  }

  /**
   *
   * @param cart
   * @returns {{pos_order_number: *|string|null, pos_order_number_sequence: *|number|Integer, total_general_items_taxless: *, total_general_items_tax: *, is_general_items_taxfree: number, total_expendable_items_taxless: *, total_expendable_items_tax: *, is_expendable_items_taxfree: number, total_exclude_items_taxless: *, total_exclude_items_tax: *, total_point_cut_items_taxless: *, total_point_cut_items_taxed: *, is_tax_free: number, cashier_id: *, staff_id, staff_name, staff_code, client_created_at: number}}
   * @private
   */
  _createPosOrderDto (cart) {
    return {
      // id: '', // サーバで発番される
      // order_id: '', // サーバで代入される
      pos_order_number: cart.posOrderNumber,
      pos_order_number_sequence: cart.orderNumberSequence,
      // origin_pos_order_number: '',
      total_general_items_taxless: this._formatAmount(cart.taxfreeGeneralsTotalTaxless),
      total_general_items_tax: this._formatAmount(cart.taxfreeGeneralsTotalTax),
      is_general_items_taxfree: cart.isTaxfreeAppliedForGeneral ? 1 : 0,
      total_expendable_items_taxless: this._formatAmount(cart.taxfreeExpendablesTotalTaxless),
      total_expendable_items_tax: this._formatAmount(cart.taxfreeExpendablesTotalTax),
      is_expendable_items_taxfree: cart.isTaxfreeAppliedForExpendable ? 1 : 0,
      total_exclude_items_taxless: this._formatAmount(cart.taxfreeNotApplicablesTotalTaxless),
      total_exclude_items_tax: this._formatAmount(cart.taxfreeNotApplicablesTotalTax),
      is_tax_free: cart.isTaxfreeApplied ? 1 : 0,
      // device_id: '', // リクエストヘッダから登録する
      cashier_id: cart.cashier.id,
      staff_id: cart.staff.id,
      staff_name: cart.staff.name,
      staff_code: cart.staff.staff_code,
      // canceled_device_id: '', // レジマイナスの時にサーバで入る
      // canceled_cashier_id: '', // レジマイナスの時にサーバで入る
      // canceled_staff_id: '', // レジマイナスの時にサーバで入る
      // canceled_staff_name: '', // レジマイナスの時にサーバで入る
      client_created_at: Math.floor(cart.createdAt.getTime() / 1000)
    }
  }

  _isItemsTaxfree (items, taxfreeType) {
    items = items.filter(item => item.product.taxfreeType === taxfreeType)
    if (items.count() === 0) {
      return false
    }
    return items.get(0).isTaxfreeApplied
  }

  /**
   *
   * @param cart
   * @param {CartPayment} payment
   * @return {{payment_method_id: *}}
   * @private
   */
  _createPayments (cart) {
    let countGiftWithChange = 0
    let countGiftWithoutChange = 0
    const payments = cart.activePayments.toArray().map(payment => {
      if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE) {
        countGiftWithChange++
      } else if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE) {
        countGiftWithoutChange++
      }
      const change = CartPayment.calculateChange(cart, payment, countGiftWithChange, countGiftWithoutChange)
      return {
        // id
        payment_method_id: payment.paymentMethodId,
        payment_method_type: payment.paymentMethodType,
        payment_method_name: payment.name,
        media_type: payment.mediaType,
        acquirer_code: payment.acquirerCode,
        // order_id
        // shop_id // Header
        // user_id // Header
        // status
        is_minus: 0,
        // transaction_code: 0,
        // currency: // Header
        amount: this._formatAmount(payment.amount),
        charge_taxless: '0.0000', // 同上
        charge_tax: '0.0000', // POSにおいて決済手数料はナシ
        extra_data: payment.extraData && JSON.stringify(payment.extraData),
        // created_at
        // updated_at
        // deleted_at
        change: this._formatAmount(change)
      }
    })

    if (cart.giftChange.gt(0)) {
      payments.push(this._createGiftChange(PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT, cart.giftChange))
    }
    if (cart.giftSurplus.gt(0)) {
      payments.push(this._createGiftChange(PAYMENT_METHOD_TYPE.GIFT_SURPLUS, cart.giftSurplus))
    }

    return payments
  }

  _createGiftChange (paymentMethodType, amount) {
    const paymentMethodName = paymentMethodType === PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT
      ? I18n.t('cart.gift_change') : I18n.t('cart.gift_surplus')
    return {
      // id
      payment_method_id: '00000000000000000000000000000' + paymentMethodType.toString(),
      payment_method_type: paymentMethodType,
      payment_method_name: paymentMethodName,
      media_type: 0,
      acquirer_code: '',
      // order_id
      // shop_id // Header
      // user_id // Header
      // status
      is_minus: 0,
      // transaction_code: 0,
      // currency: // Header
      amount: this._formatAmount(amount),
      charge_taxless: '0.0000', // 同上
      charge_tax: '0.0000', // POSにおいて決済手数料はナシ
      extra_data: '',
      // created_at
      // updated_at
      // deleted_at
      change: this._formatAmount(Decimal(0))
    }
  }

  /**
   *
   * @param {Cart} cart
   * @param {CartItem} cartItem
   * @returns {Array}
   * @private
   */
  _createPromotionOrderItemDtos (cart, cartItem) {
    const sale = cartItem.discounts.find(discount => discount.promotionType === PROMOTION_TYPE.SALE)
    const manual = cartItem.discounts.find(discount => !discount.promotionType)
    if (sale && manual &&
      cartItem.discounts.get(0).discountType === DISCOUNT_TYPE.RATE &&
      cartItem.discounts.get(1).discountType === DISCOUNT_TYPE.RATE) {
      const saleDto = this._createPromotionOrderItemDto(cart, cartItem, sale)
      const manualDto = this._createPromotionOrderItemDto(cart, cartItem, manual)

      const subtotal = cartItem.discounts.find(discount => discount.promotionType === PROMOTION_TYPE.SUBTOTAL)
      const subtotalDto = this._createPromotionOrderItemDto(cart, cartItem, subtotal)

      // 割引を同時に適用する場合、セール割引に割引金額を寄せるので、手動での割引額は0に
      manualDto.discount_taxless = this._formatTaxlessAmount(new Decimal(0))
      manualDto.discount_taxed = this._formatTaxedAmount(new Decimal(0))
      return [saleDto, manualDto, subtotalDto]
    } else {
      return cartItem.discounts.map(discount => {
        return this._createPromotionOrderItemDto(cart, cartItem, discount)
      }).toArray()
    }
  }

  /**
   *
   * @param {Cart} cart
   * @param {CartItem} cartItem
   * @param {CartItemDiscount} discount
   * @returns {Object}
   * @private
   */
  _createPromotionOrderItemDto (cart, cartItem, discount) {
    switch (discount.promotionType) {
      case PROMOTION_TYPE.BUNDLE:
        return {
          // id: '', // サーバで発番される
          // order_id: '', // サーバで代入される
          product_id: cartItem.product.productId,
          product_variant_id: cartItem.product.productVariantId,
          promotion_id: discount.promotionId,
          promotion_name: discount.promotionName,
          promotion_type: discount.promotionType,
          promotion_code: discount.promotionCode,
          bundle_group: discount.bundleNumber.toString(),
          bundle_type: discount.bundleType,
          discount_type: discount.discountType,
          discount_percent: null,
          discount_taxless: this._formatTaxlessAmount(cartItem.discountAmount.add(cartItem.bundleSurplus)),
          discount_taxed: this._formatTaxedAmount(cartItem.discountAmount.add(cartItem.bundleSurplus)),
          discount_reason_id: discount.discountReasonId,
          discount_reason_name: discount.discountReasonName,
          bundle_display_price: this._formatAmount(cartItem.bundlePriceForDisplay)
          // discount_reason_note // 自由入力は廃止された
          // created_at
          // updated_at
        }

      case PROMOTION_TYPE.SALE:
      default:
        let discountPerceint = null
        let discountTaxless = null
        let discountTaxed = null
        switch (discount.discountType) {
          case DISCOUNT_TYPE.RATE:
            discountPerceint = discount.discountPercent.toFixed(4)
            discountTaxless = this._formatTaxlessAmount(cartItem.rateDiscountAmount)
            discountTaxed = this._formatTaxedAmount(cartItem.rateDiscountAmount)
            break
          case DISCOUNT_TYPE.AMOUNT:
            discountTaxless = this._formatTaxlessAmount(discount.value)
            discountTaxed = this._formatTaxedAmount(discount.value)
            break
          default:
            logger.error('[CartDataConverter._createPromotionOrderItemDto]' +
              ' Invalid discount type ' + discount.discountType)
        }

        return {
          // id
          // order_id
          product_id: cartItem.product.productId,
          product_variant_id: cartItem.product.productVariantId,
          promotion_id: discount.promotionId,
          promotion_name: discount.promotionName,
          promotion_type: discount.promotionType,
          promotion_code: discount.promotionCode,
          // bundle_group // バンドルのみ
          // bundle_type // バンドルのみ
          discount_type: discount.discountType,
          discount_percent: discountPerceint,
          discount_taxless: discountTaxless,
          discount_taxed: discountTaxed,
          discount_reason_id: discount.discountReasonId,
          discount_reason_name: discount.discountReasonName,
          is_prorated: cartItem.isProrated && !discount.isManual
          // discount_reason_note // 自由入力は廃止された
          // created_at
          // updated_at
        }
    }
  }

  /**
   * 小計値引データ(小計割引・値引 & オーナー割引)
   */
  _createPromotions (cart) {
    if (cart.discount) {
      if (cart.ownerDiscount) return [this._createPromotionOrderDto(cart, cart.discount), this._createPromotionOrderDto(cart, cart.ownerDiscount)]
      return [this._createPromotionOrderDto(cart, cart.discount)]
    } else if (cart.ownerDiscount) {
      return [this._createPromotionOrderDto(cart, cart.ownerDiscount)]
    } else {
      return []
    }
  }
  /**
   *
   * @param {Cart} cart
   * @param {CartDiscount} discount
   * @returns {{tax_rate: (*|Decimal)}}
   * @private
   */
  _createPromotionOrderDto (cart, discount) {
    return {
      // id: '',
      // order_id: '',
      tax_rate: cart.tax.taxRate.toFixed(4),
      promotion_id: null,
      prpmotion_name: '',
      is_minus: 0,
      discount_type: discount.discountType,
      discount_percent: discount.discountPercent && this._formatAmount(discount.discountPercent),
      subtotal_discounts_taxless: this._formatTaxlessAmount(discount.discountReasonName === OWNER_DISCOUNT.NAME
        ? cart.ownerDiscountAmount : cart.subtotalDiscountAmount),
      subtotal_discounts_taxed: this._formatTaxedAmount(discount.discountReasonName === OWNER_DISCOUNT.NAME
        ? cart.ownerDiscountAmount : cart.subtotalDiscountAmount),
      discount_reason_id: discount.discountReasonId,
      discount_reason_name: discount.discountReasonName,
      // 割引順序でそれぞれの割引額やレシート表示順が変わるため、割引順序を保存
      extra_data: JSON.stringify({
        discount_order: cart.discountOrder
      })
      // created_at
      // created_by
      // updated_at
      // updated_by
      // approved_by
      // approval_status
    }
  }

  _createOrderPropertiesDto (cart, properties, index) {
    return {
      pos_order_property_id: properties.pos_order_propery_id,
      pos_order_property_option_value: properties.name,
      pos_order_property_option_code: properties.code
    }
  }

  /**
   *
   * @param {Cart} cart
   * @private
   */
  _createCashierBalanceDtos (cart) {
    const balanceDtoBase = {
      cashier_id: cart.cashier.id,
      pos_order_number: cart.posOrderNumber,
      staff_id: cart.staff.id,
      staff_name: cart.staff.name
    }
    const dtos = cart.activePayments
      .filter(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH)
      .map(payment => {
        // 釣りを引き、お客さんがお店に支払った額のみにする。入力制御側で現金は１つのみ、釣りは現金額を超えないので、この計算式でOK
        const amount = payment.amount.sub(cart.change)

        return {
          ...balanceDtoBase,
          balance_type: BALANCE_TYPE.DEPOSIT,
          payment_method_id: payment.paymentMethodId,
          payment_method_name: payment.name,
          payment_method_type: payment.paymentMethodType,
          amount: this._formatAmount(amount),
          is_change: 0
        }
      })
      .toArray()
    if (cart.giftChange.gt(0)) {
      return [{
        ...balanceDtoBase,
        balance_type: BALANCE_TYPE.WITHDRAWAL,
        payment_method_type: PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE,
        amount: this._formatAmount(cart.giftChange),
        is_change: 1
      }]
    }
    return dtos
  }

  _createOrderExtraData (cart) {
    const cctPayment = this._createPayments(cart).find(payment => isCreditCardOrEmoney(payment.payment_method_type))
    /** VEGAでの決済情報 */
    const vegaExtraData = cctPayment ? JSON.parse(cctPayment.extra_data) : null
    /** 小計値割引按分前のカート */
    const cartItemExtraData = CartBundle
      .splitSurplusBundleItems(cart.items)
      .toArray()
      .map(item => this._createOrderItemDto(item))

    const resultObject = {}
    resultObject[ORDER_EXTRA_DATA_KEY.CART_ITEMS] = cartItemExtraData
    if (vegaExtraData) {
      // order_paymentsテーブルにextra_data列が存在しないため、ordersテーブルに持たせる
      resultObject[ORDER_EXTRA_DATA_KEY.PAYMENT] = vegaExtraData
    }
    return JSON.stringify(resultObject)
  }

  _getSalesPriceTaxless (cartItem) {
    if (cartItem.bundlePriceWithProrate) {
      return this._formatTaxlessAmount(cartItem.bundlePriceWithProrate)
    }

    if (cartItem.bundlePrice) {
      return this._formatTaxlessAmount(cartItem.bundlePrice)
    }

    return this._formatTaxlessAmount(cartItem.salesPriceWithProrate)
  }

  _getSalesPriceTaxled (cartItem) {
    if (cartItem.bundlePriceWithProrate) {
      return this._formatTaxedAmount(cartItem.bundlePriceWithProrate)
    }

    if (cartItem.bundlePrice) {
      return this._formatTaxedAmount(cartItem.bundlePrice)
    }

    return this._formatTaxedAmount(cartItem.salesPriceWithProrate)
  }
}
