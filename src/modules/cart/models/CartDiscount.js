import { Record } from 'immutable'
import Decimal from 'decimal.js'
import { DISCOUNT_TYPE, DISCOUNT_ORDER, PROMOTION_TYPE } from '../../promotion/models'
import { ceilByCurrency } from '../../../common/models/Currency'

const CartDiscountRecord = Record({
  presetCode: '',
  discountType: DISCOUNT_TYPE.AMOUNT,
  value: Decimal(0),
  discountReasonId: null,
  discountReasonName: null,
  promotionType: null
})

/**
 * 小計値割引
 * @property {integer} discountType
 * @property {Decimal} value
 * @property {string} discountReasonId
 */
export default class CartDiscount extends CartDiscountRecord {
  /**
   * 割引率 (値引きの場合はnull)
   * @returns {Decimal|null}
   */
  get discountRate () {
    if (this.discountType === DISCOUNT_TYPE.RATE) {
      return this.value
    } else {
      return null
    }
  }

  /**
   * 割引率 % (値引きの場合はnull)
   * @returns {Decimal|null}
   */
  get discountPercent () {
    if (this.discountType === DISCOUNT_TYPE.RATE) {
      return this.value.mul(100)
    } else {
      return null
    }
  }

  // 小計値引
  static discountAmount (discountOrder, subTotalDiscount, ownerDiscount, totalItems, currency) {
    return this.getDiscountAmount(false, discountOrder, subTotalDiscount, ownerDiscount, totalItems, currency)
  }

  // オーナーズカード値引
  static ownerDiscountAmount (discountOrder, subTotalDiscount, ownerDiscount, totalItems, currency) {
    return this.getDiscountAmount(true, discountOrder, subTotalDiscount, ownerDiscount, totalItems, currency)
  }

  static getDiscountAmount (isOwnerDiscount, discountOrder, subTotalDiscount, ownerDiscount, totalItems, currency) {
    let discountAmount = new Decimal(0)
    let ownerDiscountAmount = new Decimal(0)

    switch (discountOrder) {
      // 先に小計値引計算
      case DISCOUNT_ORDER.DISCOUNT_FIRST:
        if (subTotalDiscount) {
          discountAmount = subTotalDiscount.discountType === DISCOUNT_TYPE.RATE ? ceilByCurrency(totalItems.mul(subTotalDiscount.discountRate), currency) : subTotalDiscount.value
        }
        if (ownerDiscount) {
          ownerDiscountAmount = ceilByCurrency((totalItems.sub(discountAmount)).mul(ownerDiscount.discountRate), currency)
        }
        break
      // 先にオーナー値引計算
      case DISCOUNT_ORDER.OWNER_DISCOUNT_FISRT:
        if (ownerDiscount) {
          ownerDiscountAmount = ceilByCurrency(totalItems.mul(ownerDiscount.discountRate), currency)
        }
        if (subTotalDiscount) {
          discountAmount = subTotalDiscount.discountType === DISCOUNT_TYPE.RATE ? ceilByCurrency((totalItems.sub(ownerDiscountAmount)).mul(subTotalDiscount.discountRate), currency) : subTotalDiscount.value
        }
        break
      default:
        break
    }
    return isOwnerDiscount ? ownerDiscountAmount : discountAmount
  }

  /**
   * 値割引一覧から割引率合計を算出する
   * @param {CartDiscount} discount
   * @return Decimal
   */
  static discountRateTotal (discount) {
    return discount && discount.discountType === DISCOUNT_TYPE.RATE ? discount.value : new Decimal(0)
  }

  static createWithPromotionPreset (promotionPreset, discountReasonName) {
    return new CartDiscount({
      presetCode: promotionPreset.promotion_preset_code,
      discountType: promotionPreset.discount_type,
      value: Decimal(promotionPreset.discount_value),
      discountReasonId: promotionPreset.discount_reason_id,
      discountReasonName: discountReasonName,
      promotionType: promotionPreset.promotion_type
    })
  }

  static createWithDiscountReason (amount, discountType, discountReasonId, discountReasonName, promotionType) {
    return new CartDiscount({
      discountReasonId,
      discountReasonName,
      discountType,
      value: Decimal(amount),
      promotionType: promotionType
    })
  }
}
