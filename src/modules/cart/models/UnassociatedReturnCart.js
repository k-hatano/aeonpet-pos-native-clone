/**
 * @class UnassociatedReturnCart
 */
import Cart from './Cart'
import Decimal from 'decimal.js'
import CartPayment from './CartPayment'

export default class UnassociatedReturnCart extends Cart {
  get canApplyTaxfree () {
    return false
  }

  get canSetCustomer (): boolean {
    return true
  }

  get isReturn (): boolean {
    return true
  }

  get canUsePoint () {
    return this.customerIsNotTemporary
  }

  get maxUsePoint () {
    return Number(this.totalItemsBase)
  }

  maxAmountForPayment (payment: CartPayment): Decimal {
    if (!this.canUsePayment(payment)) {
      return Decimal(0)
    } else {
      return this.justAmountForPayment(payment)
    }
  }
}
