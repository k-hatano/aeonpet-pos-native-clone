/**
 * @class Cart
 * @property {string} id
 * @property {List.<CartItem>} items
 * @property {List} payments
 * @property {string} currency
 * @property {boolean} isTaxfree
 * @property {boolean} isBundleDisabled
 * @property {int} addingPoint
 * @property {int} usePoint
 * @property {List} appliedBundles
 * @property {CartDiscount | null} ownerDiscount
 * @property {CartDiscount | null} discount
 * @property {CustomerDetailDto} customer
 * @property {Tax} tax
 * @property {integer} orderStatus
 * @property {Date|null} createdAt
 * @property {Object} staff
 * @property {Object} cashier
 * @property {Object} shop
 * @property {Integer} orderNumberSequence
 * @property {string|null} bundleRiichiMessage
 * @property {List} orderProperties
 * @property {integer|null} obtainedPoint
 * @property {boolean} isReturn
 * @property {boolean} isTraining
 * @property {boolean} isPushed
 */
import { Record, List } from 'immutable'
import logger from 'common/utils/logger'
import CartItem from './CartItem'
import CartItemProduct from './CartItemProduct'
import CartItemDiscount from './CartItemDiscount'
import Decimal from 'decimal.js'
import CartBundle from './CartBundle'
import CartDiscount from './CartDiscount'
import CartItemTaxfree from './CartItemTaxfree'
import Tax from 'common/models/Tax'
import { PAYMENT_METHOD_TYPE } from '../../payment/models'
import { TAX_RULE, TAX_CODE } from '../../../common/models/Tax'
import { MAX_PAYMENT_AMOUNT, GRANT_POINT } from '../models'
import { POINT_TYPE, DISCOUNT_TYPE, OWNER_DISCOUNT, DISCOUNT_ORDER } from '../../promotion/models'
import CashChanger from 'modules/printer/models/CashChanger'

const CartRecord = Record({
  id: null,
  items: List(),
  proratedItems: List(),
  payments: List(),
  currency: 'jpy',
  isTaxfree: false,
  isBundleDisabled: false,
  addingPoint: 0,
  usePoint: 0,
  appliedBundles: List(),
  ownerDiscount: null,
  discount: null,
  discountOrder: 0,
  customer: null,
  tax: new Tax(),
  orderStatus: 0,
  createdAt: null,
  staff: null,
  cashier: null,
  shop: null,
  orderNumberSequence: 0,
  bundleRiichiMessage: null,
  orderProperties: List(),
  obtainedPoint: null,
  isReturn: false,
  isPended: false,
  posOrderNumber: '',
  originPosOrderNumber: '',
  isTraining: false,
  isPushed: false,
  extendsData: null,
  scannedPromotionBarcodes: List()
})

export default class Cart extends CartRecord {
  async addProductAsync (product, productVariant, quantity, amount = undefined) {
    if (amount !== undefined) {
      productVariant.dataValues.price = amount
    }

    // 非課税商品以外は税を店舗設定に合わせる
    if (product.tax_rule !== TAX_RULE.NONE) {
      product.tax_rule = this.tax.taxRule
      product.tax_rate = this.tax.taxRate
    }

    let item = await this.createItemAsync(product, productVariant, quantity)
    return this._setItems(this.items.push(item))
  }

  addItem (cartItem) {
    return this._setItems(this.items.push(cartItem))
  }

  _setItems (items) {
    items = CartItem.compactItems(items)

    if (this.isBundleDisabled) {
      items = items.map(item => {
        return item.resetForUnbundle().applySaleDiscount(this.scannedPromotionBarcodes)
      })

      items = Cart._sortItems(List(), items)

      if (this.isTaxfree) {
        items = CartItemTaxfree.applyToItems(items, this.tax)
      } else {
        items = items.map(items => items.setIsTaxfreeApplied(false))
      }

      items = items.sortBy(item => item.instanceUniqueKey)

      return this.set('items', items)
        .set('appliedBundles', List())
        .set('discount', null)
        .set('ownerDiscount', null)
        .set('discountOrder', 0)
        .set('usePoint', 0)
        .set('bundleRiichiMessage', '')
        .clearPayments()
    } else {
      let { bundles, unbundledItems, riichiMessage } = CartBundle.applyToCartItems(items, this.scannedPromotionBarcodes)

      unbundledItems = unbundledItems.map(item => item.applySaleDiscount(this.scannedPromotionBarcodes))

      // TODO リファクタリング ソート処理を何度か行っているので、最低限の回数で行えるようにする。
      items = Cart._sortItems(bundles, unbundledItems)

      if (this.isTaxfree) {
        items = CartItemTaxfree.applyToItems(items, this.tax)
        items = items.sortBy(item => item.instanceUniqueKey)
      } else {
        items = items.map(items => items.setIsTaxfreeApplied(false))
      }

      bundles = bundles.map(bundle => {
        return CartBundle.refreshBundledItems(bundle, items)
      })

      return this.set('items', items)
        .set('appliedBundles', bundles)
        .set('discount', null)
        .set('ownerDiscount', null)
        .set('discountOrder', 0)
        .set('usePoint', 0)
        .set('bundleRiichiMessage', riichiMessage)
        .clearPayments()
    }
  }

  static _sortItems (bundles, unbundledItems) {
    let items = List()

    bundles.forEach(bundle =>
      bundle.items.forEach(item => {
        items = items.push(item)
      })
    )
    unbundledItems.forEach(item => {
      items = items.push(item)
    })

    items.sortBy(item => item.instanceUniqueKey)

    return CartItem.compactItems(items).sortBy(item => item.instanceUniqueKey)
  }

  /**
   * cart_itemsに明細番号を採番する。
   * @return {Cart}
   */
  numberSortOrderToItems () {
    let sortOrder = 1
    const items = []


    this.appliedBundles.forEach(bundle => {
      return bundle.items.forEach(item => {
        items.push(item.setSortOrder(sortOrder))
        sortOrder++
      })
    })
  
    this.items.filter(item => !item.isBundled).forEach(item => {
      items.push(item.setSortOrder(sortOrder))
      sortOrder++
    })

    const bundles = this.appliedBundles.map(bundle => {
      return CartBundle.refreshBundledItems(bundle, items)
    })

    return this.set('items', List(items)).set('appliedBundles', bundles)
  }

  /**
   * バンドル適用商品における明細番号採番
   * 会計保留時は、小計値引按分が行われないため、処理を分岐
   */
  numberSortOrderWithBundles (items, sortOrder, item) {
    // 会計保留時
    if (this.isPended) {
      items.push(item.setSortOrder(sortOrder))
    // 会計時
    } else {
      const cartItems = this.items
        .filter(innerItem => innerItem.isBundled)
        .filter(innerItem => innerItem.product.productId === item.product.productId)
      if (cartItems.size > 0) {
        cartItems.forEach(oldItem => {
          items.push(oldItem.setSortOrder(sortOrder))
        })
      } else {
        items.push(item.setSortOrder(sortOrder))
      }
    }
  }

  /**
   * @private
   * @param {Object} product
   * @param {Object} productVariant
   * @param {int} quantity
   * @return {Promise.<CartItem>}
   */
  async createItemAsync (product, productVariant, quantity) {
    const cartItemProduct = await CartItemProduct.createUsingRepositories(
      product,
      productVariant,
      this.tax,
      this.isReturn
    )

    return new CartItem({
      product: cartItemProduct,
      quantity,
      currency: this.currency,
      instanceUniqueKey: CartItem.generateInstanceUniqueKey()
    })
  }

  /**
   * Swap cart item
   * @param {CartItem} oldItem
   * @param {CartItem} newItem
   * @return {Promise.<void>}
   */
  async updateItemAsync (oldItem, newItem) {
    const oldIndex = this.items.indexOf(oldItem)
    if (oldIndex >= 0) {
      if (newItem.quantity <= 0) {
        return this.deleteItemAt(oldIndex)
      } else {
        return this._setItems(this.items.set(oldIndex, newItem))
      }
    } else {
      logger.warning('Cart.updateItemAsync cannot find oldItem')
    }
    return this
  }

  addPayment (payment) {
    return this.set('payments', this.payments.push(payment))
  }

  updatePayment (oldPaymentMethod, newPaymentMethod) {
    const oldIndex = this.payments.indexOf(oldPaymentMethod)
    if (oldIndex >= 0) {
      if (newPaymentMethod.paymentMethodType === PAYMENT_METHOD_TYPE.INTERNAL_POINT) {
        return this.updatePaymentWithPoint(newPaymentMethod, oldIndex)
      }
      return this.set('payments', this.payments.set(oldIndex, newPaymentMethod))
    }
    return this
  }

  /**
   * 支払いがポイント利用時の処理
   */
  updatePaymentWithPoint (newPaymentMethod, oldIndex) {
    if (this.maxUsePoint < newPaymentMethod.amount) {
      newPaymentMethod = newPaymentMethod.updateAmount(this.maxUsePoint)
    }
    const basePoint = this.shop.useable_base_points
    const basePointCoefficient = Math.floor(newPaymentMethod.amount / basePoint)
    newPaymentMethod = newPaymentMethod.updateAmount(basePoint * basePointCoefficient)
    return this.set('payments', this.payments.set(oldIndex, newPaymentMethod)).set('usePoint', newPaymentMethod.amount)
  }

  initializePayments () {
    return this.set('payments', List())
  }

  /**
   * 他の支払い方法の入力状況も考慮し、該当の支払い方法が利用可能かどうかを調べる
   * @param payment
   * @private
   */
  canUsePayment (payment) {
    // 既に金額が入力されているということは、利用可能であることは間違いない
    if (payment.amount.gt(0)) {
      return true
    }

    // 会員選択されていない場合、ポイント利用不可
    if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.INTERNAL_POINT && !this.canUsePoint) {
      return false
    }

    const total = this.totalTaxed

    // １円も支払う必要がないなら、支払いはできない。
    if (total.lte(0)) {
      return false
    }

    // 既に合計金額を超えているなら、支払不可
    if (total.sub(this.deposit).lte(0)) {
      return false
    }

    const activePayments = this.activePayments

    // まだ他の支払い方法が一つも利用されていないなら、全て利用可能
    if (activePayments.count() === 0) {
      return true
    }

    // 売掛はほかの支払い方法が有効であれば、利用できない。
    if (
      payment.paymentMethodType === PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE &&
      activePayments.find(payment => payment.paymentMethodType !== PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE)
    ) {
      return false
    }

    const activeAccountsReceivable = activePayments.find(
      payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE
    )
    // 売掛は他の支払いタイプと併用不可
    if (activeAccountsReceivable) {
      return payment.paymentMethodType === PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE
    }

    // 電子マネーの併用は特定の支払いのみ可
    // その他の種別の支払いを行なった場合、電子マネー支払いを利用不可にする
    if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY &&
      activePayments.find(payment => (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.EMONEY) &&
        (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.CASH) &&
        (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE) &&
        (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.EXTERNAL_POINT) &&
        (payment.paymentMethodType !== PAYMENT_METHOD_TYPE.INTERNAL_POINT))) {
      return false
    }
    // 電子マネー支払いが行われている場合、特定の支払い以外を利用不可にする
    const activeEmoney = activePayments.find(
      payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY
    )
    if (activeEmoney) {
      return (payment.paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY) ||
        (payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH) ||
        (payment.paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE) ||
        (payment.paymentMethodType === PAYMENT_METHOD_TYPE.EXTERNAL_POINT) ||
        (payment.paymentMethodType === PAYMENT_METHOD_TYPE.INTERNAL_POINT)
    }

    // クレジットと電子マネーはお互いの支払い方法と、同タイプでの併用が不可
    const isCreditOrEMoney = _payment => {
      return (
        _payment.paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT ||
        _payment.paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY ||
        _payment.paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY
      )
    }
    if (isCreditOrEMoney(payment)) {
      const cannotUseWith = activePayments.find(isCreditOrEMoney)
      if (cannotUseWith) {
        return false
      }
    }

    if (
      activePayments.find(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.EXTERNAL_POINT) &&
      payment.paymentMethodType === PAYMENT_METHOD_TYPE.EXTERNAL_POINT
    ) {
      return false
    }

    return true
  }

  /**
   * 指定の支払い方法のジャスト金額を返す
   * @param {CartPayment} payment
   * @return {Decimal}
   */
  justAmountForPayment (payment) {
    const just = payment.amount.add(this.totalTaxed.sub(this.deposit))
    if (just.lt(0)) {
      return Decimal(0)
    } else {
      return just
    }
  }

  /**
   * 指定の支払い方法の最大入力金額を返す
   * @param {CartPayment} payment
   * @return {Decimal}
   */
  maxAmountForPayment (payment) {
    if (!this.canUsePayment(payment)) {
      return Decimal(0)
    }

    if (payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH && !CashChanger.isEnabled) {
      // ドロアー運用時
      return Decimal(MAX_PAYMENT_AMOUNT)
    }

    switch (payment.paymentMethodType) {
      case PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE:
      case PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE:
        return Decimal(MAX_PAYMENT_AMOUNT)
      case PAYMENT_METHOD_TYPE.INTERNAL_POINT:
        return Decimal(Math.min(this.justAmountForPayment(payment), this.maxUsePoint))
      default:
        return this.justAmountForPayment(payment)
    }
  }

  /**
   * 小計値割引を適用します。
   * @param {CartDiscount} discount
   * @return {Cart}
   */
  applySubtotalDiscount (discount) {
    // オーナーズカードによる値引の場合
    if (discount.presetCode === OWNER_DISCOUNT.CODE) {
      return this.set('discountOrder', DISCOUNT_ORDER.DISCOUNT_FIRST)
        .set('ownerDiscount', discount)
        .set('usePoint', 0)
        .clearPayments()
    }
    // 小計値引の場合
    return this.set('discountOrder', DISCOUNT_ORDER.OWNER_DISCOUNT_FISRT)
      .set('discount', discount)
      .set('usePoint', 0)
      .clearPayments()
  }

  cancelDiscount (discountType) {
    // オーナー値引解除
    if (discountType === DISCOUNT_TYPE.OWNER) {
      return this.set('discountOrder', DISCOUNT_ORDER.OWNER_DISCOUNT_FISRT)
        .set('ownerDiscount', null)
        .set('usePoint', 0)
        .clearPayments()
    }
    // 従来小計値引解除
    return this.set('discountOrder', DISCOUNT_ORDER.DISCOUNT_FIRST)
      .set('discount', null)
      .set('usePoint', 0)
      .clearPayments()
  }

  setIsTaxfree (isTaxfree) {
    return this.set('isTaxfree', isTaxfree)._setItems(this.items) // To apply taxfree
  }

  get isTaxfreeApplied () {
    return !!this.items.find(item => item.isTaxfreeApplied)
  }

  get canApplyTaxfree () {
    return !!(this.shop && this.shop.is_taxfree)
  }

  /**
   * バンドルの不許可設定と、それに従ったバンドルの組み直しを行う。
   * @param disabled
   * @return {Cart}
   */
  setIsBundleDisabled (disabled) {
    return this.set('isBundleDisabled', disabled)._setItems(this.items)
  }

  deleteItemAt (itemIndex) {
    return this._setItems(this.items.remove(itemIndex))
  }

  /**
   *
   * @return {Cart}
   */
  clearItems () {
    return this._setItems(List())
      .set('discount', null)
      .set('ownerDiscount', null)
      .set('discountOrder', 0)
      .set('usePoint', 0)
      .clearPayments()
      .clearScannedPromotionBarcodes()
  }

  clearPayments () {
    return this.set(
      'payments',
      this.payments.map(payment => {
        return payment.updateAmount(Decimal(0))
      })
    )
  }

  setUsePoint (point) {
    return this.set('usePoint', point).clearPayments()
  }

  /**
   *
   * @param {Object} customer
   * @return {Immutable.Map<string, V>}
   */
  setCustomer (customer) {
    if (customer === null && this.customer === null) {
      return this
    } else if (customer === null && this.usePoint > 0) {
      return this.set('customer', customer).setUsePoint(0)
    } else {
      return this.set('customer', customer)
    }
  }

  /**
   *
   */
  get canSetCustomer () {
    return true
  }

  /**
   * 小計金額 (税計算前)
   * @return {*|Decimal.Decimal|Decimal}
   */
  get totalItemsBase () {
    return this.items.reduce((sum, item) => {
      return sum.add(item.salesTotal)
    }, new Decimal(0))
  }

  /**
   * 標準税率小計金額 (非課税商品含む)
   * @return {*|Decimal.Decimal|Decimal}
   */
  get standardTotalItemsBase () {
    return this.items.filter(item => item.product.taxCode === TAX_CODE.STANDARD).reduce((sum, item) => {
      return sum.add(item.salesTotal)
    }, Decimal(0))
  }

  /**
   *  軽減税率小計金額 (非課税商品含む)
   * @return {*|Decimal.Decimal|Decimal}
   */
  get reducedTotalItemsBase () {
    return this.items.filter(item => item.product.taxCode === TAX_CODE.REDUCED).reduce((sum, item) => {
      return sum.add(item.salesTotal)
    }, Decimal(0))
  }

  /**
   * 小計金額(税抜)
   * @returns {Decimal}
   */
  get totalItemsTaxless () {
    if (this.tax.isTaxIncluded) {
      return this.totalItemsBase.sub(this.totalItemsTax)
    } else {
      return this.totalItemsBase
    }
  }
  /**
   * 標準税率の小計金額(税抜)
   */
  get standardTotalItemsTaxless () {
    if (this.tax.isTaxIncluded) {
      return this.standardTotalItemsBase.sub(this.standardTotalItemsTax)
    } else {
      return this.standardTotalItemsBase
    }
  }

  /**
   * 軽減税率の小計金額(税抜)
   */
  get reducedTotalItemsTaxless () {
    if (this.tax.isTaxIncluded) {
      return this.reducedTotalItemsBase.sub(this.reducedTotalItemsTax)
    } else {
      return this.reducedTotalItemsBase
    }
  }

  /**
   * 小計金額(税額)
   * @returns {Decimal}
   */
  get totalItemsTax () {
    return this.standardTotalItemsTax.plus(this.reducedTotalItemsTax)
  }

  /**
   * 標準税率小計金額(税額)
   * @returns {Decimal}
   */
  get standardTotalItemsTax () {
    return this.calculateStandardTax(this.standardTaxableTotalBase)
  }

  /**
   * 軽減税率小計金額(税額)
   * @returns {Decimal}
   */
  get reducedTotalItemsTax () {
    return this.calculateReducedTax(this.reducedTaxableTotalBase)
  }

  /**
   * 小計金額(税込)
   * @returns {*}
   */
  get totalItemsTaxed () {
    if (this.tax.isTaxIncluded) {
      return this.totalItemsBase
    } else {
      return this.totalItemsBase.add(this.totalItemsTax)
    }
  }

  /**
   * 標準税率小計金額(税込)
   * @returns {*}
   */
  get standardTotalItemsTaxed () {
    if (this.tax.isTaxIncluded) {
      return this.standardTotalItemsBase
    } else {
      return this.standardTotalItemsBase.add(this.standardTotalItemsTax)
    }
  }

  /**
   * 標準税率小計金額(税込)
   * @returns {*}
   */
  get reducedTotalItemsTaxed () {
    if (this.tax.isTaxIncluded) {
      return this.reducedTotalItemsBase
    } else {
      return this.reducedTotalItemsBase.add(this.reducedTotalItemsTax)
    }
  }

  get taxfreeGeneralsTotalTaxless () {
    return CartItemTaxfree.generalsTotalTaxless(this.items, this.tax)
  }

  get taxfreeExpendablesTotalTaxless () {
    return CartItemTaxfree.expendablesTotalTaxless(this.items, this.tax)
  }

  get taxfreeNotApplicablesTotalTaxless () {
    return CartItemTaxfree.notApplicablesTotalTaxless(this.items, this.tax)
  }

  get taxfreeGeneralsTotalTax () {
    return CartItemTaxfree.generalsTotalTax(this.items, this.tax)
  }

  get taxfreeExpendablesTotalTax () {
    return CartItemTaxfree.expendablesTotalTax(this.items, this.tax)
  }

  get taxfreeNotApplicablesTotalTax () {
    return CartItemTaxfree.notApplicablesTotalTax(this.items, this.tax)
  }

  /**
   * 一般品に免税が適用されたか否か
   * @return {boolean}
   */
  get isTaxfreeAppliedForGeneral () {
    const firstGeneral = CartItemTaxfree.firstGeneral(this.items)
    return !!(firstGeneral && firstGeneral.isTaxfreeApplied)
  }

  /**
   * 消耗品に免税が適用されたか否か
   * @return {boolean}
   */
  get isTaxfreeAppliedForExpendable () {
    const firstExpendable = CartItemTaxfree.firstExpendable(this.items)
    return !!(firstExpendable && firstExpendable.isTaxfreeApplied)
  }

  /**
   * 合計個数
   * @return {integer}
   */
  get totalQuantity () {
    return this.items.reduce((sum, item) => {
      return sum + item.quantity
    }, 0)
  }

  /**
   * ゼロ円商品を除いた合計個数
   * @return {integer}
   */
  get totalQuantityWithoutZeroProduct () {
    return this.items.reduce((sum, item) => {
      return item.listPrice.lte(0) ? sum : sum + item.quantity
    }, 0)
  }

  /**
   * 小計値割引額
   * @return {Decimal}
   */
  get subtotalDiscountAmount () {
    return CartDiscount.discountAmount(
      this.discountOrder,
      this.discount,
      this.ownerDiscount,
      this.totalItemsBase,
      this.currency
    )
  }

  /**
   * オーナー割引額
   * @return {Decimal}
   */
  get ownerDiscountAmount () {
    return CartDiscount.ownerDiscountAmount(
      this.discountOrder,
      this.discount,
      this.ownerDiscount,
      this.totalItemsBase,
      this.currency
    )
  }

  /**
   * 小計値引き額
   * @return {Decimal}
   */
  get subtotalAmountDiscountAmount () {
    return this.discount && this.discount.discountType === DISCOUNT_TYPE.RATE
      ? new Decimal(0)
      : this.subtotalDiscountAmount
  }

  /**
   * 小計割引額
   * @return {Decimal}
   */
  get subtotalRateDiscountAmount () {
    return this.discount && this.discount.discountType === DISCOUNT_TYPE.RATE
      ? this.subtotalDiscountAmount
      : new Decimal(0)
  }

  /**
   * 小計割引率(%)
   * @return {Decimal}
   */
  get subtotalDiscountPercent () {
    return this.subtotalDiscountRate.mul(100)
  }

  /**
   * 小計割引率
   * @return {Decimal}
   */
  get subtotalDiscountRate () {
    return CartDiscount.discountRateTotal(this.discount)
  }

  /**
   * オーナー割引率(%)
   * @return {Decimal}
   */
  get ownerDiscountPercent () {
    return this.ownerDiscountRate.mul(100)
  }

  /**
   * オーナー割引率
   * @return {Decimal}
   */
  get ownerDiscountRate () {
    return CartDiscount.discountRateTotal(this.ownerDiscount)
  }

  /**
   * 標準税率
   */
  get standardTaxRate () {
    return this.tax.standardTaxRate
  }

  /**
   * 軽減税率
   */
  get reducedTaxRate () {
    return this.tax.reducedTaxRate
  }

  /**
   * 課税対象小計
   * @return {Decimal}
   */
  get taxableTotalBase () {
    return this.items.filter(item => item.product.tax.taxRule !== TAX_RULE.NONE).reduce((sum, item) => {
      return item.salesTotal.add(sum)
    }, Decimal(0))
  }

  /**
   * 標準税率課税対象小計
   * @return {Decimal}
   */
  get standardTaxableTotalBase () {
    return this.items
      .filter(item => item.product.tax.taxRule !== TAX_RULE.NONE && item.product.taxCode === TAX_CODE.STANDARD)
      .reduce((sum, item) => {
        return item.salesTotal.add(sum)
      }, Decimal(0))
  }

  /**
   * 軽減税率課税対象小計
   * @return {Decimal}
   */
  get reducedTaxableTotalBase () {
    return this.items
      .filter(item => item.product.tax.taxRule !== TAX_RULE.NONE && item.product.taxCode === TAX_CODE.REDUCED)
      .reduce((sum, item) => {
        return item.salesTotal.add(sum)
      }, Decimal(0))
  }

  /**
   * 課税対象小計 (税抜き)
   * @return {Decimal}
   */
  get taxableTotalTaxless () {
    return this.calculateTaxless(this.taxableTotalBase)
  }

  /**
   * 標準税率課税対象小計 (税抜き)
   * @return {Decimal}
   */
  get standardTaxableTotalTaxless () {
    return this.standardCalculateTaxless(this.standardTaxableTotalBase)
  }

  /**
   * 軽減税率課税対象小計 (税抜き)
   * @return {Decimal}
   */
  get reducedTaxableTotalTaxless () {
    return this.reducedCalculateTaxless(this.reducedTaxableTotalBase)
  }

  /**
   * 課税対象小計 (税込み)
   * @return {Decimal}
   */
  get taxableTotalTaxed () {
    return this.taxableTotalTaxless.add(this.calculateTax(this.taxableTotalBase))
  }

  /**
   * 標準税率課税対象小計 (税込み)
   * @return {Decimal}
   */
  get standardTaxableTotalTaxed () {
    return this.standardTaxableTotalTaxless.add(this.calculateStandardTax(this.standardTaxableTotalBase))
  }

  /**
   * 軽減課税対象小計 (税込み)
   * @return {Decimal}
   */
  get reducedTaxableTotalTaxed () {
    return this.reducedTaxableTotalTaxless.add(this.calculateReducedTax(this.reducedTaxableTotalBase))
  }

  /**
   * 合計金額 (税計算前)
   * @return {Decimal}
   */
  get totalBase () {
    return this.totalItemsBase.sub(this.subtotalDiscountAmount).sub(this.ownerDiscountAmount)
  }

  /**
   * 免税額合計
   */
  get taxfreeTotalTax () {
    let totalTax = new Decimal(0)
    if (this.isTaxfreeAppliedForExpendable) {
      totalTax = totalTax.add(this.taxfreeExpendablesTotalTax)
    }
    if (this.isTaxfreeAppliedForGeneral) {
      totalTax = totalTax.add(this.taxfreeGeneralsTotalTax)
    }
    return totalTax
  }

  /**
   * 合計金額(税額)
   */
  get totalTax () {
    if (this.isTaxfreeApplied) {
      let totalTax = this.taxfreeNotApplicablesTotalTax
      if (!this.isTaxfreeAppliedForExpendable) {
        totalTax = totalTax.add(this.taxfreeExpendablesTotalTax)
      }
      if (!this.isTaxfreeAppliedForGeneral) {
        totalTax = totalTax.add(this.taxfreeGeneralsTotalTax)
      }
      return totalTax
    } else {
      return this.standardTotalTax.plus(this.reducedTotalTax)
    }
  }

  /**
   * 標準税率対象の合計税額
   */
  get standardTotalTax () {
    if (this.isTaxfreeApplied) {
      return this.totalTax
    }
    return this.calculateStandardTax(this.discountedStandardTaxableTotal)
  }

  /**
   * 軽減税率対象の合計税額
   */
  get reducedTotalTax () {
    if (this.isTaxfreeApplied) {
      return this.totalTax
    }
    return this.calculateReducedTax(this.discountedReducedTaxableTotal)
  }

  /**
   * 小計値引適用後の標準税率課税対象額
   */
  get discountedStandardTaxableTotal () {
    return this.standardTaxableTotalBase.sub(this.standardSubTotalDiscount).sub(this.standardOwnerDiscount)
  }
  /**
   * 小計値引適用後の標準税率課税対象額(税抜)
   */

  get discountedStandardTaxableTotalTaxless () {
    return this.standardCalculateTaxless(this.discountedStandardTaxableTotal)
  }

  /**
   * 小計値引適用後の軽減税率課税対象額
   */
  get discountedReducedTaxableTotal () {
    return this.reducedTaxableTotalBase.sub(this.reducedSubTotalDiscount).sub(this.reducedOwnerDiscount)
  }
  /**
   * 小計値引適用後の軽減税率課税対象額(税抜)
   */
  get discountedReducedTaxableTotalTaxless () {
    return this.reducedCalculateTaxless(this.discountedReducedTaxableTotal)
  }

  /**
   * 小計/オーナー割引額を標準税率対象に按分
   */
  calculateStandardDiscount (discountAmount) {
    if (this.taxableTotalTaxless.lte(0)) {
      return new Decimal(0)
    }
    const standardDiscountAmount = discountAmount.mul(this.standardTaxableTotalTaxless).div(this.taxableTotalTaxless)

    if (standardDiscountAmount.toNumber() % 1 !== 0) {
      return this.standardTaxableTotalTaxless.lt(this.reducedTaxableTotalTaxless)
        ? standardDiscountAmount.floor()
        : standardDiscountAmount.ceil()
    }
    return standardDiscountAmount
  }

  /**
   * 標準税率小計値引額
   */
  get standardSubTotalDiscount () {
    return this.calculateStandardDiscount(this.subtotalDiscountAmount)
  }
  /**
   * 標準税率小計値引額(税抜)
   */
  get standardSubTotalDiscountTaxless () {
    return this.standardCalculateTaxless(this.standardSubTotalDiscount)
  }

  /**
   * 標準税率オーナー割引額
   */

  get standardOwnerDiscount () {
    return this.calculateStandardDiscount(this.ownerDiscountAmount)
  }
  /**
   * 標準税率オーナー割引額(税抜)
   */

  get standardOwnerDiscountTaxless () {
    return this.standardCalculateTaxless(this.standardOwnerDiscount)
  }

  /**
   * オーナー割引額合計
   */
  get ownerDiscountTaxless () {
    return this.standardOwnerDiscountTaxless.add(this.reducedOwnerDiscountTaxless)
  }

  /**
   * 小計/オーナー割引額を軽減税率対象に按分
   */
  calculateReducedDiscount (discountAmount) {
    if (this.taxableTotalTaxless.lte(0)) {
      return new Decimal(0)
    }
    const reducedDiscountAmount = discountAmount.mul(this.reducedTaxableTotalTaxless).div(this.taxableTotalTaxless)

    if (reducedDiscountAmount.toNumber() % 1 !== 0) {
      return this.standardTaxableTotalTaxless.lt(this.reducedTaxableTotalTaxless)
        ? reducedDiscountAmount.ceil()
        : reducedDiscountAmount.floor()
    }
    return reducedDiscountAmount
  }

  /**
   * 軽減税率小計値引額
   */
  get reducedSubTotalDiscount () {
    return this.calculateReducedDiscount(this.subtotalDiscountAmount)
  }
  /**
   * 軽減税率小計値引額(税抜)
   */
  get reducedSubTotalDiscountTaxless () {
    return this.reducedCalculateTaxless(this.reducedSubTotalDiscount)
  }

  /**
   * 軽減税率オーナー割引額
   */
  get reducedOwnerDiscount () {
    return this.calculateReducedDiscount(this.ownerDiscountAmount)
  }
  /**
   * 軽減税率オーナー割引額(税抜)
   */
  get reducedOwnerDiscountTaxless () {
    return this.reducedCalculateTaxless(this.reducedOwnerDiscount)
  }

  /**
   * 合計金額(税抜)
   * @return {Decimal}
   */
  get totalTaxless () {
    if (this.tax.isTaxIncluded) {
      return this.totalBase.sub(this.totalTax).sub(this.taxfreeTotalTax)
    } else {
      return this.totalBase
    }
  }

  /**
   * 合計金額(税込)
   * @return {Decimal}
   */
  get totalTaxed () {
    if (this.tax.isTaxIncluded) {
      return this.totalBase.sub(this.taxfreeTotalTax)
    } else {
      return this.totalBase.add(this.totalTax)
    }
  }

  /**
   * お預かり
   * @returns {Decimal}
   */
  get deposit () {
    return this.payments.reduce((sum, item) => {
      return sum.add(item.amount)
    }, new Decimal(0))
  }

  /**
   * お支払残り
   * @returns {Decimal}
   */
  get remainDeposit () {
    const remain = this.totalTaxed.sub(this.deposit)
    if (remain.lte(0)) {
      return new Decimal(0)
    }
    return remain
  }

  /**
   * おつり
   * @returns {Decimal}
   */
  get change () {
    /** @var {Decimal} */
    const change = this.deposit.sub(this.totalTaxed).sub(this.giftSurplus)
    if (change.lte(0)) {
      return new Decimal(0)
    }
    return change
  }

  /**
   * 券差額 = おつりの出ない商品券で合計金額をオーバーした分
   * @returns {Decimal}
   */
  get giftSurplus () {
    const total = this.totalTaxed
    if (total.gte(this.deposit)) {
      Decimal(0)
    }
    const activePayments = this.activePayments
    const unchangeablePayments = activePayments.filter(payment => {
      return (
        payment.paymentMethodType !== PAYMENT_METHOD_TYPE.CASH &&
        payment.paymentMethodType !== PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE
      )
    })
    const unchangeableAmount = unchangeablePayments.reduce((sum, payment) => {
      return sum.add(payment.amount)
    }, Decimal(0))

    if (unchangeableAmount.gt(total)) {
      // 券つり無し以外で合計金額を超えられる支払い方法はないので、この計算で券差額が出る
      return unchangeableAmount.sub(total)
    } else {
      return Decimal(0)
    }
  }

  /**
   * 券つり = おつりの出る商品券で購入した場合のおつりの金額
   * @returns {Decimal}
   */
  get giftChange () {
    if (this.totalTaxed.gte(this.deposit)) {
      Decimal(0)
    }
    const cashPayments = this.activePayments.filter(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.CASH)
    const cashAmount = cashPayments.reduce((sum, payment) => {
      return sum.add(payment.amount)
    }, Decimal(0))

    const giftChange = this.change.sub(cashAmount)
    return giftChange.gte(0) ? giftChange : Decimal(0)
  }

  /**
   * 収入印紙の印字が必要か否か
   * @return {boolean}
   */
  get needsTaxStamp () {
    const creditAmount = this.payments
      .filter(payment => payment.paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT)
      .reduce((sum, payment) => sum.add(payment.amount), Decimal(0))
    return this.totalTaxless.sub(creditAmount).gte(50000)
  }

  /**
   * 会計完了可能
   * @return {boolean}
   */
  get canComplete () {
    return this.totalQuantity > 0 && this.deposit.gte(this.totalTaxed)
  }

  complete (staff, orderNumberSequence, cartOrderProperties, createdAt) {
    return this.set('staff', staff)
      .set('orderNumberSequence', orderNumberSequence)
      .set('orderProperties', cartOrderProperties)
      .set('createdAt', createdAt)
  }

  completePend (staff, createdAt) {
    return this.set('staff', staff)
      .set('createdAt', createdAt)
      .set('discount', null)
      .set('ownerDiscount', null)
      .set('discountOrder', 0)
      .set('proratedItems', this.items)
      .set('isPended', true)
  }

  /**
   * 利用可能な最大ポイント数
   * @return {number}
   */
  get maxUsePoint () {
    if (this.customer) {
      return Math.min(Number(this.customer.customer_activity.points), this.totalTaxed.toNumber() + this.usePoint)
    } else {
      return 0
    }
  }

  /**
   * 仮会員でない会員がセットされているか否か
   * @return boolean
   */
  get customerIsNotTemporary () {
    return !!this.customer
  }

  /**
   * 入力可能な最大値引額
   */
  get maxAmountDiscountAmount () {
    return this.totalItemsBase
  }

  /**
   * 入力可能な最大割引率
   * @returns {*}
   */
  get maxRateDiscountRate () {
    return Decimal(1)
  }

  /**
   *
   * @return {boolean}
   */
  get canUsePoint () {
    return this.customerIsNotTemporary && this.maxUsePoint > 0 && !this.isTaxfree
  }

  /**
   * 小計値割引を利用できるか否かを返す
   * @return {boolean}
   */
  get canDiscount () {
    return this.items.count() > 0 && !this.isTaxfree
  }

  get activePayments () {
    return this.payments.filter(payment => payment.amount.gt(0))
  }

  get posOrderNumber () {
    if (!this.cashier || !this.orderNumberSequence) return null
    return ('000' + this.cashier.cashier_number).slice(-4) + '-' + ('000000' + this.orderNumberSequence).slice(-7)
  }

  get isCancel () {
    return false
  }

  setIsTrainingMode (isTraining) {
    return this.set('isTraining', isTraining)
  }

  setIsPushed (isPushed = true) {
    return this.set('isPushed', isPushed)
  }

  setId (id) {
    return this.set('id', id)
  }

  /**
   *
   * @param {string} key
   * @param value
   * @return {Cart}
   * @protected Don't access this method on other file
   */
  set (key, value) {
    return super.set(key, value)
  }

  calculateTax (amount) {
    return this.tax.calculateTax(amount, this.currency)
  }

  // 標準税率税額計算
  calculateStandardTax (amount) {
    return this.tax.calculateStandardTax(amount, this.currency)
  }

  // 軽減税率税額計算
  calculateReducedTax (amount) {
    return this.tax.calculateReducedTax(amount, this.currency)
  }

  calculateTaxless (amount) {
    return this.tax.calculateTaxless(amount, this.currency)
  }

  // 標準税率の税抜額計算
  standardCalculateTaxless (amount) {
    return this.tax.standardCalculateTaxless(amount, this.currency)
  }

  // 軽減税率の税抜額計算
  reducedCalculateTaxless (amount) {
    return this.tax.reducedCalculateTaxless(amount, this.currency)
  }

  /**
   * ポイント計算流れ↓
   * ポイント付与対象金額を基底金額で割った商(小数切捨)に対して
   * 基底ポイント数とポイント付与倍率をかける
   */
  get obtainedPoint () {
    if (!this.customer) {
      return null
    }
    const grantBasePointsAmount = new Decimal(this.shop.grant_base_points_amount)
    const grantBasePoints = this.shop.grant_base_points
    if (grantBasePointsAmount.eq(Decimal(0))) {
      return 0
    }

    // 各商品に按分された小計割引の合計
    let assignedDiscountSum = Decimal(0)
    // N倍設定されていない商品の合計額
    let notHouseAppliedTotalPrice = Decimal(0)
    // ポイント付与計算済の倍率
    let alreadyCalculatedRatios = []
    // 合計付与ポイント
    let totalGrantPoint = Decimal(0)

    // 適用されているポイント付与倍率毎にポイント計算
    for (let i = 0; i < this.items.size; i++) {
      const item = this.items.get(i)
      const assignedDiscount = this.assignSubTotalDiscountToItem(item, i, assignedDiscountSum)
      assignedDiscountSum = assignedDiscountSum.add(assignedDiscount)

      // 小計値引を按分後の商品価格
      const discountedItemPrice = item.salesTotalTaxless.sub(assignedDiscount)

      // 付与可能フラグ
      if (item.product.canGrantPoint !== GRANT_POINT.CAN) continue

      // ポイント付与倍率が設定されていない場合は、設定されていない商品合計額に加算
      if (!item.product.productPointRatio || item.product.productPointRatio.point_type !== POINT_TYPE.AEON_PET_POINT) {
        notHouseAppliedTotalPrice = notHouseAppliedTotalPrice.add(discountedItemPrice)
        continue
      }

      // ポイント付与倍率確認
      const productAssignedRatio = item.product.productPointRatio.assigned_ratio
      // 既にポイント計算されている倍率ならスルー
      if (this.isNewRatio(productAssignedRatio, alreadyCalculatedRatios)) {
        // その倍率が適用されている商品の合計額・付与ポイントを計算
        const thisRatioTotalPrice = this.calculateTotalPriceWithRatio(productAssignedRatio)
        const thisRatioGrantPoints = thisRatioTotalPrice
          .div(grantBasePointsAmount)
          .floor()
          .mul(grantBasePoints)
          .mul(productAssignedRatio)

        // 合計ポイントに追加
        totalGrantPoint = totalGrantPoint.add(thisRatioGrantPoints)

        // 計算した倍率を計算済リストに追加
        alreadyCalculatedRatios.unshift(productAssignedRatio)
      }
    }
    // ポイント付与倍率が設定されていない商品の付与ポイント計算(1倍)
    const notHouseAppliedPoints = notHouseAppliedTotalPrice.div(grantBasePointsAmount).floor().mul(grantBasePoints)

    // 合計付与ポイント
    return totalGrantPoint.add(notHouseAppliedPoints).toNumber()
  }

  /**
   * 小計値引を各商品に反映(按分)
   */
  assignSubTotalDiscountToItem (item, listNumber, assignedDiscountSum) {
    // 小計値引合計
    const subTotalDisCountTaxless = this.standardSubTotalDiscountTaxless.add(this.reducedSubTotalDiscountTaxless)
    const ownerDiscountTaxless = this.standardOwnerDiscountTaxless.add(this.reducedOwnerDiscountTaxless)
    const subTotalDiscountAmountSum = subTotalDisCountTaxless.add(ownerDiscountTaxless)
    if (!subTotalDiscountAmountSum.gt(Decimal(0))) return Decimal(0)

    // 最後の商品で端数分調整
    const isLastItem = listNumber === this.items.size - 1
    if (isLastItem) {
      return subTotalDiscountAmountSum.sub(assignedDiscountSum)
    }
    // 小計(税抜)に占める各商品小計(税抜)の割合で按分
    return subTotalDiscountAmountSum.mul(item.salesTotalTaxless).div(this.totalItemsTaxless).floor()
  }

  /**
   * 小計値引を各商品に反映(按分)
   */
  assignSubTotalDiscountToItemForOrder () {
    /** 小計値割引按分 */
    const subtotalProrated = oldItems => {
      const totalDiscountAmount = this.subtotalDiscountAmount.add(this.ownerDiscountTaxless)
      if (totalDiscountAmount.gt(0)) {
        const discount = this.subtotalDiscountAmount.eq(0) ? this.ownerDiscount : this.discount
        const { items, fraction } = this.prorated(discount, totalDiscountAmount, oldItems)
        const resultItems = fraction.eq(0) ? items : this._addFractionProrate(discount, items, fraction)
        return resultItems.map(resultItem => resultItem.setIsProrated(true))
      }
      return oldItems
    }

    const resultItems = subtotalProrated(this.items)
    return this.set('proratedItems', resultItems)
  }

  /**
   * 値引額按分  
   * 小数点以下を切り捨てて加算していった端数を返却する  
   * cartDiscount: 小計値割引  
   * discountAmount: 値割引合計額  
   * items: 按分前商品リスト  
   * 
   * @param {CartDiscount} cartDiscount
   * @param {Decimal} discountAmount 
   * @param {List.<CartItem>} items 
   * @returns {{items: List.<CartItem>, fraction: Decimal}}
   */
  prorated (cartDiscount, discountAmount, items) {
    /** 端数 */
    let fraction = new Decimal(0)
    /** 切り捨てられる端数を加算していく */
    const sumFraction = n => {
      // 按分金額の丸め処理
      fraction = fraction.add(n.sub(n.floor()))
      return n.floor()
    }

    if (items.size === 1) {
      const item = items.get(0)
      /** 商品小計毎の按分額 */
      const itemSubtotalProrate = item.salesTotalTaxless.div(this.totalItemsTaxless).mul(discountAmount)
      /** 単品毎の按分額 */
      const itemProrate = itemSubtotalProrate.div(item.quantity)

      for (let i = 0; i < item.quantity; i++) {
        sumFraction(itemProrate)
      }
      const newDiscount = CartItemDiscount.convertCartDiscountToCartItemDiscount(cartDiscount, itemProrate.floor())
      items = this._applyProrateDiscount(item, newDiscount, items)
    } else {
      items.forEach(item => {
        /** 商品小計毎の按分額 */
        const itemSubtotalProrate = item.salesTotalTaxless.div(this.totalItemsTaxless).mul(discountAmount)
        /** 単品毎の按分額 */
        const itemProrate = itemSubtotalProrate.div(item.quantity)

        if (item.quantity > 1) {
          for (let i = 0; i < item.quantity; i++) {
            sumFraction(itemProrate)
          }
          const newDiscount = CartItemDiscount.convertCartDiscountToCartItemDiscount(cartDiscount, itemProrate.floor())
          items = this._applyProrateDiscount(item, newDiscount, items)
        } else {
          const newDiscount = CartItemDiscount.convertCartDiscountToCartItemDiscount(
            cartDiscount,
            sumFraction(itemProrate)
          )
          items = this._applyProrateDiscount(item, newDiscount, items)
        }
      })
    }

    return { items, fraction }
  }

  /**
    * 按分用の値割引を適用させる
    * 
    * @param {CartItem} item 
    * @param {CartItemDiscount} prorateDiscount 
    * @param {List.<CartItem>} items 
    * @returns {List.<CartItem>}
    */
  _applyProrateDiscount (item, prorateDiscount = null, items) {
    const index = items.indexOf(item)
    if (prorateDiscount) {
      const newItem = item.applyDiscount(prorateDiscount)
      return items.set(index, newItem)
    }
    return items.set(index, item)
  }

  /**
   * 按分時に切り捨てられた端数をさらに按分する
   * 
   * @param {CartDiscount} cartDiscount 
   * @param {List.<CartItem>} items  
   * @param {Decimal} fraction 
   */
  _addFractionProrate (cartDiscount, items, fraction) {
    /** 小計値引き按分された商品リスト */
    const targetItems = items.filter(
      item => !!item.discounts.find(discount => discount.promotionType === cartDiscount.promotionType)
    )
    if (targetItems.size === 1) {
      let targetItem = targetItems.get(0)
      const prorateDiscount = CartItem.getProrateDiscount(targetItem)
      /** 元按分金額に1円加算した金額 */
      const newValue = prorateDiscount.value.add(1).round()
      const newDiscount = prorateDiscount.updateValue(newValue)
      // 按分額が変わるため別の商品として分離
      targetItem = targetItem.decrease(fraction.toNumber()) // 端数の値は商品個数以上にはならないため個数-端数額を設定する
      items = this._applyProrateDiscount(targetItem, null, items)
      items = items.push(targetItem.applyDiscount(newDiscount).changeQuantity(fraction.toNumber())) // 端数金額を個数とする
    } else {
      // 商品が1種類以上 または 按分によって1つの商品が分離された
      const addFraction = (cartItems, innerFraction) => {
        let newCartItems = cartItems.map(innerItem => {
          if (innerFraction.floor().eq(0)) {
            return innerItem
          }

          const prorateDiscount = CartItem.getProrateDiscount(innerItem)
          let newValue = new Decimal(0)

          // 1円ずつ按分額を加算していく
          newValue = prorateDiscount.value.add(1)
          innerFraction = innerFraction.sub(1)

          const newDiscount = prorateDiscount.updateValue(newValue.floor())
          return innerItem.applyDiscount(newDiscount)
        })

        if (innerFraction.floor().gt(1)) {
          // （商品単価 * 個数）= 商品小計が成立しない時 (※バンドルの時のみ入る条件)
          return this._addFractionProrate(cartDiscount, newCartItems, innerFraction)
        }
        return newCartItems
      }
      const priceSortDescItems = targetItems.sortBy(item => item.price).reverse() // 昇順でソートされるため降順に直す
      items = addFraction(priceSortDescItems, fraction)
    }

    return items
  }

  /**
   * 倍率の既出(計算済)を確認
   */
  isNewRatio (ratio, alreadyRatios) {
    for (let i = 0; i < alreadyRatios.length; i++) {
      if (ratio === alreadyRatios[i]) return false
    }
    return true
  }

  /**
   * 特定のポイント付与倍率が適用されている商品の合計額を計算
   */
  calculateTotalPriceWithRatio (ratio) {
    let amountPrice = Decimal(0)
    let assignedDiscountSum = Decimal(0)

    // obtainedPoint()と同様にカート内商品をリサーチ
    for (let i = 0; i < this.items.size; i++) {
      const item = this.items.get(i)
      const assignedDiscount = this.assignSubTotalDiscountToItem(item, i, assignedDiscountSum)
      assignedDiscountSum = assignedDiscountSum.add(assignedDiscount)
      const discountedItemPrice = item.salesTotalTaxless.sub(assignedDiscount)

      if (
        item.product.canGrantPoint !== GRANT_POINT.CAN ||
        !item.product.productPointRatio ||
        item.product.productPointRatio.point_type !== POINT_TYPE.AEON_PET_POINT
      ) {
        continue
      }

      // 特定の倍率が適用されている商品なら加算
      const productAssignedRatio = item.product.productPointRatio.assigned_ratio
      if (productAssignedRatio === ratio) amountPrice = amountPrice.add(discountedItemPrice)
    }
    return amountPrice
  }

  addScannedPromotionBarcode(barcode) {
    return this.set('scannedPromotionBarcodes', this.scannedPromotionBarcodes.push(barcode))
  }

  removeScannedPromotionBarcode(barcode) {
    return this.set('scannedPromotionBarcodes', this.scannedPromotionBarcodes.filter(item => item != barcode))
  }

  clearScannedPromotionBarcodes() {
    return this.set('scannedPromotionBarcodes', List())
  }
}
