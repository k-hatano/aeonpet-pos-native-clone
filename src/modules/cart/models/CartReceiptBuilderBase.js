import * as _ from 'underscore'
import { List } from 'immutable'
import { TAX_RULE, TAXFREE_TYPE, TAX_CODE } from '../../../common/models/Tax'
import ReceiptBuilder from '../../printer/models/ReceiptBuilder'
import SettingKeys from '../../setting/models/SettingKeys'
import { DISCOUNT_TYPE } from '../../promotion/models'
import CartItemDiscount from './CartItemDiscount'
import I18n from 'i18n-js'
import MultipleTaxRatesRepository from '../../product/repositories/MultipleTaxRatesRepository'

export default class CartReceiptBuilderBase extends ReceiptBuilder {
  constructor () {
    super()
    this.setReceiptInfo()
  }

  // レシートサインとレシート注釈データをクラスで保持
  setReceiptInfo () {
    this.receiptComment = ''
    this.receiptSign = ''

    const standardTaxData = MultipleTaxRatesRepository.findTaxWithTaxCode(TAX_CODE.REDUCED)

    standardTaxData.then(data => {
      if (data !== null) {
        this.receiptComment = data.receipt_comment
        this.receiptSign = data.receipt_sign
      }
    })
  }

  get _isPrintZeroProduct () {
    return this.getSetting(SettingKeys.RECEIPT.ACCOUNTING.IS_PRINT_ZERO_PRODUCTS)
  }

  bundlesElement (bundles, currency, taxfreeType = TAXFREE_TYPE.NOT_APPLICABLE) {
    let elements = []

    bundles.forEach(bundle => {
      elements = elements.concat(this._bundleElement(bundle, currency, taxfreeType))
    })

    return elements
  }

  /**
   *
   * @param {CartBundle} bundle
   * @param {string} currency
   * @param taxfreeType
   * @return {[null]}
   * @private
   */
  _bundleElement (bundle, currency, taxfreeType) {
    if (taxfreeType === TAXFREE_TYPE.NOT_APPLICABLE) {
      return [
        this.leftTextElement(bundle.name),
        ..._.flatten(bundle.items.map(item => this.cartItemElement(item, '  ')).toArray()),
        this._3columnElement('', 'バンドル値引き', this.formatMoney(bundle.bundleDiscountTotal.mul(-1), currency))
      ]
    } else {
      const filterdBundle = bundle.items.filter(item => item.product.taxfreeType === taxfreeType)
      if (filterdBundle.size > 0) {
        return [
          this.leftTextElement(bundle.name),
          ..._.flatten(bundle.items.map(item => this.cartItemElement(item, '  ')).toArray()),
          this._3columnElement('', 'バンドル値引き', this.formatMoney(bundle.bundleDiscountTotal.mul(-1), currency))
        ]
      } else {
        return []
      }
    }
  }

  /**
   *
   * @param {CartItem} cartItem
   * @param {string} offset
   * @return {[Object]}
   * @private
   */
  cartItemElement (cartItem, offset = '') {
    if (!this._isPrintZeroProduct && cartItem.listPrice.lte(0)) {
      return []
    }
    let productName = cartItem.product.shortName || cartItem.product.name
    const receiptSignSpace = ' '
    if (cartItem.product.taxCode === TAX_CODE.REDUCED && cartItem.product.tax.taxRule !== TAX_RULE.NONE &&
      !cartItem.isTaxfreeApplied) {
      productName = productName + receiptSignSpace + this.receiptSign
    }
    const elements = [
      {
        element: 'text',
        align: 'left',
        text: offset + cartItem.product.productCode
      },
      {
        element: 'text',
        align: 'left',
        text: offset + productName
      }
    ]

    const notTaxableMark = cartItem.product.tax.taxRule === TAX_RULE.NONE ? '非' : ''
    const itemTotalAmount = cartItem.price.mul(cartItem.quantity)
    const itemTotal = this.formatMoney(itemTotalAmount, cartItem.currency)
    elements.push(this._priceQuantityElement(
      notTaxableMark + this.formatMoney(cartItem.price, cartItem.currency),
      this.formatCount(cartItem.quantity),
      itemTotal
    ))

    elements.push(...this._cartItemDiscountsElements(cartItem))

    return elements
  }

  /**
   * 明細に対する割引の項目
   * @param {CartItem} cartItem
   * @return {[Object]}
   * @private
   */
  _cartItemDiscountsElements (cartItem) {
    const sale = CartItemDiscount.getSaleDiscount(cartItem.discounts)
    const manual = CartItemDiscount.getManualDiscount(cartItem.discounts)

    if (
      sale && sale.discountType === DISCOUNT_TYPE.RATE &&
      manual && manual.discountType === DISCOUNT_TYPE.RATE) {
      // 個別割引とセール割引が併用された場合のみ特別表記
      const rate = CartItemDiscount.discountRateTotal(cartItem.discounts)
      const amount = cartItem.rateDiscountAmount.mul(cartItem.quantity)
      return [
        this._3columnElement('', `セール${sale.value.mul(100)}` + I18n.t('promotion.percent_off'), ''),
        this._3columnElement('', `${manual.value.mul(100)}` + I18n.t('promotion.percent_off'), ''),
        this._3columnElement('', `(${rate.mul(100)}` + I18n.t('promotion.percent_off')
          , this.formatMoney(amount.mul(-1), cartItem.currency))
      ]
    } else {
      const elements = []
      if (sale) {
        elements.push(this._cartItemDiscountElement(sale, cartItem))
      }
      if (manual) {
        elements.push(this._cartItemDiscountElement(manual, cartItem))
      }

      return elements
    }
  }

  _priceQuantityElement (price, quantity, total) {
    const firstSpaceCount = 13
    const secondSpaceCount = 8
    const thirdSpaceCount = 13
    const firstSpace = ' '.repeat(Math.max(firstSpaceCount - this.lengthByShiftJisByte(total), 0))
    const secondSpace = ' '.repeat(Math.max(secondSpaceCount - this.lengthByShiftJisByte(quantity), 0))
    const thirdSpace = ' '.repeat(Math.max(thirdSpaceCount - this.lengthByShiftJisByte(price), 0))

    return {
      element: 'text',
      align: 'left',
      text: thirdSpace + price + secondSpace + quantity + firstSpace + total
    }
  }

  /**
   *
   * @param {CartItemDiscount} cartItemDiscount
   * @param {CartItem} cartItem
   * @private
   */
  _cartItemDiscountElement (cartItemDiscount, cartItem) {
    const amount = CartItemDiscount.discountTotalAmount(
      List([cartItemDiscount]), cartItem.price, cartItem.currency).mul(cartItem.quantity)
    switch (cartItemDiscount.discountType) {
      case DISCOUNT_TYPE.RATE:
        const saleMark = cartItemDiscount.isSale ? 'セール' : ''
        const label = `${saleMark}${cartItemDiscount.value.mul(100)}` + I18n.t('promotion.percent_off')
        return this._3columnElement('', label, this.formatMoney(amount.mul(-1), cartItem.currency))

      case DISCOUNT_TYPE.AMOUNT:
        const discountLabel = cartItemDiscount.isSale ? 'セール' : '値引き'
        return this._3columnElement('', discountLabel, this.formatMoney(amount.mul(-1), cartItem.currency))

      default:
        return null
    }
  }

  _3columnElement (left, center, right) {
    const space = ' '.repeat(Math.max(13 - this.lengthByShiftJisByte(right), 0))
    return this.combineLeftRightElement(left, center + space + right)
  }

  _trainingModeTitle (cart) {
    if (cart.isTraining) {
      return this.centerTextElement(I18n.t('receipt.training_mode'))
    }
  }

  // 消費税内訳表示
  taxContent (totalItemBase, totalTax, taxRate) {
    const spaceCount = 13
    const totalItemBaseSpace = ' '.repeat(Math.max(spaceCount - this.lengthByShiftJisByte(totalItemBase), 0))
    const totalTaxSpace = ' '.repeat(Math.max(spaceCount - this.lengthByShiftJisByte(totalTax), 0))
    const taxPercent = taxRate * 100
    const frontBracket = taxPercent < 10 ? '( ' : '('
    const totalItemBaseText = frontBracket + taxPercent + I18n.t('order.percent_taxable')
    const totalTaxText = frontBracket + taxPercent + I18n.t('order.percent_tax')

    return [
      this._3columnElement('', totalItemBaseText, totalItemBaseSpace + totalItemBase + ')'),
      this._3columnElement('', totalTaxText, totalTaxSpace + totalTax + ')')
    ]
  }
}
