import { Record, List } from 'immutable'
import { Decimal } from 'decimal.js'
import Tax, { TAXFREE_TYPE, TAX_CODE } from '../../../common/models/Tax'
import PromotionRepository from '../../promotion/repositories/PromotionRepository'
import ProductTagRepository from '../../product/repositories/ProductTagRepository'
import ProductRepository from '../../product/repositories/ProductRepository'
import StockItemsRepository from '../../stock/repositories/StockItemsRepository'
import logger from 'common/utils/logger'
import { STOCK_MODE } from '../../product/models'
import { CURRENCY_JPY } from '../../../common/models'

const CartItemProductRecord = Record({
  productId: null,
  productVariantId: null,
  productCode: '',
  price: new Decimal(0),
  originalPrice: Decimal(0),
  immutableOriginalPrice: Decimal(0),
  name: '',
  shortName: '',
  productNameSys21: '',
  tax: null,
  taxType: '',
  taxfreeType: TAXFREE_TYPE.NOT_APPLICABLE,
  originalTaxfreeType: TAXFREE_TYPE.NOT_APPLICABLE,
  productVariantName: '',
  sku: '',
  ownCompanyCode: '',
  makerCode: '',
  articleNumber: '',
  warehouseCode: '',
  costPrice: Decimal(0),
  closureTypeId: null,
  closureTypeName: null,
  closureTypeCode: null,
  vintage: null,
  labelVersion: null,
  boxVersion: null,
  bestByDate: null,
  capacity: null,
  liquorTaxType1: null,
  liquorTaxType2: null,
  quantityPerCarton: null,
  canGrantPoint: null,
  isSalesExcluded: null,
  bundleCandidates: List(),
  sales: List(),
  productPointRatio: null,
  tags: List(),
  isParentProduct: false,
  childProducts: List(),
  isDiscountable: false,
  productGroupId: null,
  stockMode: STOCK_MODE.USE,
  aopDomaine: '',
  stockItem: null,
  taxCode: 0,
  currency: CURRENCY_JPY
})

/**
 * @typedef {ProductEntity} ProductSetProduct
 * @property {integer} quantity
 */

/**
 * @class CartItemProduct
 * @property {string|null} productId
 * @property {string|null} productVariantId
 * @property {string} productCode
 * @property {string} name
 * @property {string} shortName
 * @property {string} productNameSys21
 * @property {Decimal} price
 * @property {Decimal} originalPrice - 売価変更前の商品価格
 * @property {Tax} tax
 * @property {Tax} taxType
 * @property {integer} taxfreeType
 * @property {integer} originalTaxfreeType - 上書き前の免税区分
 * @property {string} productVariantName
 * @property {string} sku
 * @property {string} ownCompanyCode
 * @property {string} makerCode
 * @property {string} articleNumber
 * @property {string} warehouseCode
 * @property {Decimal} costPrice
 * @property {string} closureTypeId
 * @property {string} closureTypeName
 * @property {string} closureTypeCode
 * @property {string} vintage
 * @property {string} labelVersion
 * @property {string} boxVersion
 * @property {string} bestByDate
 * @property {integer} capacity
 * @property {string} liquorTaxType1
 * @property {string} liquorTaxType2
 * @property {integer} quantityPerCarton
 * @property {integer} canGrantPoint
 * @property {integer} isSalesExcluded
 * @property {List} bundleCandidates
 * @property {List} sales
 * @property {Object} productPointRatio
 * @property {List} tags
 * @property {boolean} isParentProduct - Indicate if it is the parent of product set
 * @property {List.<ProductSetProduct>} childProducts
 * @property {boolean} isDiscountable
 * @property {string} productGroupId
 * @property {integer} stockMode
 * @property {string} aopDomaine
 * @property {Object} stockItem
 * @property {integer} taxCode
 */
export default class CartItemProduct extends CartItemProductRecord {
  constructor (product, productVariant, taxRate, bundleCandidates = [], sales = [], productPointRatio, tags = [], set = []) {
    super({
      productId: product.id,
      productVariantId: productVariant.id,
      productCode: product.product_code,
      name: product.name,
      shortName: product.short_name,
      price: new Decimal(productVariant.price),
      originalPrice: new Decimal(productVariant.price),
      immutableOriginalPrice: new Decimal(productVariant.price),
      tax: new Tax({ tax_rule: product.tax_rule, tax_rate: taxRate }),
      taxfreeType: Number(product.taxfree_type),
      originalTaxfreeType: Number(product.taxfree_type),
      productVariantName: productVariant.product_variant_name,
      sku: productVariant.sku,
      ownCompanyCode: productVariant.own_company_code,
      makerCode: productVariant.maker_code,
      articleNumber: productVariant.article_number,
      warehouseCode: productVariant.warehouse_code,
      costPrice: productVariant.cost_price && Decimal(productVariant.cost_price),
      canGrantPoint: productVariant.can_grant_points,
      isSalesExcluded: product.is_sales_excluded,
      bundleCandidates: new List(bundleCandidates),
      productPointRatio: productPointRatio != null ? productPointRatio : null,
      sales: List(sales),
      tags: new List(tags),
      isParentProduct: !!product.is_parent_product,
      childProducts: new List(set),
      isDiscountable: !!product.is_discountable,
      productGroupId: product.product_group_id,
      stockMode: productVariant.stock_mode,
      stockItem: productVariant.stock_item,
      taxCode: product.tax_code
    })
  }

  /**
   * 売価変更(価変(無))
   */
  changePrice (price) {
    return this.set('price', Decimal(price)).set('originalPrice', this.immutableOriginalPrice)
  }

  /**
   * 大元の売価変更(ズバリ価格)
   */
  changeOriginalPrice (price) {
    return this.set('price', Decimal(price)).set('originalPrice', Decimal(price))
  }

  /**
   * 売価変更(価変(無))されたか
   */
  get priceIsChanged (): boolean {
    return !this.price.eq(this.originalPrice)
  }

  /**
   * 大元の売価変更(ズバリ価格)されたか
   */
  get originalPriceIsChanged () {
    return !this.originalPrice.eq(this.immutableOriginalPrice)
  }

  /**
   * 売価変更を取り消す
   */
  resetPrice () {
    return this.set('price', this.originalPrice)
  }

  overwriteTaxfreeType (taxfreeType) {
    return this.set('taxfreeType', taxfreeType)
  }

  resetTaxfreeType () {
    return this.set('taxfreeType', this.originalTaxfreeType)
  }

  /**
   * 不変の販売価格の設定
   * (ズバリ価格適用の)保留会計再開時に利用
   */
  overwriteImmutableOriginalPrice (price) {
    return this.set('immutableOriginalPrice', Decimal(price))
  }

  static async createUsingRepositories (product, productVariant, cartTax, isReturn = false) {
    if (product.tax_code === null) {
      product.tax_code = TAX_CODE.STANDARD
    }

    const bundleCandidates = await PromotionRepository.findBundlesByProductId(product.id)
    const tags = await ProductTagRepository.findByProductId(product.id)
    const productPointRatio = await PromotionRepository.findProductPointRatioByProductId(product.id)
    const set = product.is_parent_product ? await ProductRepository.findSetById(product.id) : []
    const taxRate = product.tax_code === TAX_CODE.REDUCED ? cartTax.reducedTaxRate : cartTax.standardTaxRate
    var filteredSales = []

    if (!isReturn) {
      const sales = await PromotionRepository.findSalesByProductId(product.id)
      sales.forEach(sale => {
        if (sale.discount_price > productVariant.price) {
          // 割引額がマスタ金額より大きい場合、その旨をwarningログに記録し、割引適用しない
          logger.warning('Discount price is too large. code: ' + sale.code)
          return
        }
        filteredSales.push(sale)
      })
    }
    return new CartItemProduct(product, productVariant, taxRate, bundleCandidates, filteredSales, productPointRatio, tags, set)
  }

  static async createByPendedOrderProduct (product, changedPrice = undefined) {
    const bundleCandidates = await PromotionRepository.findBundlesByProductId(product.productId)
    const tags = await ProductTagRepository.findByProductId(product.productId)
    const stockItem = await StockItemsRepository.findByProductId(product.productId)
    const productPointRatio = await PromotionRepository.findProductPointRatioByProductId(product.productId)

    let pendedOrderProduct = product
    if (changedPrice !== undefined) {
      // 売価変更をした会計保留である場合は変更後の価格セットする
      pendedOrderProduct = pendedOrderProduct.set('price', changedPrice)
    }

    return pendedOrderProduct
      .set('bundleCandidates', new List(bundleCandidates))
      .set('tags', new List(tags))
      .set('stockItem', stockItem[0])
      .set('productPointRatio', productPointRatio)
  }

  /**
   *
   * @param {string} key
   * @param value
   * @return {Cart}
   * @protected Don't access this method on other file
   */
  set (key, value) {
    return super.set(key, value)
  }

  /**
   * 税込価格
   */
  get taxedPrice () {
    if (this.tax.isNotTaxable || this.tax.isTaxIncluded) {
      return this.price
    }
    const taxAmount = this.tax.calculateTax(this.price, this.currency)
    return this.price.add(taxAmount)
  }
}
