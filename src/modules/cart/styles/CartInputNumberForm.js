import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 36,
    width: 164,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  button: {
    width: 36,
    height: 36,
    color: 'white',
    fontSize: 26,
    backgroundColor: baseStyleValues.mainColor
  },
  buttonTouchableArea: {
    width: 52,
    height: 36,
    paddingLeft: 4,
    paddingRight: 12
  },
  number: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  numberTouchableArea: {
    width: 76,
    height: 36,
    paddingHorizontal: 8,
    paddingVertical: 3,
    backgroundColor: 'transparent'
  }
})
