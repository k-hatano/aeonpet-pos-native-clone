import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    backgroundColor: '#f7f7f7'
  },
  headerLabelsArea: {
    height: 17
  },
  taxFreeLabelBase: {
    backgroundColor: 'red',
    width: 96
  },
  taxFreeLabelText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  closureLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#f56c23',
    width: 89,
    height: 17
  },
  labelLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#417505',
    width: 89,
    height: 17
  },
  boxLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#4a90e2',
    width: 89,
    height: 17
  },
  vintageLabelBase: {
    overflow: 'hidden',
    backgroundColor: '#ff0068',
    width: 89,
    height: 17
  },
  invisibleLabelBase: {
    backgroundColor: '#f7f7f7',
    width: 89,
    height: 17
  },
  productLabelArea: {
    flex: 1
  },
  productLabelWrapper: {
    flexDirection: 'row',
    flex: 7,
    justifyContent: 'space-between'
  },
  productLabelText: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    height: 16,
    color: '#ffffff',
    letterSpacing: -0.26,
    textAlign: 'center'
  },
  headerArea: {
    height: 25,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  mainArea: {
    height: 55,
    marginLeft: 20,
    justifyContent: 'center'
  },
  productNameText: {
    height: 25
  },
  aopDomaineText: {
    height: 25
  },
  subArea: {
    height: 50,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24,
    alignItems: 'center'
  },
  priceText: {
    marginRight: 30,
    height: 24,
    lineHeight: 24
  },
  discountArea: {
    height: 22,
    flexDirection: 'row'
  },
  discountText: {
    color: 'red',
    marginLeft: 20,
    fontSize: 18
  },
  footer: {
    height: 10
  },
  openSetButton: {
    width: 160,
    height: 32,
    marginLeft: 20,
    marginVertical: 5
  },
  discountOptionsModal: {
    width: 400,
    height: 450,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  },
  cannotDiscountIcon: {
    width: 38,
    height: 27,
    marginLeft: 15
  },
  productSetContainer: {
    marginLeft: 20
  },
  productSetText: {
    marginVertical: 4,
    color: 'black'
  },
  discountLabelArea: {
    flex: 2,
    alignItems: 'flex-end',
    marginRight: 20
  },
  lineNumberArea: {
    width: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginHorizontal: 12
  },
  unitPriceText: {
    height: 21.5
  }
})
