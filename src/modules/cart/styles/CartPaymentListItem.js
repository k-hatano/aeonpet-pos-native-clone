import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: '100%',
    height: 65,
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconArea: {
    width: 28,
    height: 28,
    margin: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  nameText: {
    flex: 1
  },
  numberBox: {
    backgroundColor: '#f0f0f0',
    height: 40,
    width: 200,
    fontSize: 24,
    lineHeight: 24
  },
  unitText: {
    fontSize: 30,
    width: 40,
    margin: 8
  },
  button: {
    width: 86,
    height: 32,
    margin: 8,
    fontSize: 16,
    fontWeight: 'bold'
  },
  unavailableText: {
    color: 'red',
    fontSize: 16,
    margin: 20
  },
  disableText: {
    color: 'gray'
  }
})
