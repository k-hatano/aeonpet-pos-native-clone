import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 135,
    flexDirection: 'row',
    padding: 20
  },
  totalArea: {
    flex: 1.2
  },
  depositArea: {
    flex: 2
  },
  amountTitleText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  amountText: {
    fontSize: 30,
    color: baseStyleValues.mainTextColor,
    fontWeight: 'bold',
    textAlign: 'right'
  },
  amountSubInfoText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right'
  },
  depositAmountLine: {
    flexDirection: 'row',
    height: 34,
    marginBottom: 16,
    justifyContent: 'center'
  },
  depositAmountTitle: {
    flex: 1,
    fontSize: 20,
    lineHeight: 34,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right'
  },
  depositAmountText: {
    flex: 1,
    fontSize: 28,
    lineHeight: 34,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right'
  }
})
