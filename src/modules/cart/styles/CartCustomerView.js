import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 61,
    backgroundColor: 'white'
  },
  emptyViewLayout: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  selectCustomerButton: {
    width: 176,
    height: 40,
    fontSize: 24,
    marginRight: 24,
    fontWeight: 'bold'
  },
  customerViewLayout: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  customerCodeText: {
    marginHorizontal: 12,
    fontSize: 20,
    height: 24,
    color: '#4a4a4a'
  },
  customerNameText: {
    marginHorizontal: 12,
    fontSize: 20,
    height: 24,
    color: '#4a4a4a'
  },
  pointText: {
    marginHorizontal: 12,
    fontSize: 20,
    height: 24,
    color: '#4a4a4a'
  },
  circleCloseIcon: {
    width: 32,
    height: 32,
    marginHorizontal: 24
  },
  userIcon: {
    marginRight: 0,
    width: 18,
    height: 21
  },
  separator: {
    height: 1,
    backgroundColor: '#979797'
  }
})
