import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    flex: 1
  },
  subSeparator: {
    height: 1,
    marginHorizontal: 20,
    backgroundColor: '#d8d8d8'
  },
  paymentsSeparator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  },
  separator: {
    height: 1,
    backgroundColor: '#979797'
  },
  paymentMethodList: {
    flex: 1
  },
  submitButton: {
    fontSize: 36,
    height: 64,
    margin: 8
  }
})
