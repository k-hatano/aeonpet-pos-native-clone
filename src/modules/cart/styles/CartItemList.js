import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
