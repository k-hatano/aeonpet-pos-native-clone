import { StyleSheet } from 'react-native'
import baseStyleValues from '../../../common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
    width: '100%',
    backgroundColor: '#f7f7f7'
  },
  headerLabelsArea: {
    height: 16
  },
  taxFreeLabelBase: {
    backgroundColor: 'red',
    width: 96
  },
  taxFreeLabelText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  header: {
    margin: 20,
    marginTop: 8
  },
  bundleName: {
    height: 50
  },
  headerSubArea: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  openListButton: {
    width: 188
  },
  priceText: {
    marginRight: 200
  },
  listArea: {
    flexDirection: 'row'
  },
  groupIndex: {
    backgroundColor: baseStyleValues.mainColor,
    width: 16
  },
  items: {
    flex: 1
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
