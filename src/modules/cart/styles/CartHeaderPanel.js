import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
  },
  messageArea: {
    height: 64,
    backgroundColor: '#ffebd7',
    justifyContent: 'center'
  },
  message: {
    marginLeft: 20,
    color: baseStyleValues.mainTextColor
  },
  controlArea: {
    height: 64,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  controlLeftArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  disableBundleLayout: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 200,
    left: 20
  },
  taxfreeSwitchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 110
  },
  controlRightArea: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  resumeButton: {
    width: 144,
    height: 40,
    marginRight: 16,
    fontSize: 24,
    fontWeight: 'bold'
  },
  clearButton: {
    width: 94,
    height: 40,
    marginRight: 16,
    fontSize: 24,
    fontWeight: 'bold'
  },
  separator: {
    width: 1,
    height: 44,
    backgroundColor: '#979797',
    marginHorizontal: 20
  }
})

const staffIds = {
  id1: '0283890260D50599767D97511B8E90A5',
  id2: '08C6BF4A27D09AE2407789830FD0F8CF',
  id3: '28CA72D187E50E566D74965DC1566381',
  id4: '343E15A347742A96A561C0983A4EA9D5',
  id5: '45318F45F7C340D56A38B2A616BD78F1'
}

const staffCodes = {
  code1: '1',
  code2: '2',
  code3: '3',
  code4: '4',
  code5: '5'
}

const staffRoleIds = {
  id1: '54624FFFC7CB6CC40127465B82A50C37',
  id2: '5EBB72A738DC23C33ED19DA7CF42C82B',
  id3: '6F899626877A310A325166A9704CA8B2',
  id4: '815D02CCAF306029A8A3043CAC44F3B6',
  id5: 'D15A52DC0920300C17F8CA79FE8C7825'
}

const staffRolePermissionIds = {
  id1: '2F2F51E9A794E870EADB860EDAD6E7FB',
  id2: 'B29ADBC5D34D266376DE0EFD23D07614',
  id3: 'A1795D72C33966E903B6B23D13FB2852',
  id4: '4C64F6B70757E3A9668946BF5A1149BA',
  id5: 'A2D899F76289186F71B4CEE8AC25D038',
  id6: '2E85AC7E6DB7B302877401E6E9CA19CF',
  id7: '23C76A1F036701EA718BFC93833672F9',
  id8: 'AB923F0D6F96419BFB397D7FF59B95B1',
  id9: 'F05680694A3C2D5C0F0DB08CA76C84EA',
  id10: '810937D007C6DDCEFCF0F47CD7974EB7',
  id11: '2A190CA84D3C01BFA331CD3C0BCB4417',
  id12: '195BD5CF45117E9B9EABAA9FB7F1F010',
  id13: '9F54097D12DEA1DFB94C7B8E880AB305',
  id14: 'B4E80C19726F7C49252F6FE894E2089A',
  id15: 'D422A29E8C1F144253C97FCE51CA2574',
  id16: '7270E6DD68463F5C1D031EB3371AB4A6',
  id17: '81FA4B9ECA2A069C8B5D4D8A101A9F0A',
  id18: '87D599EAD8A91653D9429ED7AF2F3BEC',
  id19: '5F12410B46788B68708DA04F38382B89',
  id20: '7B962E6319888A95D948EC7A40BEFFAF',
  id21: '699F60FF6B633CC16CD901D1E2F4BB5E',
  id22: 'CFF56A6D91F9B8CC430132E43BFE6556',
  id23: 'FA86863412D8F55A4CE8596BD503F445',
  id24: 'ED26D3C5EF323DCE17B09893FA8A0E4E',
  id25: '7B4757C1B2F31837CDACF0A994C392A8',
  id26: '384426F8453B17B30DE1F86BF6961A3B',
  id27: '9BF6C5F645DAF0070F3111E6070CF4F3',
  id28: '22E9A0729D12DA50C3E232AD03D5CEB3',
  id29: 'D72EE3272320172E67D713DD829B74A3',
  id30: '38355B083693CF39E68C9984FA75883F'
}

export const staffRoles = [
  {
    id: staffRoleIds.id1,
    name: 'role1',
    staffs: [
      {
        id: staffIds.id1,
        status: 1,
        name: 'スタッフ１',
        staff_code: staffCodes.code1,
        staff_role_id: staffRoleIds.id1,
        sort_order: 1
      }
    ],
    permissions: [
      {
        id: staffRolePermissionIds.id1,
        staff_role_id: staffRoleIds.id1,
        permission_code: '1'
      },
      {
        id: staffRolePermissionIds.id2,
        staff_role_id: staffRoleIds.id1,
        permission_code: '2'
      },
      {
        id: staffRolePermissionIds.id3,
        staff_role_id: staffRoleIds.id1,
        permission_code: '3'
      },
      {
        id: staffRolePermissionIds.id4,
        staff_role_id: staffRoleIds.id1,
        permission_code: '4'
      },
      {
        id: staffRolePermissionIds.id5,
        staff_role_id: staffRoleIds.id1,
        permission_code: '5'
      },
      {
        id: staffRolePermissionIds.id6,
        staff_role_id: staffRoleIds.id1,
        permission_code: '6'
      },
      {
        id: staffRolePermissionIds.id7,
        staff_role_id: staffRoleIds.id1,
        permission_code: '7'
      },
      {
        id: staffRolePermissionIds.id8,
        staff_role_id: staffRoleIds.id1,
        permission_code: '8'
      },
      {
        id: staffRolePermissionIds.id9,
        staff_role_id: staffRoleIds.id1,
        permission_code: '9'
      },
      {
        id: staffRolePermissionIds.id10,
        staff_role_id: staffRoleIds.id1,
        permission_code: '10'
      },
      {
        id: staffRolePermissionIds.id11,
        staff_role_id: staffRoleIds.id1,
        permission_code: '11'
      },
      {
        id: staffRolePermissionIds.id12,
        staff_role_id: staffRoleIds.id1,
        permission_code: '12'
      },
      {
        id: staffRolePermissionIds.id13,
        staff_role_id: staffRoleIds.id1,
        permission_code: '13'
      },
      {
        id: staffRolePermissionIds.id14,
        staff_role_id: staffRoleIds.id1,
        permission_code: '14'
      },
      {
        id: staffRolePermissionIds.id15,
        staff_role_id: staffRoleIds.id1,
        permission_code: '15'
      },
      {
        id: staffRolePermissionIds.id16,
        staff_role_id: staffRoleIds.id1,
        permission_code: '16'
      },
      {
        id: staffRolePermissionIds.id17,
        staff_role_id: staffRoleIds.id1,
        permission_code: '17'
      },
      {
        id: staffRolePermissionIds.id18,
        staff_role_id: staffRoleIds.id1,
        permission_code: '18'
      },
      {
        id: staffRolePermissionIds.id19,
        staff_role_id: staffRoleIds.id1,
        permission_code: '19'
      },
      {
        id: staffRolePermissionIds.id20,
        staff_role_id: staffRoleIds.id1,
        permission_code: '20'
      },
      {
        id: staffRolePermissionIds.id22,
        staff_role_id: staffRoleIds.id1,
        permission_code: '21'
      },
      {
        id: staffRolePermissionIds.id23,
        staff_role_id: staffRoleIds.id1,
        permission_code: '22'
      }
    ]
  },
  {
    id: staffRoleIds.id2,
    name: 'role2',
    staffs: [
      {
        id: staffIds.id2,
        status: 1,
        name: 'スタッフ２',
        staff_code: staffCodes.code2,
        staff_role_id: staffRoleIds.id2,
        sort_order: 2
      }
    ],
    permissions: [
      {
        id: staffRolePermissionIds.id23,
        staff_role_id: staffRoleIds.id2,
        permission_code: '20'
      }
    ]
  },
  {
    id: staffRoleIds.id3,
    name: 'role3',
    staffs: [
      {
        id: staffIds.id3,
        status: 1,
        name: 'スタッフ３',
        staff_code: staffCodes.code3,
        staff_role_id: staffRoleIds.id3,
        sort_order: 3
      }
    ],
    permissions: [
      {
        id: staffRolePermissionIds.id24,
        staff_role_id: staffRoleIds.id3,
        permission_code: '19'
      }
    ]
  },
  {
    id: staffRoleIds.id4,
    name: 'role4',
    staffs: [
      {
        id: staffIds.id4,
        status: 1,
        name: 'スタッフ４',
        staff_code: staffCodes.code4,
        staff_role_id: staffRoleIds.id4,
        sort_order: 4
      }
    ],
    permissions: [
      {
        id: staffRolePermissionIds.id25,
        staff_role_id: staffRoleIds.id4,
        permission_code: '18'
      }
    ]
  },
  {
    id: staffRoleIds.id5,
    name: 'role5',
    staffs: [
      {
        id: staffIds.id5,
        status: 1,
        name: 'スタッフ５',
        staff_code: staffCodes.code5,
        staff_role_id: staffRoleIds.id5,
        sort_order: 5
      }
    ],
    permissions: [
      {
        id: staffRolePermissionIds.id26,
        staff_role_id: staffRoleIds.id5,
        permission_code: '17'
      }
    ]
  }
]
