import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    height: '100%'
  },
  itemList: {
    flex: 1
  }
})

export default styles
