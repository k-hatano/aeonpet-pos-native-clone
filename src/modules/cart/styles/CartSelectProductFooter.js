import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
    height: 80,
    backgroundColor: '#ffffff'
  },
  separator: {
    height: 1,
    backgroundColor: '#979797'
  },
  contentArea: {
    height: 79,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  summaryText: {
    marginRight: 27
  },
  subtotalButton: {
    width: 210,
    fontSize: 32,
    fontWeight: 'bold',
    margin: 8,
    height: 48
  }
})
