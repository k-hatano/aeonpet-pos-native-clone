import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 160,
    flexDirection: 'row',
    padding: 15
  },
  amountArea: {
    flex: 3
  },
  discountArea: {
    flex: 4
  },
  operationArea: {
    width: 159
  },
  button: {
    width: 144,
    height: 40,
    marginHorizontal: 8,
    marginBottom: 24,
    fontSize: 20
  },
  amountTitleText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  amountText: {
    fontSize: 30,
    color: baseStyleValues.mainTextColor,
    fontWeight: 'bold',
    textAlign: 'right'
  },
  amountSubInfoText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right'
  },
  discountLine: {
    flexDirection: 'row',
    height: 24
  },
  discountText: {
    fontSize: 20,
    color: 'red',
    flex: 1,
    textAlign: 'right'
  },
  discountPercentText: {
    fontSize: 20,
    color: 'red',
    textAlign: 'right'
  },
  discountTextSeparator: {
    fontSize: 20,
    color: 'red',
    flex: 0.1
  },
  discountOptionsModal: {
    width: 400,
    height: 450,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  }
})
