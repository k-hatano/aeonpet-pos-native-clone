import { connect } from 'react-redux'
import InputPasswordViewModalComponent from '../components/InputPasswordViewModal'
import { listStaffs, listPermissions } from '../actions'
import { MODULE_NAME } from '../models'
import AlertView from '../../../common/components/widgets/AlertView'
import StaffRepository from '../repositories/StaffRepository'
import { updateSetting } from 'modules/setting/actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import StaffRoleRepository from '../repositories/StaffRoleRepository'
import Modal from 'common/components/widgets/Modal'
import I18n from 'i18n-js'
import { onSelectStaff } from '../services'
import logger from 'common/utils/logger'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
  },
  onFind: async (staff, searchText, permissionCode) => {
    console.log(staff)
    if (staff.staff_password === searchText) {
      await onSelectStaff(staff, permissionCode, (role) => {
        dispatch(updateSetting({key: SettingKeys.STAFF.CURRENT_STAFF, value: staff}))
        dispatch(listPermissions(role.permissions))
      })
      return staff
    } else {
      return AlertView.showAsync(I18n.t('errors.incorrect_input_data'))
    }
  }
})

const mapStateToProps = state => ({
  staffs: state[MODULE_NAME].staffs
})

const InputPasswordViewModal = connect(mapStateToProps, mapDispatchToProps)(InputPasswordViewModalComponent)
export default InputPasswordViewModal

export const showInputPasswordViewModal = (staff, permissionCode, onCompleted) => {
  Modal.open(InputPasswordViewModal, {
    isBackgroundVisible: true,
    enableBackgroundClose: false,
    props: {
      staff: staff,
      permissionCode: permissionCode,
      onClose: (staff) => {
        Modal.close()
        onCompleted(staff)
      }
    }
  })
}
