import { connect } from 'react-redux'
import SelectStaffViewModalComponent from '../components/SelectStaffViewModal'
import { listStaffs, listPermissions, listPromotionMessages } from '../actions'
import { MODULE_NAME } from '../models'
import AlertView from '../../../common/components/widgets/AlertView'
import StaffRepository from '../repositories/StaffRepository'
import { updateSetting } from 'modules/setting/actions'
import SettingKeys from 'modules/setting/models/SettingKeys'
import StaffRoleRepository from '../repositories/StaffRoleRepository'
import PromotionRepository from '../../../modules/promotion/repositories/PromotionRepository'
import Modal from 'common/components/widgets/Modal'
import I18n from 'i18n-js'
import { onSelectStaff } from '../services'
import logger from 'common/utils/logger'

const mapDispatchToProps = dispatch => ({
  onInit: async () => {
    let messages = []
    await PromotionRepository.findPromotionMessages()
      .then((data) => {
        if (data !== null) {
          data.forEach(message => {
            messages.push(message.campaign_message)
          })
          dispatch(listPromotionMessages(messages))
        }
      })
  },
  onFind: async (searchText, isReader, permissionCode) => {
    try {
      const staffs = await StaffRepository.findByCode(searchText)
      if (staffs.length === 1) {
        if (isReader) {
          await onSelectStaff(staffs[0], permissionCode, (role) => {
            dispatch(updateSetting({key: SettingKeys.STAFF.CURRENT_STAFF, value: staffs[0]}))
            dispatch(listPermissions(role.permissions))
          })
        }
        return staffs[0]
      } else if (staffs.length === 0) {
        await logger.info(`Staff not found. code=${searchText}`)
        return AlertView.showAsync(I18n.t('message.A-03-E001'))
      } else {
        await logger.warning(`Found more than one staff. code=${searchText} found=${JSON.stringify(staffs)}`)
        return AlertView.showAsync(I18n.t('errors.unexpected_error'))
      }
    } catch (error) {
      await logger.warning(`Found more than one staff. code=${searchText} error=${error.message}`)
      return AlertView.showAsync(I18n.t('errors.unexpected_error'))
    }
  }
})

const mapStateToProps = state => ({
  staffs: state[MODULE_NAME].staffs,
  promotionMessages: state[MODULE_NAME].promotionMessages
})

const SelectStaffViewModal = connect(mapStateToProps, mapDispatchToProps)(SelectStaffViewModalComponent)
export default SelectStaffViewModal

export const showSelectStaffViewModal = (permissionCode, onCompleted) => {
  Modal.open(SelectStaffViewModal, {
    isBackgroundVisible: true,
    enableBackgroundClose: false,
    props: {
      permissionCode: permissionCode,
      onClose: (staff) => {
        Modal.close()
        onCompleted(staff)
      }
    }
  })
}
