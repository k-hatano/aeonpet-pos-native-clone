import { connect } from 'react-redux'
import I18n from 'i18n-js'
import { MODULE_NAME as staffModuleName } from '../models'
import { MODULE_NAME as settingModuleName } from '../../setting/models'
import SettingKeys from '../../setting/models/SettingKeys'
import AlertView from '../../../common/components/widgets/AlertView'
import { showSelectStaffViewModal } from './SelectStaffViewModal'
import StaffRoleRepository from '../repositories/StaffRoleRepository'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  permissions: state[staffModuleName].permissions,
  isFixStaff: state[settingModuleName].settings[SettingKeys.STAFF.IS_FIX_STAFF]
})

const hasPermission = (permissionCode, permissions) => {
  return permissions.find(permission => {
    return permission.permission_code === permissionCode
  })
}

const checkPermissionAndCallback = (onPress, args, permissionCode, permissions) => {
  if (onPress) {
    if (permissionCode === null || hasPermission(permissionCode, permissions)) {
      onPress(...args)
    } else {
      AlertView.show(I18n.t('message.A-01-E001'))
      return false
    }
  } else {
    console.warn('"onPress" property is not set.')
  }
  return true
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    onPress: (...args) => {
      const permissionCode = ownProps.permissionCode
      if (permissionCode === undefined) {
        console.warn('Please set "permissionCode" property')
      }

      if (stateProps.isFixStaff) {
        checkPermissionAndCallback(
          ownProps.onPress, args, permissionCode, stateProps.permissions)
      } else if (ownProps.skipSelectStaff) {
        if (hasPermission(permissionCode, stateProps.permissions)) {
          if (ownProps.onPress) {
            ownProps.onPress(...args)
          } else {
            console.warn('"onPress" property is not set.')
          }
        } else {
          showSelectStaffViewModal(null, async (staff) => {
            if (staff) {
              const role = await StaffRoleRepository.findByRoleId(staff.staff_role_id)
              checkPermissionAndCallback(
                ownProps.onPress, args, permissionCode, role.permissions)
            }
          })
        }
      } else if (permissionCode !== null) {
        showSelectStaffViewModal(null, async (staff) => {
          if (staff) {
            const role = await StaffRoleRepository.findByRoleId(staff.staff_role_id)
            checkPermissionAndCallback(
              ownProps.onPress, args, permissionCode, role && role.permissions)
          }
        })
      } else {
        ownProps.onPress(...args)
      }
    }
  }
}

export default (Component) => {
  return connect(mapStateToProps, mapDispatchToProps, mergeProps)(Component)
}
