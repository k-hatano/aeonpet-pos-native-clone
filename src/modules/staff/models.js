export const MODULE_NAME = 'staff'

export const PERMISSION_CODES = {
  OPEN_SALES: 'open',
  NEW_ORDER: 'new_order',
  SEARCH_ORDER: 'order_search',
  CANCEL_ORDER: 'cancel_order',
  RETURN_ORDER: 'return_order',
  PRINT_TAXFREE_RECEIPT: 'print_taxfree_receipt',
  INDEPENDENT_RETURN_ORDER: 'independent_return_order',
  CASHIER_BALANCE: 'cashier_balance',
  CHECK_SALES: 'check_sales',
  CLOSE_SALES: 'close_sales',
  FORCE_CLOSE_SALES: 'force_close_sales',
  OPEN_DRAWER: 'drawer_open',
  SETTING: 'setting',
  SYNC_DATA: 'sync_data',
  SEARCH_CUSTOMER: 'customer_search',
  SEARCH_STOCK: 'stock_search',
  STOCK_TAKING: 'stock_taking',
  STOCK_ADJUSTMENT: 'stock_adjustment',
  STOCK_MOVE_IN: 'stock_in',
  STOCK_MOVE_OUT: 'stock_out',
  TRAINING: 'training',
  SUMMARY_CLOSE_SALES: 'close_sales_summary',
  CUSTOMER_REGISTER: 'customer_register'
}
