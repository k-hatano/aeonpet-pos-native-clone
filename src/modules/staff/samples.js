import * as _ from 'underscore'
import generateUuid from '../../common/utils/generateUuid'
import { sampleStaffRoleMap } from './repositories/sample/SampleStaffRoleRepository'

export const sampleStaffMap = {
  staff1: {
    id: generateUuid(),
    status: 1,
    name: 'スタッフ１',
    staff_code: '0001',
    staff_role_id: sampleStaffRoleMap.allPermission.id
  },
  staff2: {
    id: generateUuid(),
    status: 1,
    name: 'スタッフ2',
    staff_code: '0002',
    staff_role_id: sampleStaffRoleMap.staff.id
  },
  staff3: {
    id: generateUuid(),
    status: 1,
    name: 'スタッフ3',
    staff_code: '0003',
    staff_role_id: sampleStaffRoleMap.staff.id
  },
  staff4: {
    id: generateUuid(),
    status: 1,
    name: 'スタッフ4',
    staff_code: '0004',
    staff_role_id: sampleStaffRoleMap.staff.id
  },
  staff5: {
    id: generateUuid(),
    status: 1,
    name: 'スタッフ5',
    staff_code: '0005',
    staff_role_id: sampleStaffRoleMap.customerSearcher.id
  }
}
export const sampleStaffs = _.values(sampleStaffMap)
