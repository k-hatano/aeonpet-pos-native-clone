import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  staffs: [],
  permissions: [],
  promotionMessages: []
}

const handlers = {
  [actions.listPermissions]: (state, action) => ({
    ...state,
    permissions: action.payload
  }),
  [actions.listPromotionMessages]: (state, action) => ({
    ...state,
    promotionMessages: action.payload
  })
}

export default handleActions(handlers, defaultState)
