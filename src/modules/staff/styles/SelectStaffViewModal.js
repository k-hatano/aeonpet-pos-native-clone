import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: 640,
    height: 232,
    marginBottom: 340
  },
  titleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: baseStyleValues.mainTextColor,
    marginTop: 5,
    marginBottom: 10
  },
  messageText: {
    color: baseStyleValues.mainTextColor,
    fontSize: 24,
    marginTop: 2,
    marginBottom: 2
  },
  messageArea: {
    marginTop: 14,
    marginBottom: 14,
    marginLeft: 32,
    marginRight: 32,
    flex: 1
  }
})
