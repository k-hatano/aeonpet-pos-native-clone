import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
  },
  searchFormArea: {
    height: 86,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 32
  },
  searchInputWrapper: {
    flex: 1,
    marginRight: 16
  },
  searchInput: {
    height: 48,
    fontSize: 24,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 16
  },
  searchButton: {
    width: 96,
    height: 48,
    fontSize: 24
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  },
  emptyText: {

  },
  listArea: {
    paddingHorizontal: 32,
    marginTop: 20,
    height: '80%'
  },
  listItem: {
    height: 86,
    paddingHorizontal: 20
  },
  staffNameText: {
    marginTop: 16
  },
  staffCodeText: {
    marginTop: 8
  },
  messageText: {
    color: baseStyleValues.mainTextColor,
    fontSize: 24,
    marginLeft: 32,
    marginTop: 14
  }
})
