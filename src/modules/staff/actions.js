import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listStaffs = createAction(`${MODULE_NAME}_listStaffs`)
export const listPermissions = createAction(`${MODULE_NAME}_listPermissions`)
export const listPromotionMessages = createAction(`${MODULE_NAME}_listPromotionMessages`)
