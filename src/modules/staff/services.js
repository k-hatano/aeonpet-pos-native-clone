import I18n from 'i18n-js'
import StaffRoleRepository from './repositories/StaffRoleRepository'
import AlertView from '../../common/components/widgets/AlertView'

/**
 * 担当者が選択された時に呼び出す関数です。
 * 担当者の権限を確認し、権限を持っていればonSuccessコールバックを呼び出します。
 * onSuccess内で担当者選択状態を保存する実装を行ってください。
 * onSuccessの引数には担当者に設定されているロールのエンティティが渡されます
 *
 * @param staff
 * @param permissionCode
 * @param onSuccess
 * @return {Promise<void>}
 */
export async function onSelectStaff (staff, permissionCode, onSuccess) {
  try {
    const role = await StaffRoleRepository.findByRoleId(staff.staff_role_id)
    const permission = role.permissions.find(permission => {
      return permission.permission_code === permissionCode
    })
    if (permissionCode && !permission) {
      await AlertView.showAsync(I18n.t('message.A-01-E001'))
    } else {
      await onSuccess(role)
    }
  } catch (error) {
    console.error(error)
  }
}
