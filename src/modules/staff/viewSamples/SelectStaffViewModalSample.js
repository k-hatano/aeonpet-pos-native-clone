import SelectStaffViewModal from '../components/SelectStaffViewModal'

export default {
  name: 'SelectStaffViewModal',
  component: SelectStaffViewModal,
  properties: [
    {
      title: 'Default',
      property: {
        staffs: [
          {name: 'Test', staff_code: 'a'}
        ]
      }
    },
    {
      title: 'Empty',
      property: {
        staffs: []
      }
    }
  ],
  frames: [
    {
      title: 'W683',
      style: {width: 683}
    }
  ]
}
