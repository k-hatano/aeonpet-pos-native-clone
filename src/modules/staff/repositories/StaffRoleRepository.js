import StandardStaffRoleRepository from './standard/StandardStaffRoleRepository'
import SampleStaffRoleRepository from './sample/SampleStaffRoleRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StaffRoleRepository {
  static _implement = new SampleStaffRoleRepository()

  static async findByRoleId (roleId) {
    return this._implement.findByRoleId(roleId)
  }

  static async clearAll () {
    return this._implement.clearAll()
  }

  static async syncRemoteToLocal () {
    return this._implement.syncRemoteToLocal()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStaffRoleRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardStaffRoleRepository()
        break

      default:
        this._implement = new SampleStaffRoleRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StaffRoleRepository', (context) => {
  StaffRoleRepository.switchImplement(context)
})
