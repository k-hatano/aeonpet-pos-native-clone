import * as _ from 'underscore'
import { PERMISSION_CODES } from '../../models'
import generateUuid from '../../../../common/utils/generateUuid'
import logger from 'common/utils/logger'

const makePermissions = (permissionCodes, staffRoleId) => {
  return permissionCodes.map(code => {
    return {
      id: generateUuid(),
      permission_code: code,
      staff_role_id: staffRoleId
    }
  })
}

const id1 = '7a4c0a66f4034aa4836417c2baae26e2'
const id2 = '4f56070a0d284e11a8567cf9263d96d7'
const id3 = '4f56070a0d284e11a8567cf9263d96d8'
const id4 = '49045a14249d41ec87878b64022d2a2d'

export const sampleStaffRoleMap = {
  allPermission: {
    id: id1,
    name: '管理者',
    permissions: makePermissions(_.values(PERMISSION_CODES), id1)
  },
  staff: {
    id: id2,
    name: 'レジ打ちバイト',
    permissions: makePermissions([
      PERMISSION_CODES.NEW_ORDER,
      PERMISSION_CODES.ORDER,
      PERMISSION_CODES.OPEN_DRAWER
    ], id2)
  },
  customerSearcher: {
    id: id3,
    name: '顧客検索者',
    permissions: makePermissions([
      PERMISSION_CODES.SEARCH_CUSTOMER
    ], id3)
  },
  leader: {
    id: id4,
    name: 'バイトリーダー',
    permissions: makePermissions([
      PERMISSION_CODES.ORDER,
      PERMISSION_CODES.OPEN_DRAWER
    ], id4)
  }
}

export const sampleStaffRoles = _.values(sampleStaffRoleMap)

export default class SampleStaffRoleRepository {
  async findByRoleId (roleId) {
    return sampleStaffRoles.find(staffRole => (staffRole.id === roleId))
  }

  async syncRemoteToLocal () {
    logger.info('SampleStaffRoleRepository.syncRemoteToLocal', 'Repository')
  }

  async clearAll () {
    logger.info('SampleStaffRoleRepository.clearAll', 'Repository')
  }
}
