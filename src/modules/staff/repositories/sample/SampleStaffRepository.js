import logger from 'common/utils/logger'
import { sampleStaffs } from '../../samples'

export default class SampleStaffRepository {
  async findAll () {
    return sampleStaffs
  }

  async findByCode (staffCode) {
    return sampleStaffs.filter(staff => (staff.staff_code === staffCode))
  }

  async findById (staffId) {
    return sampleStaffs.find(staff => staff.id === staffId)
  }

  async syncStaff () {
    logger.info('SampleStaffRepository.syncStaff', 'Repository')
  }

  async syncRemoteToLocal () {
    logger.info('SampleStaffRepository.syncRemoteToLocal', 'Repository')
  }

  async clearAll () {
    logger.info('SampleStaffRepository.clearAll', 'Repository')
  }
}
