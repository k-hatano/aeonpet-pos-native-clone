import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import AppEvents from '../../../../common/AppEvents'
import StaffEntity from './StaffEntity'
import StaffRolePermissionEntity from './StaffRolePermissionEntity'

const StaffRole = sequelize.define(
  'StaffRole',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING
  }, {
    tableName: 'staff_roles',
    underscored: true,
    timestamps: false
  }
)

StaffRole.hasMany(StaffEntity, {foreignKey: 'staff_role_id', as: 'staffs'})
StaffRole.hasMany(StaffRolePermissionEntity, {foreignKey: 'staff_role_id', as: 'permissions'})

export default StaffRole
