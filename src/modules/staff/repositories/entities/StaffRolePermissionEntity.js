import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import AppEvents from '../../../../common/AppEvents'

const StaffRolePermission = sequelize.define(
  'StaffRolePermission',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    staff_role_id: Sequelize.STRING,
    permission_code: Sequelize.STRING
  }, {
    tableName: 'staff_role_permissions',
    underscored: true,
    timestamps: false
  }
)

export default StaffRolePermission
