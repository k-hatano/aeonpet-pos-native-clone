import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'

const Staff = sequelize.define(
  'Staff',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    staff_code: Sequelize.STRING,
    staff_password: Sequelize.STRING,
    staff_role_id: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'staffs',
    underscored: true,
    timestamps: false
  }
)

export default Staff
