import StandardStaffRepository from './standard/StandardStaffRepository'
import SampleStaffRepository from './sample/SampleStaffRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StaffRepository {
  static _implement = new SampleStaffRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static async findByCode (staffCode) {
    return this._implement.findByCode(staffCode)
  }

  static async findById (staffId) {
    return this._implement.findById(staffId)
  }

  static async syncStaff () {
    return this._implement.syncStaff()
  }

  static async syncRemoteToLocal () {
    return this._implement.syncRemoteToLocal()
  }

  static async clearAll () {
    return this._implement.clearAll()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStaffRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardStaffRepository()
        break

      default:
        this._implement = new SampleStaffRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StaffRepository', (context) => {
  StaffRepository.switchImplement(context)
})
