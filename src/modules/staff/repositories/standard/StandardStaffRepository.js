import * as _ from 'underscore'
import StaffEntity from '../entities/StaffEntity'
import StaffRoleEntity from '../entities/StaffRoleEntity'
import StaffRolePermissionEntity from '../entities/StaffRolePermissionEntity'
import { handleAxiosError } from '../../../../common/errors'
import fetcher from 'common/models/fetcher'
import AppEvents from 'common/AppEvents'
import { sampleStaffs } from '../../samples'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import { sequelize } from 'common/DB'

export default class StandardStaffRepository {
  async findAll () {
    return StaffEntity.findAll({
      order: [
        ['created_at', 'DESC']
      ]
    })
  }

  async findByCode (staffCode) {
    return StaffEntity.findAll({
      where: {
        staff_code: staffCode
      },
      order: [
        ['created_at', 'DESC']
      ],
      raw: true
    })
  }

  // FIXME Fix these methods
  async findById (staffId) {
    return StaffEntity.findById(staffId, { raw: true })
  }

  async syncStaff () {
    let staffs = []
    let staffRoles = []

    try {
      const staffResponse = await fetcher.get('staffs', {...encodeCriteria({ status: 1 }), shouldFilterByStatus: true})
      const staffRoleResponse = await fetcher.get('staff-roles')
      staffs = staffResponse.data
      staffRoles = staffRoleResponse.data
    } catch (error) {
      handleAxiosError(error)
    }
    try {
      await sequelize.transaction(async function (t) {
        await StaffEntity.destroy({ where: {} }, {transaction: t})
        await StaffEntity.bulkCreate(staffs, {transaction: t})
        await StaffRolePermissionEntity.destroy({ where: {} }, {transaction: t})
        await StaffRolePermissionEntity.bulkCreate(_.flatten(staffRoles.map(role => {
          return role.staff_role_permissions
        })), {transaction: t})
        await StaffRoleEntity.destroy({ where: {} }, {transaction: t})
        staffRoles.forEach(role => delete role.staff_role_permissions)
        await StaffRoleEntity.bulkCreate(staffRoles, {transaction: t})
      })
    } catch (err) {
      handleAxiosError(err)
    }
  }

  async syncRemoteToLocal () {
    let staffs = []
    try {
      const response = await fetcher.get('staffs', {...encodeCriteria({ status: 1}), shouldFilterByStatus: true})
      staffs = response.data
    } catch (error) {
      handleAxiosError(error)
    }

    try {
      await StaffEntity.bulkCreate(staffs)
    } catch (error) {
      // TODO
    }
  }

  async clearAll () {
    try {
      await StaffEntity.destroy({ where: {} })
    } catch (error) {
      // TODO
    }
  }

  async createSamples () {
    const existingCount = await StaffEntity.count()
    if (existingCount === 0) {
      await StaffEntity.bulkCreate(sampleStaffs)
    }
  }
}

AppEvents.onSampleDataCreate('StandardStaffRepository', async () => {
  return (new StandardStaffRepository()).createSamples()
})
