import * as _ from 'underscore'
import { handleAxiosError } from '../../../../common/errors'
import AppEvents from 'common/AppEvents'
import fetcher from 'common/models/fetcher'
import StaffRoleEntity from '../entities/StaffRoleEntity'
import StaffRolePermissionEntity from '../entities/StaffRolePermissionEntity'
import { sampleStaffRoles } from '../sample/SampleStaffRoleRepository'

export default class StandardStaffRoleRepository {
  async findByRoleId (roleId) {
    return StaffRoleEntity.find({
      where: {
        id: roleId
      },
      include: [{
        model: StaffRolePermissionEntity,
        as: 'permissions'
      }]
    })
  }

  async syncRemoteToLocal () {
    let staffRoles = []
    try {
      const response = await fetcher.get('staff-roles')
      staffRoles = response.data
    } catch (error) {
      handleAxiosError(error)
    }

    try {
      await StaffRolePermissionEntity.bulkCreate(_.flatten(staffRoles.map(role => {
        return role.staff_role_permissions
      })))
      staffRoles.forEach(role => delete role.staff_role_permissions)
      await StaffRoleEntity.bulkCreate(staffRoles)
    } catch (error) {
      // TODO
    }
  }

  async clearAll () {
    try {
      await StaffRoleEntity.destroy({ where: {} })
      await StaffRolePermissionEntity.destroy({ where: {} })
    } catch (error) {
      console.log(error)
      console.error(error)
      // TODO
    }
  }

  async createSamples () {
    const existingCount = await StaffRoleEntity.count()
    if (existingCount === 0) {
      await StaffRoleEntity.bulkCreate(sampleStaffRoles.map(role => {
        return { id: role.id, name: role.name }
      }))
      await StaffRolePermissionEntity.bulkCreate(
        _.flatten(sampleStaffRoles.map(role => role.permissions))
      )
    }
  }
}

AppEvents.onSampleDataCreate('StandardStaffRoleRepository', async () => {
  return (new StandardStaffRoleRepository()).createSamples()
})
