jest.mock('react-native')
jest.mock('react-native-filesystem-v1')
jest.mock('common/DB')
import * as _ from 'underscore'
import StandardStaffRoleRepository from '../StandardStaffRoleRepository'
import Migrator from 'common/utils/migrator'
import { sampleStaffRoleMap, sampleStaffRoles } from '../../sample/SampleStaffRoleRepository'
import { sampleStaffs } from '../../../samples'
import generateUuid from '../../../../../common/utils/generateUuid'

describe('StandardStaffRoleRepository test sample', () => {
  beforeEach(async () => {
    await Migrator.up()
  })

  afterEach(async () => {
    await Migrator.down()
  })

  it('Create samples', async () => {
    const repository = new StandardStaffRoleRepository()
    await repository.createSamples()
    const staffRole = await repository.findByRoleId(sampleStaffRoleMap.allPermission.id)
    expect(staffRole.id).toBe(sampleStaffRoleMap.allPermission.id)
    expect(staffRole.permissions.length).toBe(sampleStaffRoleMap.allPermission.permissions.length)
  })

  // it('Create Sample Data SQL', async () => {
  //   const shopId = '3EBAA700A4A649479A5708E6E656B04C'
  //
  //   let staffSql = 'insert into staffs (id, status, name, staff_code, staff_role_id) VALUES ' + "\n"
  //   let shopStaffSql = 'insert into shop_staffs (id, shop_id, staff_id, staff_role_id) VALUES' + "\n"
  //   sampleStaffs.map(staff => {
  //     staffSql += '(' + [
  //         `UNHEX('${staff.id}')`,
  //         `${staff.status}`,
  //         `'${staff.name}'`,
  //         `'${staff.staff_code}'`,
  //         `UNHEX('${staff.staff_role_id}')`
  //       ].join(', ') + "),\n"
  //     shopStaffSql += '(' + [
  //         `UNHEX('${generateUuid()}')`,
  //         `UNHEX('${shopId}')`,
  //         `UNHEX('${staff.id}')`,
  //         `UNHEX('${staff.staff_role_id}')`
  //       ].join(', ') + "),\n"
  //   })
  //
  //   let staffRoleSql = 'insert into staff_roles (id) VALUES ' + "\n"
  //   let staffRoleTranslationsSql = 'insert into staff_role_translations (id, staff_role_id, locale, name) VALUES ' + "\n"
  //   let staffRolePermissionsSql = 'insert into staff_role_permissions (id, permission_code, staff_role_id) VALUES ' + "\n"
  //   sampleStaffRoles.forEach(role => {
  //     staffRoleSql += '(' + [
  //         `UNHEX('${role.id}')`
  //       ].join(', ') + "),\n"
  //
  //     staffRoleTranslationsSql += '(' + [
  //         `UNHEX('${generateUuid()}')`,
  //         `UNHEX('${role.id}')`,
  //         "'ja'",
  //         `'${role.name}'`
  //       ].join(', ') + "),\n"
  //
  //     role.permissions.forEach(permission => {
  //       staffRolePermissionsSql += '(' + [
  //           `UNHEX('${permission.id}')`,
  //           `'${permission.permission_code}'`,
  //           `UNHEX('${role.id}')`,
  //         ].join(', ') + "),\n"
  //     })
  //   })
  //
  //   console.log(
  //     staffSql + ";\n\n" +
  //     shopStaffSql + ";\n\n" +
  //     staffRoleSql + ";\n\n" +
  //     staffRoleTranslationsSql + ";\n\n" +
  //     staffRolePermissionsSql + ";")
  // })
})
