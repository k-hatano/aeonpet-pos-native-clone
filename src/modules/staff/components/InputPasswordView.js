import React from 'react'
import { TextInput, View, Text } from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/SelectStaffView'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import AlertView from '../../../common/components/widgets/AlertView'
import { registerBarcodeReaderEvent, showDebugBarcodeInput } from '../../barcodeReader/services'

export default class InputPasswordView extends React.Component {
  static propTypes = {
    staffs: React.PropTypes.array,
    onFind: React.PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      searchText: ''
    }
  }

  render () {
    return (
      <View style={styles.layoutRoot}>
        <View style={styles.searchFormArea}>
          <View style={styles.searchInputWrapper} >
            <TextInput
              value={this.state.searchText}
              onChangeText={searchText => this.setState({ searchText })}
              placeholder={I18n.t('staff.password')}
              style={styles.searchInput}
              placeholderTextColor={'#d8d8d8'}
              secureTextEntry />
          </View>
          <CommandButton
            onPress={() => this.props.onFind(this.state.searchText.trim())}
            text={I18n.t('common.confirm')}
            style={styles.searchButton}
            disabled={this.state.searchText.trim() === ''}
          />
        </View>
        <Text style={styles.messageText}>
          {I18n.t('staff.input_password')}
        </Text>
      </View>
    )
  }
}
