import React from 'react'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/SelectStaffViewModal'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import InputPasswordView from './InputPasswordView'

export default class InputPasswordViewModal extends React.Component {
  static propTypes = {
    staffs: PropTypes.array,
    onInit: PropTypes.func,
    onFind: PropTypes.func,
    onClose: PropTypes.func,
    permissionCode: PropTypes.string,
    staff: PropTypes.object
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  render () {
    return (
      <ModalFrame style={styles.layoutRoot} title={I18n.t('staff.staff_selector_title')} onClose={() => this.props.onClose()} >
        <InputPasswordView
          onFind={async (searchText) => {
            const staff = await this.props.onFind(this.props.staff, searchText, this.props.permissionCode)
            if (staff) {
              this.props.onClose(staff)
            }
          }}
          staffs={this.props.staffs} />
      </ModalFrame>
    )
  }
}
