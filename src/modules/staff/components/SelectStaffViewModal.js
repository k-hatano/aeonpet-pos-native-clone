import React from 'react'
import { Text, View, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/SelectStaffViewModal'
import SelectStaffView from './SelectStaffView'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import { showInputPasswordViewModal } from '../containers/InputPasswordViewModal'
import PromotionRepository from '../../../modules/promotion/repositories/PromotionRepository'

const maxHeight = 300

export default class SelectStaffViewModal extends React.Component {
  static propTypes = {
    staffs: PropTypes.array,
    promotionMessages: PropTypes.array,
    onInit: PropTypes.func,
    onFind: PropTypes.func,
    onClose: PropTypes.func,
    permissionCode: PropTypes.string
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  _calculateLineHeight (message) {
    let lineCount = 0
    const charaterHeight = 40
    message.split('\n').forEach(line => {
      lineCount = lineCount + 1.5
    })
    return Math.ceil(charaterHeight * lineCount)
  }

  _calculateModalHeight (messageHeight) {
    const baseHeight = 232
    return (messageHeight > maxHeight ? maxHeight : messageHeight) + baseHeight
  }

  render () {
    let messages = this.props.promotionMessages
    console.log(messages)
    const isCustomerThanksDay = messages.length !== 0
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    let modalHeight = 0
    messages.forEach(message => {
      modalHeight += this._calculateLineHeight(message)
    })

    return (
      <ModalFrame style={[styles.layoutRoot, {height: this._calculateModalHeight(modalHeight)}]} title={I18n.t('staff.staff_selector_title')} onClose={() => this.props.onClose()} >
        <SelectStaffView
          onFind={async (searchText, isReader) => {
            const staff = await this.props.onFind(searchText, isReader, this.props.permissionCode)
            if (staff) {
              if (isReader) {
                this.props.onClose(staff)
              } else {
                await showInputPasswordViewModal(staff, this.props.permissionCode, (staff) => {
                  this.props.onClose(staff)
                })
              }
            }
          }}
          staffs={this.props.staffs} />
        {(() => {
          if (isCustomerThanksDay) {
            return (
              <View style={styles.messageArea}>
                <Text style={styles.titleText}>{I18n.t('staff.promotion_title')}</Text>
                <ListView
                  dataSource={dataSource.cloneWithRows(messages)}
                  renderRow={data => <Text style={styles.messageText}>{data}</Text>}
                />
              </View>
            )
          }
        })()}
      </ModalFrame>
    )
  }
}
