import SampleZipAddressRepository from './sample/SampleZipAddressRepository'
import StandardZipAddressRepository from './standard/StandardZipAddressRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class ZipAddressRepository {
  static _implement = new SampleZipAddressRepository()

  static async fetchByZipCode (zipCode) {
    return this._implement.fetchByZipCode(zipCode)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardZipAddressRepository()
        break
      default:
        this._implement = new SampleZipAddressRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('ZipAddressRepository', context => {
  ZipAddressRepository.switchImplement(context)
})
