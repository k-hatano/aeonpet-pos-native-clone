const sampleZipAddresses = [
  {id:'0000000000000001', zip_code: '8770211', city: '大分県', locality: '日田市前津江町赤石'},
  {id:'0000000000000002', zip_code: '6190201', city: '京都府', locality: '木津川市山城町綺田'},
  {id:'0000000000000003', zip_code: '0611279', city: '北海道', locality: '北広島市大曲並木'}
]

export default class SampleZipAddressRepository {
  async fetchByZipCode (zipCode) {
    return sampleZipAddresses.filter(address => address.zip_code === zipCode)
  }
}
