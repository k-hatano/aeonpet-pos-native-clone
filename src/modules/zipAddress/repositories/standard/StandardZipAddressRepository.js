import fetcher from 'common/models/fetcher'
import { handleAxiosError } from '../../../../common/errors'
import { encodeCriteria } from 'common/utils/searchUtils'

export default class StandardZipAddressRepository {
  async fetchByZipCode (zipCode) {
    try {
      const response = await fetcher.get('zip-addresses/', encodeCriteria({ zip_code: zipCode }))
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
