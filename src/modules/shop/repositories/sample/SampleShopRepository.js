import { sampleShops } from '../../samples'

export default class SampleShopRepository {
  _localShop = null
  _serverShops = sampleShops.map(shop => ({ ...shop }))

  async find () {
    return this._localShop || { ...this._localShop }
  }

  async save (shop) {
    console.log('SampleShopRepository.save', shop)
    return this._shops.concat(shop)
  }

  static async clearAll () {
    console.log('SampleShopRepository.clearAll')
    this._localShop = null
  }

  async fetchAll (withShopAddress = false) {
    console.log('SampleShopRepository.fetchAll')
    return this._serverShops
  }

  async fetchAllavailableShop () {
    console.log('SampleShopRepository.fetchAllavailableShop')
    return this._serverShops
  }

  async syncRemoteToLocalById (shopId) {
    console.log('SampleShopRepository.syncRemoteToLocalById', {shopId})
    const shop = this._serverShops.find(shop => shop.id === shopId)
    this._localShop = shop
  }

  async fetchById (shopId) {
    return this._serverShops.find(shop => shop.id === shopId)
  }
}
