import SampleShopRepository from './sample/SampleShopRepository'
import StandardShopRepository from './standard/StandardShopRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class ShopRepository {
  static _implement = new SampleShopRepository()

  /**
   *
   * @return {Promise.<ShopEntity>}
   */
  static async find () {
    return this._implement.find()
  }

  static async clearAll () {
    return this._implement.clearAll()
  }

  static async fetchAll (withShopAddress = false) {
    return this._implement.fetchAll(withShopAddress)
  }

  static async fetchAllavailableShop () {
    return this._implement.fetchAllavailableShop()
  }

  static async syncRemoteToLocalById (shopId) {
    return this._implement.syncRemoteToLocalById(shopId)
  }

  /**
   * 引数に指定された店舗情報を保存する。
   * 現在の店舗情報の変更を意味するため、既存のデータは破棄する。
   * @param shop
   * @return {Promise.<void>}
   */
  static async saveCurrentShop (shop) {
    return this._implement.saveCurrentShop(shop)
  }

  static async fetchById (shopId) {
    return this._implement.fetchById(shopId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardShopRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardShopRepository()
        break

      default:
        this._implement = new SampleShopRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('ShopRepository', context => {
  ShopRepository.switchImplement(context)
})
