import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

/**
 * @typedef {Object} ShopEntity
 * @property {string} id
 * @property {string} name
 * @property {string} shop_code
 * @property {string} reserve_code
 * @property {integer} calc_type
 * @property {integer} tax_rule
 * @property {integer} tax_rate
 * @property {string} pos_close_time
 * @property {string} credit_user_id
 * @property {string} password
 * @property {string} region
 * @property {string} locality
 * @property {string} street
 * @property {integer} useable_base_points
 */

const Shop = sequelize.define(
  'Shop',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    shop_code: Sequelize.STRING,
    reserve_code: Sequelize.STRING,
    calc_type: Sequelize.INTEGER,
    tax_rule: Sequelize.INTEGER,
    tax_rate: Sequelize.DECIMAL(5, 4),
    pos_closed_time: Sequelize.TIME,
    credit_user_id: Sequelize.STRING,
    password: Sequelize.STRING,
    region: Sequelize.STRING,
    locality: Sequelize.STRING,
    street: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    is_taxfree: Sequelize.BOOLEAN,
    point_rate: Sequelize.DECIMAL(5, 4),
    grant_base_points_amount: {
      type: Sequelize.DECIMAL(19, 4),
      allowNull: false
    },
    grant_base_points: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    useable_base_points: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    store_opening_type: Sequelize.INTEGER,
    issuer_code: Sequelize.STRING,
    issuer_name: Sequelize.STRING
  },
  {
    tableName: 'shops',
    underscored: true,
    timestamps: false
  }
)

export default Shop
