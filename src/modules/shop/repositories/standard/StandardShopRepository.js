import AppEvents from 'common/AppEvents'
import ShopEntity from '../entities/ShopEntity'
import { sampleShops } from '../../samples'
import fetcher from 'common/models/fetcher'
import { handleAxiosError, handleDatabaseError } from '../../../../common/errors'
import { encodeCriteria } from '../../../../common/utils/searchUtils'

export default class StandardShopRepository {
  async find () {
    const shops = await ShopEntity.findAll({ raw: true })
    return shops ? shops[0] : null
  }

  async clearAll () {
    await ShopEntity.destroy({ truncate: true })
  }

  async fetchAll (withShopAddress = false) {
    const options = withShopAddress ? {with: 'shopAddress'} : {}
    try {
      const response = await fetcher.get('shops', options)
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchAllavailableShop () {
    try {
      const response = await fetcher.get('shops', encodeCriteria({ status: 1 }))
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async syncRemoteToLocalById (shopId) {
    let shop
    try {
      // axiosでAPIから店舗等の情報を取得
      const response = await fetcher.get('shops/' + shopId)
      shop = { ...response.data.shop_address, ...response.data }
    } catch (error) {
      handleAxiosError(error)
    }
    try {
      // shopのDBを一回破棄して、APIで取得したデータをshopに登録している
      await ShopEntity.destroy({ truncate: true })
      await ShopEntity.create(shop)
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async saveCurrentShop (shop) {
    try {
      await ShopEntity.destroy({ truncate: true })
      await ShopEntity.create(shop)
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async createSamples () {
    const existingCount = await ShopEntity.count()
    console.log(`Create Shops ----- exitstCount ${existingCount}`)
    if (existingCount === 0) {
      console.log('Create Shops')
      await ShopEntity.bulkCreate(sampleShops)
    }
  }

  async fetchById (shopId) {
    try {
      const response = await fetcher.get('shops/' + shopId)
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}

AppEvents.onSampleDataCreate('StandardShopRepository', () => {
  return new StandardShopRepository().createSamples()
})
