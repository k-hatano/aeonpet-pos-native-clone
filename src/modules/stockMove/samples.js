import generateUuid from 'common/utils/generateUuid'

const stockMovementId1 = generateUuid()
const stockMovementId2 = generateUuid()
const stockMovementId3 = generateUuid()
const stockMovementId4 = generateUuid()

const date = new Date()
const dateTime = Math.floor(date.getTime() / 1000)

const sampleStockMovementItems = [
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId1,
    product_id: generateUuid(),
    product_name: '商品A',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格A',
    product_code: '000000000001',
    sku: '000000000001',
    quantity: 10,
    extra_data: null,
    created_at: dateTime,
    updated_at: dateTime
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId2,
    product_id: generateUuid(),
    product_name: '商品B',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格B',
    product_code: '000000000002',
    sku: '000000000002',
    quantity: 20,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60,
    updated_at: dateTime - 24 * 60 * 60
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId2,
    product_id: generateUuid(),
    product_name: '商品C',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格C',
    product_code: '000000000003',
    sku: '000000000003',
    quantity: 30,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60,
    updated_at: dateTime - 24 * 60 * 60
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId3,
    product_id: generateUuid(),
    product_name: '商品D',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格D',
    product_code: '000000000004',
    sku: '000000000004',
    quantity: 40,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 2,
    updated_at: dateTime - 24 * 60 * 60 * 2
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId3,
    product_id: generateUuid(),
    product_name: '商品E',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格E',
    product_code: '000000000005',
    sku: '000000000005',
    quantity: 50,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 2,
    updated_at: dateTime - 24 * 60 * 60 * 2
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId3,
    product_id: generateUuid(),
    product_name: '商品F',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格F',
    product_code: '000000000006',
    sku: '000000000006',
    quantity: 60,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 2,
    updated_at: dateTime - 24 * 60 * 60 * 2
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId4,
    product_id: generateUuid(),
    product_name: '商品G',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格G',
    product_code: '000000000007',
    sku: '000000000007',
    quantity: 70,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 3,
    updated_at: dateTime - 24 * 60 * 60 * 3
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId4,
    product_id: generateUuid(),
    product_name: '商品H',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格H',
    product_code: '000000000008',
    sku: '000000000008',
    quantity: 80,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 3,
    updated_at: dateTime - 24 * 60 * 60 * 3
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId4,
    product_id: generateUuid(),
    product_name: '商品I',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格I',
    product_code: '000000000009',
    sku: '000000000009',
    quantity: 90,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 3,
    updated_at: dateTime - 24 * 60 * 60 * 3
  },
  {
    id: generateUuid(),
    stock_movement_id: stockMovementId4,
    product_id: generateUuid(),
    product_name: '商品J',
    product_variant_id: generateUuid(),
    product_variant_name: '商品規格J',
    product_code: '000000000010',
    sku: '000000000010',
    quantity: 100,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 3,
    updated_at: dateTime - 24 * 60 * 60 * 3
  }
]

export const sampleStockMovements = [
  {
    id: stockMovementId1,
    stock_transction_id: generateUuid(),
    outstock_warehouse_id: generateUuid(),
    outstock_warehouse_name: '出庫倉庫A',
    outstock_staff_id: generateUuid(),
    outstock_staff_name: '出庫担当者A',
    outstocked_at: dateTime,
    instock_warehouse_id: generateUuid(),
    instock_warehouse_name: '入庫倉庫A',
    instock_staff_id: generateUuid(),
    instock_staff_name: '入庫担当者A',
    instocked_at: dateTime,
    slip_number: '00000000001',
    status: 1,
    scheduled_instock_at: dateTime,
    extra_data: null,
    created_at: dateTime,
    updated_at: dateTime,
    canceled_at: null,
    stock_movement_items: sampleStockMovementItems.filter(item => item.stock_movement_id === stockMovementId1)
  },
  {
    id: stockMovementId2,
    stock_transction_id: generateUuid(),
    outstock_warehouse_id: generateUuid(),
    outstock_warehouse_name: '出庫倉庫B',
    outstock_staff_id: generateUuid(),
    outstock_staff_name: '出庫担当者B',
    outstocked_at: dateTime - 24 * 60 * 60,
    instock_warehouse_id: generateUuid(),
    instock_warehouse_name: '入庫倉庫B',
    instock_staff_id: generateUuid(),
    instock_staff_name: '入庫担当者B',
    instocked_at: dateTime - 24 * 60 * 60,
    slip_number: '00000000002',
    status: 2,
    scheduled_instock_at: dateTime - 24 * 60 * 60,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60,
    updated_at: dateTime - 24 * 60 * 60,
    canceled_at: null,
    stock_movement_items: sampleStockMovementItems.filter(item => item.stock_movement_id === stockMovementId2)
  },
  {
    id: stockMovementId3,
    stock_transction_id: generateUuid(),
    outstock_warehouse_id: generateUuid(),
    outstock_warehouse_name: '出庫倉庫C',
    outstock_staff_id: generateUuid(),
    outstock_staff_name: '出庫担当者C',
    outstocked_at: dateTime - 24 * 60 * 60 * 2,
    instock_warehouse_id: generateUuid(),
    instock_warehouse_name: '入庫倉庫C',
    instock_staff_id: generateUuid(),
    instock_staff_name: '入庫担当者C',
    instocked_at: dateTime - 24 * 60 * 60 * 2,
    slip_number: '00000000003',
    status: 3,
    scheduled_instock_at: dateTime - 24 * 60 * 60 * 2,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 2,
    updated_at: dateTime - 24 * 60 * 60 * 2,
    canceled_at: null,
    stock_movement_items: sampleStockMovementItems.filter(item => item.stock_movement_id === stockMovementId3)
  },
  {
    id: stockMovementId4,
    stock_transction_id: generateUuid(),
    outstock_warehouse_id: generateUuid(),
    outstock_warehouse_name: '出庫倉庫D',
    outstock_staff_id: generateUuid(),
    outstock_staff_name: '出庫担当者D',
    outstocked_at: dateTime - 24 * 60 * 60 * 3,
    instock_warehouse_id: generateUuid(),
    instock_warehouse_name: '入庫倉庫D',
    instock_staff_id: generateUuid(),
    instock_staff_name: '入庫担当者D',
    instocked_at: dateTime - 24 * 60 * 60 * 3,
    slip_number: '00000000004',
    status: 9,
    scheduled_instock_at: dateTime - 24 * 60 * 60 * 3,
    extra_data: null,
    created_at: dateTime - 24 * 60 * 60 * 3,
    updated_at: dateTime - 24 * 60 * 60 * 3,
    canceled_at: null,
    stock_movement_items: sampleStockMovementItems.filter(item => item.stock_movement_id === stockMovementId4)
  }
]
