import I18n from 'i18n-js'
import * as _ from 'underscore'
import * as actions from './actions'
import { getSettingFromState } from '../../modules/setting/models'
import SettingKeys from '../../modules/setting/models/SettingKeys'
import WarehouseRepository from '../../modules/stock/repositories/WarehouseRepository'
import AlertView from '../../common/components/widgets/AlertView'
import { isHalfWidthAndSymbol } from '../../common/utils/validations'

export const MODULE_NAME = 'stockMove'
export const MAX_ITEM_QUANTITY = 99999
/**
 * 出庫画面の入庫予定日の初期値として設定する日付の現在日からのオフセット日数
 * @type {number}
 */
export const DEFAULT_SCHEDULED_INSTOCK_DAY_OFFSET = 2

/**
 * 在庫移動のステータスを表します。
 * @type {{OUT_STOCKED: number, IN_STOCKED: number, CANCELED: number}}
 */
export const STOCK_MOVEMENT_STATUS = {
  OUT_STOCKED: 1,
  IN_STOCKED: 2,
  CANCELED: 9
}

/**
 * 入出庫画面の検索モードを表します。
 * @type {{OUT_STOCK: number, IN_STOCK: number}}
 */
export const STOCK_MOVEMENT_SEARCH_MODE = {
  OUT_STOCK: 1,
  IN_STOCK: 2
}

/**
 * 在庫移動のステータスを日本語表記に変換します。
 * @param {Number} status
 * @param {bool} useOutStock 出庫画面から利用されているかどうか
 * @returns {*}
 */
export const statusToLabel = (status, useOutStock = false) => {
  switch (status) {
    case STOCK_MOVEMENT_STATUS.OUT_STOCKED:
      return useOutStock ? '' : I18n.t('stock_move.out_stocked')
    case STOCK_MOVEMENT_STATUS.IN_STOCKED:
      return I18n.t('stock_move.in_stocked')
    case STOCK_MOVEMENT_STATUS.CANCELED:
      return I18n.t('stock_move.cancel')
  }
  return null
}

/**
 * 出庫確定が可能かどうかを取得します。
 * @param createInputs
 * @returns {boolean}
 */
export const canCompleteStockMovement = createInputs => {
  const {instockWarehouse, scheduledInstockAt, products, slipNumber, autoSlipNumber} = createInputs
  // 伝票番号は入力されていなくても、自動採番にチェックが入っていればオッケー
  return instockWarehouse && instockWarehouse.warehouse_code &&
    scheduledInstockAt && products.length > 0 && (slipNumber || autoSlipNumber)
}

/**
 * 入出庫伝票検索が可能かどうかを取得します。
 * @param searchInputs
 */
export const canSearchStockMovement = searchInputs => {
  const searchStatus = getSearchStatusValue(searchInputs.status)

  return searchStatus.length > 0
}

/**
 * 出庫情報画面を初期化します。
 * @param dispatch
 * @returns {Promise<void>}
 */
export const initForStockMovement = async (dispatch) => {
  dispatch(actions.setSelectedTab(0))

  const warehouses = await WarehouseRepository.findAll()
  dispatch(actions.resetCreateInputs())
  dispatch(actions.listWarehouses(warehouses))
}

/**
 * State情報から設定情報を取得します。
 * @param state
 * @returns {{shopId: *, deviceId: *, currentStaff: *}}
 */
export const getSettingsFromState = state => {
  return {
    shopId: getSettingFromState(state, SettingKeys.COMMON.SHOP_ID),
    deviceId: getSettingFromState(state, SettingKeys.COMMON.DEVICE_ID),
    currentStaff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)
  }
}

/**
 * 数量が変更された明細情報をサーバーから取得した情報に反映します。
 * @param stockMovement
 * @param {Array} stockMovementItems 数量が入力されたアイテム
 */
export const applyModifyQuantities = (stockMovement, stockMovementItems) => {
  const idToItem = _.indexBy(stockMovementItems, 'id')
  const newItems = stockMovement.stock_movement_items.map(item => {
    const newItem = idToItem[item.id]
    return {
      ...item,
      quantity: newItem.quantity
    }
  })
  return {
    ...stockMovement,
    stock_movement_items: newItems
  }
}

/**
 * 出庫時の入力情報を検証します。
 * @param createInputs
 * @returns {boolean}
 */
export const validateCreateInputs = async createInputs => {
  const {slipNumber, autoSlipNumber} = createInputs
  // 伝票番号にはいろいろ制限があるのよ。
  if (!autoSlipNumber) {
    const isValid = isHalfWidthAndSymbol(slipNumber) && slipNumber.indexOf('STM') === -1

    if (!isValid) {
      await AlertView.showAsync(I18n.t('message.N-02-E001'))

      return false
    }
  }
  return true
}

/**
 * サーバー送信用の出庫情報を生成します。
 * @param createInputs
 * @param currentSettings
 * @returns {Promise<{}>}
 */
export const makeStockMovementForPush = async (createInputs, currentSettings) => {
  const {products, instockWarehouse} = createInputs
  const {shopId, deviceId, currentStaff} = currentSettings
  const outstockWarehouse = await WarehouseRepository.fetchByShopId(shopId)
  const stockMovementItems = products.map((item, index) => ({
    product_id: item.id,
    product_name: item.name,
    product_variant_id: item.product_variants[0].id,
    product_variant_name: '',
    product_code: item.product_code,
    sku: item.product_variants[0].sku,
    article_number: item.product_variants[0].article_number,
    quantity: item.quantity,
    row_number: index + 1,
    price: item.product_variants[0].price,
    cost_price: item.product_variants[0].cost_price
  }))
  return {
    stock_movement_items: stockMovementItems,
    outstock_warehouse_id: outstockWarehouse[0].id,
    outstock_warehouse_name: outstockWarehouse[0].name,
    outstock_staff_id: currentStaff.id,
    outstock_staff_name: currentStaff.name,
    outstocked_at: null,
    instock_warehouse_id: instockWarehouse.id,
    instock_warehouse_name: instockWarehouse.name,
    instock_staff_id: null,
    instock_staff_name: null,
    instocked_at: null,
    shop_id: shopId,
    device_id: deviceId,
    slip_number: createInputs.slipNumber,
    auto_slip_number: createInputs.autoSlipNumber,
    status: STOCK_MOVEMENT_STATUS.OUT_STOCKED,
    scheduled_instock_at: createInputs.scheduledInstockAt
  }
}

/**
 * 検索ステータスからチェックがついている値を配列で取得します。
 * @param searchStatus
 */
export const getSearchStatusValue = (searchStatus) => {
  return Object.keys(searchStatus).filter(key => searchStatus[key])
}
