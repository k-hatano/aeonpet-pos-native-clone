import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { loading } from '../../common/sideEffects'
import { formatDate } from '../../common/utils/dateTime'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import AlertView from '../../common/components/widgets/AlertView'
import ConfirmView from '../../common/components/widgets/ConfirmView'
import * as actions from './actions'
import {
  makeStockMovementForPush, getSearchStatusValue, STOCK_MOVEMENT_STATUS, STOCK_MOVEMENT_SEARCH_MODE
} from './models'
import StockItemsRepository from '../../modules/stock/repositories/StockItemsRepository'
import StockMovementRepository from './repositories/StockMovementRepository'

/**
 * 入出庫画面に入力された条件を元に検索の実行と画面表示を行います。
 * @param dispatch
 * @param searchInputs
 * @returns {Promise<void>}
 */
export const searchStockMovements = async (dispatch, searchInputs) => {
  let params = {}

  if (searchInputs.instockWarehouse) {
    params['instock_warehouse_id'] = searchInputs.instockWarehouse.id
  }
  if (searchInputs.outstockWarehouse) {
    params['outstock_warehouse_id'] = searchInputs.outstockWarehouse.id
  }
  // 伝票番号が指定されている場合、他の条件は無視する。
  if (searchInputs.slipNumber) {
    params.slip_number = searchInputs.slipNumber

    if (searchInputs.mode === STOCK_MOVEMENT_SEARCH_MODE.IN_STOCK) {
      delete params['outstock_warehouse_id']
    } else {
      delete params['instock_warehouse_id']
    }
  } else {
    params = {
      ...params,
      status: getSearchStatusValue(searchInputs.status),
      productSearch: searchInputs.word
    }
    const {dateFrom, dateTo} = searchInputs
    if (dateFrom > dateTo) {
      params['scheduled_instock_at__gte'] = formatDate(dateTo)
      params['scheduled_instock_at__lte'] = formatDate(dateFrom)
      // ひっくり返す
      dispatch(actions.setSearchInputValue({propName: 'dateFrom', value: dateTo}))
      dispatch(actions.setSearchInputValue({propName: 'dateTo', value: dateFrom}))
    } else {
      params['scheduled_instock_at__gte'] = formatDate(dateFrom)
      params['scheduled_instock_at__lte'] = formatDate(dateTo)
    }
  }
  await loading(dispatch, async () => {
    try {
      const stockMovements = await StockMovementRepository.fetchByCondition(params)
      // 入庫と出庫でメッセージ変えるんやってさ
      if (stockMovements.length === 0) {
        AlertView.show(I18n.t(
          searchInputs.mode === STOCK_MOVEMENT_SEARCH_MODE.IN_STOCK ? 'message.N-01-E001' : 'message.N-03-E001'
        ))
      }
      dispatch(actions.setSelectedStockMovement(null))
      dispatch(actions.setStockMovements(stockMovements))
    } catch (error) {
      AlertView.show(getCommonNetworkErrorMessage(error))
    }
  })
}

/**
 * 出庫画面に入力された出庫情報をサーバーに送信します。
 * @param dispatch
 * @param createInputs
 * @param currentSettings
 * @returns {Promise<void>}
 */
export const pushStockMovement = async (dispatch, createInputs, currentSettings) => {
  ConfirmView.show(I18n.t('message.L-02-I001'), async () => {
    const result = await loading(dispatch, async () => {
      try {
        const stockMovement = await makeStockMovementForPush(createInputs, currentSettings)

        await StockMovementRepository.pushStockMovement(stockMovement)
        return {isSuccess: true}
      } catch (error) {
        return {
          isSuccess: false,
          message: getCommonNetworkErrorMessage(error)
        }
      }
    })
    if (!result.isSuccess) {
      AlertView.show(result.message)
    } else {
      await AlertView.showAsync(I18n.t('message.L-02-I002'))
      dispatch(actions.resetCreateInputs())
      // 完了メッセージ後、画面遷移して再検索
      Actions.stockMoveOutPage()
    }
  })
}

/**
 * 入庫情報をサーバーに送信します。
 * @param dispatch
 * @param stockMovement
 * @param searchInputs
 * @param currentSettings
 * @returns {Promise<void>}
 */
export const updateStockMovement = async (dispatch, stockMovement, searchInputs, currentSettings) => {
  const {currentStaff} = currentSettings

  ConfirmView.show(I18n.t('message.N-04-I002'), async () => {
    const result = await loading(dispatch, async () => {
      try {
        await StockMovementRepository.updateStockMovement(stockMovement.id, {
          ...stockMovement,
          status: STOCK_MOVEMENT_STATUS.IN_STOCKED,
          instock_staff_id: currentStaff.id,
          instock_staff_name: currentStaff.name
        })
        // ローカルの在庫を更新
        await syncStockItems(dispatch, stockMovement.stock_movement_items)
        return {isSuccess: true}
      } catch (error) {
        return {
          isSuccess: false,
          message: getCommonNetworkErrorMessage(error)
        }
      }
    })
    if (!result.isSuccess) {
      AlertView.show(result.message)
    } else {
      await AlertView.showAsync(I18n.t('message.N-04-I003'))
      dispatch(actions.setSelectedStockMovement(null))
      dispatch(actions.setStockMovements([]))
      // 再検索
      await searchStockMovements(dispatch, searchInputs)
    }
  })
}

/**
 * 出庫情報のキャンセルをサーバーに送信します。
 * @param dispatch
 * @param stockMovement
 * @param searchInputs
 * @param currentSettings
 * @returns {Promise<void>}
 */
export const cancelStockMovement = async (dispatch, stockMovement, searchInputs, currentSettings) => {
  const {currentStaff, deviceId} = currentSettings

  ConfirmView.show(I18n.t('message.N-04-I001'), async () => {
    const result = await loading(dispatch, async () => {
      try {
        await StockMovementRepository.cancelStockMovement(stockMovement.id, {
          ...stockMovement,
          status: STOCK_MOVEMENT_STATUS.CANCELED,
          canceled_device_id: deviceId,
          canceled_staff_id: currentStaff.id,
          canceled_staff_name: currentStaff.name
        })
        return {isSuccess: true}
      } catch (error) {
        return {
          isSuccess: false,
          message: getCommonNetworkErrorMessage(error)
        }
      }
    })
    if (!result.isSuccess) {
      AlertView.show(result.message)
    } else {
      await AlertView.showAsync(I18n.t('message.N-04-I004'))
      dispatch(actions.setSelectedStockMovement(null))
      dispatch(actions.setStockMovements([]))
      // 再検索
      await searchStockMovements(dispatch, searchInputs)
    }
  })
}

/**
 * 入庫した商品の在庫情報をサーバーと同期します。
 * @param dispatch
 * @param stockMovementItems
 * @returns {Promise<void>}
 */
export const syncStockItems = async (dispatch, stockMovementItems) => {
  const vids = stockMovementItems.map(item => {
    return item.product_variant_id
  })
  const stockItems = await StockItemsRepository.fetchByProductVariantIds(vids)
  await StockItemsRepository.bulkSave(stockItems)
}
