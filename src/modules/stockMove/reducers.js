import * as actions from './actions'
import { handleActions } from 'redux-actions'
import { getToday, getEndOfToday, addDays } from '../../common/utils/dateTime'
import { DEFAULT_SCHEDULED_INSTOCK_DAY_OFFSET } from './models'

const defaultState = {
  warehouses: [],
  currentWarehouse: {},
  stockMovements: [],
  stockMovementItems: null,
  selectedStockMovement: null,
  createInputs: {
    products: [],
    instockWarehouse: null,
    slipNumber: null,
    autoSlipNumber: false,
    scheduledInstockAt: ''
  },
  searchInputs: {
    mode: null,
    status: [],
    slipNumber: null,
    instockWarehouse: null,
    outstockWarehouse: null,
    word: null,
    dateFrom: new Date(),
    dateTo: new Date()
  },
  selectedTab: 0
}

const handlers = {
  [actions.setWarehouses]: (state, action) => ({
    ...state,
    warehouses: action.payload
  }),
  [actions.setCurrentWarehouse]: (state, action) => ({
    ...state,
    currentWarehouse: action.payload
  }),
  [actions.listWarehouses]: (state, action) => ({
    ...state,
    warehouses: action.payload
  }),
  [actions.setStockMovements]: (state, action) => ({
    ...state,
    stockMovements: action.payload
  }),
  [actions.setStockMovementItems]: (state, action) => ({
    ...state,
    stockMovementItems: action.payload
  }),
  [actions.setSelectedStockMovement]: (state, action) => ({
    ...state,
    selectedStockMovement: action.payload
  }),
  [actions.setCreateInputValue]: (state, action) => {
    const {value, propName} = action.payload
    const createInputs = {
      ...state.createInputs
    }
    createInputs[propName] = value
    return {
      ...state,
      createInputs: createInputs
    }
  },
  [actions.resetCreateInputs]: (state, action) => ({
    ...state,
    createInputs: {
      ...defaultState.createInputs,
      scheduledInstockAt: addDays(getToday(), DEFAULT_SCHEDULED_INSTOCK_DAY_OFFSET, 'YYYY-MM-DD')
    }
  }),
  [actions.setSearchInputValue]: (state, action) => {
    const {value, propName} = action.payload
    const searchInputs = {
      ...state.searchInputs
    }
    searchInputs[propName] = value
    return {
      ...state,
      searchInputs: searchInputs
    }
  },
  [actions.setSearchStatus]: (state, action) => {
    const {status} = state.searchInputs
    const {value, checked} = action.payload
    return {
      ...state,
      searchInputs: {
        ...state.searchInputs,
        status: {
          ...status,
          [value]: checked
        }
      }
    }
  },
  [actions.resetSearchInputs]: (state, action) => {
    const {defaultStatus} = action.payload
    return {
      ...state,
      searchInputs: {
        ...defaultState.searchInputs,
        dateFrom: getToday(),
        dateTo: getEndOfToday(),
        status: defaultStatus !== undefined ? defaultStatus : defaultState.searchInputs.status
      }
    }
  },
  [actions.setSelectedTab]: (state, action) => ({
    ...state,
    selectedTab: action.payload
  })
}

export default handleActions(handlers, defaultState)
