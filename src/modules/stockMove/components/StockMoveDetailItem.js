import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import baseStyle from '../../../common/styles/baseStyle'
import styles from '../styles/StockMoveDetail'
import { formatNumber, formatPriceWithCurrency } from '../../../common/utils/formats'
import { STOCK_MOVEMENT_STATUS, MAX_ITEM_QUANTITY } from '../models'
import CartInputNumberForm from '../../../modules/cart/components/CartInputNumberForm'

export default class StockMoveDetailItem extends Component {
  static propTypes = {
    itemKey: PropTypes.number,
    onChangeQuantity: PropTypes.func,
    status: PropTypes.number,
    stockMovementItem: PropTypes.object,
    stockMovementStatus: PropTypes.number
  }

  _onIncrease (itemKey, stockMovementItem) {
    const newValue = stockMovementItem.quantity + 1
    // 入力数が上限値を超えている場合は増加させない。
    if (newValue > MAX_ITEM_QUANTITY) return

    const stockItem = stockMovementItem.stock_item
    const stockQuantity = stockItem ? stockItem.quantity : 0
    if (Math.abs(stockQuantity - newValue) > MAX_ITEM_QUANTITY) return false
    this.props.onChangeQuantity(itemKey, newValue)
    return true
  }

  _onDecrease (itemKey, stockMovementItem) {
    const newValue = stockMovementItem.quantity - 1
    if (newValue < 0) return false
    this.props.onChangeQuantity(itemKey, newValue)
    return true
  }

  _renderQuantity (stockMovementItem) {
    const {
      itemKey,
      status,
      stockMovementStatus
    } = this.props

    if (status === STOCK_MOVEMENT_STATUS.IN_STOCKED && stockMovementStatus === STOCK_MOVEMENT_STATUS.OUT_STOCKED) {
      return (
        <View style={styles.operationArea}>
          <CartInputNumberForm
            number={stockMovementItem.quantity}
            onIncrease={() => this._onIncrease(itemKey, stockMovementItem)}
            onDecrease={() => this._onDecrease(itemKey, stockMovementItem)}
            maxNumber={MAX_ITEM_QUANTITY}
            onChange={(quantity) => this.props.onChangeQuantity(itemKey, quantity)}
            canIncreaseDecrease />
        </View>
      )
    } else {
      return (
        <View style={styles.operationArea}>
          <Text style={baseStyle.commonText}>
            {I18n.t('common.amount_of_point', {
              amount: formatNumber(stockMovementItem.quantity)
            })}
          </Text>
        </View>
      )
    }
  }

  render () {
    const {stockMovementItem} = this.props
    return (
      <View>
        <View style={styles.headerArea}>
          <View style={styles.productLabelArea}>
            <Text
              numberOfLines={1}
              ellipsizeMode={'middle'}
              style={baseStyle.subText}>
              {stockMovementItem.product_code}
            </Text>
          </View>
        </View>
        <View style={styles.mainArea}>
          <View style={styles.productSubInfoArea}>
            <Text numberOfLines={1} style={baseStyle.mainText}>
              {stockMovementItem.product_name}
            </Text>
          </View>
        </View>
        <View style={styles.subArea}>
          <View style={styles.productSubInfoArea}>
            <Text style={[baseStyle.subText, styles.unitPriceText]}>
              {I18n.t('cart.unit_price')} : {formatPriceWithCurrency(stockMovementItem.price, 'jpy')}
            </Text>
          </View>
          {this._renderQuantity(stockMovementItem)}
        </View>
      </View>
    )
  }
}
