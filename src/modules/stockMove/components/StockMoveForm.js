import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/StockMoveForm'
import Select from '../../../common/components/elements/Select'
import CheckBox from '../../../common/components/elements/CheckBox'
import { formatDate, getStartOfDay } from '../../../common/utils/dateTime'
import DateTimePicker from 'react-native-modal-datetime-picker'
import dateTimePickerStyles from '../../../common/styles/elements/DateTimePicker'

class RowLayout extends Component {
  render () {
    const { title, style, children } = this.props
    return (
      <View
        style={[componentStyles.sampleRowContainer, style && style]}>
        <View style={componentStyles.sampleRowTitle}>
          {title && (
            <Text style={componentStyles.commonText}>{title} : </Text>
          )}
        </View>
        <View style={componentStyles.sampleRowTextContainer}>
          {children}
        </View>
      </View>
    )
  }
}

export default class StockMoveForm extends Component {
  static propTypes = {
    warehouses: PropTypes.array,
    createInputs: PropTypes.object,
    onSetCreateInputValue: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  _showDateTimePicker (title, date, key) {
    this.setState({
      isDatePickerVisible: true,
      date: date ? new Date(date) : new Date(),
      datePickerTitle: title,
      dateKey: key
    })
  }

  _hideDateTimePicker = () => this.setState({ isDatePickerVisible: false })

  _handleDatePicked (date) {
    this.setState({ isDatePickerVisible: false })
    this.props.onSetCreateInputValue(date, 'scheduledInstockAt')
  }

  render () {
    const {slipNumber, autoSlipNumber, instockWarehouse, scheduledInstockAt} = this.props.createInputs
    const {onSetCreateInputValue} = this.props
    return (
      <View style={{ flex: 3, marginTop: 5 }}>
        <ScrollView>
          {
            <View style={componentStyles.innerContainer}>
              <View style={[componentStyles.formSearchLayout]}>
                <RowLayout title={I18n.t('stock_move.slip_number')}>
                  <TextInput
                    style={[componentStyles.searchInput, autoSlipNumber ? componentStyles.searchInputDisabled : null]}
                    onChangeText={value => onSetCreateInputValue(value, 'slipNumber')}
                    value={slipNumber}
                    placeholder={I18n.t('stock_move.slip_number')}
                    maxLength={8}
                    editable={!autoSlipNumber} />
                </RowLayout>
                <RowLayout style={componentStyles.compactRowContainer}>
                  <CheckBox
                    label={I18n.t('stock_move.auto_slip_number')}
                    labelStyle={componentStyles.checkboxOptionText}
                    onChange={value => onSetCreateInputValue(value, 'autoSlipNumber')}
                    mutableChecked={autoSlipNumber} />
                </RowLayout>
                <RowLayout title={I18n.t('stock_move.instock_warehouse_name')}>
                  <Select
                    style={componentStyles.selectInput}
                    items={this.props.warehouses}
                    selectedItem={instockWarehouse}
                    displayProperty='name'
                    onItemChange={value => onSetCreateInputValue(value, 'instockWarehouse')}
                    placeholder={I18n.t('stock_search.not_selected')}
                  />
                </RowLayout>
                <RowLayout title={I18n.t('stock_move.scheduled_instock_at')}>
                  <View style={componentStyles.dateWrapper}>
                    <View style={componentStyles.inputContainer}>
                      <TextInput
                        ref='scheduledInstockAt'
                        style={[dateTimePickerStyles.datePickerInput]}
                        value={formatDate(scheduledInstockAt)} />
                      <TouchableOpacity
                        style={dateTimePickerStyles.datePickerTouchable}
                        onPress={() => {
                          this._showDateTimePicker(
                            I18n.t('stock_move.scheduled_instock_at'),
                            formatDate(scheduledInstockAt),
                            'dateTo')
                        }} />
                    </View>
                  </View>
                </RowLayout>
              </View>
            </View>
          }
        </ScrollView>
        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          mode='date'
          date={this.state.date}
          titleIOS={this.state.datePickerTitle}
          titleStyle={dateTimePickerStyles.datePickerTitle}
          confirmTextIOS={I18n.t('common.ok')}
          confirmTextStyle={dateTimePickerStyles.datePickerConfirm}
          cancelTextIOS={I18n.t('common.cancel')}
          cancelTextStyle={dateTimePickerStyles.datePickerCancel}
          minimumDate={new Date('1900/01/01')}
          onConfirm={(date) => { this._handleDatePicked(date) }}
          onCancel={this._hideDateTimePicker}
          neverDisableConfirmIOS />
      </View>
    )
  }
}
