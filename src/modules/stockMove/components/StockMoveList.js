import React, { Component } from 'react'
import { ListView, View } from 'react-native'
import styles from '../styles/StockMoveList'
import PropTypes from 'prop-types'
import StockMoveListItem from './StockMoveListItem'

export default class StockMoveList extends Component {
  static propTypes = {
    onInit: PropTypes.func,
    onSelect: PropTypes.func,
    status: PropTypes.number,
    stockMovements: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  render () {
    const source = this.dataSource.cloneWithRows(this.props.stockMovements)
    return (
      <View style={styles.container}>
        <ListView
          enableEmptySections
          dataSource={source}
          renderSeparator={(sectionId, rowId) => <View
            key={`${sectionId}-${rowId}`}
            style={styles.separator} />}
          renderRow={(item, index) => (
            <StockMoveListItem
              item={item}
              status={this.props.status}
              onSelectItem={(item) => this.props.onSelect(item)} />
          )} />
      </View>
    )
  }
}
