import React, { Component } from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import StockMoveDetailItem from './StockMoveDetailItem'
import styles from '../styles/StockMoveDetail'

export default class StockMoveDetail extends Component {
  static propTypes = {
    onChangeQuantity: PropTypes.func,
    status: PropTypes.number,
    stockMovementItems: PropTypes.array,
    selectedStockMovement: PropTypes.object
  }

  render () {
    const { stockMovementItems, status, selectedStockMovement } = this.props
    let dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const source = dataSource.cloneWithRows(stockMovementItems)
    return (
      <View style={styles.main}>
        <ListView
          enableEmptySections
          dataSource={source}
          style={styles.itemContainer}
          renderSeparator={(sectionId, rowId) => <View
            key={`${sectionId}-${rowId}`}
            style={styles.separator} />}
          renderRow={(item, index, key) => (
            <StockMoveDetailItem
              itemKey={Number(key)}
              onChangeQuantity={
                (itemKey, quantity) => this.props.onChangeQuantity(stockMovementItems, itemKey, quantity)
              }
              status={status}
              stockMovementItem={item}
              stockMovementStatus={selectedStockMovement.status} />
          )}
        />
      </View>
    )
  }
}
