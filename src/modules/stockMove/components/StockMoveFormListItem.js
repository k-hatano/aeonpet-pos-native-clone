'use strict'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import styles from '../styles/StockMoveFormListItem'
import baseStyle from '../../../common/styles/baseStyle'
import CartInputNumberForm from '../../../modules/cart/components/CartInputNumberForm'
import { MAX_ITEM_QUANTITY } from '../models'

export default class StockMoveFormListItem extends Component {
  static propTypes = {
    product: PropTypes.object,
    currency: PropTypes.string,
    onChangeQuantity: PropTypes.func
  }

  render () {
    const { product, onChangeQuantity, currency } = this.props
    const productVariant = product.product_variants[0]

    return (
      <View>
        <View style={styles.root} ref='layoutRoot'>
          <View style={styles.headerArea}>
            <View style={[styles.productLabelArea, { flex: 3 }]}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'middle'}
                style={baseStyle.subText}>
                {product.product_code}
              </Text>
            </View>
          </View>
          <View style={styles.mainArea}>
            <Text numberOfLines={1} style={[baseStyle.mainText, styles.productNameText]}>
              {product.name}
            </Text>
          </View>
          <View style={styles.subArea}>
            <View style={styles.productSubInfoArea}>
              <Text style={[baseStyle.subText, styles.unitPriceText]}>
                {I18n.t('cart.unit_price')} : {formatPriceWithCurrency(productVariant.price, currency)}
              </Text>
            </View>
            <View style={styles.operationArea}>
              <CartInputNumberForm
                onIncrease={() => onChangeQuantity(product.id, product.quantity + 1)}
                onDecrease={() => onChangeQuantity(product.id, product.quantity - 1)}
                onChange={number => onChangeQuantity(product.id, number)}
                number={product.quantity}
                maxNumber={MAX_ITEM_QUANTITY}
                canIncreaseDecrease
              />
            </View>
          </View>
          <View style={styles.footer} />
        </View>
      </View>
    )
  }
}
