import React, { Component } from 'react'
import { View, ScrollView, Text } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/StockMoveSlipInfo'
import { statusToLabel, STOCK_MOVEMENT_STATUS } from '../models'
import moment from 'moment'

export default class StockMoveSlipInfo extends Component {
  static propTypes = {
    status: PropTypes.number,
    selectedStockMovement: PropTypes.object
  }

  _formatDateTime (value) {
    return value === null ? '' : moment.unix(value).format('YYYY-MM-DD HH:mm:ss')
  }

  render () {
    const stockMovement = this.props.selectedStockMovement
    const useMoveInPage = this.props.status === STOCK_MOVEMENT_STATUS.IN_STOCKED
    const isInStocked = stockMovement.status === STOCK_MOVEMENT_STATUS.IN_STOCKED
    const isCanceled = stockMovement.status === STOCK_MOVEMENT_STATUS.CANCELED
    return (
      <ScrollView>
        <View style={styles.mainArea}>
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.slip_number')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.slip_number}
            </Text>
          </View>
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.outstocked_at')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {this._formatDateTime(stockMovement.outstocked_at)}
            </Text>
          </View>
          {useMoveInPage &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.outstock_warehouse_name')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.outstock_warehouse_name}
            </Text>
          </View>
          }
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.outstock_staff_name')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.outstock_staff_name}
            </Text>
          </View>
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.status')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {statusToLabel(stockMovement.status)}
            </Text>
          </View>
          {!useMoveInPage &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.instock_warehouse_name')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.instock_warehouse_name}
            </Text>
          </View>
          }
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.scheduled_instock_at')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.scheduled_instock_at}
            </Text>
          </View>
          {isInStocked &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.instocked_at')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {this._formatDateTime(stockMovement.instocked_at)}
            </Text>
          </View>
          }
          {useMoveInPage && isInStocked &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.instock_staff_name')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.instock_staff_name}
            </Text>
          </View>
          }
          {isCanceled &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.canceled_at')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {this._formatDateTime(stockMovement.canceled_at)}
            </Text>
          </View>
          }
          {useMoveInPage && isCanceled &&
          <View style={styles.detailViewContainer}>
            <Text style={styles.detailViewTextLeft}>
              {I18n.t('stock_move.canceled_staff_name')}
            </Text>
            <Text style={styles.detailViewTextRight}>
              {stockMovement.canceled_staff_name}
            </Text>
          </View>
          }
        </View>
      </ScrollView>
    )
  }
}
