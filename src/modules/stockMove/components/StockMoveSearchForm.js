import React, { Component } from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native'
import I18n from 'i18n-js'
import styles from '../styles/StockMoveSearchForm'
import PropTypes from 'prop-types'
import { STOCK_MOVEMENT_SEARCH_MODE, STOCK_MOVEMENT_STATUS } from '../models'
import Select from '../../../common/components/elements/Select'
import CheckBox from '../../../common/components/elements/CheckBox'
import { CommandButton, ProceedButton, OptionalCommandButton } from '../../../common/components/elements/StandardButton'
import { Actions } from 'react-native-router-flux/index'
import { formatDate } from '../../../common/utils/dateTime'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import DateTimePicker from 'react-native-modal-datetime-picker'
import dateTimePickerStyles from '../../../common/styles/elements/DateTimePicker'

function RowLayout (props) {
  return (
    <View style={[styles.sampleRowContainer, props.style && props.style]}>
      <View style={styles.sampleRowTitle}>
        {props.title && <Text style={styles.commonText}>{props.title} : </Text>}
      </View>
      <View style={styles.sampleRowTextContainer}>
        {props.children}
      </View>
    </View>
  )
}

export default class StockMoveSearchForm extends Component {
  static propTypes = {
    resetToDefault: PropTypes.bool,
    allowCreateNew: PropTypes.bool,
    searchMode: PropTypes.number,
    warehouses: PropTypes.array,
    onInit: PropTypes.func,
    onResetToDefault: PropTypes.func,
    onSearch: PropTypes.func,
    onSearchInputChanged: PropTypes.func,
    onSearchStatusChanged: PropTypes.func,
    canSearch: PropTypes.bool,
    isSearchProduct: PropTypes.bool
  }
  static defaultProps = {
    resetToDefault: false,
    isSearchProduct: true
  }

  constructor (props) {
    super(props)
    this.statusCheckBox = [
      { label: I18n.t('stock_move.out_stocked'), value: STOCK_MOVEMENT_STATUS.OUT_STOCKED },
      { label: I18n.t('stock_move.in_stocked'), value: STOCK_MOVEMENT_STATUS.IN_STOCKED },
      { label: I18n.t('stock_move.cancel'), value: STOCK_MOVEMENT_STATUS.CANCELED }
    ]
    this.state = {
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(body => {
      if (!this.props.isSearchProduct) return

      this.props.onSearchInputChanged(body.trim(), 'word')
    })
    await this.props.onInit(this.props)
    // 色んな所から遷移するけど、ほとんどの場合入力値は維持するので、
    // resetToDefault が指定された場合のみ初期化するようにする。
    if (this.props.resetToDefault) {
      await this.props.onResetToDefault(this.props)
    }
    await this.props.onSearch(this.props)
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  _showDateTimePicker (title, date, key) {
    this.setState({
      isDatePickerVisible: true,
      date: date ? new Date(date) : new Date(),
      datePickerTitle: title,
      dateKey: key
    })
  }

  _hideDateTimePicker = () => this.setState({ isDatePickerVisible: false })

  _handleDatePicked (date) {
    this.setState({ isDatePickerVisible: false })
    this.props.onSearchInputChanged(date, this.state.dateKey)
  }

  _renderNewButtonArea () {
    if (!this.props.allowCreateNew) return null
    return (
      <View style={styles.newButtonArea}>
        <OptionalCommandButton
          style={styles.newButtonStyle}
          text={I18n.t('stock_move.action_new')}
          onPress={() => { Actions.stockMove() }}
        />
      </View>
    )
  }

  _isChecked (value) {
    const {status} = this.props.searchInputs
    const checked = status[value]
    return checked !== undefined ? checked : false
  }
  /**
   * 処理区分のチェックボックスを描画します。
   * @returns {any[]}
   * @private
   */
  _renderStatus () {
    return this.statusCheckBox.map(item => {
      return (
        <CheckBox
          key={item.value}
          label={item.label}
          labelStyle={styles.commonText}
          onChange={(checked) => this.props.onSearchStatusChanged(item, checked)}
          mutableChecked={this._isChecked(item.value)} />
      )
    })
  }

  _renderCommonArea (isInstock) {
    const {searchInputs, onSearchInputChanged} = this.props
    return (
      <View>
        <RowLayout title={I18n.t('stock_move.slip_number')}>
          <TextInput
            style={styles.searchInput}
            onChangeText={(value) => onSearchInputChanged(value, 'slipNumber')}
            value={searchInputs.slipNumber}
            placeholder={I18n.t('stock_move.slip_number')}
            maxLength={8} />
        </RowLayout>
        {isInstock ? (
          <RowLayout title={I18n.t('stock_move.outstock_warehouse_name')}>
            <Select
              style={styles.selectorContainer}
              items={this.props.warehouses}
              selectedItem={searchInputs.outstockWarehouse}
              displayProperty='name'
              onItemChange={item => onSearchInputChanged(item, 'outstockWarehouse')}
              placeholder={I18n.t('common.select_one')} />
          </RowLayout>
        ) : (
          <RowLayout title={I18n.t('stock_move.instock_warehouse_name')}>
            <Select
              style={styles.selectorContainer}
              items={this.props.warehouses}
              selectedItem={searchInputs.instockWarehouse}
              displayProperty='name'
              onItemChange={item => onSearchInputChanged(item, 'instockWarehouse')}
              placeholder={I18n.t('common.select_one')} />
          </RowLayout>
        )}
        <RowLayout title={I18n.t('stock_move.search_word')}>
          <TextInput
            style={styles.searchInput}
            onChangeText={value => onSearchInputChanged(value, 'word')}
            value={searchInputs.word}
            placeholder={I18n.t('stock_move.search_word_placeholder')} />
        </RowLayout>
        <RowLayout title={I18n.t('stock_move.scheduled_instock_at')}>
          <View style={styles.dateWrapper}>
            <View style={styles.inputContainer}>
              <TextInput
                ref='dateFrom'
                style={[dateTimePickerStyles.datePickerInput]}
                value={formatDate(searchInputs.dateFrom)} />
              <TouchableOpacity
                style={dateTimePickerStyles.datePickerTouchable}
                onPress={() => {
                  this._showDateTimePicker(
                    I18n.t('stock_move.scheduled_instock_at_from'),
                    formatDate(searchInputs.dateFrom),
                    'dateFrom')
                }} />
            </View>
            <View style={styles.dateTextWrapper}>
              <Text style={styles.commonText}>{I18n.t('common.from')}</Text>
            </View>
          </View>
        </RowLayout>
        <RowLayout>
          <View style={styles.dateWrapper}>
            <View style={styles.inputContainer}>
              <TextInput
                ref='dateTo'
                style={[dateTimePickerStyles.datePickerInput]}
                value={formatDate(searchInputs.dateTo)} />
              <TouchableOpacity
                style={dateTimePickerStyles.datePickerTouchable}
                onPress={() => {
                  this._showDateTimePicker(
                    I18n.t('stock_move.scheduled_instock_at_to'),
                    formatDate(searchInputs.dateTo),
                    'dateTo')
                }} />
            </View>
            <View style={styles.dateTextWrapper}>
              <Text style={styles.commonText}>{I18n.t('common.until')}</Text>
            </View>
          </View>
        </RowLayout>
        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          mode='date'
          date={this.state.date}
          titleIOS={this.state.datePickerTitle}
          titleStyle={dateTimePickerStyles.datePickerTitle}
          confirmTextIOS={I18n.t('common.ok')}
          confirmTextStyle={dateTimePickerStyles.datePickerConfirm}
          cancelTextIOS={I18n.t('common.cancel')}
          cancelTextStyle={dateTimePickerStyles.datePickerCancel}
          minimumDate={new Date('1900/01/01')}
          onConfirm={(date) => { this._handleDatePicked(date) }}
          onCancel={this._hideDateTimePicker}
          neverDisableConfirmIOS />
      </View>
    )
  }

  render () {
    const isInstock = this.props.searchMode === STOCK_MOVEMENT_SEARCH_MODE.IN_STOCK
    return (
      <View style={styles.mainArea}>
        <View style={styles.searchAreaContainer} >
          {this._renderNewButtonArea()}
          <View style={styles.container}>
            <View style={styles.innerContainer}>
              <RowLayout title={I18n.t('stock_move.status')} style={styles.statusContainer}>
                <View style={styles.checkboxesContainer}>
                  {this._renderStatus()}
                </View>
              </RowLayout>
              {this._renderCommonArea(isInstock)}
              <View style={styles.btnClearWrapper}>
                <ProceedButton
                  style={styles.btnClear}
                  onPress={() => this.props.onResetToDefault(this.props)}
                  text={I18n.t('common.clear')} />
              </View>
              <View style={styles.btnSearchWrapper}>
                <CommandButton
                  style={styles.btnSearch}
                  onPress={() => this.props.onSearch(this.props, false)}
                  disabled={!this.props.canSearch}
                  text={I18n.t('common.search')} />
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
