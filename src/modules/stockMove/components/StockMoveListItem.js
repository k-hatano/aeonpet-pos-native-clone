import React, { Component } from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import styles from '../styles/StockMoveList'
import PropTypes from 'prop-types'
import { statusToLabel, STOCK_MOVEMENT_STATUS } from '../models'

export default class StockMoveListItem extends Component {
  static propTypes = {
    item: PropTypes.object,
    status: PropTypes.number,
    onSelectItem: PropTypes.func
  }

  render () {
    const { item } = this.props
    const useInStock = this.props.status === STOCK_MOVEMENT_STATUS.IN_STOCKED
    const isOutStocked = item.status === STOCK_MOVEMENT_STATUS.OUT_STOCKED
    const warehouseName = useInStock ? item.outstock_warehouse_name : item.instock_warehouse_name
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.props.onSelectItem(item)}
      >
        <View style={styles.rowView}>
          <View style={styles.dateContainer}>
            <Text style={styles.rowTextDate}>
              {item.scheduled_instock_at}
            </Text>
          </View>
          <View style={styles.whereHouseContainer}>
            <Text style={styles.rowTextTitle}>
              {warehouseName}
            </Text>
          </View>
          <View style={styles.rowTextWrapper}>
            <Text style={[styles.rowTextTitle, useInStock && isOutStocked ? styles.listItemAlertText : {}]}>
              {statusToLabel(item.status, !useInStock)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
