'use strict'
import React, { Component } from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import StockMoveFormListItem from './StockMoveFormListItem'
import styles from '../styles/StockMoveFormList'

export default class StockMoveFormList extends Component {
  static propTypes = {
    createInputs: PropTypes.object,
    currency: PropTypes.string,
    onChangeQuantity: PropTypes.func
  }

  render () {
    const {products} = this.props.createInputs
    const {currency, onChangeQuantity} = this.props
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    return (
      <View>
        <ListView
          enableEmptySections
          dataSource={dataSource.cloneWithRows(products)}
          renderRow={product => (
            <View>
              <StockMoveFormListItem
                product={product}
                currency={currency}
                onChangeQuantity={(id, quantity) => onChangeQuantity(id, quantity, products)} />
              <View style={styles.separator} />
            </View>
          )}
        />
      </View>
    )
  }
}
