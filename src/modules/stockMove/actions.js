import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setWarehouses = createAction(`${MODULE_NAME}_setWarehouses`)
export const setCurrentWarehouse = createAction(`${MODULE_NAME}_setCurrentWarehouse`)
export const listWarehouses = createAction(`${MODULE_NAME}_listWarehouses`)
export const setStockMovements = createAction(`${MODULE_NAME}_setStockMovements`)
export const setSelectedStockMovement = createAction(`$(MODULE_NAME)_setSelectedStockMovement`)
export const setStockMovementItems = createAction(`$(MODULE_NAME)_setStockMovementItems`)
export const setCreateInputValue = createAction(`${MODULE_NAME}_setCreateInputValue`)
export const resetCreateInputs = createAction(`${MODULE_NAME}_resetStockMove`)
export const setSearchInputValue = createAction(`$(MODULE_NAME)_setSearchInputValue`)
export const setSearchStatus = createAction(`$(MODULE_NAME)_setSearchStatus`)
export const resetSearchInputs = createAction(`$(MODULE_NAME)_resetSearchInputs`)
export const setSelectedTab = createAction(`$(MODULE_NAME)_setSelectedTab`)
