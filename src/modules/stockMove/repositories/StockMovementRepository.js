import SampleStockMovementRepository from './sample/SampleStockMovementRepository'
import StandardStockMovementRepository from './standard/StandardStockMovementRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockMovementRepository {
  static _implement = new StockMovementRepository()

  static async fetchByCondition (condition) {
    return this._implement.fetchByCondition(condition)
  }
  /**
   * 出庫情報をサーバーに送信します。
   * @param stockMovement
   * @returns {Promise<void>}
   */
  static async pushStockMovement (stockMovement) {
    return this._implement.pushStockMovement(stockMovement)
  }
  /**
   * 出庫情報の更新（入庫）をサーバーに送信します。
   * @param id
   * @param stockMovement
   * @returns {Promise<void>}
   */
  static async updateStockMovement (id, stockMovement) {
    return this._implement.updateStockMovement(id, stockMovement)
  }
  /**
   * 出庫情報のキャンセルをサーバーに送信します。
   * @param id
   * @param stockMovement
   * @returns {Promise<void>}
   */
  static async cancelStockMovement (id, stockMovement) {
    return this._implement.cancelStockMovement(id, stockMovement)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockMovementRepository()
        break

      default:
        this._implement = new SampleStockMovementRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StockMovementRepository', context => {
  StockMovementRepository.switchImplement(context)
})
