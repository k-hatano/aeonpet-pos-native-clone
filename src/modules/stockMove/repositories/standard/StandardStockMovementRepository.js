import I18n from 'i18n-js'
import fetcher from '../../../../common/models/fetcher'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import { handleAxiosError, REQUEST_ERROR_CODE } from '../../../../common/errors'
import MessageError from '../../../../common/errors/MessageError'
import { MAX_ITEM_QUANTITY } from '../../models'

export default class StandardStockMovingRepository {
  async fetchByCondition (condition) {
    try {
      const result = await fetcher.get('stock-movements', ({
        ...encodeCriteria(condition),
        with: 'stockMovementItems'
      }))
      return result.data
    } catch (error) {
      this.handleStockMoveError(error)
    }
  }

  async pushStockMovement (stockMovement) {
    try {
      await fetcher.post('stock-movements', stockMovement)
    } catch (error) {
      this.handleStockMoveError(error)
    }
  }

  async updateStockMovement (id, stockMovement) {
    try {
      await fetcher.put('stock-movements/' + id, stockMovement)
    } catch (error) {
      this.handleStockMoveError(error, true)
    }
  }

  async cancelStockMovement (id, stockMovement) {
    try {
      await fetcher.post('stock-movements/' + id + '/cancel', stockMovement)
    } catch (error) {
      this.handleStockMoveError(error)
    }
  }

  /**
   * 入出庫固有のエラーハンドリングとアプリ共通のエラーハンドリングを行います。
   * @param error
   * @param {bool} isInStock 入庫処理かどうか
   */
  handleStockMoveError (error, isInStock = false) {
    const {response} = error

    if (response && response.status === 404) {
      // 存在しません。
      throw new MessageError('message.N-03-E003')
    }
    if (response && response.status === 400 && response.data) {
      const action = isInStock
        ? I18n.t('stock_move.action_instock') : I18n.t('stock_move.action_outstock')
      switch (response.data.code) {
        case REQUEST_ERROR_CODE.IN_STOCK_BAD_STATUS:
          // 入庫確定できない。ステータス不正
          throw new MessageError('message.N-03-E002')
        case REQUEST_ERROR_CODE.OUT_STOCK_DUPLICATE_SLIP_NUMBER:
          // 伝票番号が重複してる。
          throw new MessageError('message.N-02-E002')
        case REQUEST_ERROR_CODE.CANCEL_STOCK_BAD_STATUS:
          // 入庫キャンセルできない。ステータス不正
          throw new MessageError('message.N-04-E001')
        case REQUEST_ERROR_CODE.STOCK_MOVE_PRODUCT_NOT_FOUND:
          // 商品が存在しない。
          throw new MessageError('message.N-00-E001', {action: action})
        case REQUEST_ERROR_CODE.STOCK_MOVE_PRODUCT_STOCK_MODE:
          // 在庫管理されていない商品
          throw new MessageError('message.N-00-E002', {action: action})
        case REQUEST_ERROR_CODE.STOCK_TRANSACTION_OVER_MAX_QUANTITY:
          // 在庫数が最大数を上回る場合
          throw new MessageError('message.N-00-E004', {action: action, quantity: MAX_ITEM_QUANTITY})
        case REQUEST_ERROR_CODE.STOCK_TRANSACTION_UNDER_MIN_QUANTITY:
          // 在庫数が最小数を下回る場合
          throw new MessageError('message.N-00-E003', {action: action, quantity: MAX_ITEM_QUANTITY})
      }
    }
    handleAxiosError(error)
  }
}
