import { sampleStockMovements } from '../../samples'
import { formatDate } from '../../../../common/utils/dateTime'

export default class SampleStockMovingRepository {
  async fetchByCondition (condition) {
    let stockMovement
    if (condition['deliv_date__gte'] && condition['deliv_date__lte']) {
      stockMovement = sampleStockMovements.filter(movement => (condition['deliv_date__gte'] <= movement.deliv_date && movement.deliv_date <= condition['deliv_date__lte']))
    }
    if (condition['instocked_at__gte'] && condition['instocked_at__lte']) {
      stockMovement = sampleStockMovements.filter(movement => (condition['instocked_at__gte'] <= movement.instocked_at && movement.instocked_at <= condition['instocked_at__lte']))
    }
    if (condition['status'].length > 0) {
      stockMovement = stockMovement.filter(movement => condition['status'].includes(movement.status))
    }
    if (condition['stockMovementItems.product_name']) {
      stockMovement = stockMovement.filter(movement => movement.stock_movement_items.some(item => (
        item.product_name.indexOf(condition['stockMovementItems.product_name']) !== -1
      )))
    }
    if (condition['outstock_warehouse_id']) {
      stockMovement = stockMovement.filter(movement => movement.outstock_warehouse_id === condition['outstock_warehouse_id'])
    }
    if (condition['instock_warehouse_id']) {
      stockMovement = stockMovement.filter(movement => movement.instock_warehouse_id === condition['instock_warehouse_id'])
    }
    if (condition['instock_staff_id']) {
      stockMovement = stockMovement.filter(movement => movement.instock_staff_id === condition['instock_staff_id'])
    }
    return stockMovement
  }

  async pushStockMovement (stockMovement) {
    sampleStockMovements.push(stockMovement)
  }

  async updateStockMovement (id, stockMovement) {
    const target = sampleStockMovements.filter(item => item.id === stockMovement.id)
    target.status = stockMovement.status
    target.instocked_at = formatDate(new Date())
    target.instock_staff_id = stockMovement.instock_staff_id
    target.instock_staff_name = stockMovement.instock_staff_name
    target.updated_at = formatDate(new Date())
  }

  async cancelStockMovement (id, stockMovement) {
    const target = sampleStockMovements.filter(item => item.id === stockMovement.id)
    target.status = stockMovement.status
    target.canceled_device_id = stockMovement.canceled_device_id
    target.canceled_staff_id = stockMovement.canceled_staff_id
    target.canceled_staff_name = stockMovement.canceled_staff_name
    target.canceled_at = formatDate(new Date())
    target.updated_at = formatDate(new Date())
  }
}
