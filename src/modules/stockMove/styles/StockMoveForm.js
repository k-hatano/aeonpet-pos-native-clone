import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  formSearchLayout: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  dateWrapper: {
    flexDirection: 'row',
    width: '100%'
  },
  searchInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: '100%',
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  searchInputDisabled: {
    backgroundColor: '#DCDCDC'
  },
  checkboxOptionText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  touchableContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  inputContainer: {
    height: 50,
    flex: 8
  },
  innerContainer: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 20
  },
  sampleRowContainer: {
    flexDirection: 'row',
    height: 50,
    marginBottom: 30
  },
  sampleRowTextContainer: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sampleRowTitle: {
    flex: 3,
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  compactRowContainer: {
    flexDirection: 'row',
    height: 15,
    marginBottom: 30
  },
  commonText: {
    fontSize: 24,
    height: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  selectInput: {
    width: '100%'
  },
  inlineRowLayout: {
    marginBottom: 25
  },
  listContainer: {
    flex: 1
  },
  tabContainer: {
    flex: 1,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5
  },
  tabContentArea: {
    justifyContent: 'flex-end',
    height: '99%',
    paddingBottom: 80,
    borderColor: '#979797',
    borderWidth: 1
  },
  categoryProductContainer: {
    flex: 3,
    marginTop: 10
  },
  categoryProduct: {
    height: 775
  },
  movementButton: {
    fontSize: 30,
    fontWeight: 'bold',
    height: 56,
    marginBottom: 5
  },
  tabStyle: {
    width: 136
  }
})
