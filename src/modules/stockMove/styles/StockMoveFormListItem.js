import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    backgroundColor: '#f7f7f7'
  },
  invisibleLabelBase: {
    backgroundColor: '#f7f7f7',
    width: 89,
    height: 17
  },
  productLabelArea: {
    flex: 3
  },
  headerArea: {
    height: 27,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  mainArea: {
    height: 60,
    marginLeft: 20,
    justifyContent: 'center'
  },
  productNameText: {
    height: 30
  },
  subArea: {
    height: 52,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24,
    alignItems: 'center'
  },
  priceText: {
    height: 29,
    marginRight: 30,
    justifyContent: 'space-around'
  },
  footer: {
    height: 10
  },
  buttonStyle: {
    fontSize: 24,
    fontWeight: 'bold',
    height: 40,
    width: 200
  }
})
