import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    width: '100%',
    height: '90%',
    overflow: 'hidden'
  },
  separator: {
    flex: 1,
    height: 1,
    backgroundColor: '#d8d8d8',
    marginLeft: 24,
    marginRight: 24
  },
  itemContainer: {
    marginLeft: 24,
    marginRight: 24
  },
  rowView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    width: 635,
    height: 64
  },
  dateContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  whereHouseContainer: {
    flex: 2.5,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  rowTextDate: {
    color: '#9b9b9b',
    fontFamily: 'Helvetica',
    fontSize: 18,
    letterSpacing: -0.29,
    paddingRight: 24,
    height: 22,
    paddingTop: 2
  },
  rowTextTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29,
    height: 22,
    paddingTop: 2
  },
  rowTextWrapper: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  listItemAlertText: {
    color: 'red'
  }
})
