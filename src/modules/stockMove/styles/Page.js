import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainArea: {
    flex: 1
  },
  listHeader: {
    paddingLeft: 10,
    paddingRight: 10,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9b9b9b'
  },
  headerText: {
    fontSize: 24,
    height: 24,
    fontFamily: 'HiraginoSans-W6',
    color: '#ffffff',
    letterSpacing: -0.38,
    fontWeight: '500'
  },
  detailContainer: {
    height: 64,
    width: '100%',
    backgroundColor: '#95989a',
    flexDirection: 'row',
    paddingLeft: 16
  },
  backButtonContainer: {
    position: 'absolute',
    left: 16,
    top: 16,
    width: 32,
    height: 32,
    zIndex: 1
  },
  backButton: {
    width: 32,
    height: 32
  },
  headerContainer: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  resultDetailArea: {
    flex: 1
  },
  resultDetailTab: {
    width: 176
  },
  actionButtonArea: {
    height: 80,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: '#979797',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  cancelButton: {
    width: 144,
    height: 60,
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10
  },
  instockButton: {
    width: 200,
    height: 60,
    fontSize: 24,
    fontWeight: 'bold',
    marginRight: 10
  }
})
