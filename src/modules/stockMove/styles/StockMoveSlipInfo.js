import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  mainArea: {
    paddingLeft: 22,
    paddingRight: 24
  },
  detailViewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#d8d8d8',
    borderBottomWidth: 1,
    height: 48,
    alignItems: 'center'
  },
  detailViewTextLeft: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: '#4a4a4a',
    textAlign: 'left',
    paddingTop: 5
  },
  detailViewTextRight: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    color: '#4a4a4a',
    textAlign: 'right',
    paddingTop: 5
  },
  detailNoteViewContainer: {
    borderColor: '#d8d8d8',
    borderBottomWidth: 1,
    height: 144
  },
  dateInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: 350,
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  quantityInput: {
    width: 93,
    height: 46
  }
})
