import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  main: {
    height: '100%'
  },
  itemContainer: {
    flex: 1
  },
  separator: {
    flex: 1,
    height: 1,
    backgroundColor: '#d8d8d8',
  },
  invisibleLabelBase: {
    backgroundColor: '#f7f7f7',
    width: 89,
    height: 17
  },
  productLabelArea: {
    flex: 3
  },
  headerArea: {
    height: 27,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  mainArea: {
    height: 55,
    marginLeft: 20,
    justifyContent: 'center'
  },
  subArea: {
    height: 52,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24,
    alignItems: 'center'
  },
  unitPriceText: {
    height: 21.5
  },
  quantityInput: {
    width: 93,
    height: 46
  }
})
