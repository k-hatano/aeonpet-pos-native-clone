import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  mainArea: {
    flex: 1,
    backgroundColor: '#f0f0f0'
  },
  tabContainer: {
    width: 176
  },
  sampleRowContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 22
  },
  sampleRowTextContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sampleRowTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  commonText: {
    fontSize: 24,
    height: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0'
  },
  innerContainer: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 20
  },
  dateWrapper: {
    flexDirection: 'row',
    margin: 0,
    width: '100%'
  },
  dateTextWrapper: {
    flex: 2,
    marginLeft: 10,
    justifyContent: 'center'
  },
  searchInput: {
    height: 48,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: 412,
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  inputContainer: {
    height: 50,
    flex: 8
  },
  checkboxesContainer: {
    justifyContent: 'center',
    flexDirection: 'row'
  },
  selectorContainer: {
    width: 320
  },
  btnClearWrapper: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 10,
    marginTop: 50
  },
  btnClear: {
    width: 144,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    fontSize: 24,
    fontWeight: 'bold'
  },
  btnSearchWrapper: {
    alignItems: 'center',
    padding: 10,
    marginTop: 50
  },
  btnSearch: {
    height: 60,
    width: '80%',
    fontSize: 32,
    fontWeight: 'bold'
  },
  newOutstockRegistButtonContainer: {
    marginBottom: 24,
    alignItems: 'flex-end'
  },
  newOutstockRegistButton: {
    width: 234,
    height: 50,
    fontSize: 24,
    fontWeight: 'bold'
  },
  searchAreaContainer: {
    flex: 1
  },
  newButtonArea: {
    width: 240,
    alignSelf: 'flex-end',
    marginTop: 30,
    marginRight: 50,
    marginBottom: 15
  },
  newButtonStyle: {
    height: 56,
    fontSize: 24,
    fontWeight: 'bold'
  },
  statusContainer: {
    marginTop: 15,
    marginBottom: 40
  }
})
