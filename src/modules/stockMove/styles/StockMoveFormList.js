import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
