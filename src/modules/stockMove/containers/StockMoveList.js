import { connect } from 'react-redux'
import StockMoveList from '../components/StockMoveList'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import * as _ from 'underscore'
import { loading } from '../../../common/sideEffects'
import ProductRepository from '../../../modules/product/repositories/ProductRepository'

const mapDispatchToProps = dispatch => ({
  onSelect: async (stockMovement) => {
    await loading(dispatch, async () => {
      const stockMovementItems = stockMovement.stock_movement_items
      const productIds = stockMovementItems.map(item => item.product_id)
      const productIdMap = _.indexBy(await ProductRepository.findProductsByProductId(productIds), 'id')
      const resultItems = stockMovementItems.map(item => {
        const product = productIdMap[item.product_id]
        if (product !== undefined) {
          return {
            ...item,
            product_code: item.product_code,
            product_name: item.product_name,
            price: item.price,
            stock_item: product.product_variants[0].stock_item
          }
        } else {
          return {
            ...item
          }
        }
      })
      dispatch(actions.setSelectedStockMovement(stockMovement))
      dispatch(actions.setStockMovementItems(resultItems))
    })
  }
})

const mapStateToProps = state => ({
  stockMovements: state[MODULE_NAME].stockMovements
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveList)
