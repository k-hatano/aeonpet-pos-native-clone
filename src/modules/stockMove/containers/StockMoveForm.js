import * as actions from '../actions'
import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import StockMoveForm from '../components/StockMoveForm'

const mapDispatchToProps = dispatch => ({
  onSetCreateInputValue: (value, propName) => {
    dispatch(actions.setCreateInputValue({value, propName}))
  }
})

const mapStateToProps = state => ({
  warehouses: state[MODULE_NAME].warehouses,
  createInputs: state[MODULE_NAME].createInputs
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveForm)
