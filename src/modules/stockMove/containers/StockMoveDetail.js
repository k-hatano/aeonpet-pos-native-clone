import { connect } from 'react-redux'
import StockMoveDetail from '../components/StockMoveDetail'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'

const mapDispatchToProps = dispatch => ({
  onChangeQuantity: async (stockMovementItems, itemKey, quantity) => {
    const modifyItems = stockMovementItems.map((item, keyIndex) => {
      if (keyIndex === itemKey) {
        return {
          ...item,
          quantity: quantity
        }
      }
      return item
    })
    dispatch(actions.setStockMovementItems(modifyItems))
  }
})

const mapStateToProps = state => ({
  selectedStockMovement: state[MODULE_NAME].selectedStockMovement,
  stockMovementItems: state[MODULE_NAME].stockMovementItems
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveDetail)
