import { connect } from 'react-redux'
import StockMoveSlipInfo from '../components/StockMoveSlipInfo'
import { MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  selectedStockMovement: state[MODULE_NAME].selectedStockMovement
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveSlipInfo)
