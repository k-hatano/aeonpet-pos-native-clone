import { connect } from 'react-redux'
import SettingKeys from '../../../modules/setting/models/SettingKeys'
import { getSettingFromState } from '../../../modules/setting/models'
import * as actions from '../actions'
import { MODULE_NAME, MAX_ITEM_QUANTITY } from '../models'
import StockMoveFormList from '../components/StockMoveFormList'
import I18n from 'i18n-js'
import AlertView from '../../../common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  onChangeQuantity: (id, quantity, products) => {
    // 入力数が上限値を超えている場合は増加させない。
    if (quantity > MAX_ITEM_QUANTITY) return

    const productIndex = products.findIndex(product => product.id === id)

    if (productIndex >= 0) {
      const stockItem = products[productIndex].product_variants[0].stock_item
      const stockQuantity = (stockItem && stockItem.quantity) || 0

      if (Math.abs(stockQuantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.N-02-E003', {quantity: MAX_ITEM_QUANTITY}))
        return false
      } else {
        if (quantity > 0) {
          products[productIndex].quantity = quantity
        } else {
          products.splice(productIndex, 1)
        }
        dispatch(actions.setCreateInputValue({value: [...products], propName: 'products'}))
      }
    }
  }
})

const mapStateToProps = state => ({
  createInputs: state[MODULE_NAME].createInputs,
  currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY)
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveFormList)
