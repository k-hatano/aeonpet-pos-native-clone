import { connect } from 'react-redux'
import StockMoveSearchForm from '../components/StockMoveSearchForm'
import {
  canSearchStockMovement, MODULE_NAME, STOCK_MOVEMENT_SEARCH_MODE, STOCK_MOVEMENT_STATUS
} from '../models'
import * as actions from '../actions'
import ShopRepository from '../../shop/repositories/ShopRepository'
import WarehouseRepository from '../../../modules/stock/repositories/WarehouseRepository'
import { getSettingFromState } from '../../../modules/setting/models'
import SettingKeys from '../../../modules/setting/models/SettingKeys'
import { searchStockMovements } from '../services'
import { addDays, getToday } from '../../../common/utils/dateTime'
import { loading } from '../../../common/sideEffects'

const mapDispatchToProps = dispatch => ({
  onInit: async (props) => {
    await loading(dispatch, async () => {
      const currentShop = await ShopRepository.find()
      const warehouses = await WarehouseRepository.findAll()
      const currentWarehouse = warehouses.find(function (w) { return w.warehouse_code === currentShop.shop_code })

      dispatch(actions.setWarehouses(warehouses))
      dispatch(actions.setCurrentWarehouse(currentWarehouse))
    })
  },
  onResetToDefault: (props) => {
    const today = getToday()

    let propName
    let defaultStatus = []
    let defaultDateFrom
    let defaultDateTo
    // 入庫検索の場合は出庫先が自店舗倉庫
    // 出庫検索の場合は出庫元が自店舗倉庫
    if (props.searchMode === STOCK_MOVEMENT_SEARCH_MODE.OUT_STOCK) {
      propName = 'outstockWarehouse'
      defaultStatus[STOCK_MOVEMENT_STATUS.IN_STOCKED] = true
      defaultStatus[STOCK_MOVEMENT_STATUS.OUT_STOCKED] = true
      defaultDateFrom = today
      defaultDateTo = addDays(today, 7)
    } else {
      propName = 'instockWarehouse'
      defaultStatus[STOCK_MOVEMENT_STATUS.OUT_STOCKED] = true
      defaultDateFrom = addDays(today, -7)
      defaultDateTo = addDays(today, 7)
    }
    dispatch(actions.resetSearchInputs({defaultStatus}))
    dispatch(actions.setSearchInputValue({value: defaultDateFrom, propName: 'dateFrom'}))
    dispatch(actions.setSearchInputValue({value: defaultDateTo, propName: 'dateTo'}))
    dispatch(actions.setSearchInputValue({value: props.currentWarehouse, propName}))
    dispatch(actions.setSearchInputValue({value: props.searchMode, propName: 'mode'}))
    dispatch(actions.setStockMovements([]))
  },
  onSearchStatusChanged: (item, checked) => {
    dispatch(actions.setSearchStatus({value: item.value, checked}))
  },
  onSearchInputChanged: (value, propName) => {
    dispatch(actions.setSearchInputValue({value, propName}))
  },
  onSearch: async (props) => {
    await searchStockMovements(dispatch, props.searchInputs)
  }
})

const mapStateToProps = state => ({
  warehouses: state[MODULE_NAME].warehouses,
  currentWarehouse: state[MODULE_NAME].currentWarehouse,
  shopId: getSettingFromState(state, SettingKeys.COMMON.SHOP_ID),
  searchInputs: state[MODULE_NAME].searchInputs,
  canSearch: canSearchStockMovement(state[MODULE_NAME].searchInputs)
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveSearchForm)
