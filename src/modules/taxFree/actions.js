import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setOrder = createAction(`${MODULE_NAME}_setOrder`)

//CustomerInfo
export const listCountries = createAction(`${MODULE_NAME}_listCountries`)
export const updateTypeOfPassport = createAction(`${MODULE_NAME}_updateTypeOfPassport`)
export const updateResidenceStatus = createAction(`${MODULE_NAME}_updateResidenceStatus`)
export const updatePassportNumber = createAction(`${MODULE_NAME}_updatePassportNumber`)
export const updateCustomerName = createAction(`${MODULE_NAME}_updateCustomerName`)
export const updateBirthday = createAction(`${MODULE_NAME}_updateBirthday`)
export const updateCountryOfCitizenship = createAction(`${MODULE_NAME}_updateCountryOfCitizenship`)
export const updateLandedAt = createAction(`${MODULE_NAME}_updateLandedAt`)
export const updateOtherResidenceStatus = createAction(`${MODULE_NAME}_updateOrtherResidenceStatus`)

//SellerInfo
export const updateJurisdictionTaxOffice = createAction(`${MODULE_NAME}_updateJurisdictionTaxOffice`)
export const updatePlaceOfTaxPayment = createAction(`${MODULE_NAME}_updatePlaceOfTaxPayment`)
export const updateSellerLocation = createAction(`${MODULE_NAME}_updateSellerLocation`)
export const updateSellerNameNickName = createAction(`${MODULE_NAME}_updateSellerNameNickName`)

export const setEJournals = createAction(`${MODULE_NAME}_setEJournals`)
export const updateIsPushed = createAction(`${MODULE_NAME}_updateIsPushed`)
export const updateIsPrinted = createAction(`${MODULE_NAME}_updateIsPrinted`)

export const clearTaxFree = createAction(`${MODULE_NAME}_clearTaxFree`)