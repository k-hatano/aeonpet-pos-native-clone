import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  wrapContainer: {
    backgroundColor: '#ccc',
    width: 400,
    height: 396
  },
  wrapContainerList: {
    backgroundColor: '#fff',
    height:'100%'
  },
  headerStyle: {
    height: 66,
    marginLeft: 30
  },
  listContainer: {
    borderTopColor: '#ccc',
    borderTopWidth: 2
  },
  listTitle: {
    height: 79,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 32,
    marginRight: 32
  }
})
