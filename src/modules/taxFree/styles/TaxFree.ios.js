import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  flex1: {
    flex: 1
  },
  flex2: {
    flex: 2
  },
  column: {
    flexDirection: 'column'
  },
  pageTemplate: {
    backgroundColor: '#fff'
  },
  header: {
    flex: 0,
    height: 72,
    zIndex: 1
  },
  headerInfo: {
    flex: 0,
    height: 130
  },
  headerContent: {
    padding: 32
  },
  headerSubComponent: {
    flex: 0,
    height: 48,
    backgroundColor: '#f0f0f0',
    alignItems: 'flex-start',
    paddingLeft: 32,
    paddingTop: 10
  },
  formTitle: {
    height: 48,
    backgroundColor: 'red',
    alignItems: 'center',
    paddingLeft: 32
  },
  radioGroupContainer: {
    padding: 32,
    flexDirection: 'row'
  },
  textInputContainer: {
    paddingLeft: 32,
    flexDirection: 'row',
    paddingTop: 16,
    paddingBottom: 16
  },
  textInput: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    height: 46,
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 20
  },
  inputTitle: {
    width: 200
  },
  normalTextStyle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  oneButtonContainer: {
    height: 50,
    width: '100%',
    alignItems: 'flex-end',
    paddingRight: 32,
    marginBottom: 32
  },
  twoButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    width: '100%',
    paddingRight: 32,
    paddingLeft: 32,
    marginBottom: 32
  },
  rightButton: {
    width: 450,
    height: 56,
    paddingRight: 32,
    paddingLeft: 32
  },
  leftButton: {
    width: 530,
    height: 50,
    paddingRight: 32,
    paddingLeft: 32
  },
  textButtonNormal: {
    height: 32,
    fontFamily: 'HiraginoSans-W6',
    fontSize: 32,
    letterSpacing: -0.51,
    fontWeight: 'bold'
  },
  reviewText: {
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29,
    marginRight: 200,
    marginLeft: 80,
    marginBottom: 30
  },
  inlineView: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  articleLabel: {
    width: 200
  }
})
