import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  item1: {
    flex: 1
  },
  item2: {
    flex: 1
  },
  textInput: {
    width: 320
  },
  selectInput: {
    width: 320
  },
  numberInput: {
    width: 1000
  },
  touchableContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  datePickerTitle: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  datePickerConfirm: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#44bcb9',
    color: '#ffffff',
    padding: 15
  },
  datePickerCancel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#ffffff',
    color: '#44bcb9'
  },
  selectStyle: {
    height: 46,
    marginTop: 0,
    marginBottom: 0
  }
})
