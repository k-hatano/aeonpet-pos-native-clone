import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  confirmFormContainer: {
    paddingLeft: 32,
    paddingRight: 32,
    paddingTop: 32
  },
  confrimFormRow: {
    paddingLeft: 32,
    paddingRight: 32,
    marginTop: 32,
    flexDirection: 'row'
  },
  textButtonExtend: {
    fontSize: 32,
    fontWeight: 'bold'
  }
})
