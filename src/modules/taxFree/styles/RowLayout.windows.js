import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    paddingLeft: 32,
    flexDirection: 'row',
    paddingTop: 16,
    paddingBottom: 16
  },
  inputTitle: {
    width: 200
  },
  textNormal: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  input: {
    flex: 1
  },
  inlineView: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  articleLabel: {
    width: 200
  },
  reviewText: {
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29,
    marginRight: 200,
    marginLeft: 80,
    marginBottom: 30
  }
})
