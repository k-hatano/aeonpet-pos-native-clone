import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    paddingLeft: 30,
    flexDirection: 'row',
    paddingTop: 30,
    paddingBottom: 30,
    zIndex: 0
  },
  inputTitle: {
    width: 200
  },
  textNormal: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  input: {
    flex: 1,
    width: 320
  },
  inlineView: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  articleLabel: {
    width: 200
  }
})
