import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  textInput: {
    width: 640
  },
  taxOfficeInput: {
    width: 160
  },
  jurisdictionTaxOfficeContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  jurisdictionTaxOfficeText: {
    marginLeft: 16
  },
  clearButton: {
    width: 306
  }
})
