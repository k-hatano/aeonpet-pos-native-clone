import React, { Component } from 'react'
import { View, Text } from 'react-native'
import componentStyles from '../styles/RowLayout'

export default class RowLayout extends Component {
  render() {
    return (
      <View style={[componentStyles.container, this.props.style]}>
        <View style={componentStyles.inputTitle}>
          <Text style={componentStyles.textNormal}>
            {this.props.title}
          </Text>
        </View>
        <View style={componentStyles.input}>
          {this.props.children}
        </View>
      </View>
    )
  }
}

export class ConfirmRowLayout extends Component {
  render() {
    return (
      <View style={componentStyles.inlineView}>
        <Text style={[this.props.labelStyle, componentStyles.articleLabel]}>
          {this.props.label}
        </Text>
        <Text style={[this.props.textStyle, componentStyles.textNormal]}>
          {this.props.text}
        </Text>
      </View>
    )
  }
}
