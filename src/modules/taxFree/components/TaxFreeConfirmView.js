import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import I18n from 'i18n-js'
import { Row, Grid } from 'react-native-easy-grid'
import commonStyle from '../styles/TaxFree'
import componentStyle from '../styles/TaxFreeConfirmView'
import PropTypes from 'prop-types'
import MomentPropTyes from 'react-moment-proptypes'
import {
  CommandButton,
  ProceedButton
} from '../../../common/components/elements/StandardButton'
import {
  TypeOfPassport,
  ResidenceStatus,
  passportTypeToLabel,
  residenceStatusToLabel,
  RESIDENCE_TYPE
} from '../models'
import { ConfirmRowLayout } from './RowLayout'
import moment from 'moment'

export default class TaxFreeConfirmView extends Component {
  static propTypes = {
    order: PropTypes.object,
    //CustomerInfo
    typeOfPassport: PropTypes.number,
    passportNumber: PropTypes.string,
    customerName: PropTypes.string,
    birthday: MomentPropTyes.momentObj,
    countryOfCitizenship: PropTypes.object,
    landedAt: MomentPropTyes.momentObj,
    residenceStatus: PropTypes.number,
    otherResidenceStatus: PropTypes.string,
    //SellerInfo
    jurisdictionTaxOffice: PropTypes.string,
    placeOfTaxPayment: PropTypes.string,
    sellerLocation: PropTypes.string,
    sellerNameNickName: PropTypes.string,

    onComplete: PropTypes.func,
    onFix: PropTypes.func
  }

  _getClientCreateDateTimeFormat = (timestamp) => {
    return moment.unix(timestamp).format('YYYY-MM-DD HH:mm:ss')
  }

  render() {
    const order = this.props.order
    const pos_order = order.pos_order
    const countryOfCitizenshipLabel = this.props.countryOfCitizenship ? this.props.countryOfCitizenship.label : ''

    return (
      <Grid style={commonStyle.pageTemplate}>
        <Row style={commonStyle.headerInfo}>
          <View style={commonStyle.headerContent}>
            <ConfirmRowLayout
              label={I18n.t('tax_free.pos_order_number')}
              text={pos_order.pos_order_number}
              labelStyle={commonStyle.normalTextStyle} />
            <ConfirmRowLayout
              label={I18n.t('tax_free.client_created_at')}
              text={this._getClientCreateDateTimeFormat(pos_order.client_created_at)}
              labelStyle={commonStyle.normalTextStyle} />
          </View>
        </Row>
        <View style={commonStyle.flex1}>
          <Row style={commonStyle.headerSubComponent}>
            <Text style={commonStyle.normalTextStyle}>
              {I18n.t('tax_free.confirm_form_inputs')}
            </Text>
          </Row>
          <Row style={commonStyle.column}>
            <ScrollView>
              <View style={componentStyle.confirmFormContainer}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t('tax_free.customer_info')}
                </Text>
                <ConfirmRowLayout
                  label={I18n.t('tax_free.type_of_passport')}
                  text={passportTypeToLabel(this.props.typeOfPassport)}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.passport_number')}
                  text={this.props.passportNumber}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.customer_name')}
                  text={this.props.customerName}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.country_of_citizenship')}
                  text={countryOfCitizenshipLabel}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.birthday')}
                  text={
                    this.props.birthday &&
                    this.props.birthday.format('YYYY/MM/DD')
                  }
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.residence_status')}
                  text={
                    residenceStatusToLabel(this.props.residenceStatus) +
                    ' ' +
                    ((this.props.residenceStatus === RESIDENCE_TYPE.OTHER) && this.props.otherResidenceStatus ?
                      this.props.otherResidenceStatus : '')
                  }
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.landed_at')}
                  text={
                    this.props.landedAt &&
                    this.props.landedAt.format('YYYY/MM/DD')
                  }
                  labelStyle={commonStyle.reviewText}
                />

              </View>
              <View style={componentStyle.confirmFormContainer}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t('tax_free.seller_info')}
                </Text>
                <ConfirmRowLayout
                  label={I18n.t('tax_free.jurisdiction_tax_office')}
                  text={this.props.jurisdictionTaxOffice}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.place_of_tax_payment')}
                  text={this.props.placeOfTaxPayment}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.seller_location')}
                  text={this.props.sellerLocation}
                  labelStyle={commonStyle.reviewText}
                />
                <ConfirmRowLayout
                  label={I18n.t('tax_free.seller_name_nick_name')}
                  text={this.props.sellerNameNickName}
                  labelStyle={commonStyle.reviewText}
                />
              </View>
            </ScrollView>
            <View style={commonStyle.twoButtonContainer}>
              <View style={commonStyle.rightButton}>
                <CommandButton
                  text={I18n.t('tax_free.correct_input')}
                  style={componentStyle.textButtonExtend}
                  onPress={this.props.onFix}
                />
              </View>
              <View style={commonStyle.rightButton}>
                <ProceedButton
                  text={I18n.t('tax_free.print')}
                  style={componentStyle.textButtonExtend}
                  onPress={this.props.onComplete}
                />
              </View>
            </View>
          </Row>
        </View>
      </Grid>
    )
  }
}
