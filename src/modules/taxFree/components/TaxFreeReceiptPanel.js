import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
// import { Row, Grid } from 'react-native-easy-grid'
import Modal from '../../../common/components/widgets/Modal'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import PropTypes from 'prop-types'
import commonStyle from '../styles/TaxFree'
import componentStyles from '../styles/ReceiptList'

export default class TaxFreeReceiptList extends Component {
  static propTypes = {
    ejournals: PropTypes.object,
    onPrintSales: PropTypes.func,
    onPrintCustomer: PropTypes.func,
    onPrintshipping: PropTypes.func,
    onPrintAll: PropTypes.func,
    onClose: PropTypes.func
  }

  render () {
    const salesEjournals = this.props.salesEjournals
    const customerEjournals = this.props.customerEjournals
    const shippingEjournals = !!this.props.shippingEjournals ? this.props.shippingEjournals : null
    const modalHeight = shippingEjournals ? null : { height: 317 }
    return (
      <ModalFrame
        title={I18n.t('tax_free.reprint_receipt')}
        style={[componentStyles.wrapContainer, modalHeight]}
        onClose={() => this.props.onClose()}>
        <View style={componentStyles.wrapContainerList}>
          <View style={componentStyles.listContainer}>
            <TouchableOpacity
              onPress={() =>
                this.props.onPrintSales(salesEjournals)}>
              <View style={componentStyles.listTitle}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t(
                    'tax_free.record_of_purchase_of_consumption_taxFree_for_export'
                  )}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={componentStyles.listContainer}>
            <TouchableOpacity
              onPress={() => this.props.onPrintCustomer(customerEjournals)}>
              <View style={componentStyles.listTitle}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t('tax_free.covenant_of_purchaser')}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {shippingEjournals && 
            <View style={componentStyles.listContainer}>
              <TouchableOpacity
                onPress={() => this.props.onPrintShipping(shippingEjournals)}>
                <View style={componentStyles.listTitle}>
                  <Text style={commonStyle.normalTextStyle}>
                    {I18n.t('tax_free.packing_list')}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          }
          <View style={componentStyles.listContainer}>
            <TouchableOpacity onPress={() => this.props.onPrintAll(salesEjournals, customerEjournals, shippingEjournals)}>
              <View style={componentStyles.listTitle}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t('tax_free.print_all')}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ModalFrame>
    )
  }
}
