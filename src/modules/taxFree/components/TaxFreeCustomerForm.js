import React, { Component } from 'react'
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  NativeModules,
  NativeAppEventEmitter
} from 'react-native'
import I18n from 'i18n-js'
import { Row, Grid } from 'react-native-easy-grid'
import commonStyle from '../styles/TaxFree'
import componentStyle from '../styles/TaxFreeCustomerForm'
import Select from '../../../common/components/elements/Select'
import PropTypes from 'prop-types'
import MomentPropTyes from 'react-moment-proptypes'
import StandardButton from '../../../common/components/elements/StandardButton'
import RowLayout from './RowLayout'
import {
  passportTypeToLabel,
  getResidenceStatusOption,
  getPassportTypeOption,
  PASSPORT_TYPE,
  RESIDENCE_TYPE,
  CustomerFormGroup as FormGroup

} from '../models'
import Moment from 'moment'
import { RadioGroup } from '../../../common/components/elements/Inputs'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { setPassportInfo } from '../services'
import AlertView from 'common/components/widgets/AlertView'
import KeyboardSpace from '../../../common/components/layout/KeyboardSpace'

export default class TaxFreeCustomerForm extends Component {
  static propTypes = {
    passportNumber: PropTypes.string,
    customerName: PropTypes.string,
    birthday: MomentPropTyes.momentObj,
    countryOfCitizenship: PropTypes.object,
    landedAt: MomentPropTyes.momentObj,
    residenceStatus: PropTypes.number,
    otherResidenceStatus: PropTypes.string,
    countries: PropTypes.array,
    order: PropTypes.object,
    updateTypePassport: PropTypes.func,
    onChangedForm: PropTypes.func,
    onSubmit: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  _showDateTimePicker (title, date, key) {
    this.setState({
      isDatePickerVisible: true,
      date: date ? new Date(date) : new Date(),
      datePickerTitle: title,
      dateKey: key
    })
  }

  _hideDateTimePicker = () => this.setState({ isDatePickerVisible: false })

  _handleDatePicked (date) {
    this.setState({ isDatePickerVisible: false })
    this.props.onChangedForm(this.state.dateKey, Moment(date))
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  componentDidMount () {
    this.eventBarcodeReader = NativeAppEventEmitter.addListener(
      'barcode',
      async body => {
        const result = await setPassportInfo(body.trim())
        if (result) {
          this.props.onChangedForm(FormGroup.PASSPORT_NUMBER, result.passportNo)
          this.props.onChangedForm(FormGroup.TYPE_OF_PASSPORT, PASSPORT_TYPE.PASSPORT)
          this.props.onChangedForm(FormGroup.CUSTOMER_NAME, result.customertName)
          this.props.onChangedForm(FormGroup.BIRTHDAY, result.birthDay)
          this.props.onChangedForm(FormGroup.COUNTRY_OF_CITIZENSHIP, result.country)
          this.refs.radioGroup.onSelect(
            {
              label: passportTypeToLabel(PASSPORT_TYPE.PASSPORT),
              value: PASSPORT_TYPE.PASSPORT,
              selected: true
            }
          )
        } else {
          AlertView.show(I18n.t('message.K-01-E001'))
        }
      }
    )

    NativeModules.BarcodeReaderBridge.setBarcodeReaderProperty({
      BARCODE_READER_SDK: 0,
      BARCODE_READER_STD: 1,
      BARCODE_READER_IP: 'empty'
    })
  }

  _getClientCreateDateTimeFormat = (timestamp) => {
    return Moment.unix(timestamp).format('YYYY-MM-DD HH:mm:ss')
  }

  _changeStatusOfResidence = (item) => {
    this.props.onChangedForm(
      FormGroup.STATUS_OF_RESIDENCE,
      item.value
    )
    if (item.value !== RESIDENCE_TYPE.OTHER) {
      this.props.onChangedForm(
        FormGroup.OTHER_STATUS_OF_RESIDENCE,
        null
      )
    }
  }

  render () {
    const order = this.props.order
    const posOrder = order.pos_order

    return (
      <Grid>
        <View>
          <ScrollView>
            <Row style={commonStyle.headerInfo}>
              <View style={commonStyle.headerContent}>
                <View style={commonStyle.inlineView}>
                  <Text
                    style={[commonStyle.normalTextStyle, commonStyle.articleLabel]}>
                    {I18n.t('tax_free.pos_order_number')}
                  </Text>
                  <Text style={commonStyle.normalTextStyle}>
                    {posOrder.pos_order_number}
                  </Text>
                </View>
                <View style={commonStyle.inlineView}>
                  <Text
                    style={[commonStyle.normalTextStyle, commonStyle.articleLabel]}>
                    {I18n.t('tax_free.client_created_at')}
                  </Text>
                  <Text style={commonStyle.normalTextStyle}>
                    {this._getClientCreateDateTimeFormat(posOrder.client_created_at)}
                  </Text>
                </View>
              </View>
            </Row>
            <View style={commonStyle.flex1}>
              <View style={commonStyle.headerSubComponent}>
                <Text style={commonStyle.normalTextStyle}>
                  {I18n.t('tax_free.customer_info')}
                </Text>
              </View>
              <View>
                <RowLayout
                  title={I18n.t('tax_free.type_of_passport')}
                  style={{ width: '80%' }}>
                  <RadioGroup
                    ref={'radioGroup'}
                    options={getPassportTypeOption()}
                    onSelectItem={item =>
                      this.props.onChangedForm(
                        FormGroup.TYPE_OF_PASSPORT,
                        item.value
                      )}
                    wrap={true}
                  />
                </RowLayout>
                <RowLayout title={I18n.t('tax_free.passport_number')}>
                  <TextInput
                    ref="passportNumber"
                    placeholder={I18n.t('tax_free.passport_number')}
                    style={[commonStyle.textInput, componentStyle.numberInput]}
                    onChangeText={text =>
                      this.props.onChangedForm(FormGroup.PASSPORT_NUMBER, text)}
                    value={this.props.passportNumber}
                    maxLength={50}
                  />
                </RowLayout>
                <View style={componentStyle.container}>
                  <View style={componentStyle.item1}>
                    <RowLayout title={I18n.t('tax_free.customer_name')}>
                      <TextInput
                        ref="customerName"
                        placeholder={I18n.t('tax_free.customer_name')}
                        style={[commonStyle.textInput, componentStyle.textInput]}
                        onChangeText={text =>
                          this.props.onChangedForm(FormGroup.CUSTOMER_NAME, text)}
                        value={this.props.customerName}
                        maxLength={50}
                      />
                    </RowLayout>
                    <RowLayout title={I18n.t('tax_free.birthday')}>
                      <TextInput
                        ref="birthday"
                        placeholder={I18n.t('tax_free.birthday')}
                        style={[commonStyle.textInput, componentStyle.textInput]}
                        onChangeText={text =>
                          this.props.onChangedForm(FormGroup.BIRTHDAY, text)}
                        value={
                          this.props.birthday &&
                          this.props.birthday.format('YYYY/MM/DD')
                        } />
                      <TouchableOpacity
                        style={componentStyle.touchableContainer}
                        onPress={() => {
                          this._showDateTimePicker(
                            I18n.t('tax_free.birthday'),
                            this.props.birthday && this.props.birthday.format('YYYY/MM/DD'),
                            FormGroup.BIRTHDAY)
                        }}
                      />
                    </RowLayout>
                  </View>
                  <View style={componentStyle.item2}>
                    <RowLayout
                      title={I18n.t('tax_free.country_of_citizenship')}
                      style={{ zIndex: 1000 }}>
                      <View
                        style={componentStyle.selectInput}>
                        <Select
                          selectStyle={componentStyle.selectStyle}
                          items={this.props.countries}
                          displayProperty='label'
                          selectedItem={this.props.countryOfCitizenship}
                          onItemChange={item =>
                            this.props.onChangedForm(
                              FormGroup.COUNTRY_OF_CITIZENSHIP,
                              item
                            )}
                          placeholder={I18n.t('tax_free.country_of_citizenship')}
                        />
                      </View>
                    </RowLayout>
                    <RowLayout title={I18n.t('tax_free.landed_at')}>
                      <TextInput
                        ref="landedAt"
                        placeholder={I18n.t('tax_free.landed_at')}
                        style={[commonStyle.textInput, componentStyle.textInput]}
                        value={
                          this.props.landedAt &&
                          this.props.landedAt.format('YYYY/MM/DD')
                        } />
                      <TouchableOpacity
                        style={componentStyle.touchableContainer}
                        onPress={() => {
                          this._showDateTimePicker(
                            I18n.t('tax_free.landed_at'),
                            this.props.landedAt && this.props.landedAt.format('YYYY/MM/DD'),
                            FormGroup.LANDED_AT)
                        }}
                      />
                    </RowLayout>
                  </View>
                </View>
                <RowLayout
                  title={I18n.t('tax_free.residence_status')}
                  style={{ width: '67%' }}>
                  <RadioGroup
                    options={getResidenceStatusOption()}
                    onSelectItem={item => this._changeStatusOfResidence(item)}
                    wrap={true} />
                  <TextInput
                    ref="otherResidenceStatus"
                    placeholder={I18n.t('tax_free.others')}
                    style={[
                      commonStyle.textInput,
                      componentStyle.textInput,
                      { marginLeft: '20%', width: '80%' }
                    ]}
                    onChangeText={text =>
                      this.props.onChangedForm(
                        FormGroup.OTHER_STATUS_OF_RESIDENCE,
                        text
                      )}
                    value={this.props.otherResidenceStatus}
                    maxLength={50}
                    editable={this.props.residenceStatus === RESIDENCE_TYPE.OTHER}
                  />
                </RowLayout>
              </View>
              <View style={commonStyle.oneButtonContainer}>
                <View style={commonStyle.rightButton}>
                  <StandardButton
                    isBorderButton={false}
                    textStyle={commonStyle.textButtonNormal}
                    onPress={() => {
                      this.props.onSubmit(this.props)
                    }}
                    text={I18n.t('tax_free.confirm')}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
          <KeyboardSpace />
          <DateTimePicker
            isVisible={this.state.isDatePickerVisible}
            mode='date'
            date={this.state.date}
            titleIOS={this.state.datePickerTitle}
            titleStyle={componentStyle.datePickerTitle}
            confirmTextIOS={I18n.t('common.ok')}
            confirmTextStyle={componentStyle.datePickerConfirm}
            cancelTextIOS={I18n.t('common.cancel')}
            cancelTextStyle={componentStyle.datePickerCancel}
            minimumDate={new Date('1900/01/01')}
            maximumDate={new Date()}
            onConfirm={(date) => { this._handleDatePicked(date) }}
            onCancel={this._hideDateTimePicker}
            neverDisableConfirmIOS />
        </View>
      </Grid>
    )
  }
}
