import React, { Component } from 'react'
import { Text, View, ScrollView, TextInput } from 'react-native'
import I18n from 'i18n-js'
import { Row, Grid } from 'react-native-easy-grid'
import commonStyle from '../styles/TaxFree'
import componentStyles from '../styles/TaxFreeSellerForm'
import PropTypes from 'prop-types'
import StandardButton from '../../../common/components/elements/StandardButton'
import RowLayout from './RowLayout'
import { SellerFormGroup as FormGroup } from '../models'
import moment from 'moment'

export default class TaxFreeSellerForm extends Component {
  static propTypes = {
    order: PropTypes.object,
    jurisdictionTaxOffice: PropTypes.string,
    placeOfTaxPayment: PropTypes.string,
    sellerLocation: PropTypes.string,
    sellerNameNickName: PropTypes.string,
    canComplete: PropTypes.bool,
    onSubmit: PropTypes.func,
    onChangedForm: PropTypes.func
  }

  _getClientCreateDateTimeFormat = (timestamp) => {
    return moment.unix(timestamp).format('YYYY-MM-DD HH:mm:ss')
  }

  render() {
    const order = this.props.order
    const pos_order = order.pos_order
    return (
      <Grid style={commonStyle.pageTemplate}>
        <Row style={commonStyle.headerInfo}>
          <View style={commonStyle.headerContent}>
            <View style={commonStyle.inlineView}>
              <Text
                style={[commonStyle.normalTextStyle, commonStyle.articleLabel]}>
                {I18n.t('tax_free.pos_order_number')}
              </Text>
              <Text style={commonStyle.normalTextStyle}>
                {pos_order.pos_order_number}
              </Text>
            </View>
            <View style={commonStyle.inlineView}>
              <Text
                style={[commonStyle.normalTextStyle, commonStyle.articleLabel]}>
                {I18n.t('tax_free.client_created_at')}
              </Text>
              <Text style={commonStyle.normalTextStyle}>
                {this._getClientCreateDateTimeFormat(pos_order.client_created_at)}
              </Text>
            </View>
          </View>
        </Row>
        <View style={commonStyle.flex1}>
          <Row style={commonStyle.headerSubComponent}>
            <Text style={commonStyle.normalTextStyle}>
              {I18n.t('tax_free.input_seller_info')}
            </Text>
          </Row>
          <Row style={commonStyle.column}>
            <ScrollView>
              <RowLayout title={I18n.t('tax_free.jurisdiction_tax_office')}>
                <View style={componentStyles.jurisdictionTaxOfficeContainer}>
                  <TextInput
                    placeholder={I18n.t('tax_free.jurisdiction_tax_office')}
                    style={[commonStyle.textInput, componentStyles.textInput]}
                    onChangeText={text =>
                      this.props.onChangedForm(
                        FormGroup.JURISDICTION_TAX_OFFICE,
                        text
                      )}
                    value={this.props.jurisdictionTaxOffice}
                    maxLength={50}
                  />
                </View>
              </RowLayout>
              <RowLayout title={I18n.t('tax_free.place_of_tax_payment')}>
                <TextInput
                  placeholder={I18n.t('tax_free.place_of_tax_payment')}
                  style={[commonStyle.textInput, componentStyles.textInput]}
                  onChangeText={text =>
                    this.props.onChangedForm(
                      FormGroup.PLACE_OF_TAX_PAYMENT,
                      text
                    )}
                  value={this.props.placeOfTaxPayment}
                  maxLength={50}
                />
              </RowLayout>
              <RowLayout title={I18n.t('tax_free.seller_location')}>
                <TextInput
                  placeholder={I18n.t('tax_free.seller_location')}
                  style={[commonStyle.textInput, componentStyles.textInput]}
                  onChangeText={text =>
                    this.props.onChangedForm(FormGroup.SELLER_LOCATION, text)}
                  value={this.props.sellerLocation}
                  maxLength={50}
                />
              </RowLayout>
              <RowLayout title={I18n.t('tax_free.seller_name_nick_name')}>
                <TextInput
                  placeholder={I18n.t('tax_free.seller_name_nick_name')}
                  style={[commonStyle.textInput, componentStyles.textInput]}
                  onChangeText={text =>
                    this.props.onChangedForm(
                      FormGroup.SELLER_NAME_NICK_NAME,
                      text
                    )}
                  value={this.props.sellerNameNickName}
                  maxLength={50}
                />
              </RowLayout>
            </ScrollView>
            <View style={commonStyle.twoButtonContainer}>
              <View style={commonStyle.rightButton}>
                <StandardButton
                  isBorderButton={false}
                  textStyle={commonStyle.textButtonNormal}
                  onPress={() => {
                    this.props.onSubmit(this.props)
                  }}
                  text={I18n.t('tax_free.confirm')}
                  disabled={!this.props.canComplete}
                />
              </View>
            </View>
          </Row>
        </View>
      </Grid>
    )
  }
}
