import { connect } from 'react-redux'
import TaxFreeCustomerForm from '../components/TaxFreeCustomerForm'
import {
  MODULE_NAME,
  CustomerFormGroup as FormGroup,
  canCompleteCustomerForm
} from '../models'
import * as actions from '../actions'
import AlertView from '../../../common/components/widgets/AlertView'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import CashierSettingRepository from '../../setting/repositories/CashierSettingRepository'
import ShopRepository from '../../shop/repositories/ShopRepository'

const mapDispatchToProps = dispatch => ({
  onChangedForm: async (name, value) => {
    switch (name) {
      case FormGroup.TYPE_OF_PASSPORT:
        dispatch(actions.updateTypeOfPassport(value))
        break
      case FormGroup.PASSPORT_NUMBER:
        dispatch(actions.updatePassportNumber(value))
        break
      case FormGroup.CUSTOMER_NAME:
        dispatch(actions.updateCustomerName(value))
        break
      case FormGroup.BIRTHDAY:
        dispatch(actions.updateBirthday(value))
        break
      case FormGroup.COUNTRY_OF_CITIZENSHIP:
        const selectedValue = value.value ? value : null
        dispatch(actions.updateCountryOfCitizenship(selectedValue))
        break
      case FormGroup.LANDED_AT:
        dispatch(actions.updateLandedAt(value))
        break
      case FormGroup.STATUS_OF_RESIDENCE:
        dispatch(actions.updateResidenceStatus(value))
        break
      case FormGroup.OTHER_STATUS_OF_RESIDENCE:
        dispatch(actions.updateOtherResidenceStatus(value))
        break
      default:
        throw new Error('Form group not defined.')
    }
  },
  onSubmit: async props => {
    /** @type {CashierSettingEntity} */
    const cashierSetting = await CashierSettingRepository.find()
    /** @type {ShopEntity} */
    const shop = await ShopRepository.find()

    dispatch(actions.updateJurisdictionTaxOffice(cashierSetting.tax_office))
    dispatch(actions.updatePlaceOfTaxPayment(cashierSetting.tax_payment_place))
    dispatch(actions.updateSellerLocation(
      shop.region + shop.locality + shop.street))
    dispatch(actions.updateSellerNameNickName(
      cashierSetting.company_name + ' ' + shop.name))

    const validate = canCompleteCustomerForm(props)
    if (validate.result) {
      Actions.taxFreeConfirm({ order: props.order })
    } else {
      AlertView.show(
        validate.error
          .map(e =>
            e.message
          )
          .join('\n'),
        null
      )
    }
  }
})

const mapStateToProps = state => ({
  typeOfPassport: state[MODULE_NAME].typeOfPassport,
  passportNumber: state[MODULE_NAME].passportNumber,
  customerName: state[MODULE_NAME].customerName,
  birthday: state[MODULE_NAME].birthday,
  countryOfCitizenship: state[MODULE_NAME].countryOfCitizenship,
  landedAt: state[MODULE_NAME].landedAt,
  residenceStatus: state[MODULE_NAME].residenceStatus,
  otherResidenceStatus: state[MODULE_NAME].otherResidenceStatus,
  countries: state[MODULE_NAME].countries,
})

export default connect(mapStateToProps, mapDispatchToProps)(TaxFreeCustomerForm)
