import { connect } from 'react-redux'
import TaxFreeSellerForm from '../components/TaxFreeSellerForm'
import { MODULE_NAME, SellerFormGroup as FormGroup, canCompleteSellerForm } from '../models'
import * as actions from '../actions'
import AlertView from '../../../common/components/widgets/AlertView'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'

const mapDispatchToProps = dispatch => ({
  onChangedForm: async (name, value) => {
    switch (name) {
      case FormGroup.JURISDICTION_TAX_OFFICE:
        dispatch(actions.updateJurisdictionTaxOffice(value))
        break
      case FormGroup.PLACE_OF_TAX_PAYMENT:
        dispatch(actions.updatePlaceOfTaxPayment(value))
        break
      case FormGroup.SELLER_LOCATION:
        dispatch(actions.updateSellerLocation(value))
        break
      case FormGroup.SELLER_NAME_NICK_NAME:
        dispatch(actions.updateSellerNameNickName(value))
        break
      default:
        throw new Error('Form group not defined.')
    }
  },
  onSubmit: async props => {
    const validate = canCompleteSellerForm(props)
    if (validate.result) {
      Actions.taxFreeConfirm({order: props.order})
    } else {
      AlertView.show(
        validate.error
          .map(e =>
            I18n.t('message.K-02-E001', {
              item: I18n.t(e.item),
              format: I18n.t(e.format)
            })
          )
          .join('\n'),
        null
      )
    }
  }
})

const mapStateToProps = state => ({
  jurisdictionTaxOffice: state[MODULE_NAME].jurisdictionTaxOffice,
  placeOfTaxPayment: state[MODULE_NAME].placeOfTaxPayment,
  sellerLocation: state[MODULE_NAME].sellerLocation,
  sellerNameNickName: state[MODULE_NAME].sellerNameNickName,
  canComplete: canCompleteSellerForm(state[MODULE_NAME]).result
})

export default connect(mapStateToProps, mapDispatchToProps)(TaxFreeSellerForm)
