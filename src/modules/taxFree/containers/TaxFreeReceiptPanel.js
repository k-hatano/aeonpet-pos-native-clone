import { connect } from 'react-redux'
import TaxFreeReceiptPanel from '../components/TaxFreeReceiptPanel'
import { MODULE_NAME, makeReprintTaxFreeEJournal } from '../models'
import * as actions from '../actions'
import { Actions } from 'react-native-router-flux'
import Modal from '../../../common/components/widgets/Modal'
import AlertView from '../../../common/components/widgets/AlertView'
import EJournalRepository from '../../printer/repositories/EJournalRepository'
import PrinterManager from '../../printer/models/PrinterManager'
import { loading } from 'common/sideEffects'
import I18n from 'i18n-js'
import { RECEIPT_TYPE } from '../../printer/models'
import ReprintOrderReceiptBuilder from '../../order/models/ReprintOrderReceiptBuilder'

const mapDispatchToProps = dispatch => ({
  onPrintSales: async (ejournal) => {
    Modal.close()
    try {
      await loading(dispatch, async () => {
        const builder = new ReprintOrderReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournal.data), ejournal.is_reprint === 0)
        await PrinterManager.print(receipt)
        const reprintEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_sales'), RECEIPT_TYPE.TAXFREE_SALES, ejournal, receipt, new Date())
        await EJournalRepository.save(reprintEjournal)
        await EJournalRepository.pushById(reprintEjournal.id)
        dispatch(actions.clearTaxFree())
      })
    } catch (error) {
      AlertView.show(
        I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_sales')
        })
      )
    }
  },
  onPrintCustomer: async (ejournal) => {
    Modal.close()
    try {
      await loading(dispatch, async () => {
        const builder = new ReprintOrderReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournal.data), ejournal.is_reprint === 0)
        await PrinterManager.print(receipt)
        const reprintEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_customer'), RECEIPT_TYPE.TAXFREE_CUSTOMER, ejournal, receipt, new Date())
        await EJournalRepository.save(reprintEjournal)
        await EJournalRepository.pushById(reprintEjournal.id)
        dispatch(actions.clearTaxFree())
      })
    } catch (error) {
      AlertView.show(
        I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_customer')
        })
      )
    }
  },
  onPrintShipping: async (ejournal) => {
    Modal.close()
    try {
      await loading(dispatch, async () => {
        const builder = new ReprintOrderReceiptBuilder()
        await builder.initializeAsync()
        const receipt = builder.buildReprintOrderReceipt(JSON.parse(ejournal.data), ejournal.is_reprint === 0)
        await PrinterManager.print(receipt)
        const reprintEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_shipping'), RECEIPT_TYPE.TAXFREE_SHIPPING, ejournal, receipt, new Date())
        await EJournalRepository.save(reprintEjournal)
        await EJournalRepository.pushById(reprintEjournal.id)
        dispatch(actions.clearTaxFree())
      })
    } catch (error) {
      AlertView.show(
        I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_shipping')
        })
      )
    }
  },
  onPrintAll: async (salesEjournal, customerEjournal, shippingEjournal) => {
    Modal.close()
    try {
      await loading(dispatch, async () => {
        const salesBuilder = new ReprintOrderReceiptBuilder()
        await salesBuilder.initializeAsync()
        const salesReceipt = salesBuilder.buildReprintOrderReceipt(JSON.parse(salesEjournal.data), salesEjournal.is_reprint === 0)
        await PrinterManager.print(salesReceipt)
        const reprintSalesEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_sales'), RECEIPT_TYPE.TAXFREE_SALES, salesEjournal, salesReceipt, new Date())
        await EJournalRepository.save(reprintSalesEjournal)
        await EJournalRepository.pushById(reprintSalesEjournal.id)
      })
    } catch (error) {
      AlertView.show(
        I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_sales')
        })
      )
    }
    try {
      await loading(dispatch, async () => {
        const customerBuilder = new ReprintOrderReceiptBuilder()
        await customerBuilder.initializeAsync()
        const customerReceipt = customerBuilder.buildReprintOrderReceipt(JSON.parse(customerEjournal.data), customerEjournal.is_reprint === 0)
        await PrinterManager.print(customerReceipt)
        const reprintCustomerEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_customer'), RECEIPT_TYPE.TAXFREE_CUSTOMER, customerEjournal, customerReceipt, new Date())
        await EJournalRepository.save(reprintCustomerEjournal)
        await EJournalRepository.pushById(reprintCustomerEjournal.id)
      })
    } catch (error) {
      AlertView.show(
        I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_customer')
        })
      )
    }
    if (shippingEjournal) {
      try {
        await loading(dispatch, async () => {
          const shippingBuilder = new ReprintOrderReceiptBuilder()
          await shippingBuilder.initializeAsync()
          const shippingReceipt = shippingBuilder.buildReprintOrderReceipt(JSON.parse(shippingEjournal.data), shippingEjournal.is_reprint === 0)
          await PrinterManager.print(shippingReceipt)
          const reprintShippingEjournal = makeReprintTaxFreeEJournal(I18n.t('receipt.title.tax_free_shipping'), RECEIPT_TYPE.TAXFREE_SHIPPING, shippingEjournal, shippingReceipt, new Date())
          await EJournalRepository.save(reprintShippingEjournal)
          await EJournalRepository.pushById(reprintShippingEjournal.id)
        })
      } catch (error) {
        AlertView.show(
          I18n.t('message.K-03-E002', {
            receipt: I18n.t('receipt.title.tax_free_shipping')
          })
        )
      }
    }
    dispatch(actions.clearTaxFree())
  },
  onClose: async () => {
    dispatch(actions.clearTaxFree())
    Modal.close()
  }
})

const mapStateToProps = state => ({
  ejournals: state[MODULE_NAME].ejournals
})

const TaxFreeReceiptPanelViewModal = connect(mapStateToProps, mapDispatchToProps)(TaxFreeReceiptPanel)
export default TaxFreeReceiptPanelViewModal

export const TaxFreeReceiptPanelModal = (salesEjournals, customerEjournals, shippingEjournals) => {
  Modal.open(TaxFreeReceiptPanelViewModal, {
    enableBackgroundClose: false,
    isBackgroundVisible: true,
    props: {
      salesEjournals: salesEjournals,
      customerEjournals: customerEjournals,
      shippingEjournals: shippingEjournals
    }
  })
}
