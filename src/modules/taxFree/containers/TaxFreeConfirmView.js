import { connect } from 'react-redux'
import TaxFreeConfirmView from '../components/TaxFreeConfirmView'
import { MODULE_NAME } from '../models'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  typeOfPassport: state[MODULE_NAME].typeOfPassport,
  passportNumber: state[MODULE_NAME].passportNumber,
  customerName: state[MODULE_NAME].customerName,
  birthday: state[MODULE_NAME].birthday,
  countryOfCitizenship: state[MODULE_NAME].countryOfCitizenship,
  landedAt: state[MODULE_NAME].landedAt,
  residenceStatus: state[MODULE_NAME].residenceStatus,
  otherResidenceStatus: state[MODULE_NAME].otherResidenceStatus,
  jurisdictionTaxOffice: state[MODULE_NAME].jurisdictionTaxOffice,
  placeOfTaxPayment: state[MODULE_NAME].placeOfTaxPayment,
  sellerLocation: state[MODULE_NAME].sellerLocation,
  sellerNameNickName: state[MODULE_NAME].sellerNameNickName
})

export default connect(mapStateToProps, mapDispatchToProps)(TaxFreeConfirmView)
