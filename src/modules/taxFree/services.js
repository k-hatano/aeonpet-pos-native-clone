import CashierSettingRepository from '../setting/repositories/CashierSettingRepository'
import ShopRepository from '../shop/repositories/ShopRepository'
import Moment from 'moment'
import CountryRepository from 'modules/country/repositories/CountryRepository'
import OrderRepository from 'modules/order/repositories/OrderRepository'
import { STATUS } from 'modules/order/models'
import { RECEIPT_TYPE } from 'modules/printer/models'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'

export async function canCreateTaxfreeReceipt () {
  /** @type {CashierSettingEntity} */
  const cashierSetting = await CashierSettingRepository.find()
  /** @type {ShopEntity} */
  const shop = await ShopRepository.find()

  return !!cashierSetting.tax_office &&
    !!cashierSetting.tax_payment_place &&
    !!cashierSetting.company_name &&
    !!shop.region &&
    !!shop.locality &&
    !!shop.street
}

export async function setPassportInfo (passportCode) {
  let result = {}
  try {
    if (passportCode.length < 88 || passportCode.indexOf('<') === -1) {
      result = false
    } else {
      const mrp1 = passportCode.substring(2, 44).split('<').filter((str) => str !== '')
      const mrp2 = passportCode.substring(44)

      const passportNo = mrp2.substring(0, mrp2.substring(9, 1) === '<' ? 8 : 9)
      const countryCode = mrp2.substring(10, 13)
      const firstName = mrp1[0].substring(3)
      const lastName = mrp1.length === 3 ? mrp1[1] + ' ' + mrp1[2] : mrp1[1]
      const customertName = firstName + ' ' + lastName
      const year = mrp2.substring(13, 15)
      const month = mrp2.substring(15, 17)
      const day = mrp2.substring(17, 19)

      const nowYear = Moment(new Date()).format('YY')
      const pullYear = year <= nowYear ? '20' + year : '19' + year
      const birthDay = Moment(new Date(pullYear + '-' + month + '-' + day))
      const country = await CountryRepository.findByCountryCode(countryCode)

      result.passportNo = passportNo
      result.customertName = customertName
      result.birthDay = birthDay
      result.country = country
    }
  } catch (error) {
    result = false
  }
  return result
}

export async function fetchOrderStatus (orderId) {
  try {
    const order = await OrderRepository.fetchByOrderId(orderId)
    return order ? order.status : STATUS.CANCELED
  } catch (error) {
    // ネットワークエラーが出た場合は握りつぶして、免税帳票作成はOKにする。
    return STATUS.COMPLETED
  }
}

export async function fetchTaxFreeEJournals (order) {
  const orderNumber = order.pos_order.pos_order_number
  let salesEJournal = null
  let customerEJournal = null
  let shippingEJournal = null
  try {
    salesEJournal = await EJournalRepository.fetchByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_SALES)
    customerEJournal = await EJournalRepository.fetchByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_CUSTOMER)
    shippingEJournal = await EJournalRepository.fetchByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_SHIPPING)
  } catch (error) {
    // ネットワークエラーが出た場合は、取れていないものだけローカルから取得する。
    salesEJournal = salesEJournal || await EJournalRepository.findByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_SALES)
    customerEJournal = customerEJournal || await EJournalRepository.findByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_CUSTOMER)
    shippingEJournal = shippingEJournal || await EJournalRepository.findByOrderNumber(orderNumber, RECEIPT_TYPE.TAXFREE_SHIPPING)
  }
  return {
    sales: salesEJournal,
    customer: customerEJournal,
    shipping: shippingEJournal,
    isComplete: salesEJournal && salesEJournal.length > 0 && customerEJournal && customerEJournal.length > 0
  }
}
