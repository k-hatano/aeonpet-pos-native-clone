import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  //TaxFreeCustomerForm
  typeOfPassport: null,
  passportNumber: null,
  customerName: null,
  birthday: null,
  countryOfCitizenship: null,
  landedAt: null,
  residenceStatus: null,
  otherResidenceStatus: null,
  order: {},
  countries: [],
  canComplete: false,
  //TaxFreeSellerForm
  jurisdictionTaxOffice: null,
  placeOfTaxPayment: null,
  sellerLocation: null,
  sellerNameNickName: null,
  isPushed: null,
  isPrinted: null,
  isReceipt: false,
  ejournals: null
}

const handlers = {
  [actions.setOrder]: (state, action) => ({
    ...state,
    ...{ order: action.payload }
  }),
  //CustomerInfo
  [actions.listCountries]: (state, action) => ({
    ...state,
    ...{ countries: action.payload }
  }),
  [actions.updateTypeOfPassport]: (state, action) => ({
    ...state,
    ...{ typeOfPassport: action.payload }
  }),
  [actions.updatePassportNumber]: (state, action) => ({
    ...state,
    ...{ passportNumber: action.payload }
  }),
  [actions.updateCustomerName]: (state, action) => ({
    ...state,
    ...{ customerName: action.payload }
  }),
  [actions.updateBirthday]: (state, action) => ({
    ...state,
    ...{ birthday: action.payload }
  }),
  [actions.updateCountryOfCitizenship]: (state, action) => ({
    ...state,
    ...{ countryOfCitizenship: action.payload }
  }),
  [actions.updateLandedAt]: (state, action) => ({
    ...state,
    ...{ landedAt: action.payload }
  }),
  [actions.updateResidenceStatus]: (state, action) => ({
    ...state,
    ...{ residenceStatus: action.payload }
  }),
  [actions.updateOtherResidenceStatus]: (state, action) => ({
    ...state,
    ...{ otherResidenceStatus: action.payload }
  }),
  //SellerInfo
  [actions.updateJurisdictionTaxOffice]: (state, action) => ({
    ...state,
    ...{ jurisdictionTaxOffice: action.payload }
  }),
  [actions.updatePlaceOfTaxPayment]: (state, action) => ({
    ...state,
    ...{ placeOfTaxPayment: action.payload }
  }),
  [actions.updateSellerLocation]: (state, action) => ({
    ...state,
    ...{ sellerLocation: action.payload }
  }),
  [actions.updateSellerNameNickName]: (state, action) => ({
    ...state,
    ...{ sellerNameNickName: action.payload }
  }),
  [actions.setEJournals]: (state, action) => ({
    ...state,
    ...{ ejournals: action.payload }
  }),
  [actions.updateIsPushed]: (state, action) => ({
    ...state,
    ...{ isPushed: action.payload }
  }),
  [actions.updateIsPrinted]: (state, action) => ({
    ...state,
    ...{ isPrinted: action.payload }
  }),
  [actions.clearTaxFree]: (state, action) => ({
    ...defaultState
  })
}

export default handleActions(handlers, defaultState)
