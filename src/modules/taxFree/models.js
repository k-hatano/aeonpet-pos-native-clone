import I18n from 'i18n-js'
import * as actions from './actions'
import CountryRepository from '../country/repositories/CountryRepository'
import ProductRepository from '../product/repositories/ProductRepository'
import * as Validator from '../../common/models/Validator'
import { getSettingFromState } from 'modules/setting/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { makeBaseEJournal } from '../printer/models'
import ShopRepository from '../shop/repositories/ShopRepository'
import { TAXFREE_TYPE } from '../../common/models/Tax'
import Decimal from 'decimal.js'

export const MODULE_NAME = 'taxFree'

export const PASSPORT_TYPE = {
  PASSPORT: 1,
  SHIP_SIGHTSHEENG_LANDING_PERMISSION: 2,
  CREW_LANDING_PERMISSION: 3,
  EMERGENCY_LANDING_PERMISSION: 4,
  DISTRESS_LANDING_PERMISSION: 5
}

export const RESIDENCE_TYPE = {
  SHORT_TERM_STAY: 1,
  STUDY_ABROAD: 2,
  ENROLLMENT: 3,
  ENTERTAINMENT: 4,
  CULTURAL_ACTIVITIES: 5,
  OTHER: 6
}

export const CustomerFormGroup = {
  TYPE_OF_PASSPORT: 'type_of_passport',
  STATUS_OF_RESIDENCE: 'residence_status',
  PASSPORT_NUMBER: 'passport_number',
  CUSTOMER_NAME: 'customer_name',
  BIRTHDAY: 'birthday',
  COUNTRY_OF_CITIZENSHIP: 'country_of_citizenship',
  LANDED_AT: 'landed_at',
  OTHER_STATUS_OF_RESIDENCE: 'other_of_residence'
}

export const SellerFormGroup = {
  JURISDICTION_TAX_OFFICE: 'jurisdiction_tax_office',
  PLACE_OF_TAX_PAYMENT: 'place_of_tax_payment',
  SELLER_LOCATION: 'seller_location',
  SELLER_NAME_NICK_NAME: 'seller_name_nick_name'
}

export const passportTypeToLabel = value => {
  switch (value) {
    case PASSPORT_TYPE.PASSPORT:
      return I18n.t('tax_free.passport')
    case PASSPORT_TYPE.SHIP_SIGHTSHEENG_LANDING_PERMISSION:
      return I18n.t('tax_free.ship_sightseeing_landing_permission')
    case PASSPORT_TYPE.CREW_LANDING_PERMISSION:
      return I18n.t('tax_free.crew_landing_permission')
    case PASSPORT_TYPE.EMERGENCY_LANDING_PERMISSION:
      return I18n.t('tax_free.emergency_landing_permission')
    case PASSPORT_TYPE.DISTRESS_LANDING_PERMISSION:
      return I18n.t('tax_free.distress_landing_permission')
  }
}

export const residenceStatusToLabel = value => {
  switch (value) {
    case RESIDENCE_TYPE.SHORT_TERM_STAY:
      return I18n.t('tax_free.short_term_stay')
    case RESIDENCE_TYPE.STUDY_ABROAD:
      return I18n.t('tax_free.study_abroad')
    case RESIDENCE_TYPE.ENROLLMENT:
      return I18n.t('tax_free.enrollment')
    case RESIDENCE_TYPE.ENTERTAINMENT:
      return I18n.t('tax_free.entertainment')
    case RESIDENCE_TYPE.CULTURAL_ACTIVITIES:
      return I18n.t('tax_free.cultural_activities')
    case RESIDENCE_TYPE.OTHER:
      return I18n.t('tax_free.others')
  }
}

export const getResidenceStatusOption = () => {
  return Object.keys(RESIDENCE_TYPE).map(type => {
    return {
      label: residenceStatusToLabel(RESIDENCE_TYPE[type]),
      value: RESIDENCE_TYPE[type],
      selected: RESIDENCE_TYPE[type] === RESIDENCE_TYPE.SHORT_TERM_STAY
    }
  })
}

export const getPassportTypeOption = () => {
  return Object.keys(PASSPORT_TYPE).map(type => {
    return {
      label: passportTypeToLabel(PASSPORT_TYPE[type]),
      value: PASSPORT_TYPE[type],
      selected: PASSPORT_TYPE[type] === PASSPORT_TYPE.PASSPORT
    }
  })
}

export const canCompleteCustomerForm = state => {
  const messages = []

  if (Validator.isNullOrEmpty(state.typeOfPassport)) {
    messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.type_of_passport')})})
  }

  if (!Validator.isHalfWidthAlphaNumeric(state.passportNumber, true)) {
    messages.push({
      message: '・' + I18n.t(
        'message.K-01-E002',
        {item: I18n.t('tax_free.passport_number'),format: I18n.t('validation.halfWidth')}) + '\n　' + I18n.t('validation.notAllowSpace')
    })
  }
  if (Validator.isNullOrEmpty(state.customerName) || !(state.customerName).match(/^[a-zA-Z\s]+$/)) {
    messages.push({
      message: '・' + I18n.t(
        'message.K-01-E002',
        {item: I18n.t('tax_free.customer_name'),format: I18n.t('validation.halfWidthAlpha')}) + '\n　' + I18n.t('validation.allowSpace')
    })
  }
  if (Validator.isNullOrEmpty(state.countryOfCitizenship)) {
    messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.country_of_citizenship')})})
  }
  if (Validator.isNullOrEmpty(state.birthday)) {
    messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.birthday')})})
  }
  if (Validator.isNullOrEmpty(state.landedAt)) {
    messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.landed_at')})})
  }
  if (Validator.isNullOrEmpty(state.residenceStatus)) {
    messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.residence_status')})})
  } else if (
    state.residenceStatus === RESIDENCE_TYPE.OTHER
  ) {
    if (Validator.isNullOrEmpty(state.otherResidenceStatus)) {
      messages.push({message: '・' + I18n.t('message.K-02-E001', {item: I18n.t('tax_free.other_of_residence')})})
    }
  }

  const result =
    messages.length > 0 ? { result: false, error: messages } : { result: true }

  return result
}

export const canCompleteSellerForm = state => {
  const messages = []

  if (Validator.isNullOrEmpty(state.jurisdictionTaxOffice)) {
    messages.push({
      item: 'tax_free.jurisdiction_tax_office',
      format: 'validation.nullOrEmpty'
    })
  }
  if (Validator.isNullOrEmpty(state.placeOfTaxPayment)) {
    messages.push({
      item: 'tax_free.place_of_tax_payment',
      format: 'validation.nullOrEmpty'
    })
  }
  if (Validator.isNullOrEmpty(state.sellerLocation)) {
    messages.push({
      item: 'tax_free.seller_location',
      format: 'validation.nullOrEmpty'
    })
  }
  if (Validator.isNullOrEmpty(state.sellerNameNickName)) {
    messages.push({
      item: 'tax_free.seller_name_nick_name',
      format: 'validation.nullOrEmpty'
    })
  }

  const result =
    messages.length > 0 ? { result: false, error: messages } : { result: true }

  return result
}

export const initForTaxFreeCustomerForm = async (dispatch) => {
  // 国情報取得
  const _countries = await CountryRepository.findAll()
  const countries = await _countries.map(country => {
    return {
      label: country.country_code,
      value: country.id
    }
  })

  dispatch(actions.listCountries(countries))
}

export const initForTaxFreeSellerForm = async dispatch => {
  const shop = await ShopRepository.find()

  if (shop) {
    dispatch(actions.updateJurisdictionTaxOffice(shop.tax_office))
    dispatch(actions.updatePlaceOfTaxPayment(shop.tax_payment_place))
    dispatch(actions.updateSellerLocation(shop.seller_address))
    dispatch(actions.updateSellerNameNickName(shop.seller_name))
  }
}

export const makeTaxFreeEJournal = (
  title,
  type,
  receipt,
  taxFree,
  order,
  createdAt
) => {
  const ejournal = makeBaseEJournal(title, receipt, type, createdAt)
  ejournal.order_id = order.id
  ejournal.currency = taxFree.currency
  ejournal.cashier_id = order.pos_order.cashier_id
  ejournal.staff_id = order.pos_order.staff_id
  ejournal.amount = order.pos_order.total_expendable_items_taxless
  ejournal.pos_order_number = order.pos_order.pos_order_number
  ejournal.is_print_tax_stamp = 0

  return ejournal
}

export const makeReprintTaxFreeEJournal = (title, type, originEjournal, receipt, createdAt) => {
  const ejournal = makeBaseEJournal(title, receipt, type, createdAt)
  ejournal.is_printed = 1
  ejournal.is_reprint = 1
  ejournal.order_id = originEjournal.order_id
  ejournal.currency = originEjournal.currency
  ejournal.cashier_id = originEjournal.cashier_id
  ejournal.staff_id = originEjournal.staff_id
  ejournal.amount = originEjournal.amount
  ejournal.pos_order_number = originEjournal.pos_order_number
  ejournal.is_print_tax_stamp = originEjournal.is_print_tax_stamp

  return ejournal
}

export const makeTaxFreeForState = state => {
  return {
    ...state[MODULE_NAME],
    currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY),
    cashier_id: getSettingFromState(state, SettingKeys.COMMON.CASHIER_ID),
    device_id: getSettingFromState(state, SettingKeys.COMMON.DEVICE_ID),
    shop_id: getSettingFromState(state, SettingKeys.COMMON.SHOP_ID),
    staff_id: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF).id
  }
}

export const makeOrderForTaxFree = async order => {
  let orderGeneralProducts = []
  let orderComsumableProducts = []
  for (let orderItem of order.order_items) {
    const product = await ProductRepository.findWithDetail(orderItem.product_id)

    switch (product.taxfree_type) {
      case TAXFREE_TYPE.GENERAL:
        if (order.pos_order.is_general_items_taxfree) {
          orderGeneralProducts.push({ ...orderItem, product })
        }
        break
      case TAXFREE_TYPE.EXPENDABLE:
        if (order.pos_order.is_expendable_items_taxfree) {
          orderComsumableProducts.push({ ...orderItem, product })
        }
        break
    }
  }

  return {
    ...order,
    order_general_products: orderGeneralProducts,
    order_comsumable_products: orderComsumableProducts
  }
}
