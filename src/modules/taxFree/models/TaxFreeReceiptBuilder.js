// import I18n from 'i18n-js'
import { RECEIPT_TYPE } from '../../printer/models'
import { passportTypeToLabel, residenceStatusToLabel } from '../models'
import * as CovenantOfPurchaserReceipt from './receipt/CovenantOfPurchaserReceipt'
import * as PackingList from './receipt/PackingList'
import * as RecordOfPurchaseOfConsumptionTaxFreeForExportReceipt from './receipt/RecordOfPurchaseOfConsumptionTaxFreeForExportReceipt'
import moment from 'moment'
import OrderedCart from '../../order/models/OrderedCart'
import * as _ from 'underscore'
import { TAXFREE_TYPE } from '../../../common/models/Tax'
import CartReceiptBuilderBase from '../../cart/models/CartReceiptBuilderBase'

export default class TaxFreeReceiptBuilder extends CartReceiptBuilderBase {
  buildTaxFreeReceipt (taxFreeType, taxFree, order, createdAt, isTraining) {
    switch (taxFreeType) {
      case RECEIPT_TYPE.TAXFREE_SALES:
        this.receipt = RecordOfPurchaseOfConsumptionTaxFreeForExportReceipt
        this.title = '輸出免税物品購入記録票\nRecord of Purchase of Consumption\nTax-Exempt for Export'
        this.signature = false
        this.consumableOnly = false
        this.subtitle = 'パスポート貼付用'
        break
      case RECEIPT_TYPE.TAXFREE_CUSTOMER:
        this.receipt = CovenantOfPurchaserReceipt
        this.title = '最終的に輸出となる物品の消費税免税について\nの購入者誓約書\nCovenant of Purchaser of Consumption Tax of\nPurchaser of Consumption Tax-Exempt of\nUltimate Export'
        this.signature = true
        this.consumableOnly = false
        this.subtitle = '店舗控え'
        break
      case RECEIPT_TYPE.TAXFREE_SHIPPING:
        this.receipt = PackingList
        this.title = ''
        this.signature = false
        this.consumableOnly = true
        this.subtitle = '消耗品梱包貼付用'
        break
      default:
        throw new Error('receipt type not defined.')
    }

    return this._buildTaxFreeReceipt(taxFree, order, createdAt, isTraining)
  }

  _buildTaxFreeReceipt (taxFree, order, createdAt, isTraining) {
    const orderedCart = new OrderedCart(order).setIsTrainingMode(isTraining)
    return {
      content: _.compact([
        {
          element: 'text',
          align: 'right',
          text: this.subtitle
        },
        this.feedElement(1),
        this._trainingModeTitle(orderedCart),
        this.titleElement(this.title),
        this.feedElement(1),
        ...this.fixedElement(),
        ...((this.consumableOnly) ? [] : this.infoElement(taxFree)),
        ...this.purshaseDateElement(order.pos_order.client_created_at),
        ...((this.consumableOnly || order.order_general_products.length === 0)
          ? [] : this.generalElement(order, orderedCart, taxFree.currency)),
        ...((order.order_comsumable_products.length === 0)
          ? [] : this.consumableElement(order, orderedCart, taxFree.currency)),
        {
          element: 'text',
          align: 'right',
          text: '取引ID:' + (order.pos_order.pos_order_number || '')
        },
        this.cutElement()
      ]),
      check_error_strictly: true
    }
  }

  fixedElement () {
    return [
      ...this.receipt.fixedElement(),
      ...(this.signature ? this.signatureElement() : this.feedElement(1)),
      this.textLineElement(),
      this.feedElement(1)
    ]
  }

  generalElement (order, cart, currency) {
    return [
      {
        element: 'text',
        align: 'left',
        text: '一般物品/Commodities except consumables'
      },
      this.feedElement(1),
      // バンドル一覧 bundles
      ...this.bundlesElement(cart.appliedBundles, currency, TAXFREE_TYPE.GENERAL),
      // 明細一覧(バンドルを除く) Items(not bundled)
      ..._.flatten(cart.items
        .filter(item => !item.isBundled && item.product.taxfreeType === TAXFREE_TYPE.GENERAL)
        .map(item => this.cartItemElement(item))
        .toArray()),
      {
        element: 'combine_left_right',
        left: '合計価額/Total amount',
        right: this.formatMoney(order.pos_order.total_general_items_taxless || 0, currency)
      },
      {
        element: 'combine_left_right',
        left: '免税額/Exemption amount',
        right: this.formatMoney(order.pos_order.total_general_items_tax || 0, currency)
      },
      this.feedElement(1),
      this.textLineElement(),
      this.feedElement(1)
    ]
  }

  consumableElement (order, cart, currency) {
    return [
      {
        element: 'text',
        align: 'left',
        text: '消耗品/Consumable Commodities'
      },
      this.feedElement(1),
      // バンドル一覧 bundles
      ...this.bundlesElement(cart.appliedBundles, currency, TAXFREE_TYPE.EXPENDABLE),
      // 明細一覧(バンドルを除く) Items(not bundled)
      ..._.flatten(cart.items
        .filter(item => !item.isBundled && item.product.taxfreeType === TAXFREE_TYPE.EXPENDABLE)
        .map(item => this.cartItemElement(item))
        .toArray()),
      {
        element: 'combine_left_right',
        left: '合計価額/Total amount',
        right: this.formatMoney(order.pos_order.total_expendable_items_taxless || 0, currency)
      },
      {
        element: 'combine_left_right',
        left: '免税額/Exemption amount',
        right: this.formatMoney(order.pos_order.total_expendable_items_tax || 0, currency)
      },
      this.feedElement(1),
      this.textLineElement(),
      this.feedElement(1)
    ]
  }

  infoElement (taxFree) {
    return [
      {
        element: 'text',
        align: 'left',
        text: '[所轄税務署/Tax office concerned]\n' + taxFree.jurisdictionTaxOffice
      },
      {
        element: 'text',
        align: 'left',
        text: '[納税地/Place for Tax Payment]\n' + taxFree.placeOfTaxPayment
      },
      {
        element: 'text',
        align: 'left',
        text: '[販売者所在地/Selling Place]\n' + taxFree.sellerLocation
      },
      {
        element: 'text',
        align: 'left',
        text: "[販売者氏名・名称/Seller's Name]\n" + taxFree.sellerNameNickName
      },
      this.feedElement(1),
      this.textLineElement(),
      this.feedElement(1),
      {
        element: 'text',
        align: 'left',
        text:
          '[旅券等の種類/Passport etc.]\n' +
          passportTypeToLabel(taxFree.typeOfPassport)
      },
      {
        element: 'text',
        align: 'left',
        text: '[番号/No.]\n' + taxFree.passportNumber
      },
      {
        element: 'text',
        align: 'left',
        text: '[国籍/Nationality]\n' + taxFree.countryOfCitizenship.label
      },
      {
        element: 'text',
        align: 'left',
        text: '[購入者氏名/Name in Full(in block Letters)]\n' + taxFree.customerName
      },
      {
        element: 'text',
        align: 'left',
        text:
          '[生年月日/Date of Birth of Purchaser]\n' +
          taxFree.birthday.format('YYYY-MM-DD')
      },
      {
        element: 'text',
        align: 'left',
        text:
          '[在留資格/Status of Residence]\n' +
          residenceStatusToLabel(taxFree.residenceStatus) +
          ' ' +
          (taxFree.otherResidenceStatus || '')
      },
      {
        element: 'text',
        align: 'left',
        text:
          '[上陸年月日/Date of Landing]\n' + taxFree.landedAt.format('YYYY-MM-DD')
      },
      this.feedElement(1),
      this.textLineElement(),
      this.feedElement(1)
    ]
  }

  purshaseDateElement (clientCreatedAt) {
    return [
      {
        element: 'text',
        align: 'left',
        text: '[購入年月日/Date of Purchase]\n' + moment.unix(clientCreatedAt).format('YYYY-MM-DD')
      },
      this.feedElement(1)
    ]
  }

  signatureElement () {
    return [
      {
        element: 'text',
        align: 'left',
        text: '[署名/Signature]'
      },
      this.feedElement(4)
    ]
  }
}
