import * as _ from 'underscore'

export const fixedElement1 = [
  '・当該消耗品を、購入した日から30日以内に輸出される' +
  'ものとして購入し、日本で処分しないことを誓約します',
  'I certify that the goods listed as “consumable ' +
  'Commodities ” on this card were purchased by me ' +
  'for export from Japan within 30days ' +
  'from the purchase date and ' +
  'will not be disposed of within Japan.',
  ''
]

export const fixedElement2 = [
  '・当該一般物品を、日本から最終的には輸出されるものとして購入し、' +
  '日本で処分しないことを誓約します。',
  'I certify that the goods listed as “commodities ' +
  'except consumables ” on this card were purchased ' +
  'by me for ultimate export from Japan and ' +
  'will not be disposed of within Japan.',
  ''
]

export const fixedElement = () => {
  return _.chain([fixedElement1, fixedElement2])
    .flatten()
    .map(element => ({
      element: 'text',
      align: 'left',
      text: element
    }))
    .value()
}
