import * as _ from 'underscore'

export const fixedElement1 = [
  '日本を出国するまで、開封しないでください。' +
  'なお、消費した場合には、消費税を徴収されます。 ',
  'Do not open the packaging until you have ' +
  'left Japan. Please note that if you consume ' +
  'this product while in Japan, ' +
  'you may be subject to pay consumption tax.'
]

export const fixedElement = () => {
  return _.chain([fixedElement1])
    .flatten()
    .map(element => ({
      element: 'text',
      align: 'left',
      text: element
    }))
    .value()
}
