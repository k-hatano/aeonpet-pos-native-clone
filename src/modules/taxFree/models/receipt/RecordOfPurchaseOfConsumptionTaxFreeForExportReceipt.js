import * as _ from 'underscore'

const fixedElement1 = [
  '1) 本邦から出国する際又は居住者となる際に、その' +
  '出港地を所轄する税関長又はその住所若しくは居所の所' +
  '在地を所轄する税務署長に購入記録票を提出しなけれ' +
  'ばなりません。',
  '1) When departing Japan, or if becoming a resident of ' +
  'Japan, you are required to submit your ”Record of ' +
  'Purchase Card” to either the Director of Customs that ' +
  'has jurisdiction over your departure location or the ' +
  'head of the tax office that has jurisdiction over your ' +
  'place of residence or address.',
  ''
]

const fixedElement2 = [
  '2) 本邦から出国するまでは購入記録票を旅券等から' +
  '切り離してはいけません。',
  '2) You must not remove the ”Record of Purchase ' +
  'Card” from your passport etc. until after you have ' +
  'departed Japan.',
  ''
]

const fixedElement3 = [
  '3) 免税で購入した物品を本邦からの出国の際に所持し' +
  'ていなかった場合には、その購入した物品について免' +
  '除された消費税額（地方消費税を含む。）に相当する' +
  '額を徴収されます。',
  '3) If you are not in possession of item(s) ' +
  'purchased tax free, that are listed on ' +
  'the “Record of Purchase Card”, at the ' +
  'time of departure from Japan, an amount ' +
  'equivalent to the consumption tax ' +
  'amount (including local consumption tax) ' +
  'that was exempted at the time of purchase ' +
  'will be collected before your departure ' +
  'from Japan.',
  ''
]

const fixedElement4 = [
  '4) 3)の場合において、災害その他やむを得ない事情に' +
  'より免税で購入した物品を亡失したため輸出しないこと' +
  'につき税関長の承認を受けたとき、又は既に輸出したこ' +
  'とを証する書類を出港地を所轄する税関長に提出したと' +
  'きは、消費税額（地方消費税を含む。）に相当する額を' +
  '徴収されません。',
  '4) In the case of 3) if you do not possess ' +
  'listed item(s) at the time of departure, ' +
  'if the Director of Customs has acknowledged ' +
  'acknowledged that item(s) item(s) you purchased ' +
  'purchased tax free will not be exported exported ' +
  'as a result of being lost in a disaster disaster ' +
  'or due to other unavoidable circumstances, or ' +
  'alternatively, if you have submitted documents ' +
  'to the Director of Customs that has jurisdiction ' +
  'over your departure location that verifies the item(s) ' +
  'has already been exported an amount equivalent to the ' +
  'consumption tax amount (including local consumption ' +
  'tax) will not be collected.',
  ''
]

export const fixedElement = () => {
  return _.chain([fixedElement1, fixedElement2, fixedElement3, fixedElement4])
    .flatten()
    .map(element => ({
      element: 'text',
      align: 'left',
      text: element
    }))
    .value()
}