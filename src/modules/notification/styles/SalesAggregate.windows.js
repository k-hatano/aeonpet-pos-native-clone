import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  salesAggregateContainer: {
    padding: 15,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
