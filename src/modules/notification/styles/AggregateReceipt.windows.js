import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 342,
    paddingRight: 1
  },
  receiptWrapper: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#979797'
  },
  dataAggregateContainer: {
    padding: 10,
    paddingTop: 0
  },
  headerContainer: {
    padding: 10
  },
  bodyContainer: {
    paddingTop: 10
  },
  footerContainer: {
    padding: 10
  },
  headerTitle: {
    textAlign: 'left',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerTitle2: {
    textAlign: 'center',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerTopItems: {
    textAlign: 'right',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerBottomItems: {
    textAlign: 'left',
    marginBottom: 10,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  bodyTitle: {
    paddingLeft: 5,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  bodyContentWrapper: {
    flexDirection: 'row'
  },
  bodyContentTitle: {
    textAlign: 'left',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  bodyContentValue: {
    textAlign: 'right',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  footerContentText: {
    textAlign: 'right',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  }
})
