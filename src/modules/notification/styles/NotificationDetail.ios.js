import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  detailWrapper: {
    flex: 7,
    justifyContent: 'center',
    padding: 10
  },
  bottomBar: {
    height: 104,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderColor: '#979797'
  },
  bottomBarLeftPanel: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  orangeBorderButton: {
    borderWidth: 3,
    borderColor: '#ff9024',
    borderRadius: 10,
    width: 138,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 24
  },
  orangeBorderButtonText: {
    fontSize: 24,
    color: '#ff9024',
    letterSpacing: -0.38
  },
  bottomBarRightPanel: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  orangeFilledButton: {
    backgroundColor: '#ff9024',
    borderRadius: 10,
    width: 138,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 24
  },
  orangeFilledButtonText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  }
})
