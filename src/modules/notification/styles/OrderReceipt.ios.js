import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 342,
    paddingRight: 1
  },
  receiptWrapper: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#979797'
  },
  dataAggregateContainer: {
    padding: 10,
    paddingTop: 0
  },
  headerContainer: {
    padding: 10
  },
  bodyContainer: {
    flexDirection: 'column'
  },
  bodyAmountContainer: {
    flexDirection: 'row'
  },
  footerContainer: {
    padding: 10,
    paddingBottom: 20
  },
  headerTitle: {
    textAlign: 'right',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerLogo: {
    marginBottom: 10,
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerTopItems: {
    textAlign: 'left',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerBottomItems: {
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  dataValue: {
    textAlign: 'center',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  dataValue4: {
    textAlign: 'right'
  },
  footerDataWrapper: {
    flexDirection: 'row'
  },
  footerNoteWrapper: {
    flexDirection: 'row',
    marginTop: 15
  },
  footerTitleText: {
    textAlign: 'left',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  footerContentText: {
    textAlign: 'right',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  separator: {
    marginTop: 10,
    marginBottom: 10,
    width: 240,
    alignSelf: 'center',
    alignItems: 'center'
  },
  footerConclusion1: {
    marginBottom: 10
  },
  footerConclusion5: {
    marginTop: 10
  }
})
