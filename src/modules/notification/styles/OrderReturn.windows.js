import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    width: 342,
    paddingRight: 1
  },
  receiptWrapper: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#979797'
  },
  dataAggregateContainer: {
    padding: 10,
    paddingTop: 0
  },
  headerContainer: {
    padding: 10
  },
  bodyContainer: {
    flexDirection: 'row'
  },
  footerContainer: {
    padding: 10,
    paddingBottom: 20
  },
  headerTitle: {
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerTopItems: {
    textAlign: 'left',
    marginBottom: 10,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  headerBottomItems: {
    textAlign: 'left',
    marginBottom: 10,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  dataValue: {
    textAlign: 'center',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  dataValue4: {
    textAlign: 'right'
  },
  footerDataWrapper: {
    flexDirection: 'row'
  },
  footerNoteWrapper: {
    flexDirection: 'row',
    marginTop: 15
  },
  footerTitleText: {
    textAlign: 'left',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  footerContentText: {
    textAlign: 'right',
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  }
})
