import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    margin: 15
  },
  searchProductPanel: {
    flex: 1,
    alignItems: 'center'
  },
  searchTitle: {
    fontSize: 18,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 5
  },
  listHeader: {
    width: undefined,
    height: 30,
    marginRight: 10,
    flexDirection: 'row'
  },
  headerBreadcrumbs: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: '#bbb',
    flexDirection: 'row'
  },
  breadcrumbsItem: {
    margin: 5
  },
  currentBreadcrumbsItem: {
    fontWeight: '500'
  },
  headerTitle: {
    marginRight: 0,
    width: 32,
    height: 32
  },
  recordListContainer: {
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  recordItem: {
    height: 90,
    backgroundColor: '#f3f3f3',
    marginTop: 2,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 2,
    borderRadius: 5,
    padding: 15,
    borderWidth: 1,
    borderColor: '#bbb',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  recordItemTitle: {
    backgroundColor: 'transparent'
  },
  productList: {
    marginTop: 10,
    width: undefined,
    flex: 1
  },
  recordItemRight: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  recordItemLeft: {
    flex: 3,
    alignItems: 'flex-start',
    height: 70
  },
  searchContainer: {
    padding: 20,
    paddingTop: 10,
    flexDirection: 'column',
    flex: 1
  },
  searchPanelWrapper: {
    height: 80,
    marginBottom: 10,
    marginTop: 0
  },
  searchPanelText: {
    margin: 0,
    padding: 0,
    marginBottom: 25
  },
  searchPanelInputWrapper: {
    flex: 1
  },
  searchPanelTextWrapper: {
    flex: 1,
    marginTop: 10
  }
})
