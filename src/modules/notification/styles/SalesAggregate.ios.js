import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  salesAggregateContainer: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
