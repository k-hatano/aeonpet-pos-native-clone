'use strict'

import React, { Component } from 'react'
import { Text, ScrollView, View } from 'react-native'
import componentStyles from '../styles/OrderReturn'

function HeaderAggregate (props) {
  const { title, topItems, bottomItems } = props
  return (
    <View style={componentStyles.headerContainer}>
      <Text style={componentStyles.headerTitle} key={title}>{title}</Text>
      <Text style={componentStyles.headerTopItems} key={topItems}>{topItems}</Text>
      <Text style={componentStyles.headerBottomItems} key={bottomItems}>{bottomItems}</Text>
    </View>
  )
}

function DataAggregate (props) {
  const { dataItems } = props
  return (
    <View>
      {dataItems.map((item, index) =>
        <View style={componentStyles.bodyContainer} key={index}>
          <View style={{ flex: 2 }}>
            <Text style={[componentStyles.dataValue, componentStyles.dataValue4]}>{item.value1}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={componentStyles.dataValue}>{item.value2}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={componentStyles.dataValue}>{item.value3}</Text>
          </View>
          <View style={{ flex: 2 }}>
            <Text style={[componentStyles.dataValue, componentStyles.dataValue4]}>{item.value4}</Text>
          </View>
        </View>
      )}
    </View>
  )
}

function FooterAggregate (props) {
  const { data, note } = props
  return (
    <View style={componentStyles.footerContainer}>
      {data.map((rowData, index) => {
        return (
          <View key={index} style={componentStyles.footerDataWrapper}>
            <View style={{ flex: 1 }}>
              <Text style={componentStyles.footerTitleText}>{rowData.title}</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={componentStyles.footerContentText}>{rowData.total}</Text>
            </View>
          </View>
        )
      })}
      <View style={componentStyles.footerNoteWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{`${note.title}:`}</Text>
        </View>
        <View style={{ flex: 4 }}>
          <Text style={componentStyles.footerContentText}>{note.content}</Text>
        </View>
      </View>
    </View>
  )
}

export default class Aggregate2 extends Component {
  render () {
    const { salesAggregate } = this.props
    if (!salesAggregate) {
      return false
    }
    const { headerTitle, headerTopItems, headerBottomItems, dataAggregate, footerData, footerNote } = salesAggregate

    return (
      <ScrollView style={componentStyles.container}>
        <View style={componentStyles.receiptWrapper}>
          <HeaderAggregate title={headerTitle} topItems={headerTopItems} bottomItems={headerBottomItems} />
          <View style={componentStyles.dataAggregateContainer}>
            {dataAggregate.map((itemAggregate, index) => <DataAggregate dataItems={itemAggregate.data} key={index} />)}
          </View>
          <FooterAggregate data={footerData} note={footerNote} />
        </View>
      </ScrollView>
    )
  }
}
