'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import AggregateReceipt from './AggregateReceipt'
import OrderReturn from './OrderReturn'
import OrderReceipt from './OrderReceipt'

export default class SalesAggregate extends Component {
  _renderReceipt = () => {
    const { itemType, salesAggregate } = this.props
    if (!salesAggregate) {
      return false
    }

    switch (itemType) {
      case 1:
        return <AggregateReceipt salesAggregate={salesAggregate} />
      case 2:
        return <OrderReturn salesAggregate={salesAggregate} />
      case 3:
        return <OrderReceipt salesAggregate={salesAggregate} />
      default:
        return false
    }
  }

  render () {
    const { componentStyles } = this.props
    const receiptComponent = this._renderReceipt()
    if (!receiptComponent) {
      return <View />
    }

    return (
      <View style={componentStyles.salesAggregateContainer}>
        {receiptComponent}
      </View>
    )
  }
}
