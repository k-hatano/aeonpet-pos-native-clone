'use strict'

import React, { Component } from 'react'
import { Text, ScrollView, View } from 'react-native'
import componentStyles from '../styles/AggregateReceipt'

function HeaderAggregate (props) {
  const { topItems, title, bottomItems } = props
  return (
    <View style={componentStyles.headerContainer}>
      {topItems.map((item, index) => <Text style={componentStyles.headerTitle} key={index}>{item}</Text>)}
      <Text style={componentStyles.headerTitle2} key={title}>{title}</Text>
      {bottomItems.map((item, index) =>
        <Text style={componentStyles.headerTopItems} key={index}>
          {`${item.label}:   ${item.text}`}
        </Text>
      )}
    </View>
  )
}

function DataAggregate (props) {
  const { title, dataItems } = props
  return (
    <View style={componentStyles.bodyContainer}>
      <Text style={componentStyles.bodyTitle} key={title}>{title}</Text>
      {dataItems.map((item, index) =>
        <View style={componentStyles.bodyContentWrapper} key={index}>
          <View style={{ flex: 1 }}>
            <Text style={componentStyles.bodyContentTitle}>{item.title}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={componentStyles.bodyContentValue}>{item.value}</Text>
          </View>
        </View>
      )}
    </View>
  )
}

function FooterAggregate (props) {
  const { data } = props
  return (
    <View style={componentStyles.footerContainer}>
      <Text style={componentStyles.footerContentText}>{`${data.title}:   ${data.total}`}</Text>
    </View>
  )
}

export default class Aggregate1 extends Component {
  render () {
    const { salesAggregate } = this.props
    if (!salesAggregate) {
      return false
    }
    const { headerTitle, headerTopItems, headerBottomItems, dataAggregate, footerData } = salesAggregate

    return (
      <ScrollView style={componentStyles.container}>
        <View style={componentStyles.receiptWrapper}>
          <HeaderAggregate title={headerTitle} topItems={headerTopItems} bottomItems={headerBottomItems} />
          <View style={componentStyles.dataAggregateContainer}>
            {dataAggregate.map((itemAggregate, index) =>
              <DataAggregate title={itemAggregate.title} dataItems={itemAggregate.data} key={index} />
            )}
          </View>
          <FooterAggregate data={footerData} />
        </View>
      </ScrollView>
    )
  }
}
