'use strict'

import React, { Component } from 'react'
import { Text, View, ListView, TouchableOpacity, StyleSheet } from 'react-native'
import styles from '../styles/NotificationSearch'

export default class NotificationSearchSection extends Component {
  constructor (props) {
    super(props)

    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      item: null
    }
  }

  _onItemChoose = item => {
    const { onItemChoose } = this.props

    onItemChoose(item)
    this.setState({
      isInDetail: true,
      item
    })
  }

  _onBack = () => {
    const { onItemUnChoose } = this.props

    onItemUnChoose()
    this.setState({
      item: null,
      fullWidth: 0,
      fullHeight: 0
    })
  }

  _onLayoutChange = event => {
    const { width, height } = event.nativeEvent.layout
    this.setState({ fullWidth: width, fullHeight: height })
  }

  render () {
    const { data } = this.props
    const source = this.dataSource.cloneWithRows(data)

    return (
      <View
        onLayout={event => {
          this._onLayoutChange(event)
        }}
        style={styles.searchContainer}
      >
        <View style={componentStyles.container}>
          <View style={componentStyles.emptyRowHeader} />
          <View style={{ flex: 1 }}>
            <ListView
              style={componentStyles.listContainer}
              dataSource={source}
              renderRow={(rowData, index) =>
                <TouchableOpacity key={index} style={componentStyles.row} onPress={() => this._onItemChoose(rowData)}>
                  <View style={componentStyles.rowName}>
                    <Text>{rowData.title}</Text>
                  </View>
                  <View style={componentStyles.rowId}>
                    <Text style={componentStyles.rowIdText}>{rowData.date}</Text>
                  </View>
                </TouchableOpacity>}
            />
          </View>
        </View>
      </View>
    )
  }
}

const componentStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  listContainer: {
    width: '100%',
    height: '100%'
  },
  emptyRowHeader: {
    backgroundColor: '#ddd',
    height: 26
  },
  rowHeader: {
    backgroundColor: '#ddd',
    height: 44,
    justifyContent: 'center',
    paddingLeft: 15
  },
  row: {
    flexDirection: 'row',
    padding: 7,
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#fff'
  },
  rowName: {
    flex: 1,
    marginRight: 10
  },
  rowId: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  rowIdText: {},
  rowIcon: {
    marginLeft: 10
  }
})
