'use strict'

import React, { Component } from 'react'
import { Text, ScrollView, View } from 'react-native'
import componentStyles from '../styles/OrderReceipt'

function HeaderAggregate (props) {
  const { title, topItems, topLogo, bottomDate, bottomId } = props
  return (
    <View style={componentStyles.headerContainer}>
      <Text style={componentStyles.headerTitle} key={title}>{title}</Text>
      <Text style={componentStyles.headerLogo} key={topLogo}>{topLogo}</Text>
      <Text style={componentStyles.headerTopItems} key={topItems}>{topItems}</Text>
      <Text style={componentStyles.headerTopItems} key={bottomDate}>{bottomDate}</Text>
      <Text style={componentStyles.headerTopItems} key={bottomId}>{`ID:${bottomId}`}</Text>
    </View>
  )
}

function DataAggregate (props) {
  const { dataItems } = props
  return (
    <View>
      {dataItems.map((item, index) =>
        <View style={componentStyles.bodyContainer} key={index}>
          <View>
            <Text style={[componentStyles.headerTopItems]}>{item.name}</Text>
          </View>
          <View style={componentStyles.bodyAmountContainer}>
            <View style={{ flex: 1 }} />
            <View style={{ flex: 2 }}>
              <Text style={componentStyles.dataValue}>{item.price}</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={componentStyles.dataValue}>{item.quantity}</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text style={[componentStyles.dataValue, componentStyles.dataValue4]}>{item.total}</Text>
            </View>
          </View>
        </View>
      )}
    </View>
  )
}

function FooterAggregate (props) {
  const { data, note } = props
  return (
    <View style={componentStyles.footerContainer}>
      <View style={[componentStyles.footerDataWrapper, componentStyles.footerConclusion1]}>
        <View style={{ flex: 2 }}>
          <Text style={componentStyles.footerTitleText}>{data.conclusion1.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion1.quantity}</Text>
        </View>
        <View style={{ flex: 2 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion1.total}</Text>
        </View>
      </View>
      <View style={componentStyles.footerDataWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{data.conclusion2.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion2.total}</Text>
        </View>
      </View>
      <View style={componentStyles.footerDataWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{`(${data.conclusion2.note})`}</Text>
        </View>
      </View>
      <View style={componentStyles.footerDataWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{data.conclusion3.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion3.total}</Text>
        </View>
      </View>
      <View style={componentStyles.footerDataWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{data.conclusion4.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion4.total}</Text>
        </View>
      </View>
      <View style={[componentStyles.footerDataWrapper, componentStyles.footerConclusion5]}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{data.conclusion5.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerContentText}>{data.conclusion5.total}</Text>
        </View>
      </View>
      <View style={componentStyles.footerNoteWrapper}>
        <View style={{ flex: 1 }}>
          <Text style={componentStyles.footerTitleText}>{`${note.title}:`}</Text>
        </View>
        <View style={{ flex: 4 }}>
          <Text style={componentStyles.footerContentText}>{note.content}</Text>
        </View>
      </View>
    </View>
  )
}

export default class Aggregate2 extends Component {
  render () {
    const { salesAggregate } = this.props
    if (!salesAggregate) {
      return false
    }
    const {
      headerTitle,
      headerTopItems,
      headerTopLogo,
      headerBottomDate,
      headerBottomId,
      dataAggregate,
      footerData,
      footerNote
    } = salesAggregate

    return (
      <ScrollView style={componentStyles.container}>
        <View style={componentStyles.receiptWrapper}>
          <HeaderAggregate
            title={headerTitle}
            topItems={headerTopItems}
            topLogo={headerTopLogo}
            bottomDate={headerBottomDate}
            bottomId={headerBottomId}
          />
          <View style={componentStyles.dataAggregateContainer}>
            <DataAggregate dataItems={dataAggregate} />
          </View>
          <View style={componentStyles.separator}>
            <Text>-------------------------------------</Text>
          </View>
          <FooterAggregate data={footerData} note={footerNote} />
        </View>
      </ScrollView>
    )
  }
}
