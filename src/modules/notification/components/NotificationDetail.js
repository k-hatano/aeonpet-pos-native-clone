'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import I18n from 'i18n-js'
import SalesAggregate from './SalesAggregate'
import stylesSalesAggregate from '../styles/SalesAggregate'
import componentStyles from '../styles/NotificationDetail'

export default class NotificationDetail extends Component {
  constructor (props) {
    super(props)

    this.bottomButtons = [
      {
        icon: 'printer',
        type: 'material',
        text: I18n.t('notification_receipt.print'),
        iconSize: 24,
        containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
        onPress: () => {}
      },
      {
        icon: 'printer',
        type: 'material',
        text: I18n.t('notification_receipt.select_printer'),
        iconSize: 24,
        containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
        onPress: () => {}
      }
    ]
  }
  render () {
    const { item } = this.props

    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.detailWrapper}>
          <SalesAggregate
            componentStyles={stylesSalesAggregate}
            salesAggregate={item && item.salesAggregate ? item.salesAggregate : null}
            itemType={item && item.type ? item.type : null}
          />
        </View>
        <View style={componentStyles.bottomBar}>
          <View style={componentStyles.bottomBarLeftPanel}>
            <TouchableOpacity style={componentStyles.orangeBorderButton}>
              <Text style={componentStyles.orangeBorderButtonText}>{I18n.t('home.delete')}</Text>
            </TouchableOpacity>
          </View>
          <View style={componentStyles.bottomBarRightPanel}>
            <TouchableOpacity style={componentStyles.orangeFilledButton}>
              <Text style={componentStyles.orangeFilledButtonText}>{I18n.t('home.reprint')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
