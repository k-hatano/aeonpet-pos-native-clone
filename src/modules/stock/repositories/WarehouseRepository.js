import SampleWarehouseRepository from './sample/SampleWarehouseRepository'
import StandardWarehouseRepository from './standard/StandardWarehouseRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class WarehouseRepository {
  static _implement = new SampleWarehouseRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static async fetchAll () {
    return this._implement.fetchAll()
  }

  static async fetchByShopId (shopId) {
    return this._implement.fetchByShopId(shopId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardWarehouseRepository()
        break

      default:
        this._implement = new SampleWarehouseRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('WarehouseRepository', (context) => {
  WarehouseRepository.switchImplement(context)
})
