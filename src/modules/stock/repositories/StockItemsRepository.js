import SampleStockItemsRepository from './sample/SampleStockItemsRepository'
import StandardStockItemsRepository from './standard/StandardStockItemsRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockItemsRepository {
  static _implement = new SampleStockItemsRepository()

  static async bulkSave (stockItems) {
    return this._implement.bulkSave(stockItems)
  }

  static async fetchAll () {
    return this._implement.fetchAll()
  }

  static async findByProductId (productId) {
    return this._implement.findByProductId(productId)
  }

  static async findByProductVariantId (productVariantId) {
    return this._implement.findByProductVariantId(productVariantId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockItemsRepository()
        break

      default:
        this._implement = new SampleStockItemsRepository()
        break
    }
  }

  static fetchByProductId (productId) {
    return this._implement.fetchByProductId(productId)
  }

  static fetchByProductVariantIds (productVariantIds) {
    return this._implement.fetchByProductVariantIds(productVariantIds)
  }
}

AppEvents.onRepositoryContextChanged('StockItemsRepository', (context) => {
  StockItemsRepository.switchImplement(context)
})
