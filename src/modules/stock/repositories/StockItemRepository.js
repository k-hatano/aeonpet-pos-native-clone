import SampleStockItemRepository from './sample/SampleStockItemRepository'
import StandardStockItemRepository from './standard/StandardStockItemRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockItemRepository {
  static _implement = new SampleStockItemRepository()

  static async fetchByProductVariantId(productVariantId, accessibles) {
    return this._implement.fetchByProductVariantId(productVariantId, accessibles)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockItemRepository()
        break

      default:
        this._implement = new SampleStockItemRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StockItemRepository', context => {
  StockItemRepository.switchImplement(context)
})
