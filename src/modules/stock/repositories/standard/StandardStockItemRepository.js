import fetcher from 'common/models/fetcher'
import { encodeCriteria } from 'common/utils/searchUtils'
import { handleAxiosError } from 'common/errors'

export default class StandardStockItemRepository {
  async fetchByProductVariantId (productVariantId, accessibles = false) {
    try {
      const response = await fetcher.get((accessibles ? 'stock-items/accessibles' : 'stock-items'), {
        ...encodeCriteria({ product_variant_id: productVariantId }),
        ...{ with: 'warehouse.shopWarehouses' }
      })
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
