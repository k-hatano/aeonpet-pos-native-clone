import { handleAxiosError } from '../../../../common/errors'
import WarehouseEntity from '../entities/WarehouseEntity'
import fetcher from '../../../../common/models/fetcher'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import { sequelize } from '../../../../common/DB'

export default class StandardWarehouseRepository {
  async findAll () {
    try {
      return WarehouseEntity.findAll({
        raw: true,
        order: sequelize.literal('lower(warehouse_code) asc')
      })
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchAll () {
    try {
      const response = await fetcher.get('warehouses', {with: 'shopWarehouses'})
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchByShopId (shopId) {
    try {
      const response = await fetcher.get('warehouses', ({
        with: 'shopWarehouses',
        ...encodeCriteria({'shopWarehouses.shop_id': shopId})
      }))
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
