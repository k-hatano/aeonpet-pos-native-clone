import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'
import StockItem from '../entities/StockItemEntity'
import { encodeCriteria } from 'common/utils/searchUtils'

export default class StandardStockItemsRepository {
  async fetchAll () {
    try {
      const result = await fetcher.get('stock-items')
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async findByProductId (productId) {
    return StockItem.findAll({
      where: {
        product_id: productId
      },
      raw: true
    })
  }

  async findByProductVariantId (productVariantId) {
    return StockItem.findAll({
      where: {
        product_variant_id: productVariantId
      },
      raw: true
    })
  }

  async bulkSave (stockItems) {
    for (const stockItem of stockItems) {
      await StockItem.destroy({ where: {id: stockItem.id} })
      await StockItem.create(stockItem)
    }
  }

  async fetchByProductId (productId) {
    try {
      const result = await fetcher.get('stock-items', encodeCriteria({product_id: productId}))
      return result.data.length > 0 ? result.data[0] : null
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchByProductVariantIds (productVariantIds) {
    try {
      const result = await fetcher.get('stock-items', encodeCriteria({product_variant_id: productVariantIds}))
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
