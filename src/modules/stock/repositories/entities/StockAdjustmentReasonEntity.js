import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const StockAdjustmentReason = sequelize.define(
  'StockAdjustmentReason',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    stock_adjustment_reason_code: Sequelize.STRING,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'stock_adjustment_reasons',
    underscored: true,
    timestampe: false
  }
)

export default StockAdjustmentReason
