import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const StockItem = sequelize.define(
  'StockItem',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    warehouse_id: Sequelize.STRING,
    warehouse_name: Sequelize.STRING,
    warehouse_code: Sequelize.STRING,
    product_id: Sequelize.STRING,
    product_variant_id: Sequelize.STRING,
    quantity: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'stock_items',
    underscored: true,
    timestamps: false
  }
)

export default StockItem
