import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const Warehouse = sequelize.define(
  'Warehouse',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    warehouse_code: Sequelize.STRING,
    warehouse_type: Sequelize.INTEGER,
    sort_order: Sequelize.INTEGER,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE
  }, {
    tableName: 'warehouses',
    underscored: true,
    timestamps: false
  }
)

export default Warehouse
