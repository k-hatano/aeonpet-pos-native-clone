import { sampleProductMap } from '../../../product/samples'
import generateUuid from '../../../../common/utils/generateUuid'
import { shopIds } from '../../../shop/samples'
import { warehouseIds } from './SampleWarehouseRepository'

export default class SampleStockItemRepository {
  _stockItems = []

  stockItems() {
    return [
      {
        id: generateUuid(),
        quantity: 11,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: sampleProductMap.standardA.product_variants[0].id,
        product_code: '123456781',
        warehouse_id: warehouseIds.A,
        warehouse_code: '12345678',
        warehouse_name: '北本 店',
        created_at: 1504765317360,
        updated_at: 1504765317360,
        warehouse: {
          id: warehouseIds.A,
          status: 1,
          warehouse_code: '01101',
          warehouse_type: 1,
          sort_order: 68,
          created_at: 1506331474,
          updated_at: 1506331474,
          deleted_at: null,
          shop_warehouses: [
            {
              id: generateUuid(),
              created_at: 1506331474,
              updated_at: 1506331474,
              shop_id: shopIds.A,
              warehouse_id: warehouseIds.A
            }
          ],
          name: '北本 店'
        }
      },
      {
        id: generateUuid(),
        quantity: 10,
        product_id: sampleProductMap.standardB.id,
        product_variant_id: sampleProductMap.standardB.product_variants[0].id,
        product_code: '123456782',
        warehouse_id: warehouseIds.B,
        warehouse_code: '12345678',
        warehouse_name: '桶川 店',
        created_at: 1504765466071,
        updated_at: 1504765466071,
        warehouse: {
          id: warehouseIds.B,
          status: 1,
          warehouse_code: '01101',
          warehouse_type: 1,
          sort_order: 68,
          created_at: 1506331474,
          updated_at: 1506331474,
          deleted_at: null,
          shop_warehouses: [
            {
              id: generateUuid(),
              created_at: 1506331474,
              updated_at: 1506331474,
              shop_id: shopIds.B,
              warehouse_id: warehouseIds.B
            }
          ],
          name: '桶川 店'
        }
      },
      {
        id: generateUuid(),
        quantity: 30,
        product_id: sampleProductMap.standardC.id,
        product_variant_id: sampleProductMap.standardC.product_variants[0].id,
        product_code: '123456783',
        warehouse_id: warehouseIds.C,
        warehouse_code: '12345678',
        warehouse_name: '鴻巣 店',
        created_at: 1504765497070,
        updated_at: 1504765497070,
        warehouse: {
          id: warehouseIds.C,
          status: 1,
          warehouse_code: '01101',
          warehouse_type: 1,
          sort_order: 68,
          created_at: 1506331474,
          updated_at: 1506331474,
          deleted_at: null,
          shop_warehouses: [
            {
              id: generateUuid(),
              created_at: 1506331474,
              updated_at: 1506331474,
              shop_id: shopIds.C,
              warehouse_id: warehouseIds.C
            }
          ],
          name: '鴻巣 店'
        }
      }
    ]
  }

  async fetchByProductVariantId(productVariantId, accessibles) {
    return this.stockItems().filter(
      stockItem => stockItem.product_variant_id == productVariantId
    )
  }
}
