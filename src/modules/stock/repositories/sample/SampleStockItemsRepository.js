import { sampleProductMap } from '../../../product/samples'
import generateUuid from '../../../../common/utils/generateUuid'

export default class SampleStockItemsRepository {
  _stockItems = []

  stockItems () {
    return [
      {
        id: generateUuid(),
        quantity: 100,
        product_id: sampleProductMap.standardA.id,
        product_variant_id: sampleProductMap.standardA.product_variants[0].id,
        product_code: '123456781',
        warehouse_id: generateUuid(),
        warehouse_code: '12345678',
        created_at: 1504765317360,
        updated_at: 1504765317360
      },
      {
        id: generateUuid(),
        quantity: 10,
        product_id: sampleProductMap.standardB.id,
        product_variant_id: sampleProductMap.standardB.product_variants[0].id,
        product_code: '123456782',
        warehouse_id: generateUuid(),
        warehouse_code: '12345678',
        created_at: 1504765466071,
        updated_at: 1504765466071
      },
      {
        id: generateUuid(),
        quantity: 30,
        product_id: sampleProductMap.standardC.id,
        product_variant_id: sampleProductMap.standardC.product_variants[0].id,
        product_code: '123456783',
        warehouse_id: generateUuid(),
        warehouse_code: '12345678',
        created_at: 1504765497070,
        updated_at: 1504765497070
      }
    ]
  }

  async bulkSave (stockItems) {
    this._stockItems = []
    console.log('push stockItems', stockItems)
    this._stockItems = this._stockItems.concat(stockItems)
  }

  async fetchAll () {
    return this.stockItems()
  }

  async findByProductId (productId) {
    return this.stockItems().filter(
      stockItem => stockItem.product_id === productId
    )
  }

  async findByProductVariantId (productVariantId) {
    return this.stockItems().filter(
      stockItem => stockItem.product_variant_id === productVariantId
    )
  }

  async fetchByProductId (productId) {
    return this.stockItems().filter(
      stockItem => stockItem.product_id === productId
    )
  }

  async fetchByProductVariantIds (productVariantIds) {
    return this.stockItems().filter(
      stockItem => productVariantIds.indexOf(stockItem.product_variant_id) > 0
    )
  }
}
