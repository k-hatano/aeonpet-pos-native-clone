import generateUuid from '../../../../common/utils/generateUuid'
import { shopIds } from '../../../shop/samples'

export const warehouseIds = {
  A: '12345EAFB60AC638E8618A7B7017FDA0',
  B: '12345D1962F655715FBA3F5A549DBED1',
  C: '12345FCADD1976634C79E3C5F2A1576F',
  D: '123450C4719F6DFC3C65A7AF27E99348',
  E: '12345AA6519483EB1154C5696E96BA19',
  F: '1234549C5A81E0208D153464308D08DB',
  G: '1234575EBD9C0F93CCCF89B7D061BDAB',
  H: '123457E2D5AF03B2C042C4D18718A59E',
  I: '1234546DE37AFEF693519EE379DBAA18'
}

export default class SampleWarehouseRepository {
  warehouses () {
    return [
      {
        id: warehouseIds.A,
        status: 1,
        warehouse_code: '018600000001',
        warehouse_type: 2,
        sort_order: 1,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '北本 店'
      },
      {
        id: warehouseIds.B,
        status: 1,
        warehouse_code: '018600000002',
        warehouse_type: 2,
        sort_order: 2,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '桶川 店'
      },
      {
        id: warehouseIds.C,
        status: 1,
        warehouse_code: '018600000003',
        warehouse_type: 2,
        sort_order: 3,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '鴻巣 店'
      }
    ]
  }

  warehousesWithShopWarehouses () {
    return [
      {
        id: warehouseIds.A,
        status: 1,
        warehouse_code: '018600000001',
        warehouse_type: 2,
        sort_order: 1,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '北本 店',
        shop_warehouses: [
          {
            id: generateUuid(),
            created_at: 1506331474,
            updated_at: 1506331474,
            shop_id: shopIds.A,
            warehouse_id: warehouseIds.A
          }
        ]
      },
      {
        id: warehouseIds.B,
        status: 1,
        warehouse_code: '018600000002',
        warehouse_type: 2,
        sort_order: 2,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '桶川 店',
        shop_warehouses: [
          {
            id: generateUuid(),
            created_at: 1506331474,
            updated_at: 1506331474,
            shop_id: shopIds.B,
            warehouse_id: warehouseIds.B
          }
        ]
      },
      {
        id: warehouseIds.C,
        status: 1,
        warehouse_code: '018600000003',
        warehouse_type: 2,
        sort_order: 3,
        created_at: 1506331474,
        updated_at: 1506331474,
        deleted_at: null,
        name: '鴻巣 店',
        shop_warehouses: [
          {
            id: generateUuid(),
            created_at: 1506331474,
            updated_at: 1506331474,
            shop_id: shopIds.C,
            warehouse_id: warehouseIds.C
          }
        ]
      }
    ]
  }

  async findAll () {
    return this.warehouses()
  }

  async fetchAll () {
    return this.warehousesWithShopWarehouses()
  }
}
