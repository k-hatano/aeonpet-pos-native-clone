import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    marginTop: 5,
    marginLeft: 10,
    backgroundColor: '#f7f7f7'
  },
  productLabelArea: {
    flex: 3
  },
  headerArea: {
    height: 27,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  mainArea: {
    height: 60,
    justifyContent: 'center'
  },
  productNameText: {
    height: 30
  },
  subArea: {
    height: 52,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  productSubInfoArea: {
    height: 50,
    justifyContent: 'space-around'
  },
  operationArea: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  priceText: {
    height: 29,
    marginRight: 30,
    justifyContent: 'space-around'
  }
})
