import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    backgroundColor: '#f7f7f7',
    justifyContent: 'center',
    borderColor: '#979797',
    borderWidth: 0,
    borderBottomWidth: 0.5
  },
  mainArea: {
    height: 80,
    margin: 10,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  warehouseArea: {
    justifyContent: 'space-around',
    flexDirection: 'column'
  },
  operationArea: {
    justifyContent: 'center',
    flexDirection: 'column',
    marginRight: 24
  }
})
