import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  listContainer: {
    borderColor: '#979797',
    borderWidth: 0,
    borderTopWidth: 0.5
  },
  tabText: {
    height: 50,
    fontSize: 24,
    textAlign: 'left',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#FFFFFF'
  }
})
