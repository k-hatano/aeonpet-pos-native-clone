import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 2,
    marginTop: 8
  }
})
