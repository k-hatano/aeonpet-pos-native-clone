import I18n from 'i18n-js'
import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import StockSearchProductForm from '../components/StockSearchProductForm'
import * as productActions from '../../../modules/product/actions'
import SettingKeys from '../../setting/models/SettingKeys'
import { getSettingFromState } from '../../setting/models'
import AlertView from '../../../common/components/widgets/AlertView'
import { PREV_SEARCH_COND } from '../../../modules/product/models'
import { loading } from '../../../common/sideEffects'
import ProductRepository from '../../product/repositories/ProductRepository'
import { searchStockItems } from '../services'

const mapDispatchToProps = dispatch => ({
  onSearchForBarcodeReader: async (barcode) => {
    const products = await loading(dispatch, async () => {
      let products = await ProductRepository.findProductsBySearchWordForStocks(barcode, true)
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      switch (products.length) {
        case 0:
          let productVariantsByWord = await ProductRepository.findVariantsBySearchWordForStocks(barcode)
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              const productDetail = await ProductRepository.findWithDetail(productVariantsByWord[0].id)
              products.push(productDetail)
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              dispatch(productActions.listProducts(productVariantsByWord))
              dispatch(productActions.listOriginProducts(productVariantsByWord))
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
              dispatch(productActions.setSearchLayoutFlg(true))
              break
          }
          break
        case 1:
          const productVariantsById = await ProductRepository.findVariantsByProductId(products[0].id)
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              break
          }
          break
        default:
          dispatch(productActions.listProducts(products))
          dispatch(productActions.listOriginProducts(products))
          dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
          dispatch(productActions.setSearchLayoutFlg(true))
          break
      }
      return products
    })
    return products
  },
  onProductSelected: async product => {
    await searchStockItems(dispatch, product)
  }
})

const mapStateToProps = state => ({
  selectProduct: state[MODULE_NAME].selectProduct,
  stockItems: state[MODULE_NAME].stockItems,
  currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY)
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockSearchProductForm
)
