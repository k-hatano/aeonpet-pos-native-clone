import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import StockSearchResultView from '../components/StockSearchResultView'
import SettingKeys from '../../setting/models/SettingKeys'
import { getSettingFromState } from '../../setting/models'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  selectProduct: state[MODULE_NAME].selectProduct,
  stockItems: state[MODULE_NAME].stockItems,
  currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY)
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockSearchResultView
)
