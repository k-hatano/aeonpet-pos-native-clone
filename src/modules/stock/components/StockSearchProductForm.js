'use strict'
import React, { Component } from 'react'
import styles from '../styles/StockSearchProductForm'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import CategoryProductView from '../../product/containers/CategoryProductView'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'

export default class StockSearchProductForm extends Component {
  static propTypes = {
    onSearchForBarcodeReader: PropTypes.func,
    onProductSelected: PropTypes.func,
    isSearchProduct: PropTypes.bool
  }
  static defaultProps = {
    isSearchProduct: true
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      if (!this.props.isSearchProduct) return

      const products = await this.props.onSearchForBarcodeReader(body.trim())

      if (products && products.length === 1) {
        this.props.onProductSelected(products[0])
      }
    })
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  render () {
    const { onProductSelected } = this.props
    return (
      <View style={styles.container}>
        <CategoryProductView
          onProductSelected={product => onProductSelected(product)}
          onShowAmountPad={product => onProductSelected(product)}
          style={{ flex: 3 }}
          showStockConditionFlg={false}
        />
      </View>
    )
  }
}
