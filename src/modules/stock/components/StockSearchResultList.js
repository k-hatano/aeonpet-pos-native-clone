import React, { Component } from 'react'
import { View, Text, ListView } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import StockSearchResultListItem from './StockSearchResultListItem'
import styles from '../styles/StockSearchResultList'

export default class StockSearchResultList extends Component {
  static propTypes = {
    stockItems: PropTypes.array
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    return (
      <View style={styles.listContainer}>
        <Text style={styles.tabText}>{I18n.t('stock_search.stock_list')}</Text>
        <ListView
          enableEmptySections
          dataSource={dataSource.cloneWithRows(this.props.stockItems)}
          renderRow={stockItem => (
            <StockSearchResultListItem
              stockItem={stockItem}
            />
          )}
          renderSeparator={(sectionID, rowID) => (
            <View key={rowID} style={styles.separator} />
          )}
        />
      </View>
    )
  }
}
