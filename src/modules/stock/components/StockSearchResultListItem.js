'use strict'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import styles from '../styles/StockSearchResultListItem'
import baseStyle from '../../../common/styles/baseStyle'
import { formatUnit } from '../../../common/utils/formats'

export default class StockSearchResultListItem extends Component {
  static propTypes = {
    stockItem: PropTypes.object
  }

  render () {
    const { stockItem } = this.props
    return (
      <View style={[styles.root]}>
        <View>
          <View style={styles.mainArea}>
            <View style={styles.warehouseArea}>
              <Text numberOfLines={2} style={baseStyle.mainText}>
                {stockItem.warehouse.name}
              </Text>
              <Text numberOfLines={2} style={baseStyle.mainText}>
                {stockItem.shop_phone_number}
              </Text>
            </View>
            <View style={styles.operationArea}>
              <Text style={baseStyle.mainText}>
                {formatUnit(stockItem.quantity, I18n.t('stock_search.item'))}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
