'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/StockSearchResultView'
import StockSearchResultProductItem from './StockSearchResultProductItem'
import StockSearchResultList from './StockSearchResultList'

export default class StockSearchResultView extends Component {
  static propTypes = {
    selectProduct: PropTypes.object,
    stockItems: PropTypes.array,
    currency: PropTypes.string
  }
  render () {
    const {
      selectProduct,
      stockItems,
      currency
    } = this.props
    return (
      <View>
        {Object.keys(selectProduct).length > 0 ? (
          <StockSearchResultProductItem
            product={selectProduct}
            currency={currency}
          />
        ) : null}
        {stockItems.length > 0 ? (
          <View View style={styles.stockListArea}>
            <StockSearchResultList stockItems={stockItems} />
          </View>
        ) : null}
      </View>
    )
  }
}
