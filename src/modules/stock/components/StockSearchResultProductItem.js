'use strict'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import { formatPriceWithCurrency, formatUnit } from '../../../common/utils/formats'
import styles from '../styles/StockSearchResultProductItem'
import baseStyle from 'common/styles/baseStyle'

export default class StockSearchResultProductItem extends Component {
  static propTypes = {
    product: PropTypes.object,
    currency: PropTypes.string
  }

  render () {
    const { product, currency } = this.props
    const productVariant = product.product_variants[0]
    const stockItem = product.stock_item

    return (
      <View>
        <View style={styles.root} ref='layoutRoot'>
          <View style={styles.headerArea}>
            <View style={styles.productLabelArea}>
              <Text
                numberOfLines={1}
                ellipsizeMode={'middle'}
                style={baseStyle.subText}>
                {product.product_code}
              </Text>
            </View>
          </View>
          <View style={styles.mainArea}>
            <Text numberOfLines={1} style={[baseStyle.mainText, styles.productNameText]}>
              {product.name}
            </Text>
          </View>
          <View style={styles.subArea}>
            <View style={styles.productSubInfoArea}>
              <Text style={[baseStyle.subText, styles.unitPriceText]}>
                {I18n.t('cart.unit_price')} : {formatPriceWithCurrency(productVariant.price, currency)}
              </Text>
            </View>
            <View style={styles.operationArea}>
              <Text style={[baseStyle.strongTextStyle, styles.priceText]}>
                {formatUnit(stockItem.quantity || 0, I18n.t('stock_search.item'))}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
