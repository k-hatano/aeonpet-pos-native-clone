import * as actions from './actions'

export const MODULE_NAME = 'stock'

export const refreshForStockSearch = dispatch => {
  dispatch(actions.refreshStock())
}
