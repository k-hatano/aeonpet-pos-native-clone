import * as actions from './actions'
import * as _ from 'underscore'
import I18n from 'i18n-js'
import AlertView from '../../common/components/widgets/AlertView'
import { loading } from '../../common/sideEffects'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import ShopRepository from '../shop/repositories/ShopRepository'
import StockItemRepository from './repositories/StockItemRepository'
import WarehouseRepository from './repositories/WarehouseRepository'
import ProductRepository from '../product/repositories/ProductRepository'

/**
 * 指定した商品の全倉庫の在庫を在庫検索画面に一覧表示します。
 * @param dispatch
 * @param product
 * @returns {Promise<boolean>}
 */
export const searchStockItems = async (dispatch, product) => {
  const result = await loading(dispatch, async () => {
    try {
      const shop = await ShopRepository.find()
      const shops = await ShopRepository.fetchAll(true)
      const shopIdToShop = _.indexBy(shops, 'id')
      const productVariantId = product.product_variants[0].id
      const productVariant = await ProductRepository.fetchProductVariantById(productVariantId)
      const stockItems = await StockItemRepository.fetchByProductVariantId(productVariantId, true)
      const currentStockItem = stockItems.find(item => item.warehouse.shop_warehouses[0].shop_id === shop.id)
      // 在庫レコードが0件の場合は、その商品が削除されたものと判定する。
      if (stockItems.length === 0) {
        return {
          success: false,
          message: I18n.t('message.L-01-E001')
        }
      }
      // 在庫管理なしの場合もエラー
      if (productVariant && productVariant.stock_mode === 0) {
        return {
          success: false,
          message: I18n.t('message.L-01-E002')
        }
      }
      const otherStockItems =
        _.sortBy(stockItems, function (item) { return item.warehouse.warehouse_code })
          .filter(item => item.warehouse.shop_warehouses[0].shop_id !== shop.id)
          .map(item => ({
            ...item,
            shop_phone_number: shopIdToShop[item.warehouse.shop_warehouses[0].shop_id].shop_address.phone_number
          }))
      const warehouses = await WarehouseRepository.fetchAll()
      dispatch(
        actions.selectProduct({
          ...product,
          ...{
            stock_item: currentStockItem || {
              quantity: 0,
              warehouse: warehouses.find(
                warehouse =>
                  warehouse.shop_warehouses &&
                  warehouse.shop_warehouses.length > 0 &&
                  warehouse.shop_warehouses[0].shop_id === shop.id
              )
            }
          }
        })
      )
      dispatch(actions.setStockItems(otherStockItems))
      return {success: true}
    } catch (error) {
      return {
        success: false,
        message: getCommonNetworkErrorMessage(error)
      }
    }
  })
  if (!result.success) {
    await AlertView.showAsync(result.message)

    dispatch(actions.selectProduct({}))
    dispatch(actions.setStockItems([]))
  }
}
