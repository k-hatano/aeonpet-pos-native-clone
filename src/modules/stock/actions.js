import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const selectProduct = createAction(`${MODULE_NAME}_selectProduct`)
export const setStockItems = createAction(`${MODULE_NAME}_setStockItems`)
export const selectStockItem = createAction(`${MODULE_NAME}_selectStockItem`)
export const refreshStock = createAction(`${MODULE_NAME}_refreshStock`)
