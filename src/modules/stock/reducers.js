import * as actions from './actions'
import { handleActions } from 'redux-actions'

const defaultState = {
  selectProduct: {},
  stockItems: [],
  selectStockItem: {},
  warehouses: []
}

const handlers = {
  [actions.selectProduct]: (state, action) => ({
    ...state,
    selectProduct: action.payload
  }),
  [actions.setStockItems]: (state, action) => ({
    ...state,
    stockItems: action.payload
  }),
  [actions.selectStockItem]: (state, action) => ({
    ...state,
    selectStockItem: action.payload
  }),
  [actions.refreshStock]: (state, action) => ({
    ...defaultState
  })
}
export default handleActions(handlers, defaultState)
