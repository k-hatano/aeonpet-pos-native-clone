import DebugModalView from '../components/DebugModalView'

export default {
  name: 'DebugModalView',
  component: DebugModalView,
  properties: [
    {
      title: 'Default',
      property: {
        modal: {
          title: 'サンプルデバッグ',
          options: [
            {
              title: 'ラジオボタン選択肢',
              type: 'radio',
              key: 'radio1',
              options: [
                { label: '選択肢1', value: 1 },
                { label: '選択肢2', value: 2 },
                { label: '選択肢3', value: 3 }
              ],
              selectedValue: 2
            },
            {
              title: 'ラジオボタン選択肢2',
              type: 'radio',
              key: 'radio2',
              options: [
                { label: '選択肢1', value: 1 },
                { label: '選択肢2', value: 2 },
                { label: '選択肢3', value: 3 },
                { label: '選択肢4', value: 4 },
                { label: '選択肢5', value: 5 },
                { label: '選択肢6', value: 6 },
                { label: '選択肢7', value: 7 },
                { label: '選択肢8', value: 8 }
              ],
              selectedValue: 5
            },
            {
              title: 'テキスト入力',
              type: 'text',
              key: 'text1'
            }
          ],
          commands: [
            {
              label: 'Error',
              onPress: (params) => {
                console.log('on error', params)
              }
            },
            {
              label: 'OK',
              onPress: (params) => {
                console.log('on ok', params)
              }
            }
          ]
        }
      }
    },
    {
      title: 'PropertySample2',
      property: {
        name: 'AnotherName'
      }
    }
  ],
  frames: [
    {
      title: '800 x 700',
      style: {width: 800, height: 700}
    }
  ]
}
