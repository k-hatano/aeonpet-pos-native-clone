import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setDebugModal = createAction(`${MODULE_NAME}_setDebugModal`)
