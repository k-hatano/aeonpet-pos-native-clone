import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  modalFrame: {
    width: 800,
    height: 700
  },
  modalContent: {
    width: '100%',
    height: '100%'
  },
  optionsArea: {
    flex: 1
  },
  buttonArea: {
    height: 60,
    flexDirection: 'row'
  },
  commandButton: {
    height: 40,
    fontSize: 30,
    margin: 10
  },
  commandButtonContainer: {
    flex: 1
  },
  optionLayout: {
    margin: 16
  },
  optionTitleText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor
  },
  radioLayout: {
    height: 40,
    margin: 8
  },
  textLayout: {
    height: 36,
    borderColor: 'gray',
    borderWidth: 2,
    margin: 8
  },
  textInput: {
    flex: 1,
    fontSize: 30
  }
})
