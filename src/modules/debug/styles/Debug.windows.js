import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  scrollViewContainer: {
    width: '100%',
    height: '100%'
  },
  contentContainerStyle: {
    flexDirection: 'column',
    padding: 20
  },
  groupHeaderContainer: {
    backgroundColor: '#f0f0f0',
    height: 60,
    justifyContent: 'center',
    paddingLeft: 24,
    paddingTop: 12,
    paddingBottom: 12,
    marginBottom: 10
  },
  groupTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 28,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  groupContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'column'
  },
  buttonWrap: {
    maxHeight: 60,
    marginVertical: 5
  },
  buttonTitleStyle: {
    fontSize: 24,
    fontFamily: 'Helvetica'
  }
})
