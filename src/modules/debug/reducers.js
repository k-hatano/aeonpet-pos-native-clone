import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  currentModal: null
}

const handlers = {
  [actions.setDebugModal]: (state, action) => ({
    ...state,
    currentModal: action.payload
  })
}

export default handleActions(handlers, defaultState)
