import React, { Component } from 'react'
import { Text, View, ListView, TextInput } from 'react-native'
import * as _ from 'underscore'
// import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/DebugModalView'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import ModalFrame from '../../../common/components/widgets/ModalFrame'
import { RadioGroup } from '../../../common/components/elements/Inputs'
// import baseStyle from 'common/styles/baseStyle'

export default class DebugModalView extends Component {
  static propTypes = {
    modal: PropTypes.object
  }

  constructor (props) {
    super(props)
    if (props.modal && props.modal.options) {
      this.state = { inputValues: this._initialInputValues(props.modal.options) }
    }
  }

  _renderOption (option) {
    return (
      <View style={styles.optionLayout}>
        <Text style={styles.optionTitleText}>{option.title}</Text>
        {this._renderOptionContent(option)}
      </View>
    )
  }

  _renderOptionContent (option) {
    switch (option.type) {
      case 'radio':
        const options = JSON.parse(JSON.stringify(option.options))
        const value = this.state.inputValues[option.key]
        const selectedOption = options.find(option => option.value === value)
        if (selectedOption) {
          selectedOption.selected = true
        }

        return (
          <View style={styles.radioLayout} >
            <RadioGroup
              options={options}
              onSelectItem={item => this.setState({ inputValues: {
                ...this.state.inputValues,
                [option.key]: item.value
              }})} />
          </View>
        )

      case 'text':
        return (
          <View style={styles.textLayout}>
            <TextInput
              style={styles.textInput}
              onChangeText={(text) => this.setState({ inputValues: {
                  [option.key]: text
                }})}
            />
          </View>
        )
    }
  }

  componentWillReceiveProps (props) {
    if (props.modal && props.modal.options) {
      this.setState({ inputValues: this._initialInputValues(props.modal.options) })
    }
  }

  _initialInputValues (options, prevValues = {}) {
    const values = {}
    console.log('_initialInputValues --', {options})
    options.forEach((option) => {
      switch (option.type) {
        case 'radio':
          let selectedIndex = prevValues[option.key]
          if (selectedIndex === undefined && option.selectedValue !== undefined) {
            selectedIndex = _.findIndex(option.options, op => op.value === option.selectedValue)
          }
          values[option.key] = selectedIndex
      }
    })
    return values
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const modal = this.props.modal
    if (modal) {
      if (modal.content) {
        const Content = modal.content
        return (
          <View style={styles.layoutRoot}>
            <Content {...modal.props} />
          </View>
        )
      } else {
        return (
          <View style={styles.layoutRoot}>
            <ModalFrame disableCloseButton title={modal.title} style={styles.modalFrame}>
              <View style={styles.modalContent}>
                <View style={styles.optionsArea}>
                  <ListView
                    dataSource={dataSource.cloneWithRows(modal.options)}
                    renderRow={option => this._renderOption(option)}
                  />
                </View>
                <View style={styles.buttonArea}>
                  {modal.commands.map(command => (
                    <View key={command.label} style={styles.commandButtonContainer}>
                      <CommandButton
                        key={command.label}
                        style={styles.commandButton}
                        text={command.label}
                        onPress={() => command.onPress(this.state.inputValues)}
                      />
                    </View>
                  ))}
                </View>
              </View>
            </ModalFrame>
          </View>
        )
      }
    } else {
      return <View />
    }
  }
}
