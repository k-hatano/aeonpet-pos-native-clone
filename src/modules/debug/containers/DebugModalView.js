import { connect } from 'react-redux'
import DebugModalView from '../components/DebugModalView'
import {} from '../actions'
import { MODULE_NAME } from '../models'
// import AlertView from '../../../common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  modal: state[MODULE_NAME].currentModal
})

export default connect(mapStateToProps, mapDispatchToProps)(DebugModalView)
