import StandardCashVoucherRepository from './standard/StandardCashVoucherRepository'
import SampleCashVoucherRepository from './sample/SampleCashVoucherRepository'
import TrainingCashVoucherRepository from './training/TrainingCashVoucherRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashVoucherRepository {
  static _implement = new SampleCashVoucherRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  /**
   * バーコードから金券を取得する
   * 
   * @param barcode 
   */
  static async findByBarcode (barcode) {
    return this._implement.findByBarcode(barcode)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashVoucherRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingCashVoucherRepository()
        break

      default:
        this._implement = new SampleCashVoucherRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashVoucherRepository', context => {
  CashVoucherRepository.switchImplement(context)
})
