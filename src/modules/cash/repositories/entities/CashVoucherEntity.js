import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import PaymentMethod from '../../../payment/repositories/entities/PaymentMethodEntity'

const CashVoucher = sequelize.define(
  'CashVoucher',
  {
    id: {
      allowNull: true,
      primaryKey: true,
      type: Sequelize.STRING(16)
    },
    barcode: {
      allowNull: true,
      type: Sequelize.STRING(128)
    },
    amount: {
      allowNull: true,
      type: Sequelize.DECIMAL(19, 4)
    },
    issuer: {
      allowNull: true,
      type: Sequelize.STRING
    },
    brand: {
      allowNull: true,
      type: Sequelize.STRING
    },
    payment_method_id: {
      allowNull: true,
      type: Sequelize.STRING(16)
    },
    issue_type: {
      allowNull: true,
      type: Sequelize.INTEGER(3)
    },
    created_at: {
      type: Sequelize.DATE,
      defaultValue: null
    },
    updated_at: {
      type: Sequelize.DATE,
      defaultValue: null
    },
    deleted_at: {
      type: Sequelize.DATE,
      defaultValue: null
    }
  },
  {
    tableName: 'cash_voucher',
    timestamps: false,
    freezeTableName: true
  }
)

CashVoucher.belongsTo(PaymentMethod, { foreignKey: 'payment_method_id', targetKey: 'id', as: 'payment_methods' })

export default CashVoucher
