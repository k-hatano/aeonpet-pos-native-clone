import { sampleCashVouchers } from '../../samples'

export default class sampleCashVoucherRepository {
  /**
   * バーコードを元に金券を取得する
   * サンプル金券のうちバーコードが一致する物を返却する
   * 
   * @param barcode
   * @returns サンプル金券 or null
   */
  findByBarcode (barcode) {
    return sampleCashVouchers.filter(voucher => voucher.barcode === barcode)[0]
  }
}
