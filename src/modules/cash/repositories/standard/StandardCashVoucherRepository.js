import CashVoucher from '../entities/CashVoucherEntity'
import PaymentMethod from '../../../payment/repositories/entities/PaymentMethodEntity'

export default class StandardCashVoucherRepository {
  async findAll () {
    return CashVoucher.findAll()
  }

  /**
   * バーコードを元に金券を取得する
   * 
   * @param barcode
   * @returns {Promise}
   */
  async findByBarcode (barcode) {
    return CashVoucher.findOne({
      where: {
        barcode
      },
      order: [['created_at', 'DESC']],
      raw: false,
      include: [
        {
          model: PaymentMethod,
          required: true,
          as: 'payment_methods'
        }
      ]
    })
  }
}
