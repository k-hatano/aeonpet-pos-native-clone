import { sampleCashVouchers } from '../../samples'
import { CASH_VOUCHER_PATTERN_FOR_TRAINING } from '../../model'

export default class TrainingCashVoucherRepository {
  /**
   * バーコードを元に金券を取得する
   * 冒頭が 984 のバーコードのみ金券と判断する
   * 金券と判断された場合、ランダムでつり有・つり無いずれかのサンプル金券を返却する
   * 
   * @param barcode
   * @returns サンプル金券 or null
   */
  findByBarcode (barcode) {
    if (barcode.match(CASH_VOUCHER_PATTERN_FOR_TRAINING) == null) {
      return null
    }

    const rand = Math.floor(Math.random() * 100) % 2
    if (rand === 0) {
      return sampleCashVouchers[0]
    }
    return sampleCashVouchers[1]
  }
}
