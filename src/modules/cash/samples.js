import * as _ from 'underscore'
import generateUuid from '../../common/utils/generateUuid'
import { PAYMENT_METHOD_TYPE } from '../payment/models'

export const sampleCashVoucherMap = {
  voucher1: {
    id: generateUuid(),
    barcode: '9848311402004',
    amount: 100,
    issuer: 'sample-issuer1',
    brand: 'sample-brand1',
    payment_method_id: 'e46d87bc1f544777844896d66561ae48',
    payment_methods: {
      id: 'e46d87bc1f544777844896d66561ae48',
      name: '券（つり有）',
      payment_method_type: PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE,
      is_offline: 1,
      sort_order: 15,
      acquire_code: null,
      created_at: 1535554800,
      updated_at: 1535555000
    },
    issuer_type: 1,
    created_at: 1535554800,
    updated_at: 1535555000
  },
  voucher2: {
    id: generateUuid(),
    barcode: '9848311440501',
    amount: 200,
    issuer: 'sample-issuer2',
    brand: 'sample-brand2',
    payment_method_id: '"0a5a0339487e4cc8893b31c981061f4d"',
    payment_methods: {
      id: '"0a5a0339487e4cc8893b31c981061f4d"',
      name: '券（つり無）',
      payment_method_type: PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE,
      is_offline: 1,
      sort_order: 16,
      acquire_code: null,
      created_at: 1535554800,
      updated_at: 1535555000
    },
    issuer_type: 107,
    created_at: 1535554800,
    updated_at: 1535555000
  }
}

export const sampleCashVouchers = _.values(sampleCashVoucherMap)
