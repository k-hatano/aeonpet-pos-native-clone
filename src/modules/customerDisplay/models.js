import CustomerDisplayManager from './models/CustomerDisplayManager'
import { isHalfChar, isSurrogatePair } from '../../common/models/Validator'
import logger from '../../common/utils/logger'

export const MODULE_NAME = 'customerDisplay'

export const PLACEMENT = {
  TOP: 1,
  BOTTOM: 2
}

export async function syncCustomerDisplay (content) {
  const topMessage = formatCustomerDisplayLine(content.top.left, content.top.right)
  const bottomMessage = formatCustomerDisplayLine(content.bottom.left, content.bottom.right)

  try {
    await CustomerDisplayManager.displayMessageToCustomerDisplay(topMessage, bottomMessage)
  } catch (error) {
    logger.warning(error)
    try {
      await CustomerDisplayManager.connectToCustomerDisplay().then(() => {
        syncCustomerDisplay(content)
      })
    } catch (error) {
      await logger.warning(error)
    }
  }
}

export function formatCustomerDisplayLine (left, right) {
  if (!left) left = ''
  if (!right) right = ''

  const spaceSize = 20 - textWidth(left) - textWidth(right)
  return left + ' '.repeat(spaceSize) + right
}

export function textWidth (text) {
  let width = 0
  if (text) {
    for (let count = 0; count < text.length; count++) {
      if (isHalfChar(text.charCodeAt(count))) {
        width += 1
      } else if (isSurrogatePair(text.charCodeAt(count), text.charCodeAt(count + 1))) {
        width += 2
        count++
      } else {
        width += 2
      }
    }
  }
  return width
}
