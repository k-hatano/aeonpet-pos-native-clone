import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  content: null
}

const handlers = {
  [actions.setCustomerDisplayText]: (state, content) => ({
    ...state,
    content
  })
}

export default handleActions(handlers, defaultState)
