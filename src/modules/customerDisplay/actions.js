import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setCustomerDisplayText = createAction(`${MODULE_NAME}_setCustomerDisplayText`)
