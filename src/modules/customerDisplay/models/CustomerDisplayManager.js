import { NativeModules } from 'react-native'
import logger from 'common/utils/logger'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { customerDisplayParameter } from '../../setting/models'
import { errorMessage, isSuccess } from '../../printer/models/PrinterResult'

class CustomerDisplayManager extends StoreAccessibleBase {
  displayBridge = NativeModules.DisplayBridge

  async connectToCustomerDisplay () {
    const customerDisplayMaker = this.getSetting(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER)
    if (customerDisplayMaker) {
      await this.displayBridge.setCustomerDisplayProperty({
        CUSTOMER_DISPLAY_STD: customerDisplayParameter(customerDisplayMaker).interface,
        CUSTOMER_DISPLAY_IP: this.getSetting(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.ADDRESS)
      })
      const display = this
      return new Promise((resolve, reject) => {
        display._connect(
          () => {
            resolve()
          },
          error => {
            reject(error)
          }
        )
      })
    }
  }

  async _connect (onSuccess, onError) {
    this.displayBridge.connectCustomerDisplay(async (bridgeError, result) => {
      const errorMessage = this._getErrorMessage(bridgeError, result.PRINTER_RESULT)
      if (errorMessage) {
        onError(errorMessage)
      }
      if (isSuccess(result.PRINTER_RESULT)) {
        onSuccess()
      }
    })
  }

  async displayMessageToCustomerDisplay (topMessage, bottomMessage) {
    const customerDisplayMaker = this.getSetting(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER)
    if (customerDisplayMaker) {
      const display = this
      return new Promise((resolve, reject) => {
        display._displayMessage(
          topMessage,
          bottomMessage,
          () => {
            resolve()
          },
          error => {
            reject(error)
          }
        )
      })
    }
  }

  async _displayMessage (topMessage = '', bottomMessage = '', onSuccess, onError) {
    const customerDisplayMaker = this.getSetting(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER)
    if (customerDisplayMaker) {
      this.displayBridge.displayMessage(topMessage, bottomMessage, async (bridgeError, result) => {
        const errorMessage = this._getErrorMessage(bridgeError, result.PRINTER_RESULT)
        if (errorMessage) {
          onError(errorMessage)
        }
        if (isSuccess(result.PRINTER_RESULT)) {
          onSuccess()
        }
      })
    }
  }

  async disconnect () {
    const customerDisplayMaker = this.getSetting(SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.MAKER)
    if (customerDisplayMaker) {
      this.displayBridge.disconnectCustomerDisplay((bridgeError, result) => {
        logger.debug(this._getErrorMessage(bridgeError, result))
      })
    }
  }

  _getErrorMessage (bridgeError, result) {
    if (bridgeError) {
      return Error(bridgeError.toSource())
    } else if (!isSuccess(result)) {
      return errorMessage(result)
    }
    return null
  }
}

export default new CustomerDisplayManager()
