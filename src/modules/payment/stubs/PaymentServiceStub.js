import EventEmitter from 'events'
import logger from 'common/utils/logger'
import VEGA3000DataConverter from '../models/VEGA3000/VEGA3000DataConverter'
import { PAYMENT_METHOD_TYPE, VEGA_RECODE_TYPE, VEGA_TELEGRAM_TYPE, VEGA_API_TYPE } from '../models'
import Telegram from '../models/VEGA3000/telegrams/TelegramBase'
import TelegramCredit from '../models/VEGA3000/telegrams/TelegramCredit'
import TelegramEMoney from '../models/VEGA3000/telegrams/TelegramEMoney'
import TelegramUnionPay from '../models/VEGA3000/telegrams/TelegramUnionPay'
import { PAYMENT_RESULT_CODE } from '../models/PaymentResultCode'

export default class PaymentServiceStub extends EventEmitter {
  static LOG_PREFIX = '[PaymentServiceStub]'
  _request = null
  _messageTimeoutId

  constructor () {
    super()
    this._open()
  }

  /**
   * = NativeEvemtEmitter.addListener()
   * @param {string} _key 
   * @param {function} callback 
   */
  addListener (_key, callback) {
    return this.on(_key, callback)
  }

  pay (object) {
    this.send(object.REQUEST)
  }

  refund (object) {
    this.send(object.REQUEST)
  }

  cancel () {
    const response = new Telegram()
    response.apiType = VEGA_API_TYPE.INTERRUPT
    response.resultCode = PAYMENT_RESULT_CODE.CANCEL
    clearTimeout(this._messageTimeoutId)
    this.emit('message', response.toString())
  }

  /**
   * = NativeEvemtEmitter.send()
   * @param {string} _string 
   */
  send (_string) {
    this._request = VEGA3000DataConverter.convertResponse(_string.REQUEST)
    if (this._request.apiType !== VEGA_API_TYPE.INTERRUPT) {
      this._message()
    }

    logger.debug('Training is ' + String(this._request.recodeType === VEGA_RECODE_TYPE.TRAINING))
  }

  setPaymentProperty (object) {
    console.log(object)
  }

  /**
   * = NativeEvemtEmitter.onopen
   */
  _open () {
    setTimeout(() => {
      logger.debug(PaymentServiceStub.LOG_PREFIX + ' open emit')
      this.emit('open')
    }, 500)
  }

  /**
   * = NativeEvemtEmitter.onmessage
   */
  _message () {
    this._messageTimeoutId = setTimeout(() => {
      const response = this._createResponse()
      if (response.resultCode === PAYMENT_RESULT_CODE.RESULT_INVALID) {
        this._error()
        return
      }

      logger.debug(PaymentServiceStub.LOG_PREFIX + ' message emit')
      this.emit('message', response.toString())
    }, 5000)
  }

  /**
   * = NativeEvemtEmitter.onerror
   */
  _error () {
    setTimeout(() => {
      logger.debug(this.LOG_PREFIX + ' error emit')
      this.emit('error', 'connection refused')
    }, 5000)
  }

  _createResponse () {
    const response = new Telegram()
    response.dataType = VEGA_TELEGRAM_TYPE.RESPONSE
    response.recodeType = '11'
    response.apiType = this._request.apiType
    response.resultCode = this._getResultCode()
    response.amount = this._request.amount
    response.receiptNumber = '01234'

    switch (this._request.apiType) {
      case VEGA_API_TYPE.CREDIT_PAY:
      case VEGA_API_TYPE.CREDIT_RETURN:
        response.detailData = new TelegramCredit()
        break
      case VEGA_API_TYPE.EMONEY_PAY:
      case VEGA_API_TYPE.EMONEY_RETURN:
        response.detailData = new TelegramEMoney()
        break
      case VEGA_API_TYPE.UNIONPAY_RETURN:
        response.detailData = new TelegramUnionPay()
        break
      case VEGA_API_TYPE.UNIONPAY_PAY:
        response.detailData = new TelegramUnionPay()
        response.detailData.guideMessage = 'コンタクトEMV(IC)クレジットカードからの拒否'
        break
      default:
        logger.debug('unsupported api type')
        break
    }

    return response
  }

  _getResultCode () {
    // クレジット取消、銀聯売上の時にエラーコードを返す
    switch (this._request.apiType) {
      case VEGA_API_TYPE.EMONEY_PAY:
        return PAYMENT_RESULT_CODE.RESULT_INVALID
      case VEGA_API_TYPE.UNIONPAY_PAY:
        return PAYMENT_RESULT_CODE.RESULT_CONTACT_EMV_DENIED
      case VEGA_API_TYPE.INTERRUPT:
        return PAYMENT_RESULT_CODE.RESULT_CANCEL
      default:
        return PAYMENT_RESULT_CODE.RESULT_SUCCESS
    }
  }

  /**
   * 
   * @param {string} _apiType 
   */
  _apiTypeToPaymentMethodType (_apiType) {
    switch (_apiType) {
      case VEGA_API_TYPE.CREDIT_PAY:
      case VEGA_API_TYPE.CREDIT_RETURN:
        return PAYMENT_METHOD_TYPE.CREDIT
      case VEGA_API_TYPE.EMONEY_PAY:
      case VEGA_API_TYPE.EMONEY_RETURN:
        return PAYMENT_METHOD_TYPE.EMONEY
      case VEGA_API_TYPE.UNIONPAY_PAY:
      case VEGA_API_TYPE.UNIONPAY_RETURN:
        return PAYMENT_METHOD_TYPE.UNION_PAY
      default:
        return null
    }
  }
}
