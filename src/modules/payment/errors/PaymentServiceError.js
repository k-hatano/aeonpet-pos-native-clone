import OrangeErrorBase from '../../../common/errors/OrangeErrorBase'

export default class PaymentServiceError extends OrangeErrorBase {
  _isUserCancel = false
  _message = ''
  _response = {}

  constructor (message, response, isUserCancel = false) {
    super('')
    this._message = message
    this._isUserCancel = isUserCancel
    this._response = response
  }

  get isUserCancel () {
    return this._isUserCancel
  }

  get message () {
    return this._message
  }

  get response () {
    return this._response
  }
}
