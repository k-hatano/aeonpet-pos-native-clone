import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'
import AppEvents from 'common/AppEvents'

const PaymentMethodEntity = sequelize.define(
  'PaymentMethod',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    name: Sequelize.STRING,
    payment_method_type: Sequelize.INTEGER,
    media_type: Sequelize.INTEGER,
    acquirer_code: Sequelize.STRING,
    sort_order: Sequelize.INTEGER,
    is_offline: Sequelize.INTEGER,
    created_at: Sequelize.INTEGER,
    updated_at: Sequelize.INTEGER
  }, {
    tableName: 'payment_methods',
    underscored: true,
    timestamps: false
  }
)

export default PaymentMethodEntity
