import fetcher from 'common/models/fetcher'
import { handleAxiosError } from 'common/errors'
import { encodeCriteria } from 'common/utils/searchUtils'

export default class StandardPaymentRepository {
  async fetchByOrderId (orderId) {
    try {
      const response = await fetcher.get('payments', {
        ...encodeCriteria({ order_id: orderId })
      })
      return response.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
}
