import PaymentMethodEntity from '../entities/PaymentMethodEntity'
import AppEvents from 'common/AppEvents'
import fetcher from 'common/models/fetcher'
import { samplePaymentMethods } from '../../samples'
import { handleAxiosError } from '../../../../common/errors'
import { sequelize } from 'common/DB'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import { OTHER_CARD } from '../../models'
import I18n from 'i18n-js'
import { fixPaymentMethodsFromRemoteToLocal } from '../../../syncData/models'

export default class StandardPaymentMethodRepository {
  async findAll () {
    return PaymentMethodEntity.findAll({
      order: [
        ['sort_order', 'ASC']
      ],
      raw: true
    })
  }

  async findByPaymentMethodType (paymentMethodType) {
    try {
      return PaymentMethodEntity.findAll({
        where: {
          payment_method_type: paymentMethodType
        },
        raw: true
      })
    } catch (error) {
      // TODO エラーハンドリング
    }
  }

  async createSamples () {
    const count = await PaymentMethodEntity.count()
    if (count === 0) {
      await PaymentMethodEntity.bulkCreate(samplePaymentMethods)
    }
  }

  async syncPaymentMethod () {
    let paymentMethods = []
    try {
      const paymentMethodResponse = await fetcher.get('payment-methods', encodeCriteria({ status: 1 }))
      paymentMethods = fixPaymentMethodsFromRemoteToLocal(paymentMethodResponse.data)
    } catch (error) {
      handleAxiosError(error)
    }
    try {
      await sequelize.transaction(async function (t) {
        await PaymentMethodEntity.destroy({ where: {} }, {transaction: t})
        await PaymentMethodEntity.bulkCreate(paymentMethods, {transaction: t})
      })
    } catch (err) {
      handleAxiosError(err)
    }
  }
}

AppEvents.onSampleDataCreate('StandardPaymentMethodRepository', async () => {
  return (new StandardPaymentMethodRepository()).createSamples()
})
