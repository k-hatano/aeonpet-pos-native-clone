import logger from 'common/utils/logger'
import { samplePaymentMethods } from '../../samples'

export default class SamplePaymentMethodRepository {
  async findAll () {
    return samplePaymentMethods
  }

  async findByPaymentMethodType (paymentMethodType) {
    return samplePaymentMethods.filter(
      paymentMethod => paymentMethod.payment_method_type === paymentMethodType)
  }

  async syncPaymentMethod () {
    logger.info('SamplePaymentMethodRepository.syncPaymentMethod', 'Repository')
  }
}
