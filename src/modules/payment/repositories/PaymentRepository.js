import SamplePaymentRepository from './sample/SamplePaymentRepository'
import StandardPaymentRepository from './standard/StandardPaymentRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class PaymentRepository {
  static _implement = new SamplePaymentRepository()

  static async fetchByOrderId (orderId) {
    return this._implement.fetchByOrderId(orderId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardPaymentRepository()
        break

      default:
        this._implement = new SamplePaymentRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('PaymentRepository', (context) => {
  PaymentRepository.switchImplement(context)
})
