import StandardPaymentMethodRepository from './standard/StandardPaymentMethodRepository'
import SamplePaymentMethodRepository from './sample/SamplePaymentMethodRepository'
import AppEvents, { REPOSITORY_CONTEXT } from '../../../common/AppEvents'

export default class PaymentMethodRepository {
  /**
   * @type {SamplePaymentMethodRepository}
   * @private
   */
  static _implement = new SamplePaymentMethodRepository()

  static async findAll () {
    return this._implement.findAll()
  }

  static async findByPaymentMethodType (paymentMethodType) {
    return this._implement.findByPaymentMethodType(paymentMethodType)
  }

  static async syncPaymentMethod () {
    return this._implement.syncPaymentMethod()
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardPaymentMethodRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardPaymentMethodRepository()
        break

      default:
        this._implement = new SamplePaymentMethodRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('PaymentMethodRepository', (context) => {
  PaymentMethodRepository.switchImplement(context)
})
