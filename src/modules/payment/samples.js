import { PAYMENT_METHOD_TYPE } from './models'

export const paymentIds = {
  cash: 'a901ed67c8e24b57aba17bfefcc10858',
  credit: '42d0866dccd84d71925589d90f7f378a',
  emoney: '666b04caafe244949d736092caf3248d',
  gift_with_change: '4503d822bcae43c99e5b85b806434e7a',
  gift_without_change: '3381f91a648f4e3aa5c2365dbff9e007',
  accounts_receivable: '6c879dce7f5144858499720f412f2555',
  other: 'bede7c326e064dc5b4f4befc08a5de1d',
  credit_visa: '399f23d3cfbf4a119135aef79dd18d27',
  credit_amex: '7efbd92e2a654e02ae021a29fbc3785f',
  credit_diners: '052f2de60fe44ec5ae6c82fe452e23f1',
  credit_jcb: '77259714f39940c088f2c9babd514da1',
  unionpay: '2c21ef64c81b49bfbdfac3941cb4f80c',
  credit_visa_offline: '503127ebeb084bd9aedb85890c964743',
  credit_amex_offline: 'a376d5beccf4427d84d6897e6813b3a4',
  credit_diners_offline: 'ff5c3eb6bb294de0b03db799b4b68ef0',
  credit_jcb_offline: 'c7d2d298306941de9b201020dd97161f',
  unionpay_offline: 'a1aced60cbb545c8952542b4fae3724a',
  external_point: '5411325D757E2904DF1DF029A378032E'
}

let sortOrder = 1

export const samplePaymentMethods = [
  {
    id: paymentIds.cash,
    name: '現金',
    payment_method_type: PAYMENT_METHOD_TYPE.CASH,
    payment_method_code: 'payment-method1',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit,
    name: 'カード',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'payment-method2',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.emoney,
    name: '電子マネー',
    payment_method_type: PAYMENT_METHOD_TYPE.EMONEY,
    payment_method_code: 'payment-method3',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.gift_with_change,
    name: '券つり有',
    payment_method_type: PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE,
    payment_method_code: 'payment-method4',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.gift_without_change,
    name: '券つり無',
    payment_method_type: PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE,
    payment_method_code: 'payment-method5',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.accounts_receivable,
    name: '売掛',
    payment_method_type: PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE,
    payment_method_code: 'payment-method6',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.other,
    name: 'その他',
    payment_method_type: PAYMENT_METHOD_TYPE.OTHER,
    payment_method_code: 'payment-method7',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit_visa,
    name: 'VISA',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_visa',
    acquirer_code: '04',
    media_type: '53',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit_amex,
    name: 'AMEX',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_amex',
    acquirer_code: '07',
    media_type: '54',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit_diners,
    name: 'DINERS',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_diners',
    acquirer_code: '01',
    media_type: '55',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit_jcb,
    name: 'JCB',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_jcb',
    acquirer_code: '02',
    media_type: '56',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.unionpay,
    name: '銀聯',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'unionpay',
    acquirer_code: '67',
    media_type: '68',
    sort_order: sortOrder++,
    is_offline: 0
  },
  {
    id: paymentIds.credit_visa_offline,
    name: 'VISA (OFF',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_visa_offline',
    acquirer_code: '04',
    media_type: '53',
    sort_order: sortOrder++,
    is_offline: 1
  },
  {
    id: paymentIds.credit_amex_offline,
    name: 'AMEX (OFF',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_amex_offline',
    acquirer_code: '07',
    media_type: '54',
    sort_order: sortOrder++,
    is_offline: 1
  },
  {
    id: paymentIds.credit_diners_offline,
    name: 'DINERS (OFF',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_diners_offline',
    acquirer_code: '01',
    media_type: '55',
    sort_order: sortOrder++,
    is_offline: 1
  },
  {
    id: paymentIds.credit_jcb_offline,
    name: 'JCB (OFF',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'credit_jcb_offline',
    acquirer_code: '02',
    media_type: '56',
    sort_order: sortOrder++,
    is_offline: 1
  },
  {
    id: paymentIds.unionpay_offline,
    name: '銀聯 (OFF',
    payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
    payment_method_code: 'unionpay_offline',
    acquirer_code: '67',
    media_type: '68',
    sort_order: sortOrder++,
    is_offline: 1
  },
  {
    id: paymentIds.external_point,
    name: '外部ポイント',
    payment_method_type: PAYMENT_METHOD_TYPE.EXTERNAL_POINT,
    payment_method_code: 'external_point',
    sort_order: sortOrder++,
    is_offline: 1
  }
]
