import logger from 'common/utils/logger'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import { PAYMENT_METHOD_TYPE, brandCodeToPaymentMethodType } from '../models'
import EmptyPaymentService from './EmptyPaymentService'
import VEGA3000PaymentService from './VEGA3000PaymentService'
import SettingKeys from '../../setting/models/SettingKeys'
import { PAYMENT_SERVICE } from '../../setting/models'
import ShopRepository from '../../shop/repositories/ShopRepository'
import PaymentServiceError from '../errors/PaymentServiceError'
import PaymentMethodRepository from 'modules/payment/repositories/PaymentMethodRepository'
import CartPayment from 'modules/cart/models/CartPayment'
import I18n from 'i18n-js'

export default class PaymentServiceManager extends StoreAccessibleBase {
  service = null

  /**
   *
   * @param {CartPayment} cartPayment
   * @returns {Promise}
   */
  async pay (cartPayment) {
    this.service = this._selectPaymentService(cartPayment)
    const result = await this.service.pay(cartPayment)
    if (result) {
      const paymentMethodType = brandCodeToPaymentMethodType(result.brandCode)
      if (paymentMethodType) {
        const paymentMethods = await PaymentMethodRepository.findByPaymentMethodType(paymentMethodType)
        cartPayment = CartPayment
          .createFromPaymentMethodEntity(paymentMethods[0], cartPayment.waonPointRate)
          .updateAmount(result.amount)
      }
      return cartPayment.setExtraData(result)
    } else {
      return null
    }
  }

  async refund (cartPayment) {
    if (!cartPayment.extra_data || !cartPayment.extra_data.type) {
      return null
    }

    this.service = this._selectPaymentServiceForRefund(cartPayment)
    const extraData = await this.service.refund(cartPayment)
    return cartPayment.setExtraData(extraData)
  }

  async cancel (cartPayment) {
    if (!cartPayment.extra_data || !cartPayment.extra_data.type) {
      return null
    }

    this.service = this._selectPaymentServiceForRefund(cartPayment)
    const extraData = await this.service.cancel(cartPayment)
    return cartPayment.setExtraData(extraData)
  }

  interrupt () {
    if (this.service == null) {
      return null
    }
    return this.service.interrupt()
  }

  async charge (balance) {
    switch (this._paymentMethodSettings) {
      case PAYMENT_SERVICE.VEGA3000:
        this.service = new VEGA3000PaymentService()
        break
      default:
        return new Promise((resolve, reject) => {
          reject(new PaymentServiceError(
            I18n.t('message.V-03-E001'),
            null,
            false
          ))
        })
    }
    const result = await this.service.charge(balance)
    return result
  }

  /**
   *
   * @param {CartPayment} cartPayment
   * @returns {Object}
   */
  _selectPaymentService (cartPayment) {
    if (this._isSupportedCCTPayment(cartPayment.paymentMethodType) && !cartPayment.isOffline) {
      const paymentService = this._paymentMethodSettings
      switch (paymentService) {
        case PAYMENT_SERVICE.VEGA3000:
          logger.debug('Use vega3000 payment service')
          return new VEGA3000PaymentService()
        case PAYMENT_SERVICE.NOT_SELECTED:
        default:
          logger.debug("Don't use payment service")
          return new EmptyPaymentService()
      }
    }

    logger.debug('Is not credit payment service')
    return new EmptyPaymentService()
  }

  paymentMethodIsConnectedToService (paymentMethod: Object) {
    if (paymentMethod.payment_method_type !== PAYMENT_METHOD_TYPE.CREDIT) {
      return false
    }
    return !!this._paymentMethodSettings
  }

  _selectPaymentServiceForRefund (cartPayment) {
    switch (cartPayment.extra_data.type) {
      case 'vega3000':
        return new VEGA3000PaymentService()
      default:
        logger.fatal('Invalid Payment Service Type For Refund')
    }

    return new EmptyPaymentService()
  }

  get _settings () {
    return {
      accountId: this.getSetting(SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ACCOUNT_ID),
      accountPassword: this.getSetting(SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.ACCOUNT_PASSWORD),
      merchantPassword: this.getSetting(SettingKeys.EQUIPMENT.PAYMENT_TERMINAL.MERCHANT_PASSWORD)
    }
  }

  async _createSetting () {
    const shop = await ShopRepository.find()
    if (shop) {
      return {
        accountId: shop.credit_user_id || '',
        merchantPassword: shop.password || ''
      }
    } else {
      return {
        accountId: '',
        merchantPassword: '',
        accountPassword: ''
      }
    }
  }

  get _paymentMethodSettings () {
    return this.getSetting(SettingKeys.ORDER.PAYMENT_SERVICE) || 0
  }

  _isSupportedCCTPayment (paymentMethodType) {
    return (
      paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT ||
      paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY ||
      paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY
    )
  }
}

// const paymentServiceManager = new PaymentServiceManager()
// export default paymentServiceManager
