export default class EmptyPaymentService {
  pay () {
    return null
  }

  refund () {
    return null
  }

  interrupt () {
    return null
  }

  charge () {
    return null
  }
}
