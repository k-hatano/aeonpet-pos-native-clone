import { PAYMENT_METHOD_TYPE, VEGA_API_TYPE, VEGA_EMONEY_BRAND, isCredit, isEMoneyWithPaymentMethodType, isEMoney } from 'modules/payment/models'
import Telegram from './telegrams/Telegram'
import TelegramCredit from './telegrams/TelegramCredit'
import TelegramEMoney from './telegrams/TelegramEMoney'
import TelegramUnionPay from './telegrams/TelegramUnionPay'

/**
 * 固定長形式のデータ文字列をオブジェクトに変換させる
 */
class VEGA3000DataConverter {
  constructor () {
    this.HEADER_BYTE = 38
    this.COMMON_DATA_BYTE = 91
  }

  /**
   * 要求電文を生成する
   * @param {VEGA_API_TYPE} _apiType 
   * @param {Number} _amount
   */
  convertRequest (_cartPayment, isRefund = false, returnType = ' ') {
    const resultEntity = new Telegram()
    resultEntity.apiType = this._paymentMethodTypeToApiType(_cartPayment.paymentMethodType, isRefund)
    // 金額は右詰、0パディング
    resultEntity.amount = this._formatAmount(_cartPayment.amount)
    resultEntity.detailData = this._getDetailDataSeciton(_cartPayment.paymentMethodType)

    // 取消返品はクレジットのみ可能
    if (isRefund) {
      resultEntity.receiptNumber = _cartPayment.extra_data.orderId
      resultEntity.detailData.cancelType = returnType
    }

    if (isEMoney(resultEntity)) {
      resultEntity.freeSpace = this._formatWAONPointRate(_cartPayment.waonPointRate)
    }

    return resultEntity
  }

  /**
   * 応答電文をオブジェクトにマッピングする
   * @param {String} _dataString 
   * @return {Telegram}
   */
  convertResponse (_dataString) {
    const resultEntity = this._stringToObject(_dataString, new Telegram())
    const detailDataString = _dataString.slice(this.HEADER_BYTE + this.COMMON_DATA_BYTE)
    if (resultEntity.apiType === VEGA_API_TYPE.INTERRUPT) {
      resultEntity.detailData = ''
      return resultEntity
    }
    const paymentMethodType = this._apiTypeToPaymentMethodType(resultEntity.apiType)
    resultEntity.detailData = this._stringToObject(detailDataString, this._getDetailDataSeciton(paymentMethodType))

    return resultEntity
  }

  /**
   * 処理中断API要求電文を作成する
   */
  createInterruptRequest () {
    const resultEntity = new Telegram()
    resultEntity.apiType = VEGA_API_TYPE.INTERRUPT
    return resultEntity
  }

  /**
   * 電子マネーチャージ電文を作成する  
   * WAONのみ
   */
  createEmoneyChargeRequest (balance) {
    const resultEntity = new Telegram()
    resultEntity.apiType = VEGA_API_TYPE.EMONEY_CHARGE
    resultEntity.amount = this._formatAmount(balance.amount)

    resultEntity.detailData = new TelegramEMoney()
    resultEntity.detailData.eMoneyBrandCode = VEGA_EMONEY_BRAND.WAON
    return resultEntity
  }

  _formatWAONPointRate (waonPointRate) {
    if (waonPointRate) {
      return String(waonPointRate).padEnd(new Telegram()._freeSpace.byte, ' ')
    }
    return String('').padEnd(new Telegram()._freeSpace.byte, ' ')
  }

  /**
   * 数値型の金額を固定長形式文字列に変換  
   * 0埋め
   * 
   * @param {Number} amount 
   * @return {String}
   */
  _formatAmount (amount) {
    return String(amount.toFixed(0)).padStart(new Telegram()._amount.byte, '0')
  }

  /**
   * 明細部エンティティ返却
   * @param {PAYMENT_METHOD_TYPE} paymentMethodType 
   */
  _getDetailDataSeciton (paymentMethodType) {
    if (isCredit(paymentMethodType)) {
      return new TelegramCredit()
    }

    if (isEMoneyWithPaymentMethodType(paymentMethodType)) {
      return new TelegramEMoney()
    }

    return new TelegramUnionPay()
  }

  /**
   * 固定長形式 → Entity
   * @param {string} _dataString 
   * @param {*} _targetEntity 
   */
  _stringToObject (_dataString, _targetEntity) {
    const keys = Object.keys(_targetEntity)
    let index = 0
    keys.forEach(key => {
      if (_targetEntity[key] && _targetEntity[key].byte) {
        _targetEntity[key].value = _dataString.substr(index, _targetEntity[key].byte)
        index += _targetEntity[key].byte
      }
    })
    return _targetEntity
  }

  /**
   * @param {PAYMENT_METHOD_TYPE} paymentMethodType 
   * @param {boolean} _isRefund 
   */
  _paymentMethodTypeToApiType (paymentMethodType, _isRefund = false) {
    if (isCredit(paymentMethodType)) {
      return _isRefund ? VEGA_API_TYPE.CREDIT_RETURN : VEGA_API_TYPE.CREDIT_PAY
    }

    if (isEMoneyWithPaymentMethodType(paymentMethodType)) {
      return VEGA_API_TYPE.EMONEY_PAY
    }

    return _isRefund ? VEGA_API_TYPE.UNIONPAY_RETURN : VEGA_API_TYPE.UNIONPAY_PAY
  }

  /**
   * @param {VEGA_API_TYPE} apiType 
   */
  _apiTypeToPaymentMethodType (apiType) {
    switch (apiType) {
      case VEGA_API_TYPE.CREDIT_RETURN:
      case VEGA_API_TYPE.CREDIT_PAY:
        return PAYMENT_METHOD_TYPE.CREDIT
      case VEGA_API_TYPE.EMONEY_RETURN:
      case VEGA_API_TYPE.EMONEY_PAY:
      case VEGA_API_TYPE.EMONEY_CHARGE:
        return PAYMENT_METHOD_TYPE.EMONEY
      case VEGA_API_TYPE.UNIONPAY_RETURN:
      case VEGA_API_TYPE.UNIONPAY_PAY:
        return PAYMENT_METHOD_TYPE.UNION_PAY
    }
  }
}

export default new VEGA3000DataConverter()
