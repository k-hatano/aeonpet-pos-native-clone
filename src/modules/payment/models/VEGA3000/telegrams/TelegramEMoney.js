import TelegramBase from './TelegramBase'

/**
 * 電文エンティティ（データ部個別項目 電子マネー）  
 * 未使用の項目は全て半角スペースをセット
 */
export default class TelegramEMoney extends TelegramBase {
  constructor () {
    super()

    /**
     * 電子マネー取引結果コード  
     * 決済端末のみ使用  
     * 3 Byte
     */
    this._eMoneyResultCode = {
      byte: 3,
      value: '   '
    }

    /**
     * 電子マネー取引エラーコード  
     * 決済端末のみ使用  
     * 3 Byte
     */
    this._eMoneyErrorCode = {
      byte: 3,
      value: '   '
    }

    /**
     * 電子マネーブランドコード  
     * 0002:楽天 Edy  
     * 0003:iD  
     * 0004:QUICPay  
     * 0005:交通系 IC  
     * 0006:WAON  
     * 0007:nanaco  
     * 0009:SAPICA  
     * 4 Byte
     */
    this._eMoneyBrandCode = {
      byte: 4,
      value: '    '
    }

    /**
     * シンカクラウド取引通番  
     * 取引種別：410「支払い取消し」  
     * 取引種別：411「チャージ取消し」  
     * 取引種別：491「直前取引内容要求」  
     * 上記以外、半角スペース  
     * 14 Byte
     */
    this._thincaCloudPaymentNumber = {
      byte: 14,
      value: '              '
    }

    /**
     * 端末取引通番  
     * 6 Byte
     */
    this._terminalPaymentNumber = {
      byte: 6,
      value: '      '
    }

    /**
     * 商品区分  
     * 決済端末のみ使用  
     * 未使用  
     * 4 Byte
     */
    this._productType = {
      byte: 4,
      value: '    '
    }

    /**
     * レシート印字情報のXMLデータ長  
     * 
     * 9 Byte
     */
    this._receiptByteLength = {
      byte: 9,
      value: '000000000'
    }

    /**
     * レシート印字情報をXML形式で返却  
     * 端末側でレシート印刷が行われない場合のみ  
     * 
     * 可変
     */
    this._receiptXML = {
      byte: 0,
      value: ''
    }
  }

  get eMoneyResultCode () {
    return this._eMoneyResultCode.value
  }
  set eMoneyResultCode (string) {
    this._eMoneyResultCode.value = string
  }

  get eMoneyErrorCode () {
    return this._eMoneyErrorCode.value
  }
  set eMoneyErrorCode (string) {
    this._eMoneyResultCode.value = string
  }

  get eMoneyBrandCode () {
    return this._eMoneyBrandCode.value
  }
  set eMoneyBrandCode (string) {
    this._eMoneyBrandCode.value = string
  }

  get thincaCloudPaymentNumber () {
    return this._thincaCloudPaymentNumber.value
  }
  set thincaCloudPaymentNumber (string) {
    this._thincaCloudPaymentNumber.value = string
  }

  get terminalPaymentNumber () {
    return this._terminalPaymentNumber.value
  }
  set terminalPaymentNumber (string) {
    this._terminalPaymentNumber.value = string
  }

  get productType () {
    return this._productType.value
  }
  set productType (string) {
    this._productType.value = string
  }

  get receiptByteLength () {
    return this._receiptByteLength.value
  }
  set receiptByteLength (string) {
    this._receiptByteLength.value = string
  }

  get receiptXML () {
    return this._receiptXML.value
  }
  set receiptXML (string) {
    this._receiptXML.value = string
  }
}
