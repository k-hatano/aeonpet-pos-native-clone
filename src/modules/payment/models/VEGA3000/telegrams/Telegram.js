import TelegramBase from './TelegramBase'
import TelegramEMoney from './TelegramEMoney'

/**
 * 電文エンティティ  
 * 未使用の項目は全て半角スペースをセット
 */
export default class Telegram extends TelegramBase {
  constructor () {
    super()

    /**
     * データ種別  
     * 2: 要求  
     * 3: 応答  
     * 1 Byte
     */
    this._dataType = {
      byte: 1,
      value: '2'
    }

    /**
     * レコード区分  
     * 10：POS/自動機から端末へ要求  
     * 20：POS/自動機から端末へ練習モード要求  
     *   
     * 11：端末からPOS/自動機への結果応答  
     * 21：端末からPOS/自動機へ練習モード結果応答  
     * 2 Byte
     */
    this._recodeType = {
      byte: 2,
      value: '10'
    }

    /**
     * 取引種別  
     * 100：「売上」  
     * 110：「取消」  
     * 150：銀聯「売上」  
     * 151：銀聯「取消」  
     * 200：NFC「売上」  
     * 210：NFC「取消」  
     * 400：電子マネー「支払」  
     * 401：電子マネー「チャージ」  
     * 410：電子マネー「支払取消」  
     * 411：電子マネー「チャージ取消」  
     * 999：「処理中断」
     * 3 Byte
     */
    this._apiType = {
      byte: 3,
      value: '100'
    }

    /**
     * 処理番号  
     * 0001 ~ 9999  
     * 4 Byte
     */
    this._processNumber = {
      byte: 4,
      value: '0001'
    }

    /**
     * 結果コード  
     * 決済端末のみ使用  
     * 3 Byte
     */
    this._resultCode = {
      byte: 3,
      value: '   '
    }

    /**
     * データ長  
     * ヘッダ部総バイト数：38 Byte  
     * データ共通部総バイト数：91 Byte  
     * データ個別部総バイト数：0 ~ 300 Byte  
     * 5 Byte
     */
    this._dataLength = {
      byte: 5,
      value: '00091'
    }

    /**
     * 自由エリア  
     * 会員情報記載欄  
     * 左詰、半角スペース埋め
     * 20 Byte
     */
    this._freeSpace = {
      byte: 20,
      value: '                    '
    }

    /**
     * 金額  
     * 右詰、0埋め  
     * 8 Byte
     */
    this._amount = {
      byte: 8,
      value: '00000000'
    }

    /**
     * 税・その他  
     * 未使用時、00000000  
     * 右詰、0埋め  
     * 8 Byte
     */
    this._tax = {
      byte: 8,
      value: '00000000'
    }

    /**
     * 決済日時  
     * 12 Byte  
     */
    this._paymentDate = {
      byte: 12,
      value: '            '
    }

    /**
     * 端末番号  
     * 決済端末のみ使用  
     * 13 Byte  
     */
    this._terminalNumber = {
      byte: 13,
      value: '             '
    }

    /**
     * 伝票番号  
     * 売上時、半角スペース  
     * 取消時、取消対象の伝票番号  
     * 5 Byte
     */
    this._receiptNumber = {
      byte: 5,
      value: '     '
    }

    /**
     * 会員番号  
     * 決済端末のみ使用   
     * 19 Byte
     */
    this._memberNumber = {
      byte: 19,
      value: '                   '
    }

    /**
     * カード所有者名  
     * 決済端末のみ使用  
     * 26 Byte
     */
    this._cardHolder = {
      byte: 26,
      value: '                          '
    }

    /**
     * 個別項目  
     * 各エンティティに依存  
     *
     * @see TeregramCredit
     * @see TelegramEMone
     * @see TelegramUnionPay
     */
    this._detailData = null

    Object.seal(this)
  }

  get dataType () {
    return this._dataType.value
  }
  set dataType (string) {
    this._dataType.value = string
  }

  get recodeType () {
    return this._recodeType.value
  }
  set recodeType (string) {
    this._recodeType.value = string
  }

  get apiType () {
    return this._apiType.value
  }
  set apiType (string) {
    this._apiType.value = string
  }

  get processNumber () {
    return this._processNumber.value
  }
  set processNumber (string) {
    this._processNumber.value = string
  }

  get resultCode () {
    return this._resultCode.value
  }
  set resultCode (string) {
    this._resultCode.value = string
  }

  get dataLength () {
    return this._dataLength.value
  }
  set dataLength (string) {
    this._dataLength.value = string
  }

  get freeSpace () {
    return this._freeSpace.value
  }
  set freeSpace (string) {
    this._freeSpace.value = string
  }

  get amount () {
    return this._amount.value
  }
  set amount (string) {
    this._amount.value = string
  }

  get tax () {
    return this._tax.value
  }
  set tax (string) {
    this._tax.value = string
  }

  get paymentDate () {
    return this._paymentDate.value
  }
  set paymentDate (string) {
    this._paymentDate.value = string
  }

  get terminalNumber () {
    return this._terminalNumber.value
  }
  set terminalNumber (string) {
    this._terminalNumber.value = string
  }

  get receiptNumber () {
    return this._receiptNumber.value
  }
  set receiptNumber (string) {
    this._receiptNumber.value = string
  }

  get memberNumber () {
    return this._memberNumber.value
  }
  set memberNumber (string) {
    this._memberNumber.value = string
  }

  get cardHolder () {
    return this._cardHolder.value
  }
  set cardHolder (string) {
    this._cardHolder.value = string
  }

  get detailData () {
    return this._detailData
  }
  set detailData (object) {
    this._detailData = object
  }

  toString () {
    if (this.detailData instanceof Object) {
      this.dataLength = String(this.detailData.length).padStart(this._dataLength.byte, '0')
    }

    const valueArray = Object.entries(this)
    valueArray.pop()

    const dataArray = []
    valueArray.forEach((element) => {
      dataArray.push(element.pop().value)
    })

    if (!this.detailData) {
      return dataArray.join('')
    }

    if (!(this.detailData instanceof TelegramEMoney) && this._detailData.guideMessage.length === 0) {
      let guideMessage = ''
      const guideMessageLength = this.detailData._guideMessage.byte
      for (let i = 0; i < guideMessageLength; i++) {
        guideMessage = guideMessage + ' '
      }
      this.detailData.guideMessage = guideMessage
    }

    if (this.detailData == null) {
      return dataArray.join('')
    }

    return dataArray.concat(this._detailData.toArray()).join('')
  }
}
