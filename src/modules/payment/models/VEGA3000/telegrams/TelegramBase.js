export default class TelegramBase {
  toArray () {
    const dataArray = []
    Object.entries(this).forEach(element => {
      const value = element.pop().value
      if (value) {
        dataArray.push(value)
      }
    })

    return dataArray
  }

  get length () {
    let result = 0
    const all = Object.keys(this)
    all.forEach(key => {
      const element = this[key]
      if (element != null) {
        if (element.byte >= 0) {
          result += element.byte
        } else if (element instanceof Object) {
          result += element.length
        }
      }
    })

    return result
  }
}
