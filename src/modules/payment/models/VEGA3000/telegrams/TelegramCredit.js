import TelegramBase from './TelegramBase'

/**
 * 電文エンティティ（データ部個別項目 クレジット）  
 * 未使用の項目は全て半角スペースをセット
 */
export default class TelegramCredit extends TelegramBase {
  constructor () {
    super()

    /**
     * 支払区分  
     * 左詰め（余白は半角スペース）  
     * 3 Byte
     */
    this._jpo = {
      byte: 3,
      value: '   '
    }

    /**
     * クレジットカード会社ID  
     * 決済端末のみ使用  
     * 3 Byte
     */
    this._kid = {
      byte: 3,
      value: '   '
    }

    /**
     * 処理識別  
     * 決済端末のみ使用  
     * 1：MS（磁気ストライプ）  
     * 2：IC（接触EMV）  
     * 3：NFC決済（非接触EMV）
     * 1 Byte
     */
    this._processIdentifier = {
      byte: 1,
      value: ' '
    }

    /**
     * 会社略号  
     * 決済端末のみ使用
     * 10 Byte
     */
    this._companyShortName = {
      byte: 10,
      value: '          '
    }

    /**
     * カード有効期限  
     * 決済端末のみ使用  
     * 4 Byte
     */
    this._expirationDate = {
      byte: 4,
      value: '    '
    }

    /**
     * 分割回数  
     * 01~99  
     * 未使用時、半角スペース  
     * 2 Byte
     */
    this._installments = {
      byte: 2,
      value: '  '
    }

    /**
     * ボーナス月  
     * 01~12  
     * 未使用時、半角スペース  
     * 2 Byte
     */
    this._bonusMonth = {
      byte: 2,
      value: '  '
    }

    /**
     * カード会社承認番号  
     * 取引種別：130「承認後売り上げ」以外、半角スペース  
     * 7 Byte
     * @see {@link TelegramEntity.apiType}
     */
    this._approveNumber = {
      byte: 7,
      value: '       '
    }

    /**
     * 商品区分コード  
     * 未使用時、半角スペース  
     * 3 Byte
     */
    this._productTypeCode = {
      byte: 3,
      value: '   '
    }

    /**
     * 取消区分    
     * 取引種別：110「取消」以外、半角スペース  
     * 1: 売上取消（当日）  
     * 2: 売上返品（翌日以降）  
     * 3: 承認後売上取消  
     * 4: 承認後売上返品  
     * 5: オーソリ予約取消  
     * 6: オーソリ予約返品  
     * 1 Byte
     * @see {@link TelegramEntity.apiType}
     */
    this._cancelType = {
      byte: 1,
      value: ' '
    }

    /**
     * 加盟店名  
     * 決済端末のみ使用  
     * 23 Byte
     */
    this._memberShopName = {
      byte: 23,
      value: '                       '
    }

    /**
     * 加盟店TEL番号  
     * 決済端末のみ使用  
     * 23 Byte
     */
    this._memberShopTel = {
      byte: 23,
      value: '                       '
    }

    /**
     * ご案内メッセージ  
     * 決済端末のみ使用  
     * 136 Byte
     */
    this._guideMessage = {
      byte: 136,
      value: ''
    }

    /**
     * アプリケーション識別子  
     * 決済端末のみ使用  
     * 32 Byte
     */
    this._aid = {
      byte: 32,
      value: '                                '
    }

    /**
     * 未使用  
     * 半角スペース固定  
     * 1 Byte
     */
    this._notUse = {
      byte: 1,
      value: ' '
    }

    /**
     * アプリケーション取引カウンタ  
     * 決済端末のみ使用  
     * 5 Byte
     */
    this._atc = {
      byte: 5,
      value: '     '
    }

    /**
     * ICブランド名  
     * 決済端末のみ使用  
     * 10 Byte
     */
    this._icBrandName = {
      byte: 10,
      value: '          '
    }

    /**
     * NFCラベル  
     * 決済端末のみ使用  
     * 32 Byte
     */
    this._nfcLabel = {
      byte: 32,
      value: '                                '
    }

    /**
     * カードシーケンス番号  
     * 決済端末のみ使用  
     * 2 Byte
     */
    this._cardSequenceNumber = {
      byte: 2,
      value: '  '
    }
  }

  get jpo () {
    return this._jpo.value
  }
  set jpo (string) {
    this._jpo.value = string
  }

  get kid () {
    return this._kid.value
  }
  set kid (string) {
    this._kid.value = string
  }

  get processIdentifier () {
    return this._processIdentifier.value
  }
  set processIdentifier (string) {
    this._processIdentifier.value = string
  }

  get companyShortName () {
    return this._companyShortName.value
  }
  set companyShortName (string) {
    this._companyShortName.value = string
  }

  get expirationDate () {
    return this._expirationDate.value
  }
  set expirationDate (string) {
    this._expirationDate.value = string
  }

  get installments () {
    return this._installments.value
  }
  set installments (string) {
    this._installments.value = string
  }

  get bonusMonth () {
    return this._bonusMonth.value
  }
  set bonusMonth (string) {
    this._bonusMonth.value = string
  }

  get approveNumber () {
    return this._approveNumber.value
  }
  set approveNumber (string) {
    this._approveNumber.value = string
  }

  get productTypeCode () {
    return this._productTypeCode.value
  }
  set productTypeCode (string) {
    this._productTypeCode.value = string
  }

  get cancelType () {
    return this._cancelType.value
  }
  set cancelType (string) {
    this._cancelType.value = string
  }

  get memberShopName () {
    return this._memberShopName.value
  }
  set memberShopName (string) {
    this._memberShopName.value = string
  }

  get memberShopTel () {
    return this._memberShopTel.value
  }
  set memberShopTel (string) {
    this._memberShopTel.value = string
  }

  get guideMessage () {
    return this._guideMessage.value
  }
  set guideMessage (string) {
    this._guideMessage.value = string
  }

  get aid () {
    return this._aid.value
  }
  set aid (string) {
    this._aid.value = string
  }

  get notUse () {
    return this._notUse.value
  }
  set notUse (string) {
    this._notUse.value = string
  }

  get atc () {
    return this._atc.value
  }
  set atc (string) {
    this._atc.value = string
  }

  get icBrandName () {
    return this._icBrandName.value
  }
  set icBrandName (string) {
    this._icBrandName.value = string
  }

  get nfcLabel () {
    return this._nfcLabel.value
  }
  set nfcLabel (string) {
    this._nfcLabel.value = string
  }

  get cardSequenceNumber () {
    return this._cardSequenceNumber.value
  }
  set cardSequenceNumber (string) {
    this._cardSequenceNumber.value = string
  }
}
