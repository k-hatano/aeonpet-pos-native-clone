import TelegramBase from './TelegramBase'

/**
 * 電文エンティティ（データ部個別項目 電子マネー）
 * 未使用の項目は全て半角スペースをセット
 */
export default class TelegramUnionPay extends TelegramBase {
  constructor () {
    super()

    /**
     * 支払区分  
     * 取引種別：150「売上」の時、「10：一括」  
     * 取引種別：151「取消」の時、「110：一括」  
     * 左詰、半角スペース埋め  
     * 3 Byte
     */
    this._jpo = {
      byte: 3,
      value: '   '
    }

    /**
     * 金融機関名  
     * 決済端末のみ使用  
     * 20 Byte
     */
    this._bankName = {
      byte: 20,
      value: '                    '
    }

    /**
     * アクワイアラID  
     * 決済端末のみ使用  
     * 11 Byte
     */
    this._acquirerId = {
      byte: 11,
      value: '           '
    }

    /**
     * センター処理通番  
     * 決済端末のみ使用  
     * 7 Byte
     */
    this._centerProcessNumber = {
      byte: 7,
      value: '       '
    }

    /**
     * カード有効期限  
     * 決済端末のみ使用  
     * YYMM  
     * 4 Byte
     */
    this._expirationDate = {
      byte: 4,
      value: '    '
    }

    /**
     * カード会社承認番号  
     * 取引種別：130「承認後売り上げ」以外、半角スペース  
     * 7 Byte
     *@see {@link TelegramEntity.apiType}
     */
    this._approveNumber = {
      byte: 7,
      value: '       '
    }

    /**
     * 銀聯番号  
     * MMDDHHMMSS  
     * 6 Byte
     */
    this._unionPayNumber = {
      byte: 6,
      value: '      '
    }

    /**
     * 銀聯送信日時  
     * 10 Byte
     */
    this._unionPaySendDate = {
      byte: 10,
      value: '          '
    }

    /**
     * 商品区分コード  
     * 端末のみ使用
     * 3 Byte
     */
    this._productTypeCode = {
      byte: 3,
      value: '   '
    }

    /**
     * 取消区分    
     * 取引種別：110「取消」以外、半角スペース  
     * 1: 売上取消（当日）  
     * 2: 売上返品（翌日以降）  
     * 3: 承認後売上取消  
     * 4: 承認後売上返品  
     * 5: オーソリ予約取消  
     * 6: オーソリ予約返品  
     * 1 Byte
     * @see {@link TelegramEntity.apiType}
     */
    this._canselType = {
      byte: 1,
      value: ' '
    }

    /**
     * 加盟店名  
     * 決済端末のみ使用  
     * 23 Byte
     */
    this._memberShopName = {
      byte: 23,
      value: '                       '
    }

    /**
     * 加盟店TEL番号  
     * 決済端末のみ使用  
     * 23 Byte
     */
    this._memberShopTel = {
      byte: 23,
      value: '                       '
    }

    /**
     * ご案内メッセージ  
     * 決済端末のみ使用  
     * 136 Byte
     */
    this._guideMessage = {
      byte: 136,
      value: ''
    }

    /**
     * 処理識別  
     * 1：MS（磁気ストライプ）  
     * 2：IC（接触EMV）
     * 1 Byte
     */
    this._processIdentifier = {
      byte: 1,
      value: ' '
    }

    /**
     * アプリケーション識別子  
     * 決済端末のみ使用  
     * 32 Byte
     */
    this._aid = {
      byte: 32,
      value: '                                '
    }

    /**
     * アプリケーション取引カウンタ  
     * 決済端末のみ使用  
     * 5 Byte
     */
    this._atc = {
      byte: 5,
      value: '     '
    }

    /**
     * カードシーケンス番号  
     * 決済端末のみ使用  
     * 2 Byte
     */
    this._cardSequenceNumber = {
      byte: 2,
      value: '  '
    }
  }

  get jpo () {
    return this._jpo.value
  }
  set jpo (string) {
    this._jpo.value = string
  }

  get bankName () {
    return this._bankName.value
  }
  set bankName (string) {
    this._bankName.value = string
  }

  get acquirerId () {
    return this._acquirerId.value
  }
  set acquirerId (string) {
    this._acquirerId.value = string
  }

  get centerProcessNumber () {
    return this._centerProcessNumber.value
  }
  set centerProcessNumber (string) {
    this._centerProcessNumber.value = string
  }

  get expirationDate () {
    return this._expirationDate.value
  }
  set expirationDate (string) {
    this._expirationDate.value = string
  }

  get approveNumber () {
    return this._approveNumber.value
  }
  set approveNumber (string) {
    this._approveNumber.value = string
  }

  get unionPayNumber () {
    return this._unionPayNumber.value
  }
  set unionPayNumber (string) {
    this._unionPayNumber.value = string
  }

  get unionPaySendDate () {
    return this._unionPaySendDate.value
  }
  set unionPaySendDate (string) {
    this._unionPaySendDate.value = string
  }

  get productTypeCodejpo () {
    return this._productTypeCode.value
  }
  set productTypeCode (string) {
    this._productTypeCode.value = string
  }

  get canselType () {
    return this._canselType.value
  }
  set canselType (string) {
    this._canselType.value = string
  }

  get memberShopName () {
    return this._memberShopName.value
  }
  set memberShopName (string) {
    this._memberShopName.value = string
  }

  get memberShopTel () {
    return this._memberShopTel.value
  }
  set memberShopTel (string) {
    this._memberShopTel.value = string
  }

  get guideMessage () {
    return this._guideMessage.value
  }
  set guideMessage (string) {
    this._guideMessage.value = string
  }

  get processIdentifier () {
    return this._processIdentifier.value
  }
  set processIdentifier (string) {
    this._processIdentifier.value = string
  }

  get aid () {
    return this._aid.value
  }
  set aid (string) {
    this._aid.value = string
  }

  get atc () {
    return this._atc.value
  }
  set atc (string) {
    this._atc.value = string
  }

  get cardSequenceNumber () {
    return this._cardSequenceNumber.value
  }
  set cardSequenceNumber (string) {
    this._cardSequenceNumber.value = string
  }
}
