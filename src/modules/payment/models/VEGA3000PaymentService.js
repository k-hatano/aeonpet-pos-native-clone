import PaymentServiceError from '../errors/PaymentServiceError'
import { isSuccess, isUserCancel, errorMessage } from 'modules/payment/models/PaymentResultCode'
import VEGA3000DataConverter from './VEGA3000/VEGA3000DataConverter'
import { VEGA_API_TYPE, VEGA_RETURN_TYPE, isWaon, isEMoney, eMoneyBrandCodeToBrandName } from '../models'
import NetworkError from 'common/errors/NetworkError'
import logger from 'common/utils/logger'
import { PAYMENT_RESULT_CODE } from './PaymentResultCode'
import { STATUS } from 'modules/order/models'
import TelegramEMoney from './VEGA3000/telegrams/TelegramEMoney'
import Telegram from './VEGA3000/telegrams/Telegram'
import { NativeEventEmitter, NativeModules } from 'react-native'
import I18n from 'i18n-js'
import settingGetter from 'modules/setting/models/settingGetter'
const { PaymentBridge } = NativeModules

/**
 * VEGA3000決済端末に対し、連動APIを送信する
 */
export default class VEGA3000PaymentService {
  LOG_PREFIX = `[${this.constructor.name}]`

  constructor () {
    PaymentBridge.setPaymentProperty({
      ADDRESS: settingGetter.paymentTerminalAddress,
      PORT: settingGetter.paymentTerminalPort,
      TIMEOUT_SEC: 15
    })
    this.emitter = new NativeEventEmitter(PaymentBridge)
  }

  /**
   * 売上APIを送信する
   * 
   * @param {Cart} _cartPayment 
   */
  pay (_cartPayment) {
    const requestData = VEGA3000DataConverter.convertRequest(_cartPayment)
    return this._listen({REQUEST: requestData.toString()})
  }

  /**
   * 取消APIを送信する
   */
  refund (_cartPayment) {
    const requestData = VEGA3000DataConverter.convertRequest(_cartPayment, true, VEGA_RETURN_TYPE.RETURN)
    return this._listen({REQUEST: requestData.toString()})
  }

  cancel (_cartPayment) {
    const requestData = VEGA3000DataConverter.convertRequest(_cartPayment, true, VEGA_RETURN_TYPE.CANCEL)
    return this._listen({REQUEST: requestData.toString()})
  }

  /**
   * 処理中断APIを送信する
   */
  interrupt () {
    return new Promise((resolve, reject) => {
      PaymentBridge.isOpen(isOpen => {
        if (!isOpen) {
          resolve()
          return
        }

        PaymentBridge.getEvents(events => {
          this.emitter.addListener(events.INTERRUPT, data => {
            const response = VEGA3000DataConverter.convertResponse(data)
            if (isSuccess(response.resultCode)) {
              resolve()
            } else {
              reject(response)
            }
          })
        })

        const requestData = VEGA3000DataConverter.createInterruptRequest()
        PaymentBridge.sendInterrupt({REQUEST: requestData.toString()})
      })
    })
  }

  charge (balance) {
    const requestData = VEGA3000DataConverter.createEmoneyChargeRequest(balance)
    return this._listen({REQUEST: requestData.toString()})
  }

  /**
   *
   */
  _listen (_requestData) {
    return new Promise((resolve, reject) => {
      PaymentBridge.getEvents(events => {
        this.emitter.addListener(events.OPEN, message => {
          logger.info(this._concatLogPrefix(`[${events.OPEN}] ` + message))
        })

        this.emitter.addListener(events.RECIVE, data => {
          logger.debug(this._concatLogPrefix(`[${events.RECIVE}] ` + data))
          const response = VEGA3000DataConverter.convertResponse(data)
          if (isSuccess(response.resultCode)) {
            resolve(this._responseToDto(response))
          } else {
            reject(new PaymentServiceError(
              this._getErrorMessage(response),
              this._responseToDto(response),
              isUserCancel(response.resultCode)
            ))
          }
        })

        this.emitter.addListener(events.ERROR, errorMessage => {
          logger.warning(this._concatLogPrefix(`[${events.ERROR}] ` + errorMessage))
          reject(new NetworkError(NetworkError.TYPE.ANY, I18n.t('message.V-99-E901')))
          this._removeListener(events)
        })

        this.emitter.addListener(events.CLOSE, message => {
          logger.info(this._concatLogPrefix(`[${events.CLOSE}] ` + message))
          this._removeListener(events)
        })

        PaymentBridge.send(_requestData)
      })
    })
  }

  _removeListener (events) {
    for (let key in events) {
      this.emitter.removeAllListeners(events[key])
    }
  }

  /**
   * @param {Telegram} _response 
   */
  _responseToDto (_response) {
    return {
      type: 'vega3000',
      orderId: this._getOrderId(_response),
      amount: _response.amount,
      authCode: _response.detailData.approveNumber,
      acquirerCode: null,
      resultCode: _response.resultCode,
      status: this._getStatus(_response),
      paymentTime: _response.paymentDate,
      jpo: isEMoney(_response) ? null : _response.detailData.jpo,
      emvAppLabel: null,
      cardNumber: _response.memberNumber,
      cardType: isEMoney(_response) ? null : _response.detailData.processIdentifier,
      brandCode: isEMoney(_response) ? _response.detailData.eMoneyBrandCode : _response.detailData.kid,
      brandName: isEMoney(_response) ? eMoneyBrandCodeToBrandName(_response.detailData.eMoneyBrandCode) : _response.detailData.icBrandName,
      waonPointRate: isWaon(_response) ? parseInt(_response.freeSpace) : null
    }
  }

  /**
   * @param {Telegram} _response 
   */
  _getErrorMessage (_response) {
    let message = _response.detailData.guideMessage
    if (message) {
      message = message.trim()
      if (message.length > 0) {
        return message
      }
    }

    if (isEMoney(_response)) {
      message = errorMessage(_response.detailData.eMoneyErrorCode, isWaon(_response))
      if (message.length > 0) {
        return message
      }
    }
    return errorMessage(_response.resultCode)
  }

  /**
   * @param {Telegram} _response 
   */
  _getStatus (_response) {
    switch (_response.resultCode) {
      case PAYMENT_RESULT_CODE.RESULT_SUCCESS:
        if (_response.apiType === VEGA_API_TYPE.CREDIT_RETURN ||
            _response.apiType === VEGA_API_TYPE.EMONEY_RETURN ||
            _response.apiType === VEGA_API_TYPE.UNIONPAY_RETURN) {
          return STATUS.RETURN_COMPLETED
        }
        return STATUS.COMPLETED
      default:
        return STATUS.STATUS_FAILED
    }
  }

  /**
   * @param {Telegram} _response 
   */
  _getOrderId (_response) {
    if (isEMoney(_response)) {
      return _response.detailData.thincaCloudPaymentNumber
    }
    return _response.receiptNumber
  }

  /**
   * @param {Telegram} _response 
   */
  _isEMoney (_entity) {
    return _entity.detailData instanceof TelegramEMoney
  }

  _concatLogPrefix (_log) {
    return this.LOG_PREFIX + ' ' + _log
  }
}
