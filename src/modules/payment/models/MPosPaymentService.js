import moment from 'moment'
import { NativeModules } from 'react-native'
import logger from 'common/utils/logger'
import PaymentResultCode from 'modules/payment/models/PaymentResultCode'
import PaymentServiceError from '../errors/PaymentServiceError'

/**
 * @typedef {Object} MPosResponse
 * @property {string} acquirerCode
 * @property {string} amount
 * @property {string} authCode
 * @property {string} cardNumber
 * @property {string} cardProviderCode
 */

/*
acquirerCode:"04"
amount:"123"
authCode:"000000"
cardNumber:"000000********00"
cardProviderCode:"VI"
cardType:"IC"
cvmr:"1e0300"
emvAppId:"a0000000031010"
emvAppLabel:"VISACREDIT"
emvAtc:"0007"
jpo:"10"
orderDescription:"Orange Operation お会計"
orderId:"003Fuo"
panSeqNumber:"1"
paymentMethodCode:"001"
paymentTime:"20170906222711"
resultCode:"000"
status:"success"
 */
export default class MPosPaymentService {
  _settings = {}

  constructor (settings) {
    this._settings = settings
  }

  async pay (cartPayment) {
    await this._connect()
    return new Promise((resolve, reject) => {
      NativeModules.PaymentBridge.pay(
        {
          amount: cartPayment.amount.toFixed(0),
          // 金額を自由入力させる機能を使う場合は、下記２行を有効にし、画面のタイトルを指定する
          // orderDescription: '',
          // inputAmountView: '1',
          completeView: '1'
        },
        (error, response) => {
          const responseDto = this._responseToDto(response)
          if (!PaymentResultCode.isSuccess(response)) {
            logger.warning('mPos payment failed ' + JSON.stringify(responseDto))
            reject(new PaymentServiceError(
              PaymentResultCode.errorMessage(response),
              responseDto,
              PaymentResultCode.isUserCancel(response)
            ))
          } else {
            logger.info('mPos payment success ' + JSON.stringify(responseDto))
            resolve(responseDto)
          }
        }
      )
    })
  }

  async _connect () {
    return NativeModules.PaymentBridge.setPaymentProperty({
      PAYMENT_SDK: '0',
      USER_ID: this._settings.accountId,
      USER_PASSWORD: this._settings.accountPassword,
      USER_MERCHENT_PASSWORD: this._settings.merchantPassword,
      READER_TYPE: 'ctMP200',
      LOCALE: 'ja'
    })
  }

  _responseToDto (response) {
    return {
      type: 'mPos',
      orderId: response.orderId,
      amount: response.amount,
      authCode: response.authCode,
      acquirerCode: response.acquirerCode,
      resultCode: response.resultCode,
      status: response.status,
      paymentTime: response.paymentTime,
      jpo: response.jpo,
      emvAppLabel: response.emvAppLabel,
      cardNumber: response.cardNumber,
      cardType: response.cardType
    }
  }

  async refund (cartPayment, extraData) {
    await this._connect()
    return new Promise((resolve, reject) => {
      NativeModules.PaymentBridge.refund(
        {
          orderId: extraData.orderId
        },
        (error, response) => {
          const responseDto = this._responseToDto(response)
          if (!PaymentResultCode.isSuccess(response)) {
            logger.warning('mPos payment failed ' + JSON.stringify(responseDto))
            reject(new PaymentServiceError(
              PaymentResultCode.errorMessage(response),
              responseDto,
              PaymentResultCode.isUserCancel(response)
            ))
          } else {
            logger.info('mPos payment success ' + JSON.stringify(responseDto))
            responseDto.originOrderId = extraData.orderId
            resolve({
              ...responseDto,
              amount: extraData.amount,
              originOrderId: extraData.orderId,
              paymentTime: moment().format('YYYYMMDDHHmmss'),
              emvAppLabel: extraData.emvAppLabel,
              cardType: extraData.cardType
            })
          }
        }
      )
    })
  }
}

/*
以下、現在利用していないが利用可能な機能のサンプルコード
{
  title: 'mPOS Search',
    onPress: async () => {
  this.progress.show()
  NativeModules.PaymentBridge.search(
    {
      orderId: '002vZr',
      transactionCount: '10'
    },
    (error, events) => {
      if (!PaymentResultCode.isSuccess(events.resultCode)) {
        console.warn('mPOS Search', PaymentResultCode.errorMessage(events.resultCode))
      } else {
        console.warn('mPOS Search', JSON.stringify(events.historyList))
      }
    }
  )
  this.progress.hide()
}
},
{
  title: 'mPOS Receipt',
    onPress: async () => {
  this.progress.show()
  NativeModules.PaymentBridge.receipt(
    {
      orderId: '002vZr',
      email: 'keisuke.tanaka@aijus.com'
    },
    (error, event) => {
      if (!PaymentResultCode.isSuccess(event.resultCode)) {
        console.warn('mPOS Receipt', PaymentResultCode.errorMessage(event.resultCode))
      }
    }
  )
  this.progress.hide()
}
},
{
  title: 'mPOS Change password',
    onPress: async () => {
  this.progress.show()
  NativeModules.PaymentBridge.changePassword(
    {
      oldPassword: '', // 現在のパスワード
      newPassword: '' // 新しいパスワード
    },
    (error, event) => {
      if (!PaymentResultCode.isSuccess(event.resultCode)) {
        console.warn(
          'mPOS Change password',
          PaymentResultCode.errorMessage(event.resultCode)
        )
      }
    }
  )
  this.progress.hide()
}
}
*/
