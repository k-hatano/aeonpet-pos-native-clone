import I18n from 'i18n-js'

export const PAYMENT_RESULT_CODE = {
  RESULT_SUCCESS: '000',
  RESULT_IMPOSSIBLE_REQUEST: 'L05',
  RESULT_IMPOSSIBLE_SUSPEND: 'L06',
  RESULT_IMPOSSIBLE_PROCESS: 'L07',
  RESULT_TIMEOUT: 'L10',
  RESULT_CANCEL: 'L11',
  RESULT_VARIFYCATION_ERROR: 'L20',
  RESULT_INVALID_PARAM: 'L30',
  RESULT_INVALID: 'L31',
  RESULT_NETWORK_ERROR: 'L40',
  RESULT_CONTACT_EMV_ERROR_DETECTION: 'L80',
  RESULT_CONTACTLESS_EMV_ERROR_DETECTION: 'L90',
  RESULT_LOCAL_ERROR: 'L99',
  RESULT_CONTACT_EMV_DENIED: 'Z01',
  RESULT_CONTACTLESS_EMV_DENIED: 'Z02',
  RESULT_ONLINE_PROCESS_DENIED: 'Z03',
  RESULT_ERROR_FROM_EMONEY: 'T10'
}

const EMONEY_RESULT_CODE = {
  CANCEL: '101',
  ERR_INVALID_PARAM: '201',
  ERR_ALWAYS_PROCESSING: '202',
  ERR_CCT_CONNECT: '203',
  ERR_CONNECTION_TIMEOUT: '204',
  ERR_IN_CARD_READER: '205',
  CCT_ERR_BAD_STATUS: '206',
  CCT_ERR_NETWORK_DISABLED: '207',
  CCT_ERR_BAD_REQUEST: '303',
  CCT_ERR_BAD_SERVICE: '304',
  CCT_ERR_CHARGE_DEVICE: '308',
  CCT_ERR_TRANSACTION: '310',
  CCT_ERR_AMOUNT: '401',
  CCT_ERR_MULTIPLE_CARD: '402',
  CCT_ERR_UNSUPPORTED_CARD: '403',
  CCT_ERR_TRANSACTION_TIMEOUT: '404',
  CCT_ERR_CARD_COMMAND: '405',
  CCT_ERR_PAYMENT_LIMIT: '406',
  CCT_ERR_HOLD_LIMIT: '407',
  CCT_ERR_CHARGE_LIMIT: '408',
  CCT_ERR_BAD_CARD_STATUS: '409',
  CCT_ERR_AUTH: '412',
  CCT_ERR_DIFF_CARD: '413',
  CCT_ERR_INVALID_CARD: '502',
  CCT_ERR_NEGA_CARD: '503',
  CCT_ERR_EXPAIRED_CARD: '504',
  CCT_ERR_ID_PASSWORD_LOCK: '505',
  CCT_ERR_NOT_EXIST_RECENTLY_TRANSACTION: '800',
  CCT_ERR_NEED_CONFIRM_RECENTLY_TRANSACTION: '801',
  CCT_ERR_NEED_CARD_BRAND_CONTACT: '802',
  CCT_ERR_ALARM_TRANSACTION: '803',
  CCT_ERR_SYSTEM_FATAL: '901',
  CCT_ERR_SESSION_TIMEOUT: '902'
}

module.exports.isSuccess = resultCode => {
  return resultCode === PAYMENT_RESULT_CODE.RESULT_SUCCESS
}

module.exports.isUserCancel = resultCode => {
  return resultCode === PAYMENT_RESULT_CODE.RESULT_CANCEL
}

module.exports.errorMessage = (resultCode, isWaon = false) => {
  switch (resultCode) {
    case PAYMENT_RESULT_CODE.RESULT_IMPOSSIBLE_REQUEST:
      return I18n.t('message.V-01-E001')
    case PAYMENT_RESULT_CODE.RESULT_IMPOSSIBLE_SUSPEND:
      return I18n.t('message.V-01-E002')
    case PAYMENT_RESULT_CODE.RESULT_TIMEOUT:
      return I18n.t('message.V-01-E003')
    case PAYMENT_RESULT_CODE.RESULT_VARIFYCATION_ERROR:
    case PAYMENT_RESULT_CODE.RESULT_INVALID_PARAM:
    case PAYMENT_RESULT_CODE.RESULT_INVALID:
      return I18n.t('message.V-01-E004')
    case PAYMENT_RESULT_CODE.RESULT_NETWORK_ERROR:
      return I18n.t('message.V-01-E005')
    case PAYMENT_RESULT_CODE.RESULT_LOCAL_ERROR:
      return I18n.t('message.V-01-E006')
    case PAYMENT_RESULT_CODE.RESULT_CONTACT_EMV_ERROR_DETECTION:
    case PAYMENT_RESULT_CODE.RESULT_CONTACT_EMV_DENIED:
    case PAYMENT_RESULT_CODE.RESULT_ONLINE_PROCESS_DENIED:
      return I18n.t('message.V-01-E007')
    case EMONEY_RESULT_CODE.CANCEL:
      return I18n.t('message.V-02-E101')
    case EMONEY_RESULT_CODE.ERR_INVALID_PARAM:
      return I18n.t('message.V-02-E201')
    case EMONEY_RESULT_CODE.ERR_ALWAYS_PROCESSING:
      return I18n.t('message.V-02-E202')
    case EMONEY_RESULT_CODE.ERR_CCT_CONNECT:
      if (isWaon) {
        return I18n.t('message.V-02-E203-WAON')
      } else {
        return I18n.t('message.V-02-E203')
      }
    case EMONEY_RESULT_CODE.ERR_CONNECTION_TIMEOUT:
      if (isWaon) {
        return I18n.t('message.V-02-E204-WAON')
      } else {
        return I18n.t('message.V-02-E204')
      }
    case EMONEY_RESULT_CODE.ERR_IN_CARD_READER:
      return I18n.t('message.V-02-E205')
    case EMONEY_RESULT_CODE.CCT_ERR_BAD_STATUS:
      return I18n.t('message.V-02-E206')
    case EMONEY_RESULT_CODE.CCT_ERR_NETWORK_DISABLED:
      return I18n.t('message.V-02-E207')
    case EMONEY_RESULT_CODE.CCT_ERR_BAD_REQUEST:
      if (isWaon) {
        return I18n.t('message.V-02-E303-WAON')
      } else {
        return I18n.t('message.V-02-E303')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_BAD_SERVICE:
      return I18n.t('message.V-02-E304')
    case EMONEY_RESULT_CODE.CCT_ERR_CHARGE_DEVICE:
      return I18n.t('message.V-02-E308')
    case EMONEY_RESULT_CODE.CCT_ERR_TRANSACTION:
      return I18n.t('message.V-02-E310')
    case EMONEY_RESULT_CODE.CCT_ERR_AMOUNT:
      if (isWaon) {
        return I18n.t('message.V-02-E401-WAON')
      } else {
        return I18n.t('message.V-02-E401')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_MULTIPLE_CARD:
      return I18n.t('message.V-02-E402')
    case EMONEY_RESULT_CODE.CCT_ERR_UNSUPPORTED_CARD:
      if (isWaon) {
        return I18n.t('message.V-02-E403-WAON')
      } else {
        return I18n.t('message.V-02-E403')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_TRANSACTION_TIMEOUT:
      return I18n.t('message.V-02-E404')
    case EMONEY_RESULT_CODE.CCT_ERR_CARD_COMMAND:
      return I18n.t('message.V-02-E405')
    case EMONEY_RESULT_CODE.CCT_ERR_PAYMENT_LIMIT:
      return I18n.t('message.V-02-E406')
    case EMONEY_RESULT_CODE.CCT_ERR_HOLD_LIMIT:
      return I18n.t('message.V-02-E407')
    case EMONEY_RESULT_CODE.CCT_ERR_CHARGE_LIMIT:
      if (isWaon) {
        return I18n.t('message.V-02-E408-WAON')
      } else {
        return I18n.t('message.V-02-E408')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_BAD_CARD_STATUS:
      if (isWaon) {
        return I18n.t('message.V-02-E409-WAON')
      } else {
        return I18n.t('message.V-02-E409')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_AUTH:
      return I18n.t('message.V-02-E412')
    case EMONEY_RESULT_CODE.CCT_ERR_DIFF_CARD:
      if (isWaon) {
        return I18n.t('message.V-02-E413-WAON')
      } else {
        return I18n.t('message.V-02-E413')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_INVALID_CARD:
      if (isWaon) {
        return I18n.t('message.V-02-E502-WAON')
      } else {
        return I18n.t('message.V-02-E502')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_NEGA_CARD:
      if (isWaon) {
        return I18n.t('message.V-02-E503-WAON')
      } else {
        return I18n.t('message.V-02-E503')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_EXPAIRED_CARD:
      if (isWaon) {
        return I18n.t('message.V-02-E504-WAON')
      } else {
        return I18n.t('message.V-02-E504')
      }
    case EMONEY_RESULT_CODE.CCT_ERR_ID_PASSWORD_LOCK:
      return I18n.t('message.V-02-E505')
    case EMONEY_RESULT_CODE.CCT_ERR_NEED_CARD_BRAND_CONTACT:
      return I18n.t('message.V-02-E802')
    case EMONEY_RESULT_CODE.CCT_ERR_ALARM_TRANSACTION:
      return I18n.t('message.V-02-E803')
    case EMONEY_RESULT_CODE.CCT_ERR_SYSTEM_FATAL:
      return I18n.t('message.V-02-E901')
    case EMONEY_RESULT_CODE.CCT_ERR_SESSION_TIMEOUT:
      return I18n.t('message.V-02-E902')
    default:
      return I18n.t('message.V-99-E999')
  }
}
