import { setDebugModal } from '../../debug/actions'
import PaymentServiceError from '../errors/PaymentServiceError'

const acquirerCodes = {
  VISA: '04',
  AMEX: '07',
  DINERS: '01',
  JCB: '02',
  銀聯: '67'
}

const acquirerCodeOptions = []
for (const key in acquirerCodes) {
  acquirerCodeOptions.push({
    label: key,
    value: acquirerCodes[key]
  })
}

export default class DebugPaymentService {
  _dispatch

  constructor (dispatch) {
    this._dispatch = dispatch
  }

  async pay (cartPayment) {
    return new Promise((resolve, reject) => {
      this._dispatch(setDebugModal({
        title: 'Debug Payment Service',
        options: [
          {
            title: 'Acquirer',
            type: 'radio',
            key: 'acquirerCode',
            options: acquirerCodeOptions
          }
        ],
        commands: [
          {
            label: 'Error',
            onPress: () => {
              this._dispatch(setDebugModal(null))
              reject(new PaymentServiceError(
                '何かしらのエラーが起こった想定の動作です。',
                { debug: true },
                false
              ))
            }
          },
          {
            label: 'Cancel',
            onPress: () => {
              this._dispatch(setDebugModal(null))
              reject(new PaymentServiceError(
                'ユーザーによりキャンセルされました。',
                { debug: true },
                true
              ))
            }
          },
          {
            label: 'Complete',
            onPress: (input) => {
              this._dispatch(setDebugModal(null))
              resolve({
                type: 'mPos_debug',
                orderId: '1',
                amount: cartPayment.amount.toFixed(0),
                authCode: 'test',
                acquirerCode: input.acquirerCode,
                resultCode: '000',
                status: 'success',
                paymentTime: '20170906222711',
                jpo: '10',
                emvAppLabel: 'DebugCard',
                cardNumber: '4980??********??',
                cardType: 'IC'
              })
            }
          }
        ]
      }))
    })
  }

  async refund (cartPayment, extraData) {
    return new Promise((resolve, reject) => {
      this._dispatch(setDebugModal({
        title: 'Debug Payment Refund',
        options: [],
        commands: [
          {
            label: 'Error',
            onPress: () => {
              this._dispatch(setDebugModal(null))
              reject(new PaymentServiceError(
                '何かしらのエラーが起こった想定の動作です。',
                { debug: true },
                false
              ))
            }
          },
          {
            label: 'Complete',
            onPress: (input) => {
              this._dispatch(setDebugModal(null))
              resolve({
                type: 'mPos_debug',
                orderId: '1',
                originOrderId: '111',
                amount: cartPayment.amount.toFixed(0),
                authCode: 'test',
                acquirerCode: input.acquirerCode,
                resultCode: '000',
                status: 'success',
                paymentTime: '20170906222711',
                jpo: '10',
                emvAppLabel: 'DebugCard',
                cardNumber: '4980??********??',
                cardType: 'IC'
              })
            }
          }
        ]
      }))
    })
  }
}
