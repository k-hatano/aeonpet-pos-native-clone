import I18n from 'i18n-js'
import logger from 'common/utils/logger'
import { toUnixTimestamp } from 'common/utils/dateTime'
import TelegramEMoney from './models/VEGA3000/telegrams/TelegramEMoney'

export const PAYMENT_METHOD_TYPE = {
  CASH: 210,
  CREDIT: 220,
  UNION_PAY: 230,
  EMONEY: 240,
  GIFT_WITH_CHANGE: 250,
  GIFT_WITHOUT_CHANGE: 260,
  ACCOUNTS_RECEIVABLE: 270,
  EXTERNAL_POINT: 280,
  INTERNAL_POINT: 290,

  /** クレジット KID */
  DINERS: 701,
  JCB: 702,
  AMEX: 703,
  ACS: 704,
  ACS_991: 781,
  ACS_992: 782,
  ACS_997: 787,
  ACS_998: 788,
  ACS_999: 789,
  OTHER: 799,

  /** 電子マネー ブランド */
  ID: 801,
  QUICPAY: 802,
  TRANSPORTATION_IC: 803,
  WAON: 804,

  /** 券つり */
  CHANGE_OF_GIFT: 901,

  /** 券差額 */
  GIFT_SURPLUS: 902
}

export const VEGA_KID = {
  DINERS: '101',
  JCB: '102',
  OTHER: '106',
  AMEX: '107',
  ACS_991: '991',
  ACS_992: '992',
  ACS_997: '997',
  ACS_998: '998',
  ACS_999: '999',
  ACS: '615'
}

export const VEGA_EMONEY_BRAND = {
  ID: '0003',
  QUICPAY: '0004',
  TRANSPORTATION_IC: '0005',
  WAON: '0006'
}

export const VEGA_DATA_TYPE = {
  REQUEST: '2',
  RESPONSE: '3'
}

export const VEGA_TELEGRAM_TYPE = {
  REQUEST: '2',
  RESPONSE: '3'
}

export const VEGA_RECODE_TYPE = {
  NON_TRAINING: '10',
  TRAINING: '20'
}

export const VEGA_API_TYPE = {
  CREDIT_PAY: '100',
  CREDIT_RETURN: '110',
  UNIONPAY_PAY: '150',
  UNIONPAY_RETURN: '151',
  EMONEY_PAY: '400',
  EMONEY_CHARGE: '401',
  EMONEY_RETURN: '410',
  INTERRUPT: '999'
}

export const VEGA_RETURN_TYPE = {
  CANCEL: '1',
  RETURN: '2'
}

export const paymentMethodTypeToLabel = type => {
  switch (type) {
    case PAYMENT_METHOD_TYPE.CASH:
      return I18n.t('settings_order.cash')
    case PAYMENT_METHOD_TYPE.CREDIT:
      return I18n.t('settings_order.credit')
    case PAYMENT_METHOD_TYPE.EMONEY:
      return I18n.t('settings_order.e_money')
    case PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE:
      return I18n.t('settings_order.gift_change')
    case PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE:
      return I18n.t('settings_order.gift_no_change')
    case PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE:
      return I18n.t('settings_order.account_receivable')
    case PAYMENT_METHOD_TYPE.EXTERNAL_POINT:
      return I18n.t('settings_order.external_point')
    case PAYMENT_METHOD_TYPE.OTHER:
      return I18n.t('settings_order.other')
    case PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT:
    case PAYMENT_METHOD_TYPE.GIFT_SURPLUS:
      return null
    default:
      logger.error(`Invalid Payment Method Type：${type}`)
      return null
  }
}

export const paymentMethodTypeToImage = type => {
  switch (type) {
    case PAYMENT_METHOD_TYPE.CASH:
      return require('assets/images/payments/yen@1x.png')
    case PAYMENT_METHOD_TYPE.CREDIT:
    case PAYMENT_METHOD_TYPE.UNION_PAY:
      return require('assets/images/payments/credit@1x.png')
    case PAYMENT_METHOD_TYPE.EMONEY:
      return require('assets/images/payments/emoney@1x.png')
    case PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE:
    case PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE:
    case PAYMENT_METHOD_TYPE.EXTERNAL_POINT:
    case PAYMENT_METHOD_TYPE.INTERNAL_POINT:
      return require('assets/images/payments/gift@1x.png')
    case PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE:
      return require('assets/images/payments/accountsReceivable@1x.png')
    case PAYMENT_METHOD_TYPE.OTHER:
      return require('assets/images/payments/yen@1x.png')
    case PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT:
    case PAYMENT_METHOD_TYPE.GIFT_SURPLUS:
      return null
    default:
      logger.error(`Invalid Payment Method Type：${type}`)
      return null
  }
}

export const _paymentMethodTypeToShortLabel = paymentMethodType => {
  switch (paymentMethodType) {
    case PAYMENT_METHOD_TYPE.CASH:
      return '現'
    case PAYMENT_METHOD_TYPE.UNION_PAY:
    case PAYMENT_METHOD_TYPE.CREDIT:
      return 'ク'
    case PAYMENT_METHOD_TYPE.EMONEY:
      return 'E'
    case PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE:
      return '券'
    case PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE:
      return '券'
    case PAYMENT_METHOD_TYPE.ACCOUNTS_RECEIVABLE:
      return '掛'
    case PAYMENT_METHOD_TYPE.EXTERNAL_POINT:
    case PAYMENT_METHOD_TYPE.INTERNAL_POINT:
      return 'P'
    case PAYMENT_METHOD_TYPE.OTHER:
      return '他'
    case PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT:
    case PAYMENT_METHOD_TYPE.GIFT_SURPLUS:
      return null
    default:
      logger.error(`Invalid Payment Method Type：${paymentMethodType}`)
      return null
  }
}

export const OTHER_CARD = {
  id: '00000000000000000000000000000102',
  payment_method_code: '102',
  payment_method_type: 102,
  acquirer_code: '',
  media_type: 72,
  shop_mode: 2,
  is_offline: 0,
  sort_order: 9999,
  created_at: toUnixTimestamp(new Date()),
  updated_at: toUnixTimestamp(new Date())
}

export function isGiftPaymentMethodType (paymentMethodType) {
  return (
    paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITH_CHANGE ||
    paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_WITHOUT_CHANGE
  )
}

export function isGiftChangeOrSurplusPaymentMethodType (paymentMethodType) {
  return (
    paymentMethodType === PAYMENT_METHOD_TYPE.CHANGE_OF_GIFT || paymentMethodType === PAYMENT_METHOD_TYPE.GIFT_SURPLUS
  )
}

export function isCredit (paymentMethodType) {
  return paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT ||
        paymentMethodType === PAYMENT_METHOD_TYPE.DINERS ||
        paymentMethodType === PAYMENT_METHOD_TYPE.JCB ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS_991 ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS_992 ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS_997 ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS_998 ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ACS_999 ||
        paymentMethodType === PAYMENT_METHOD_TYPE.AMEX ||
        paymentMethodType === PAYMENT_METHOD_TYPE.OTHER
}

export function isCreditCardOrEmoney (paymentMethodType) {
  return isCredit(paymentMethodType) ||
        isEMoneyWithPaymentMethodType(paymentMethodType) ||
        paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY
}

/**
 * @param {TeregramEntity} _teregramEntity
 */
export function isEMoney (_teregramEntity) {
  return _teregramEntity.detailData instanceof TelegramEMoney
}

export function isEMoneyWithPaymentMethodType (paymentMethodType) {
  return paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY ||
        paymentMethodType === PAYMENT_METHOD_TYPE.ID ||
        paymentMethodType === PAYMENT_METHOD_TYPE.QUICPAY ||
        paymentMethodType === PAYMENT_METHOD_TYPE.TRANSPORTATION_IC ||
        paymentMethodType === PAYMENT_METHOD_TYPE.WAON
}

export function isExternalPoint (paymentMethodType) {
  return paymentMethodType === PAYMENT_METHOD_TYPE.EXTERNAL_POINT
}

export function isCash (paymentMethodType) {
  return paymentMethodType === PAYMENT_METHOD_TYPE.CASH
}

export const getOrderPaymentString = orderPayments => {
  let orderPaymentLabels = orderPayments.map(payment => _paymentMethodTypeToShortLabel(payment.payment_method_type))
  return orderPaymentLabels
    .filter(function (value, index) {
      return index === orderPaymentLabels.lastIndexOf(value)
    })
    .join('')
}

export function isWaon (_teregramEntity) {
  if (!isEMoney(_teregramEntity)) return false

  return _teregramEntity.detailData.eMoneyBrandCode === VEGA_EMONEY_BRAND.WAON
}

export function brandCodeToPaymentMethodType (brandCode) {
  switch (brandCode) {
    case VEGA_KID.DINERS:
      return PAYMENT_METHOD_TYPE.DINERS
    case VEGA_KID.JCB:
      return PAYMENT_METHOD_TYPE.JCB
    case VEGA_KID.AMEX:
      return PAYMENT_METHOD_TYPE.AMEX
    case VEGA_KID.ACS:
      return PAYMENT_METHOD_TYPE.ACS
    case VEGA_KID.ACS_991:
      return PAYMENT_METHOD_TYPE.ACS_991
    case VEGA_KID.ACS_992:
      return PAYMENT_METHOD_TYPE.ACS_992
    case VEGA_KID.ACS_997:
      return PAYMENT_METHOD_TYPE.ACS_997
    case VEGA_KID.ACS_998:
      return PAYMENT_METHOD_TYPE.ACS_998
    case VEGA_KID.ACS_999:
      return PAYMENT_METHOD_TYPE.ACS_999
    case VEGA_EMONEY_BRAND.ID:
      return PAYMENT_METHOD_TYPE.ID
    case VEGA_EMONEY_BRAND.QUICPAY:
      return PAYMENT_METHOD_TYPE.QUICPAY
    case VEGA_EMONEY_BRAND.TRANSPORTATION_IC:
      return PAYMENT_METHOD_TYPE.TRANSPORTATION_IC
    case VEGA_EMONEY_BRAND.WAON:
      return PAYMENT_METHOD_TYPE.WAON
    default:
      return PAYMENT_METHOD_TYPE.OTHER
  }
}

export function eMoneyBrandCodeToBrandName (eMoneyBrandCode) {
  switch (eMoneyBrandCode) {
    case VEGA_EMONEY_BRAND.ID:
      return I18n.t('payment.brand_name.id')
    case VEGA_EMONEY_BRAND.QUICPAY:
      return I18n.t('payment.brand_name.quic_pay')
    case VEGA_EMONEY_BRAND.TRANSPORTATION_IC:
      return I18n.t('payment.brand_name.transpotation_ic')
    case VEGA_EMONEY_BRAND.WAON:
      return I18n.t('payment.brand_name.waon')
  }
}

export function isNotIgnorePayment (paymentMethodType) {
  switch (paymentMethodType) {
    case PAYMENT_METHOD_TYPE.DINERS:
    case PAYMENT_METHOD_TYPE.JCB:
    case PAYMENT_METHOD_TYPE.AMEX:
    case PAYMENT_METHOD_TYPE.ACS:
    case PAYMENT_METHOD_TYPE.ACS_991:
    case PAYMENT_METHOD_TYPE.ACS_992:
    case PAYMENT_METHOD_TYPE.ACS_997:
    case PAYMENT_METHOD_TYPE.ACS_998:
    case PAYMENT_METHOD_TYPE.ACS_999:
    case PAYMENT_METHOD_TYPE.ID:
    case PAYMENT_METHOD_TYPE.QUICPAY:
    case PAYMENT_METHOD_TYPE.TRANSPORTATION_IC:
    case PAYMENT_METHOD_TYPE.WAON:
    case PAYMENT_METHOD_TYPE.OTHER:
      return false
    default:
      return true
  }
}
