import connectPermissionButton from '../../staff/containers/connectPermissionButton'
import IconButton from 'common/components/elements/IconButton'

export default connectPermissionButton(IconButton)
