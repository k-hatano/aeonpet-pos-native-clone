import HomeTrainingButton from '../components/HomeTrainingButton'
import connectPermissionButton from '../../staff/containers/connectPermissionButton'

export default connectPermissionButton(HomeTrainingButton)
