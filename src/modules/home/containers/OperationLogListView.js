import { connect } from 'react-redux'
import OperationLogListView from '../components/OperationLogListView'
import { MODULE_NAME } from '../models'

const mapStateToProps = state => ({
  operationLogs: state[MODULE_NAME].operationLogs
})

export default connect(mapStateToProps)(OperationLogListView)
