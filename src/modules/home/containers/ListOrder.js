import { connect } from 'react-redux'
import I18n from 'i18n-js'
import logger from 'common/utils/logger'
import ListOrder from '../components/ListOrder'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import OperationLogRepository from '../repositories/OperationLogRepository'
import OrderRepository from 'modules/order/repositories/OrderRepository'
import CashierCashRecordRepository from 'modules/cashier/repositories/CashierCashRecordRepository'
import PaymentRepository from 'modules/payment/repositories/PaymentRepository'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { makeDateRange } from '../../../common/utils/dateTime'
import moment from 'moment'
import { loading } from '../../../common/sideEffects'
import { createUnpushedItems, UnpushedOrderItem } from '../models/UnpushedItem'
import { selectOrder, setReturnedOrder } from '../../order/actions'
import * as _ from 'underscore'
import { STATUS } from '../../order/models'
import NetworkError from '../../../common/errors/NetworkError'
import AlertView from '../../../common/components/widgets/AlertView'
import SettingKeys from 'modules/setting/models/SettingKeys'

const mapDispatchToProps = dispatch => ({
  onSelectUnpushedItem: async (item) => {
    console.log('onSelectUnpushedItem', item)
    if (item instanceof UnpushedOrderItem) {
      try {
        console.log('onSelectUnpushedItem')
        const order = await OrderRepository.findById(item.order.id)
        dispatch(selectOrder(order))
        return true
      } catch (error) {
        // TODO エラーハンドリング
        return false
      }
    }
    return false
  },
  onSelectHistoryOrder: async (order) => {
    try {
      if (order && !order.is_pended) {
        const selectedOrder = await OrderRepository.fetchByOrderId(order.id)
        let returnedOrder = null
        if (selectedOrder.status === STATUS.RETURNED) {
          const conditions = {
            is_minus: 1,
            origin_order_id: selectedOrder.id
          }
          const fetchedReturnedOrder = await OrderRepository.fetchByConditions(conditions)
          returnedOrder = fetchedReturnedOrder.length > 0 ? fetchedReturnedOrder[0] : null
        }
        dispatch(setReturnedOrder(returnedOrder))
        if (order.status !== STATUS.RETURNED && selectedOrder.status === STATUS.RETURNED) {
          const promotionOrders = await OrderRepository.fetchPromotionOrderByOrderId(order.id)
          const promotionOrderItems = await OrderRepository.fetchPromotionOrderItemsByOrderId(order.id)
          const payments = await PaymentRepository.fetchByOrderId(order.id)
          let paymentIdMaps = {}
          // payments と order_payments の並び順は一致しないので、Id で Map にして突合する。
          for (let i = 0; i < order.order_payments.length; i++) {
            paymentIdMaps[order.order_payments[i].payment_method_id] = order.order_payments[i].payment_method_name
          }
          for (let i = 0; i < payments.length; i++) {
            payments[i].payment_method_name = paymentIdMaps[payments[i].payment_method_id]
          }
          selectedOrder.promotion_orders = promotionOrders
          selectedOrder.promotion_order_items = promotionOrderItems
          selectedOrder.payments = payments
          const updatedOrder = await OrderRepository.updateLocal(selectedOrder)
          dispatch(selectOrder(updatedOrder))
          return
        }
        if (!order.finalized_at && selectedOrder.finalized_at) {
          const oldOrder = await OrderRepository.findById(order.id)
          if (oldOrder) {
            const newOrder = {
              ...oldOrder,
              finalized_at: selectedOrder.finalized_at
            }
            const updatedOrder = await OrderRepository.updateLocal(newOrder)
            dispatch(selectOrder(updatedOrder))
          }
        }
      }
    } catch (error) {
      dispatch(selectOrder(order))
    }
    dispatch(selectOrder(order))
  },
  onChangeDate: async (chosenDate) => {
    try {
      const date = moment(chosenDate, 'YYYY-MM-DD').toDate()
      const { from, to } = makeDateRange(date, date)
      const operationLogs = await OperationLogRepository.findByDate(from, to)
      const orderHistory = await OrderRepository.findByDate(from, to)
      dispatch(actions.listOrderHistory(_.sortBy(orderHistory, item => -item.pos_order.client_created_at)))
      dispatch(actions.listOperationLogs(operationLogs))
      dispatch(actions.setChosenDate(chosenDate))
    } catch (error) {
      logger.error('ListOrder.onChangeDate ' + error)
    }
  },
  onSendData: async () => {
    try {
      const checkResult = (result) => {
        if (result.errors.length !== 1) { return }
        const error = result.errors[0]
        if (error instanceof NetworkError && (
          error.type === NetworkError.TYPE.OFFLINE ||
          error.type === NetworkError.TYPE.TIMEOUT)) {
          throw error
        }
      }
      let hasError = false
      await loading(dispatch, async () => {
        const orderResult = await OrderRepository.syncLocalToRemote()
        checkResult(orderResult)
        hasError = hasError || orderResult.errors.length > 0

        const ejournalResult = await EJournalRepository.sendUnpushEjournals()
        checkResult(ejournalResult)
        hasError = hasError || ejournalResult.errors.length > 0

        const cashierCashRecordResult = await CashierCashRecordRepository.syncLocalToRemote()
        checkResult(cashierCashRecordResult)
        hasError = hasError || cashierCashRecordResult.errors.length > 0
      })
      if (hasError) {
        await AlertView.showAsync(I18n.t('message.A-01-E002'))
      } else {
        await AlertView.showAsync(I18n.t('message.A-01-I001'))
      }
    } catch (error) {
      const message = error.message || ''
      await AlertView.showAsync(I18n.t('message.A-01-E002'))
    }
    const unpushedItems = await createUnpushedItems()
    dispatch(actions.listUnpushedItems(unpushedItems))
  }
})

const mapStateToProps = state => ({
  unpushedItems: state[MODULE_NAME].unpushedItems,
  selectedOrder: state['order'].selectedOrder,
  orderHistory: state[MODULE_NAME].orderHistory,
  pendedOrders: state[MODULE_NAME].pendedOrders,
  isStockManagementMode: !state.setting.settings[SettingKeys.COMMON.CASHIER_ID],
  returnedOrder: state['order'].returnedOrder
})

export default connect(mapStateToProps, mapDispatchToProps)(ListOrder)
