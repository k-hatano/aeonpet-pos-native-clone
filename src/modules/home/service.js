import { toUnixTimestamp } from 'common/utils/dateTime'
import OperationLogRepository from 'modules/home/repositories/OperationLogRepository'
import { operationTypeToName } from 'modules/home/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'

export const saveOperationLog = async (operationType, operationAt) => {
  const currentStaff = new StoreAccessibleBase().getSetting(SettingKeys.STAFF.CURRENT_STAFF)
  const operationLog = {
    operation_name: operationTypeToName(operationType),
    operation_at: toUnixTimestamp(operationAt),
    operation_type: operationType,
    staff_name: currentStaff ? currentStaff.name : null,
    staff_code: currentStaff ? currentStaff.staff_code : null
  }
  await OperationLogRepository.save(operationLog)
}
