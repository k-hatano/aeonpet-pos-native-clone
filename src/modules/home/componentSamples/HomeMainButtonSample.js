import HomeMainButton from '../components/HomeMainButton'

import { View } from 'react-native'
import React from 'react'
import I18n from 'i18n-js'
import * as ButtonContents from '../components/ButtonContents'

export default {
  name: 'HomeMainButton',
  component: HomeMainButton,
  properties: [
    {
      title: 'Test',
      property: {
        title: 'TestName',
        content: () => (<View style={{width: 64, height: 64, backgroundColor: 'green'}} />)
      }
    },
    {
      title: 'ButtonContents',
      property: {
        title: I18n.t('home.balance_cache'),
        content: ButtonContents.CashierBalanceIcon
      }
    },
    {
      title: 'SearchOrderIcon',
      property: {
        title: I18n.t('home.orders_search'),
        content: ButtonContents.SearchOrderIcon
      }
    },
    {
      title: 'SearchCustomerIcon',
      property: {
        title: I18n.t('home.member_search'),
        content: ButtonContents.SearchCustomerIcon
      }
    },
    {
      title: 'ReturnOrderIcon',
      property: {
        title: I18n.t('home.return'),
        content: ButtonContents.ReturnOrderIcon
      }
    },
    {
      title: 'SearchStockIcon',
      property: {
        title: I18n.t('home.stocks'),
        content: ButtonContents.SearchStockIcon
      }
    },
    {
      title: 'StockTakingIcon',
      property: {
        title: I18n.t('home.stock_taking'),
        content: ButtonContents.StockTakingIcon
      }
    },
    {
      title: 'StockMoveInIcon',
      property: {
        title: I18n.t('home.in_stocks'),
        content: ButtonContents.StockMoveInIcon
      }
    },
    {
      title: 'StockMoveOutIcon',
      property: {
        title: I18n.t('home.out_stocks'),
        content: ButtonContents.StockMoveOutIcon
      }
    },
    {
      title: 'StockAdjustmentIcon',
      property: {
        title: I18n.t('home.adjust_stocks'),
        content: ButtonContents.StockAdjustmentIcon
      }
    },
    {
      title: 'OrderIcon',
      property: {
        title: I18n.t('home.new_order'),
        content: () => (<ButtonContents.OrderIcon imageStyle={{width: 60, height: 51}} />),
        stylesheet: require('modules/home/styles/HomeMainButton-Primary').default
      }
    },
    {
      title: 'CheckSalesIcon',
      property: {
        title: I18n.t('home.check_sales'),
        content: ButtonContents.CheckSalesIcon,
        stylesheet: require('modules/home/styles/HomeMainButton-Second').default
      }
    },
    {
      title: 'StockAdjustmentIcon',
      property: {
        title: I18n.t('home.close_sales'),
        content: ButtonContents.CloseSalesIcon,
        stylesheet: require('modules/home/styles/HomeMainButton-Second').default
      }
    }
  ],
  frames: [
    {
      title: '165 x 128',
      style: {width: 165, height: 128}
    },
    {
      title: '256 x 216',
      style: {width: 256, height: 216}
    },
    {
      title: '256 x 100',
      style: {width: 256, height: 100}
    }
  ]
}
