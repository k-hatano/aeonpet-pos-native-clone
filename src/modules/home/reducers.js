import { handleActions } from 'redux-actions'

import * as actions from './actions'

const defaultState = {
  operationLogs: [],
  printerErrorsCount: 0,
  orderHistory: [],
  unpushedItems: [],
  pendedOrders: [],
  chosenDate: ""
}

const handlers = {
  [actions.listOperationLogs]: (state, action) => ({
    ...state,
    ...{ operationLogs: action.payload }
  }),
  [actions.setPrinterErrorsCount]: (state, action) => ({
    ...state,
    ...{ printerErrorsCount: action.payload }
  }),
  [actions.listOrderHistory]: (state, action) => ({
    ...state,
    ...{ orderHistory: action.payload }
  }),
  [actions.listUnpushedItems]: (state, action) => ({
    ...state,
    ...{ unpushedItems: action.payload }
  }),
  [actions.listPendedOrders]: (state, action) => ({
    ...state,
    ...{ pendedOrders: action.payload }
  }),
  [actions.setChosenDate]: (state, action) => ({
    ...state,
    ...{ chosenDate: action.payload }
  })
}

export default handleActions(handlers, defaultState)
