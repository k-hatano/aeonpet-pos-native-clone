import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const listOperationLogs = createAction(`${MODULE_NAME}_listOperationLogs`)
export const setPrinterErrorsCount = createAction(`${MODULE_NAME}_printerErrorsCount`)
export const listOrderHistory = createAction(`${MODULE_NAME}_listOrderHistory`)
export const listUnpushedItems = createAction(`${MODULE_NAME}_listUnpushedItems`)
export const listPendedOrders = createAction(`${MODULE_NAME}_listPendedOrders`)
export const setChosenDate = createAction(`${MODULE_NAME}_setChosenDate`)
