import StandardOperationLogRepository from '../standard/StandardOperationLogRepository'

export default class TrainingOperationLogRepository {
  async findByDate (fromDate, toDate) {
    return []
  }

  async save (operationLog) {
    await new StandardOperationLogRepository().save(operationLog)
  }
}