import SampleOperationLogRepository from './sample/SampleOperationLogRepository'
import StandardOperationLogRepository from './standard/StandardOperationLogRepository'
import TrainingOperationLogRepository from './training/TrainingOperationLogRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class OperationLogRepository {
  static _implement = new SampleOperationLogRepository()

  static async findByDate (fromDate, toDate) {
    return this._implement.findByDate(fromDate, toDate)
  }

  static async save (operationLog) {
    return this._implement.save(operationLog)
  }

  static async clearBeforeDate (timestamp) {
    return this._implement.clearBeforeDate(timestamp)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardOperationLogRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingOperationLogRepository()
        break

      default:
        this._implement = new SampleOperationLogRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('OperationLogRepository', (context) => {
  OperationLogRepository.switchImplement(context)
})
