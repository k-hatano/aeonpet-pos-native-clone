const operationLogIds = {
  id1: '0283890260D50599767D97511B8E90A5',
  id2: '08C6BF4A27D09AE2407789830FD0F8CF',
  id3: '28CA72D187E50E566D74965DC1566381',
  id4: '343E15A347742A96A561C0983A4EA9D5',
  id5: '45318F45F7C340D56A38B2A616BD78F1',
  id6: '54624FFFC7CB6CC40127465B82A50C37',
  id7: '5EBB72A738DC23C33ED19DA7CF42C82B',
  id8: '6F899626877A310A325166A9704CA8B2',
  id9: '815D02CACF306029A8A3043CAC44F3B6',
  id10: 'D15A52DC0920300C17F8CA79FE8C7825',
  id11: '815D02CCAF30602917F8CA79FE8C7825',
  id12: 'D15A52DC0920300127465B82A50C37AC',
  id13: '0283890260D505997127465B82A50C37',
  id14: '6D74965DC156638127465B82A50C37AC',
  id15: '343E15A347742A96A127465B82A50C3C',
  id16: 'D15A738DC23C33ED19012746550C37AC',
  id17: 'D15A5A347742A96A50127465B82A50AC',
  id18: 'D1599626877A310A32565B82A50C37AC'
}

const TODAY = new Date()
const YESTERDAY = new Date(TODAY.setDate((TODAY.getDate() - 1)))
const TOMORROW = new Date(TODAY.setDate((TODAY.getDate() + 1)))

export const operationLogs = [
  {
    id: operationLogIds.id1,
    operation_name: '開局',
    operation_at: Math.floor(new Date(TODAY.setHours(0, 0, 5)).getTime() / 1000),
    operation_type: 1,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id2,
    operation_name: '点検',
    operation_at: Math.floor(new Date(TODAY.setHours(0, 0, 4)).getTime() / 1000),
    operation_type: 2,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id3,
    operation_name: '精算',
    operation_at: Math.floor(new Date(TODAY.setHours(0, 0, 3)).getTime() / 1000),
    operation_type: 3,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id4,
    operation_name: 'トレーニングモード開始',
    operation_at: Math.floor(new Date(TODAY.setHours(0, 0, 2)).getTime() / 1000),
    operation_type: 4,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id5,
    operation_name: 'トレーニングモード終了',
    operation_at: Math.floor(new Date(TODAY.setHours(0, 0, 1)).getTime() / 1000),
    operation_type: 5,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id6,
    operation_name: '入金',
    operation_at: Math.floor(new Date(TODAY.setHours(23, 59, 59)).getTime() / 1000),
    operation_type: 6,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id7,
    operation_name: '出金',
    operation_at: Math.floor(new Date(TODAY.setHours(23, 59, 58)).getTime() / 1000),
    operation_type: 7,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id8,
    operation_name: 'データ同期完了',
    operation_at: Math.floor(new Date(TODAY.setHours(23, 59, 57)).getTime() / 1000),
    operation_type: 8,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id9,
    operation_name: 'データ同期失敗',
    operation_at: Math.floor(new Date(TODAY.setHours(23, 59, 56)).getTime() / 1000),
    operation_type: 9,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id10,
    operation_name: '未送信データ発生',
    operation_at: Math.floor(new Date(TODAY.setHours(23, 59, 55)).getTime() / 1000),
    operation_type: 10,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id11,
    operation_name: '印刷エラー発生',
    operation_at: Math.floor(new Date(TODAY.setHours(1, 2, 3)).getTime() / 1000),
    operation_type: 11,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id12,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(TODAY.setHours(3, 2, 1)).getTime() / 1000),
    operation_type: 12,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id13,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(YESTERDAY.setHours(23, 59, 59)).getTime() / 1000),
    operation_type: 1,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id14,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(YESTERDAY.setHours(23, 59, 58)).getTime() / 1000),
    operation_type: 2,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id15,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(YESTERDAY.setHours(23, 59, 57)).getTime() / 1000),
    operation_type: 3,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id16,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(TOMORROW.setHours(0, 0, 0)).getTime() / 1000),
    operation_type: 4,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id17,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(TOMORROW.setHours(0, 0, 1)).getTime() / 1000),
    operation_type: 5,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  },
  {
    id: operationLogIds.id18,
    operation_name: 'ドロアオープン',
    operation_at: Math.floor(new Date(TOMORROW.setHours(0, 0, 2)).getTime() / 1000),
    operation_type: 6,
    staff_name: 'staff1',
    staff_code: 'STAFF00000001'
  }
]

export default class SampleOperationLogRepository {
  async findAll () {
    return operationLogs
  }

  async findByDate (fromDate, toDate) {
    return operationLogs
  }

  async save (operationLog) {
    console.log('operationLog save')
  }

  async clearBeforeDate (timestamp) {
    console.log('Delete operationLog')
  }
}
