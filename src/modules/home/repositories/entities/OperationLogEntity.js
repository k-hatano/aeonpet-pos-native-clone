import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import AppEvents from '../../../../common/AppEvents'

const OperationLog = sequelize.define(
  'OperationLog',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    operation_name: Sequelize.STRING,
    operation_at: Sequelize.INTEGER,
    operation_type: Sequelize.INTEGER,
    staff_name: Sequelize.STRING,
    staff_code: Sequelize.STRING
  },
  {
    tableName: 'operation_logs',
    underscored: true,
    timestamps: false
  }
)

export default OperationLog
