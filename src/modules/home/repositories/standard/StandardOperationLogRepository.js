import AppEvents from 'common/AppEvents'
import OperationLogEntity from '../entities/OperationLogEntity'
import { operationLogs } from '../sample/SampleOperationLogRepository'
import generateUuid from 'common/utils/generateUuid'
import { handleDatabaseError } from '../../../../common/errors'

export default class StandardOperationLogRepository {
  async findByDate (from, to) {
    return OperationLogEntity.findAll({
      where: {
        operation_at: {
          $gte: from,
          $lte: to
        }
      },
      order: [
        ['operation_at', 'DESC']
      ]
    })
  }

  async save (operationLog) {
    await OperationLogEntity.create({
      id: generateUuid(),
      operation_name: operationLog.operation_name,
      operation_at: operationLog.operation_at,
      operation_type: operationLog.operation_type,
      staff_name: operationLog.staff_name,
      staff_code: operationLog.staff_code
    })
  }

  async createSamples () {
    const existingCount = await OperationLogEntity.count()
    if (existingCount === 0) {
      await OperationLogEntity.bulkCreate(
        Array.prototype.concat.apply([], operationLogs.map(operationLog => {
          return {
            id: operationLog.id,
            operation_name: operationLog.operation_name,
            operation_at: operationLog.operation_at,
            operation_type: operationLog.operation_type,
            staff_name: operationLog.staff_name,
            staff_code: operationLog.staff_code
          }
        })))
    }
  }

  async clearBeforeDate (timestamp) {
    try {
      await OperationLogEntity.destroy(
        {
          where: {
            operation_at: {
              $lte: timestamp
            }
          }
        }
      )
    } catch (error) {
      handleDatabaseError(error)
    }
  }
}

AppEvents.onSampleDataCreate('StandardOperationLogRepository', async () => {
  return (new StandardOperationLogRepository()).createSamples()
})
