import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    maxWidth: 564,
    maxHeight: 216,
    paddingLeft: 10,
    paddingRight: 10
  },
  buttonBigWrapper: {
    flex: 1,
    alignItems: 'center',
    maxHeight: 216,
    maxWidth: 264,
    marginRight: 8
  },
  buttonsLeftWrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    maxWidth: 264,
    marginLeft: 8
  },
  buttonAboveWrapper: {
    flex: 1,
    maxHeight: 120,
    maxWidth: 264,
    alignSelf: 'stretch',
    paddingBottom: 8
  },
  buttonBelowWrapper: {
    flex: 1,
    maxHeight: 120,
    maxWidth: 264,
    alignSelf: 'stretch',
    paddingTop: 8
  },
  orangeButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#D0D0D0'
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 110
  },
  sliderArrowTouchable: {
    position: 'absolute',
    width: 25,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sliderArrowIcon: {
    color: '#fff',
    fontSize: 20
  },
  textButton: {
    color: '#000'
  }
})
