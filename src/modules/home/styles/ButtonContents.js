import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    width: '100%',
    height: 60,
    backgroundColor: baseStyleValues.mainColor
  }
})
