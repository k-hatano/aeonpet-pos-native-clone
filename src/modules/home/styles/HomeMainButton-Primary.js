import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    borderWidth: 1,
    borderColor: 'gray',
    backgroundColor: 'white',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    margin: 4
  }
})
