import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    marginLeft: 24,
    marginRight: 24
  },
  rowView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    height: 63
  },
  logType: {
    width: 45,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconStyle: {
    width: 19,
    height: 19
  },
  operationAt: {
    flex: 1,
    justifyContent: 'center'
  },
  operationLabel: {
    flex: 2,
    justifyContent: 'center'
  },
  paymentMethodType: {
    flex: 1,
    justifyContent: 'center'
  },
  totalPaid: {
    flex: 1,
    justifyContent: 'center'
  },
  operationAtText: {
    fontFamily: 'Helvetica',
    color: '#9b9b9b',
    width: '100%',
    fontSize: 18,
    height: 22
  },
  operationLabelText: {
    fontFamily: 'HiraginoSans-W3',
    color: '#4a4a4a',
    width: '100%',
    fontSize: 20,
    height: 24,
    textAlign: 'right'
  }
})
