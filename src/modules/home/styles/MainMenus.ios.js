import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    height: '90%',
    paddingLeft: 30,
    paddingRight: 16
  },
  recordListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  recordItem: {
    width: 131,
    height: 121,
    margin: 1,
    marginRight: 15,
    marginBottom: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  recordItemTitle: {
    backgroundColor: 'transparent',
    fontSize: 13
  },
  recordList: {
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5
  },
  disabledButton: {
    opacity: 0.5
  },
  orangeButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#D0D0D0'
  },
  buttonWrapperStyle: {
    width: 130,
    height: 120,
    alignItems: 'center',
    justifyContent: 'center'
  },
  listViewWrapper: {
    flex: 1
  },
  textButton: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 16,
    color: '#4a4a4a',
    letterSpacing: -0.32
  }
})
