import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  recordListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  recordItem: {
    width: 165,
    height: 128,
    margin: 1,
    marginRight: 15,
    marginBottom: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  recordItemTitle: {
    backgroundColor: 'transparent',
    fontSize: 13,
    color: '#414141'
  },
  recordList: {},
  container: {
    maxWidth: 564,
    paddingLeft: 10,
    height: '90%'
  },
  disabledButton: {
    opacity: 0.5
  },
  orangeButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#D0D0D0'
  },
  buttonWrapperStyle: {
    width: 165,
    height: 128,
    alignItems: 'center',
    justifyContent: 'center'
  },
  listViewWrapper: {
    flex: 1
  },
  textButton: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  }
})
