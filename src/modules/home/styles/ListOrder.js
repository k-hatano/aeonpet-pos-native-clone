import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  tabContainer: {
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    borderColor: '#979797',
    borderWidth: StyleSheet.hairlineWidth,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderRightWidth: 0
  },
  historyContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  disabledButton: {
    opacity: 0.5
  },
  watchControlContainer: {
    flex: 1,
    marginLeft: 40,
    marginRight: 40,
    marginTop: 5
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8e8e8e'
  },
  headerTitle: {
    marginLeft: 20,
    flex: 1,
    fontWeight: '500'
  },
  sectionHeaderText: {
    marginLeft: 10,
    color: '#fff',
    fontWeight: '400'
  },
  rowTextWrapper: {
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowText: {
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29
  },
  headPicker: {
    height: 35,
    backgroundColor: '#3c3c3c'
  },
  listHeader: {
    height: 64,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#95989a',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingRight: 24
  },
  orderDetail: {
    height: 64,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#94989B',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    paddingRight: 10
  },
  sectionHeader: {
    backgroundColor: '#777',
    flex: 1,
    height: 30
  },
  rowView: {
    flex: 1,
    height: 60
  },
  calendarView: {
    position: 'absolute'
  },
  notificationPosition: {
    right: 1,
    top: 51,
    zIndex: 999
  },
  calendarPosition: {
    right: 0,
    top: 50
  },
  rowTextDate: {
    color: '#9b9b9b',
    fontSize: 18,
    letterSpacing: -0.29
  },
  detailOrderContainer: {
    height: '100%',
    width: '100%',
    flexDirection: 'column'
  },
  detailOrderTabContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  listOrderTitleContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  listOrderTitleText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38,
    fontWeight: 'bold'
  },
  listOrderDateWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  listOrderDateIcon: {
    alignSelf: 'center',
    width: 23,
    height: 24
  },
  listOrderDateText: {
    fontSize: 24,
    color: '#ffffff',
    marginLeft: 12,
    paddingTop: 12,
    fontFamily: 'Helvetica',
    letterSpacing: -0.38,
    alignSelf: 'center',
    lineHeight: 24
  },
  orderTabPanelWrapper: {
    flex: 1,
    backgroundColor: '#fff'
  },
  buttonBackWrapper: {
    flex: 1,
    flexDirection: 'row'
  },
  detailHeaderEmptySection: {
    flex: 1
  },
  listOrderHeaderEmptySection: {
    flex: 1
  },
  sendUnsentButtonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sendUnsentButton: {
    backgroundColor: '#44bcb9',
    borderWidth: 2,
    borderColor: '#ffffff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 13,
    paddingTop: 8,
    paddingBottom: 2
  },
  sendUnsentButtonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 14,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  buttonBackButton: {
    backgroundColor: '#d8d8d8',
    borderWidth: 2,
    borderColor: '#fff',
    borderRadius: 10,
    padding: 12,
    paddingTop: 8,
    paddingBottom: 0
  },
  buttonBackButtonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  tabWrapperStyle: {
    backgroundColor: '#fff'
  },
  tabStyle: {
    width: 136
  },
  orderHistoryContainer: {
    height: 104,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: '#979797'
  },
  sendButton: {
    width: 216,
    height: 40,
    fontSize: 21,
    fontWeight: 'bold',
    borderWidth: 2,
    borderColor: '#fff'
  },
  calenderPosition: {
    right: 0,
    top: 50
  },
  calenderModalContalner: {
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  },
  backButtonContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center'
  },
  backButton: {
    width: 32,
    height: 32
  }
})
