import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    maxHeight: 260
  },
  buttonBigWrapper: {
    flex: 1,
    alignItems: 'center',
    padding: 5,
    paddingLeft: 0,
    maxHeight: 260
  },
  buttonsLeftWrapper: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  buttonAboveWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    padding: 5,
    paddingRight: 0
  },
  buttonBelowWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    padding: 5,
    paddingRight: 0
  },
  orangeButton: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#D0D0D0'
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 110
  },
  sliderArrowTouchable: {
    position: 'absolute',
    width: 25,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sliderArrowIcon: {
    color: '#fff',
    fontSize: 20
  },
  textButton: {
    color: '#000'
  }
})
