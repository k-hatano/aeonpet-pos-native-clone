import I18n from 'i18n-js'
import * as _ from 'underscore'
import Decimal from 'decimal.js'
import { orderStatusToIcon } from '../models'
import { formatPriceWithCurrency } from '../../../common/utils/formats'
import OrderRepository from '../../order/repositories/OrderRepository'
import EJournalRepository from '../../printer/repositories/EJournalRepository'
import CashierCashRecordRepository from '../../cashier/repositories/CashierCashRecordRepository'

export class UnpushedItemBase {
  get icon () { return '' }
  get createdAt () { return 1 }
  get label () { return '' }
}

export class UnpushedOrderItem extends UnpushedItemBase {
  _order = null

  constructor (order) {
    super()
    this._order = order
  }

  get order () {
    return this._order
  }

  get icon () {
    return orderStatusToIcon(this._order.is_minus)
  }

  get createdAt () {
    return this._order.pos_order.client_created_at
  }

  get label () {
    const total = Decimal(this._order.total_paid_taxed).abs()
    return formatPriceWithCurrency(total, this._order.currency)
  }
}

export class UnpushedEJournalItem extends UnpushedItemBase {
  _eJournal = null

  constructor (eJournal) {
    super()
    this._eJournal = eJournal
  }

  get icon () {
    return require('../../../assets/images/homepage/paper_black.png')
  }

  get createdAt () {
    return this._eJournal.client_created_at
  }

  get label () {
    return I18n.t('home.ejournal')
  }
}

export class UnpushedCashierRecordItem extends UnpushedItemBase {
  _cashierRecord = null

  constructor (cashierRecord) {
    super()
    this._cashierRecord = cashierRecord
  }

  get icon () {
    return require('../../../assets/images/homepage/CashierRecord.png')
  }

  get createdAt () {
    return this._cashierRecord.client_created_at
  }

  get label () {
    return formatPriceWithCurrency(this._cashierRecord.total_amount, this._cashierRecord.currency)
  }
}

export const createUnpushedItems = async () => {
  const unpushedOrders = await OrderRepository.findUnpushedOrders()
  const unpushedEJournals = await EJournalRepository.findUnpushedEJournals()
  const unpushedCashierRecords = await CashierCashRecordRepository.findUnpushed()
  const unpushedItems = [
    ...unpushedOrders.map(order => new UnpushedOrderItem(order)),
    ...unpushedEJournals.map(eJournal => new UnpushedEJournalItem(eJournal)),
    ...unpushedCashierRecords.map(cashierRecord => new UnpushedCashierRecordItem(cashierRecord))
  ]
  return _.sortBy(unpushedItems, item => -item.createdAt)
}
