import StoreAccessibleBase from '../../../common/models/StoreAccessibleBase'
import Modal from 'common/components/widgets/Modal'

class ExistModalsOrLoadings extends StoreAccessibleBase {
  get isExistModalsOrLoadings() {
    const cannotAbort = Modal.cannotAbort()
    const isLoading = this.getState().common.isLoading
    const isHomePage = this.getState().common.isHomePage
    return !cannotAbort && !isLoading && isHomePage
  }
}

export default new ExistModalsOrLoadings()