import I18n from 'i18n-js'
import ReceiptBuilder from '../../printer/models/ReceiptBuilder'

export default class OpenDrawerReceiptBuilder extends ReceiptBuilder {
  buildOpenDrawerReceipt (createdAt) {
    return {
      content: [
        {
          element: 'text',
          align: 'left',
          text: this._title()
        },
        {
          element: 'text',
          align: 'left',
          text: this.formatDateTime(createdAt)
        },
        this.staffElement(),
        this.cutElement()
      ]
    }
  }
  _title (balanceType) {
    return I18n.t('receipt.title.open_drawer')
  }
}
