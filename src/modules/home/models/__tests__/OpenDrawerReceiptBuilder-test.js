import OpenDrawerReceiptBuilder from '../OpenDrawerReceiptBuilder'
import ReceiptTextConverter from '../../../printer/models/ReceiptTextConverter'
import I18n from 'i18n-js'
import initialize from 'common/initialize'
import { propertyA } from '../../../printer/samples'
I18n.locale = 'ja'

const targetReceiptText = [
  "ドロアオープン",
  "2017-07-31 17:57:31",
  "担当者:                               スタッフ１",
  ""
].join("\n")

describe('Open Drawer Receipt Builder test', () => {
  it('open drawer receipt text build', async () => {
    const builder = new OpenDrawerReceiptBuilder()
    await builder.initializeAsync()

    const createdAt = new Date("2017-07-31 17:57:31")
    const receiptObj = builder.buildOpenDrawerReceipt(createdAt)
    const receiptTextConverter = new ReceiptTextConverter()
    const receiptText = receiptTextConverter.contentToText(receiptObj.content, propertyA)
    expect(receiptText).toBe(targetReceiptText)
  })
})
