import React from 'react'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import * as ButtonContents from './components/ButtonContents'
import { PERMISSION_CODES } from '../staff/models'
import { isDevMode } from '../../common/utils/environment'
import { makeBaseEJournal, RECEIPT_TYPE } from '../printer/models'
import EJournalRepository from '../printer/repositories/EJournalRepository'
import { IS_MINUS } from 'modules/order/models'
import OrderRepository from 'modules/order/repositories/OrderRepository'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import CashierCashRecordRepository from 'modules/cashier/repositories/CashierCashRecordRepository'
import SettingGetter from 'modules/setting/models/settingGetter'
import NetworkError from 'common/errors/NetworkError'
import AlertView from 'common/components/widgets/AlertView'
import ConfirmView from 'common/components/widgets/ConfirmView'
import { loading } from 'common/sideEffects'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import logger from 'common/utils/logger'

export const MODULE_NAME = 'home'

export const STATUS_SOLD = 0
export const STATUS_RETURNED = 1
export const STATUS_CANCELED = 2

export const TAB_TYPE = {
  OPERATION: 'operation',
  ORDER: 'order'
}

export const OPERATION_NAME = {
  OPEN: '開局',
  CHECK_SALE: '点検',
  CLOSE_SALE: '精算',
  START_TRAINING_MODE: 'トレーニングモード開始',
  END_TRAINING_MODE: 'トレーニングモード終了',
  DEPOSIT: '入金',
  WITHDRAWAL: '出金',
  COMPLETED_SYNC: 'データ同期完了',
  FAILED_SYNC: 'データ同期失敗',
  UNSET_ITEM: '未送信データ発生',
  PRINT_ERROR: '印刷エラー発生',
  OPEN_DRAWER: 'ドロアオープン',
  FORCED_CLOSE_SALE: '強制精算'
}

export const OPERATION_TYPE = {
  OPEN: 1,
  CHECK_SALE: 2,
  CLOSE_SALE: 3,
  START_TRAINING_MODE: 4,
  END_TRAINING_MODE: 5,
  DEPOSIT: 6,
  WITHDRAWAL: 7,
  COMPLETED_SYNC: 8,
  FAILED_SYNC: 9,
  UNSET_ITEM: 10,
  PRINT_ERROR: 11,
  OPEN_DRAWER: 12,
  FORCED_CLOSE_SALE: 13
}

export const orderStatusToIcon = (isMinus) => {
  switch (isMinus) {
    case IS_MINUS.RETURNED:
      return require('../../assets/images/homepage/return_black.png')
    default:
      return require('../../assets/images/homepage/Accounting.png')
  }
}

export const HomeButtons = {
  newOrder: {
    title_key: 'home.new_order',
    content: () => (<ButtonContents.OrderIcon imageStyle={{width: 60, height: 51}} />),
    stylesheet: require('modules/home/styles/HomeMainButton-Primary').default,
    permissionCode: PERMISSION_CODES.NEW_ORDER,
    command: () => Actions.cartPage({ mode: 'order' }),
    isUseTraining: true
  },
  checkSales: {
    title_key: 'home.check_sales',
    content: ButtonContents.CheckSalesIcon,
    stylesheet: require('modules/home/styles/HomeMainButton-Second').default,
    permissionCode: PERMISSION_CODES.CHECK_SALES,
    command: async ({dispatch}) => moveToCheckSales(dispatch),
    isUseTraining: false
  },
  closeSales: {
    title_key: 'home.close_sales',
    content: ButtonContents.CloseSalesIcon,
    stylesheet: require('modules/home/styles/HomeMainButton-Second').default,
    permissionCode: PERMISSION_CODES.CLOSE_SALES,
    command: async ({dispatch}) => moveToCloseSales(dispatch),
    isUseTraining: false
  },
  searchOrder: {
    title_key: 'home.orders_search',
    content: ButtonContents.SearchOrderIcon,
    permissionCode: PERMISSION_CODES.SEARCH_ORDER,
    command: () => Actions.orderSearchPage(),
    isUseTraining: false
  },
  searchCustomer: {
    title_key: 'home.member_search',
    content: ButtonContents.SearchCustomerIcon,
    permissionCode: PERMISSION_CODES.SEARCH_CUSTOMER,
    command: () => Actions.customer(),
    isUseTraining: false
  },
  customerRegister: {
    title_key: 'home.customer_register',
    content: ButtonContents.CustomerRegisterIcon,
    permissionCode: PERMISSION_CODES.CUSTOMER_REGISTER,
    command: () => Actions.customerRegisterPage(),
    isUseTraining: false
  },
  returnOrder: {
    title_key: 'home.return',
    content: ButtonContents.ReturnOrderIcon,
    permissionCode: PERMISSION_CODES.INDEPENDENT_RETURN_ORDER,
    command: () => Actions.cartPage({ mode: 'unassociated_return' }),
    isUseTraining: true
  },
  cashierBalance: {
    title_key: 'home.balance_cache',
    content: ButtonContents.CashierBalanceIcon,
    permissionCode: PERMISSION_CODES.CASHIER_BALANCE,
    command: () => Actions.balances(),
    isUseTraining: false
  },
  searchStock: {
    title_key: 'home.stocks',
    content: ButtonContents.SearchStockIcon,
    permissionCode: PERMISSION_CODES.SEARCH_STOCK,
    command: () => Actions.stockSearch({ init: true }),
    isUseTraining: false
  },
  stockAdjustment: {
    title_key: 'home.adjust_stocks',
    content: ButtonContents.StockAdjustmentIcon,
    permissionCode: PERMISSION_CODES.STOCK_ADJUSTMENT,
    command: () => Actions.stockAdjustmentSearch(),
    isUseTraining: false
  },
  stockMoveOut: {
    title_key: 'home.out_stocks',
    content: ButtonContents.StockMoveOutIcon,
    permissionCode: PERMISSION_CODES.STOCK_MOVE_OUT,
    command: () => Actions.stockMoveOutPage({resetToDefault: true}),
    isUseTraining: false
  },
  stockMoveIn: {
    title_key: 'home.in_stocks',
    content: ButtonContents.StockMoveInIcon,
    permissionCode: PERMISSION_CODES.STOCK_MOVE_IN,
    command: () => Actions.stockMoveInPage({resetToDefault: true}),
    isUseTraining: false
  },
  salesAggregate: {
    title_key: 'home.sales_aggregate',
    content: ButtonContents.SalesAggregateIcon,
    permissionCode: PERMISSION_CODES.SUMMARY_CLOSE_SALES,
    command: () => Actions.closeSalesAggregate(),
    isUseTraining: false
  },
  debug: {
    title_key: 'home.debug',
    content: ButtonContents.StockAdjustmentIcon,
    permissionCode: null,
    command: () => Actions.debug(),
    isUseTraining: false
  },
  help: {
    title_key: 'home.help',
    content: ButtonContents.HelpIcon,
    permissionCode: null,
    command: () => Actions.helpPage(),
    isUseTraining: false
  }
}

const MOVE_TO_SALES_STATUS = {
  CANNOT_MOVE: 0,
  SELECTABLE: 1,
  CAN_MOVE: 2
}

const moveToCheckSales = async (dispatch) => {
  const result = await sendUnpushedItems(dispatch)
  switch (result) {
    case MOVE_TO_SALES_STATUS.CAN_MOVE:
      Actions.checkSales()
      break
    case MOVE_TO_SALES_STATUS.SELECTABLE:
      await ConfirmView.show(I18n.t('message.G-02-I004'), async () => { Actions.checkSales() })
      break
    case MOVE_TO_SALES_STATUS.CANNOT_MOVE:
    default:
      break
  }
}

const moveToCloseSales = async (dispatch) => {
  const canProceed = await clearPendedOrders(OPERATION_NAME.CLOSE_SALE)
  const result = await sendUnpushedItems(dispatch)
  if (canProceed) {
    switch (result) {
      case MOVE_TO_SALES_STATUS.CAN_MOVE:
        Actions.closeSales()
        break
      case MOVE_TO_SALES_STATUS.SELECTABLE:
        await ConfirmView.show(I18n.t('message.G-02-I004'), async () => { Actions.closeSales() })
        break
      case MOVE_TO_SALES_STATUS.CANNOT_MOVE:
      default:
        break
    }
  }
}

const clearPendedOrders = async (operationName) => {
  const pendedOrder = await OrderPendRepository.findAll()
  if (pendedOrder.length === 0) {
    return true
  } else {
    const result = await ConfirmView.showAsync(I18n.t('message.G-02-I005', {cashierRecordModeName: operationName}))

    if (result) {
      try {
        await OrderPendRepository.clearAll()
      } catch (error) {
        await AlertView.showAsync(I18n.t('message.G-02-E008'))
        logger.warning('failed to delete pended order.')
      }
    }

    return result
  }
}

const sendUnpushedItems = async (dispatch) => {
  const unpushedOrders = await OrderRepository.findUnpushedOrders()
  const unpushedEJournals = await EJournalRepository.findUnpushedEJournals()
  const unpushedCashierRecords = await CashierCashRecordRepository.findUnpushed()
  const unpushedCount = unpushedOrders.length + unpushedEJournals.length + unpushedCashierRecords.length

  if (unpushedCount > 0) {
    // 未送信会計の送信を試みる
    try {
      const checkResult = (result) => {
        if (result.errors.length !== 1) { return }
        const error = result.errors[0]
        if (error instanceof NetworkError && (
          error.type === NetworkError.TYPE.OFFLINE ||
          error.type === NetworkError.TYPE.TIMEOUT)) {
          throw error
        }
      }
      let hasError = false
      let cashierCashRecordResult
      await loading(dispatch, async () => {
        const orderResult = await OrderRepository.syncLocalToRemote()
        checkResult(orderResult)
        hasError = hasError || orderResult.errors.length > 0

        const ejournalResult = await EJournalRepository.sendUnpushEjournals()
        checkResult(ejournalResult)
        hasError = hasError || ejournalResult.errors.length > 0

        cashierCashRecordResult = await CashierCashRecordRepository.syncLocalToRemote()
        checkResult(cashierCashRecordResult)
        hasError = hasError || cashierCashRecordResult.errors.length > 0
      })
      if (hasError) {
        if (cashierCashRecordResult.errors.length > 0) {
          await AlertView.showAsync(I18n.t('message.G-02-E005'))
          return MOVE_TO_SALES_STATUS.CANNOT_MOVE
        } else {
          return MOVE_TO_SALES_STATUS.SELECTABLE
        }
      } else {
        return MOVE_TO_SALES_STATUS.CAN_MOVE
      }
    } catch (error) {
      await AlertView.showAsync(getCommonNetworkErrorMessage(error, I18n.t('message.G-02-E006')))
      return MOVE_TO_SALES_STATUS.CANNOT_MOVE
    }
  } else {
    return MOVE_TO_SALES_STATUS.CAN_MOVE
  }
}

export const OptionalHomeBottons = {

}

export const getHomePrimaryMenu = (menus) => {
  return menus.accounting && !isCashierIdUnregistered() ? HomeButtons.newOrder : null
}

export const getHomeSecondaryMenus = (menus) => {
  let secondaryMenus = []
  if (menus.checkSales && !isCashierIdUnregistered()) {
    secondaryMenus.push(HomeButtons.checkSales)
  }
  if (menus.closeSales && !isCashierIdUnregistered()) {
    secondaryMenus.push(HomeButtons.closeSales)
  }
  return secondaryMenus
}

// TODO リファクタリング
export const getHomeSubMenus = (subMenus) => {
  const menus = []
  if (subMenus.orderSearch && !isCashierIdUnregistered()) {
    menus.push(HomeButtons.searchOrder)
  }
  if (subMenus.customerSearch && !isCashierIdUnregistered()) {
    menus.push(HomeButtons.searchCustomer)
  }
  if (subMenus.customerRegister) {
    menus.push(HomeButtons.customerRegister)
  }
  if (subMenus.return && !isCashierIdUnregistered()) {
    menus.push(HomeButtons.returnOrder)
  }
  if (subMenus.depositsAndWithdrawals && !isCashierIdUnregistered()) {
    menus.push(HomeButtons.cashierBalance)
  }
  if (subMenus.stockSearch1) {
    menus.push(HomeButtons.searchStock)
  }
  if (subMenus.stockTaking || isCashierIdUnregistered()) {
    menus.push(HomeButtons.stockTaking)
  }
  if (subMenus.stockIn1) {
    menus.push(HomeButtons.stockMoveIn)
  }
  if (subMenus.stockOut1) {
    menus.push(HomeButtons.stockMoveOut)
  }
  if (subMenus.stockAdjustment1) {
    menus.push(HomeButtons.stockAdjustment)
  }
  if (subMenus.closeSalesSummary && !isCashierIdUnregistered()) {
    menus.push(HomeButtons.salesAggregate)
  }
  if (isDevMode()) {
    menus.push(HomeButtons.debug)
  }
  menus.push(HomeButtons.help)
  return menus
}

export const makeOpenDrawerEJournal = (receipt, createdAt) => {
  return makeBaseEJournal(I18n.t('receipt.title.open_drawer'), receipt, RECEIPT_TYPE.OPEN_DRAWER, createdAt)
}

export const getPrinterErrorCount = async () => {
  const printerErrors = await EJournalRepository.findByIsPrinted(0)
  const printerErrorsCount = Object.keys(printerErrors).length

  return printerErrorsCount
}

export const operationTypeToIconPath = (operationType) => {
  switch (operationType) {
    case OPERATION_TYPE.OPEN:
      return require('../../assets/images/homepage/open.png')
    case OPERATION_TYPE.CHECK_SALE:
      return require('../../assets/images/homepage/check_sale.png')
    case OPERATION_TYPE.CLOSE_SALE:
      return require('../../assets/images/homepage/close_sale.png')
    case OPERATION_TYPE.FORCED_CLOSE_SALE:
      return require('../../assets/images/homepage/close_sale.png')
    case OPERATION_TYPE.START_TRAINING_MODE:
      return require('../../assets/images/homepage/training_mode.png')
    case OPERATION_TYPE.END_TRAINING_MODE:
      return require('../../assets/images/homepage/training_mode.png')
    case OPERATION_TYPE.DEPOSIT:
      return require('../../assets/images/homepage/deposit.png')
    case OPERATION_TYPE.WITHDRAWAL:
      return require('../../assets/images/homepage/withdrawal.png')
    case OPERATION_TYPE.COMPLETED_SYNC:
      return require('../../assets/images/homepage/completed_sync.png')
    case OPERATION_TYPE.FAILED_SYNC:
      return require('../../assets/images/homepage/failed_sync.png')
    case OPERATION_TYPE.UNSET_ITEM:
      return require('../../assets/images/homepage/unset_item.png')
    case OPERATION_TYPE.PRINT_ERROR:
      return require('../../assets/images/homepage/print_error.png')
    case OPERATION_TYPE.OPEN_DRAWER:
      return require('../../assets/images/homepage/open_drawer.png')
    default:
      return null
  }
}

export const operationTypeToLabel = (operationType) => {
  switch (operationType) {
    case OPERATION_TYPE.OPEN:
      return I18n.t('operation.open')
    case OPERATION_TYPE.CHECK_SALE:
      return I18n.t('operation.check_sale')
    case OPERATION_TYPE.CLOSE_SALE:
      return I18n.t('operation.close_sale')
    case OPERATION_TYPE.FORCED_CLOSE_SALE:
      return I18n.t('operation.forced_close_sale')
    case OPERATION_TYPE.START_TRAINING_MODE:
      return I18n.t('operation.start_training_mode')
    case OPERATION_TYPE.END_TRAINING_MODE:
      return I18n.t('operation.Exit_training_mode')
    case OPERATION_TYPE.DEPOSIT:
      return I18n.t('operation.deposit')
    case OPERATION_TYPE.WITHDRAWAL:
      return I18n.t('operation.withdrawal')
    case OPERATION_TYPE.COMPLETED_SYNC:
      return I18n.t('operation.completed_sync')
    case OPERATION_TYPE.FAILED_SYNC:
      return I18n.t('operation.failed_sync')
    case OPERATION_TYPE.UNSET_ITEM:
      return I18n.t('operation.unset_item')
    case OPERATION_TYPE.PRINT_ERROR:
      return I18n.t('operation.print_error')
    case OPERATION_TYPE.OPEN_DRAWER:
      return I18n.t('operation.open_drawer')
    default:
      return ''
  }
}

export const operationTypeToName = (operationType) => {
  switch (operationType) {
    case OPERATION_TYPE.OPEN:
      return OPERATION_NAME.OPEN
    case OPERATION_TYPE.CHECK_SALE:
      return OPERATION_NAME.CHECK_SALE
    case OPERATION_TYPE.CLOSE_SALE:
      return OPERATION_NAME.CLOSE_SALE
    case OPERATION_TYPE.FORCED_CLOSE_SALE:
      return OPERATION_NAME.FORCED_CLOSE_SALE
    case OPERATION_TYPE.START_TRAINING_MODE:
      return OPERATION_NAME.START_TRAINING_MODE
    case OPERATION_TYPE.END_TRAINING_MODE:
      return OPERATION_NAME.END_TRAINING_MODE
    case OPERATION_TYPE.DEPOSIT:
      return OPERATION_NAME.DEPOSIT
    case OPERATION_TYPE.WITHDRAWAL:
      return OPERATION_NAME.WITHDRAWAL
    case OPERATION_TYPE.COMPLETED_SYNC:
      return OPERATION_NAME.COMPLETED_SYNC
    case OPERATION_TYPE.FAILED_SYNC:
      return OPERATION_NAME.FAILED_SYNC
    case OPERATION_TYPE.UNSET_ITEM:
      return OPERATION_NAME.UNSET_ITEM
    case OPERATION_TYPE.PRINT_ERROR:
      return OPERATION_NAME.PRINT_ERROR
    case OPERATION_TYPE.OPEN_DRAWER:
      return OPERATION_NAME.OPEN_DRAWER
    default:
      return null
  }
}

const isCashierIdUnregistered = () => {
  return !SettingGetter.cashierId
}
