import React from 'react'
import { ListView, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OperationLogListView'
import PendedOrderListItem from './PendedOrderListItem'

export default class PendedOrderList extends React.Component {
  static propTypes = {
    pendedOrders: PropTypes.array,
    onSelectOrder: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  render () {
    const source = this.dataSource.cloneWithRows(this.props.pendedOrders)
    return (
      <View style={styles.container}>
        <ListView
          enableEmptySections
          dataSource={source}
          renderSeparator={(sectionId, rowId) =>
            <View
              key={`${sectionId}-${rowId}`}
              style={styles.separator} />}
          renderRow={(order, index) => (
            <PendedOrderListItem
              key={index}
              order={order}
              onSelectOrder={order => this.props.onSelectOrder(order)} />)} />
      </View>
    )
  }
}
