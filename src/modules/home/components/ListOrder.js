'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import Calendar from '../../../common/components/widgets/Calendar'
import Modal from '../../../common/components/widgets/Modal'
import OperationLogListView from '../containers/OperationLogListView'
import OrderHistoryList from '../components/OrderHistoryList'
import UnpushedOrderList from '../components/UnpushedOrderList'
import PendedOrderList from '../components/PendedOrderList'
import OrderSearchResultDetailView from 'modules/order/containers/OrderSearchResultDetailView'
import OrderView from 'modules/order/components/OrderView'
import TabPanel from '/common/components/widgets/TabPanel'
import ImageButton from '../../../common/components/elements/ImageButton'
import { ProceedButton } from 'common/components/elements/StandardButton'
import componentStyles from '../styles/ListOrder'
import OrderHistoryActionsPanel from 'modules/order/containers/OrderHistoryActionsPanel'
import PropTypes from 'prop-types'
import { MODULE_NAME } from '../models'
import { STATUS } from 'modules/order/models'
import { orderTabTitles } from '../../order/models'

export default class ListOrder extends Component {
  static propTypes = {
    onSelectUnpushedItem: PropTypes.func,
    unpushedItems: PropTypes.array,
    onSelectHistoryOrder: PropTypes.func,
    selectedOrder: PropTypes.object,
    orderHistory: PropTypes.array,
    pendedOrders: PropTypes.array,
    initialTab: PropTypes.string,
    repositoryContext: PropTypes.string,
    returnedOrder: PropTypes.object
  }

  constructor (props) {
    super(props)

    this.state = {
      selectedDate: '',
      selectedTab: 0,
      openCalendar: false,
      openNotify: false,
      chosenDate: new Date(),
      isDetail: false
    }
  }

  componentDidMount () {
    if (this.props.initialTab === 'pend') {
      // 未送信データがある場合 => 操作一覧：0、会計一覧：1、未送信一覧：2、保留一覧：3
      // 未送信データがない場合 => 操作一覧：0、会計一覧：1、保留一覧：2
      const tabId = this.props.unpushedItems.length > 0 ? 3 : 2
      this.setState({
        selectedTab: tabId
      })
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.repositoryContext !== nextProps.repositoryContext) {
      this.setState({
        selectedTab: 0
      })
    }
    if (this.props.selectedOrder && this.props.selectedOrder.is_pended && this.props.pendedOrders.length === 1 && nextProps.pendedOrders.length === 0) {
      this.setState({
        selectedTab: 0
      })
    }
    if (!this.props.selectedOrder && nextProps.selectedOrder) {
      this.setState({ isDetail: true })
    }

    if (this.props.selectedOrder && !nextProps.selectedOrder) {
      this.setState({ isDetail: false })
    }
  }

  _onToggleOrderDetail = () => {
    this.setState({ isDetail: false })
    this.props.onSelectHistoryOrder(null)
  }

  _renderSendButton () {
    if (this.props.unpushedItems.length && !this.props.isStockManagementMode) {
      return (
        <ProceedButton
          style={componentStyles.sendButton}
          onPress={() => {
            this.setState({selectedTab: 0})
            this.props.onSendData()
          }}
          text={I18n.t('home.send_unsent_items')} />
      )
    }
  }

  _renderDateSelector () {
    if (!this.props.isStockManagementMode) {
      return (
        <TouchableOpacity
          ref={'calender'}
          style={componentStyles.listOrderDateWrapper}
          onPress={() => this._openModal('calender')}
        >
          <ImageButton
            style={componentStyles.listOrderDateIcon}
            toggle
            ref={'calender'}
            onPress={() => this._openModal('calender')}
            appearance={{
              normal: require('../../../assets/images/homepage/calendar.png'),
              highlight: require('../../../assets/images/homepage/calendar.png')
            }}
          />
          <Text style={componentStyles.listOrderDateText}>
            {I18n.l('date.formats.month_day', this.state.chosenDate)}
          </Text>
        </TouchableOpacity>
      )
    } else {
      return null
    }
  }

  _renderOrderHistoryList () {
    if (!this.props.isStockManagementMode) {
      return (
        <View
          tabName={I18n.t('home.order_history')}
          onTabClick={tabId => this.setState({selectedTab: tabId})}>
          <OrderHistoryList
            orderHistory={this.props.orderHistory}
            onSelectOrder={(order) => this._selectHistoryOrder(order)} />
        </View>
      )
    } else {
      return null
    }
  }

  _renderUnpushedOrderList () {
    if (this.props.unpushedItems.length && !this.props.isStockManagementMode) {
      const unPushedCount = this.props.unpushedItems.length > 9 ? '9+' : this.props.unpushedItems.length
      return (
        <View
          tabNameStyle={{color: 'red'}}
          tabName={I18n.t('home.unsent_items')}
          onTabClick={tabId => this.setState({selectedTab: tabId})}
          error={unPushedCount}>
          <UnpushedOrderList
            unpushedItems={this.props.unpushedItems}
            onSelectItem={(item) => this._selectUnpushedItem(item)} />
        </View>
      )
    } else {
      return null
    }
  }

  _renderPendedOrderList () {
    if (this.props.pendedOrders.length > 0 && !this.props.isStockManagementMode) {
      return (
        <View
          tabNameStyle={{color: 'red'}}
          tabName={I18n.t('home.pending_orders')}
          onTabClick={tabId => this.setState({selectedTab: tabId})}
          error={this.props.pendedOrders.length}>
          <PendedOrderList
            pendedOrders={this.props.pendedOrders}
            onSelectOrder={(order) => this._selectHistoryOrder(order)} />
        </View>
      )
    } else {
      return null
    }
  }

  _openModal (refKey) {
    Modal.openBy(ModalContent, this.refs[refKey], {
      direction: Modal.DIRECTION.TOP,
      isBackgroundVisible: false,
      props: {
        type: refKey,
        onSetState: (state) => {
          this.setState({chosenDate: state})
          this.props.onChangeDate(state)
        }}
    })
  }

  _detailOrder = () => {
    const { selectedOrder, returnedOrder } = this.props
    const isViewButton = selectedOrder && selectedOrder.is_pushed !== false
    const tabTitles = orderTabTitles(selectedOrder)

    return (
      <View style={componentStyles.detailOrderContainer}>
        <View style={componentStyles.orderDetail}>
          <View style={componentStyles.backButtonContainer}>
            <ImageButton
              style={componentStyles.backButton}
              toggle
              appearance={{
                normal: require('../../../assets/images/header/chevron_left.png'),
                highlight: require('../../../assets/images/header/chevron_left.png')
              }}
              onPress={this._onToggleOrderDetail}
            />
          </View>
          <View style={componentStyles.listOrderTitleContainer}>
            <Text style={componentStyles.listOrderTitleText}> {I18n.t('home.detail')} </Text>
          </View>
          <View style={componentStyles.detailHeaderEmptySection} />
        </View>
        <View style={[componentStyles.tabContainer, componentStyles.detailOrderTabContainer]}>
          <TabPanel selectedTab={0} style={componentStyles.tabWrapperStyle} tabStyle={componentStyles.tabStyle}>
            <View tabName={tabTitles.total}>
              <OrderView
                selectedOrder={selectedOrder}
                returnedOrder={null} />
            </View>
            <View tabName={tabTitles.detail}>
              <OrderSearchResultDetailView />
            </View>
            {returnedOrder &&
            <View
              tabName={I18n.t('order.return_total')}
              tabNameStyle={{color: 'red'}}>
              <OrderView
                selectedOrder={selectedOrder}
                returnedOrder={returnedOrder} />
            </View>}
            {selectedOrder.status === STATUS.RETURNED &&
            <View
              tabName={I18n.t('order.return_detail')}
              tabNameStyle={{color: 'red'}}>
              <OrderSearchResultDetailView />
            </View>}
          </TabPanel>
          {isViewButton && (
            <View style={componentStyles.orderHistoryContainer}>
              <OrderHistoryActionsPanel moduleName={MODULE_NAME} />
            </View>)}
        </View>
      </View>
    )
  }

  async _selectUnpushedItem (item) {
    await this.props.onSelectUnpushedItem(item)
  }

  async _selectHistoryOrder (order) {
    this.props.onSelectHistoryOrder(order)
  }

  _listOrder = openCalendar => {
    const errorBackground = this.props.unpushedItems.length ? { backgroundColor: '#ff0000' } : {}
    return (
      <View style={componentStyles.tabContainer}>
        <View style={[componentStyles.listHeader, errorBackground]}>
          <View style={componentStyles.sendUnsentButtonWrapper}>
            {this._renderSendButton()}
          </View>
          <View style={componentStyles.listOrderTitleContainer}>
            <Text style={componentStyles.listOrderTitleText}> {I18n.t('home.history_list')} </Text>
          </View>
          <View style={componentStyles.historyContainer}>
            {this._renderDateSelector()}
          </View>
        </View>
        <View style={componentStyles.orderTabPanelWrapper}>
          <TabPanel selectedTab={this.state.selectedTab} tabStyle={componentStyles.tabStyle}>
            <View tabName={I18n.t('home.operation_history')} onTabClick={tabId => this.setState({selectedTab: tabId})} >
              <OperationLogListView />
            </View>
            {this._renderOrderHistoryList()}
            {this._renderUnpushedOrderList()}
            {this._renderPendedOrderList()}
          </TabPanel>
        </View>
      </View>
    )
  }

  render () {
    const { mainStyles } = this.props
    const { openCalendar } = this.state
    return (
      <View style={mainStyles}>
        <View style={componentStyles.tabContainer}>
          {this.state.isDetail ? this._detailOrder() : this._listOrder(openCalendar)}
        </View>
      </View>
    )
  }
}

class ModalContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      chosenDate: new Date()
    }
  }
  render () {
    return (
      <View style={componentStyles.calenderModalContalner}>
        <Calendar
          visible
          position={componentStyles.calenderPosition}
          maxDate={new Date()}
          onChange={item => {
            this.props.onSetState(item.format('YYYY-MM-DD'))
            Modal.close()
          }} />
      </View>
    )
  }
}
