import React from 'react'
import { ListView, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OperationLogListView'
import UnpushedOrderListItem from './UnpushedOrderListItem'

export default class UnpushedOrderList extends React.Component {
  static propTypes = {
    unpushedItems: PropTypes.array,
    onSelectItem: PropTypes.func,
    onInit: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  componentWillMount () {
    this.props.onInit && this.props.onInit()
  }

  render () {
    const source = this.dataSource.cloneWithRows(this.props.unpushedItems)
    return (
      <View style={styles.container}>
        <ListView
          enableEmptySections
          dataSource={source}
          renderSeparator={(sectionId, rowId) =>
            <View
              key={`${sectionId}-${rowId}`}
              style={styles.separator} />}
          renderRow={(item, index) => (
            <UnpushedOrderListItem
              key={index}
              item={item}
              onSelectItem={item => this.props.onSelectItem(item)}
            />)} />
      </View>
    )
  }
}
