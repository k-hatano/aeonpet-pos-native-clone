import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OperationLogListItem'
import { formatDateTime } from 'common/utils/dateTime'

export default class UnpushedOrderListItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onSelectItem: PropTypes.func
  }

  render () {
    /** @type UnpushedItemBase */
    const item = this.props.item

    return (
      <TouchableOpacity onPress={() => this.props.onSelectItem(item)} >
        <View style={styles.container}>
          <View style={styles.rowView}>
            <View style={styles.logType}>
              <Image
                source={item.icon}
                style={styles.iconStyle} />
            </View>
            <View style={styles.operationAt}>
              <Text style={styles.operationAtText}>{formatDateTime(item.createdAt * 1000)}</Text>
            </View>
            <View style={styles.totalPaid}>
              <Text style={styles.operationLabelText}>{item.label}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
