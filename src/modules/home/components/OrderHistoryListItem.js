import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import Decimal from 'decimal.js'
import styles from '../styles/OperationLogListItem'
import { formatPriceWithCurrency } from 'common/utils/formats'
import { orderStatusToIcon } from '../models'
import { getOrderPaymentString } from '../../payment/models'
import { formatDateTime } from '../../../common/utils/dateTime'
import componentStyles from '../../order/styles/OrderListItem'
import { getTypeOfOrder, ORDER_TYPE } from '../../order/models'

export default class OrderHistoryListItem extends React.Component {
  static propTypes = {
    order: PropTypes.object,
    onSelectOrder: PropTypes.func
  }

  _getStatusIcon = (order) => {
    switch (getTypeOfOrder(order)) {
      case ORDER_TYPE.RETURN:
        return (
          <Image
            source={require('../../../assets/images/homepage/return_black.png')}
            style={componentStyles.iconOrder}
          />
        )
      case ORDER_TYPE.CANCELED:
        return (
          <Image
            source={require('../../../assets/images/homepage/Void.png')}
            style={componentStyles.iconOrder}
          />
        )
      default:
        return (
          <Image
            source={require('../../../assets/images/homepage/Accounting.png')}
            style={componentStyles.iconOrder}
          />
        )
    }
  }

  render () {
    /** @type {OrderDto} */
    const order = this.props.order
    const sign = order.is_minus ? -1 : 1
    const total = new Decimal(order.total_paid_taxed).mul(sign)
    return (
      <TouchableOpacity
        onPress={() => this.props.onSelectOrder(order)}>
        <View style={styles.container}>
          <View style={styles.rowView}>
            <View style={styles.logType}>
              {this._getStatusIcon(order)}
            </View>
            <View style={styles.operationAt}>
              <Text style={styles.operationAtText}>
                {formatDateTime(order.pos_order.client_created_at * 1000)}
              </Text>
            </View>
            <View style={styles.paymentMethodType}>
              <Text style={styles.operationAtText}>
                {getOrderPaymentString(order.order_payments)}
              </Text>
            </View>
            <View style={styles.totalPaid}>
              <Text style={styles.operationLabelText}>
                {formatPriceWithCurrency(total, order.currency)}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
