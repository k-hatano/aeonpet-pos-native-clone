import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/HomeMainButton'
import baseStyle from 'common/styles/baseStyle'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import CustomPropTypes from 'common/components/CustomPropTypes'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import ImageButton from 'common/components/elements/ImageButton'

export default class HomeMainButton extends React.Component {
  static propTypes = {
    style: CustomPropTypes.Style,
    onPress: PropTypes.func,
    repositoryContext: PropTypes.string
  }

  render () {
    if (this.props.repositoryContext === REPOSITORY_CONTEXT.STANDARD) {
      return (
        <ImageButton
          style={this.props.style}
          toggle
          appearance={{
            normal: require('../../../assets/images/homepage/training_off.png'),
            highlight: require('../../../assets/images/homepage/training_off.png')
          }}
          onPress={this.props.onPress} />
      )
    } else if (this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING) {
      return (
        <ImageButton
          style={this.props.style}
          toggle
          appearance={{
            normal: require('../../../assets/images/homepage/training_on.png'),
            highlight: require('../../../assets/images/homepage/training_on.png')
          }}
          onPress={this.props.onPress} />
      )
    }
    return null
  }
}
