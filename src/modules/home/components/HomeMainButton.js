import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/HomeMainButton'
import baseStyle from 'common/styles/baseStyle'
import TouchableOpacitySfx from 'common/components/elements/TouchableOpacitySfx'
import CustomPropTypes from 'common/components/CustomPropTypes'

export default class HomeMainButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.any,
    style: CustomPropTypes.Style,
    stylesheet: PropTypes.object,
    onPress: PropTypes.func
  }

  render () {
    let Content = this.props.content || (() => null)
    const style = this.props.stylesheet || styles

    return (
      <TouchableOpacitySfx onPress={this.props.onPress}>
        <View style={[style.layoutRoot, this.props.style]}>
          <View>
            <Content />
          </View>
          <Text style={style.titleText}>
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacitySfx>
    )
  }
}
