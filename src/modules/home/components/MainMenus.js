'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text, ListView, Image } from 'react-native'
import styles from '../styles/MainMenus'

function MainMenu ({ item }) {
  const disabledStyle = item.disabled ? styles.disabledButton : {}
  return (
    <View style={[styles.recordItem, item.buttonWrapperStyle, disabledStyle]}>
      <TouchableOpacity style={[styles.orangeButton, item.buttonStyle]} onPress={() => item.onPress(item.permissionCode)} disabled={item.disabled}>
        <View style={styles.buttonWrapperStyle}>
          <View style={item.iconsStyle}>
            <Image source={item.firstIcon} style={item.firstIconStyle} />
            {item.secondIcon && <Image source={item.secondIcon} style={item.secondIconStyle} />}
          </View>
          <Text style={[styles.textButton, item.buttonTitleStyle]}> {item.title} </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}
export default class MainMenus extends Component {
  constructor (props) {
    super(props)
    const dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      dataSource
    }
  }

  render () {
    const { dataSource } = this.state
    const { componentStyles, menus } = this.props
    let source = dataSource.cloneWithRows(menus)

    return (
      <View style={[componentStyles, styles.container]}>
        <View style={styles.listViewWrapper}>
          <ListView
            decelerationRate='fast'
            snapToAlignment='start'
            showsHorizontalScrollIndicator={false}
            enableEmptySections
            dataSource={source}
            contentContainerStyle={styles.recordListContainer}
            renderRow={rowData => {
              return <MainMenu item={rowData} />
            }}
          />
        </View>
      </View>
    )
  }
}
