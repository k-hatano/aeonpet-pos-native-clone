import React from 'react'
import { Text, View, Image } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/ButtonContents'
import baseStyle from 'common/styles/baseStyle'

export class ButtonContents extends React.Component {
  static propTypes = {
    name: PropTypes.string
  }

  render () {
    return (
      <View style={styles.layoutRoot}>
        <Text style={baseStyle.mainText}>
          {this.props.name}
        </Text>
      </View>
    )
  }
}

export const CashierBalanceIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/currency_jpy.png')}
      style={{ width: 21, height: 29 }} />
    <Image
      source={require('../../../assets/images/homepage/transfer.png')}
      style={{ width: 20, height: 15, marginLeft: 10 }} />
  </View>
)

export const SearchOrderIcon = () => (
  <View style={{
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: 64,
    height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/bag.png')}
      style={{width: 37, height: 32, marginTop: 9, marginLeft: 5}} />
    <Image
      source={require('../../../assets/images/homepage/search.png')}
      style={{width: 19, height: 19, position: 'absolute', top: 37, left: 40}} />
  </View>
)

export const SearchCustomerIcon = () => (
  <View style={{
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: 64,
    height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/user.png')}
      style={{width: 23, height: 28, marginTop: 11, marginLeft: 11}} />
    <Image
      source={require('../../../assets/images/homepage/search.png')}
      style={{width: 19, height: 19, position: 'absolute', top: 37, left: 40}} />
  </View>
)

export const ReturnOrderIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/return.png')}
      style={{width: 36, height: 33}} />
  </View>
)

export const SearchStockIcon = () => (
  <View style={{
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: 64,
    height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/tag.png')}
      style={{width: 31, height: 31, marginTop: 14, marginLeft: 13}} />
    <Image
      source={require('../../../assets/images/homepage/search.png')}
      style={{width: 19, height: 19, position: 'absolute', top: 37, left: 40}} />
  </View>
)

export const StockTakingIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/checkbox.png')}
      style={{width: 31, height: 31}} />
  </View>
)

export const StockMoveInIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/box.png')}
      style={{width: 30, height: 28}} />
    <Image
      source={require('../../../assets/images/homepage/arrow_left.png')}
      style={{width: 20, height: 21, marginLeft: 4}} />
  </View>
)

export const StockMoveOutIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/box.png')}
      style={{width: 30, height: 28}} />
    <Image
      source={require('../../../assets/images/homepage/arrow_right.png')}
      style={{width: 20, height: 21, marginLeft: 4}} />
  </View>
)

export const StockAdjustmentIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/tags.png')}
      style={{width: 39, height: 31}} />
  </View>
)

export const HelpIcon = () => (
  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/inspect.png')}
      style={{width: 39, height: 31}} />
  </View>
)

export const OrderIcon = ({style, imageStyle}) => (
  <View style={[{alignItems: 'center', justifyContent: 'center', width: 64, height: 64}, style]}>
    <Image
      source={require('../../../assets/images/homepage/cart.png')}
      style={[{width: 36, height: 29}, imageStyle]} />
  </View>
)

export const CheckSalesIcon = () => (
  <View style={{alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/inspect.png')}
      style={{width: 36, height: 29}} />
  </View>
)

export const CloseSalesIcon = () => (
  <View style={{alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/close_sales.png')}
      style={{width: 36, height: 38}} />
  </View>
)

export const SalesAggregateIcon = () => (
  <View style={{alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/sales_aggregate.png')}
      style={{width: 36, height: 50}} />
  </View>
)

export const CustomerRegisterIcon = () => (
  <View style={{alignItems: 'center', justifyContent: 'center', width: 64, height: 64}}>
    <Image
      source={require('../../../assets/images/homepage/user.png')}
      style={{width: 36, height: 50}} />
  </View>
)

/*
export const SearchOrderIcon = () => (
  <View style={{}}>
    <Image
      source={require('../../')}
      style={{}} />
    <Image
      source={require('../../')}
      style={{}} />
  </View>
)
*/
