import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import Decimal from 'decimal.js'
import styles from '../styles/OperationLogListItem'
import { formatPriceWithCurrency } from 'common/utils/formats'
import { orderStatusToIcon } from '../models'
import { formatDateTime } from '../../../common/utils/dateTime'

export default class PendedOrderListItem extends React.Component {
  static propTypes = {
    order: PropTypes.object,
    onSelectOrder: PropTypes.func
  }

  render () {
    const order = this.props.order
    const total = Decimal(order.total_paid_taxed)
    return (
      <TouchableOpacity
        onPress={() => this.props.onSelectOrder(order)}>
        <View style={styles.container}>
          <View style={styles.rowView}>
            <View style={styles.logType}>
              <Image
                source={orderStatusToIcon(order.is_minus)}
                style={styles.iconStyle} />
            </View>
            <View style={styles.operationAt}>
              <Text style={styles.operationAtText}>
                {formatDateTime(order.pos_order.client_created_at * 1000)}
              </Text>
            </View>
            <View style={styles.paymentMethodType}>
              <Text style={styles.operationAtText}>
              </Text>
            </View>
            <View style={styles.totalPaid}>
              <Text style={styles.operationLabelText}>
                {formatPriceWithCurrency(total, order.currency)}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
