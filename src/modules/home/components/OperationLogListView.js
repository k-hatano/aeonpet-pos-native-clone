import React from 'react'
import { ListView, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OperationLogListView'
import OperationLogListItem from './OperationLogListItem'

export default class OperationLogListView extends React.Component {
  static propTypes = {
    operationLogs: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  render () {
    let source = this.dataSource.cloneWithRows(this.props.operationLogs)
    return (
      <View style={styles.container}>
        <ListView
          enableEmptySections
          dataSource={source}
          renderSeparator={(sectionId, rowId) => <View
            key={`${sectionId}-${rowId}`}
            style={styles.separator}
          />}
          renderRow={(item, index) => (
            <OperationLogListItem
              key={index}
              item={item}
            />
          )}
        />
      </View>
    )
  }
}
