import React from 'react'
import { Text, View, Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../styles/OperationLogListItem'
import { operationTypeToIconPath, operationTypeToLabel } from '../models'
import moment from 'moment'

export default class OperationLogListItem extends React.Component {
  static propTypes = {
    item: PropTypes.object
  }

  _getTypeIcon = (type) => {
    let path = operationTypeToIconPath(type)

    if (path) {
      return (
        <Image
          source={path}
          style={styles.iconStyle}
        />
      )
    } else {
      return null
    }
  }

  render () {
    const { item } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.rowView}>
          <View style={styles.logType}>
            {this._getTypeIcon(item.operation_type)}
          </View>
          <View style={styles.operationAt}>
            <Text style={styles.operationAtText}>{moment.unix(item.operation_at).format('YYYY-MM-DD HH:mm:ss')}</Text>
          </View>
          <View style={styles.operationLabel}>
            <Text style={styles.operationLabelText}>{operationTypeToLabel(item.operation_type)}</Text>
          </View>
        </View>
      </View>
    )
  }
}
