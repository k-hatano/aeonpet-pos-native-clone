export default class SampleCashierCashRecordRepository {
  constructor () {
    this._cashierCashRecords = []
  }

  async fetchLatestRecord (cashierId, shopId, cashierCashRecordMode) {
    return {
      cashier_id: cashierId,
      shop_id: shopId,
      carry_forward_amount: 9999
    }
  }

  async push (cashierRecord) {
    console.log('push cash record.', cashierRecord)
  }

  async find () {
    return this._cashierCashRecords
  }

  async findUnpushed () {
    return [
      {
        id: 1,
        client_created_at: 1504593788,
        currency: 'jpy',
        total_amount: 20000
      },
      {
        id: 2,
        client_created_at: 1504593790,
        currency: 'jpy',
        total_amount: 20000
      }
    ]
  }

  async save (cashierRecord) {
    return this._cashierCashRecords.concat(cashierRecord)
  }

  // カスタマイズ-WBS-124
  async fetchWithdrawalAmountSummary () {
    return {
      withdrawal_amount_summary: 100500
    }
  }
  // カスタマイズ-WBS-124
}
