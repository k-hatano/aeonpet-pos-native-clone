import { sampleCashiers } from '../../samples'
import StoreAccessibleBase from '../../../../common/models/StoreAccessibleBase'
import SettingKeys from '../../../setting/models/SettingKeys'

export default class SampleCashierRepository extends StoreAccessibleBase {
  _serverCashiers = sampleCashiers.map(cashier => ({ ...cashier }))

  _cashier = null

  async find () {
    return this._cashier || {...this._cashier}
  }

  async fetchAll () {
    return this._serverCashiers.filter(cashier => {
      return cashier.shop_id === this.getSetting(SettingKeys.COMMON.SHOP_ID)
    })
  }

  async clearAll () {
    console.log('SampleCashierRepository.clear')
    this._cashier = null
  }

  async save (cashier) {
    console.log('SampleCashierRepository.save', cashier)
    this._cashier = cashier
  }

  async pushDeviceIdById (id, deviceId) {
    console.log('SampleCashierRepository.pushDeviceIdById')
    const cashier = this._serverCashiers.find(cashier => cashier.id === id)
    if (cashier) {
      cashier.device_id = deviceId
    }
  }

  async fetchIsOpenedByCashierId (cashierId) {
    // 常にtrueを返す
    return true
  }
}
