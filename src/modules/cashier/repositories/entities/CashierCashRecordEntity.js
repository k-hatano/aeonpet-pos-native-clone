import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'
import AppEvents from '../../../../common/AppEvents'

const CashierCashRecord = sequelize.define(
  'CashierCashRecord',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    cashier_id: Sequelize.STRING,
    staff_id: Sequelize.STRING,
    staff_name: Sequelize.STRING,
    data: Sequelize.TEXT,
    currency: Sequelize.STRING,
    total_amount: Sequelize.INTEGER,
    deposit_amount: Sequelize.INTEGER,
    carry_forward_amount: Sequelize.INTEGER,
    withdrawal_amount: Sequelize.INTEGER, //2019.6 中間回収額追加
    cashier_cash_record_mode: Sequelize.INTEGER,
    is_pushed: Sequelize.BOOLEAN,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    client_created_at: Sequelize.INTEGER
  },
  {
    tableName: 'cashier_cash_records',
    underscored: true,
    timestamps: false
  }
)

export default CashierCashRecord
