import Sequelize from 'sequelize'
import { sequelize } from '../../../../common/DB'

const Cashier = sequelize.define(
  'Cashier',
  {
    id: {
      primaryKey: true,
      type: Sequelize.STRING
    },
    cashier_code: Sequelize.STRING,
    cashier_number: Sequelize.INTEGER,
    status: Sequelize.INTEGER,
    shop_id: Sequelize.STRING,
    device_id: Sequelize.STRING,
    name: Sequelize.TEXT,
    setting_data: Sequelize.TEXT,
    created_at: Sequelize.INTEGER,
    updated_at: Sequelize.INTEGER,
    deleted_at: Sequelize.INTEGER
  },
  {
    tableName: 'cashiers',
    underscored: true,
    timestamps: false
  }
)

export default Cashier
