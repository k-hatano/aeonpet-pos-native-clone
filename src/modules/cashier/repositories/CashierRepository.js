import SampleCashierRepository from './sample/SampleCashierRepository'
import StandardCashierRepository from './standard/StandardCashierRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierRepository {
  static _implement = new SampleCashierRepository()

  static async find () {
    return this._implement.find()
  }

  static async fetchAll () {
    return this._implement.fetchAll()
  }

  static async fetchUnclosedCount () {
    return this._implement.fetchUnclosedCount()
  }

  static async clearAll () {
    return this._implement.clearAll()
  }

  static async save (cashier) {
    return this._implement.save(cashier)
  }

  static async pushDeviceIdById (id, deviceId) {
    return this._implement.pushDeviceIdById(id, deviceId)
  }

  static async fetchIsOpenedByCashierId (cashierId) {
    return this._implement.fetchIsOpenedByCashierId(cashierId)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashierRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new StandardCashierRepository()
        break

      default:
        this._implement = new SampleCashierRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierRepository', context => {
  CashierRepository.switchImplement(context)
})
