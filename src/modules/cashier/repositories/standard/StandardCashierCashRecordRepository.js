import fetcher from 'common/models/fetcher'
import CashierCashRecordEntity from '../entities/CashierCashRecordEntity'
import { handleAxiosError, handleDatabaseError } from 'common/errors'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import DbError from '../../../../common/errors/DbError'
import NetworkError from '../../../../common/errors/NetworkError'
import logger from 'common/utils/logger'
import { REQUEST_ERROR_CODE } from '../../../../common/errors'

export default class StandardCashierCashRecordRepository {
  async fetchLatestRecord (cashierId, shopId, cashierCashRecordMode) {
    try {
      const options = {
        ...encodeCriteria({
          cashier_id: cashierId,
          cashier_cash_record_mode: cashierCashRecordMode
        }),
        ...{
          orderBy: 'client_created_at',
          sortedBy: 'desc',
          limit: 1
        }
      }
      return (await fetcher.get('cashier-cash-records', options)).data[0]
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async push (cashierRecord) {
    try {
      await fetcher.post('cashier-cash-records', cashierRecord)
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async find () {
    const cashierCashRecords = await CashierCashRecordEntity.findAll({
      raw: true
    })
    return cashierCashRecords[0]
  }

  async _syncLocalCashierCashRecordToRemote (cashierCashRecord) {
    let isPushed = false
    try {
      logger.debug('StandardCashierCashRecordRepository._syncLocalCashierCashRecordToRemote ' + cashierCashRecord.data)
      await fetcher.post('cashier-cash-records', cashierCashRecord)
      isPushed = true
    } catch (error) {
      if (this._isDuplicatedError(error)) {
        // 重複データである = 既に送信済みでローカルが認識していなかっただけなので、成功として扱う
        logger.warning('Duplicated CashierCashRecord ' + cashierCashRecord.data)
        isPushed = true
      } else {
        try {
          handleAxiosError(error)
        } catch (error2) {
          if (error2 instanceof NetworkError) {
            if (error2.type === NetworkError.TYPE.SERVER_ERROR) {
              // サーバに異常がある状態のため、調査用に詳細を記録しておく
              logger.error(
                'StandardCashierCashRecordRepository._syncLocalCashierCashRecordToRemote Server Error ' +
                JSON.stringify(error.response) + ' ' +
                cashierCashRecord.data
              )
            } else if (error2.type === NetworkError.TYPE.CLIENT_ERROR) {
              // 想定外の処理をクライアントが行った可能性が高いため、調査用に詳細を記録しておく
              logger.error(
                'StandardCashierCashRecordRepository._syncLocalCashierCashRecordToRemote Client Error ' +
                JSON.stringify(error.response) + ' ' +
                cashierCashRecord.data
              )
            }
          }
          throw error2
        }
      }
    }
    if (isPushed) {
      try {
        await CashierCashRecordEntity.update(
          { is_pushed: true },
          { where: { id: cashierCashRecord.id } }
        )
      } catch (error) {
        throw new DbError(error)
      }
    }
  }

  async syncLocalToRemote () {
    let cashierCashRecords
    try {
      cashierCashRecords = await this.findUnpushed()
    } catch (error) {
      handleDatabaseError(error)
    }

    const errors = []
    const cashierRecordResults = []
    for (const i in cashierCashRecords) {
      try {
        const cashierCashRecord = cashierCashRecords[i]
        const result = await this._syncLocalCashierCashRecordToRemote(cashierCashRecord)
        cashierRecordResults.push(result)
      } catch (error) {
        errors.push(error)
        if (error instanceof NetworkError &&
          (error.type === NetworkError.TYPE.TIMEOUT || error.type === NetworkError.TYPE.OFFLINE)) {
          // タイムアウトとオフラインの場合、次を送信できる見込みがほとんどないので、この時点で終了する
          break
        }
      }
    }
    return { errors, orders: cashierRecordResults }
  }

  async findUnpushed () {
    try {
      const cashierCashRecords = await CashierCashRecordEntity.findAll({
        where: {
          is_pushed: false
        },
        raw: true
      })
      return cashierCashRecords
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  async save (cashierRecord) {
    try {
      await CashierCashRecordEntity.create({
        ...cashierRecord
      })
    } catch (error) {
      handleDatabaseError(error)
    }
  }

  _isDuplicatedError (error) {
    return error.response &&
      error.response.status === 400 &&
      error.response.data &&
      error.response.data.code === REQUEST_ERROR_CODE.DUPLICATED_ENTRY
  }

  // カスタマイズ-WBS-124
  async fetchWithdrawalAmountSummary(cashierId){
    try {
      const withdrawalAmountSummary = await fetcher.get(
        'cashier-cash-records/withdrawal-amount-summary', {
          cashier_id: cashierId
        }
      )
      return withdrawalAmountSummary.data
    } catch (error) {
      handleAxiosError(error)
    }
  }
  // カスタマイズ-WBS-124
}
