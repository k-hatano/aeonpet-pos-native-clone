import fetcher from 'common/models/fetcher'
import CashierEntity from '../entities/CashierEntity'
import { handleAxiosError } from 'common/errors'
import { sampleCashier } from '../../samples'
import AppEvents from 'common/AppEvents'

export default class StandardCashierRepository {
  async find () {
    const cashiers = await CashierEntity.findAll({ raw: true })
    return cashiers[0]
  }

  async fetchAll () {
    try {
      const result = await fetcher.get('cashiers')
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async fetchUnclosedCount () {
    try {
      const result = await fetcher.get('cashiers/unclosed')
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async clearAll () {
    await CashierEntity.destroy({ where: {} })
  }

  async pushDeviceIdById (id, deviceId) {
    try {
      const result = await fetcher.put('cashiers/' + id, { device_id: deviceId })
      return result.data
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async save (cashier) {
    const newCashier = await CashierEntity.create(cashier)
    return newCashier
  }

  async fetchIsOpenedByCashierId (cashierId) {
    try {
      const result = await fetcher.get('cashiers/' + cashierId + '/is-opened')
      return result.data.result
    } catch (error) {
      handleAxiosError(error)
    }
  }

  async createSamples () {
    if (await CashierEntity.count() === 0) {
      await CashierEntity.create((sampleCashier))
    }
  }
}

AppEvents.onSampleDataCreate('StandardCashierRepository', async () => {
  return (new StandardCashierRepository()).createSamples()
})
