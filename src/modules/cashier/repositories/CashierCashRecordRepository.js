import SampleCashierCashRecordRepository from './sample/SampleCashierCashRecordRepository'
import StandardCashierCashRecordRepository from './standard/StandardCashierCashRecordRepository'
import TrainingCashierCashRecordRepository from './training/TrainingCashierCashRecordRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class CashierCashRecordRepository {
  static _implement = new SampleCashierCashRecordRepository()

  /**
   * 前回在高情報をサーバーから取得します。
   * @param cashierId
   * @param shopId
   * @param cashierCashRecordMode
   * @returns {Promise}
   */
  static async fetchLatestRecord (cashierId, shopId, cashierCashRecordMode) {
    return this._implement.fetchLatestRecord(cashierId, shopId, cashierCashRecordMode)
  }

  static async push (opensales) {
    return this._implement.push(opensales)
  }

  static async find () {
    return this._implement.find()
  }

  static async save (opensales) {
    return this._implement.save(opensales)
  }

  static async findUnpushed () {
    return this._implement.findUnpushed()
  }

  static async syncLocalToRemote () {
    return this._implement.syncLocalToRemote()
  }
  // カスタマイズ-WBS-124
  static async fetchWithdrawalAmountSummary(cashierId) {
    return this._implement.fetchWithdrawalAmountSummary(cashierId)
  }
  // カスタマイズ-WBS-124

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardCashierCashRecordRepository()
        break

      case REPOSITORY_CONTEXT.TRAINING:
        this._implement = new TrainingCashierCashRecordRepository()
        break

      default:
        this._implement = new SampleCashierCashRecordRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('CashierCashRecordRepository', context => {
  CashierCashRecordRepository.switchImplement(context)
})
