export const MODULE_NAME = 'cashier'

export const CASHIER_CASH_RECORD_MODE = {
  OPEN_SALE: 1,  // 開局
  CLOSE_SALE: 2  // 点検・精算
}
