import * as _ from 'underscore'
import generateUuid from '../../common/utils/generateUuid'
import { sampleShops, shopIds } from '../shop/samples'

let cashierNumber = 1

export const sampleCashierMap = {
  cashierA_1: {
    id: generateUuid(),
    cashier_code: '1',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジA-1',
    shop_id: shopIds.A
  },
  cashierA_2: {
    id: generateUuid(),
    cashier_code: '2',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジA-2',
    shop_id: shopIds.A
  },
  cashierA_3: {
    id: generateUuid(),
    cashier_code: '23',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジA-3',
    shop_id: shopIds.A
  },
  cashierA_4: {
    id: generateUuid(),
    cashier_code: '4',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジA-4',
    shop_id: shopIds.A
  },
  cashierB_1: {
    id: generateUuid(),
    cashier_code: '1',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジB-1',
    shop_id: shopIds.A
  },
  cashierB_2: {
    id: generateUuid(),
    cashier_code: '2',
    cashier_number: cashierNumber++,
    status: 1,
    name: 'テストレジB-2',
    shop_id: shopIds.A
  }
}

export const sampleCashiers = _.values(sampleCashierMap)

export const sampleCashier = sampleCashierMap.cashierA_1
