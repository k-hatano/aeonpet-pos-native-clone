import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setLoggedUser = createAction(`${MODULE_NAME}_setLoggedUser`)
export const setLoggedDevice = createAction(`${MODULE_NAME}_setLoggedDevice`)
export const setAvailableShops = createAction(`${MODULE_NAME}_setAvailableShops`)
export const setAvailableCashiers = createAction(`${MODULE_NAME}_setAvailableCashiers`)
export const setSelectedShop = createAction(`${MODULE_NAME}_setSelectedShop`)
export const setSelectedCashier = createAction(`${MODULE_NAME}_setSelectedCashier`)
export const refreshLoginScreen = createAction(`${MODULE_NAME}_refreshLoginScreen`)
