import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  loginConfirmation: {
    width: '100%',
    height: '100%',
    zIndex: 1026
  },
  modalContentWrapper: {
    flex: 1
  },
  confirmationTitleWrapper: {
    flex: 1,
    justifyContent: 'center'
  },
  confirmationTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 36,
    color: '#4a4a4a',
    letterSpacing: -0.58,
    textAlign: 'center'
  },
  currentShopWrapper: {
    flex: 1 / 2,
    justifyContent: 'center'
  },
  currentCashierWrapper: {
    flex: 1
  },
  modalContentText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 48,
    color: '#4a4a4a',
    letterSpacing: -0.77,
    textAlign: 'center'
  },
  modalButtonWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  confirmationButtons: {
    width: 312,
    height: 65
  },
  modalLeftButton: {
    marginRight: 100
  },
  confirmationButtonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 36,
    letterSpacing: -0.58,
    textAlign: 'center',
    height: 48,
    paddingTop: 3
  }
})

export default styles
