import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  basicLayoutRootStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  containerCenter: {
    alignItems: 'center',
    marginTop: 75
  },
  instructionTextRow: {
    marginTop: 140
  },
  instructionText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 36,
    color: '#4a4a4a',
    letterSpacing: -0.58,
    textAlign: 'center'
  },
  inputRow: {
    width: 677,
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    flex: 0
  },
  inputLabel: {
    width: 200,
    height: 28,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  inputLabelText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  inputContainer: {
    width: 520,
    height: 48
  },
  inputField: {
    fontFamily: 'Helvetica',
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    width: 520,
    height: 46,
    paddingLeft: 9,
    paddingRight: 9,
    color: '#4a4a4a',
    fontSize: 24,
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  deviceIdTextRow: {
    width: 520,
    height: 28
  },
  deviceIdText: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    height: 28,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  buttonLoginRow: {
    marginTop: 50
  },
  buttonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 36,
    color: '#fff',
    letterSpacing: -0.58,
    height: 48,
    paddingTop: 3
  },
  buttonLogin: {
    width: 474,
    height: 58,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9024',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: '#ff9024'
  },
  innerModal: {
    width: 638,
    height: 679,
    zIndex: 1025
  },
  modalTitle: {
    marginTop: 33,
    marginLeft: 31
  },
  listSeparator: {
    backgroundColor: '#d8d8d8',
    flex: 1,
    height: StyleSheet.hairlineWidth
  },
  listRow: {
    width: 578,
    height: 79
  },
  shopTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  posTitle: {
    paddingTop: 26,
    paddingBottom: 18,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  loginConfirmation: {
    width: '100%',
    height: '100%',
    zIndex: 1026
  },
  confirmationButtons: {
    width: 312,
    height: 65
  },
  confirmationButtonText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 36,
    letterSpacing: -0.58,
    textAlign: 'center',
    height: 48,
    paddingTop: 3
  }
})

export default styles
