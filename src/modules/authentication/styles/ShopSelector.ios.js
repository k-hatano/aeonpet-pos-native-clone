import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  innerModal: {
    width: 638,
    height: 679,
    zIndex: 1025
  },
  modalContentWrapper: {
    alignItems: 'center'
  },
  modalTitle: {
    marginTop: 33,
    marginLeft: 31
  },
  listSeparator: {
    backgroundColor: '#d8d8d8',
    flex: 1,
    height: StyleSheet.hairlineWidth
  },
  listRow: {
    width: 578,
    height: 79
  },
  shopTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  cashierButton: {
    flex: 1
  },
  cashierTitle: {
    paddingTop: 26,
    paddingBottom: 18,
    fontFamily: 'HiraginoSans-W3',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    textAlign: 'left'
  },
  expandingIcon: {
    marginTop: 28,
    width: 21,
    height: 13
  },
  shopSeparator: {
    flexDirection: 'row',
    width: 578,
    height: 79,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d8d8d8'
  },
  shopNameButton: {
    flex: 0.8,
    marginTop: 28
  },
  listCashiers: {
    flexDirection: 'row',
    width: 548,
    paddingLeft: 30
  }
})

export default styles
