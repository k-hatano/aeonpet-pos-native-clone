import React, { Component } from 'react'
import { View, ListView, TouchableOpacity, Text } from 'react-native'
import I18n from 'i18n-js'
import Modal from '../../../common/components/widgets/OldModal'
import ImageButton from '../../../common/components/elements/ImageButton'
import componentStyles from '../styles/ShopSelector'

class ShopSelector extends Component {
  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 })
  }

  _renderExpandButton = shopId => {
    const { selectShop, selectedShop } = this.props

    if (selectedShop && selectedShop.id === shopId) {
      return (
        <ImageButton
          toggle
          style={componentStyles.expandingIcon}
          onPress={() => {
            return false
          }}
          appearance={{
            normal: require('../../../assets/images/collapse.png'),
            highlight: require('../../../assets/images/collapse.png')
          }}
        />
      )
    }
    return (
      <ImageButton
        toggle
        style={componentStyles.expandingIcon}
        onPress={() => {
          selectShop(selectedShop)
        }}
        appearance={{
          normal: require('../../../assets/images/expand.png'),
          highlight: require('../../../assets/images/expand.png')
        }}
      />
    )
  }

  render () {
    const { shops, cashiers, selectedShop, selectShop, chooseCashier, onClose } = this.props
    let availableShops = shops
    if (selectedShop) {
      availableShops = shops.filter(shop => shop.id === selectedShop.id)
    }

    return (
      <Modal.InnerModal
        onClose={onClose}
        position={'center'}
        animationDuration={0}
        modalRef={'shopSelector'}
        backdropPressToClose={false}
        style={componentStyles.innerModal}
        headerStyle={componentStyles.headerStyle}
        titleStyle={componentStyles.modalTitle}
        title={I18n.t('authentication.please_select_shop')}
      >
        <View style={componentStyles.modalContentWrapper}>
          {availableShops.map(shop => {
            const shopId = shop.id
            return (
              <View key={shopId}>
                <View style={componentStyles.shopSeparator}>
                  <TouchableOpacity
                    activeOpacity={0.9}
                    style={componentStyles.shopNameButton}
                    onPress={() => {
                      if (selectedShop && selectedShop.id === shopId) {
                        return false
                      } else {
                        selectShop(shop)
                      }
                    }}
                  >
                    <Text style={componentStyles.shopTitle}>{shop.name}</Text>
                  </TouchableOpacity>
                  {this._renderExpandButton(shopId)}
                </View>
                {selectedShop &&
                  selectedShop.id === shopId &&
                  cashiers.length > 0 &&
                  <View style={componentStyles.listCashiers}>
                    <ListView
                      enableEmptySections
                      dataSource={this.dataSource.cloneWithRows(cashiers)}
                      renderSeparator={(sectionId, rowId) =>
                        <View key={`${sectionId}-${rowId}`} style={componentStyles.listSeparator} />}
                      renderRow={(cashier, index) =>
                        <View key={index} style={componentStyles.listRow}>
                          <TouchableOpacity
                            style={componentStyles.cashierButton}
                            activeOpacity={0.9}
                            onPress={() => {
                              console.log(cashier)
                              chooseCashier(cashier)
                            }}
                          >
                            <Text style={componentStyles.cashierTitle}>
                              {cashier.name}
                            </Text>
                          </TouchableOpacity>
                        </View>}
                    />
                  </View>}
              </View>
            )
          })}
        </View>
      </Modal.InnerModal>
    )
  }
}

export default ShopSelector
