import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import { reduxForm } from 'redux-form'
import { Row, Grid } from 'react-native-easy-grid'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import Modal from '../../../common/components/widgets/OldModal'
import { Field, SubmitButton } from '../../../common/components/elements/Form'
import ShopSelector from './ShopSelector'
import LoginConfirmationModal from './LoginConfirmationModal'
import { isLoggedIn, logout, getUdid, setDeviceToken } from '../../../common/models'
import componentStyles from '../styles/FormLogin'
import AppEvents from 'common/AppEvents'
import { loadSettingsAsync } from '../../setting/models'
import { isDevMode } from 'common/utils/environment'

export const FORM_NAME = 'FormLogin'

class FormLogin extends Component {
  constructor (props) {
    super(props)

    this.deviceId = getUdid()
  }

  _displayShopSelector () {
    Modal.open('shopSelector')
  }

  _closeShopSelector () {
    Modal.close('shopSelector')
  }

  _displayLoginConfirmation () {
    Modal.open('loginConfirmation')
  }

  _closeLoginConfirmation () {
    Modal.close('loginConfirmation')
  }

  _onCloseShopSelector = async () => {
    const { refreshLoginScreen } = this.props
    await logout()
    refreshLoginScreen()
  }

  _confirmLogin = async () => {
    const { registerCashier, selectedCashier, currentDevice } = this.props
    const success = await registerCashier(selectedCashier.id, currentDevice.id)
    if (success) {
      Actions.home()
    } else {
      this._displayNetworkError()
    }
  }

  _displayNetworkError () {
    Alert.alert(I18n.t('errors.error'), I18n.t('errors.network_error'))
    this._onCloseShopSelector()
    this._closeShopSelector()
  }

  _displayNoAvailableCashier () {
    Alert.alert(I18n.t('errors.error'), I18n.t('errors.no_cashier_available'))
    this._onCloseShopSelector()
    this._closeShopSelector()
  }

  _displayLoginFailed () {
    Alert.alert(I18n.t('errors.error'), I18n.t('errors.login_failed'))
    this._onCloseShopSelector()
    this._closeShopSelector()
  }

  async _handleCashierDisplay (device, currentShopId, getCashierList, setAvailableCashiers, refreshDeviceLogin) {
    await setDeviceToken(device.token)
    await refreshDeviceLogin(currentShopId)
    const shopCashiers = await getCashierList(currentShopId)
    if (Array.isArray(shopCashiers) && shopCashiers.length > 0) {
      const registeredCashier = shopCashiers.find(cashier => cashier.device_id === device.id)
      if (registeredCashier) {
        this._chooseCashier(registeredCashier)
      } else {
        const availableCashiers = shopCashiers.filter(cashier => !cashier.device_id)
        if (Array.isArray(availableCashiers) && availableCashiers.length > 0) {
          setAvailableCashiers(availableCashiers)
        } else {
          this._displayNoAvailableCashier()
          return false
        }
      }
    } else {
      this._displayNoAvailableCashier()
      return false
    }
  }

  _selectShop = async (shop = null) => {
    if (!shop) {
      return false
    }

    const {
      currentDevice,
      selectShop,
      deviceLogin,
      registerDevice,
      refreshUserLogin,
      refreshDeviceLogin,
      getCashierList,
      setAvailableCashiers
    } = this.props
    const currentShopId = shop ? shop.id : null
    await selectShop(shop)
    await refreshUserLogin(currentShopId)

    if (currentDevice && currentDevice.token) {
      this._handleCashierDisplay(currentDevice, currentShopId, getCashierList, setAvailableCashiers, refreshDeviceLogin)
    } else {
      const isDeviceRegistered = await registerDevice(this.deviceId)
      if (!isDeviceRegistered) {
        this._displayNetworkError()
        return false
      } else {
        const device = await deviceLogin(this.deviceId)
        if (device && device.token) {
          this._handleCashierDisplay(device, currentShopId, getCashierList, setAvailableCashiers, refreshDeviceLogin)
        } else {
          this._displayNetworkError()
          return false
        }
      }
    }
  }

  _chooseCashier = cashier => {
    if (!cashier) {
      return false
    }
    const { selectCashier } = this.props
    selectCashier(cashier)
    this._displayLoginConfirmation()
  }

  async _handleSubmit (values) {
    const { onLogin, getShopList } = this.props
    const [user, device] = await onLogin(values)
    if (user) {
      if (!device) {
        await getShopList()
      } else {
        const deviceShopId = device.shop_id
        await getShopList(deviceShopId)
      }
      this._displayShopSelector()
    } else {
      this._displayLoginFailed()
      return false
    }
  }

  render () {
    const { cashierFetching, handleSubmit, selectedShop, selectedCashier, shops, cashiers } = this.props

    return (
      <View style={componentStyles.basicLayoutRootStyle}>
        <View style={componentStyles.instructionTextRow}>
          <Text style={componentStyles.instructionText}>
            {I18n.t('authentication.login_instruction')}
          </Text>
        </View>
        <Grid style={componentStyles.containerCenter}>
          <Row style={componentStyles.inputRow}>
            <View style={componentStyles.inputLabel}>
              <Text style={componentStyles.inputLabelText}>
                {I18n.t('authentication.connecting_url')}：
              </Text>
            </View>
            <View style={componentStyles.inputContainer}>
              <Field
                name='connectingUrl'
                autoCapitalize='none'
                returnKeyType='next'
                style={componentStyles.inputField}
                placeholder={I18n.t('authentication.connecting_url')}
              />
            </View>
          </Row>
          <Row style={componentStyles.inputRow}>
            <View style={componentStyles.inputLabel}>
              <Text style={componentStyles.inputLabelText}>
                {I18n.t('authentication.login_id')}：
              </Text>
            </View>
            <View style={componentStyles.inputContainer}>
              <Field
                name='loginId'
                autoCapitalize='none'
                returnKeyType='next'
                style={componentStyles.inputField}
                placeholder={I18n.t('authentication.login_id')}
              />
            </View>
          </Row>
          <Row style={componentStyles.inputRow}>
            <View style={componentStyles.inputLabel}>
              <Text style={componentStyles.inputLabelText}>
                {I18n.t('authentication.password')}：
              </Text>
            </View>
            <View style={componentStyles.inputContainer}>
              <Field
                secureTextEntry
                name='password'
                returnKeyType='send'
                style={componentStyles.inputField}
                placeholder={I18n.t('authentication.password')}
              />
            </View>
          </Row>
          <Row style={componentStyles.inputRow}>
            <View style={componentStyles.inputLabel}>
              <Text style={componentStyles.inputLabelText}>
                {I18n.t('authentication.device_id')}：
              </Text>
            </View>
            <View style={componentStyles.deviceIdTextRow}>
              <Text style={componentStyles.deviceIdText}>{this.deviceId}</Text>
            </View>
          </Row>
          <Row style={componentStyles.buttonLoginRow}>
            <SubmitButton
              label={I18n.t('authentication.login')}
              buttonStyle={componentStyles.buttonLogin}
              textStyle={componentStyles.buttonText}
              onSubmit={handleSubmit(value => this._handleSubmit(value))}
            />
          </Row>
          { !isDevMode() ? <View /> : (
            <Row>
              <SubmitButton
                label={I18n.t('authentication.sample_mode')}
                buttonStyle={componentStyles.buttonLogin}
                textStyle={componentStyles.buttonText}
                onSubmit={async () => {
                  AppEvents.repositoryContextChange('sample')
                  await loadSettingsAsync(this.props.dispatch)
                  Actions.home()
                }}
              />
            </Row>
          )}

        </Grid>
        <ShopSelector
          cashierFetching={cashierFetching}
          onClose={this._onCloseShopSelector}
          shops={shops}
          cashiers={cashiers}
          selectedShop={selectedShop}
          selectShop={this._selectShop}
          chooseCashier={this._chooseCashier}
        />
        <LoginConfirmationModal
          shop={selectedShop}
          cashier={selectedCashier}
          onCancel={this._closeLoginConfirmation}
          onOk={this._confirmLogin}
        />
      </View>
    )
  }
}

export default reduxForm({
  form: FORM_NAME,
  validate: values => {
    const errors = {}
    if (!values.connectingUrl) {
      errors.connectingUrl = I18n.t('common.item_required', { item: I18n.t('authentication.connecting_url') })
    }
    if (!values.loginId) {
      errors.loginId = I18n.t('common.item_required', { item: I18n.t('authentication.login_id') })
    }
    if (!values.password) {
      errors.password = I18n.t('common.item_required', { item: I18n.t('authentication.password') })
    }
    return errors
  },
  initialValues: {
    connectingUrl: 'http://orange-micro-demo.japaneast.cloudapp.azure.com',
    loginId: 'admin@example.com',
    password: 'Admin1234',
    deviceId: getUdid()
  }
})(FormLogin)
