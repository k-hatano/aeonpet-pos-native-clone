import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import Modal from '../../../common/components/widgets/OldModal'
import StandardButton from '../../../common/components/elements/StandardButton'
import componentStyles from '../styles/LoginConfirmationModal'

class LoginConfirmationModal extends Component {
  render () {
    const { shop, cashier, onCancel, onOk } = this.props

    return (
      <Modal.InnerModal
        header={false}
        position={'center'}
        animationDuration={0}
        hasCloseButton={false}
        backdropPressToClose={false}
        modalRef={'loginConfirmation'}
        style={componentStyles.loginConfirmation}
      >
        <View style={componentStyles.modalContentWrapper}>
          <View style={componentStyles.confirmationTitleWrapper}>
            <Text style={componentStyles.confirmationTitle}>
              {I18n.t('authentication.login_confirmation')}
            </Text>
          </View>
          <View style={componentStyles.currentShopWrapper}>
            <Text style={componentStyles.modalContentText}>
              {shop ? shop.name : ''}
            </Text>
          </View>
          <View style={componentStyles.currentCashierWrapper}>
            <Text style={componentStyles.modalContentText}>
              {cashier ? cashier.name : ''}
            </Text>
          </View>
          <View style={componentStyles.modalButtonWrapper}>
            <View style={[componentStyles.confirmationButtons, componentStyles.modalLeftButton]}>
              <StandardButton
                color={'#ff9024'}
                textColor={'#fff'}
                text={I18n.t('authentication.login')}
                textStyle={componentStyles.confirmationButtonText}
                onPress={() => {
                  if (onOk && typeof onOk === 'function') {
                    onOk()
                  }
                }}
              />
            </View>
            <View style={componentStyles.confirmationButtons}>
              <StandardButton
                isBorderButton
                color={'#ff9024'}
                textColor={'#ff9024'}
                text={I18n.t('common.cancel')}
                textStyle={componentStyles.confirmationButtonText}
                onPress={() => {
                  if (onCancel && typeof onCancel === 'function') {
                    onCancel()
                  }
                }}
              />
            </View>
          </View>
        </View>
      </Modal.InnerModal>
    )
  }
}

export default LoginConfirmationModal
