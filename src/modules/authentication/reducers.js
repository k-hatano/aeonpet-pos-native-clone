import { handleActions } from 'redux-actions'
import {
  setLoggedUser,
  setLoggedDevice,
  setAvailableShops,
  setAvailableCashiers,
  setSelectedShop,
  setSelectedCashier,
  refreshLoginScreen
} from './actions'

const defaultState = {
  currentUser: null,
  currentDevice: null,
  selectedShop: null,
  selectedCashier: null,
  shops: [],
  cashiers: []
}

const handlers = {
  [setLoggedUser]: (state, action) => ({
    ...state,
    ...{ currentUser: action.payload }
  }),
  [setLoggedDevice]: (state, action) => ({
    ...state,
    ...{ currentDevice: action.payload }
  }),
  [setSelectedShop]: (state, action) => ({
    ...state,
    ...{ selectedShop: action.payload }
  }),
  [setSelectedCashier]: (state, action) => ({
    ...state,
    ...{ selectedCashier: action.payload }
  }),
  [setAvailableShops]: (state, action) => ({
    ...state,
    ...{ shops: action.payload }
  }),
  [setAvailableCashiers]: (state, action) => ({
    ...state,
    ...{ cashiers: action.payload }
  }),
  [refreshLoginScreen]: state => ({
    ...defaultState
  })
}

export default handleActions(handlers, defaultState)
