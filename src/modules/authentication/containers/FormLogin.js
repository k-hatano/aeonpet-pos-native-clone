import { connect } from 'react-redux'
import { fetchApi } from '../../../common/api'
import FormLogin, { FORM_NAME } from '../components/FormLogin'
import { setConnectingUrl, setUserToken, setDeviceToken, setCurrentShopId } from '../../../common/models'
import { MODULE_NAME } from '../models'
import {
  setLoggedUser,
  setLoggedDevice,
  setSelectedShop,
  setSelectedCashier,
  setAvailableShops,
  setAvailableCashiers,
  refreshLoginScreen
} from '../actions'

function loginByUser ({ dispatch, username, password }) {
  return fetchApi(dispatch, {
    key: '/admin-members/login',
    method: 'post',
    url: '/admin-members/login',
    data: { username, password }
  })
    .then(async response => {
      const user = response.data
      await setUserToken(user.token)
      dispatch(setLoggedUser(user))
      return user
    })
    .catch(() => {
      return null
    })
}

function loginByDevice ({ dispatch, udid }) {
  return fetchApi(
    dispatch,
    {
      key: '/devices/login',
      method: 'post',
      url: '/devices/login',
      data: { udid }
    },
    true
  )
    .then(async response => {
      const device = response.data
      dispatch(setLoggedDevice(device))
      return device
    })
    .catch(() => {
      return null
    })
}

function getShops ({ dispatch }) {
  return fetchApi(
    dispatch,
    {
      key: '/shops',
      method: 'get',
      url: '/shops'
    },
    true
  )
    .then(response => {
      const shops = response.data
      return shops
    })
    .catch(() => {
      return []
    })
}

function registerDevice ({ dispatch, udid }) {
  return fetchApi(
    dispatch,
    {
      key: '/devices',
      method: 'post',
      url: '/devices',
      data: {
        udid,
        name: udid
      }
    },
    true
  )
    .then(response => {
      const registeredDevice = response.data
      return registeredDevice
    })
    .catch(() => {
      return null
    })
}

function getCashiers ({ dispatch, shopId }) {
  return fetchApi(dispatch, {
    key: '/cashiers',
    method: 'get',
    url: '/cashiers'
  })
    .then(response => {
      const cashiers = response.data
      return cashiers.filter(cashier => cashier.shop_id === shopId)
    })
    .catch(() => {
      return []
    })
}

function registerCashier ({ dispatch, cashierId, deviceId }) {
  return fetchApi(dispatch, {
    key: '/cashiers/update',
    method: 'put',
    url: `/cashiers/${cashierId}`,
    data: { device_id: deviceId }
  })
    .then(response => true)
    .catch(() => false)
}

function refreshUserLogin ({ dispatch, shopId }) {
  return fetchApi(
    dispatch,
    {
      key: '/users/login/refresh',
      method: 'put',
      url: '/users/login/refresh',
      data: { shop_id: shopId }
    },
    true
  )
    .then(async response => {
      const user = response.data
      await setUserToken(user.token)
      dispatch(setLoggedUser(user))
      return user
    })
    .catch(() => {
      return null
    })
}

function refreshDeviceLogin ({ dispatch, shopId }) {
  return fetchApi(dispatch, {
    key: '/devices/login/refresh',
    method: 'put',
    url: '/devices/login/refresh',
    data: { shop_id: shopId }
  })
    .then(async response => {
      const device = response.data
      await setDeviceToken(device.token)
      dispatch(setLoggedDevice(device))
      return device
    })
    .catch(() => {
      return null
    })
}

const mapDispatchToProps = dispatch => ({
  onLogin: async values => {
    const { connectingUrl, loginId, password, deviceId } = values
    await setConnectingUrl(connectingUrl)
    let user = null
    let device = null
    user = await loginByUser({ dispatch, username: loginId, password })
    if (user) {
      device = await loginByDevice({ dispatch, udid: deviceId })
    }
    return [user, device]
  },
  deviceLogin: async deviceId => {
    const device = loginByDevice({ dispatch, udid: deviceId })
    return device
  },
  getShopList: async (deviceShopId = null) => {
    const shops = await getShops({ dispatch })
    let availableShops = shops
    if (deviceShopId) {
      availableShops = shops.filter(shop => shop.id === deviceShopId)
    }
    dispatch(setAvailableShops(availableShops))
  },
  setAvailableShops: (shops, device) => {
    const availableShops = shops.filter(shop => shop.id === device.shop_id)
    dispatch(setAvailableShops(availableShops))
  },
  selectShop: async shop => {
    await setCurrentShopId(shop.id)
    dispatch(setSelectedShop(shop))
  },
  registerDevice: async (deviceId, name = '') => {
    const device = registerDevice({ dispatch, udid: deviceId, name })
    return device
  },
  getCashierList: async shopId => {
    const cashiers = await getCashiers({ dispatch, shopId })
    return cashiers
  },
  setAvailableCashiers: cashiers => {
    dispatch(setAvailableCashiers(cashiers))
  },
  selectCashier: async cashier => {
    dispatch(setSelectedCashier(cashier))
  },
  registerCashier: async (cashierId, deviceId) => {
    const cashier = await registerCashier({ dispatch, cashierId, deviceId })
    dispatch(setSelectedCashier(cashier))
    return cashier
  },
  refreshUserLogin: async shopId => {
    const user = refreshUserLogin({ dispatch, shopId })
    return user
  },
  refreshDeviceLogin: async shopId => {
    const device = refreshDeviceLogin({ dispatch, shopId })
    return device
  },
  refreshLoginScreen: () => {
    dispatch(refreshLoginScreen({}))
  }
})

const mapStateToProps = state => ({
  shops: state[MODULE_NAME].shops,
  cashiers: state[MODULE_NAME].cashiers,
  currentUser: state[MODULE_NAME].currentUser,
  currentDevice: state[MODULE_NAME].currentDevice,
  selectedShop: state[MODULE_NAME].selectedShop,
  selectedCashier: state[MODULE_NAME].selectedCashier,
  values: state.form[FORM_NAME] && state.form[FORM_NAME].values,
  cashierFetching: state.common.fetching['/cashiers']
})

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin)
