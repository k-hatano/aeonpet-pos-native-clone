import StockAdjustmentDetailView from '../components/StockAdjustmentDetailView'
import { Record, List } from 'immutable'
import Cart from '../../cart/models/Cart'
import {
  SimpleCartItemSample,
  SetCartItemSample
} from '../../cart/viewSamples/CartItemViewSample'

let cart = new Cart()
cart = cart.addItem(SimpleCartItemSample)

export default {
  name: 'StockAdjustmentDetailView',
  component: StockAdjustmentDetailView,
  properties: [
    {
      title: 'Default',
      property: {
        cart: new Cart(),
        onCartItemSelected: () => {
          console.info('onCartItemSelected')
        },
        onUpdateCart: cart => {
          console.info('onUpdateCart')
        },
        onItemChanged: (cart, oldItem, newItem) => {
          console.info('onItemChanged')
        }
      },
      currency: 'jpy'
    },
    {
      title: 'View Cart Item',
      property: {
        cart: cart,
        onCartItemSelected: () => {
          console.info('onCartItemSelected')
        },
        onUpdateCart: cart => {
          console.info('onUpdateCart')
        },
        onItemChanged: (cart, oldItem, newItem) => {
          console.info('onItemChanged')
        }
      },
      currency: 'jpy'
    }
  ],
  frames: [
    {
      title: 'Default',
      style: { width: '100%', height: '90%' }
    }
  ]
}
