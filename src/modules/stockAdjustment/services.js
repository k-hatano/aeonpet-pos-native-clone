import I18n from 'i18n-js'
import * as actions from './actions'
import AlertView from '../../common/components/widgets/AlertView'
import { getEndOfDay, getStartOfDay, toUnixTimestamp } from '../../common/utils/dateTime'
import { loading } from '../../common/sideEffects'
import { getCommonNetworkErrorMessage } from '../../common/errors'
import StockItemsRepository from '../../modules/stock/repositories/StockItemsRepository'
import StockAdjustmentRepository from '../../modules/stockAdjustment/repositories/StockAdjustmentRepository'

/**
 * 在庫調整検索画面に入力された条件を元に検索の実行と画面表示を行います。
 * @param dispatch
 * @param state
 * @param barcode
 * @returns {Promise<void>}
 */
export const searchStockAdjustments = async (dispatch, state, barcode) => {
  const condition = {}
  if (state.searchStockAdjustmentReason) {
    condition['stock_adjustment_reason_id'] = state.searchStockAdjustmentReason.id
  }
  if (barcode || state.searchWord.length > 0) {
    condition['productSearch'] = barcode || state.searchWord
  }
  const dateFrom = state.searchDateFrom
  const dateFromUnix = toUnixTimestamp(dateFrom)
  const dateTo = state.searchDateTo
  const dateToUnix = toUnixTimestamp(dateTo)

  if (dateFromUnix > dateToUnix) {
    condition['created_at__gte'] = toUnixTimestamp(getStartOfDay(dateTo))
    condition['created_at__lte'] = toUnixTimestamp(getEndOfDay(dateFrom))
    dispatch(actions.setSearchDateFrom(dateTo))
    dispatch(actions.setSearchDateTo(dateFrom))
  } else {
    condition['created_at__gte'] = toUnixTimestamp(getStartOfDay(dateFrom))
    condition['created_at__lte'] = toUnixTimestamp(getEndOfDay(dateTo))
  }
  await loading(dispatch, async () => {
    try {
      const stockAdjustments = await StockAdjustmentRepository.fetch(condition)

      dispatch(actions.setStockAdjustments(stockAdjustments))
      dispatch(actions.setCurrentStockAdjustment({}))

      if (stockAdjustments.length <= 0) {
        AlertView.show(I18n.t('message.M-02-E001'))
      }
    } catch (error) {
      AlertView.show(getCommonNetworkErrorMessage(error))
    }
  })
}

/**
 * 在庫調整した商品の在庫情報をサーバーと同期します。
 * @param dispatch
 * @param stockAdjustmentItems
 * @returns {Promise<void>}
 */
export const syncStockItems = async (dispatch, stockAdjustmentItems) => {
  const vids = stockAdjustmentItems.map(item => {
    return item.product_variant_id
  })
  const stockItems = await StockItemsRepository.fetchByProductVariantIds(vids)
  await StockItemsRepository.bulkSave(stockItems)
}
