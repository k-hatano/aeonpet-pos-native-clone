import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    flex: 3
  },
  backButton: {
    width: 32,
    height: 32
  },
  cancelButton: {
    width: 144,
    height: 60,
    marginLeft: 10,
    marginBottom: 10,
    fontSize: 24,
    fontWeight: 'bold'
  },
  subHeaderWrapper: {
    flex: 1
  },
  subHeaderText: {
    fontSize: 24,
    color: '#fff',
    alignSelf: 'center',
    fontWeight: 'bold',
    height: 25,
    marginRight: 32
  },
  tabHeaderWrapper: {
    flexDirection: 'row',
    backgroundColor: '#9b9b9b',
    paddingLeft: 10,
    paddingRight: 10,
    height: 64,
    alignItems: 'center'
  },
  tabStyle: {
    width: 136
  }
})
