import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  root: {
    flex: 3
  },
  headerTextContainer: {
    height: 64,
    justifyContent: 'center',
    borderColor: baseStyleValues.borderColor,
    borderBottomWidth: 1,
    backgroundColor: '#9b9b9b'
  },
  headerText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38,
    textAlign: 'center',
    fontWeight: 'bold',
    height: 25
  },
  listItemContainer: {
    borderColor: '#d8d8d8',
    borderBottomWidth: 1,
    height: 64,
    flexDirection: 'row',
    padding: 10,
    marginLeft: 24,
    marginRight: 24
  },
  createdAtContainer: {
    flex: 2.5,
    justifyContent: 'center'
  },
  createdAtText: {
    fontSize: 18,
    height: 19,
    color: baseStyleValues.subTextColor
  },
  reasonContainer: {
    flex: 3,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  reasonText: {
    fontSize: 20,
    height: 21,
    color: baseStyleValues.mainTextColor,
    paddingLeft: 10
  },
  staffContainer: {
    flex: 2,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  staffText: {
    fontSize: 20,
    height: 21,
    color: baseStyleValues.mainTextColor
  },
  listContainer: {
    height: '92%'
  }
})
