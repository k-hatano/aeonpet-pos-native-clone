import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  listContainer: {
    height: '100%'
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
