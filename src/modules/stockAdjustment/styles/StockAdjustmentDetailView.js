import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
    flex: 3,
    marginTop: 5
  },
  contentWrapper: {
    borderColor: '#d8d8d8',
    borderBottomWidth: 1,
    height: 50,
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    marginTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 8
  },
  titleTextWrapper: {
    flex: 1
  },
  titleText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: baseStyleValues.subTextColor,
    letterSpacing: -0.29
  },
  contentTextWrapper: {
    flex: 3,
    alignItems: 'flex-end'
  },
  contentText: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    color: baseStyleValues.mainTextColor,
    letterSpacing: -0.32
  }
})
