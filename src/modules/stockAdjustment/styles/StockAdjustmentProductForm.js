import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  categoryProductViewContainer: {
    justifyContent: 'flex-end',
    height: '90%'
  },
  completeButton: {
    fontSize: 36,
    padding: 10,
    marginTop: 25
  }
})
