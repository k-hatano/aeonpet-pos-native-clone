import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  root: {
    flex: 3,
    marginTop: 5
  },
  headPicker: {
    width: 100,
    height: 30,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: '#DDDDDD'
  },
  headPickerText: {
    color: '#373737'
  },
  calendarView: {
    position: 'absolute',
    zIndex: 999
  },
  formSearchLayout: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  dateWrapper: {
    flexDirection: 'row',
    margin: 0,
    marginTop: 15,
    width: '100%'
  },
  dateTextWrapper: {
    flex: 2,
    marginLeft: 10,
    justifyContent: 'center'
  },
  btnClearWrapper: {
    alignItems: 'flex-end',
    padding: 10,
    marginBottom: 30
  },
  btnClear: {
    width: 144,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30
  },
  btnSearchWrapper: {
    alignItems: 'flex-end',
    padding: 10,
    paddingTop: 0
  },
  btnSearch: {
    height: 60,
    width: '85%',
    marginRight: 30,
    marginBottom: 15
  },
  searchInput: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: '100%',
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  searchInputArea: {
    height: 200,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: '100%',
    fontSize: 24,
    color: baseStyleValues.mainTextColor,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  touchableContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  inputContainer: {
    height: 50,
    flex: 8
  },
  searchInputTotal: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: baseStyleValues.borderColor,
    width: 250,
    fontSize: 24,
    backgroundColor: baseStyleValues.mainBackgroundColor
  },
  commonText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontFamily: 'HiraginoSans-W3'
  },
  innerContainer: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 20
  },
  scrollView: {
    width: '100%',
    height: '100%'
  },
  sampleRowContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 10
  },
  buttonRowContainer: {
    marginRight: 10,
    marginLeft: 10
  },
  sampleRowTextContainer: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  sampleRowTitle: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  rowLayout30: {
    marginBottom: 30
  },
  rowLayout: {
    marginBottom: 20
  },
  selectContainer: {
    width: '100%',
    flexDirection: 'row'
  },
  select: {
    width: '100%',
    marginTop: 10
  },
  selectInput: {
    width: '100%',
    marginTop: 5
  },
  calendarContainer: {
    zIndex: 2,
    paddingHorizontal: 15
  },
  paymentMethodWrapper: {
    zIndex: 1000
  },
  inlineRowLayout: {
    marginBottom: 25
  },
  button: {
    width: 36,
    height: 36,
    color: 'white',
    fontSize: 26,
    backgroundColor: baseStyleValues.mainColor
  },
  buttonTouchableArea: {
    width: 42,
    height: 36,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 15
  },
  operationArea: {
    flexDirection: 'row',
    marginRight: 24
  },
  subText: {
    fontSize: 22,
    marginLeft: 20,
    marginTop: 10
  },
  subButtonStyle: {
    fontSize: 18,
    height: 50,
    marginBottom: 5,
    width: 200,
    alignSelf: 'flex-end'
  },
  searchButtonStyle: {
    fontSize: 18,
    height: 50,
    width: '70%',
    marginTop: 30,
    alignSelf: 'center'
  },
  clearButtonArea: {
    alignSelf: 'flex-end',
    width: 144
  },
  clearButton: {
    fontSize: 24,
    fontWeight: 'bold',
    height: 56
  },
  listContainer: {
    borderColor: '#979797',
    borderTopWidth: 1,
    marginTop: 20
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8'
  }
})
