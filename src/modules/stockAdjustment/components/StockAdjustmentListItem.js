import React, { Component } from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../styles/StockAdjustmentListItem'
import { formatPriceWithCurrency, formatUnit } from '../../../common/utils/formats'
import { MAX_ITEM_QUANTITY } from '../models'
import StockAdjustmentInputNumberForm from './StockAdjustmentInputNumberForm'
import baseStyle from '../../../common/styles/baseStyle'
import Swipeout from 'react-native-swipeout'
import Modal from '../../../common/components/widgets/Modal'

export default class StockAdjustmentListItem extends Component {
  static propTypes = {
    product: PropTypes.object,
    currency: PropTypes.string,
    onChangeQuantity: PropTypes.func
  }

  render () {
    const { product, onChangeQuantity, currency } = this.props
    const swipeOutButton = [
      {
        text: I18n.t('stock_adjustment.delete'),
        onPress: () => onChangeQuantity(product.id, 0)
      }
    ]
    const productVariant = product.product_variants[0]
    // 在庫レコードが存在しない場合は 0 として扱う
    const originalQuantity = productVariant.stock_item ? productVariant.stock_item.quantity : 0
    return (
      <View>
        <View style={styles.root} ref={'layoutRoot'}>
          <Swipeout
            right={swipeOutButton}
            autoClose
            backgroundColor={'transparent'}>
            <View style={styles.headerArea}>
              <View style={[styles.productLabelArea, { flex: 3 }]}>
                <Text
                  numberOfLines={1}
                  ellipsizeMode={'middle'}
                  style={baseStyle.subText}>
                  {product.product_code}
                </Text>
              </View>
            </View>
            <View style={styles.mainArea}>
              <View style={styles.productSubInfoArea}>
                <Text numberOfLines={1} style={[baseStyle.mainText, styles.productNameText]}>
                  {product.name}
                </Text>
              </View>
            </View>
            <View style={styles.subArea}>
              <View style={styles.productSubInfoArea}>
                <Text style={[baseStyle.subText, styles.unitPriceText]}>
                  {I18n.t('cart.unit_price')} : {formatPriceWithCurrency(productVariant.price, currency)}
                </Text>
                <Text style={[baseStyle.subText, styles.unitPriceText]}>
                  {I18n.t('order.stock')} : {formatUnit(originalQuantity || 0, I18n.t('stock_search.item'))}
                </Text>
              </View>
              <View style={styles.operationArea}>
                <StockAdjustmentInputNumberForm
                  onIncrease={() => onChangeQuantity(product.id, product.quantity + 1)}
                  onDecrease={() => onChangeQuantity(product.id, product.quantity - 1)}
                  onChange={number => {
                    Modal.close()
                    return onChangeQuantity(product.id, number)
                  }}
                  number={product.quantity}
                  maxNumber={MAX_ITEM_QUANTITY}
                  canIncreaseDecrease
                />
              </View>
            </View>
            <View style={styles.footer} />
          </Swipeout>
        </View>
      </View>
    )
  }
}
