import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/StockAdjustmentSearchDetailView'
import TabPanel from 'common/components/widgets/TabPanel'
import { WeakProceedButton } from 'common/components/elements/StandardButton'
import StockAdjustmentDetailView from '../containers/StockAdjustmentDetailView'
import StockAdjustmentProductView from '../containers/StockAdjustmentProductView'
import ImageButton from 'common/components/elements/ImageButton'

export default class StockAdjustmentSearchDetailView extends Component {
  static propTypes = {
    onBack: PropTypes.func,
    onCancel: PropTypes.func
  }
  render () {
    const { onBack, onCancel } = this.props
    return (
      <View style={componentStyles.root}>
        <View style={componentStyles.tabHeaderWrapper}>
          <ImageButton
            style={componentStyles.backButton}
            toggle
            appearance={{
              normal: require('../../../assets/images/header/chevron_left.png'),
              highlight: require('../../../assets/images/header/chevron_left.png')
            }}
            onPress={() => onBack()}
          />
          <View style={componentStyles.subHeaderWrapper}>
            <Text style={componentStyles.subHeaderText}>
              {I18n.t('stock_adjustment.details')}
            </Text>
          </View>
        </View>
        <TabPanel tabStyle={componentStyles.tabStyle}>
          <View
            tabName={I18n.t('stock_adjustment.receipt_information')}
            onTabClick={() => {}}>
            <StockAdjustmentDetailView />
          </View>
          <View
            tabName={I18n.t('stock_adjustment.description')}
            onTabClick={() => {}}>
            <StockAdjustmentProductView />
          </View>
        </TabPanel>
        <WeakProceedButton
          style={componentStyles.cancelButton}
          text={I18n.t('stock_adjustment.cancel')}
          onPress={() => onCancel(this.props.stockAdjustment, this.props.settings, this.props.stockAdjustmentState)}
        />
      </View>
    )
  }
}
