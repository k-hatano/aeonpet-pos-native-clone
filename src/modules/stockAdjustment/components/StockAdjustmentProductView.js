import React, { Component } from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import componentStyles from '../styles/StockAdjustmentProductView'
import StockAdjustmentProductItemView from './StockAdjustmentProductItemView'
import _ from 'underscore'

export default class StockAdjustmentProductView extends Component {
  static propTypes = {
    currency: PropTypes.string
  }

  _sortItems (stockAdjustmentItems) {
    const resultArray = _.sortBy(stockAdjustmentItems, item => item.row_number)
    return resultArray
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    const { currency, stockAdjustment } = this.props
    const stockAdjustmentItems = this._sortItems(stockAdjustment.stockAdjustmentItems)

    return (
      <View>
        <ListView
          enableEmptySections
          style={componentStyles.listContainer}
          dataSource={dataSource.cloneWithRows(stockAdjustmentItems)}
          renderRow={product => (
            <View>
              <StockAdjustmentProductItemView
                product={product}
                currency={currency}
              />
              <View style={componentStyles.separator} />
            </View>
          )}
        />
      </View>
    )
  }
}
