import React, { Component } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/StockAdjustmentDetailView'
import { formatDateTime } from 'common/utils/dateTime'

export default class StockAdjustmentDetailView extends Component {
  static propTypes = {
    stockAdjustment: PropTypes.object
  }
  render () {
    const { stockAdjustment } = this.props
    return (
      <View style={componentStyles.root}>
        <View>
          <View style={componentStyles.contentWrapper}>
            <View style={componentStyles.titleTextWrapper}>
              <Text style={componentStyles.titleText}>
                {I18n.t('stock_adjustment.adjustment_at')}
              </Text>
            </View>
            <View style={componentStyles.contentTextWrapper}>
              <Text style={componentStyles.contentText}>
                {formatDateTime(Math.floor(stockAdjustment.created_at * 1000))}
              </Text>
            </View>
          </View>
          <View style={componentStyles.contentWrapper}>
            <View style={componentStyles.titleTextWrapper}>
              <Text style={componentStyles.titleText}>
                {I18n.t('stock_adjustment.adjustment_reason')}
              </Text>
            </View>
            <View style={componentStyles.contentTextWrapper}>
              <Text style={componentStyles.contentText}>
                {stockAdjustment.stock_adjustment_reason_name}
              </Text>
            </View>
          </View>
          <View style={componentStyles.contentWrapper}>
            <View style={componentStyles.titleTextWrapper}>
              <Text style={componentStyles.titleText}>
                {I18n.t('stock_adjustment.staff_name')}
              </Text>
            </View>
            <View style={componentStyles.contentTextWrapper}>
              <Text style={componentStyles.contentText}>
                {stockAdjustment.staff_name}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}
