'use strict'
import React, { Component } from 'react'
import I18n from 'i18n-js'
import {
  View,
  TextInput,
  TouchableHighlight,
  ListView,
  Text,
  TouchableOpacitySfx
} from 'react-native'
import PropTypes from 'prop-types'
import StockAdjustmentListItem from './StockAdjustmentListItem'
import componentStyles from '../styles/StockAdjustmentList'
import Select from 'common/components/elements/Select'
import { stockAdjustmentFormGroup } from '../models'
import { ProceedButton } from 'common/components/elements/StandardButton'

export default class StockAdjustmentList extends Component {
  static propTypes = {
    products: PropTypes.array,
    currency: PropTypes.string,
    stockAdjustmentReasons: PropTypes.array,
    stockAdjustmentReason: PropTypes.object,
    onClear: PropTypes.func,
    onChangeQuantity: PropTypes.func,
    onSelectStockAdjustmentReason: PropTypes.func
  }

  _onSelectStockAdjustmentReason (item) {
    const stockAdjustmentReason = item.id ? item : null
    this.props.onSelectStockAdjustmentReason(stockAdjustmentReason)
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const {
      products,
      currency,
      stockAdjustmentReasons,
      stockAdjustmentReason,
      onChangeQuantity,
      onClear
    } = this.props
    return (
      <View style={componentStyles.root}>
        <View style={[componentStyles.sampleRowContainer]}>
          <View style={componentStyles.sampleRowTitle}>
            <Text style={componentStyles.commonText}>
              {I18n.t('stock_adjustment.adjustment_reason')} :{' '}
            </Text>
          </View>
          <View style={componentStyles.sampleRowTextContainer}>
            <Select
              style={componentStyles.selectInput}
              items={stockAdjustmentReasons}
              selectedItem={stockAdjustmentReason}
              displayProperty='name'
              onItemChange={item => this._onSelectStockAdjustmentReason(item)}
              placeholder={I18n.t('stock_adjustment.not_selected')}
            />
          </View>
        </View>
        <View style={componentStyles.buttonRowContainer}>
          <View style={componentStyles.clearButtonArea}>
            <ProceedButton
              style={componentStyles.clearButton}
              text={I18n.t('stock_adjustment.clear')}
              onPress={() => onClear()}
            />
          </View>
        </View>
        <ListView
          enableEmptySections
          style={componentStyles.listContainer}
          dataSource={dataSource.cloneWithRows(products)}
          renderRow={product => (
            <View>
              <StockAdjustmentListItem
                product={product}
                currency={currency}
                onChangeQuantity={(id, quantity) =>
                  onChangeQuantity(id, quantity, products)}
              />
              <View style={componentStyles.separator} />
            </View>
          )}
        />
      </View>
    )
  }
}
