'use strict'
import React, { Component } from 'react'
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/StockAdjustmentSearchForm'
import Select from '../../../common/components/elements/Select'
import {
  OptionalCommandButton,
  CommandButton,
  ProceedButton
} from '../../../common/components/elements/StandardButton'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import { formatDate } from '../../../common/utils/dateTime'
import DateTimePicker from 'react-native-modal-datetime-picker'
import dateTimePickerStyles from '../../../common/styles/elements/DateTimePicker'

function RowLayout (props) {
  return (
    <View style={[componentStyles.sampleRowContainer, props.style && props.style]}>
      <View style={componentStyles.sampleRowTitle}>
        {props.title && (
          <Text style={componentStyles.commonText}>{props.title} : </Text>
        )}
      </View>
      <View style={componentStyles.sampleRowTextContainer}>
        {props.children}
      </View>
    </View>
  )
}

export default class StockAdjustmentSearchForm extends Component {
  static propTypes = {
    stockAdjustmentReasons: PropTypes.array,
    onSearch: PropTypes.func,
    onCreate: PropTypes.func,
    onClear: PropTypes.func,
    onItemChange: PropTypes.func,
    onChangeText: PropTypes.func,
    onSetFromDate: PropTypes.func,
    onSetToDate: PropTypes.func,
    isSearchProduct: PropTypes.bool
  }
  static defaultProps = {
    isSearchProduct: true
  }

  constructor (props) {
    super(props)
    this.state = {
      isDatePickerVisible: false,
      date: new Date(),
      datePickerTitle: '',
      dateKey: ''
    }
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      if (!this.props.isSearchProduct) return

      await this.props.onChangeText(body.trim())
    })
    this.props.onClear()
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  _showDateTimePicker (title, date, key) {
    this.setState({
      isDatePickerVisible: true,
      date: date ? new Date(date) : new Date(),
      datePickerTitle: title,
      dateKey: key
    })
  }

  _hideDateTimePicker = () => this.setState({ isDatePickerVisible: false })

  _handleDatePicked (date) {
    this.setState({ isDatePickerVisible: false })

    if (this.state.dateKey === 'searchDateFrom') {
      this.props.onSetFromDate(date)
    } else if (this.state.dateKey === 'searchDateTo') {
      this.props.onSetToDate(date)
    }
  }

  render () {
    const {
      stockAdjustmentReasons,
      onCreate
    } = this.props

    return (
      <View style={componentStyles.root}>
        <ScrollView>
          {
            <View style={componentStyles.innerContainer}>
              <View style={componentStyles.formSearchLayout}>
                <View style={componentStyles.newButtonArea}>
                  <OptionalCommandButton
                    style={componentStyles.newButtonStyle}
                    text={I18n.t('stock_adjustment.new_registration')}
                    onPress={() => onCreate()}
                  />
                </View>
                <RowLayout title={I18n.t('stock_adjustment.adjustment_reason')}>
                  <View style={componentStyles.selectContainer}>
                    <Select
                      style={componentStyles.selectInput}
                      items={stockAdjustmentReasons}
                      selectedItem={this.props.searchStockAdjustmentReason}
                      displayProperty='name'
                      onItemChange={item => this.props.onItemChange(item)}
                      placeholder={I18n.t('stock_adjustment.not_selected')}
                    />
                  </View>
                </RowLayout>
                <RowLayout title={I18n.t('stock_adjustment.product')}>
                  <TextInput
                    style={componentStyles.searchInput}
                    onChangeText={text => this.props.onChangeText(text)}
                    value={this.props.searchWord}
                    placeholder={I18n.t('stock_adjustment.product_search_word')}
                  />
                </RowLayout>
                <RowLayout title={I18n.t('stock_adjustment.adjustment_at')}>
                  <View style={componentStyles.dateWrapper}>
                    <View style={componentStyles.inputContainer}>
                      <TextInput
                        ref='dateFrom'
                        style={[dateTimePickerStyles.datePickerInput]}
                        value={formatDate(this.props.searchDateFrom)} />
                      <TouchableOpacity
                        style={dateTimePickerStyles.datePickerTouchable}
                        onPress={() => {
                          this._showDateTimePicker(
                            I18n.t('stock_adjustment.adjustment_at_from'),
                            formatDate(this.props.searchDateFrom),
                            'searchDateFrom')
                        }} />
                    </View>
                    <View style={componentStyles.dateTextWrapper}>
                      <Text style={componentStyles.commonText}>
                        {I18n.t('common.from')}
                      </Text>
                    </View>
                  </View>
                </RowLayout>
                <RowLayout>
                  <View style={componentStyles.dateWrapper}>
                    <View style={componentStyles.inputContainer}>
                      <TextInput
                        ref='dateTo'
                        style={[dateTimePickerStyles.datePickerInput]}
                        value={formatDate(this.props.searchDateTo)} />
                      <TouchableOpacity
                        style={dateTimePickerStyles.datePickerTouchable}
                        onPress={() => {
                          this._showDateTimePicker(
                            I18n.t('stock_adjustment.adjustment_at_to'),
                            formatDate(this.props.searchDateTo),
                            'searchDateTo')
                        }} />
                    </View>
                    <View style={componentStyles.dateTextWrapper}>
                      <Text style={componentStyles.commonText}>
                        {I18n.t('common.until')}
                      </Text>
                    </View>
                  </View>
                </RowLayout>
                <View style={{ width: '100%', justifyContent: 'flex-end' }}>
                  <View style={componentStyles.subButtonArea}>
                    <ProceedButton
                      style={componentStyles.subButtonStyle}
                      text={I18n.t('stock_adjustment.clear')}
                      onPress={() => this.props.onClear()}
                    />
                  </View>
                </View>
                <View style={{ justifyContent: 'flex-end' }}>
                  <View style={componentStyles.searchButtonArea}>
                    <CommandButton
                      style={componentStyles.searchButtonStyle}
                      text={I18n.t('stock_adjustment.search')}
                      onPress={() => this.props.onSearch(this.props)}
                    />
                  </View>
                </View>
              </View>
            </View>
          }
        </ScrollView>
        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          mode='date'
          date={this.state.date}
          titleIOS={this.state.datePickerTitle}
          titleStyle={dateTimePickerStyles.datePickerTitle}
          confirmTextIOS={I18n.t('common.ok')}
          confirmTextStyle={dateTimePickerStyles.datePickerConfirm}
          cancelTextIOS={I18n.t('common.cancel')}
          cancelTextStyle={dateTimePickerStyles.datePickerCancel}
          minimumDate={new Date('1900/01/01')}
          onConfirm={(date) => { this._handleDatePicked(date) }}
          onCancel={this._hideDateTimePicker}
          neverDisableConfirmIOS />
      </View>
    )
  }
}
