import CartItemNumberForm from '../../cart/components/CartInputNumberForm'
import AmountPadModal from 'common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from 'common/components/elements/AmountPad'

export default class StockAdjustmentItemNumberForm extends CartItemNumberForm {
  _onChangeAmount () {
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.NUMBER,
      maxValue: this.props.maxNumber,
      onComplete: amount => {
        return this.props.onChange && this.props.onChange(amount)
      }
    })
  }
}
