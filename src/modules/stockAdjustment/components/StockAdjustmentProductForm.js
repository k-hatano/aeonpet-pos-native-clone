'use strict'
import React, { Component } from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import styles from '../styles/StockAdjustmentProductForm'
import { CommandButton } from '../../../common/components/elements/StandardButton'
import CategoryProductView from '../../product/containers/CategoryProductView'
import AmountPadModal from '../../../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../../../common/components/elements/AmountPad'
import Modal from '../../../common/components/widgets/Modal'
import { registerBarcodeReaderEvent } from '../../barcodeReader/services'
import { MAX_ITEM_QUANTITY } from '../models'

export default class StockAdjustmentProductForm extends Component {
  static propTypes = {
    products: PropTypes.array,
    stockAdjustmentReason: PropTypes.object,
    settings: PropTypes.object,
    onProductSelected: PropTypes.func,
    onComplete: PropTypes.func,
    conComplete: PropTypes.bool,
    onSearchForBarcodeReader: PropTypes.func,
    isSearchProduct: PropTypes.bool
  }
  static defaultProps = {
    isSearchProduct: true
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      if (!this.props.isSearchProduct) return

      const result = await this.props.onSearchForBarcodeReader(body.trim())

      if (result && result.length === 1) {
        this._openNumberPad(result[0], this.props.products)
      }
    })
  }

  componentWillUnmount () {
    this.eventBarcodeReader.remove()
  }

  _openNumberPad (product) {
    Modal.close()
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.NUMBER,
      title: product.name,
      maxValue: MAX_ITEM_QUANTITY,
      onComplete: amount => {
        Modal.close()
        this.props.onProductSelected(product, this.props.products, amount)
      }
    })
  }

  render () {
    const {
      onComplete,
      products,
      canComplete,
      stockAdjustmentReason,
      settings
    } = this.props
    return (
      <View>
        <View style={{ padding: 10 }}>
          <View style={styles.categoryProductViewContainer}>
            <CategoryProductView
              onProductSelected={product => this._openNumberPad(product)}
              onShowAmountPad={product => this._openNumberPad(product)}
              style={{ flex: 1 }}
              showStockConditionFlg={false}
            />
          </View>
          <CommandButton
            onPress={() => onComplete(products, stockAdjustmentReason, settings, this.props.stockAdjustmentState)}
            style={styles.completeButton}
            disabled={!canComplete}
            text={I18n.t('stock_adjustment.complete')}
          />
        </View>
      </View>
    )
  }
}
