'use strict'
import React, { Component } from 'react'
import { Text, View, TouchableHighlight, ListView } from 'react-native'
import PropTypes from 'prop-types'
import I18n from 'i18n-js'
import componentStyles from '../styles/StockAdjustmentSearchList'
import { formatDateTime } from 'common/utils/dateTime'

export default class StockAdjustmentSearchList extends Component {
  static propTypes = {
    stockAdjustments: PropTypes.array,
    onSelect: PropTypes.func
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    const { stockAdjustments, onSelect } = this.props
    return (
      <View style={componentStyles.root}>
        <View style={componentStyles.headerTextContainer}>
          <Text style={componentStyles.headerText}>
            {I18n.t('stock_adjustment.search_result')}
          </Text>
        </View>
        <View>
          <ListView
            enableEmptySections
            style={componentStyles.listContainer}
            dataSource={dataSource.cloneWithRows(stockAdjustments)}
            renderRow={stockAdjustment => (
              <TouchableHighlight
                underlayColor={'#bbb'}
                onPress={() => onSelect(stockAdjustment)}>
                <View style={componentStyles.listItemContainer}>
                  <View style={componentStyles.createdAtContainer}>
                    <Text style={componentStyles.createdAtText}>
                      {formatDateTime(
                        Math.floor(stockAdjustment.created_at * 1000)
                      )}
                    </Text>
                  </View>
                  <View style={componentStyles.reasonContainer}>
                    <Text numberOfLines={1} style={componentStyles.reasonText}>
                      {stockAdjustment.stock_adjustment_reason_name}
                    </Text>
                  </View>
                  <View style={componentStyles.staffContainer}>
                    <Text numberOfLines={1} style={componentStyles.staffText}>
                      {stockAdjustment.staff_name}
                    </Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
          />
        </View>
      </View>
    )
  }
}
