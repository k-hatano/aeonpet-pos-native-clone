import generateUuid from 'common/utils/generateUuid'

export const stockAdjustmentReasons = [
  {
    id: generateUuid(),
    name: '理由１',
    stock_adjustment_reason_code: '001',
    sort_order: 1,
    created_at: 1,
    updated_at: 1,
    deleted_at: null
  },
  {
    id: generateUuid(),
    name: '理由２',
    stock_adjustment_reason_code: '002',
    sort_order: 1,
    created_at: 1,
    updated_at: 1,
    deleted_at: null
  },
  {
    id: generateUuid(),
    name: '理由３',
    stock_adjustment_reason_code: '003',
    sort_order: 1,
    created_at: 1,
    updated_at: 1,
    deleted_at: null
  }
]

export default class SampleStockAdjustmentRepository {
  async findAll () {
    return stockAdjustmentReasons
  }
}
