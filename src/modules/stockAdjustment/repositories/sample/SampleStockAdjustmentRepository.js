import generateUuid from 'common/utils/generateUuid'
import { stockAdjustmentReasons } from './SampleStockAdjustmentReasonRepository'
import { sampleProductMap } from 'modules/product/samples'

export const stockAdjustmentItems = {
  standardA: {
    product_id: sampleProductMap.standardA.id,
    product_name: sampleProductMap.standardA.name,
    product_variant_name: '',
    product_code: sampleProductMap.standardA.product_code,
    sku: sampleProductMap.standardA.product_variants[0].sku,
    article_number: sampleProductMap.standardA.article_number,
    stock_item_id: sampleProductMap.standardA.product_variants[0].stock_item.id,
    quantity: 100
  },
  standardB: {
    product_id: sampleProductMap.standardB.id,
    product_name: sampleProductMap.standardB.name,
    product_variant_name: '',
    product_code: sampleProductMap.standardB.product_code,
    sku: sampleProductMap.standardB.product_variants[0].sku,
    article_number: sampleProductMap.standardB.article_number,
    stock_item_id: sampleProductMap.standardB.product_variants[0].stock_item.id,
    quantity: 100
  },
  standardC: {
    product_id: sampleProductMap.standardC.id,
    product_name: sampleProductMap.standardC.name,
    product_variant_name: '',
    product_code: sampleProductMap.standardC.product_code,
    sku: sampleProductMap.standardC.product_variants[0].sku,
    article_number: sampleProductMap.standardC.article_number,
    stock_item_id: sampleProductMap.standardC.product_variants[0].stock_item.id,
    quantity: 100
  },
  setA: {
    product_id: sampleProductMap.setA.id,
    product_name: sampleProductMap.setA.name,
    product_variant_name: '',
    product_code: sampleProductMap.setA.product_code,
    sku: sampleProductMap.setA.product_variants[0].sku,
    article_number: sampleProductMap.setA.article_number,
    stock_item_id: sampleProductMap.setA.product_variants[0].stock_item.id,
    quantity: 100
  },
  bundle_s_1_1: {
    product_id: sampleProductMap.bundle_s_1_1.id,
    product_name: sampleProductMap.bundle_s_1_1.name,
    product_variant_name: '',
    product_code: sampleProductMap.bundle_s_1_1.product_code,
    sku: sampleProductMap.bundle_s_1_1.product_variants[0].sku,
    article_number: sampleProductMap.bundle_s_1_1.article_number,
    stock_item_id:
      sampleProductMap.bundle_s_1_1.product_variants[0].stock_item.id,
    quantity: 100
  },
  bundle_s_1_2: {
    product_id: sampleProductMap.bundle_s_1_2.id,
    product_name: sampleProductMap.bundle_s_1_2.name,
    product_variant_name: '',
    product_code: sampleProductMap.bundle_s_1_2.product_code,
    sku: sampleProductMap.bundle_s_1_2.product_variants[0].sku,
    article_number: sampleProductMap.bundle_s_1_2.article_number,
    stock_item_id:
      sampleProductMap.bundle_s_1_2.product_variants[0].stock_item.id,
    quantity: 100
  }
}

export const stockAdjustments = [
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[0].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[0].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[0].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.standardA,
      stockAdjustmentItems.standardB
    ]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[0].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[0].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[0].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [stockAdjustmentItems.standardC]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[1].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[1].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[1].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.standardC,
      stockAdjustmentItems.setA
    ]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[1].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[1].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[1].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.standardA,
      stockAdjustmentItems.standardB,
      stockAdjustmentItems.standardC
    ]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[1].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[1].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[1].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.standardC,
      stockAdjustmentItems.bundle_s_1_1
    ]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[1].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[1].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[1].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.bundle_s_1_1,
      stockAdjustmentItems.bundle_s_1_2
    ]
  },
  {
    id: generateUuid(),
    stock_transaction_id: generateUuid(),
    stock_adjustment_reason_id: stockAdjustmentReasons[2].id,
    stock_adjustment_reason_name: stockAdjustmentReasons[2].name,
    stock_adjustment_reason_code: stockAdjustmentReasons[2].stock_adjustment_reason_code,
    device_id: generateUuid(),
    staff_id: generateUuid(),
    staff_name: 'TEST1',
    created_at: 1509091159000,
    updated_at: 1509097159000,
    stock_adjustment_items: [
      stockAdjustmentItems.standardA,
      stockAdjustmentItems.standardB,
      stockAdjustmentItems.standardC,
      stockAdjustmentItems.setA,
      stockAdjustmentItems.bundle_s_1_1,
      stockAdjustmentItems.bundle_s_1_2
    ]
  }
]

export default class SampleStockAdjustmentRepository {
  async push (stockAdjustment) {
    stockAdjustments.push(stockAdjustment)
  }

  async cancel (stockAdjustment) {
    let match = stockAdjustments.find(o => o.id === stockAdjustment.id)
    match.canceled_at = stockAdjustment.canceled_at
    match.canceled_device_id = stockAdjustment.canceled_device_id
    match.canceled_staff_id = stockAdjustment.canceled_staff_id
    match.canceled_staff_name = stockAdjustment.canceled_staff_name
  }

  async fetch (params) {
    if (params.stock_adjustment_reason_id) {
      return stockAdjustments.filter(o => o.stock_adjustment_reason_id === params.stock_adjustment_reason_id)
    }
    return stockAdjustments
  }
}
