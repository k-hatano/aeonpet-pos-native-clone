export const stocks = [
  {
    id: 1,
    jan_code: 123456789,
    goods_name: 'First Product',
    master_selling_price: '10000 yen',
    goods_issue: '9999 points',
    closure_type: 'cork',
    label_version: '2017-01',
    box_version: '2017-02',
    vintage: '2017'
  },
  {
    id: 2,
    jan_code: 234567891,
    goods_name: 'Second Product',
    master_selling_price: '12000 yen',
    goods_issue: '8888 points',
    closure_type: 'metal',
    label_version: '2016-01',
    box_version: '2016-02',
    vintage: '2016'
  },
  {
    id: 3,
    jan_code: 345678912,
    goods_name: 'Third Product',
    master_selling_price: '10000 yen',
    goods_issue: '7777 points',
    closure_type: 'rubber',
    label_version: '2015-01',
    box_version: '2015-02',
    vintage: '2015'
  },
  {
    id: 4,
    jan_code: 456789123,
    goods_name: 'Fourth Product',
    master_selling_price: '10000 yen',
    goods_issue: '6666 points',
    closure_type: 'cork',
    label_version: '2014-01',
    box_version: '2014-02',
    vintage: '2014',
  },
  {
    id: 5,
    jan_code: 567891234,
    goods_name: 'Fifth Product',
    master_selling_price: '10000 yen',
    goods_issue: '5555 points',
    closure_type: 'metal',
    label_version: '2013-01',
    box_version: '2013-02',
    vintage: '2013'
  }
]


export const otherStores = [
  {
    id: 1,
    shop_name: 'Shop 1',
    goods_issue: '9999 points',
  }, {
    id: 2,
    shop_name: 'Shop 2',
    goods_issue: '9999 points',
  },
  {
    id: 3,
    shop_name: 'Shop 3',
    goods_issue: '9999 points',
  }, {
    id: 4,
    shop_name: 'Shop 4',
    goods_issue: '9999 points',
  }, {
    id: 5,
    shop_name: 'Shop 5',
    goods_issue: '9999 points',
  },
  {
    id: 6,
    shop_name: 'Shop 6',
    goods_issue: '9999 points',
  },
  {
    id: 7,
    shop_name: 'Shop 7',
    goods_issue: '9999 points',
  },
  {
    id: 8,
    shop_name: 'Shop 8',
    goods_issue: '9999 points',
  }, {
    id: 9,
    shop_name: 'Shop 9',
    goods_issue: '9999 points',
  }, {
    id: 10,
    shop_name: 'Shop 10',
    goods_issue: '9999 points',
  }

]


export const selectedStock = [{
  id: 1,
  jan_code: 123456789,
  goods_name: 'test1',
  master_selling_price: '10000 yen',
  goods_issue: '9999 points',
  closure_type: 'cork',
  label_version: '2017-01',
  box_version: '2017-02',
  vintage: '2017'
}]


export const stockAdjustments = [
  {
    id: 1,
    goods_name: 'First Product',
    jan_code: '123456789',
    adjustment_reason: 'Damage',
    created_at: '2017-09-13 10:12:24',
    incharge: 'carpenter'
  },
  {
    id: 2,
    goods_name: 'Second Product',
    jan_code: '234567891',
    adjustment_reason: 'Test 1',
    created_at: '2017-10-14 11:12:24',
    incharge: 'carpenter'
  },
  {
    id: 3,
    goods_name: 'Third Product',
    jan_code: '345678912',
    adjustment_reason: 'Test 2',
    created_at: '2017-03-09 19:12:24',
    incharge: 'carpenter'
  },
  {
    id: 4,
    goods_name: 'Fourth Product',
    jan_code: '456789123',
    adjustment_reason: 'Damage',
    created_at: '2017-09-10 6:12:35',
    incharge: 'carpenter'
  },
  {
    id: 5,
    goods_name: 'Fifth Product',
    jan_code: '567891234',
    adjustment_reason: 'Test 1',
    created_at: '2017-12-05 6:12:24',
    incharge: 'carpenter'
  },
  {
    id: 1,
    goods_name: 'First Product',
    jan_code: '123456789',
    adjustment_reason: 'Test 1',
    created_at: '2017-02-05 6:12:24',
    incharge: 'carpenter'
  },

]

export default class SampleStockRepository {

  async findAll() {
    return stocks
  }


  async findAllOtherStore() {

    return otherStores
  }


  async fetchByConditions(conditions) {
    return stocks
  }

  async fetchByStockCommodity(stock) {
    return selectedStock
  }

  async findStockById(stockId) {

    return stocks.filter(stock => (((stock.id).toString().toLowerCase() == stockId.toString().toLowerCase())))

  }

  async findStockAdjustmentById(stockId) {

    return stockAdjustments.filter(stock => (((stock.id).toString().toLowerCase() == stockId.toString().toLowerCase())))

  }


  async findStockByKeyword(searchKey) {
    return stocks.filter(stock => (((stock.jan_code).toString().toLowerCase() == searchKey.toString().toLowerCase()) || ((stock.goods_name).toString().toLowerCase() == searchKey.toString().toLowerCase())))
  }


  async filterByCategories(stocksitems, textBoxValue, closure, label, box, vintage) {

    var stocksResult = []

    if ((textBoxValue == "") && (closure == "") && (label == "") && (box == "") && (vintage == "")) {
      stocksResult = stocks
    }


    if ((textBoxValue !== "") && (closure == "") && (label == "") && (box == "") && (vintage == "")) {

      stocksResult = stocksitems.filter(stock => (((stock.goods_name) == textBoxValue) || ((stock.jan_code) == textBoxValue)))
    }
    if ((textBoxValue !== "") && (closure !== "") && (label == "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)))
    }


    if ((textBoxValue !== "") && (closure == "") && (label !== "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.label_version) == label)))
    }

    if ((textBoxValue !== "") && (closure == "") && (label == "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.box_version) == box)))
    }

    if ((textBoxValue !== "") && (closure == "") && (label == "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.vintage) == vintage)))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label !== "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.label_version) == label))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label == "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.box_version) == box))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label == "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue !== "") && (closure == "") && (label !== "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.label_version) == label_version)) && ((stock.box_version) == box))
    }

    if ((textBoxValue !== "") && (closure == "") && (label !== "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.label_version) == label_version)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue !== "") && (closure == "") && (label == "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.box_version) == box_version)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue !== "") && (closure == "") && (label !== "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.vintage) == vintage)) && ((stock.label_version) == label) && ((stock.box_version) == box))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label == "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.box_version) == box) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label !== "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.label_version) == labelox) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue !== "") && (closure !== "") && (label !== "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.label_version) == label) && ((stock.box_version) == box))
    }



    if ((textBoxValue !== "") && (closure !== "") && (label !== "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => ((((stock.goods_name) == textBoxValue || (stock.jan_code) == textBoxValue)) && ((stock.closure_type) == closure)) && ((stock.label_version) == label) && ((stock.box_version) == box) && ((stock.vintage) == vintage))
    }



    if ((textBoxValue == "") && (closure !== "") && (label == "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure)))
    }
    if ((textBoxValue == "") && (closure == "") && (label !== "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.label_version) == label)))
    }
    if ((textBoxValue == "") && (closure == "") && (label == "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.box_version) == box)))
    }
    if ((textBoxValue == "") && (closure == "") && (label == "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.vintage) == vintage)))
    }

    if ((textBoxValue == "") && (closure !== "") && (label !== "") && (box == "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure)) && ((stock.label_version) == label))
    }

    if ((textBoxValue == "") && (closure !== "") && (label == "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure)) && ((stock.box_version) == box))
    }

    if ((textBoxValue == "") && (closure !== "") && (label == "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure == "") && (label !== "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.label_version) == label)) && ((stock.box_version) == box))
    }

    if ((textBoxValue == "") && (closure == "") && (label !== "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.label_version) == label)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure == "") && (label == "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.box_version) == box)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure !== "") && (label !== "") && (box !== "") && (vintage == "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure) && ((stock.label_version) == label)) && ((stock.box_version) == box))
    }

    if ((textBoxValue == "") && (closure !== "") && (label == "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure) && ((stock.box_version) == box)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure == "") && (label !== "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.label_version) == label) && ((stock.box_version) == box)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure !== "") && (label !== "") && (box == "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.label_version) == label) && ((stock.closure_type) == closure)) && ((stock.vintage) == vintage))
    }

    if ((textBoxValue == "") && (closure !== "") && (label !== "") && (box !== "") && (vintage !== "")) {
      stocksResult = stocksitems.filter(stock => (((stock.closure_type) == closure)) && ((stock.label_version) == label) && ((stock.box_version) == box) && ((stock.vintage) == vintage))
    }

    return stocksResult
  }


  getStockResults(adjustmentReason, product, adjustmentFromdate, adjustmentToDate) {

    var stocksSearchResult = []

    //1. Given values all are null value
    if ((adjustmentReason == "") && (product == "") && (adjustmentFromdate == "") && (adjustmentToDate == "")) {
      return stocksSearchResult;
    }
    //2. Reason is not null value 
    else if ((adjustmentReason != "") && (product == "") && (adjustmentFromdate == "") && (adjustmentToDate == "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason)
        )
    }
    //3. Reason and product are not null value 
    else if ((adjustmentReason != "") && (product != "") && (adjustmentFromdate == "") && (adjustmentToDate == "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason) && (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
        )

    }
    //4. Reason,product and fromdate are not null value 
    else if ((adjustmentReason != "") && (product != "") && (adjustmentFromdate != "") && (adjustmentToDate == "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason) && (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
          && (stocks.created_at >= adjustmentFromdate)
        )
    }
    //5. Given valures are not null value 
    else if ((adjustmentReason != "") && (product != "") && (adjustmentFromdate != "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason) && (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
          && (stocks.created_at >= adjustmentFromdate && stocks.created_at <= adjustmentTodate)
        )
    }
    //6.Reason and Fromdate are not null value 
    else if ((adjustmentReason != "") && (product == "") && (adjustmentFromdate != "") && (adjustmentToDate = "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason) && (stocks.created_at >= adjustmentFromdate)
        )
    }
    //7.Reason and Todate are not null value 
    else if ((adjustmentReason != "") && (product == "") && (adjustmentFromdate == "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.adjustment_reason == adjustmentReason) && (stocks.created_at <= adjustmentToDate)
        )
    }
    //8.Product is not null value
    else if ((adjustmentReason == "") && (product != "") && (adjustmentFromdate == "") && (adjustmentToDate == "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
        )
    }
    //9. Fromdate is not null value 
    else if ((adjustmentReason == "") && (product == "") && (adjustmentFromdate != "") && (adjustmentToDate == "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.created_at >= adjustmentFromdate)
        )
    }
    //10. Todate is not null value 
    else if ((adjustmentReason == "") && (product == "") && (adjustmentFromdate == "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.created_at <= adjustmentTodate)
        )
    }
    //11. Product and Fromdate are not null value 
    else if ((adjustmentReason == "") && (product != "") && (adjustmentFromdate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
          && (stocks.created_at >= adjustmentFromdate)
        )
    }
    //12. Product,Fromdate and Todate are not null value 
    else if ((adjustmentReason == "") && (product != "") && (adjustmentFromdate != "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
          && (stocks.created_at >= adjustmentFromdate && stocks.created_at <= adjustmentTodate)
        )
    }
    //13. Product and Todate are not null value 
    else if ((adjustmentReason == "") && (product != "") && (adjustmentFromdate == "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (((stocks.goods_name) == product) || ((stocks.jan_code) == product))
          && (stocks.created_at <= adjustmentTodate)
        )
    }
    //14. Fromdate and Todate are not null value 
    else if ((adjustmentReason == "") && (product != "") && (adjustmentFromdate != "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => (stocks.created_at >= adjustmentFromdate && stocks.created_at <= adjustmentTodate)
        )
    }
    //15. Reason,Fromdate and Todate values are not null value 
    else if ((adjustmentReason != "") && (product == "") && (adjustmentFromdate != "") && (adjustmentToDate != "")) {
      stocksSearchResult = stockAdjustments.filter
        (
        stocks => ((stocks.adjustment_reason == adjustmentReason) && ((stocks.created_at >= adjustmentFromdate) && (stocks.created_at <= adjustmentToDate)))
        )
    }
    // alert(JSON.stringify(stocksSearchResult))
    return stocksSearchResult;


  }
}

