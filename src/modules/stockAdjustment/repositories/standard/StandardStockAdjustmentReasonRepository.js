import StockAdjustmentReasonEntity from '../../../stock/repositories/entities/StockAdjustmentReasonEntity'

export default class StandardStockAdjustmentReasonRepository {
  async findAll() {
    return await StockAdjustmentReasonEntity.findAll({ raw: true })
  }
}
