import I18n from 'i18n-js'
import fetcher from '../../../../common/models/fetcher'
import { handleAxiosError, REQUEST_ERROR_CODE } from '../../../../common/errors'
import { encodeCriteria } from '../../../../common/utils/searchUtils'
import MessageError from '../../../../common/errors/MessageError'
import { MAX_ITEM_QUANTITY } from '../../models'

export default class StandardStockAdjustmentRepository {
  async push (stockAdjustment) {
    try {
      await fetcher.post('stock-adjustments', stockAdjustment)
    } catch (error) {
      this.handleStockAdjustError(error)
    }
  }

  async cancel (stockAdjustment) {
    try {
      await fetcher.post('stock-adjustments/' + stockAdjustment.id + '/cancel', stockAdjustment)
    } catch (error) {
      this.handleStockAdjustError(error, true)
    }
  }

  async fetch (parameter) {
    try {
      const response = await fetcher.get('stock-adjustments', encodeCriteria(parameter))
      return response.data
    } catch (error) {
      this.handleStockAdjustError(error)
    }
  }

  /**
   * 在庫調整固有のエラーハンドリングとアプリ共通のエラーハンドリングを行います。
   * @param error
   */
  handleStockAdjustError (error, cancel = false) {
    const {response} = error

    if (response && response.status === 404) {
      // 存在しません。
      throw new MessageError('message.M-02-E001')
    }
    if (response && response.status === 400 && response.data) {
      const action = cancel
        ? I18n.t('stock_adjustment.action_cancel_stock_adjustment') : I18n.t('stock_adjustment.action_stock_adjustment')
      switch (response.data.code) {
        case REQUEST_ERROR_CODE.STOCK_ADJUST_BAD_REASON:
          // 不正な調整理由が使用されている。
          throw new MessageError('message.M-01-E003')
        case REQUEST_ERROR_CODE.STOCK_ADJUST_BAD_STATUS:
          // すでに取り消されている。
          throw new MessageError('message.M-02-E002')
        case REQUEST_ERROR_CODE.STOCK_ADJUST_PRODUCT_NOT_FOUND:
          // 商品が存在しない。
          throw new MessageError('message.M-00-E001', {action: action})
        case REQUEST_ERROR_CODE.STOCK_ADJUST_PRODUCT_STOCK_MODE:
          // 在庫管理されていない商品
          throw new MessageError('message.M-00-E002', {action: action})
        case REQUEST_ERROR_CODE.STOCK_TRANSACTION_OVER_MAX_QUANTITY:
          // 在庫数が最大数を上回る場合
          throw new MessageError('message.M-00-E004', {action: action, quantity: MAX_ITEM_QUANTITY})
        case REQUEST_ERROR_CODE.STOCK_TRANSACTION_UNDER_MIN_QUANTITY:
          // 在庫数が最小数を下回る場合
          throw new MessageError('message.M-00-E003', {action: action, quantity: MAX_ITEM_QUANTITY})
      }
    }
    handleAxiosError(error)
  }
}
