import SampleStockAdjustmentRepository from './sample/SampleStockAdjustmentRepository'
import StandardStockAdjustmentRepository from './standard/StandardStockAdjustmentRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockAdjustmentRepository {
  static _implement = new SampleStockAdjustmentRepository()

  static async push (stockAdjustment) {
    return this._implement.push(stockAdjustment)
  }

  static async cancel (stockAdjustment) {
    return this._implement.cancel(stockAdjustment)
  }

  static async fetch (parameter) {
    return this._implement.fetch(parameter)
  }

  static switchImplement (context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockAdjustmentRepository()
        break

      default:
        this._implement = new SampleStockAdjustmentRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged('StockAdjustmentRepository', context => {
  StockAdjustmentRepository.switchImplement(context)
})
