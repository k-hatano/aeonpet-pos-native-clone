import SampleStockRepository from './sample/SampleStockRepository'
import StandardStockRepository from './standard/StandardStockRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockRepository {
  static _implement = new SampleStockRepository()

  static async fetchByConditions() {
    return this._implement.fetchByConditions()
  }

  static async fetchByStockCommodity(stock) {
    return this._implement.fetchByStockCommodity(stock)
  }

  static async findStockByKeyword(searchKey) {
    return this._implement.findStockByKeyword(searchKey)
  }

  static async findStockById(stockId) {
    return this._implement.findStockById(stockId)
  }

  static async findStockAdjustmentById(stockId) {
    return this._implement.findStockAdjustmentById(stockId)
  }
  static async getStockResults(
    adjustmentReason,
    product,
    adjustmentFromdate,
    adjustmentToDate
  ) {
    return this._implement.getStockResults(
      adjustmentReason,
      product,
      adjustmentFromdate,
      adjustmentToDate
    )
  }

  static async updateMoveItems(stockid) {
    return this._implement.updateMoveItems(stockid)
  }
  static async findAll() {
    return this._implement.findAll()
  }
  static async findAllOtherStore() {
    return this._implement.findAllOtherStore()
  }

  static async filterByCategories(
    stocksitems,
    textBoxValue,
    closure,
    label,
    box,
    vintage
  ) {
    return this._implement.filterByCategories(
      stocksitems,
      textBoxValue,
      closure,
      label,
      box,
      vintage
    )
  }

  static switchImplement(context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockRepository()
        break

      default:
        this._implement = new SampleStockRepository()
        break
    }
  }

  static async createSamplesAsync() {
    return this.implement.createSamplesAsync()
  }
}

AppEvents.onRepositoryContextChanged('StockRepository', context => {
  StockRepository.switchImplement(context)
})
