import SampleStockAdjustmentReasonRepository from './sample/SampleStockAdjustmentReasonRepository'
import StandardStockAdjustmentReasonRepository from './standard/StandardStockAdjustmentReasonRepository'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'

export default class StockAdjustmentReasonRepository {
  static _implement = new SampleStockAdjustmentReasonRepository()

  static async findAll() {
    return this._implement.findAll()
  }

  static switchImplement(context) {
    switch (context) {
      case REPOSITORY_CONTEXT.STANDARD:
        this._implement = new StandardStockAdjustmentReasonRepository()
        break

      default:
        this._implement = new SampleStockAdjustmentReasonRepository()
        break
    }
  }
}

AppEvents.onRepositoryContextChanged(
  'StockAdjustmentReasonRepository',
  context => {
    StockAdjustmentReasonRepository.switchImplement(context)
  }
)
