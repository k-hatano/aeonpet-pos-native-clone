import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import StockAdjustmentProductView from '../components/StockAdjustmentProductView'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { getSettingFromState } from 'modules/setting/models'

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY),
  stockAdjustment: state[MODULE_NAME].currentStockAdjustment
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentProductView
)
