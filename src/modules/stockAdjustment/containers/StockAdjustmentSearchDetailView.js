import { connect } from 'react-redux'
import { makeSettingsForStockAdjustment, makeStockAdjustmentCancel, MODULE_NAME } from '../models'
import StockAdjustmentSearchDetailView from '../components/StockAdjustmentSearchDetailView'
import StockAdjustmentRepository from '../repositories/StockAdjustmentRepository'
import * as actions from '../actions'
import I18n from 'i18n-js'
import ConfirmView from 'common/components/widgets/ConfirmView'
import AlertView from 'common/components/widgets/AlertView'
import { loading } from 'common/sideEffects'
import { searchStockAdjustments, syncStockItems } from '../services'
import { getCommonNetworkErrorMessage } from '../../../common/errors'

const mapDispatchToProps = dispatch => ({
  onBack: async () => {
    dispatch(actions.setCurrentStockAdjustment({}))
  },
  onCancel: async (stockAdjustment, settings, stockAdjustmentState) => {
    ConfirmView.show(I18n.t('message.M-01-I003'), async () => {
      const params = await makeStockAdjustmentCancel(stockAdjustment, settings)
      const result = await loading(dispatch, async () => {
        try {
          await StockAdjustmentRepository.cancel(params)
          return {isSuccess: true}
        } catch (error) {
          return {
            isSuccess: false,
            message: getCommonNetworkErrorMessage(error)
          }
        }
      })
      if (!result.isSuccess) {
        await AlertView.showAsync(result.message)
        // 検索画面に戻って、再検索
        dispatch(actions.setCurrentStockAdjustment({}))
        await searchStockAdjustments(dispatch, stockAdjustmentState)
      } else {
        await AlertView.showAsync(I18n.t('message.M-01-I004'))
        // 検索画面に戻って、再検索
        // その後にローカルの在庫と同期
        dispatch(actions.setCurrentStockAdjustment({}))
        await searchStockAdjustments(dispatch, stockAdjustmentState)
        await syncStockItems(dispatch, stockAdjustment.stockAdjustmentItems)
      }
    })
  }
})

const mapStateToProps = state => ({
  settings: makeSettingsForStockAdjustment(state),
  stockAdjustment: state[MODULE_NAME].currentStockAdjustment,
  stockAdjustmentState: state[MODULE_NAME]
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentSearchDetailView
)
