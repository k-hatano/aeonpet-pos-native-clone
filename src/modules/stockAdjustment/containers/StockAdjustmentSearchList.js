import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import * as actions from '../actions'
import StockAdjustmentSearchList from '../components/StockAdjustmentSearchList'

const mapDispatchToProps = dispatch => ({
  onSelect: stockAdjustment => {
    dispatch(actions.setCurrentStockAdjustment(stockAdjustment))
  }
})

const mapStateToProps = state => ({
  stockAdjustments: state[MODULE_NAME].stockAdjustments
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentSearchList
)
