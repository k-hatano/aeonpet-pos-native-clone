import { connect } from 'react-redux'
import { MODULE_NAME } from '../models'
import StockAdjustmentDetailView from '../components/StockAdjustmentDetailView'

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  stockAdjustment: state[MODULE_NAME].currentStockAdjustment
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentDetailView
)
