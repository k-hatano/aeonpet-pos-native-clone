import I18n from 'i18n-js'
import { connect } from 'react-redux'
import { MODULE_NAME, MAX_ITEM_QUANTITY } from '../models'
import * as actions from '../actions'
import StockAdjustmentList from '../components/StockAdjustmentList'
import SettingKeys from '../../../modules/setting/models/SettingKeys'
import { getSettingFromState } from '../../../modules/setting/models'

import AlertView from '../../../common/components/widgets/AlertView'

const mapDispatchToProps = dispatch => ({
  onChangeQuantity: (id, quantity, products) => {
    // 入力数が上限値を超えている場合は増加させない。
    if (quantity > MAX_ITEM_QUANTITY) return

    const productIndex = products.findIndex(product => product.id === id)

    if (productIndex >= 0) {
      const stockItem = products[productIndex].product_variants[0].stock_item
      const stockQuantity = (stockItem && stockItem.quantity) || 0

      if (Math.abs(stockQuantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.M-01-E001', {quantity: MAX_ITEM_QUANTITY}))
        return false
      } else {
        if (quantity > 0) {
          products[productIndex].quantity = quantity
        } else {
          products.splice(productIndex, 1)
        }
        dispatch(actions.setProducts(Object.assign([], products)))
      }
    }
  },
  onSelectStockAdjustmentReason: async (stockAdjustmentReason) => {
    dispatch(actions.setStockAdjustmentReason(stockAdjustmentReason))
  },
  onClear: async () => {
    dispatch(actions.setStockAdjustmentReason(null))
    dispatch(actions.setProducts([]))
  }
})

const mapStateToProps = state => ({
  products: state[MODULE_NAME].products,
  currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY),
  stockAdjustmentReasons: state[MODULE_NAME].stockAdjustmentReasons,
  stockAdjustmentReason: state[MODULE_NAME].stockAdjustmentReason
})

export default connect(mapStateToProps, mapDispatchToProps)(StockAdjustmentList)
