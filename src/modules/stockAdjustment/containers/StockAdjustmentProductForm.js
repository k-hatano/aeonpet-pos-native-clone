import I18n from 'i18n-js'
import { connect } from 'react-redux'
import * as actions from '../actions'
import * as productActions from '../../../modules/product/actions'
import StockAdjustmentProductForm from '../components/StockAdjustmentProductForm'
import { Actions } from 'react-native-router-flux'
import StockAdjustmentRepository from '../repositories/StockAdjustmentRepository'
import {
  MODULE_NAME,
  MAX_ITEM_QUANTITY,
  canCompleteStockAdjustment,
  makeSettingsForStockAdjustment,
  makeStockAdjustment
} from '../models'
import AlertView from '../../../common/components/widgets/AlertView'
import { PREV_SEARCH_COND } from '../../../modules/product/models'
import { loading } from '../../../common/sideEffects'
import ProductRepository from '../../product/repositories/ProductRepository'
import ConfirmView from '../../../common/components/widgets/ConfirmView'
import { syncStockItems } from '../services'
import { getCommonNetworkErrorMessage } from '../../../common/errors'

const mapDispatchToProps = dispatch => ({
  onSearchForBarcodeReader: async (barcode) => {
    const products = await loading(dispatch, async () => {
      let products = await ProductRepository.findProductsBySearchWordForStocks(barcode, true)
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      switch (products.length) {
        case 0:
          let productVariantsByWord = await ProductRepository.findVariantsBySearchWordForStocks(barcode)
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              const productDetail = await ProductRepository.findWithDetail(productVariantsByWord[0].id)
              products.push(productDetail)
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              dispatch(productActions.listProducts(productVariantsByWord))
              dispatch(productActions.listOriginProducts(productVariantsByWord))
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
              dispatch(productActions.setSearchLayoutFlg(true))
              break
          }
          break
        case 1:
          const productVariantsById = await ProductRepository.findVariantsByProductId(products[0].id)
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              break
          }
          break
        default:
          dispatch(productActions.listProducts(products))
          dispatch(productActions.listOriginProducts(products))
          dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
          dispatch(productActions.setSearchLayoutFlg(true))
          break
      }
      return products
    })
    return products
  },
  onProductSelected: async (product, products, quantity = 1) => {
    // 数量が0の場合は何も選択されなかったと判断する。
    if (quantity === 0) return

    const productIndex = products.findIndex(item => item.id === product.id)
    const stockItem = product.product_variants[0].stock_item
    const stockQuantity = (stockItem && stockItem.quantity) || 0

    if (productIndex < 0) {
      if (Math.abs(stockQuantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.M-01-E001', {quantity: MAX_ITEM_QUANTITY}))
      } else {
        product.quantity = quantity
        dispatch(actions.setProducts(Object.assign([], [...products, product])))
      }
    } else {
      if (products[productIndex].quantity + quantity > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.M-01-E004', {quantity: MAX_ITEM_QUANTITY}))
      } else if (Math.abs(stockQuantity - products[productIndex].quantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.M-01-E001', {quantity: MAX_ITEM_QUANTITY}))
      } else {
        products[productIndex].quantity += quantity
        dispatch(actions.setProducts(Object.assign([], products)))
      }
    }
  },
  onComplete: async (products, stockAdjustmentReason, settings) => {
    ConfirmView.show(I18n.t('message.M-01-I001'), async () => {
      const result = await loading(dispatch, async () => {
        const stockAdjustment = await makeStockAdjustment(products, stockAdjustmentReason, settings)
        try {
          await StockAdjustmentRepository.push(stockAdjustment)
          await syncStockItems(dispatch, stockAdjustment.stock_adjustment_items)
          dispatch(actions.setProducts([]))
          dispatch(actions.setStockAdjustmentReason(null))
          return {isSuccess: true}
        } catch (error) {
          return {
            isSuccess: false,
            message: getCommonNetworkErrorMessage(error)
          }
        }
      })
      if (!result.isSuccess) {
        AlertView.show(result.message)
      } else {
        await AlertView.showAsync(I18n.t('message.M-01-I005'))

        Actions.stockAdjustmentSearch()
      }
    })
  }
})

const mapStateToProps = state => ({
  products: state[MODULE_NAME].products,
  stockAdjustmentReason: state[MODULE_NAME].stockAdjustmentReason,
  settings: makeSettingsForStockAdjustment(state),
  canComplete: canCompleteStockAdjustment(state[MODULE_NAME]),
  stockAdjustmentState: state[MODULE_NAME]
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentProductForm
)
