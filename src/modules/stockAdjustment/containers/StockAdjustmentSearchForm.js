import { connect } from 'react-redux'
import {
  MODULE_NAME,
  clearStateForStockAdjustmentSearch
} from '../models'
import * as actions from '../actions'
import { Actions } from 'react-native-router-flux'
import StockAdjustmentSearchForm from '../components/StockAdjustmentSearchForm'
import { searchStockAdjustments } from '../services'

const mapDispatchToProps = dispatch => ({
  onSearch: async (state, barcode) => {
    await searchStockAdjustments(dispatch, state, barcode)
  },
  onCreate: async () => {
    Actions.stockAdjustment()
  },
  onClear: async () => {
    await clearStateForStockAdjustmentSearch(dispatch)
  },
  onItemChange: (item) => {
    dispatch(actions.setSearchStockAdjustmentReason(item))
  },
  onChangeText: (text) => {
    dispatch(actions.setSearchWord(text))
  },
  onSetFromDate: (date) => {
    dispatch(actions.setSearchDateFrom(date))
  },
  onSetToDate: (date) => {
    dispatch(actions.setSearchDateTo(date))
  }
})

const mapStateToProps = state => ({
  stockAdjustmentState: state[MODULE_NAME],
  stockAdjustmentReasons: state[MODULE_NAME].stockAdjustmentReasons,
  stockAdjustmentReason: state[MODULE_NAME].stockAdjustmentReason,
  searchStockAdjustmentReason: state[MODULE_NAME].searchStockAdjustmentReason,
  searchWord: state[MODULE_NAME].searchWord,
  searchDateFrom: state[MODULE_NAME].searchDateFrom,
  searchDateTo: state[MODULE_NAME].searchDateTo
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockAdjustmentSearchForm
)
