import { handleActions } from 'redux-actions'
import {
  setProducts,
  listStockAdjustmentReasons,
  setStockAdjustmentReason,
  setStockAdjustments,
  setCurrentStockAdjustment,
  initStockAdjustmentSearch,
  setSearchStockAdjustmentReason,
  setSearchWord,
  setSearchDateFrom,
  setSearchDateTo
} from './actions'
import { getToday, getEndOfToday } from '../../common/utils/dateTime'

const defaultState = {
  products: [],
  stockAdjustmentReasons: [],
  stockAdjustmentReason: null,
  stockAdjustments: [],
  currentStockAdjustment: {}
}

const handlers = {
  [setProducts]: (state, action) => ({
    ...state,
    products: action.payload
  }),
  [listStockAdjustmentReasons]: (state, action) => ({
    ...state,
    stockAdjustmentReasons: action.payload
  }),
  [setStockAdjustmentReason]: (state, action) => ({
    ...state,
    stockAdjustmentReason: action.payload
  }),
  [setStockAdjustments]: (state, action) => ({
    ...state,
    stockAdjustments: action.payload
  }),
  [setCurrentStockAdjustment]: (state, action) => ({
    ...state,
    currentStockAdjustment: action.payload
  }),
  [initStockAdjustmentSearch]: (state, action) => ({
    ...state,
    searchStockAdjustmentReason: null,
    searchWord: '',
    searchDateFrom: getToday(),
    searchDateTo: getEndOfToday()
  }),
  [setSearchStockAdjustmentReason]: (state, action) => ({
    ...state,
    searchStockAdjustmentReason: action.payload
  }),
  [setSearchWord]: (state, action) => ({
    ...state,
    searchWord: action.payload
  }),
  [setSearchDateFrom]: (state, action) => ({
    ...state,
    searchDateFrom: action.payload
  }),
  [setSearchDateTo]: (state, action) => ({
    ...state,
    searchDateTo: action.payload
  })
}
export default handleActions(handlers, defaultState)
