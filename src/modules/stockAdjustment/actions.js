import { createAction } from 'redux-actions'
import { MODULE_NAME } from './models'

export const setProducts = createAction(`${MODULE_NAME}_setProducts`)
export const listStockAdjustmentReasons = createAction(`${MODULE_NAME}_listStockAdjustmentReasons`)
export const setStockAdjustmentReason = createAction(`${MODULE_NAME}_setStockAdjustmentReason`)
export const setStockAdjustments = createAction(`${MODULE_NAME}_setStockAdjustments`)
export const setCurrentStockAdjustment = createAction(`${MODULE_NAME}_setCurrentStockAdjustment`)
export const initStockAdjustmentSearch = createAction(`${MODULE_NAME}_initStockAdjustmentSearch`)
export const setSearchStockAdjustmentReason = createAction(`${MODULE_NAME}_setSearchStockAdjustmentReason`)
export const setSearchWord = createAction(`${MODULE_NAME}_setSearchWord`)
export const setSearchDateFrom = createAction(`${MODULE_NAME}_setSearchDateFrom`)
export const setSearchDateTo = createAction(`${MODULE_NAME}_setSearchDateTo`)
