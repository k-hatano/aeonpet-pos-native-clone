import * as actions from './actions'
import StockAdjustmentReasonRepository from './repositories/StockAdjustmentReasonRepository'
import { getSettingFromState } from '../setting/models'
import SettingKeys from '../setting/models/SettingKeys'
import ShopRepository from '../shop/repositories/ShopRepository'

export const MODULE_NAME = 'stockAdjustment'

export const MAX_ITEM_QUANTITY = 99999
export const STOCK_TRANSACTION_TYPE_ADJUSTMENT = 6

export const canCompleteStockAdjustment = state => {
  return state.stockAdjustmentReason && state.products.length > 0
}

export const makeSettingsForStockAdjustment = state => {
  const staff = getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF)

  return {
    shop_id: getSettingFromState(state, SettingKeys.COMMON.SHOP_ID),
    device_id: getSettingFromState(state, SettingKeys.COMMON.DEVICE_ID),
    staff_id: staff.id,
    staff_name: staff.name
  }
}

export const makeStockAdjustment = async (
  products,
  stockAdjustmentReason,
  settings
) => {
  let stockAdjustmentItems = []
  let currentShop = await ShopRepository.find()
  for (var index = 0, max = products.length; index < max; index++) {
    const product = products[index]
    stockAdjustmentItems.push({
      product_id: product.id,
      product_name: product.name,
      product_variant_id: product.product_variants[0].id,
      product_variant_name: '',
      product_code: product.product_code,
      sku: product.product_variants[0].sku,
      article_number: product.product_variants[0].article_number,
      quantity: product.quantity,
      price: product.product_variants[0].price,
      cost_price: product.product_variants[0].cost_price
    })
  }
  return {
    stock_adjustment_items: stockAdjustmentItems,
    stock_adjustment_reason_id: stockAdjustmentReason.id,
    stock_adjustment_reason_name: stockAdjustmentReason.name,
    stock_adjustment_reason_code:
      stockAdjustmentReason.stock_adjustment_reason_code,
    stock_transaction_type: STOCK_TRANSACTION_TYPE_ADJUSTMENT,
    shop_id: settings.shop_id,
    shop_code: currentShop.shop_code,
    device_id: settings.device_id,
    staff_id: settings.staff_id,
    staff_name: settings.staff_name
  }
}

export const makeStockAdjustmentCancel = async (stockAdjustment, settings) => {
  let currentShop = await ShopRepository.find()
  return {
    stock_adjustment_id: stockAdjustment.id,
    shop_id: settings.shop_id,
    shop_code: currentShop.shop_code,
    device_id: settings.device_id,
    staff_id: settings.staff_id,
    staff_name: settings.staff_name
  }
}

export const initForStockAdjustment = async dispatch => {
  dispatch(actions.setStockAdjustmentReason(null))
  dispatch(actions.setProducts([]))
}

export const initForStockAdjustmentSearch = async dispatch => {
  const stockAdjustmentReasons = await StockAdjustmentReasonRepository.findAll()
  dispatch(actions.listStockAdjustmentReasons(stockAdjustmentReasons))
}

export const clearStateForStockAdjustmentSearch = async dispatch => {
  dispatch(actions.initStockAdjustmentSearch())
  dispatch(actions.setCurrentStockAdjustment({}))
  dispatch(actions.setStockAdjustments([]))
}
