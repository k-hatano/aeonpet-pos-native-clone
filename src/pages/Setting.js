import React, { Component } from 'react'
import { Text, StatusBar, NativeModules } from 'react-native'
import { Row, Grid } from 'react-native-easy-grid'
import Icon from 'react-native-vector-icons/Ionicons'
import I18n from 'i18n-js'
import logger from 'common/utils/logger'
import TabPanel from '../common/components/widgets/TabPanel'
import StaffTab from '../modules/setting/components/StaffTab'
import MenuTab from '../modules/setting/components/MenuTab'
import NetworkTab from '../modules/setting/components/NetworkTab'
import LogTab from '../modules/setting/components/LogTab'
import ReceiptTab from '../modules/setting/components/ReceiptTab'
import OrderTab from '../modules/setting/components/OrderTab'
import DeviceTab from '../modules/setting/components/DeviceTab'
import commonStyles from '../common/styles/layout/Page'
import BasePage from '../common/hocs/BasePage'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class Payment extends Component {
  async componentDidMount () {
    await NativeModules.PerformanceBridge.getCurrentResidentSize(result => {
      if (result.length > 0) {
        logger.info('Setting page: Current RAM is ' + result + ' MB')
      } else {
        logger.warn('Failed to get current RAM.')
      }
    })
  }

  _renderTabContent = () => {
    return (
      <TabPanel tabWrapperStyle={{ flexDirection: 'row', backgroundColor: '#ede6e6' }}>
        <NetworkTab
          tabName={
            <Text>
              <Icon name='md-git-network' size={15} /> {I18n.t('settings.network')}
            </Text>
          }
        />
        <StaffTab
          tabName={
            <Text>
              <Icon name='md-person' size={15} /> {I18n.t('settings.staff')}
            </Text>
          }
        />
        <MenuTab
          tabName={
            <Text>
              <Icon name='md-menu' size={15} /> {I18n.t('settings.menu')}
            </Text>
          }
        />
        <OrderTab
          tabName={
            <Text>
              <Icon name='md-cart' size={15} /> {I18n.t('settings.order')}
            </Text>
          }
        />
        <ReceiptTab
          tabName={
            <Text>
              <Icon name='md-list-box' size={15} /> {I18n.t('settings.receipt')}
            </Text>
          }
        />
        <LogTab
          tabName={
            <Text>
              <Icon name='md-paper' size={15} /> {I18n.t('settings.log')}
            </Text>
          }
        />
        <DeviceTab
          tabName={
            <Text>
              <Icon name='md-print' size={15} /> {I18n.t('settings.peripheral_device')}
            </Text>
          }
        />
      </TabPanel>
    )
  }

  render () {
    const isOpening = this.props.isOpening ? true : false
    return (
      <BasePage headerProps={{hideBackButton: isOpening, canBack: isOpening, showPrinterErrorButton: false, pageName: I18n.t("page.settings"), permissionCode: PERMISSION_CODES.SETTING}}>
        <Grid style={commonStyles.basicLayoutRootStyle}>
          <StatusBar hidden />
          <Row style={{ flex: 1 }}>
            {this._renderTabContent()}
          </Row>
        </Grid>
      </BasePage>
    )
  }
}
