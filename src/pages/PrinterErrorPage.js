import React from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import BasePage from 'common/hocs/BasePage'
import PrinterErrorReceiptView from '../modules/printer/containers/PrinterErrorReceiptView'
import PrinterErrorList from '../modules/printer/containers/PrinterErrorList'
import {MODULE_NAME, initForCreatePrintErrors, clearSelected} from 'modules/printer/models'

class PrinterErrorPage extends React.Component {
  static propTypes = {

  }

  async componentDidMount () {
    initForCreatePrintErrors(this.props.dispatch)
  }

  async componentWillUnmount () {
    clearSelected(this.props.dispatch)
  }

  render () {
    return (
      <BasePage headerProps={{hideBackButton: false, showPrinterErrorButton: false, pageName: I18n.t("page.printer_error")}}>
        <View>
          <PrinterErrorReceiptView />
        </View>
        <View>
          <PrinterErrorList />
        </View>
      </BasePage>
      )
  }
}

const mapDispatchToProps = dispatch => ({

})

const mapStateToProps = state => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(PrinterErrorPage)
