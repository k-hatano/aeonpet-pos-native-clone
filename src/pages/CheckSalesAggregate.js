import React, { Component } from 'react'
import { Text } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Col, Row, Grid } from 'react-native-easy-grid'
import I18n from 'i18n-js'
import Header from '../common/components/layout/OldHeader'
import Footer from '../common/components/layout/Footer'
import TouchableOpacitySfx from '../common/components/elements/TouchableOpacitySfx'
import SalesAggregate from '../modules/checkSales/components/SalesAggregate'
import commonStyles from '../common/styles/layout/Page'
import styles from '../modules/checkSales/styles/SalesResult'
import stylesSalesAggregate from '../modules/checkSales/styles/SalesAggregate'

export default class CheckSalesAggregate extends Component {
  constructor (props) {
    super(props)

    this.state = {
      salesAggregate: {
        headerTitle: '非課非課非課',
        headerTopItems: ['<非課>', '<非課>'],
        headerBottomItems: [{ label: '計日', text: '2017年05月24日' }, { label: '計日', text: '2017年05月24日' }],
        dataAggregate: [
          {
            title: '[取引合計：通常会計]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          },
          {
            title: '[通常会計入金内訳]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          },
          {
            title: '[通常会計入金内訳]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          },
          {
            title: '[通常会計入金内訳]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          },
          {
            title: '[通常会計入金内訳]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          },
          {
            title: '[通常会計入金内訳]',
            data: [
              { title: '売上・非課税計', value: '900￥' },
              { title: '通常会計売上', value: '1,000￥' },
              { title: '通常会計値引', value: '0￥' },
              { title: '通常会計非課税', value: '5,000￥' },
              { title: '返品合計', value: '0￥' }
            ]
          }
        ],
        footerData: {
          title: '日計',
          total: '15,000￥'
        }
      }
    }
  }

  render () {
    const { salesAggregate } = this.state

    return (
      <Grid style={commonStyles.basicLayoutRootStyle}>
        <Row style={{ height: 72 }}>
          <Header
            bottomButtons={[
              {
                icon: 'md-person',
                type: 'ionic',
                text: I18n.t('customer.staff'),
                color: '#fff',
                iconSize: 24,
                containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
                onPress: () => {}
              }
            ]}
          />
        </Row>
        <Row>
          <Col style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
            <Row style={{ height: 40, marginTop: 100 }}>
              <TouchableOpacitySfx onPress={() => Actions.checkSales()} style={styles.registerButton}>
                <Text style={{ color: '#fff' }}>{I18n.t('home.check_sales')}</Text>
              </TouchableOpacitySfx>
            </Row>
          </Col>
          <SalesAggregate componentStyles={stylesSalesAggregate} salesAggregate={salesAggregate} />
        </Row>
        <Row style={{ height: 50, width: '100%' }}>
          <Footer
            rightButtons={[
              {
                icon: 'receipt',
                type: 'material',
                text: I18n.t('check_sale.print_receipt'),
                color: '#fff',
                iconSize: 24,
                containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
                onPress: () => {}
              }
            ]}
          />
        </Row>
      </Grid>
    )
  }
}
