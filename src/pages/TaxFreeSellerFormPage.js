import React, { Component } from 'react'
import { StatusBar, View, Text } from 'react-native'
import I18n from 'i18n-js'
import TaxFreeSellerFormContainer from '../modules/taxFree/containers/TaxFreeSellerForm'
import BasePage from 'common/hocs/BasePage'
import PropTypes from 'prop-types'
import { initForTaxFreeSellerForm as init } from '../modules/taxFree/models'
import * as actions from '../modules/taxFree/actions'

export default class TaxFreeSellerFormPage extends Component {
  async componentDidMount() {
    this.props.dispatch(actions.updateJurisdictionTaxOffice(null))
    this.props.dispatch(actions.updatePlaceOfTaxPayment(null))
    this.props.dispatch(actions.updateSellerLocation(null))
    this.props.dispatch(actions.updateSellerNameNickName(null))
  }

  async componentWillMount() {
    this.headerProps = {
      canBack: true,
      hideBackButton: true,
      pageName: I18n.t('tax_free.input_seller_info')
    }
  }

  render() {
    return (
      <BasePage headerProps={this.headerProps}>
        <TaxFreeSellerFormContainer
          order={this.props.order} />
      </BasePage>
    )
  }
}
