import React, { Component } from 'react'
import { StatusBar, View, Text } from 'react-native'
import I18n from 'i18n-js'
import TaxFreeCustomerFormContainer from '../modules/taxFree/containers/TaxFreeCustomerForm'
import BasePage from 'common/hocs/BasePage'
import { connect } from 'react-redux'
import ConfirmView from '../common/components/widgets/ConfirmView'
import { initForTaxFreeCustomerForm as init } from '../modules/taxFree/models'
import * as actions from '../modules/taxFree/actions'
import {
  PASSPORT_TYPE,
  RESIDENCE_TYPE
} from 'modules/taxFree/models'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'

class TaxFreePage extends Component {
  async componentDidMount () {
    init (this.props.dispatch)
    this.props.dispatch(actions.updateTypeOfPassport(PASSPORT_TYPE.PASSPORT))
    this.props.dispatch(actions.updatePassportNumber(null))
    this.props.dispatch(actions.updateCustomerName(null))
    this.props.dispatch(actions.updateBirthday(null))
    this.props.dispatch(actions.updateCountryOfCitizenship(null))
    this.props.dispatch(actions.updateLandedAt(null))
    this.props.dispatch(actions.updateResidenceStatus(RESIDENCE_TYPE.SHORT_TERM_STAY))
    this.props.dispatch(actions.updateOtherResidenceStatus(null))
  }

  async componentWillMount () {
    this.headerProps = {
      hideBackButton: false,
      beforeGoHome: () => ConfirmView.showAsync(I18n.t('message.K-01-I001')),
      onPressBackButton: this.props.onPressBackButton,
      onPressHomeButton: this.props.onPressHomeButton,
      pageName: I18n.t('tax_free.customer_info'),
      showOpenDrawer: !this.props.isTraining
    }
  }

  render () {
    return (
      <BasePage headerProps={this.headerProps}>
        <TaxFreeCustomerFormContainer order={this.props.order}/>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  isTraining: state.setting.settings[SettingKeys.COMMON.REPOSITORY_CONTEXT] === REPOSITORY_CONTEXT.TRAINING
})

export default connect(mapStateToProps, mapDispatchToProps)(TaxFreePage)
