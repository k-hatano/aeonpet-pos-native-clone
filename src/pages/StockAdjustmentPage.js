import React, { Component } from 'react'
import { View } from 'react-native'
import BasePage from '../common/hocs/BasePage'
import StockAdjustmentProductForm from '../modules/stockAdjustment/containers/StockAdjustmentProductForm'
import StockAdjustmentList from '../modules/stockAdjustment/containers/StockAdjustmentList'
import {
  initForStockAdjustment
} from '/modules/stockAdjustment/models'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { initForCategoryProductView } from '../modules/cart/services'
import { MODULE_NAME } from '../modules/stockAdjustment/models'
import ConfirmView from '../common/components/widgets/ConfirmView'

class StockAdjustmentPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isSearchProduct: true
    }
  }

  async componentWillMount () {
    await initForCategoryProductView(this.props.dispatch)

    initForStockAdjustment(this.props.dispatch)
  }

  async _canTransition () {
    if (this.props.products.length === 0) return true

    return await ConfirmView.showAsync(I18n.t('message.M-01-I002'))
  }

  render () {
    return (
      <BasePage
        headerProps={{
          hideBackButton: false,
          canBack: true,
          onBack: async () => {
            if (await this._canTransition()) {
              this.props.onBack()
            }
          },
          onChangeStaffModalShowing: (isSearchProduct) => {
            this.setState({isSearchProduct: isSearchProduct})
          },
          beforeGoHome: async () => await this._canTransition(),
          pageName: I18n.t('stock_adjustment.stock_adjustment')
        }}>
        <View style={{ flex: 1 }}>
          <StockAdjustmentList />
        </View>
        <View style={{ flex: 1 }}>
          <StockAdjustmentProductForm
            isSearchProduct={this.state.isSearchProduct} />
        </View>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onBack: async () => {
    Actions.stockAdjustmentSearch()
  }
})

const mapStateToProps = state => ({
  products: state[MODULE_NAME].products
})

export default connect(mapStateToProps, mapDispatchToProps)(StockAdjustmentPage)
