import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import BasePage from '../common/hocs/BasePage'
import StockAdjustmentSearchForm from '../modules/stockAdjustment/containers/StockAdjustmentSearchForm'
import StockAdjustmentSearchList from '../modules/stockAdjustment/containers/StockAdjustmentSearchList'
import StockAdjustmentSearchDetailView from '../modules/stockAdjustment/containers/StockAdjustmentSearchDetailView'
import {
  MODULE_NAME,
  initForStockAdjustmentSearch,
  clearStateForStockAdjustmentSearch
} from '../modules/stockAdjustment/models'
import I18n from 'i18n-js'
import { searchStockAdjustments } from '../modules/stockAdjustment/services'

class StockAdjustmentSearchPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isSearchProduct: true
    }
  }

  async componentWillMount () {
    await initForStockAdjustmentSearch(this.props.dispatch)
    await clearStateForStockAdjustmentSearch(this.props.dispatch)
    // この画面に遷移してきたら必ず検索を実施する。
    // 新規登録完了後の再検索もこれで兼ねている。
    await searchStockAdjustments(this.props.dispatch, this.props.stockAdjustmentState)
  }

  render () {
    return (
      <BasePage
        headerProps={{
          hideBackButton: false,
          onChangeStaffModalShowing: (isSearchProduct) => {
            this.setState({isSearchProduct: isSearchProduct})
          },
          pageName: I18n.t('stock_adjustment.stock_adjustment_search')
        }}>
        <View style={{ flex: 1 }}>
          <StockAdjustmentSearchForm
            isSearchProduct={this.state.isSearchProduct} />
        </View>
        <View style={{ flex: 1 }}>
          {Object.keys(this.props.stockAdjustment).length > 0 ? (
            <StockAdjustmentSearchDetailView />
          ) : (
            <StockAdjustmentSearchList />
          )}
        </View>
      </BasePage>
    )
  }
}

const mapStateToProps = state => ({
  stockAdjustment: state[MODULE_NAME].currentStockAdjustment,
  stockAdjustmentState: state[MODULE_NAME]
})

export default connect(mapStateToProps)(StockAdjustmentSearchPage)
