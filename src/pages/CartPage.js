import React from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { View, NativeModules, NativeEventEmitter } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import CartView from '../modules/cart/containers/CartView'
import * as productActions from '../modules/product/actions'
import * as orderActions from '../modules/order/actions'
import * as cartActions from '../modules/cart/actions'
import ProductRepository from '../modules/product/repositories/ProductRepository'
import {
  MODULE_NAME as cartModuleName,
  MAX_ACTIVE_PAYMENT
} from 'modules/cart/models'
import {
  MODULE_NAME as productModuleName,
  PREV_SEARCH_COND,
  CHANGE_PRICE_TYPE
} from 'modules/product/models'
import {
  MODULE_NAME as orderModuleName,
  getOrderPropertyInfo
} from 'modules/order/models'
import { MODULE_NAME as homeModuleName } from 'modules/home/models'
import Cart from '../modules/cart/models/Cart'
import CategoryProductView from '../modules/product/containers/CategoryProductView'
import BasePage from '../common/hocs/BasePage'
import TabPanel from '../common/components/widgets/TabPanel'
import CartSelectProductFooter from '../modules/cart/components/CartSelectProductFooter'
import CartAmountView from '../modules/cart/components/CartAmountView'
import CartCustomerView from '../modules/cart/containers/CartCustomerView'
import { getSettingFromState, PRINTER_MAKERS } from '../modules/setting/models'
import SettingKeys from '../modules/setting/models/SettingKeys'
import AlertView from '../common/components/widgets/AlertView'
import CashChanger from 'modules/printer/models/CashChanger'
import { loading } from '../common/sideEffects'
import {
  initilizeCart,
  restorePendedCart,
  proceedUpdateCart,
  initForCategoryProductView
} from '../modules/cart/services'
import { showSelectOrderPropertyModal } from '../modules/order/containers/OrderPropertyModalView'
// 自動釣銭機用の
import { showSelectOrderPropertyCashierModal } from '../modules/order/containers/OrderPropertyModalCashierView'
// 現金以外の支払い方法のとき
import { showSelectOrderPropertyOtherModal } from '../modules/order/containers/OrderPropertyModalOtherView'
import { PAYMENT_METHOD_TYPE, isCash } from '../modules/payment/models'
import CashVoucherRepository from '../modules/cash/repositories/CashVoucherRepository'
import { waitAsync } from '../common/utils/waitAsync'
import CartManager from '../modules/cart/models/CartManager'
import { PERMISSION_CODES } from 'modules/staff/models'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import Modal from 'common/components/widgets/Modal'
import ConfirmView from 'common/components/widgets/ConfirmView'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import { listPendedOrders } from 'modules/home/actions'
import AmountPadModal from 'common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from 'common/components/elements/AmountPad'
import logger from 'common/utils/logger'
import { registerBarcodeReaderEvent } from '../modules/barcodeReader/services'
import StoreAccessibleBase from '../common/models/StoreAccessibleBase'
import {
  isBooks,
  isFirstStepBarcodeInBooks,
  isSecondStepBarcodeInBooks,
  isPromotionBarcode
} from '../modules/product/models'
import BooksProductSearcher from '../modules/cart/models/BooksProductSearcher'
import PromotionRepository from 'modules/promotion/repositories/PromotionRepository'
import CartDiscount from 'modules/cart/models/CartDiscount'
import {
  PROMOTION_TYPE,
  DISCOUNT_TYPE,
  THRESHOLD_TYPE
} from 'modules/promotion/models'
import queue from 'fastq'

// TODO styleを追い出す

class CartPage extends React.Component {
  constructor() {
    super()
    this.state = {
      tabIndex: 1,
      isSearchProduct: true,
      isScrollScheduled: false,
      depositDisabled: false,
      queueLength: 0
    }
    this.useableBasePoints = 1
    this.productSelectQueue = queue(this._workerProductSelect, 1)
    this.barcodeQueue = queue(this._workerBarcode, 1)
    this.addMinusQueue = queue(this._workerAddMinusQueue, 1)
  }

  static propTypes = {
    updateCart: PropTypes.func,
    onPaymentChange: PropTypes.func,
    onDeposit: PropTypes.func,
    onComplete: PropTypes.func,
    onInit: PropTypes.func,
    cart: React.PropTypes.instanceOf(Cart),
    taxRule: PropTypes.number,
    taxRate: PropTypes.number,
    cartHistory: PropTypes.array,
    mode: PropTypes.oneOf(['order', 'unassociated_return', 'return']),
    repositoryContext: PropTypes.string,
    pendedOrders: PropTypes.array,
    pendedOrder: PropTypes.object,
    restorePendedOrderToCart: PropTypes.func
  }

  async componentWillMount() {
    const orderPropertiesInfo = await getOrderPropertyInfo()
    this.props.onInit && (await this.props.onInit())
    initForCategoryProductView(this.props.dispatch)
    this.props.dispatch(productActions.setSearchLayoutFlg(false))
    this.props.dispatch(
      orderActions.setOrderProperty(orderPropertiesInfo.orderProperty)
    )
    this.props.dispatch(
      orderActions.setSelectedProperties(orderPropertiesInfo.selectedProperties)
    )
    const pendedOrders = await OrderPendRepository.findAll()
    this.props.dispatch(listPendedOrders(pendedOrders))
  }

  async componentDidMount() {
    // ショップごとのポイント利用基底値取得
    // 本番時login用APIから'common.useable_base_points' として帰ってくる前提
    this.useableBasePoints = new StoreAccessibleBase().getSetting(
      SettingKeys.COMMON.USEABLE_BASE_POINTS
    )
    this.eventBarcodeReader = registerBarcodeReaderEvent(body => {
      this.barcodeQueue.push({ body })
    })
  }

  componentWillUnmount() {
    initForCategoryProductView(this.props.dispatch)
    this.eventBarcodeReader.remove()
    CashChanger.disconnectCashChanger()
    this._killAllQueue()
  }

  _workerProductSelect = async ({ product, amount }, cb) => {
    /** @type Cart */
    const cart = this.props.cart
    const productDetail = await ProductRepository.findWithDetail(product.id)
    const nextCartState = await cart.addProductAsync(
      productDetail,
      productDetail.product_variants[0],
      1,
      amount
    )
    this.props.updateCart(nextCartState)
    this.setState({
      isScrollScheduled: true,
      queueLength: this.barcodeQueue.length() + this.productSelectQueue.length()
    })
    cb(null)
  }

  _workerAddMinusQueue = async ({ cart, oldItem, newItem }, cb) => {
    const newCart = await cart.updateItemAsync(oldItem, newItem)
    await proceedUpdateCart(cart, newCart, this.props.dispatch)
    cb(null)
  }

  _handleOnItemChanged = (cart, oldItem, newItem) => {
    this.addMinusQueue.push({ cart, oldItem, newItem })
  }

  _killAllQueue = () => {
    this.productSelectQueue.kill()
    this.barcodeQueue.kill()
    this.addMinusQueue.kill()
  }

  _handleUpdateCart = cart => {
    if (cart.items.size === 0) {
      this._killAllQueue()
    }
    this.props.updateCart(cart)
  }

  _workerBarcode = async ({ body }, cb) => {
    if (this.state.isSearchProduct) {
      let searchResult = null
      const cashVoucher = await CashVoucherRepository.findByBarcode(body.trim())
      // 金券の場合
      if (cashVoucher !== null) {
        const isUpdateCart = await _updateAmountByBarcode(
          this.props.dispatch,
          this.props.cart,
          cashVoucher
        )
        const targetTabIndex = 0
        const isChangedTabIndex = this.state.currentTabIndex !== targetTabIndex

        if (isChangedTabIndex) {
          this.setState({
            tabIndex: 0
          })
        }
        if (isUpdateCart) {
          this.setState({ isScrollScheduled: true })
        }
        return
      }

      const couponDiscounts = await PromotionRepository.findCouponsByBarcode(
        body.trim()
      )
      // クーポンの場合
      if (couponDiscounts.length) {
        // クーポン値割引は全て小計値引きとして処理する
        if (couponDiscounts[0].threshold_type !== THRESHOLD_TYPE.SUBTOTAL) {
          return
        }

        const discount = await _discountCoupon(this.props.cart, couponDiscounts)

        if (!discount.totalPrice) {
          AlertView.show(I18n.t('promotion.product_not_found'))
          return
        }

        if (discount.totalPrice < couponDiscounts[0].threshold_min) {
          const couponName = I18n.t('promotion.coupon_name', {
            name: couponDiscounts[0].name
          })
          const couponCondition = I18n.t('promotion.coupon_condition', {
            threshold_min: couponDiscounts[0].threshold_min
          })
          const message =
            I18n.t('promotion.invalid_cart_condition') +
            '\n\n' +
            couponName +
            '\n' +
            couponCondition
          AlertView.show(message)
          return
        }

        const cartDiscount = CartDiscount.createWithDiscountReason(
          discount.totalDiscountAmount,
          discount.type,
          null,
          discount.name,
          PROMOTION_TYPE.COUPON
        )
        const newCart = this.props.cart.applySubtotalDiscount(cartDiscount)
        proceedUpdateCart(this.props.cart, newCart, this.props.dispatch)
        this.setState({ tabIndex: 0 })
        this.setState({ isScrollScheduled: true })
        return
      }

      // セールかバンドルのバーコード（先頭2桁が98）の場合
      if (isPromotionBarcode(body.trim())) {
        const bundle = await PromotionRepository.findBundlesByBarcode(
          body.trim()
        )
        const sale = await PromotionRepository.findSalesByBarcode(body.trim())
        if (!bundle && !sale) {
          AlertView.show(I18n.t('promotion.not_exist'))
          return
        }

        if (this.props.cart.scannedPromotionBarcodes.includes(body.trim())) {
          let name = ''
          if (bundle) {
            name = bundle.name
          }
          if (sale) {
            name = sale.name
          }

          ConfirmView.show(
            I18n.t('promotion.already_added_confirm', { name: name }),
            () => {
              const newCart = this.props.cart
                .removeScannedPromotionBarcode(body.trim())
                .setIsBundleDisabled(false)
              proceedUpdateCart(this.props.cart, newCart, this.props.dispatch)
            }
          )
          return
        }

        const newCart = this.props.cart
          .addScannedPromotionBarcode(body.trim())
          .setIsBundleDisabled(false)
        proceedUpdateCart(this.props.cart, newCart, this.props.dispatch)
        return
      }

      // バーコード読み取り時先頭3桁, 978, 979, 192 のいずれかだった場合
      if (isBooks(body.trim())) {
        searchResult = await this.props.onBooksSearch(
          this.props.cart,
          body.trim(),
          this.props.stockConditionFlg,
          true,
          this.state.lastScannedBarcode
        )
      } else {
        searchResult = await this.props.onSearch(
          this.props.cart,
          body.trim(),
          this.props.stockConditionFlg,
          true
        )
      }

      this.setState({ lastScannedBarcode: searchResult.lastScannedBarcode })

      if (searchResult.isChangedTabIndex) {
        this.setState({
          tabIndex: 1
        })
      }
      if (searchResult.isUpdateCart) {
        this.setState({ isScrollScheduled: true })
      }
    }
    this.setState({
      queueLength:
        this.barcodeQueue.length() +
        this.productSelectQueue.length() +
        this.addMinusQueue.length()
    })
    cb(null)
  }

  _productRequiredChangePriceSelected = async product => {
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.CASH,
      onComplete: amount => {
        Modal.close()
        this._productSelected(product, amount)
      },
      title: product.name
    })
  }

  _productSelected = async (product, amount = undefined) => {
    this.productSelectQueue.push({ product, amount })
    this.setState({
      queueLength:
        this.barcodeQueue.length() +
        this.productSelectQueue.length() +
        this.addMinusQueue.length()
    })
  }

  async _openOrderPropertyModal(_cart) {
    if (_cart.activePayments.size > MAX_ACTIVE_PAYMENT) {
      AlertView.show(I18n.t('message.B-06-E002'))
      return
    }
    if (_cart.isReturn) {
      this._complete([], _cart)
      return
    }
    const orderProperty = this.props.orderProperty
    let propertyCount = 0
    for (let i = 0; i < orderProperty.length; i++) {
      if (!orderProperty[i].options) {
        continue
      }
      let optionCount = 0
      for (let j = 0; j < orderProperty[i].options.length; j++) {
        if (orderProperty[i].options[j].status === 1) {
          optionCount++
        }
      }
      if (optionCount > 0) {
        propertyCount++
      }
    }
    if (propertyCount > 0) {
      showSelectOrderPropertyModal(_cart, cartOrderProperties => {
        this._showPayModal(cartOrderProperties, _cart)
      })
    } else {
      this._showPayModal([], _cart)
    }
  }

  // 自動釣銭機用関数
  checkAutoCashier(cartOrderProperties) {
    const cashPayment = this.props.cart
      .get('payments')
      .filter(payment => isCash(payment.paymentMethodType))
      .first()
    const justAmount = this.props.cart
      .justAmountForPayment(cashPayment)
      .toNumber()

    showSelectOrderPropertyCashierModal(
      justAmount,
      this.props.cart.currency,
      async newPaymentAmount => {
        if (cartOrderProperties) {
          let otherCheck = false
          const paymentsObj = this.props.cart.payments.toJS()
          for (let i = 0, len = paymentsObj.length; i < len; i++) {
            if (
              paymentsObj[i].paymentMethodType !== PAYMENT_METHOD_TYPE.CASH &&
              paymentsObj[i].amount.toNumber() !== 0
            ) {
              otherCheck = true
              break
            }
          }
          await this._updateCashCartPaymentAmount(newPaymentAmount)
          if (otherCheck) {
            // 現金以外もある
            this._otherThanCashPayment(cartOrderProperties)
          } else {
            // 現金のみ
            this._complete(cartOrderProperties)
          }
        }
      },
      () => {
        this.enableDepositBtn()
      }
    )
  }

  _updateCashCartPaymentAmount(newPaymentAmount) {
    const currentCashCartPayment = this.props.cart.payments
      .filter(payment => isCash(payment.paymentMethodType))
      .first()
    let newCashCartPayment = currentCashCartPayment.updateAmount(
      newPaymentAmount
    )
    this.props.onPaymentChange(
      this.props.cart,
      currentCashCartPayment,
      newCashCartPayment
    )
  }

  // 現金以外の支払いがあった場合の処理
  _otherThanCashPayment(cartOrderProperties, cart = this.props.cart) {
    if (this._hasCCTPayment()) {
      // （クレジット・電子マネー）の支払
      this._showPaymentProcessingModal(cartOrderProperties, cart)
      return
    }
    // その他支払（外部ポイント、アニコム保険、金券等）
    this._complete(cartOrderProperties)
  }

  /**
   * 支払処理ポップアップ表示
   * @param {Array} cartOrderProperties
   */
  _showPayModal(cartOrderProperties, _cart) {
    if (this._hasCashPayment(_cart)) {
      if (CashChanger.isEnabled) {
        this.checkAutoCashier(cartOrderProperties)
      } else {
        this._otherThanCashPayment(cartOrderProperties, _cart)
      }
    } else {
      this._otherThanCashPayment(cartOrderProperties, _cart)
    }
  }

  /**
   * 決済処理中ポップアップ表示
   * @param {Array} cartOrderProperties
   */
  _showPaymentProcessingModal(cartOrderProperties, _cart) {
    showSelectOrderPropertyOtherModal(
      _cart,
      async result => {
        result.messages.forEach(async message => {
          await AlertView.showAsync(message)
        })

        if (result.canContinue) {
          this._complete(cartOrderProperties, result.cart)
          return
        }
        this.enableDepositBtn()
      },
      async result => {
        if (result.messages) {
          result.messages.forEach(async message => {
            await AlertView.showAsync(message)
          })
        }

        if (CashChanger.isEnabled) {
          const cashPaymentAmount = _cart.payments
            .filter(payment => isCash(payment.paymentMethodType))
            .first().amount
          CashChanger.dispenseChange(cashPaymentAmount)
        }
        this.enableDepositBtn()
      }
    )
  }

  /**
   * 支払方法に現金支払が指定されているか
   * @param {Cart} cart
   */
  _hasCashPayment(cart) {
    const payments = cart.get('payments')
    return (
      payments
        .filter(
          payment =>
            isCash(payment.paymentMethodType) && payment.amount.toNumber() !== 0
        )
        .count() !== 0
    )
  }

  /**
   * 支払方法にクレジット・電子マネー決済があるか
   */
  _hasCCTPayment() {
    const paymentsObj = this.props.cart.payments.toJS()
    for (let i = 0, len = paymentsObj.length; i < len; i++) {
      if (
        (paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.CREDIT ||
          paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.EMONEY ||
          paymentsObj[i].paymentMethodType === PAYMENT_METHOD_TYPE.UNION_PAY) &&
        !paymentsObj[i].isOffline &&
        paymentsObj[i].amount.toNumber() !== 0
      ) {
        return true
      }
    }
    return false
  }

  disableDepositBtn() {
    this.setState({ depositDisabled: true })
  }

  enableDepositBtn() {
    // 連続タップ防止
    waitAsync(200).then(() => {
      this.setState({ depositDisabled: false })
    })
  }

  async _deposit() {
    this.disableDepositBtn()
    const isTraining =
      this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING
    const result = await this.props.onDeposit(this.props.cart, [], isTraining)
    if (result.canContinue) {
      this.props.updateCart(result.cart, false)
      this._openOrderPropertyModal(result.cart)
      return
    }
    this.enableDepositBtn()
  }

  async _complete(cartOrderProperties, cart = this.props.cart) {
    if (CashChanger.isEnabled) {
      const isError = await this.props.onDispenseChange(cart)
      if (isError) {
        this._updateCashCartPaymentAmount(0)
        this.enableDepositBtn()
        return
      }
    }

    const isTraining =
      this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING
    const isSucceed = await this.props.onComplete(
      cart,
      cartOrderProperties,
      isTraining
    )
    if (isSucceed) {
      if (this.props.cart.isReturn) {
        Actions.returnCompletePage({ isTraining })
      } else {
        Actions.orderCompletePage({ isTraining })
      }
      return
    }
    this.enableDepositBtn()
  }

  _getDebugMessage() {
    return JSON.stringify(this.props.cartHistory.map(cart => cart.toJSON()))
  }

  render() {
    /** @type Cart */
    const cart = this.props.cart
    const queueLength = this.state.queueLength
    const permissionCode =
      this.props.mode === 'order'
        ? PERMISSION_CODES.NEW_ORDER
        : PERMISSION_CODES.INDEPENDENT_RETURN_ORDER
    const showOpenDrawer =
      this.props.repositoryContext !== REPOSITORY_CONTEXT.TRAINING

    return (
      <BasePage
        headerProps={{
          pageName: I18n.t('page.' + this.props.mode),
          permissionCode: permissionCode,
          pendedOrders: this.props.pendedOrders,
          restorePendedOrderToCart: this.props.restorePendedOrderToCart,
          showPendedOrderButton: !cart.isReturn,
          usesCautionColor: this.props.mode !== 'order',
          beforeGoHome: () => {
            return cart.items.size > 0
              ? ConfirmView.showAsync(I18n.t('message.B-01-I002'))
              : true
          },
          showOpenDrawer,
          onChangeStaffModalShowing: isSearchProduct => {
            this.setState({ isSearchProduct: isSearchProduct })
          }
        }}
      >
        <CartView
          cart={this.props.cart}
          onUpdateCart={this._handleUpdateCart}
          onItemChanged={this._handleOnItemChanged}
          isScrollScheduled={this.state.isScrollScheduled}
          endScroll={() => this.setState({ isScrollScheduled: false })}
          isTraining={
            this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING
          }
          onPendOrder={() => this.setState({ tabIndex: 0 })}
        />
        <View style={{ flex: 1 }}>
          <CartCustomerView
            setIsSearchProduct={isSearchProduct => {
              this.setState({ isSearchProduct: isSearchProduct })
            }}
          />
          <TabPanel
            style={{ flex: 1 }}
            tabStyle={{ width: 144 }}
            selectedTab={this.state.tabIndex}
          >
            <View
              tabName={I18n.t('cart.subtotal')}
              style={{ flex: 1 }}
              onTabClick={tabIndex => this.setState({ tabIndex })}
            >
              <CartAmountView
                cart={cart}
                onPaymentChange={(old, new_) => {
                  this.props.onPaymentChange(cart, old, new_)
                }}
                queueLength={queueLength}
                onUpdateCart={this.props.updateCart}
                onDeposit={() => this._deposit()}
                depositDisabled={this.state.depositDisabled}
                useableBasePoints={this.useableBasePoints}
              />
            </View>

            <View
              tabName={I18n.t('cart.select_product')}
              style={{ justifyContent: 'flex-end', height: '100%' }}
              onTabClick={tabIndex => this.setState({ tabIndex })}
            >
              <CategoryProductView
                onProductSelected={product => {
                  product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED
                    ? this._productRequiredChangePriceSelected(product)
                    : this._productSelected(product)
                }}
                style={{ flex: 1 }}
              />
              <CartSelectProductFooter
                cart={cart}
                style={{ height: 80 }}
                onSubtotal={() => this.setState({ tabIndex: 0 })}
              />
            </View>
          </TabPanel>
        </View>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onInit: async (mode, customer, pendedOrder = null) => {
    if (pendedOrder) {
      await restorePendedCart(pendedOrder, dispatch)
    } else {
      await initilizeCart(mode, customer, dispatch)
    }
  },
  updateCart: async (oldCart, newCart, updateMesseage = true) => {
    await waitAsync(10) // ここでawaitを入れないと、小計値引き時にアラートを出すときのモーダルのClose順がおかしくなるので暫定対応
    return proceedUpdateCart(oldCart, newCart, dispatch, updateMesseage)
  },
  onPaymentChange: async (cart, oldPayment, newPayment) => {
    const newCart = await cart.updatePayment(oldPayment, newPayment)
    return proceedUpdateCart(cart, newCart, dispatch)
  },
  onDeposit: async (cart, cartOrderProperties, isTraining = false) => {
    cart = cart.setIsTrainingMode(isTraining)
    const result = await loading(dispatch, async () => {
      return new CartManager().depositAsync(cart, cartOrderProperties)
    })
    for (const i in result.messages) {
      await AlertView.showAsync(result.messages[i])
    }
    return result
  },
  onDispenseChange: async cart => {
    const result = await loading(dispatch, async () => {
      const payments = cart.get('payments')
      const cashPaymentAmount = payments
        .filter(payment => isCash(payment.paymentMethodType))
        .first().amount
      const dispenseAmount = cart.isReturn ? cashPaymentAmount : cart.change

      if(dispenseAmount == 0) {
        return {
          isError: false
        }
      }
      try {
        await CashChanger.dispenseChange(dispenseAmount)
        return {
          isError: false
        }
      } catch (error) {
        try {
          await CashChanger.dispenseChange(cashPaymentAmount)
        } catch (error) {
          // ここで失敗したらどうしようもないので、握りつぶす
          return {
            isError: true,
            messages: I18n.t('message.G-02-E010')
          }
        }
        return {
          isError: true,
          messages: error.message
        }
      }
    })

    if (result.messages) {
      await AlertView.showAsync(result.messages)
    }

    return result.isError
  },
  /**
   *
   * @param {Cart} cart
   * @param cartOrderProperties
   * @return {Promise.<boolean>}
   */
  onComplete: async (cart, cartOrderProperties, isTraining = false) => {
    cart = cart.setIsTrainingMode(isTraining)
    const result = await loading(dispatch, async () => {
      const cartManager = new CartManager()
      return cartManager.completeAsync(cart, cartOrderProperties)
    })
    for (const i in result.messages) {
      await AlertView.showAsync(result.messages[i])
    }
    dispatch(cartActions.updateCart(result.cart))
    return result.canContinue
  },
  // バーコードが読まれたときに二番目に動く
  onSearch: async (cart, word, conditionFlg, isBarcode) => {
    let isChangedTabIndex = false
    let isUpdateCart = false
    if (word.length === 0) {
      AlertView.show(I18n.t('message.B-01-I003'))
      return { isChangedTabIndex, isUpdateCart }
    } else {
      // バーコードが読み込まれた
      let products = await ProductRepository.findProductsBySearchWord(
        word,
        conditionFlg,
        isBarcode
      )
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      const manager = new CartManager()
      switch (products.length) {
        case 0:
          let productVariantsByWord = await ProductRepository.findVariantsBySearchWord(
            word,
            conditionFlg
          )
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              logger.warning(I18n.t('product.not_affected_scan') + '：' + word)
              break
            case 1:
              const product = productVariantsByWord[0]
              if (product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED) {
                Modal.close()
                await AmountPadModal.open({
                  mode: AMOUNT_PAD_MODE.CASH,
                  onComplete: async amount => {
                    Modal.close()
                    await manager.updateCart(dispatch, cart, product, amount)
                  },
                  title: product.name
                })
              } else {
                await manager.updateCart(dispatch, cart, product)
              }
              isUpdateCart = true
              break

            default:
              isChangedTabIndex = true
              dispatch(productActions.listProducts(productVariantsByWord))
              dispatch(productActions.listOriginProducts(productVariantsByWord))
              dispatch(
                productActions.setPrevSearchCond(
                  PREV_SEARCH_COND.SEARCH_BY_WORD
                )
              )
              dispatch(productActions.setSearchLayoutFlg(true))
              break
          }
          break
        case 1:
          const productVariantsById = await ProductRepository.findVariantsByProductId(
            products[0].id
          )
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              logger.warning(I18n.t('product.not_affected_scan') + '：' + word)
              break
            case 1:
              const product = products[0]
              if (product.change_price_type === CHANGE_PRICE_TYPE.REQUIRED) {
                Modal.close()
                await AmountPadModal.open({
                  mode: AMOUNT_PAD_MODE.CASH,
                  onComplete: async amount => {
                    Modal.close()
                    await manager.updateCart(dispatch, cart, product, amount)
                  },
                  title: product.name
                })
              } else {
                await manager.updateCart(dispatch, cart, product)
              }
              isUpdateCart = true
              break
            default:
              break
          }
          break
        default:
          isChangedTabIndex = true
          dispatch(productActions.listProducts(products))
          dispatch(productActions.listOriginProducts(products))
          dispatch(
            productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD)
          )
          dispatch(productActions.setSearchLayoutFlg(true))
          break
      }
      return { isChangedTabIndex, isUpdateCart }
    }
  },
  onBooksSearch: async (
    cart,
    barcode,
    conditionFlg,
    isBarcode,
    lastScannedBarcode
  ) => {
    const booksSearchResult = {
      isChangedTabIndex: false,
      isUpdateCart: false,
      lastScannedBarcode: undefined
    }

    if (barcode.length === 0) {
      AlertView.show(I18n.t('message.B-01-I003'))
      return booksSearchResult
    }

    if (
      isSecondStepBarcodeInBooks(barcode) &&
      isFirstStepBarcodeInBooks(lastScannedBarcode)
    ) {
      let products = await ProductRepository.findProductsBySearchWord(
        lastScannedBarcode,
        conditionFlg,
        isBarcode
      )
      if (products.length === 0) {
        products = await ProductRepository.findVariantsBySearchWord(
          lastScannedBarcode,
          conditionFlg
        )
      }

      // lastScannedBarcodeに値が代入されている場合、必ず商品テーブルか商品規格テーブルのどちらかに商品が存在している状態
      await new CartManager().updateCart(
        dispatch,
        cart,
        products.shift(),
        barcode.substr(7, 5)
      )
      booksSearchResult.isUpdateCart = true
      return booksSearchResult
    }

    return BooksProductSearcher.searchProducts(
      dispatch,
      cart,
      barcode,
      conditionFlg,
      isBarcode
    )
  },
  restorePendedOrderToCart: async (pendedOrder, oldCart) => {
    Modal.close()
    if (oldCart.items.size > 0 || oldCart.customer) {
      ConfirmView.show(I18n.t('message.B-02-I002'), async () => {
        restorePendedCart(pendedOrder, dispatch)
      })
    } else {
      restorePendedCart(pendedOrder, dispatch)
    }
  }
})

const _updateAmountByBarcode = async (dispatch, cart, cashVoucher) => {
  if (cashVoucher !== null) {
    let oldPayment
    for (let i = 0; i < cart.payments.toJS().length; i++) {
      if (
        cart.payments.toArray()[i].paymentMethodType ===
        cashVoucher['payment_methods']['payment_method_type']
      ) {
        oldPayment = cart.payments.toArray()[i]
        break
      }
    }

    if (!cart.canUsePayment(oldPayment)) {
      await AlertView.showAsync(I18n.t('message.B-01-E002'))
      return false
    }

    const newPayment = oldPayment.updateAmount(
      parseInt(oldPayment.amount) + parseInt(cashVoucher.amount)
    )
    const newCart = await cart.updatePayment(oldPayment, newPayment)
    proceedUpdateCart(cart, newCart, dispatch)
  }

  return true
}

const _discountCoupon = async (cart, couponDiscounts) => {
  const discount = {
    name: couponDiscounts[0].name,
    type: DISCOUNT_TYPE.AMOUNT,
    totalDiscountAmount: 0,
    totalPrice: 0
  }

  await cart.items.forEach(cartItem => {
    const coupon = couponDiscounts.find(product => {
      return cartItem.product.productId === product['coupon_item.product_id']
    })
    if (!coupon) return
    for (let i = 0; i < cartItem.quantity; i++) {
      discount.totalPrice += cartItem.product.price
      if (coupon.discount_rate) {
        discount.totalDiscountAmount +=
          cartItem.product.price * coupon.discount_rate
      } else {
        discount.totalDiscountAmount += coupon.discount_price
      }
    }
  })

  return discount
}

const mapStateToProps = state => ({
  cart: state[cartModuleName].cart,
  cartHistory: state[cartModuleName].cartHistory,
  taxRule: getSettingFromState(state, SettingKeys.ORDER.TAX.RULE),
  taxRate: getSettingFromState(state, SettingKeys.ORDER.TAX.RATE),
  staff: getSettingFromState(state, SettingKeys.STAFF.CURRENT_STAFF),
  orderProperty: state[orderModuleName].orderProperty,
  stockConditionFlg: state[productModuleName].stockConditionFlg,
  repositoryContext: getSettingFromState(
    state,
    SettingKeys.COMMON.REPOSITORY_CONTEXT
  ),
  pendedOrders: state[homeModuleName].pendedOrders
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onInit: () => {
      return dispatchProps.onInit(
        ownProps.mode,
        ownProps.customer || null,
        ownProps.pendedOrder || null
      )
    },
    onDeposit: (cart, cartOrderProperties, isTraining = false) => {
      return dispatchProps.onDeposit(cart, cartOrderProperties, isTraining)
    },
    onComplete: (cart, cartOrderProperties, isTraining = false) => {
      return dispatchProps.onComplete(cart, cartOrderProperties, isTraining)
    },
    updateCart: (newCart, updateMessage) => {
      return dispatchProps.updateCart(stateProps.cart, newCart, updateMessage)
    },
    onSearch: (cart, searchWord, stockConditionFlg, isBarCode) => {
      return dispatchProps.onSearch(
        cart,
        searchWord,
        stockConditionFlg,
        isBarCode
      )
    },
    restorePendedOrderToCart: pendedOrder => {
      return dispatchProps.restorePendedOrderToCart(
        pendedOrder,
        stateProps.cart
      )
    },
    onBooksSearch: (
      cart,
      searchWord,
      stockConditionFlg,
      isBarCode,
      lastScannedBarcode
    ) => {
      return dispatchProps.onBooksSearch(
        cart,
        searchWord,
        stockConditionFlg,
        isBarCode,
        lastScannedBarcode
      )
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CartPage)
