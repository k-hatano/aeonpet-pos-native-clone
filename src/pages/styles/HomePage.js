import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutLeft: {
    height: '100%',
    backgroundColor: '#f0f0f0'
  },
  layoutRight: {
    height: '100%'
  },
  specialCommandArea: {
    flex: 2.5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 23
  },
  menuArea: {
    flex: 30,
    marginHorizontal: 70
  },
  mainMenuArea: {
    flex: 10,
    flexDirection: 'row',
    marginVertical: 10
  },
  primaryMenuArea: {
    margin: 8,
    flex: 1
  },
  secondaryMenuArea: {
    flex: 1
  },
  subMenuArea: {
    flex: 22
  },
  secondaryMenu: {
    flex: 1,
    margin: 8
  },
  subMenuButton: {
    width: 165,
    height: 128,
    marginVertical: 8
  },
  subMenuContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: 8
  },
  specialCommandIconButtonWrapper: {
    width: 31,
    height: 31,
    marginHorizontal: 16
  },
  specialCommandIconButton: {
    width: 31,
    height: 31,
    color: '#93979A',
    fontSize: 36
  },
  ImageButtonWrapper: {
    width: '77%',
    alignItems: 'flex-end',
    height: 31
  },
  ImageButton: {
    width: 31,
    height: 31
  },
  listOrder: {
    flex: 1,
    justifyContent: 'center'
  }
})
