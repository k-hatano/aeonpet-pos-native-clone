'use strict'

import React, { Component } from 'react'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { Col, Row, Grid } from 'react-native-easy-grid'
import Header from '../common/components/layout/OldHeader'
import Footer from '../common/components/layout/Footer'
import CustomerSearchForm from '../modules/customer/components/CustomerSearchForm'
import CustomerSearchResult from '../modules/customer/components/CustomerSearchResult'
import commonStyles from '../common/styles/layout/Page'

export default class CustomerSearch extends Component {
  constructor (props) {
    super(props)

    this.state = {
      chosenCustomer: null
    }
  }

  _onCustomerUnChoose = () => {
    this.setState({ chosenCustomer: null })
  }

  _onCustomerChoose = item => {
    this.setState({ chosenCustomer: item })
  }

  _onItemChoose (customer) {
    Actions.customerResult({ customer })
  }

  render () {
    const data = [
      {
        id: 2,
        name: 'ヤマダタロウ様',
        nameKana: 'name kana 2',
        code: 1234567890123,
        status: 'unknowmember',
        possessedPoint: 1,
        buyTime: 2,
        lastBuyDate: '2017/02/28',
        registeredShop: 'shop name',
        phonetic: 'phonetic',
        postalCode: '106-0044',
        phoneNumber: 'XXX-XXXX-1234',
        rank: 'Gold',
        holdingPoints: 100,
        pointExpire: '2017/02/28',
        buyTimes: 10,
        lastShop: 'shop name',
        orders: [
          {
            id: 1,
            title: 'item1',
            total: 60000,
            date: '2017-03-24 23:00:03',
            used_points: 100,
            obtained_points: 200,
            ordered_shop: 'Shop 1',
            products: [
              { id: 1, name: 'Product 1', price: 40000, quantity: 5 },
              { id: 2, name: 'Product 2', price: 10000, quantity: 10 },
              { id: 3, name: 'Product 3', price: 10000, quantity: 7 }
            ]
          },
          {
            id: 2,
            title: 'item2',
            total: 66000,
            date: '2017-03-25 23:00:03',
            used_points: 500,
            obtained_points: 500,
            ordered_shop: 'Shop 1',
            products: [
              { id: 1, name: 'Product 1', price: 40000, quantity: 5 },
              { id: 2, name: 'Product 2', price: 10000, quantity: 10 },
              { id: 3, name: 'Product 3', price: 10000, quantity: 7 }
            ]
          }
        ]
      },
      {
        id: 3,
        name: 'ヤマダタロウ様',
        nameKana: 'name kana 3',
        code: 1234567890123,
        status: 'unknowmember',
        tel: '0909000000',
        possessedPoint: 2,
        buyTime: 3,
        lastBuyDate: '2017/02/28',
        registeredShop: 'shop name',
        phonetic: 'phonetic',
        postalCode: '106-0044',
        phoneNumber: 'XXX-XXXX-1234',
        rank: 'Gold',
        holdingPoints: 100,
        pointExpire: '2017/02/28',
        buyTimes: 10,
        lastShop: 'shop name',
        orders: []
      },
      {
        id: 2,
        name: 'ヤマダタロウ様',
        nameKana: 'name kana 2',
        code: 1234567890123,
        status: 'unknowmember',
        tel: '0908888888',
        possessedPoint: 1,
        buyTime: 2,
        lastBuyDate: '2017/02/28',
        registeredShop: 'shop name',
        phonetic: 'phonetic',
        postalCode: '106-0044',
        phoneNumber: 'XXX-XXXX-1234',
        rank: 'Gold',
        holdingPoints: 100,
        pointExpire: '2017/02/28',
        buyTimes: 10,
        lastShop: 'shop name',
        orders: []
      }
    ]

    return (
      <Grid style={commonStyles.basicLayoutRootStyle}>
        <Row style={{ height: 72 }}>
          <Header
            bottomButtons={[
              {
                icon: 'md-person',
                type: 'ionic',
                text: I18n.t('customer.staff'),
                color: '#fff',
                iconSize: 24,
                containerStyle: {
                  backgroundColor: 'transparent',
                  marginLeft: 10
                },
                onPress: () => {}
              }
            ]}
          />
        </Row>
        <Row size={0.05} />
        <Row>
          <Col>
            <CustomerSearchForm
              data={data}
              onItemChoose={this._onCustomerChoose}
              onItemUnChoose={this._onCustomerUnChoose}
            />
          </Col>
          <Col>
            <CustomerSearchResult
              data={data}
              onItemChoose={customer => {
                this._onItemChoose(customer)
              }}
            />
          </Col>
        </Row>
        <Row style={{ height: 50, width: '100%' }}>
          <Footer
            rightButtons={[
              {
                icon: 'account-plus',
                type: 'material',
                text: I18n.t('customer.title'),
                color: '#fff',
                iconSize: 24,
                containerStyle: {
                  backgroundColor: 'transparent',
                  marginLeft: 10
                },
                onPress: () => {}
              }
            ]}
            leftButtons={[
              {
                icon: 'cart-outline',
                type: 'material',
                text: I18n.t('customer.pend_order'),
                color: '#fff',
                iconSize: 24,
                containerStyle: {
                  backgroundColor: 'transparent',
                  marginLeft: 10
                },
                onPress: () => {}
              }
            ]}
          />
        </Row>
      </Grid>
    )
  }
}
