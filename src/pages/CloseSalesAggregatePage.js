import React, { Component } from 'react'
import { connect } from 'react-redux'
import BasePage from 'common/hocs/BasePage'
import ReceiptView from 'modules/printer/components/ReceiptView'
import SalesAggregateForm from 'modules/cashierTotal/containers/SalesAggregateForm'
import { MODULE_NAME } from 'modules/cashierTotal/models'
import * as actions from 'modules/cashierTotal/actions'
import { PERMISSION_CODES } from 'modules/staff/models'
import I18n from 'i18n-js'

class CloseSalesAggregatePage extends Component {
  componentWillUnmount () {
    this.props.dispatch(actions.setReceipt(null))
  }

  render () {
    return (
      <BasePage headerProps={{
        permissionCode: PERMISSION_CODES.SUMMARY_CLOSE_SALES,
        pageName: I18n.t('page.sales_aggregate'),
        showOpenDrawer: true
      }}>
        <ReceiptView receiptDataText={this.props.receiptText} />
        <SalesAggregateForm />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  receiptText: state[MODULE_NAME].receiptText
})

export default connect(mapStateToProps, mapDispatchToProps)(CloseSalesAggregatePage)
