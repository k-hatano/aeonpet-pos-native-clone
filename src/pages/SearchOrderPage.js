import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import TabPanel from '../common/components/widgets/TabPanel'
import ImageButton from '../common/components/elements/ImageButton'
import SearchOrderForm from '../modules/order/containers/SearchOrderForm'
import OrderListView from '../modules/order/containers/OrderListView'
import OrderSearchResultDetailView from '../modules/order/containers/OrderSearchResultDetailView'
import OrderView from '../modules/order/components/OrderView'
import OrderHistoryActionsPanel from '../modules/order/containers/OrderHistoryActionsPanel'
import componentStyles from '../modules/order/styles/Page'
import { selectOrder, setReturnedOrder } from '../modules/order/actions'
import { MODULE_NAME, STATUS, IS_MINUS, orderTabTitles } from '../modules/order/models'
import PropTypes from 'prop-types'
import BasePage from 'common/hocs/BasePage'
import { PERMISSION_CODES } from 'modules/staff/models'

class SearchOrderPage extends Component {
  static propTypes = {
    selectedOrder: PropTypes.object
  }

  constructor (props) {
    super(props)
    this.state = {
      isDetail: false,
      isSearchOrder: true
    }
  }

  componentWillUnmount () {
    this._onToggleOrderDetail()
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.selectedOrder && nextProps.selectedOrder) {
      this.setState({ isDetail: true })
    }

    if (this.props.selectedOrder && !nextProps.selectedOrder) {
      this.setState({ isDetail: false })
    }
  }

  _onToggleOrderDetail () {
    this.setState({ isDetail: false })
    this.props.dispatch(selectOrder(null))
    this.props.dispatch(setReturnedOrder(null))
  }

  _renderSearchResult = () => {
    return (
      <View style={componentStyles.orderList}>
        <View style={componentStyles.listHeader}>
          <Text style={componentStyles.headerText}>
            {' '}{I18n.t('common.search_result')}{' '}
          </Text>
        </View>
        <OrderListView />
      </View>
    )
  }

  _renderOrderDetail = () => {
    const { selectedOrder, returnedOrder } = this.props
    const isMinus = selectedOrder.is_minus

    const tabTitles = orderTabTitles(selectedOrder)
    return (
      <View style={componentStyles.orderDetail}>
        <View style={componentStyles.orderDetailContainer}>
          <View style={componentStyles.backButtonContainer}>
            <ImageButton
              style={componentStyles.backButton}
              toggle
              appearance={{
                normal: require('../assets/images/header/chevron_left.png'),
                highlight: require('../assets/images/header/chevron_left.png')
              }}
              onPress={() => this._onToggleOrderDetail()}
            />
          </View>
          <View style={componentStyles.headerContainer}>
            <Text style={componentStyles.headerText}> {I18n.t('home.detail')} </Text>
          </View>
        </View>
        <View style={componentStyles.tabContainer}>
          <TabPanel tabWrapperStyle={componentStyles.tabWrapperStyle} tabStyle={{width: 136}}>
            <View tabName={tabTitles.total}>
              <OrderView
                selectedOrder={selectedOrder}
                returnedOrder={null} />
            </View>
            <View tabName={tabTitles.detail}>
              <OrderSearchResultDetailView />
            </View>
            {returnedOrder && isMinus === IS_MINUS.ORDER &&
            <View
              tabName={I18n.t('order.return_total')}
              tabNameStyle={{color: 'red'}}>
              <OrderView
                selectedOrder={selectedOrder}
                returnedOrder={returnedOrder} />
            </View>}
            {selectedOrder.status === STATUS.RETURNED &&
            <View
              tabName={I18n.t('order.return_detail')}
              tabNameStyle={{color: 'red'}}>
              <OrderSearchResultDetailView />
            </View>}
          </TabPanel>
          <View style={componentStyles.orderHistoryContainer}>
            <OrderHistoryActionsPanel moduleName={MODULE_NAME} />
          </View>
        </View>
      </View>
    )
  }

  render () {
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.order_list'),
        permissionCode: PERMISSION_CODES.SEARCH_ORDER,
        onChangeStaffModalShowing: (isSearchOrder) => {
          this.setState({isSearchOrder: isSearchOrder})
        }
      }}>
        <SearchOrderForm
          isSearchOrder={this.state.isSearchOrder} />
        {this.state.isDetail ? this._renderOrderDetail() : this._renderSearchResult()}
      </BasePage>
    )
  }
}

const mapStateToProps = state => ({
  selectedOrder: state[MODULE_NAME].selectedOrder,
  returnedOrder: state[MODULE_NAME].returnedOrder
})

export default connect(mapStateToProps)(SearchOrderPage)
