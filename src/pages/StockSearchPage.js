import React, { Component } from 'react'
import I18n from 'i18n-js'
import { View } from 'react-native'
import BasePage from '../common/hocs/BasePage'
import { refreshForStockSearch } from '../modules/stock/models'
import StockSearchResultView from '../modules/stock/containers/StockSearchResultView'
import StockSearchProductForm from '../modules/stock/containers/StockSearchProductForm'
import { initForCategoryProductView } from '../modules/cart/services'
import styles from '../modules/stock/styles/StockSearchPage'

export default class StockSearchPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isSearchProduct: true
    }
  }

  async componentWillMount () {
    await initForCategoryProductView(this.props.dispatch)

    if (this.props.init) {
      refreshForStockSearch(this.props.dispatch)
    }
  }

  async componentWillUnmount () {
    await initForCategoryProductView(this.props.dispatch)
  }

  render () {
    return (
      <BasePage
        headerProps={{
          onChangeStaffModalShowing: (isSearchProduct) => {
            this.setState({isSearchProduct: isSearchProduct})
          },
          pageName: I18n.t('home.stocks')
        }}>
        <View style={styles.container}>
          <StockSearchResultView />
        </View>
        <View style={styles.container}>
          <StockSearchProductForm
            isSearchProduct={this.state.isSearchProduct} />
        </View>
      </BasePage>
    )
  }
}
