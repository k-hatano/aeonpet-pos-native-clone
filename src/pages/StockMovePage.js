import React, { Component } from 'react'
import { View } from 'react-native'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import TabPanel from '../common/components/widgets/TabPanel'
import BasePage from '../common/hocs/BasePage'
import { CommandButton } from '../common/components/elements/StandardButton'
import AlertView from '../common/components/widgets/AlertView'
import ConfirmView from '../common/components/widgets/ConfirmView'
import { initForCategoryProductView } from '../modules/cart/services'
import * as productActions from '../modules/product/actions'
import CategoryProductView from '../modules/product/containers/CategoryProductView'
import { PREV_SEARCH_COND } from '../modules/product/models'
import { loading } from '../common/sideEffects'
import ProductRepository from '../modules/product/repositories/ProductRepository'
import * as actions from '../modules/stockMove/actions'
import styles from '../modules/stockMove/styles/StockMoveForm'
import StockMoveForm from '../modules/stockMove/containers/StockMoveForm'
import StockMoveFormList from '../modules/stockMove/containers/StockMoveFormList'
import {
  MODULE_NAME,
  MAX_ITEM_QUANTITY,
  initForStockMovement,
  canCompleteStockMovement,
  getSettingsFromState, validateCreateInputs
} from '../modules/stockMove/models'
import { pushStockMovement } from '../modules/stockMove/services'
import { registerBarcodeReaderEvent } from '../modules/barcodeReader/services'
import AmountPadModal from '../common/components/elements/AmountPadModal'
import { AMOUNT_PAD_MODE } from '../common/components/elements/AmountPad'
import Modal from '../common/components/widgets/Modal'

class StockMovePage extends Component {
  constructor () {
    super()
    this.state = {
      isSearchProduct: true
    }
  }

  async componentDidMount () {
    this.eventBarcodeReader = registerBarcodeReaderEvent(async body => {
      if (!this.state.isSearchProduct) return

      const result = await this.props.onSearchForBarcodeReader(body.trim(), true)

      if (result && result.length === 1) {
        this._openNumberPad(result[0], this.props.products)
      }
    })
  }

  async componentWillMount () {
    await initForStockMovement(this.props.dispatch)
    await initForCategoryProductView(this.props.dispatch)
  }

  async componentWillUnmount () {
    this.eventBarcodeReader.remove()

    await initForStockMovement(this.props.dispatch)
    await initForCategoryProductView(this.props.dispatch)
  }

  _openNumberPad (product) {
    Modal.close()
    AmountPadModal.open({
      mode: AMOUNT_PAD_MODE.NUMBER,
      title: product.name,
      maxValue: MAX_ITEM_QUANTITY,
      onComplete: quantity => {
        Modal.close()
        this.props.onProductSelected(product, this.props.createInputs.products, quantity)
      }
    })
  }

  render () {
    const {canComplete, onComplete, selectedTab} = this.props
    return (
      <BasePage
        headerProps={{
          hideBackButton: false,
          canBack: true,
          beforeGoHome: () => ConfirmView.showAsync(I18n.t('message.L-02-I003')),
          onBack: () => this.props.onBack(),
          onChangeStaffModalShowing: (isSearchProduct) => {
            this.setState({isSearchProduct: isSearchProduct})
          },
          pageName: I18n.t('stock_search.stock_out')
        }}>
        <View style={styles.listContainer}>
          <StockMoveFormList />
        </View>
        <View style={styles.tabContainer}>
          <TabPanel tabStyle={styles.tabStyle} selectedTab={selectedTab}>
            <View
              tabName={I18n.t('stock_search.order_number')}
              onTabClick={this.props.onTabClick}
              style={styles.tabContentArea}>
              <StockMoveForm />
            </View>
            <View
              tabName={I18n.t('stock_search.add_product')}
              onTabClick={this.props.onTabClick}
              style={styles.tabContentArea}>
              <View style={styles.categoryProductContainer}>
                <CategoryProductView
                  onProductSelected={product => this._openNumberPad(product)}
                  onShowAmountPad={product => this._openNumberPad(product)}
                  style={styles.categoryProduct}
                  showStockConditionFlg={false} excludeSetProduct
                />
              </View>
            </View>
          </TabPanel>
          <View style={{ justifyContent: 'flex-end' }}>
            <CommandButton
              style={styles.movementButton}
              text={I18n.t('stock_search.stock_out')}
              disabled={!canComplete}
              onPress={() => onComplete(this.props.createInputs, this.props.currentSettings)}
            />
          </View>
        </View>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onSearchForBarcodeReader: async (barcode) => {
    const products = await loading(dispatch, async () => {
      let products = await ProductRepository.findProductsBySearchWordForStocks(barcode, true, true)
      if (products.length > 100) {
        products = products.slice(0, 100)

        AlertView.show(I18n.t('common.over_hundred_items'))
      }
      switch (products.length) {
        case 0:
          let productVariantsByWord = await ProductRepository.findVariantsBySearchWordForStocks(barcode, true)
          if (productVariantsByWord.length > 100) {
            productVariantsByWord = productVariantsByWord.slice(0, 100)

            AlertView.show(I18n.t('common.over_hundred_items'))
          }
          switch (productVariantsByWord.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              const productDetail = await ProductRepository.findWithDetail(productVariantsByWord[0].id)
              products.push(productDetail)
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              dispatch(actions.setSelectedTab(1))
              dispatch(productActions.listProducts(productVariantsByWord))
              dispatch(productActions.listOriginProducts(productVariantsByWord))
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
              dispatch(productActions.setSearchLayoutFlg(true))
              break
          }
          break

        case 1:
          const productVariantsById = await ProductRepository.findVariantsByProductId(products[0].id)
          switch (productVariantsById.length) {
            case 0:
              AlertView.show(I18n.t('product.not_exist'))
              break
            case 1:
              dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.EMPTY))
              break
            default:
              break
          }
          break

        default:
          dispatch(actions.setSelectedTab(1))
          dispatch(productActions.listProducts(products))
          dispatch(productActions.listOriginProducts(products))
          dispatch(productActions.setPrevSearchCond(PREV_SEARCH_COND.SEARCH_BY_WORD))
          dispatch(productActions.setSearchLayoutFlg(true))
          break
      }
      return products
    })
    return products
  },
  onProductSelected: async (product, products, quantity = 1) => {
    // 数量が0の場合は何も選択されなかったと判断する。
    if (quantity === 0) return

    const productIndex = products.findIndex(p => p.id === product.id)
    const stockItem = product.product_variants[0].stock_item
    const stockQuantity = (stockItem && stockItem.quantity) || 0

    if (productIndex < 0) {
      if (Math.abs(stockQuantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.N-02-E003', {quantity: MAX_ITEM_QUANTITY}))
      } else {
        product.quantity = quantity
        dispatch(actions.setCreateInputValue({
          value: Object.assign([], [...products, product]), propName: 'products'
        }))
      }
    } else {
      if (products[productIndex].quantity + quantity > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.N-02-E004', {quantity: MAX_ITEM_QUANTITY}))
      } else if (Math.abs(stockQuantity - products[productIndex].quantity - quantity) > MAX_ITEM_QUANTITY) {
        AlertView.show(I18n.t('message.N-02-E003', {quantity: MAX_ITEM_QUANTITY}))
      } else {
        products[productIndex].quantity += quantity
        dispatch(actions.setCreateInputValue({value: [...products], propName: 'products'}))
      }
    }
  },
  onComplete: async (createInputs, currentSettings) => {
    if (!(await validateCreateInputs(createInputs))) return

    await pushStockMovement(dispatch, createInputs, currentSettings)
  },
  onBack: () => {
    ConfirmView.show(I18n.t('message.L-02-I003'), () => {
      Actions.stockMoveOutPage()
    })
  },
  onTabClick: (tabIndex) => {
    dispatch(actions.setSelectedTab(tabIndex))
  }
})

const mapStateToProps = state => ({
  createInputs: state[MODULE_NAME].createInputs,
  currentSettings: getSettingsFromState(state),
  canComplete: canCompleteStockMovement(state[MODULE_NAME].createInputs),
  selectedTab: state[MODULE_NAME].selectedTab
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMovePage)
