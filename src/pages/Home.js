import React, { Component } from 'react'
import { View, StatusBar, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import { Col, Row, Grid } from 'react-native-easy-grid'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
import OldModal from '../common/components/widgets/OldModal'
import AlertView from '../common/components/widgets/AlertView'
import OrderNotification from '../common/components/widgets/OrderNotification'
import Header from '../modules/home/components/Header'
import ProgressBar from '../common/components/widgets/ProgressBar'
import MainButtons from '../modules/home/components/MainButtons'
import MainMenus from '../modules/home/components/MainMenus'
import ListOrder from '../modules/home/components/ListOrder'
import ReprintExistingTaxFree from '../modules/taxFree/components/ReprintExistingTaxFree'
import NotificationDetail from '../modules/notification/components/NotificationDetail'
import commonStyles from '../common/styles/layout/Page'
import componentStyles from '../modules/home/styles/Home'
import BalanceReceiptBuilder from '../modules/balance/models/BalanceReceiptBuilder'
import PrinterManager from '../modules/printer/models/PrinterManager'
import { loading } from '../common/sideEffects'
import BasePage from '../common/hocs/BasePage'
import StaffSettings from '../common/settings/StaffSettings'

export default class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      openNotify: false,
      isOpenPrintErrors: false,
      isOpenReceiptDetail: false,
      selectedReceipt: null,
      buttons: {
        buttonBig: {
          title: I18n.t('home.new_order'),
          icon: require('../assets/images/homepage/cart.png'),
          width: 60,
          height: 51,
          buttonWrapperStyle: componentStyles.buttonBigWrapperStyle,
          buttonStyle: componentStyles.buttonBigButtonStyle,
          buttonTitleStyle: componentStyles.buttonBigTitleStyle,
          permissionCode: '2',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.newOrder() } }
        },
        buttonAbove: {
          title: I18n.t('home.check_sales'),
          icon: require('../assets/images/homepage/inspect.png'),
          width: 36,
          height: 29,
          buttonWrapperStyle: componentStyles.buttonAboveWrapperStyle,
          buttonStyle: componentStyles.buttonAboveButtonStyle,
          buttonTitleStyle: componentStyles.buttonAboveTitleStyle,
          permissionCode: '7',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.cashierRecord({destination: 'check'}) } }
        },
        buttonBelow: {
          title: I18n.t('home.close_sales'),
          icon: require('../assets/images/homepage/cart.png'),
          width: 36,
          height: 29,
          buttonWrapperStyle: componentStyles.buttonBelowWrapperStyle,
          buttonStyle: componentStyles.buttonBelowButtonStyle,
          buttonTitleStyle: componentStyles.buttonBelowTitleStyle,
          permissionCode: '8',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.cashierRecord({destination: 'close'}) } }
        }
      },
      topMenus: [
        {
          id: '1',
          title: I18n.t('home.settings'),
          icon: 'cog',
          permissionCode: '11',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.setting() } }
        },
        {
          id: '2',
          title: I18n.t('home.sync_data'),
          icon: 'refresh',
          permissionCode: '11',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.syncData() } }
        }
      ],
      menus: [
        {
          id: '1',
          title: I18n.t('home.balance_cache'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/currency_jpy.png'),
          firstIconStyle: { width: 21, height: 29 },
          secondIcon: require('../assets/images/homepage/transfer.png'),
          secondIconStyle: { width: 20, height: 15, marginLeft: 10 },
          permissionCode: '',
          onPress: () => Actions.balances()
        },
        {
          id: '2',
          title: I18n.t('home.orders_search'),
          iconsStyle: {
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            width: 64,
            height: 64
          },
          firstIcon: require('../assets/images/homepage/bag.png'),
          firstIconStyle: { width: 37, height: 32, marginTop: 9, marginLeft: 5 },
          secondIcon: require('../assets/images/homepage/search.png'),
          secondIconStyle: { width: 19, height: 19, position: 'absolute', top: 37, left: 40 },
          permissionCode: '',
          onPress: () => Actions.orderSearchPage()
        },
        {
          id: '3',
          title: I18n.t('home.member_search'),
          iconsStyle: {
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            width: 64,
            height: 64
          },
          firstIcon: require('../assets/images/homepage/user.png'),
          firstIconStyle: { width: 23, height: 28, marginTop: 11, marginLeft: 11 },
          secondIcon: require('../assets/images/homepage/search.png'),
          secondIconStyle: { width: 19, height: 19, position: 'absolute', top: 37, left: 40 },
          permissionCode: '12',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.customer() } }
        },
        {
          id: '4',
          title: I18n.t('home.return'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/return.png'),
          firstIconStyle: { width: 36, height: 33 },
          permissionCode: '4',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.returnOrder() } }
        },
        {
          id: '5',
          title: I18n.t('home.stocks'),
          iconsStyle: {
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            width: 64,
            height: 64
          },
          firstIcon: require('../assets/images/homepage/tag.png'),
          firstIconStyle: { width: 31, height: 31, marginTop: 14, marginLeft: 13 },
          secondIcon: require('../assets/images/homepage/search.png'),
          secondIconStyle: { width: 19, height: 19, position: 'absolute', top: 37, left: 40 },
          permissionCode: '13',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.stockSearch() } }
        },
        {
          id: '6',
          title: I18n.t('home.stock_taking'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/checkbox.png'),
          firstIconStyle: { width: 31, height: 31 },
          permissionCode: '15',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) { Actions.stockTakingPage() } }
        },
        {
          id: '7',
          title: I18n.t('home.in_stocks'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/box.png'),
          firstIconStyle: { width: 30, height: 28 },
          secondIcon: require('../assets/images/homepage/arrow_left.png'),
          secondIconStyle: { width: 20, height: 21, marginLeft: 4 },
          permissionCode: '16',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) {} }
        },
        {
          id: '8',
          title: I18n.t('home.out_stocks'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/box.png'),
          firstIconStyle: { width: 30, height: 28 },
          secondIcon: require('../assets/images/homepage/arrow_right.png'),
          secondIconStyle: { width: 20, height: 21, marginLeft: 4 },
          permissionCode: '17',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) {} }
        },
        {
          id: '9',
          title: I18n.t('home.adjust_stocks'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/tags.png'),
          firstIconStyle: { width: 39, height: 31 },
          permissionCode: '15',
          onPress: (permissionCode) => { if (this._onPermissionCheck(permissionCode)) {} }
        },
        {
          id: '10',
          title: 'test',
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/tags.png'),
          firstIconStyle: { width: 39, height: 31 },
          permissionCode: '21',
          onPress: () => Actions.testPage()
        },
        {
          id: '11',
          title: 'Debug',
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/tags.png'),
          firstIconStyle: { width: 39, height: 31 },
          permissionCode: '22',
          onPress: () => Actions.debug()
        },
        {
          id: '12',
          title: 'SalesAggregate',
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/inspect.png'),
          firstIconStyle: { width: 39, height: 31 },
          permissionCode: '22',
          onPress: () => Actions.closeSalesAggregate()
        },
        {
          id: '13',
          title: I18n.t('home.help'),
          iconsStyle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 64, height: 64 },
          firstIcon: require('../assets/images/homepage/inspect.png'),
          firstIconStyle: { width: 39, height: 31 },
          permissionCode: '',
          onPress: () => Actions.helpPage()
        }
      ]
    }
  }

  _onTogglePrintErrors = isOpen => {
    const selectedReceipt = isOpen === false ? null : this.state.selectedReceipt

    this.setState({
      selectedReceipt,
      isOpenPrintErrors: isOpen
    })
  }

  _openReceipt = item => {
    this.setState({
      selectedReceipt: item
    })
  }

  _onPermissionCheck (permissionCode) {
    const permissions = StaffSettings.rolePermissions

    if (permissions && permissions.find(permission => (permission.permission_code === permissionCode))) {
      return true
    } else {
      AlertView.show(I18n.t('message.A-01-E001'), null)
      return false
    }
  }

  componentDidUpdate () {
    const { isPrintTaxFree } = this.props
    if (isPrintTaxFree) {
      OldModal.open('reprintExistingTaxFree')
    }
  }

  componentDidMount () {
    const { isPrintTaxFree } = this.props
    if (isPrintTaxFree) {
      OldModal.open('reprintExistingTaxFree')
    }
  }

  render () {
    const { buttons, menus, topMenus, isOpenPrintErrors, selectedReceipt } = this.state

    return (
      <BasePage showHeader={false}>
        <Grid style={commonStyles.basicLayoutRootStyle}>
          <StatusBar hidden />
          <Row style={rowStyle.rowHeader}>
            <Header onTogglePrintErrors={isOpen => this._onTogglePrintErrors(isOpen)} />
          </Row>
          <Row>
            {!selectedReceipt &&
            <Col size={10}>
              <Row size={0.05} style={rowStyle.rowTopMenus}>
                <View style={commonStyles.leftPanel}>
                  {topMenus.map((item, index) =>
                    <View key={index}>
                      <TouchableOpacity onPress={() => item.onPress(item.permissionCode)} style={{ marginRight: 32 }}>
                        <Icon name={item.icon} size={38} color='#93979A' />
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              </Row>
              <Row size={0.35} style={rowStyle.rowMainButtonsStyle}>
                <MainButtons mStyle={componentStyles.mainButtons} buttons={buttons} />
              </Row>
              <Row size={0.6} style={rowStyle.rowMainMenusStyle}>
                <MainMenus menus={menus} componentStyles={componentStyles.mainMenus} />
              </Row>
            </Col>}
            {selectedReceipt &&
            <Col size={10}>
              <NotificationDetail item={selectedReceipt} />
            </Col>}
            <Col size={11}>
              {!isOpenPrintErrors && <ListOrder mainStyles={componentStyles.listOrder} />}
              {isOpenPrintErrors && <OrderNotification getReceipt={item => this._openReceipt(item)} />}
            </Col>
          </Row>
          <ReprintExistingTaxFree ref={'reprintExistingTaxFree'} />
          <ProgressBar.Component global />
        </Grid>
      </BasePage>
    )
  }
}

const rowStyle = {
  rowHeader: {
    height: 72,
    zIndex: 1
  },
  rowTopMenus: {
    paddingTop: 37,
    paddingLeft: 39,
    paddingBottom: 20,
    alignItems: 'flex-start'
  },
  rowMainButtonsStyle: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowMainMenusStyle: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 10
  }
}
