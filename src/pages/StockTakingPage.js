import React, { Component } from 'react'
import StockTakingList from '../modules/stockTaking/containers/StockTakingList'
import StockTakingView from '../modules/stockTaking/containers/StockTakingView'
import StockTakingTaskItemList from '../modules/stockTaking/containers/StockTakingTaskItemList'
import { connect } from 'react-redux'
import BasePage from 'common/hocs/BasePage'
import {
  MODULE_NAME,
  initStockTakings,
  clearStockTakings
} from 'modules/stockTaking/models'
import { PERMISSION_CODES } from 'modules/staff/models'
import I18n from 'i18n-js'

class StockTakingPage extends Component {
  async componentDidMount() {
    initStockTakings(this.props.dispatch)
  }

  async componentWillMount() {
    clearStockTakings(this.props.dispatch)
  }

  _renderRight() {
    if (
      this.props.stockTakingTaskItems &&
      this.props.stockTakingTaskItems.length > 0
    ) {
      return <StockTakingTaskItemList />
    } else {
      return <StockTakingView />
    }
  }

  render() {
    return (
      <BasePage headerProps={{
        permissionCode: PERMISSION_CODES.STOCK_TAKING,
        pageName: I18n.t('page.stock_taking')
      }} >
          <StockTakingList />
          {this._renderRight()}
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  stockTakings: state[MODULE_NAME].stockTakings,
  stockTakingTaskItems: state[MODULE_NAME].stockTakingTaskItems
})

export default connect(mapStateToProps, mapDispatchToProps)(StockTakingPage)
