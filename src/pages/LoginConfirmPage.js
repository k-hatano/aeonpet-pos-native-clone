import React, { Component } from 'react'
import BasePage from '../common/hocs/BasePage'
import LoginConfirmView from '../modules/login/containers/LoginConfirmView'

export default class LoginConfirmPage extends Component {
  render () {
    return (
      <BasePage showHeader={false}>
        <LoginConfirmView />
      </BasePage>
    )
  }
}
