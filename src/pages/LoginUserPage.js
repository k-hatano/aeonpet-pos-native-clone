import React, { Component } from 'react'
import BasePage from '../common/hocs/BasePage'
import LoginUserForm from '../modules/login/containers/LoginUserForm'

export default class LoginUserPage extends Component {
  render () {
    return (
      <BasePage showHeader={false}>
        <LoginUserForm />
      </BasePage>
    )
  }
}
