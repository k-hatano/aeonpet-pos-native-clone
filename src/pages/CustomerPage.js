'use strict'

import React, { Component } from 'react'
import CustomerSearchFormContainer from '../modules/customer/containers/CustomerSearchFormContainer'
import CustomerOrderViewContainer from '../modules/customer/containers/CustomerOrderViewContainer'
import BasePage from 'common/hocs/BasePage'
import I18n from 'i18n-js'
import * as actions from '../modules/customer/actions'
import { PERMISSION_CODES } from 'modules/staff/models'
import { connect } from 'react-redux'
import { MODULE_NAME } from 'modules/customer/models'
import ConfirmView from 'common/components/widgets/ConfirmView'

class CustomerPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isStaffModalShowing: true
    }
  }

  componentWillUnmount () {
    this.props.dispatch(actions.listCustomers([]))
    this.props.dispatch(actions.listCustomerOrders([]))
    this.props.dispatch(actions.setIsEditable(false))
  }

  render () {
    const headerProps = {
      hideBackButton: false,
      showPrinterErrorButton: false,
      pageName: I18n.t("page.customer"),
      permissionCode: PERMISSION_CODES.SEARCH_CUSTOMER,
      onChangeStaffModalShowing: (isShowing) => {
        this.setState({isStaffModalShowing: isShowing})
      }
    }
    if (this.props.isEditable) {
      headerProps.beforeGoHome = () => ConfirmView.showAsync(I18n.t('message.C-03-I001'))
    }
    return (
      <BasePage headerProps={headerProps}>
        <CustomerSearchFormContainer isStaffModalShowing={this.state.isStaffModalShowing} />
        <CustomerOrderViewContainer />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  isEditable: state[MODULE_NAME].isEditable
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerPage)
