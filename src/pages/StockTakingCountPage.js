import React, { Component } from 'react'
import I18n from 'i18n-js'
import StockTakingCountView from '../modules/stockTaking/containers/StockTakingCountView'
import StockTakingSearchProductForm from '../modules/stockTaking/containers/StockTakingSearchProductForm'
import { connect } from 'react-redux'
import BasePage from 'common/hocs/BasePage'
import { MODULE_NAME } from 'modules/stockTaking/models'
import { PERMISSION_CODES } from 'modules/staff/models'
import ConfirmView from '../common/components/widgets/ConfirmView'
import { initForCategoryProductView } from 'modules/cart/services'

class StockTakingCountPage extends Component {
  async componentWillMount () {
    initForCategoryProductView(this.props.dispatch, true)
  }

  /**
   *
   * @return {Promise.<boolean>}
   * @private
   */
  async _beforeGoHome () {
    return ConfirmView.showAsync(I18n.t('message.O-04-I002'))
  }

  render () {
    return (
      <BasePage headerProps={{
        hideHomeButton: true,
        beforeGoHome: () => this._beforeGoHome(),
        permissionCode: PERMISSION_CODES.STOCK_TAKING,
        pageName: I18n.t('page.stock_taking'),
        showOpenDrawer: true
      }}>
        <StockTakingCountView />
        <StockTakingSearchProductForm />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  products: state[MODULE_NAME].products,
  stockTakingTaskItems: state[MODULE_NAME].stockTakingTaskItems
})

export default connect(mapStateToProps, mapDispatchToProps)(
  StockTakingCountPage
)
