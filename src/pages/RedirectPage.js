import React, { Component } from 'react'
import PropTypes from 'prop-types'
import BasePage from 'common/hocs/BasePage'
import { Actions } from 'react-native-router-flux'

export default class RedirectPage extends Component {
  static propTypes = {
    redirectTo: PropTypes.string
  }

  componentWillMount () {
    switch (this.props.redirectTo) {
      case 'home':
        Actions.home()
        break
      case 'cart':
        Actions.cartPage({ mode: 'order' })
        break
      default:
        break
    }
  }

  render () {
    return (
      <BasePage headerProps={{hideBackButton: false, showPrinterErrorButton: false, pageName: ''}} />
    )
  }
}
