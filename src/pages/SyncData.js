import React, { Component } from 'react'
import { connect } from 'react-redux'
import SyncSettingViewContainer from '../modules/syncData/containers/SyncSettingView'
import SyncResultViewContainer from '../modules/syncData/containers/SyncResultView'
import commonStyles from '../common/styles/layout/Page'
import pageStyles from '../modules/syncData/styles/SyncData'
import BasePage from 'common/hocs/BasePage'
import I18n from 'i18n-js'
import { PERMISSION_CODES } from 'modules/staff/models'
import PropTypes from 'prop-types'

class SyncData extends Component {
  static propTypes = {
    isImmediateExec: PropTypes.bool,
    canBack: PropTypes.bool,
    hideBackButton: PropTypes.bool
  }

  render () {
    const { isImmediateExec, canBack, hideBackButton } = this.props
    return (
      <BasePage headerProps={{
        hideBackButton: !!hideBackButton,
        showPrinterErrorButton: false,
        pageName: I18n.t("page.sync_data"),
        permissionCode: PERMISSION_CODES.SYNC_DATA,
        canBack: !!canBack}} >
        <SyncSettingViewContainer
          isImmediateExec={isImmediateExec} />
        <SyncResultViewContainer 
          isFromOpenSale={!!hideBackButton}/>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(SyncData)
