import I18n from 'i18n-js'
import React, { Component } from 'react'
import BasePage from 'common/hocs/BasePage'
import CloseSalesView from 'modules/cashierTotal/containers/CloseSalesView'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class CloseSalesPage extends Component {
  render () {
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.close_sales'),
        permissionCode: PERMISSION_CODES.CLOSE_SALES,
        showOpenDrawer: true}}>
        <CloseSalesView />
      </BasePage>
    )
  }
}
