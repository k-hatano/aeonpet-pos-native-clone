import React, { Component } from 'react'
import BasePage from '../common/hocs/BasePage'
import LoginSelectCashierForm from '../modules/login/containers/LoginSelectCashierForm'

export default class LoginSelectCashierPage extends Component {
  render () {
    return (
      <BasePage showHeader={false}>
        <LoginSelectCashierForm />
      </BasePage>
    )
  }
}
