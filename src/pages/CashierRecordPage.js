import React, { Component } from 'react'
import CashierRecordForm from 'modules/cashierTotal/containers/CashierRecordForm'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import BasePage from 'common/hocs/BasePage'
import PropTypes from 'prop-types'
import { MODULE_NAME } from 'modules/openSale/models'
import { setCashierRecordData } from 'modules/cashierTotal/actions'
import I18n from 'i18n-js'
import { CASHIER_RECORD_MODE } from 'modules/cashierTotal/models'

class CashierRecordPage extends Component {
  static propTypes = {
    hideHomeButton: PropTypes.bool,
    isOpening: PropTypes.bool,
    cashierRecordMode: PropTypes.number,
    onCompleteConfirm: PropTypes.func,
    onCashierRecordUpdated: PropTypes.func,
    originCashierRecordData: PropTypes.object
  }
  static defaultProps = {
    hideHomeButton: false
  }

  _pageName () {
    const { cashierRecordMode } = this.props
    switch (cashierRecordMode) {
      case CASHIER_RECORD_MODE.CHECK_SALES:
        return I18n.t('page.cashier_record_check')
      case CASHIER_RECORD_MODE.CLOSE_SALES:
        return I18n.t('page.cashier_record_close')
      case CASHIER_RECORD_MODE.OPEN_SALES:
        return I18n.t('page.cashier_record_open')
      default:
        return I18n.t('page.cashier_record')
    }
  }

  render () {
    const pageName = this._pageName()
    return (
      <BasePage headerProps={{
        hideBackButton: this.props.hideHomeButton,
        canBack: this.props.isOpening,
        onBack: () => this.props.onBack(this.props.onCashierRecordUpdated, this.props.isOpening, this.props.originCashierRecordData),
        showPrinterErrorButton: false,
        permissionCode: this.props.permissionCode,
        pageName: pageName,
        showOpenDrawer: true}}>
        <CashierRecordForm
          isOpening={this.props.isOpening}
          cashierRecordMode={this.props.cashierRecordMode}
          onCompleteConfirm={this.props.onCompleteConfirm}
          onCashierRecordUpdated={this.props.onCashierRecordUpdated}
        />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onBack: async (onCashierRecordUpdated, isOpening, originCashierRecordData) => {
    if (onCashierRecordUpdated != null) onCashierRecordUpdated(dispatch)
    if (isOpening) {
      dispatch(setCashierRecordData(JSON.parse(JSON.stringify(originCashierRecordData))))
    }
    Actions.pop()
  }
})

const mapStateToProps = state => ({
  originCashierRecordData: state[MODULE_NAME].originCashierRecordData
})

export default connect(mapStateToProps, mapDispatchToProps)(CashierRecordPage)
