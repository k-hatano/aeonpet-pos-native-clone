import React, { Component } from 'react'
import BasePage from 'common/hocs/BasePage'
import I18n from 'i18n-js'
import { PERMISSION_CODES } from 'modules/staff/models'
import CustomerRegisterViewContainer from '../modules/customer/containers/CustomerRegisterViewContainer'
import ConfirmView from 'common/components/widgets/ConfirmView'

export default class CustomerPage extends Component {
  constructor () {
    super()
    this.state = {
      isInputCustomerCode: true,
    }
  }

  render () {
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t("page.customer_register"),
        permissionCode: PERMISSION_CODES.CUSTOMER_REGISTER,
        beforeGoHome: () => ConfirmView.showAsync(I18n.t('message.D-01-I002')),
        onChangeStaffModalShowing: (isInputCustomerCode) => {
          this.setState({isInputCustomerCode: isInputCustomerCode})
        }}}>
        <CustomerRegisterViewContainer
          isInputCustomerCode={this.state.isInputCustomerCode}/>
      </BasePage>
    )
  }
}
