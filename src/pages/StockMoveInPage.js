import React, { Component } from 'react'
import { connect } from 'react-redux'
import BasePage from '../common/hocs/BasePage'
import { PERMISSION_CODES } from '../modules/staff/models'
import I18n from 'i18n-js'
import StockMoveSearchForm from '../modules/stockMove/containers/StockMoveSearchForm'
import StockMoveList from '../modules/stockMove/containers/StockMoveList'
import StockMoveSlipInfo from '../modules/stockMove/containers/StockMoveSlipInfo'
import StockMoveDetail from '../modules/stockMove/containers/StockMoveDetail'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import ImageButton from '../common/components/elements/ImageButton'
import TabPanel from '../common/components/widgets/TabPanel'
import { ProceedButton, WeakProceedButton } from '../common/components/elements/StandardButton'
import styles from '../modules/stockMove/styles/Page'
import {
  MODULE_NAME,
  STOCK_MOVEMENT_STATUS,
  STOCK_MOVEMENT_SEARCH_MODE,
  getSettingsFromState, applyModifyQuantities
} from '../modules/stockMove/models'
import * as actions from '../modules/stockMove/actions'
import { updateStockMovement, cancelStockMovement } from '../modules/stockMove/services'

class StockMoveInPage extends Component {
  static propTypes = {
    resetToDefault: PropTypes.bool,
    currentSettings: PropTypes.object,
    selectedStockMovement: PropTypes.object,
    stockMovementItems: PropTypes.array,
    searchInputs: PropTypes.object
  }
  static defaultProps = {
    resetToDefault: false
  }

  constructor (props) {
    super(props)
    this.state = {
      isDetail: false,
      isSearchProduct: true
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.selectedStockMovement && nextProps.selectedStockMovement) {
      this.setState({ isDetail: true })
    }
    if (this.props.selectedStockMovement && !nextProps.selectedStockMovement) {
      this.setState({ isDetail: false })
    }
  }

  componentWillUnmount () {
    this._onBack()
    this.props.dispatch(actions.setStockMovements([]))
  }

  _onBack () {
    this.setState({ isDetail: false })
    this.props.dispatch(actions.setSelectedStockMovement(null))
  }

  _renderSearchResult () {
    return (
      <View style={styles.mainArea}>
        <View style={styles.listHeader}>
          <Text style={styles.headerText}>
            {I18n.t('common.search_result')}
          </Text>
        </View>
        <StockMoveList
          status={STOCK_MOVEMENT_STATUS.IN_STOCKED} />
      </View>
    )
  }

  _renderMoveinDetail () {
    const { selectedStockMovement } = this.props
    return (
      <View style={styles.mainArea}>
        <View style={styles.detailContainer}>
          <View style={styles.backButtonContainer}>
            <ImageButton
              style={styles.backButton}
              toggle
              appearance={{
                normal: require('../assets/images/header/chevron_left.png'),
                highlight: require('../assets/images/header/chevron_left.png')
              }}
              onPress={() => this._onBack()}
            />
          </View>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}> {I18n.t('stock_move.instock_tab_detail_title')} </Text>
          </View>
        </View>
        <View style={styles.resultDetailArea}>
          <TabPanel tabStyle={styles.resultDetailTab}>
            <View tabName={I18n.t('stock_move.tab_details')}>
              <StockMoveSlipInfo
                status={STOCK_MOVEMENT_STATUS.IN_STOCKED} />
            </View>
            <View tabName={I18n.t('stock_move.tab_detail_items')}>
              <StockMoveDetail
                status={STOCK_MOVEMENT_STATUS.IN_STOCKED} />
            </View>
          </TabPanel>
          {this._renderExecButton(selectedStockMovement)}
        </View>
      </View>
    )
  }

  _renderExecButton (selectedStockMovement) {
    const {currentSettings, searchInputs, stockMovementItems} = this.props

    if (selectedStockMovement.status === STOCK_MOVEMENT_STATUS.OUT_STOCKED) {
      return (
        <View style={styles.actionButtonArea}>
          <WeakProceedButton
            text={I18n.t('stock_move.action_cancel')}
            style={styles.cancelButton}
            onPress={() => this.props.onCancel(selectedStockMovement, searchInputs, currentSettings)} />
          <ProceedButton
            text={I18n.t('stock_move.action_instock')}
            style={styles.instockButton}
            onPress={() => {
              this.props.onInStock(
                applyModifyQuantities(selectedStockMovement, stockMovementItems), searchInputs, currentSettings
              )
            }} />
        </View>
      )
    }
    return null
  }

  render () {
    return (
      <BasePage headerProps={{
        permissionCode: PERMISSION_CODES.STOCK_MOVE_IN,
        onChangeStaffModalShowing: (isSearchProduct) => {
          this.setState({isSearchProduct: isSearchProduct})
        },
        pageName: I18n.t('page.stock_move_in')
      }} >
        <StockMoveSearchForm
          searchMode={STOCK_MOVEMENT_SEARCH_MODE.IN_STOCK}
          allowCreateNew={false}
          resetToDefault={this.props.resetToDefault}
          isSearchProduct={this.state.isSearchProduct}
        />
        {this.state.isDetail ? this._renderMoveinDetail() : this._renderSearchResult()}
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onInStock: async (stockMovement, searchInputs, currentSettings) => {
    await updateStockMovement(dispatch, stockMovement, searchInputs, currentSettings)
  },
  onCancel: async (stockMovement, searchInputs, currentSettings) => {
    await cancelStockMovement(dispatch, stockMovement, searchInputs, currentSettings)
  }
})

const mapStateToProps = state => ({
  currentSettings: getSettingsFromState(state),
  selectedStockMovement: state[MODULE_NAME].selectedStockMovement,
  stockMovementItems: state[MODULE_NAME].stockMovementItems,
  searchInputs: state[MODULE_NAME].searchInputs
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveInPage)
