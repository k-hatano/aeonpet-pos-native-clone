import React from 'react'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import BasePage from 'common/hocs/BasePage'
import ReturnCompleteView from '../modules/order/containers/ReturnCompleteView'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class ReturnCompletePage extends React.Component {
  render () {
    const { isTraining } = this.props
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.return_complete'),
        permissionCode: PERMISSION_CODES.RETURN_ORDER,
        usesCautionColor: true,
        showOpenDrawer: !isTraining}}>
        <ReturnCompleteView onNext={() => {
          Actions.redirectPage({redirectTo: 'home'})
        }} />
      </BasePage>
    )
  }
}
