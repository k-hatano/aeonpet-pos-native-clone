import React, { Component } from 'react'
import BasePage from '../common/hocs/BasePage'
import LoginSelectShopForm from '../modules/login/containers/LoginSelectShopForm'

export default class LoginSelectShopPage extends Component {
  render () {
    return (
      <BasePage showHeader={false}>
        <LoginSelectShopForm />
      </BasePage>
    )
  }
}
