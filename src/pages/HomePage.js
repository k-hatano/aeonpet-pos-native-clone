import React from 'react'
import { View, ListView } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import I18n from 'i18n-js'
import BasePage from 'common/hocs/BasePage'
import styles from './styles/HomePage'
import HomeMainButton from 'modules/home/containers/HomeMainButton'
import HomeTrainingButton from 'modules/home/containers/HomeTrainingButton'
import { getHomePrimaryMenu, getHomeSecondaryMenus, getHomeSubMenus, getPrinterErrorCount } from '../modules/home/models'
import primaryMenuStyle from 'modules/home/styles/HomeMainButton-Primary'
import secondaryMenuStyle from 'modules/home/styles/HomeMainButton-Second'
import { ICON_BUTTON_TYPE } from 'common/components/elements/IconButton'
import PermissionIconButton from 'modules/home/containers/PermissionIconButton'
import { PERMISSION_CODES } from 'modules/staff/models'
import { setPrinterErrorsCount } from '../modules/home/actions'
import ListOrder from 'modules/home/containers/ListOrder'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { makeDateRange } from '/common/utils/dateTime'
import OperationLogRepository from 'modules/home/repositories/OperationLogRepository'
import OrderRepository from 'modules/order/repositories/OrderRepository'
import { createUnpushedItems } from 'modules/home/models/UnpushedItem'
import OrderPendRepository from 'modules/order/repositories/OrderPendRepository'
import * as actions from 'modules/home/actions'
import { MODULE_NAME, OPERATION_TYPE } from 'modules/home/models'
import * as _ from 'underscore'
import { STATUS } from 'modules/order/models'
import { setCustomerDisplayText } from '../modules/customerDisplay/actions'
import nativeEventAbsorber from 'common/models/NativeEventAbsorber'
import ImageButton from 'common/components/elements/ImageButton'
import { updateSetting } from 'modules/setting/actions'
import AppEvents, { REPOSITORY_CONTEXT } from 'common/AppEvents'
import { saveOperationLog } from 'modules/home/service'
import { selectOrder } from 'modules/order/actions'
import { needsOpenSale } from 'modules/openSale/services'
import ExistModalsOrLoadings from 'modules/home/models/ExistModalsOrLoadings'
import logger from 'common/utils/logger'
import { setIsHomepage } from 'common/actions'
import moment from 'moment'

class HomePage extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    mainMenus: PropTypes.object,
    subMenus: PropTypes.object,
    unpushedItems: PropTypes.array,
    selectedOrder: PropTypes.object,
    orderHistory: PropTypes.array,
    repositoryContext: PropTypes.string,
    onInit: PropTypes.func,
    onTrainingMode: PropTypes.func,
    pendedOrders: PropTypes.array,
    initialTab: PropTypes.string,
    cashierId: PropTypes.string
  }

  async componentDidMount () {
    this.props.dispatch(setPrinterErrorsCount(await getPrinterErrorCount()))
    nativeEventAbsorber.registEvent('HOME_RETURN_FROM_SLEEP')
    this.props.dispatch(setIsHomepage(true))
    this.props.repositoryContext === REPOSITORY_CONTEXT.STANDARD && this.props.onCheckOpenSales()
  }

  componentWillMount () {
    this.props.onInit("")
  }

  componentWillUnmount () {
    this.props.dispatch(setIsHomepage(false))
    this.props.dispatch(selectOrder(null))
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.selectedOrder &&
      !this.props.selectedOrder.is_pended &&
      this.props.selectedOrder.status !== STATUS.RETURNED &&
      !nextProps.selectedOrder) {
      this.props.onInit(nextProps.choseDate)
    }
  }

  async _onToggleTrainingMode () {
    await this.props.onTrainingMode(this.props.repositoryContext)
    await this.props.onInit()
  }
  _renderPrimaryMenu (isTrainingMode) {
    const primaryMenu = getHomePrimaryMenu(this.props.mainMenus)
    return (
      <View style={styles.primaryMenuArea}>
        {primaryMenu && <HomeMainButton
          title={I18n.t(primaryMenu.title_key)}
          content={primaryMenu.content}
          stylesheet={primaryMenuStyle}
          onPress={!isTrainingMode || isTrainingMode === primaryMenu.isUseTraining ? primaryMenu.command : () => {}}
          permissionCode={!isTrainingMode ? primaryMenu.permissionCode : null} />}
      </View>
    )
  }
  _renderSecondaryMenus (isTrainingMode) {
    const secondaryMenus = getHomeSecondaryMenus(this.props.mainMenus)
    const dispatch = this.props.dispatch
    return (
      <View style={styles.secondaryMenuArea}>
        {
          secondaryMenus && secondaryMenus.map(menu =>
            <View style={styles.secondaryMenu} key={menu.title_key}>
              <HomeMainButton
                title={I18n.t(menu.title_key)}
                content={menu.content}
                stylesheet={secondaryMenuStyle}
                onPress={!isTrainingMode || isTrainingMode === menu.isUseTraining ? () => menu.command({dispatch}) : () => {}}
                permissionCode={!isTrainingMode ? menu.permissionCode : null} />
            </View>
          )
        }
      </View>
    )
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    })
    const isTrainingMode = this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING
    const isCashierIdUnregistered = !this.props.cashierId

    return (
      <BasePage headerProps={{
        hideBackButton: true,
        showPrinterErrorButton: true,
        pageName: '',
        showOpenDrawer: !isTrainingMode}}>
        <View style={styles.layoutLeft}>
          <View style={styles.specialCommandArea}>
            <View style={styles.specialCommandIconButtonWrapper}>
              <PermissionIconButton
                style={styles.specialCommandIconButton}
                type={ICON_BUTTON_TYPE.FONTAWESOME}
                icon='cog'
                onPress={!isTrainingMode ? Actions.setting : () => {}}
                permissionCode={
                  this.props.repositoryContext !== REPOSITORY_CONTEXT.TRAINING ? PERMISSION_CODES.SETTING : null} />
            </View>
            <View style={styles.specialCommandIconButtonWrapper}>
              <PermissionIconButton
                style={styles.specialCommandIconButton}
                type={ICON_BUTTON_TYPE.FONTAWESOME}
                icon='refresh'
                onPress={!isTrainingMode ? Actions.syncData : () => {}}
                permissionCode={!isTrainingMode ? PERMISSION_CODES.SYNC_DATA : null} />
            </View>
            {!isCashierIdUnregistered &&
              <View style={styles.ImageButtonWrapper}>
                <HomeTrainingButton 
                  onPress={() => this._onToggleTrainingMode(isTrainingMode)}
                  style={styles.ImageButton}
                  repositoryContext={this.props.repositoryContext}
                  permissionCode={!isTrainingMode ? PERMISSION_CODES.TRAINING : null} />
              </View>
            }
          </View>
          <View style={styles.menuArea}>
            {!isCashierIdUnregistered &&
              <View style={styles.mainMenuArea}>
                {this._renderPrimaryMenu(isTrainingMode)}
                {this._renderSecondaryMenus(isTrainingMode)}
              </View>
            }
            <View style={styles.subMenuArea}>
              <ListView
                dataSource={dataSource.cloneWithRows(getHomeSubMenus(this.props.subMenus))}
                contentContainerStyle={styles.subMenuContainer}
                renderRow={(menu) => (
                  <View style={styles.subMenuButton}>
                    <HomeMainButton
                      title={I18n.t(menu.title_key)}
                      content={menu.content}
                      onPress={!isTrainingMode || isTrainingMode === menu.isUseTraining ? menu.command : () => {}}
                      permissionCode={!isTrainingMode ? menu.permissionCode : null} />
                  </View>
                )} />
            </View>
          </View>
        </View>
        <View tyle={styles.layoutRight}>
          <ListOrder
            initialTab={this.props.initialTab}
            repositoryContext={this.props.repositoryContext} />
        </View>
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onInit: async (topMessage, bottomMessage, repositoryContext, chosenDate = '') => {
    try {
      let date
      if (chosenDate.length > 0) {
        date = moment(chosenDate, 'YYYY-MM-DD').toDate()
      } else {
        date = new Date()
      }
      const { from, to } = makeDateRange(date, date)
      const operationLogs = await OperationLogRepository.findByDate(from, to)
      dispatch(actions.listOperationLogs(operationLogs))
      const orderHistory = await OrderRepository.findByDate(from, to)
      dispatch(actions.listOrderHistory(_.sortBy(orderHistory, item => -item.pos_order.client_created_at)))
      const unpushedItems = await createUnpushedItems()
      dispatch(actions.listUnpushedItems(unpushedItems))
      const pendedOrders = await OrderPendRepository.findAll()
      dispatch(actions.listPendedOrders(pendedOrders))
      dispatch(selectOrder(null))
      dispatch(setCustomerDisplayText({
        top: {
          left: topMessage
        },
        bottom: {
          left: bottomMessage
        }
      }))
      dispatch(setPrinterErrorsCount(await getPrinterErrorCount()))
    } catch (error) {
      console.log(error)
    }
  },
  onTrainingMode: async (repositoryContext) => {
    if (repositoryContext && repositoryContext === REPOSITORY_CONTEXT.STANDARD) {
      dispatch(updateSetting({ key: SettingKeys.COMMON.TRAINING_ORDER_NUMBER_SEQUENCE, value: 9999999 }))
      dispatch(updateSetting({
        key: SettingKeys.COMMON.REPOSITORY_CONTEXT,
        value: REPOSITORY_CONTEXT.TRAINING }))
      AppEvents.repositoryContextChange(REPOSITORY_CONTEXT.TRAINING)
      dispatch(setPrinterErrorsCount(await getPrinterErrorCount()))
      await saveOperationLog(OPERATION_TYPE.START_TRAINING_MODE, new Date())
    } else if (repositoryContext && repositoryContext === REPOSITORY_CONTEXT.TRAINING) {
      dispatch(updateSetting({
        key: SettingKeys.COMMON.REPOSITORY_CONTEXT,
        value: REPOSITORY_CONTEXT.STANDARD }))
      AppEvents.repositoryContextChange(REPOSITORY_CONTEXT.STANDARD)
      dispatch(setPrinterErrorsCount(await getPrinterErrorCount()))
      await saveOperationLog(OPERATION_TYPE.END_TRAINING_MODE, new Date())
    }
  },
  onCheckOpenSales: async () => {
    if (await needsOpenSale(dispatch)) {
      logger.info('Cheking needsOpenSale completed. It needs to open sales.')
      if (ExistModalsOrLoadings.isExistModalsOrLoadings) {
        logger.info('Page transiting...')
        Actions.openSales()
      } else {
        logger.info('Page transition was cancelled.')
      }
    } else {
      logger.info('Cheking needsOpenSale completed. It doesn\'t need to open sales.')
    }
  }
})

const mapStateToProps = state => ({
  // name: state[XXX_MODULE_NAME].name
  mainMenus: state['setting'].settings[SettingKeys.MENU.MAIN_MENU],
  subMenus: state['setting'].settings[SettingKeys.MENU.SUB_MENU],
  unpushedItems: state[MODULE_NAME].unpushedItems,
  selectedOrder: state['order'].selectedOrder,
  orderHistory: state[MODULE_NAME].orderHistory,
  pendedOrders: state[MODULE_NAME].pendedOrders,
  topMessage: state['setting'].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.TOP_MESSAGE],
  bottomMessage: state['setting'].settings[SettingKeys.EQUIPMENT.CUSTOMER_DISPLAY.BOTTOM_MESSAGE],
  repositoryContext: state['setting'].settings[SettingKeys.COMMON.REPOSITORY_CONTEXT],
  cashierId: state['setting'].settings[SettingKeys.COMMON.CASHIER_ID],
  choseDate: state[MODULE_NAME].chosenDate
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...dispatchProps,
    ...stateProps,
    onInit: async (chosenDate) => {
      dispatchProps.onInit(
        stateProps.topMessage,
        stateProps.bottomMessage,
        stateProps.repositoryContext,
        chosenDate
      )
    }
    // name: ownProps.name + stateProps.name
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(HomePage)
