import React, { Component } from 'react'
import { Image } from 'react-native'
import initialize from '../common/initialize'
import { Actions } from 'react-native-router-flux'
import Migrator from '../common/utils/migrator'
import BasePage from '../common/hocs/BasePage'
import { loadSettingsAsync, loadSettingsWithRepositoryAsync } from '../modules/setting/models'
import fetcher from 'common/models/fetcher'
import logger from 'common/utils/logger'
import { updateSetting } from '../modules/setting/actions'
import SettingKeys from '../modules/setting/models/SettingKeys'
import { REPOSITORY_CONTEXT } from '../common/AppEvents'
import { needsOpenSale } from 'modules/openSale/services'
import commonStyles from '../common/styles/layout/Page'
import ShopRepository from '../modules/shop/repositories/ShopRepository'
import { shopIds } from '../modules/shop/samples'
import CashierRepository from '../modules/cashier/repositories/CashierRepository'
import { sampleCashierMap } from '../modules/cashier/samples'
import OrderRepository from '../modules/order/repositories/OrderRepository'
import EJournalRepository from '../modules/printer/repositories/EJournalRepository'
import { toUnixTimestamp } from 'common/utils/dateTime'
import OperationLogRepository from '../modules/home/repositories/OperationLogRepository'

export default class InitializePage extends Component {
  async componentDidMount () {
    let settings = {}
    try {
      await this.props.dispatch(updateSetting({ key: SettingKeys.COMMON.REPOSITORY_CONTEXT, value: REPOSITORY_CONTEXT.STANDARD }))
      settings = await loadSettingsAsync(this.props.dispatch)
      logger.init()
      await initialize(this.props.dispatch)
      await Migrator.up()
      await loadSettingsWithRepositoryAsync(this.props.dispatch, settings)
      const regularlyDeleteDate = toUnixTimestamp(new Date()) - (90 * 24 * 60 * 60)
      await OrderRepository.clearBeforeDate(regularlyDeleteDate)
      await EJournalRepository.clearBeforeDate(regularlyDeleteDate)
      await OperationLogRepository.clearBeforeDate(regularlyDeleteDate)
      logger.info('Initialized')
    } catch (error) {
      console.error('Faital Error. Cannot Initialize.')
    }
    const isLoggedIn = fetcher.isLoggedIn
    if (isLoggedIn) {
      if (await needsOpenSale(this.props.dispatch)) {
        Actions.openSales()
      } else {
        Actions.home()
      }
    } else if (settings[SettingKeys.COMMON.REPOSITORY_CONTEXT] === REPOSITORY_CONTEXT.SAMPLE) {
      await ShopRepository.syncRemoteToLocalById(shopIds.A)
      await CashierRepository.save(sampleCashierMap.cashierA_1.id)
      Actions.home()
    } else {
      // アプリ削除前のトークンが残っていることがあるので削除
      this.props.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_ID, value: '' }))
      this.props.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: '' }))
      Actions.loginUserPage()
    }
  }

  render () {
    return (
      <BasePage showHeader={false}>
        <Image source={require('assets/images/splash/splash_landscape@3x.png')} style={commonStyles.backgroundImage} />
      </BasePage>
    )
  }
}
