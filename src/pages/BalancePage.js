import React, { Component } from 'react'
import { connect } from 'react-redux'
import BalanceReasonViewContainer from '../modules/balance/containers/BalanceReasonView'
import BalanceAmountFormContainer from '../modules/balance/containers/BalanceAmountForm'
import CashierBalanceRepository from '../modules/balance/repositories/CashierBalanceRepository'
import { isEmoneyCharge, balanceTypeToLabel, balanceTypeToOperationType } from 'modules/balance/models'
import PropTypes from 'prop-types'
import { loading } from 'common/sideEffects'
import PrinterManager from '../modules/printer/models/PrinterManager'
import BalanceReceiptBuilder from '../modules/balance/models/BalanceReceiptBuilder'
import {
  initForCreateBalances,
  makeBalanceEJournal,
  clearSelected,
  makeBalancesFromState
} from '../modules/balance/services'
import EJournalRepository from '../modules/printer/repositories/EJournalRepository'
import BasePage from 'common/hocs/BasePage'
import AlertView from '../common/components/widgets/AlertView'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { toUnixTimestamp } from 'common/utils/dateTime'
import { saveOperationLog } from 'modules/home/service'
import { PERMISSION_CODES } from 'modules/staff/models'
import PaymentServiceManager from 'modules/payment/models/PaymentServiceManager'
import { showSelectOrderPropertyCashierModal } from 'modules/order/containers/OrderPropertyModalCashierView'
import CashChanger from 'modules/printer/models/CashChanger'

class BalancePage extends Component {
  static propTypes = {
    balances: PropTypes.array,
    onComplete: PropTypes.func
  }

  async componentDidMount () {
    initForCreateBalances(this.props.dispatch)
  }

  async componentWillUnmount () {
    clearSelected(this.props.dispatch)
  }

  async _onComplete () {
    if (CashChanger.isEnabled) {
      const cashBalance = this.props.balances[0]
      showSelectOrderPropertyCashierModal(cashBalance['amount'], cashBalance['currency'], async newPaymentAmount => {
        const result = await this.props.onComplete(this.props.balances)
        if (result) {
          await CashChanger.dispenseChange(parseInt(newPaymentAmount) - parseInt(cashBalance['amount']))
          Actions.home()
        } else {
          await CashChanger.dispenseChange(parseInt(newPaymentAmount))
        }
      })
    } else {
      const result = await this.props.onComplete(this.props.balances)
      if (result) {
        Actions.home()
      }
    }
  }

  render () {
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.balance'),
        permissionCode: PERMISSION_CODES.CASHIER_BALANCE,
        showOpenDrawer: true}}>
        <BalanceReasonViewContainer />
        <BalanceAmountFormContainer onComplete={() => this._onComplete()} selectedBalance={this.props.balances[0]} />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onComplete: async (balances) => {
    let result
    if (isEmoneyCharge(balances[0])) {
      result = await loading(dispatch, async () => {
        const messages = []
        try {
          const paymentServiceManager = new PaymentServiceManager()
          await paymentServiceManager.charge(balances[0])
          return {
            isSuccess: true
          }
        } catch (error) {
          if (error.isUserCancel) {
            messages.push(I18n.t('message.H-02-E001'))
          } else if (error.message && error.message.length > 0) {
            messages.push(error.message)
          }

          return {
            isSuccess: false,
            isUserCancel: error.isUserCancel,
            messages: messages
          }
        }
      })

      if (!result.isSuccess) {
        if (result.isUserCancel) {
          for (const i in result.messages) {
            await AlertView.showAsync(result.messages[i])
          }
          return result.isSuccess
        } else {
          if (!result.messages.length) result.messages.push(I18n.t('message.H-02-E003'))
          for (const i in result.messages) {
            await AlertView.showAsync(result.messages[i])
          }
          return result.isSuccess
        }
      }
    }

    result = await loading(dispatch, async () => {
      let ejournal = null
      let receipt = null
      const createAt = new Date()
      const messages = []

      try {
        const builder = new BalanceReceiptBuilder()
        await builder.initializeAsync()
        receipt = builder.buildBalanceReceipt(balances, createAt)
        ejournal = makeBalanceEJournal(receipt, balances, toUnixTimestamp(createAt))
        await CashierBalanceRepository.bulkPush(balances)
        const operationtype = balanceTypeToOperationType(balances[0].balance_type)
        await saveOperationLog(operationtype, createAt)
        await EJournalRepository.save(ejournal)
      } catch (error) {
        messages.push(I18n.t('message.H-01-E002', {
          process: balanceTypeToLabel(balances[0].balance_type),
          error: error.message
        }))
        return {
          isSuccess: false,
          messages: messages
        }
      }

      try {
        // 入出金の際もドロアオープンを行う
        // トレーニングモード中は入出金操作自体行えない為、trueとする
        await PrinterManager.print(receipt, true)
        await EJournalRepository.saveIsPrintedById(ejournal.id)
      } catch (error) {
        messages.push(I18n.t('message.H-01-E003'))
      }

      try {
        await EJournalRepository.pushById(ejournal.id, false)
      } catch (error) {
        // ユーザーへのエラー通知は行わない。
      }

      return {
        isSuccess: true,
        messages
      }
    })
    for (const i in result.messages) {
      await AlertView.showAsync(result.messages[i])
    }
    if (result.isSuccess) {
      await AlertView.showAsync(I18n.t('message.H-01-I001'))
    }
    return result.isSuccess
  }
})

const mapStateToProps = state => ({
  balances: makeBalancesFromState(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(BalancePage)
