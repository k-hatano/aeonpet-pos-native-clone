import React, { Component } from 'react'
import OpenSaleViewContainer from 'modules/openSale/containers/OpenSaleView'
import { MODULE_NAME } from 'modules/openSale/models'
import { MODULE_NAME as MODULE_NAME_CASHIER_TOTAL } from 'modules/cashierTotal/models'
import { makeCashierRecordFromState } from 'modules/cashierTotal/services'
import { initOpenSale, pushCashierCashRecord } from 'modules/openSale/services'
import { getSettingFromState } from 'modules/setting/models'
import { connect } from 'react-redux'
import BasePage from 'common/hocs/BasePage'
import PropTypes from 'prop-types'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { PERMISSION_CODES } from 'modules/staff/models'
import I18n from 'i18n-js'
import { setIsOpening } from 'common/actions'

class OpenSales extends Component {
  static propTypes = {
    cashierRecord: PropTypes.object,
    cashierRecordData: PropTypes.object,
    onComplete: PropTypes.func
  }

  async componentWillMount () {
    initOpenSale(this.props.dispatch, this.props.cashierRecord, this.props.cashierRecordData)
  }

  componentDidMount () {
    this.props.dispatch(setIsOpening(true))
  }

  componentWillUnmount () {
  }

  render () {
    const { cashierRecord, cashierRecordData, isUpdated } = this.props
    return (
      <BasePage headerProps={{
        hideBackButton: true,
        showForcedCloseSalesButton: true,
        showReprintCloseSales: true,
        pageName: I18n.t('page.open'),
        permissionCode: PERMISSION_CODES.OPEN_SALES,
        showHeaderMenuSetting: true,
        showOpenDrawer: true,
        showIncrementalDataSync: true}}>
        <OpenSaleViewContainer
          cashierRecord={this.props.cashierRecord}
          onComplete={() => this.props.onComplete(cashierRecord, cashierRecordData, isUpdated)}
        />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onComplete: async (cashierRecord, cashierRecordData, isUpdated) => {
    pushCashierCashRecord(dispatch, cashierRecord, cashierRecordData, isUpdated)
  }
})

const mapStateToProps = state => ({
  cashierRecord: {
    ...makeCashierRecordFromState(state),
    ...{
      currency: getSettingFromState(state, SettingKeys.COMMON.CURRENCY),
      staff_id: state[MODULE_NAME].selectedStaff ? state[MODULE_NAME].selectedStaff.id : null,
      staff_name: state[MODULE_NAME].selectedStaff ? state[MODULE_NAME].selectedStaff.name : null
    }
  },
  cashierRecordData: state[MODULE_NAME_CASHIER_TOTAL].cashierRecordData,
  isUpdated: state[MODULE_NAME].isUpdated
})

export default connect(mapStateToProps, mapDispatchToProps)(OpenSales)
