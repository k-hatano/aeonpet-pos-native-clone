import I18n from 'i18n-js'
import React, { Component } from 'react'
import BasePage from 'common/hocs/BasePage'
import CheckSalesView from 'modules/cashierTotal/containers/CheckSalesView'
import { PERMISSION_CODES } from 'modules/staff/models'

export default class CheckSalesPage extends Component {
  render () {
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.check_sales'),
        permissionCode: PERMISSION_CODES.CHECK_SALES,
        showOpenDrawer: true}}>
        <CheckSalesView />
      </BasePage>
    )
  }
}
