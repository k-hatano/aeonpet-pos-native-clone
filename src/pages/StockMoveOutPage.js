import React, { Component } from 'react'
import { connect } from 'react-redux'
import BasePage from '../common/hocs/BasePage'
import { PERMISSION_CODES } from '../modules/staff/models'
import I18n from 'i18n-js'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import ImageButton from '../common/components/elements/ImageButton'
import TabPanel from '../common/components/widgets/TabPanel'
import StockMoveSearchForm from '../modules/stockMove/containers/StockMoveSearchForm'
import StockMoveList from '../modules/stockMove/containers/StockMoveList'
import StockMoveSlipInfo from '../modules/stockMove/containers/StockMoveSlipInfo'
import StockMoveDetail from '../modules/stockMove/containers/StockMoveDetail'
import styles from '../modules/stockMove/styles/Page'
import * as actions from '../modules/stockMove/actions'
import {
  MODULE_NAME, STOCK_MOVEMENT_STATUS, STOCK_MOVEMENT_SEARCH_MODE
} from '../modules/stockMove/models'

class StockMoveOutPage extends Component {
  static PropTypes = {
    selectedStockMovement: PropTypes.object,
    resetToDefault: PropTypes.bool
  }
  static defaultProps = {
    resetToDefault: false
  }

  componentWillUnmount () {
    this._onBack()
    this.props.dispatch(actions.setStockMovements([]))
  }

  constructor (props) {
    super(props)
    this.state = {
      isDetail: false,
      isSearchProduct: true
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.selectedStockMovement && nextProps.selectedStockMovement) {
      this.setState({ isDetail: true })
    }
    if (this.props.selectedStockMovement && !nextProps.selectedStockMovement) {
      this.setState({ isDetail: false })
    }
  }

  _onBack () {
    this.setState({ isDetail: false })
    this.props.dispatch(actions.setSelectedStockMovement(null))
  }

  _renderSearchResult () {
    return (
      <View style={styles.mainArea}>
        <View style={styles.listHeader}>
          <Text style={styles.headerText}>
            {I18n.t('common.search_result')}
          </Text>
        </View>
        <StockMoveList
          status={STOCK_MOVEMENT_STATUS.OUT_STOCKED} />
      </View>
    )
  }

  _renderMoveoutDetail () {
    return (
      <View style={styles.mainArea}>
        <View style={styles.detailContainer}>
          <View style={styles.backButtonContainer}>
            <ImageButton
              style={styles.backButton}
              toggle
              appearance={{
                normal: require('../assets/images/header/chevron_left.png'),
                highlight: require('../assets/images/header/chevron_left.png')
              }}
              onPress={() => this._onBack()}
            />
          </View>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}> {I18n.t('stock_move.outstock_tab_detail_title')} </Text>
          </View>
        </View>
        <View style={styles.resultDetailArea}>
          <TabPanel tabStyle={styles.resultDetailTab}>
            <View tabName={I18n.t('stock_move.tab_details')}>
              <StockMoveSlipInfo
                status={STOCK_MOVEMENT_STATUS.OUT_STOCKED} />
            </View>
            <View tabName={I18n.t('stock_move.tab_detail_items')}>
              <StockMoveDetail
                status={STOCK_MOVEMENT_STATUS.OUT_STOCKED} />
            </View>
          </TabPanel>
        </View>
      </View>
    )
  }

  render () {
    return (
      <BasePage headerProps={{
        permissionCode: PERMISSION_CODES.STOCK_MOVE_OUT,
        onChangeStaffModalShowing: (isSearchProduct) => {
          this.setState({isSearchProduct: isSearchProduct})
        },
        pageName: I18n.t('page.stock_move_out')
      }} >
        <StockMoveSearchForm
          searchMode={STOCK_MOVEMENT_SEARCH_MODE.OUT_STOCK}
          allowCreateNew
          resetToDefault={this.props.resetToDefault}
          isSearchProduct={this.state.isSearchProduct}
        />
        {this.state.isDetail ? this._renderMoveoutDetail() : this._renderSearchResult()}
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
})

const mapStateToProps = state => ({
  selectedStockMovement: state[MODULE_NAME].selectedStockMovement
})

export default connect(mapStateToProps, mapDispatchToProps)(StockMoveOutPage)
