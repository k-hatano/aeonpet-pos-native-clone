import React, { Component } from 'react'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import BasePage from '../common/hocs/BasePage'
import OrderCompleteView from '../modules/order/containers/OrderCompleteView'
import { PERMISSION_CODES } from 'modules/staff/models'

class OrderCompletePage extends Component {
  render () {
    const { isTraining } = this.props
    return (
      <BasePage headerProps={{
        hideBackButton: false,
        showPrinterErrorButton: false,
        pageName: I18n.t('page.order_complete'),
        permissionCode: PERMISSION_CODES.NEW_ORDER,
        showOpenDrawer: !isTraining}}>
        <OrderCompleteView onNext={() => {
          Actions.redirectPage({redirectTo: 'cart'})
        }} />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderCompletePage)
