'use strict'

import { View, Text, ScrollView, NativeModules, NativeEventEmitter } from 'react-native'
// import I18n from 'i18n-js'
import React, { Component } from 'react'
import Migrator from '../common/utils/migrator'
import styles from '../modules/debug/styles/Debug'
import commonStyles from '../common/styles/layout/Page'
import { Col, Row, Grid } from 'react-native-easy-grid'
import Header from '../common/components/layout/OldHeader'
import ProgressBar from '../common/components/widgets/ProgressBar'
import AppEvents, { REPOSITORY_CONTEXT } from '../common/AppEvents'
import BasePage from '../common/hocs/BasePage'
import { Actions } from 'react-native-router-flux'
import StandardButton from '../common/components/elements/StandardButton'
import initialize from '../common/initialize'
import { loading } from '../common/sideEffects'
import logout from 'modules/login/models/logout'
import { updateSetting } from '../modules/setting/actions'
import SettingKeys from '../modules/setting/models/SettingKeys'
import { sequelize } from 'common/DB'
import PaymentServiceManager from 'modules/payment/models/PaymentServiceManager'
import CartPayment from '../modules/cart/models/CartPayment'
import { PAYMENT_METHOD_TYPE } from '../modules/payment/models'
import AlertView from '../common/components/widgets/AlertView'
import CashierBalanceReasonEntity from 'modules/balance/repositories/entities/CashierBalanceReasonEntity'
import EJournalRepository from '../modules/printer/repositories/EJournalRepository'
import OrderRepository from '../modules/order/repositories/OrderRepository'
import PrinterManager from 'modules/printer/models/PrinterManager'
import PasoriManager from 'modules/pasori/PasoriManager'
import CustomerDisplayManager from '../modules/customerDisplay/models/CustomerDisplayManager'
import { cChangerErrorMessage } from 'modules/printer/models/CashChangerResult'
import logger from '../common/utils/logger'
import fetcher from 'common/models/fetcher'
import generateUuid from 'common/utils/generateUuid'
import { TAX_CODE } from 'common/models/Tax'
import moment from 'moment'
import settingGetter from 'modules/setting/models/settingGetter'
import CashChangerDataConverter from '../modules/printer/models/CashChangerDataConverter.js'
import StandardPromotionRepository from 'modules/promotion/repositories/standard/StandardPromotionRepository'
import ProductEntity from 'modules/product/repositories/entities/ProductEntity'

export default class Debug extends Component {
  debugManager = NativeModules.PrinterBridge
  debugManagerEmitter = new NativeEventEmitter(this.debugManager)
  paymentManager = new PaymentServiceManager()

  sendCashChangerErrorListener = null
  errorListener = null
  printCompletedListener = null
  cashCountCompleteListener = null
  sendCashChangerStatusListener = null

  constructor (props) {
    super(props)

    this.progress = null
    const dispatch = this.props.dispatch
    this.groups = [
      {
        title: 'DB',
        buttons: [
          {
            title: 'Migrate Up',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                await Migrator.up()
              })
            }
          },
          {
            title: 'Migrate Down',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                await Migrator.down()
              })
            }
          },
          {
            title: 'Drop All Table',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                console.log(sequelize)
                const tables = await sequelize.query("select name from sqlite_master where type='table'")
                console.log(tables)
                const dropTables = tables.filter(table => table !== 'sqlite_sequence')
                for (const i in dropTables) {
                  const table = dropTables[i]
                  await sequelize.query('DROP TABLE ' + table)
                }
              })
            }
          },
          {
            title: 'Delete EJournals',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                console.log(sequelize)
                const tables = await sequelize.query('delete from ejournals')
                console.log(tables)
              })
            }
          },
          {
            title: 'Clear Unpushed Data',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                await EJournalRepository.saveIsPushedAll()
                await OrderRepository.saveIsPushedAll()
              })
            }
          },
          {
            title: 'CreateSampleData',
            onPress: async () => {
              this.progress.show()
              await AppEvents.initializeTableAsync()
              await AppEvents.sampleDataCreateAsync()
              this.progress.hide()
            }
          },
          {
            title: 'Throw DB Error',
            onPress: async () => {
              try {
                console.log('start throw db error')
                await CashierBalanceReasonEntity.create({
                  sort_order: NaN
                })
                console.log('no throw')
              } catch (error) {
                console.log(error)
              }
            }
          }
        ]
      },
      {
        title: 'Sample Pages',
        buttons: [
          {
            title: 'Modal',
            onPress: async () => {
              Actions.modalSamplePage()
            }
          },
          {
            title: 'Buttons',
            onPress: async () => {
              Actions.buttonSamplePage()
            }
          },
          {
            title: 'Components',
            onPress: async () => {
              Actions.componentSamplePage()
            }
          },
          {
            title: 'OrderCompletePage',
            onPress: async () => {
              Actions.orderComplete()
            }
          },
          {
            title: 'TaxFreePage',
            onPress: async () => {
              Actions.taxFree({ orderNumber: '00000000000000000000090000000001' })
            }
          },
          {
            title: 'Logout',
            onPress: async () => {
              await logout(dispatch)
              dispatch(
                updateSetting({
                  key: SettingKeys.COMMON.REPOSITORY_CONTEXT,
                  value: REPOSITORY_CONTEXT.STANDARD
                })
              )
              Actions.loginUserPage()
            }
          },
          {
            title: 'OPN-3200Si',
            onPress: async () => {
              Actions.OPN3200SamplePage()
            }
          },
          {
            title: 'AlertView',
            onPress: async () => {
              Actions.alertViewSamplePage()
            }
          }
        ]
      },
      {
        title: 'Commands',
        buttons: [
          {
            title: 'Set language to ja',
            onPress: async () => {
              await initialize(this.props.dispatch, 'ja')
            }
          },
          {
            title: 'Set language to en',
            onPress: async () => {
              await initialize(this.props.dispatch, 'en')
            }
          }
        ]
      },
      {
        title: 'Customer Display',
        buttons: [
          {
            title: 'Connect',
            onPress: async () => {
              this.progress.show()
              CustomerDisplayManager._connect(
                () => {
                  console.log('接続成功')
                },
                error => {
                  console.log(error)
                }
              )
              this.progress.hide()
            }
          },
          {
            title: 'Display',
            onPress: async () => {
              this.progress.show()
              CustomerDisplayManager.displayMessageToCustomerDisplay(
                'あいうえお',
                'アイウエオ',
                () => {
                  console.log('表示成功')
                },
                error => {
                  console.log(error)
                }
              )

              this.progress.hide()
            }
          },
          {
            title: 'Disconnect',
            onPress: async () => {
              this.progress.show()
              CustomerDisplayManager.disconnect()
              this.progress.hide()
            }
          }
        ]
      },
      {
        title: 'VEGA3000',
        buttons: [
          {
            title: 'VEGA Payment',
            onPress: async () => {
              try {
                let cartPayment = CartPayment.createFromPaymentMethodEntity({
                  name: 'クレジット',
                  payment_method_type: PAYMENT_METHOD_TYPE.CREDIT,
                  is_offline: false
                })
                cartPayment = cartPayment.updateAmount(1)
                const result = await this.paymentManager.pay(cartPayment)
                console.log('payment success ', result)
                this._paymentResult = result
              } catch (error) {
                console.log('payment failed', error)
              }
            }
          },
          {
            title: 'VEGA Refund',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                try {
                  const result = await this.paymentManager.refund(this._paymentResult)
                  console.log('refund success ', result)
                } catch (error) {
                  console.log('refund failed', error)
                }
              })
            }
          },
          {
            title: 'Interrupt',
            onPress: async () => {
              this.paymentManager.interrupt()
              logger.debug('payment interrupt')
            }
          }
        ]
      },
      {
        title: 'Cash Changer',
        buttons: [
          {
            title: 'Disconnect',
            onPress: async () => {
              try {
                await PrinterManager.disconnectCasher()
                logger.debug('disconnect success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('disconnect failed ' + error)
              }
            }
          },
          {
            title: 'Connect',
            onPress: async () => {
              try {
                await PrinterManager.connectCasher()
                AlertView.show('connect success')
                logger.debug('connect success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('connect failed ' + error)
              }
            }
          },
          {
            title: 'Read cash Count',
            onPress: async () => {
              try {
                await PrinterManager.readCashCount()
                logger.debug('readCashCount success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('readCashCount failed ' + error)
              }
            }
          },
          {
            title: 'Deposit',
            onPress: async () => {
              try {
                await PrinterManager.depositCasher()
                logger.debug('deposit success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('deposit failed ' + error)
              }
            }
          },
          {
            title: 'Pause Deposit',
            onPress: async () => {
              try {
                await PrinterManager.pauseDepositCasher()
                logger.debug('pause deposit success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('pause deposit failed ' + error)
              }
            }
          },
          {
            title: 'Restart Deposit',
            onPress: async () => {
              try {
                await PrinterManager.restartDepositCasher()
                logger.debug('restart deposit success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('restart deposit failed ' + error)
              }
            }
          },
          {
            title: 'End Deposit',
            onPress: async () => {
              try {
                const result = await PrinterManager.endDepositCasher(0)
                logger.debug('end deposit success ' + result)
              } catch (error) {
                AlertView.show(error)
                logger.debug('end deposit failed ' + error)
              }
            }
          },
          {
            title: 'Dispense',
            onPress: async () => {
              try {
                const obj = {
                  'jpy5000': 0,
                  'jpy5': 0,
                  'jpy1000': 0,
                  'jpy10': 0,
                  'jpy2000': 0,
                  'jpy100': 1,
                  'jpy10000': 0,
                  'jpy500': 0,
                  'jpy1': 0,
                  'jpy50': 0
                }
                await PrinterManager.dispenseCasher(obj)
                logger.debug('dispense success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('dispense failed ' + error)
              }
            }
          },
          {
            title: 'Dispense Change',
            onPress: async () => {
              try {
                await PrinterManager.dispenseChange(1000)
                logger.debug('dispense change success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('dispense change failed ' + error)
              }
            }
          },
          {
            title: 'Read Status',
            onPress: async () => {
              try {
                await PrinterManager.readCasherStatus()
                logger.debug('Read Status success')
              } catch (error) {
                AlertView.show(error)
                logger.debug('Read Status failed ' + error)
              }
            }
          }
        ]
      },
      {
        title: 'Pasori',
        buttons: [
          {
            title: 'Start initialize',
            onPress: () => {
              PasoriManager.startInitialize()
            }
          },
          {
            title: 'Close',
            onPress: () => {
              PasoriManager.close()
            }
          },
          {
            title: 'Start',
            onPress: async () => {
              try {
                const result = await PasoriManager.start()
                AlertView.show(JSON.stringify(result))
              } catch (error) {
                AlertView.show(error)
                logger.debug('Start Pasoori Failed ' + error)
              }
            }
          }
        ]
      },
      {
        title: 'Multiple Tax Rates',
        buttons: [
          {
            title: 'Insert Standard Tax',
            onPress: async () => {
              this.progress.show()
              try {
                const testData = {
                  id: generateUuid(),
                  tax_code: TAX_CODE.STANDARD,
                  start_date: moment().toDate(),
                  end_date: moment().add(1, 'd').toDate(),
                  tax_rate: 0.1000,
                  receipt_sign: '',
                  receipt_comment: ''
                }
                await fetcher.post('multiple-tax-rate', testData)
              } catch (error) {

              }
              this.progress.hide()
            }
          },
          {
            title: 'Insert Reduced Tax',
            onPress: async () => {
              this.progress.show()
              try {
                const testData = {
                  id: generateUuid(),
                  tax_code: TAX_CODE.REDUCED,
                  start_date: moment().toDate(),
                  end_date: moment().add(1, 'd').toDate(),
                  tax_rate: 0.0800,
                  receipt_sign: '*',
                  receipt_comment: '*は軽減税率8%適用商品'
                }
                await fetcher.post('multiple-tax-rate', testData)
              } catch (error) {

              }
              this.progress.hide()
            }
          }
        ]
      },
      {
        title: 'Promotion Points',
        buttons: [
          {
            title: 'Get Index From Product Promotion Points',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                try {
                  const result = await fetcher.get('point-promotions/product/index/' + settingGetter.shopId)
                  console.log(result)
                } catch (error) {
                  console.log(error)
                }
              })
            }
          },
          {
            title: 'Get Index From Shop Promotion Points',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                try {
                  const result = await fetcher.get('point-promotions/shop/index/' + settingGetter.shopId)
                  console.log(result)
                } catch (error) {
                  console.log(error)
                }
              })
            }
          },
          {
            title: 'Search Local for Shop Point Ratio',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                try {
                  const repo = new StandardPromotionRepository()
                  const result = await repo.findTodaysShopPointRatio(2)
                  console.log(result)
                } catch (error) {
                  console.error(error)
                }
              })
            }
          },
          {
            title: 'Search Local for Product Point Ratio',
            onPress: async () => {
              await loading(this.props.dispatch, async () => {
                try {
                  const repo = new StandardPromotionRepository()
                  const result = await repo.findProductPointRatioByProductId('0871601AC52F472DAE72AFCD574476EA')
                  console.log(result)
                } catch (error) {
                  console.error(error)
                }
              })
            }
          }
        ]
      },
      {
        title: 'Cash Voucher',
        buttons: [
          {
            title: 'Insert Cash Voucher',
            onPress: async () => {
              this.progress.show()
              try {
                const testData = {
                  id: generateUuid(),
                  barcode: '9848311410504',
                  amount: 500,
                  issuer: '発行会社名',
                  brand: 'ブランド名',
                  payment_method_id: 'E46D87BC1F544777844896D66561AE48',
                  issue_type: ''
                }
                await fetcher.post('cash-voucher', testData)
              } catch (error) {

              }
              this.progress.hide()
            }
          }
        ]
      },
      {
        title: 'Crash Logs',
        buttons: [
          {
            title: 'Force Crash',
            onPress: async () => {
              NativeModules.CrashReportBridge.forceCrash()
            }
          },
          {
            title: 'Get Report',
            onPress: async () => {
              NativeModules.CrashReportBridge.getRreportFileNames((reportNames) => {
                console.log('reportNames', reportNames)
                if (reportNames.length > 0) {
                  NativeModules.CrashReportBridge.getRreport(reportNames[0], (report) => {
                    console.log('report', report)
                  })
                }
              })
            }
          },
          {
            title: 'Get Epson Log',
            onPress: async () => {
              NativeModules.CrashReportBridge.getEpsonLog((epsonLog) => {
                console.log('epsonLog', epsonLog)
              })
            }
          }
        ]
      }
    ]
  }

  componentDidMount () {
    this._addListener()
  }

  componentWillUnmount () {
    this._removeListener()
  }

  _addListener () {
    this.errorListener = this.debugManagerEmitter.addListener('Error', (body) => {
      logger.debug(body)
    })

    this.printCompletedListener = this.debugManagerEmitter.addListener('PrintCompleted', (body) => {
      AlertView.show('印刷が完了しました')
    })

    this.cashCountCompleteListener = this.debugManagerEmitter.addListener('CashCountComplete', (body) => {
      AlertView.show(JSON.stringify(body))
    })

    this.sendCashChangerErrorListener = this.debugManagerEmitter.addListener('SendCashChangerError', (body) => {
      const error = cChangerErrorMessage(body)
      AlertView.show(error)
    })

    this.sendCashChangerStatus = this.debugManagerEmitter.addListener('SendCashChangerStatus', (body) => {
      const result = CashChangerDataConverter.convertResponse(body)
      logger.debug(result)
      AlertView.show(result.toString())
    })
  }

  _removeListener () {
    this.debugManagerEmitter.removeSubscription(this.errorListener)
    this.debugManagerEmitter.removeSubscription(this.printCompletedListener)
    this.debugManagerEmitter.removeSubscription(this.cashCountCompleteListener)
    this.debugManagerEmitter.removeSubscription(this.sendCashChangerErrorListener)
    this.debugManagerEmitter.removeSubscription(this.sendCashChangerStatus)
  }

  renderGroups () {
    return this.groups.map((group, key) =>
      <View style={styles.groupContainer} key={key}>
        <View style={styles.groupHeaderContainer}>
          <Text style={styles.groupTitle}>
            {group.title}
          </Text>
        </View>
        <View>
          {group.buttons &&
            group.buttons.map((button, index) =>
              <View style={styles.buttonWrap} key={index}>
                <StandardButton
                  text={button.title}
                  color={'#ff9024'}
                  textStyle={styles.buttonTitleStyle}
                  onPress={() => button.onPress()}
                />
              </View>
            )}
        </View>
      </View>
    )
  }

  render () {
    return (
      <BasePage showHeader={false}>
        <Grid style={commonStyles.basicLayoutRootStyle}>
          <Row style={{ height: 72 }}>
            <Header />
          </Row>
          <Row>
            <ProgressBar.Component
              ref={progress => {
                this.progress = progress
              }}
            />
            <Col>
              <ScrollView style={styles.scrollViewContainer} contentContainerStyle={styles.contentContainerStyle}>
                {this.renderGroups()}
              </ScrollView>
            </Col>
          </Row>
        </Grid>
      </BasePage>
    )
  }
}
