import React, { Component } from 'react'
import { connect } from 'react-redux'
import I18n from 'i18n-js'
import TaxFreeConfirmViewContainer from 'modules/taxFree/containers/TaxFreeConfirmView'
import BasePage from 'common/hocs/BasePage'
import PropTypes from 'prop-types'
import {
  makeTaxFreeEJournal,
  makeTaxFreeForState,
  makeOrderForTaxFree
} from 'modules/taxFree/models'
import ConfirmView from 'common/components/widgets/ConfirmView'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import PrinterManager from 'modules/printer/models/PrinterManager'
import TaxFreeReceiptBuilder from 'modules/taxFree/models/TaxFreeReceiptBuilder'
import * as actions from 'modules/taxFree/actions'
import { Actions, ActionConst } from 'react-native-router-flux'
import { loading } from 'common/sideEffects'
import { RECEIPT_TYPE } from 'modules/printer/models'
import AlertView from 'common/components/widgets/AlertView'
import { getSettingFromState } from '../modules/setting/models'
import SettingKeys from '../modules/setting/models/SettingKeys'
import { REPOSITORY_CONTEXT } from '../common/AppEvents'

class TaxFreeConfirmPage extends Component {
  static propTypes = {
    taxFree: PropTypes.object,
    order: PropTypes.object,
    onFix: PropTypes.func,
    onComplete: PropTypes.func
  }

  async componentWillMount () {
    this.headerProps = {
      hideBackButton: false,
      beforeGoHome: () => ConfirmView.showAsync(I18n.t('message.K-01-I001')),
      pageName: I18n.t('tax_free.confirm_form_inputs'),
      showOpenDrawer: !this.props.isTraining
    }
  }

  render () {
    return (
      <BasePage headerProps={this.headerProps}>
        <TaxFreeConfirmViewContainer
          onFix={() => this.props.onFix()}
          onComplete={() =>
            ConfirmView.show(I18n.t('message.K-03-I001'), () =>
              this.props.onComplete(this.props.taxFree, this.props.order)
            )}
          order={this.props.order}
        />
      </BasePage>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onFix: async () => {
    Actions.pop({
      type: ActionConst.REFRESH
    })
  },
  onComplete: async (taxFree, order, isTraining) => {
    const builder = new TaxFreeReceiptBuilder()

    // 受注情報から商品情報取得
    let newOrder = await makeOrderForTaxFree(order)
    const now = new Date()
    const currentTimestamp = Math.floor(new Date().getTime() / 1000)

    // 輸出免税物品購入記録起票レシート
    const receiptSales = builder.buildTaxFreeReceipt(
      RECEIPT_TYPE.TAXFREE_SALES,
      taxFree,
      newOrder,
      now,
      isTraining
    )

    const ejournalSales = makeTaxFreeEJournal(
      I18n.t('receipt.title.tax_free_sales'),
      RECEIPT_TYPE.TAXFREE_SALES,
      receiptSales,
      taxFree,
      newOrder,
      currentTimestamp
    )

    // 購入者誓約書レシート
    const receiptCustomer = builder.buildTaxFreeReceipt(
      RECEIPT_TYPE.TAXFREE_CUSTOMER,
      taxFree,
      newOrder,
      now,
      isTraining
    )

    const ejournalCustomer = makeTaxFreeEJournal(
      I18n.t('receipt.title.tax_free_customer'),
      RECEIPT_TYPE.TAXFREE_CUSTOMER,
      receiptCustomer,
      taxFree,
      newOrder,
      currentTimestamp
    )

    // 梱包用リストレシート
    let receiptShipping = null
    let ejournalShipping = null
    if (newOrder.order_comsumable_products.length > 0) {
      receiptShipping = builder.buildTaxFreeReceipt(
        RECEIPT_TYPE.TAXFREE_SHIPPING,
        taxFree,
        newOrder,
        now,
        isTraining
      )

      ejournalShipping = makeTaxFreeEJournal(
        I18n.t('receipt.title.tax_free_shipping'),
        RECEIPT_TYPE.TAXFREE_SHIPPING,
        receiptShipping,
        taxFree,
        newOrder,
        currentTimestamp
      )
    }

    const messages = []
    await loading(dispatch, async () => {
      await EJournalRepository.save(ejournalSales)
      await EJournalRepository.save(ejournalCustomer)
      if (receiptShipping) {
        await EJournalRepository.save(ejournalShipping)
      }

      // 輸出免税物品購入記録起票レシート
      try {
        await PrinterManager.print(receiptSales)
        await EJournalRepository.saveIsPrintedById(ejournalSales.id)
        await EJournalRepository.pushById(ejournalSales.id)
      } catch (error) {
        messages.push(I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_sales')
        }))
      }

      // 購入者誓約書レシート
      try {
        await PrinterManager.print(receiptCustomer)
        await EJournalRepository.saveIsPrintedById(ejournalCustomer.id)
        await EJournalRepository.pushById(ejournalCustomer.id)
      } catch (error) {
        messages.push(I18n.t('message.K-03-E002', {
          receipt: I18n.t('receipt.title.tax_free_customer')
        }))
      }

      // 梱包用リストレシート
      if (receiptShipping) {
        try {
          await PrinterManager.print(receiptShipping)
          await EJournalRepository.saveIsPrintedById(ejournalShipping.id)
          await EJournalRepository.pushById(ejournalShipping.id)
        } catch (error) {
          messages.push(I18n.t('message.K-03-E002', {
            receipt: I18n.t('receipt.title.tax_free_shipping')
          }))
        }
      }
    })

    for (let i = 0; i < messages.length; i++) {
      const message = messages[i]
      await AlertView.showAsync(message)
    }

    dispatch(
      actions.setEJournals({
        sales: ejournalSales,
        customer: ejournalCustomer,
        shipping: ejournalShipping
      })
    )
    dispatch(actions.clearTaxFree())
    Actions.home()
  }
})

const mapStateToProps = state => ({
  taxFree: makeTaxFreeForState(state),
  isTraining: getSettingFromState(state, SettingKeys.COMMON.REPOSITORY_CONTEXT) === REPOSITORY_CONTEXT.TRAINING
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...dispatchProps,
    ...stateProps,
    onComplete: async (taxFree, order, isTraining) => {
      dispatchProps.onComplete(taxFree, order, stateProps.isTraining)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(TaxFreeConfirmPage)
