import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import { Row, Grid } from 'react-native-easy-grid'
import Modal from '../common/components/widgets/OldModal'
import Header from '../common/components/layout/OldHeader'
import Footer from '../common/components/layout/Footer'
import NumberPadModal from '../common/components/widgets/NumberPadModal'
import SalesResult from '../modules/cashierTotal/components/SalesResult'
import SalesControl from '../modules/cashierTotal/components/SalesControl'
import { formatMoney } from '../common/utils/formats'
import commonStyles from '../common/styles/layout/Page'
import salesResultStyles from '../modules/cashierTotal/styles/SalesResult'

export default class Opening extends Component {
  constructor (props) {
    super(props)

    this.state = {
      values: {
        ten_thousand_yen: { value: 0, multiple: 10000 },
        five_thousand_yen: { value: 0, multiple: 5000 },
        two_thousand_yen: { value: 0, multiple: 2000 },
        one_thousand_yen: { value: 0, multiple: 1000 },
        five_hundred_yen: { value: 0, multiple: 500 },
        five_hundred_yen_coin_bar: { value: 0, multiple: 500 * 50 },
        one_hundred_yen: { value: 0, multiple: 100 },
        one_hundred_yen_coin_bar: { value: 0, multiple: 100 * 50 },
        fifty_yen: { value: 0, multiple: 100 },
        fifty_yen_coin_bar: { value: 0, multiple: 50 * 50 },
        ten_yen: { value: 0, multiple: 10 },
        ten_yen_coin_bar: { value: 0, multiple: 10 * 50 },
        five_yen: { value: 0, multiple: 5 },
        five_yen_coin_bar: { value: 0, multiple: 5 * 50 },
        one_yen: { value: 0, multiple: 1 },
        one_yen_coin_bar: { value: 0, multiple: 1 * 50 }
      }
    }

    this.selected = null
  }

  onNumpadPress (value) {
    let { values } = this.state
    if (this.selected && values[this.selected.key]) {
      const key = this.selected.coinBar ? `${this.selected.key}_coin_bar` : this.selected.key
      values[key].value = value
      this.setState({
        values
      })
      NumberPadModal.close()
    }
  }

  onControlPress (item, coinBar = false) {
    this.selected = { key: item.key, coinBar }
    NumberPadModal.open()
  }

  calculatorTotal (values) {
    let total = 0
    Object.keys(values).map(key => {
      const item = values[key]
      total += item.value * item.multiple
    })
    return total
  }

  resetValues () {
    let { values } = this.state

    Object.keys(values).map(key => {
      values[key].value = 0
    })

    this.setState({
      values
    })
  }

  render () {
    const { values } = this.state
    const total = this.calculatorTotal(values)

    const menus = {
      top: [
        {
          title: I18n.t('check_sale.ten_thousand_yen'),
          key: 'ten_thousand_yen'
        },
        {
          title: I18n.t('check_sale.five_thousand_yen'),
          key: 'five_thousand_yen'
        },
        {
          title: I18n.t('check_sale.two_thousand_yen'),
          key: 'two_thousand_yen'
        },
        {
          title: I18n.t('check_sale.one_thousand_yen'),
          key: 'one_thousand_yen'
        }
      ],
      bottom: [
        {
          title: I18n.t('check_sale.five_hundred_yen'),
          key: 'five_hundred_yen'
        },
        {
          title: I18n.t('check_sale.one_hundred_yen'),
          key: 'one_hundred_yen'
        },
        {
          title: I18n.t('check_sale.fifty_yen'),
          key: 'fifty_yen'
        },
        {
          title: I18n.t('check_sale.ten_yen'),
          key: 'ten_yen'
        },
        {
          title: I18n.t('check_sale.five_yen'),
          key: 'five_yen'
        },
        {
          title: I18n.t('check_sale.one_yen'),
          key: 'one_yen'
        }
      ]
    }

    return (
      <Grid style={commonStyles.basicLayoutRootStyle}>
        <Row style={{ height: 72 }}>
          <Header
            bottomButtons={[
              {
                icon: 'md-person',
                type: 'ionic',
                text: 'Staff name',
                color: '#fff',
                iconSize: 24,
                containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
                onPress: () => {}
              }
            ]}
          />
        </Row>
        <Row>
          <SalesControl
            onControlPress={(item, coinBar = false) => this.onControlPress(item, coinBar)}
            menus={menus}
            values={values}
            onClear={() => this.resetValues()}
          />
          <SalesResult
            componentStyles={{ paddingHorizontal: 15, paddingVertical: 10 }}
            menus={menus.top.concat(menus.bottom)}
            values={values}
            total={total}
            onRegister={() => Modal.open('modalOpening')}
          />
        </Row>
        <Row style={{ height: 50, width: '100%' }}>
          <Footer
            leftButtons={[
              {
                icon: 'cart-outline',
                type: 'material',
                text: I18n.t('order.pending_order'),
                color: '#fff',
                iconSize: 24,
                containerStyle: { backgroundColor: 'transparent', marginLeft: 10 },
                onPress: () => {}
              }
            ]}
          />
        </Row>
        <Modal.InnerModal
          style={salesResultStyles.openingModalStyle}
          headerStyle={{ marginBottom: 0 }}
          position={'center'}
          modalRef={'modalOpening'}
          animationDuration={0}
        >
          <View style={salesResultStyles.modalContainer}>
            <View style={salesResultStyles.modalTitleContainer}>
              <Text style={salesResultStyles.modalTitle}>{I18n.t('check_sale.register_container_message')}</Text>
            </View>
            <View style={salesResultStyles.modalItemContainer}>
              <View style={{ flex: 1 }}>
                <Text style={salesResultStyles.saleItemText}>{I18n.t('check_sale.change_fund')}</Text>
              </View>
              <View style={{ flex: 1, alignItems: 'flex-end' }}>
                <Text style={salesResultStyles.saleItemText}>{formatMoney(total)}</Text>
              </View>
            </View>
            <View style={salesResultStyles.modalItemContainer}>
              <View style={salesResultStyles.modalCenterContainer}>
                <TouchableOpacity style={salesResultStyles.approvalButton} onPress={() => Actions.home()}>
                  <Text style={{ color: '#fff' }}>{I18n.t('check_sale.approval')}</Text>
                </TouchableOpacity>
              </View>
              <View style={salesResultStyles.modalCenterContainer}>
                <TouchableOpacity style={salesResultStyles.resettingButton} onPress={() => Modal.close('modalOpening')}>
                  <Text style={{ color: '#000' }}>{I18n.t('check_sale.resetting')}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal.InnerModal>
        <NumberPadModal.NumberPadModalComponent />
      </Grid>
    )
  }
}
