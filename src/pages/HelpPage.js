import React, { Component } from 'react';
import { WebView } from 'react-native';
import BasePage from '../common/hocs/BasePage';

export default class HelpPage extends Component {
  render() {
    return (
      <BasePage>
        <WebView
          // TODO: 仮PDFの為、後程本番用PDFに変更
          source={require('../assets/help.pdf')}
        />
      </BasePage>
    );
  }
}
