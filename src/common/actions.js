import { createAction } from 'redux-actions'

export const setUserLanguage = createAction('setUserLanguage')
export const setUserToken = createAction('setUserToken')
export const setUserType = createAction('setUserType')
export const setIsHomepage = createAction('setIsHomepage')
export const setIsOpening = createAction('setIsOpening')
