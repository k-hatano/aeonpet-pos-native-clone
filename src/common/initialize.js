import I18n from 'i18n-js'
import DeviceInfo from 'react-native-device-info'
import { DEFAULT_LANGUAGE, setUserLocale, getUserLocale } from './models'
import { setUserLanguage } from './actions'

I18n.defaultLocale = 'ja'
I18n.fallbacks = true
I18n.translations = {
  ja: require('../assets/lang/ja.json'),
  en: require('../assets/lang/en.json')
}

export default async function (dispatch, locale = null) {
  const deviceLocale = DeviceInfo.getDeviceLocale().substring(0, 2)
  const userLocale = await getUserLocale()
  const appLocale = locale || userLocale || deviceLocale || DEFAULT_LANGUAGE
  await setUserLocale(appLocale)
  I18n.locale = appLocale
  dispatch(setUserLanguage({ userLanguage: appLocale }))
}
