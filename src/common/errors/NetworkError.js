import I18n from 'i18n-js'
import OrangeErrorBase from './OrangeErrorBase'

export default class NetworkError extends OrangeErrorBase {
  static TYPE = {
    ANY: 0,
    OFFLINE: 1,
    TIMEOUT: 2,
    CLIENT_ERROR: 3,
    SERVER_ERROR: 4,
    INVALID_REQUEST: 5,
    UNKNOWN: 6,
    UNAUTHORIZED: 7,
    BASIC_AUTH: 8
  }

  static typeToDefailtMessage (errorType, originError) {
    switch (errorType) {
      case this.TYPE.ANY:
        return I18n.t('message.J-10-E001')
      case this.TYPE.OFFLINE:
        return I18n.t('errors.offline')
      case this.TYPE.TIMEOUT:
        return I18n.t('message.A-09-E002')
      case this.TYPE.CLIENT_ERROR:
        // 基本的にクライアント起因のエラーが起こらないようにプログラムするので、4xx系エラーは想定外
        return I18n.t('message.A-00-E001', {error: originError})
      case this.TYPE.SERVER_ERROR:
        return I18n.t('errors.server_error')
      case this.TYPE.INVALID_REQUEST:
        return I18n.t('message.J-10-E001')
      case this.TYPE.UNKNOWN:
        return I18n.t('message.J-10-E001')
      case this.TYPE.UNAUTHORIZED:
        return I18n.t('errors.unauthorized')
      default:
        return I18n.t('message.J-10-E001')
    }
  }

  constructor (errorType = NetworkError.TYPE.ANY, message = undefined, originError = null) {
    super()
    this._message = message || NetworkError.typeToDefailtMessage(errorType, originError)
    this._type = errorType
  }

  _message = ''
  get message () {
    return this._message
  }

  _type = NetworkError.TYPE.UNKNOWN
  get type () {
    return this._type
  }

  /**
   * エラーとなっているが、レスポンスを受け取れなかっただけで成功した可能性がある場合にtrueを返す
   * @return boolean
   */
  get hasSuccessPossibility () {
    switch (this.type) {
      case NetworkError.TYPE.ANY:
      case NetworkError.TYPE.TIMEOUT:
      case NetworkError.TYPE.UNKNOWN:
      case NetworkError.TYPE.INVALID_REQUEST:
        return true
      case NetworkError.TYPE.CLIENT_ERROR: // 明示的にエラーレスポンスが帰ってきているので、成功した可能性なし
      case NetworkError.TYPE.SERVER_ERROR: // 明示的にエラーレスポンスが帰ってきているので、成功した可能性なし
      case NetworkError.TYPE.OFFLINE: // リクエストを送る前に判定するので、成功した可能性なし
      case NetworkError.TYPE.UNAUTHORIZED: // 明示的にエラーレスポンスが帰ってきているので、成功した可能性なし
      case NetworkError.TYPE.BASIC_AUTH: // 明示的にエラーレスポンスが帰ってきているので、成功した可能性なし
        return false
      default:
        return true
    }
  }
}
