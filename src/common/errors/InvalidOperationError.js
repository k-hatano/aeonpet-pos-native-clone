/*
オブジェクトの現在の状態に対して不正な操作が行われた場合に利用するエラー
 */
export default class InvalidOperationError extends Error {
  errorType = 'InvalidOperation'
}
