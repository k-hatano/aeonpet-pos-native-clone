import OrangeErrorBase from './OrangeErrorBase'
import logger from 'common/utils/logger'

export default class EquipError extends OrangeErrorBase {
  constructor (messageKey) {
    super(messageKey)
    if (!messageKey) {
      logger.fatal('No Error Message')
    }
  }
}
