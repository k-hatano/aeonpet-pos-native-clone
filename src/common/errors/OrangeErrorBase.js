import I18n from 'i18n-js'

export default class OrangeErrorBase {
  _messageKey = ''
  _messageArgs = {}
  /**
   * メッセージのキー名と引数を指定して、OrangeErrorBaseクラスの新しいインスタンスを生成します。
   * @param {String} messageKey メッセージキー
   * @param {Object} messageArgs メッセージ引数
   */
  constructor (messageKey, messageArgs = {}) {
    this._messageKey = messageKey
    this._messageArgs = messageArgs
  }

  get message () {
    return I18n.t(this._messageKey, this._messageArgs)
  }
}
