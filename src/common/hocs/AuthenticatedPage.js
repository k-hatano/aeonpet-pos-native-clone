import I18n from 'i18n-js'
import React from 'react'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { fetchApi } from '../api'
import defaultPage from './DefaultPage'
import { login, logout, isLoggedIn, getToken, getCurrentShopId, setCurrentShopId } from '../models'

export const authenticatedPageHoc = Page =>
  class AuthenticatedPage extends React.Component {
    async componentDidMount () {
      if (await isLoggedIn()) {
        this.props.refreshLogin()
      }
    }

    componentWillUpdate (nextProps, nextState) {
      const { userLanguage } = nextProps
      if (this.props.userLanguage !== userLanguage) {
        I18n.defaultLocale = userLanguage
        I18n.locale = userLanguage
      }
    }

    render () {
      return <Page {...this.props} />
    }
  }

const mapDispatchToProps = dispatch => ({
  refreshLogin: async (nextShopId = null) => {
    const shopId = nextShopId || (await getCurrentShopId())
    const token = await getToken()
    return fetchApi(dispatch, {
      key: '/login/refresh',
      method: 'put',
      url: '/devices/login/refresh',
      headers: {
        'X-Authorization': `Bearer ${token}`
      },
      data: {
        shop_id: shopId || undefined
      }
    })
      .then(async response => {
        if (nextShopId) {
          await setCurrentShopId(shopId)
        }
        const deviceData = Object.assign({}, response.data, { shop_id: shopId })
        await login(deviceData)
      })
      .catch(error => {
        logout()
        Actions.login()
        console.log(error)
      })
  }
})

const mapStateToProps = state => ({
  userLanguage: state.common.userLanguage
})

export default ({ Page }) =>
  defaultPage({
    Page: connect(mapStateToProps, mapDispatchToProps)(authenticatedPageHoc(Page))
  })
