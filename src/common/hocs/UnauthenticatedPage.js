import I18n from 'i18n-js'
import React from 'react'
import { connect } from 'react-redux'
import defaultPage from './DefaultPage'

export const unauthenticatedPageHoc = Page =>
  class UnauthenticatedPage extends React.Component {
    componentWillUpdate (nextProps, nextState) {
      const { userLanguage } = nextProps
      if (this.props.userLanguage !== userLanguage) {
        I18n.defaultLocale = userLanguage
        I18n.locale = userLanguage
      }
    }

    render () {
      return <Page {...this.props} />
    }
  }

const mapDispatchToProps = dispatch => ({})

const mapStateToProps = state => ({
  userLanguage: state.common.userLanguage
})

export default ({ Page }) =>
  defaultPage({
    Page: connect(mapStateToProps, mapDispatchToProps)(unauthenticatedPageHoc(Page))
  })
