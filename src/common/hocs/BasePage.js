import React, { Component } from 'react'
import { View, StatusBar, Dimensions } from 'react-native'
import LoadingView from '../components/widgets/LoadingView'
import PropTypes from 'prop-types'
import Modal from '../components/widgets/Modal'
import Header from 'common/containers/layout/Header'
import { isDevMode } from '../utils/environment'
import DebugModalView from '../../modules/debug/containers/DebugModalView'
import { DEFAULT_DESIGN_HEIGHT, DEFAULT_DESIGN_WIDTH } from '../components/ComponentDefinitions'

export default class BasePage extends Component {
  static propTypes = {
    showHeader: PropTypes.bool,
    headerProps: PropTypes.object,
    pageName: PropTypes.string
  }

  static defaultProps = {
    showHeader: true
  }

  constructor (props) {
    super(props)
    this.state = { ready: false }
  }

  _renderChildren () {
    if (this.props.children) {
      const children = this.props.children
      if (!children.length) {
        return this.props.children
      } else if (this.props.children.length === 2) {
        return (
          <View style={{flexDirection: 'row', alignItems: 'stretch', flex: 1}}>
            <View style={{flex: 1}}>
              {this.props.children[0]}
            </View>
            <View style={{width: 1, backgroundColor: '#979797'}} />
            <View style={{flex: 1}}>
              {this.props.children[1]}
            </View>
          </View>
        )
      } else {
        return this.props.children
      }
    }
    return null
  }

  render () {
    // 画面サイズに合わせてスケール (defaultWidthの幅前提でデザインされているため)
    const defaultWidth = DEFAULT_DESIGN_WIDTH
    const defaultHeight = DEFAULT_DESIGN_HEIGHT
    const windowSize = Dimensions.get('window')
    const scale = windowSize.width / defaultWidth

    // 単にscaleを記載するだけだと、小さい画面の時に右下に寄ってしまうので、位置修正をする
    const translateX = -(defaultWidth - windowSize.width) / 2 / scale
    const translateY = -(defaultHeight - windowSize.height) / 2 / scale

    return (
      <View
        style={{ width: defaultWidth, height: defaultHeight }}
        transform={[{scale}, {translateX}, {translateY}]}>
        <StatusBar barStyle="light-content" />
        <View style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          alignItems: 'stretch'}}>
          { !this.props.showHeader ? null : (
            <View style={{ height: 72 }}>
              <Header {...this.props.headerProps} />
            </View>
          )}
          <View style={{ flex: 1 }}>
            {this._renderChildren()}
          </View>
        </View>
        <Modal />
        <LoadingView />
        {isDevMode() ? <DebugModalView /> : null}
      </View>
    )
  }
}
