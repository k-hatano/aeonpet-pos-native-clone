import React from 'react'
import { View, StatusBar } from 'react-native'
import { MessageBar, MessageBarManager } from 'react-native-message-bar'
import ProgressBar from '../components/widgets/ProgressBar'
import AlertModal from '../components/widgets/AlertModal'
import PropTypes from 'prop-types'

class DefaultPage extends React.Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    content: PropTypes.element
  }

  constructor (props) {
    super(props)
    this.state = { ready: false }
  }

  componentDidMount () {
    MessageBarManager.registerMessageBar(this.refs.notificationSystem)
  }

  componentWillUnmount () {
    MessageBarManager.unregisterMessageBar()
  }

  render () {
    const { ready } = this.state
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden />
        {ready && <Page {...this.props} />}
        <MessageBar ref='notificationSystem' />
        <AlertModal.AlertModalComponent />
        { !this.props.isLoading ? null : (
          <View style={{
            backgroundColor: '#88888844',
            height: '100%',
            width: '100%',
            position: 'absolute',
            flex: 1}}>
            <ProgressBar.Component />
          </View>
        )}
      </View>
    )
  }
}


export default ({ Page }) =>
  class DefaultPage extends React.Component {
    static propTypes = {
      isLoading: React.PropTypes.bool
    }

    constructor (props) {
      super(props)
      this.state = { ready: false }
    }

    componentDidMount () {
      MessageBarManager.registerMessageBar(this.refs.notificationSystem)
    }

    componentWillUnmount () {
      MessageBarManager.unregisterMessageBar()
    }

    render () {
      const { ready } = this.state
      return (
        <View style={{ flex: 1 }}>
          <StatusBar hidden />
          {ready && <Page {...this.props} />}
          <MessageBar ref='notificationSystem' />
          <AlertModal.AlertModalComponent />
          { !this.props.isLoading ? null : (
            <View style={{
              backgroundColor: '#88888844',
              height: '100%',
              width: '100%',
              position: 'absolute',
              flex: 1}}>
              <ProgressBar.Component />
            </View>
          )}
        </View>
      )
    }
  }
