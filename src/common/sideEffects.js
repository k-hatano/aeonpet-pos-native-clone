import { Platform } from 'react-native'
import SensitiveInfo from 'react-native-sensitive-info'
import { takeEvery, all, select } from 'redux-saga/effects'
import ProgressBar from './components/widgets/ProgressBar'
import { fetchStart, fetchSuccess, fetchFailure } from './api'
import { createAction } from 'redux-actions'
import { updateSetting } from 'modules/setting/actions'
import { setCustomerDisplayText } from 'modules/customerDisplay/actions'
import { PLACEMENT, formatDisplayText, syncCustomerDisplay } from 'modules/customerDisplay/models'
import SettingDefinitions, { SETTING_TYPE } from '../modules/setting/models/SettingDefinitions'
import SettingStorage from '../modules/setting/models/SettingStorage'

function * onFetchStart ({ payload: { config } }) {
  // console.log('Fetch Start', config)
  ProgressBar.show()
}

function * onFetchSuccess ({ payload: { response, config } }) {
  // console.log('Fetch Success', config)
  ProgressBar.hide()
}

function * onFetchFailure ({ payload: { error, config } }) {
  // Notification.error(error.message)
  ProgressBar.hide()
}

function * watchFetchStart () {
  yield takeEvery(fetchStart.toString(), onFetchStart)
}
function * watchFetchSuccess () {
  yield takeEvery(fetchSuccess.toString(), onFetchSuccess)
}
function * watchFetchFailure () {
  yield takeEvery(fetchFailure.toString(), onFetchFailure)
}

function * onLoadingChanged () {
  const isLoading = yield select(state => state.common.isLoading)

  if (isLoading) {
    ProgressBar.show()
  } else {
    ProgressBar.hide()
  }
}

function * watchLoadStart () {
  yield takeEvery(loadStart.toString(), onLoadingChanged)
}

function * watchLoadEnd () {
  yield takeEvery(loadEnd.toString(), onLoadingChanged)
}

function * onUpdateSetting ({ payload: { key, value } }) {
  const definition = SettingDefinitions[key]
  if (definition) {
    if (definition.isSensitive) {
      if (definition.type !== SETTING_TYPE.STRING) {
        console.error('Can save only string value to SensitiveInfo')
      }
      if (Platform.OS === 'ios') {
        SensitiveInfo.setItem(key, value, {})
      } else {
        console.warn('Cannot Save To SensitiveInfo In Windows')
        SettingStorage.save(key, value, definition.type)
      }
    } else {
      SettingStorage.save(key, value, definition.type)
    }
  } else {
    console.error('Invalid setting key. Please define it in SettingKeys.js and SettingDefinitions.js.')
  }
}

function * onSetCustomerDisplayText ({ payload: content }) {
  if (Platform.OS === 'ios') { // 現段階ではWindows未対応
    syncCustomerDisplay(content)
  }
}

function * watchUpdateSetting () {
  yield takeEvery(updateSetting.toString(), onUpdateSetting)
}

function * watchSetCustomerDisplayText () {
  yield takeEvery(setCustomerDisplayText.toString(), onSetCustomerDisplayText)
}

export default getState => {
  function * rootSaga () {
    yield all([
      watchFetchStart(),
      watchFetchSuccess(),
      watchFetchFailure(),
      watchLoadStart(),
      watchLoadEnd(),
      watchUpdateSetting(),
      watchSetCustomerDisplayText()
    ])
  }
  return rootSaga
}

export const loadStart = createAction('loading_start')
export const loadEnd = createAction('loading_end')

export async function loading (dispatch, fetchingProcess) {
  dispatch(loadStart({config: {key: 'loading'}}))
  try {
    const ret = await fetchingProcess()
    dispatch(loadEnd({config: {key: 'loading'}}))
    return ret
  } catch (error) {
    dispatch(loadEnd({config: {key: 'loading'}}))
    throw error
  }
}
