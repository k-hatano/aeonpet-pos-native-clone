import Header from '../../components/layout/Header'

export default {
  name: 'Header',
  component: Header,
  properties: [
    {
      title: 'Default',
      property: {
        currentStaff: {
          name: '佐藤'
        }
      }
    },
  ],
  frames: [
    {
      title: 'W900',
      style: {width: 900}
    }
  ]
}
