import HeaderMenu from '../../components/layout/HeaderMenu'

export default {
  name: 'HeaderMenu',
  component: HeaderMenu,
  properties: [
    {
      title: 'Default',
      property: {
        menus: [
          {
            label: 'ドロアオープン',
            command: () => {
              console.log('Open Drawer !')
            }
          },
          {
            label: 'ログアウト',
            command: () => {
              console.log('Logout !')
            }
          }
        ]
      }
    }
  ],
  frames: [
    {
      title: '600 x 200',
      style: {width: 600, height: 200}
    }
  ]
}
