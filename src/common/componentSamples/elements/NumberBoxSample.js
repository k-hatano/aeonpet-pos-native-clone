import NumberBox from '../../components/elements/NumberBox'

export default {
  name: 'NumberBox',
  component: NumberBox,
  properties: [
    {
      title: 'Default',
      property: {
        amount: 1000,
        style: {},
        onChange: (number) => console.log('Number changed. num = ' + number)
      }
    },
    {
      title: 'ChangeStyle',
      property: {
        amount: 1000,
        style: {
          fontSize: 30,
          backgroundColor: '#dddddd',
          borderWidth: 2,
          width: 120,
          height: 40
        },
        onChange: (number) => console.log('Number changed. num = ' + number)
      }
    }
  ],
  frames: [
    {
      title: '80 x 36',
      style: {width: 80, height: 36}
    }
  ]
}
