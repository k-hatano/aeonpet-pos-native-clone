export const REPOSITORY_CONTEXT = {
  SAMPLE: 'sample',
  STANDARD: 'standard',
  TRAINING: 'training'
}

export default class AppEvents {
  /**
   * @private
   */
  static _onInitializeTableCallbacks = {}

  static onInitializeTable (key, callback) {
    this._onInitializeTableCallbacks[key] = callback
  }

  static async initializeTableAsync () {
    for (const i in this._onInitializeTableCallbacks) {
      await this._onInitializeTableCallbacks[i]()
    }
  }

  /**
   * @private
   */
  static _onRepositoryContextChangedCallbacks = {}

  static onRepositoryContextChanged (key, callback) {
    this._onRepositoryContextChangedCallbacks[key] = callback
  }

  static repositoryContextChange (context) {
    for (const i in this._onRepositoryContextChangedCallbacks) {
      this._onRepositoryContextChangedCallbacks[i](context)
    }
  }

  static _onSampleDataCreate = {}

  static async onSampleDataCreate (key, callback) {
    this._onSampleDataCreate[key] = callback
  }

  static async sampleDataCreateAsync () {
    for (const i in this._onSampleDataCreate) {
      await this._onSampleDataCreate[i]()
    }
  }
}
