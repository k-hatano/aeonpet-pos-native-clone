import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { reducer as formReducer } from 'redux-form'
import createSaga from './sideEffects'
import commonReducer from './reducers/commonReducer'
import routeReducer from './reducers/routeReducer'
import loginReducer from '../modules/login/reducers/loginReducer'
import cartReducer from '../modules/cart/reducers/cartReducer'
import balanceReducer from '../modules/balance/reducers/balanceReducer'
import settingReducer from '../modules/setting/reducers'
import authenticationReducer from '../modules/authentication/reducers'
import homeReducer from '../modules/home/reducers'
import productReducer from '../modules/product/reducers'
import promotionReducer from '../modules/promotion/reducers'
import stockReducer from '../modules/stock/reducers'
import stockAdjustmentReducer from '../modules/stockAdjustment/reducers'
import cashierTotalReducer from '../modules/cashierTotal/reducers'
import taxFreeReducer from '../modules/taxFree/reducers'
import syncDataReducer from '../modules/syncData/reducer'
import customerReducer from '../modules/customer/reducers'
import openSaleReducer from '../modules/openSale/reducers'
import order from '../modules/order/reducers'
import staffReducer from '../modules/staff/reducers'
import stockTakingReducer from '../modules/stockTaking/reducers'
import printerReducer from '../modules/printer/reducers/printerErrorReducer'
import debugReducer from '../modules/debug/reducers'
import stockMoveReducer from '../modules/stockMove/reducers'

const createMiddlewares = sagaMiddleware => {
  const middlewares = []
  if (sagaMiddleware) {
    middlewares.push(sagaMiddleware)
  }
  return applyMiddleware.apply({}, middlewares)
}

const createReducers = reducers => {
  return combineReducers({
    common: commonReducer,
    route: routeReducer,
    setting: settingReducer,
    form: formReducer,
    login: loginReducer,
    cart: cartReducer,
    balance: balanceReducer,
    authentication: authenticationReducer,
    home: homeReducer,
    order: order,
    product: productReducer,
    promotion: promotionReducer,
    stock: stockReducer,
    stockAdjustment: stockAdjustmentReducer,
    staff: staffReducer,
    customer: customerReducer,
    syncData: syncDataReducer,
    cashierTotal: cashierTotalReducer,
    stockTaking: stockTakingReducer,
    opensale: openSaleReducer,
    taxFree: taxFreeReducer,
    printer: printerReducer,
    debug: debugReducer,
    stockMove: stockMoveReducer,
    ...reducers
  })
}

const composeEnhancers = __DEV__ // eslint-disable-line no-undef
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  : compose
const buildStore = (reducers, initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(createReducers(reducers), initialState, composeEnhancers(createMiddlewares(sagaMiddleware)))

  if (module.hot) {
    module.hot.accept(() => {
      store.replaceReducer(createReducers(reducers))
    })
  }

  store.reducers = createReducers(reducers)
  sagaMiddleware.run(createSaga(store.getState))
  return store
}

export default buildStore()
