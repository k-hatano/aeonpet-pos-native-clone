import Sequelize from 'sequelize'

// Hack to make sequelize work
process.on = function () {}
global.Buffer = {isBuffer: () => { return false }}

const sequelize = new Sequelize(
  'database', '', '', {
    dialect: 'sqlite',
    // storage: 'main.db', // Use on memory sqlite
    logging: () => {}
  }
)

export {sequelize}
