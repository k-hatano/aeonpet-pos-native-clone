import { handleActions } from 'redux-actions'
import { fetchStart, fetchSuccess, fetchFailure } from '../api'
import * as actions from '../actions'
import { loadEnd, loadStart } from '../sideEffects'

const updateFetching = (fetching, payload, upDown) => {
  const { config } = payload
  const key = config.key || config.url
  return { ...fetching, ...{ [key]: (fetching[key] || 0) + upDown } }
}

const defaultState = {
  fetching: {},
  loadingCount: 0,
  isLoading: false,
  userLanguage: null,
  userToken: null,
  isHomePage: false,
  isOpening: false
}

const handlers = {
  [fetchStart]: (state, action) => ({
    ...state,
    ...{ fetching: updateFetching(state.fetching, action.payload, 1) }
  }),
  [fetchSuccess]: (state, action) => ({
    ...state,
    ...{ fetching: updateFetching(state.fetching, action.payload, -1) }
  }),
  [fetchFailure]: (state, action) => ({
    ...state,
    ...{ fetching: updateFetching(state.fetching, action.payload, -1) }
  }),
  [actions.setUserLanguage]: (state, action) => ({
    ...state,
    ...{ userLanguage: action.payload }
  }),
  [actions.setUserToken]: (state, action) => ({
    ...state,
    ...{ userToken: action.payload }
  }),
  [loadStart]: (state, action) => ({
    ...state,
    ...{
      loadingCount: state.loadingCount + 1,
      isLoading: true
    }
  }),
  [loadEnd]: (state, action) => ({
    ...state,
    ...{
      loadingCount: state.loadingCount - 1,
      isLoading: state.loadingCount > 1
    }
  }),
  [actions.setIsHomepage]: (state, action) => ({
    ...state,
    ...{ isHomePage: action.payload }
  }),
  [actions.setIsOpening]:  (state, action) => ({
    ...state,
    ...{ isOpening: action.payload }
  })
}

export default handleActions(handlers, defaultState)
