import configureMockStore from 'redux-mock-store'
import commonReducer from '../commonReducer'
import { setUserLanguage } from '../../actions'
import { fetchStart, fetchSuccess, fetchFailure } from '../../api'
import { loadEnd, loadStart } from '../../sideEffects'

const middlewares = []
const mockStore = configureMockStore(middlewares)
const initialState = {}
const store = mockStore(initialState)

afterEach(() => {
  store.clearActions()
})

describe('[Common] User Language Reducer Test', () => {
  it('Should dispatch setUserLanguage action', () => {
    store.dispatch(setUserLanguage('en'))
    const expectedPayload = {
      payload: 'en',
      type: 'setUserLanguage'
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle setUserLanguage', () => {
    expect(
      commonReducer({ userLanguage: null }, setUserLanguage('en'))
    )
      .toEqual({ userLanguage: 'en' })
  })
})

describe('[Common] Fetching Reducer Test', () => {
  it('Should dispatch fetchStart action', () => {
    store.dispatch(fetchStart({
      config: {
        key: '/demo',
        method: 'get',
        url: '/demo'
      }
    }))
    const expectedPayload = {
      payload: {
        config: {
          key: '/demo',
          method: 'get',
          url: '/demo'
        }
      },
      type: 'api_fetchStart'
    }
    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle fetchStart', () => {
    expect(
      commonReducer({ fetching: {} }, fetchStart({
        config: {
          key: '/demo1',
          method: 'get'
        }
      }))
    )
      .toEqual({ fetching: { '/demo1': 1 } })

    expect(
      commonReducer({ fetching: {} }, fetchStart({
        config: {
          method: 'get',
          url: '/demo2'
        }
      }))
    )
      .toEqual({ fetching: { '/demo2': 1 } })
  })

  it('Should dispatch fetchSuccess action', () => {
    store.dispatch(fetchSuccess({
      response: [1, 2],
      config: {
        key: '/demo',
        method: 'get',
        url: '/demo'
      }
    }))
    const expectedPayload = {
      payload: {
        response: [1, 2],
        config: {
          key: '/demo',
          method: 'get',
          url: '/demo'
        }
      },
      type: 'api_fetchSuccess'
    }
    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle fetchSuccess', () => {
    expect(
      commonReducer({ fetching: {} }, fetchSuccess({
        response: [1, 2],
        config: {
          key: '/demo1',
          method: 'get'
        }
      }))
    )
      .toEqual({ fetching: { '/demo1': -1 } })

    expect(
    commonReducer({ fetching: {} }, fetchSuccess({
      response: [1, 2],
      config: {
        method: 'get',
        url: '/demo2'
      }
    }))
  )
    .toEqual({ fetching: { '/demo2': -1 } })
  })

  it('Should dispatch fetchFailure action', () => {
    store.dispatch(fetchFailure({
      error: { message: 'Not found' },
      config: {
        key: '/demo',
        method: 'get',
        url: '/demo'
      }
    }))
    const expectedPayload = {
      payload: {
        error: { message: 'Not found' },
        config: {
          key: '/demo',
          method: 'get',
          url: '/demo'
        }
      },
      type: 'api_fetchFailure'
    }
    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle fetchFailure', () => {
    expect(
      commonReducer({ fetching: {} }, fetchFailure({
        error: { message: 'Not found' },
        config: {
          key: '/demo1',
          method: 'get'
        }
      }))
    )
      .toEqual({ fetching: { '/demo1': -1 } })

    expect(
      commonReducer({ fetching: {} }, fetchFailure({
        error: { message: 'Not found' },
        config: {
          method: 'get',
          url: '/demo2'
        }
      }))
    )
      .toEqual({ fetching: { '/demo2': -1 } })
  })
})

describe('[Common] Loading Reducer Test', () => {
  it('Should dispatch loadStart action', () => {
    store.dispatch(loadStart({ config: { key: 'loading' } }))
    const expectedPayload = {
      payload: {
        config: { key: 'loading' }
      },
      type: 'loading_start'
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle loadStart', () => {
    expect(
      commonReducer({ loadingCount: 0, isLoading: false }, loadStart({ config: { key: 'loading' } }))
    )
      .toEqual({ loadingCount: 1, isLoading: true })
  })

  it('Should dispatch loadEnd action', () => {
    store.dispatch(loadEnd({ config: { key: 'loading' } }))
    const expectedPayload = {
      payload: {
        config: { key: 'loading' }
      },
      type: 'loading_end'
    }

    expect(store.getActions())
      .toEqual([expectedPayload])
  })

  it('Should handle loadEnd', () => {
    expect(
      commonReducer({ loadingCount: 0, isLoading: false }, loadEnd({ config: { key: 'loading' } }))
    )
      .toEqual({ loadingCount: -1, isLoading: false })

    expect(
      commonReducer(
        commonReducer({ loadingCount: 0, isLoading: false }, loadStart({ config: { key: 'loading' } })),
        loadEnd({ config: { key: 'loading' } }))
    )
      .toEqual({ loadingCount: 0, isLoading: false })
  })
})
