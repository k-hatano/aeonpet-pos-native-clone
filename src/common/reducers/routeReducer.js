import { handleActions } from 'redux-actions'
import { ActionConst } from 'react-native-router-flux'

const defaultState = {
  scene: {}
}

const handlers = {
  [ActionConst.FOCUS]: (state, action) => ({
    ...state,
    ...{ scene: action.scene }
  }),
  [ActionConst.POP]: (state, action) => ({
    ...state,
    ...{ scene: action.scene }
  }),
  [ActionConst.ANDROID_BACK]: (state, action) => ({
    ...state,
    ...{ scene: action.scene }
  }),
  [ActionConst.RESET]: (state, action) => ({
    ...state,
    ...{ scene: action.scene }
  })
}

export default handleActions(handlers, defaultState)
