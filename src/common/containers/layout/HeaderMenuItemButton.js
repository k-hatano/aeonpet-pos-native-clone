import HeaderMenuItemButton from '../../components/layout/HeaderMenuItemButton'
import connectPermissionButton from 'modules/staff/containers/connectPermissionButton'

export default connectPermissionButton(HeaderMenuItemButton)