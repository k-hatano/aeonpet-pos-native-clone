import { connect } from 'react-redux'
import Header from '../../components/layout/Header'
import SettingKeys from '../../../modules/setting/models/SettingKeys'
import DrawerOpenReceiptBuilder, { saveDrawerOpenEJournal } from '../../models/DrawerOpenReceiptBuilder'
import PrinterManager from 'modules/printer/models/PrinterManager'
import { saveOperationLog } from 'modules/home/service'
import { OPERATION_TYPE } from 'modules/home/models'
import { makeDateRange } from '/common/utils/dateTime'
import * as actions from 'modules/home/actions'
import OperationLogRepository from 'modules/home/repositories/OperationLogRepository'
import { MODULE_NAME as staffModuleName } from '/modules/staff/models'
import AlertView from 'common/components/widgets/AlertView'
import I18n from 'i18n-js'
import CashierTotalRepository from 'modules/cashierTotal/repositories/CashierTotalRepository'
import { loading } from 'common/sideEffects'
import CashierTotalReceiptBuilder from 'modules/cashierTotal/models/CashierTotalReceiptBuilder'
import moment from 'moment'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import { RECEIPT_TYPE } from 'modules/printer/models'
import { makeCashierTotalEJournal } from 'modules/cashierTotal/services'
import logger from 'common/utils/logger'
import { Actions } from 'react-native-router-flux'
import ConfirmView from 'common/components/widgets/ConfirmView'
import CashierRepository from '../../../modules/cashier/repositories/CashierRepository'
import { getCommonNetworkErrorMessage } from '../../errors'
import { AGGREGATE_TYPE } from 'modules/cashierTotal/models'
import { reprintCloseSale } from '../../../modules/cashierTotal/services'
import { showDebugBarcodeInput } from '../../../modules/barcodeReader/services'

const mapDispatchToProps = dispatch => ({
  onOpenDrawer: async () => {
    try {
      const date = new Date()
      const builder = new DrawerOpenReceiptBuilder()
      await builder.initializeAsync()
      const receipt = builder.buildDrawerOpenReceipt(date)
      await PrinterManager.print(receipt, true)
      await saveOperationLog(OPERATION_TYPE.OPEN_DRAWER, date)
      await saveDrawerOpenEJournal(dispatch, receipt)
      const { from, to } = makeDateRange(date, date)
      const operationLogs = await OperationLogRepository.findByDate(from, to)
      dispatch(actions.listOperationLogs(operationLogs))
    } catch (error) {
    }
  },
  onSelectSetting: async (receiptPrintNum) => {
    Actions.setting({isOpening: true})
  },
  onReprintCloseSales: async (receiptPrintNum) => {
    try {
      const cashier = await CashierRepository.find()
      const reprintResult = await loading(dispatch, async () => {
        return reprintCloseSale(cashier, receiptPrintNum)
      })
      for (const message of reprintResult.messages) {
        AlertView.showAsync(message)
      }
    } catch (error) {
      // try内のメッセージ表示処理で例外メッセージは表示されるはず。
    }
  },
  onForcedCloseSales: async (receiptPrintNum) => {
    const isOk = await ConfirmView.showAsync(I18n.t('message.G-02-I003'))
    if (!isOk) return
    try {
      const messages = []
      const prepareSuccess = await loading(dispatch, async () => {
        let unclosedCount
        try {
          unclosedCount = await CashierRepository.fetchUnclosedCount()
        } catch (error) {
          messages.push(getCommonNetworkErrorMessage(error))
          return false
        }

        if (unclosedCount && unclosedCount.cashier_count === 0) {
          messages.push(I18n.t('message.G-02-E004'))
          return false
        }
        logger.info('unclosed cashier count = ' + unclosedCount && unclosedCount.cashier_count)
        return true
      })

      for (let i in messages) {
        await AlertView.showAsync(messages[i])
      }

      if (!prepareSuccess) return

      const result = await loading(dispatch, async () => {
        const messages = []
        const createdAt = new Date()
        const cashierTotals = await CashierTotalRepository.fetchForcedCloseSales()
        await saveOperationLog(OPERATION_TYPE.FORCED_CLOSE_SALE, createdAt)
        for (let i = 0; i < cashierTotals.length; i++) {
          const cashierTotal = cashierTotals[i]
          const builder = new CashierTotalReceiptBuilder()
          await builder.initializeAsync()
          const receipt = builder.buildCashierTotalReceipt(cashierTotal, createdAt, RECEIPT_TYPE.CLOSE_SALES)
          // 印字枚数の数だけ繰り返す。
          for (let j = 0; j < receiptPrintNum; j++) {
            const eJournal = makeCashierTotalEJournal(
              receipt, cashierTotal, RECEIPT_TYPE.CLOSE_SALES, moment(createdAt).unix()
            )
            await EJournalRepository.save(eJournal)

            try {
              await PrinterManager.print(receipt)
              // 印刷に成功した場合のみ、印刷済みとする。
              // 失敗したらエラーにして、処理を続行する。
              await EJournalRepository.saveIsPrintedById(eJournal.id)
            } catch (error) {
              logger.warning('forcedCloseSales - print receipt error. ' + error.message)
              // エラーは通知しない
              messages.push(I18n.t('message.G-02-E001'))
            }

            try {
              await EJournalRepository.pushById(eJournal.id)
            } catch (error) {
              // 電子ジャーナルの送信に失敗してもエラーは通知しない。
              logger.warning('forcedCloseSales - push eJournal error. ' + error.message)
            }
          }
        }
        return {messages: messages}
      })
      for (let i in result.messages) {
        await AlertView.showAsync(result.messages[i])
      }
      Actions.openSales()
    } catch (error) {
      AlertView.show(getCommonNetworkErrorMessage(error))
    }
  },
  onSelectDataSync: async () => {
    Actions.syncData({
      isImmediateExec: true,
      canBack: true,
      hideBackButton: true
    })
  },
  onOpenDebugBarcodeInput: async () => {
    showDebugBarcodeInput(dispatch)
  }
})

const mapStateToProps = state => ({
  currentStaff: state.setting.settings[SettingKeys.STAFF.CURRENT_STAFF],
  printerErrorsCount: state.home.printerErrorsCount,
  repositoryContext: state.setting.settings[SettingKeys.COMMON.REPOSITORY_CONTEXT],
  receiptPrintNum: state.setting.settings[SettingKeys.RECEIPT.CASHIER_TOTAL.PRINT_NUM],
  permissions: state[staffModuleName].permissions,
  isFixStaff: state.setting.settings[SettingKeys.STAFF.IS_FIX_STAFF],
  isStockManagementMode: !state.setting.settings[SettingKeys.COMMON.CASHIER_ID]
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  return {
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    onForcedCloseSales: async () => {
      dispatchProps.onForcedCloseSales(
        stateProps.receiptPrintNum
      )
    },
    onReprintCloseSales: async () => {
      return dispatchProps.onReprintCloseSales(
        stateProps.receiptPrintNum
      )
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Header)
