import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

const styles = StyleSheet.create({
  selectWrapper: {
    position: 'absolute',
    width: '100%',
    top: 8
  },
  selectDropdown: {
    backgroundColor: '#fff',
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:1, width: 1}

  },
  selectInput: {
    height: 50,
    borderWidth: 1,
    borderColor: '#979797',
    backgroundColor: '#fff',
    marginTop: 5,
    marginBottom: 5,
    zIndex: 1000
  },
  selectOption: {
    top: 0
  },
  selectValue: {
    padding: 8,
    height: '100%',
    fontSize: 25,
    color: baseStyleValues.mainTextColor
  },
  listValue: {
    padding: 8,
    fontSize: 24,
    color: baseStyleValues.mainTextColor
  },
  selectMark: {
    color: baseStyleValues.mainTextColor,
    fontSize: 20,
    textAlign: 'center',
    margin: 5
  }
})

export default styles
