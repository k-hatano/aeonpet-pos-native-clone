import { StyleSheet } from 'react-native'
import baseStyleValues from '../baseStyleValues'

export default StyleSheet.create({
  datePickerInput: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    height: 46,
    backgroundColor: baseStyleValues.mainBackgroundColor,
    borderWidth: 1,
    borderColor: '#979797',
    paddingLeft: 20,
    width: 320
  },
  datePickerTouchable: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute'
  },
  datePickerTitle: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  datePickerConfirm: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#44bcb9',
    color: '#ffffff',
    padding: 15
  },
  datePickerCancel: {
    fontFamily: 'Helvetica',
    fontSize: 24,
    fontWeight: 'bold',
    backgroundColor: '#ffffff',
    color: '#44bcb9'
  }
})
