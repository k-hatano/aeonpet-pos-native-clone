import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    width: undefined,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  searchIcon: {
    position: 'absolute',
    left: 0,
    marginLeft: 10,
    zIndex: 1,
    backgroundColor: '#f0f0f0'
  },
  searchInputWrapper: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    flex: 1
  },
  searchInput: {
    borderWidth: 2,
    paddingLeft: 34,
    height: 34,
    borderRadius: 1
  },
  noIconSearchInput: {
    paddingLeft: 7
  }
})

export default styles
