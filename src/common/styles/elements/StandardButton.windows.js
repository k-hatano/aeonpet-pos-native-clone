import { StyleSheet } from 'react-native'
import commonStyle from './StandardButton.common'
import mergeStyle from '../../utils/mergeStyle'

const styles = StyleSheet.create(
  mergeStyle(commonStyle,
    {
      standardTouchable: {
        width: '100%'
      }
    })
)

export default styles
