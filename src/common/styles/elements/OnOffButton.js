import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  layoutRoot: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    width: 80,
    borderRadius: 2,
    backgroundColor: '#d4d4d4'
  }
})

export default styles
