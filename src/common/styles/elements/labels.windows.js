import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  numberInputLabelBorder: {
    borderWidth: 2,
    borderColor: '#ddd',
    width: 80,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  numberInputLabelText: {
    marginRight: 5,
    textAlign: 'right'
  }
})

export default styles
