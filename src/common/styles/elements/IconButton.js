import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 5
  }
})
