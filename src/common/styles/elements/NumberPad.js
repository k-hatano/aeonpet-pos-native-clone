import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  componentContainer: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    padding: 5
  },
  resultSide: {
    backgroundColor: '#4a4a4a',
    borderWidth: 0,
    alignItems: 'flex-end',
    justifyContent: 'space-around',
    paddingRight: 9,
    flex: 1 / 7,
    margin: 5
  },
  resultText: {
    height: 48,
    color: '#ffffff',
    fontSize: 100,
    fontWeight: '500'
  },
  featureSide: {
    flex: 1 / 7,
    flexDirection: 'row'
  },
  featureButton: {
    width: '24%',
    height: '90%',
    borderColor: '#979797',
    borderWidth: 1,
    borderRadius: 10,
    margin: 5
  },
  featureButtonTouch: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  featureButtonText: {
    fontSize: 48,
    color: '#4a4a4a',
    fontWeight: '500'
  },
  confirm: {
    flex: 1 / 7,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  confirmTouchText: {
    fontSize: 40,
    height: 40,
    fontFamily: 'Helvetica',
    fontWeight: 'bold'
  },
  pointConfirmTouchText: {
    fontSize: 25,
    fontFamily: 'Helvetica',
    height: 25
  },
  featureFunctionSide: {
    flex: 2 / 7,
    flexDirection: 'column'
  },
  additionalTitleContainer: {
    height: 60
  },
  additionalTitleText: {
    fontSize: 24,
    height: 30,
    fontWeight: 'bold',
    color: '#4a4a4a',
    marginLeft: 26
  }
})
