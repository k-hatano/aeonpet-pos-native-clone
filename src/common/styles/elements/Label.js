import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  background: {
    justifyContent: 'center',
    alignItems: 'center'
  }
})
