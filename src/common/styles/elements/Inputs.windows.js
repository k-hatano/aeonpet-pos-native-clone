import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  radioGroupContainer: {
    flexDirection: 'row'
  },
  radioGroupOption: {
    margin: 5,
    backgroundColor: 'transparent'
  },
  radioGroupOptionSign: {
    height: 24,
    width: 24,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#979797',
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 7
  },
  radioGroupOptionSelectedSign: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: '#333333'
  },
  radioGroupOptionText: {
    fontSize: 24,
    fontFamily: 'HiraginoSans-W3',
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  checkedStyle: {
    width: 30,
    height: 22,
    marginLeft: 10,
    marginBottom: 5
  },
  radioGroupOptionSubText: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 18,
    color: '#4a4a4a',
    letterSpacing: -0.29,
    marginTop: 20
  },
  radioGroupOptionSubTextWrapper: {
    marginLeft: 33
  },
  mainInputWrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})
