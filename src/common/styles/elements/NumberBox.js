import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 30,
    borderWidth: 1,
    borderColor: '#979797',
    justifyContent: 'center',
    padding: 3
  },
  mainText: {
    fontSize: 20,
    color: baseStyleValues.mainTextColor,
    textAlign: 'right'
  }
})
