import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  textInput: {
    height: 40,
    borderWidth: 1,
    width: 350,
    margin: 20
  },
  calendar: {

  },
  calendarContainer: {
    flexDirection: 'row',
    width: 600,
    height: 310,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height: 1, width: 1}
  }
})
