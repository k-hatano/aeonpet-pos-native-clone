import BaseStyleValues from '../baseStyleValues'

const buttonBase = {
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 10
}

export default {
  standardButtonContainer: {
    borderRadius: 10,
    backgroundColor: '#ff9024',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  standardBorderButton: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    borderColor: '#ff9024',
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  standardButtonText: {
    color: '#ffffff',
    fontSize: 20
  },
  standardBorderButtonText: {
    color: '#ff9024',
    fontSize: 20
  },
  standardTouchable: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5
  },
  commandButton: {
    ...buttonBase,
    backgroundColor: BaseStyleValues.mainButtonColor,
    borderWidth: 0
  },
  commandButtonDisabled: {
    ...buttonBase,
    backgroundColor: BaseStyleValues.disabledColor,
    borderWidth: 0
  },
  commandButtonText: {
    color: '#ffffff',
    fontSize: 20
  },
  commandButtonTextDisabled: {
    color: '#ffffff',
    fontSize: 20
  },
  optionalCommandButton: {
    ...buttonBase,
    backgroundColor: 'white',
    borderColor: BaseStyleValues.mainButtonColor,
    borderWidth: 3
  },
  optionalCommandButtonDisabled: {
    ...buttonBase,
    backgroundColor: 'white',
    borderColor: BaseStyleValues.disabledColor,
    borderWidth: 3
  },
  optionalCommandButtonText: {
    color: BaseStyleValues.mainButtonColor,
    fontSize: 20
  },
  optionalCommandButtonTextDisabled: {
    color: BaseStyleValues.disabledColor,
    fontSize: 20
  },
  proceedButton: {
    ...buttonBase,
    backgroundColor: BaseStyleValues.subButtonColor,
    borderWidth: 0
  },
  proceedButtonDisabled: {
    ...buttonBase,
    backgroundColor: BaseStyleValues.disabledColor,
    borderWidth: 0
  },
  proceedButtonText: {
    color: '#ffffff',
    fontSize: 20
  },
  proceedButtonTextDisabled: {
    color: '#ffffff',
    fontSize: 20
  },
  weakProceedButton: {
    ...buttonBase,
    backgroundColor: 'white',
    borderColor: BaseStyleValues.subButtonColor,
    borderWidth: 3
  },
  weakProceedButtonDisabled: {
    ...buttonBase,
    backgroundColor: 'white',
    borderColor: BaseStyleValues.disabledColor,
    borderWidth: 3
  },
  weakProceedButtonText: {
    color: BaseStyleValues.subButtonColor,
    fontSize: 20
  },
  weakProceedButtonTextDisabled: {
    color: BaseStyleValues.disabledColor,
    fontSize: 20
  },
  unSelectedButtonText: {
    color: BaseStyleValues.mainTextColor,
    fontSize: 24,
    fontFamily: 'HiraginoSans-W3'
  },
  unSelectedButton: {
    ...buttonBase,
    backgroundColor: buttonBase.subBackgroundColor,
    borderColor: buttonBase.subTextColor,
    borderWidth: 1
  }
}
