import { StyleSheet } from 'react-native'
import commonStyle from './StandardButton.common'

export default StyleSheet.create(commonStyle)
