import { StyleSheet } from 'react-native'
import baseStyleValues from './baseStyleValues'

export default StyleSheet.create({
  mainText: {
    color: baseStyleValues.mainTextColor,
    fontSize: 24
  },
  commonText: {
    color: baseStyleValues.mainTextColor,
    fontSize: 18
  },
  subText: {
    color: baseStyleValues.subTextColor,
    fontSize: 18
  },
  strongTextStyle: {
    color: baseStyleValues.mainTextColor,
    fontSize: 24,
    fontWeight: 'bold'
  }
})
