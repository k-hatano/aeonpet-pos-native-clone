import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    height: 72,
    backgroundColor: '#fd7c1c',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5
  },
  layoutRootForTraining: {
    backgroundColor: baseStyleValues.subButtonColor
  },
  layoutRootForCaution: {
    backgroundColor: 'red'
  },
  leftArea: {
    flex: 1,
    flexDirection: 'row'
  },
  centerArea: {
  },
  rightArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  iconImageButton: {
    width: 72,
    height: 72
  },
  iconImagePrinterButton: {
    width: 40,
    height: 72
  },
  iconImagePendButton: {
    width: 40,
    height: 72
  },
  appName: {
    color: 'white',
    fontSize: 36,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  pageName: {
    color: 'white',
    fontSize: 24,
    marginTop: 25
  },
  separator: {
    width: 1,
    backgroundColor: 'white',
    marginVertical: 12,
    marginHorizontal: 10
  },
  text: {
    fontSize: 24,
    lineHeight: 36,
    marginRight: 16,
    paddingTop: 6,
    color: 'white',
    alignSelf: 'center'
  },

  // Icons
  backIcon: {
    width: 32,
    height: 32
  },
  homeIcon: {
    width: 32,
    height: 26
  },
  userIcon: {
    width: 36,
    height: 36
  },
  menuIcon: {
    width: 31,
    height: 26
  },
  printerIcon: {
    width: 34,
    height: 31
  },
  pendIcon: {
    width: 34,
    height: 31
  },
  exclamationIcon: {
    width: 5,
    height: 16,
    marginTop: 35
  },

  // Version Modal
  VersionModal: {
    width: 640,
    height: 302
  },
  VersionModalDeviceNameArea: {
    flex: 3
  },
  VersionModalDeviceNameText: {
    fontSize: 36,
    fontWeight: 'bold',
    marginLeft: 10
  },
  VersionModalVersionArea: {
    flex: 2
  },
  VersionModalVersionText: {
    fontSize: 24,
    marginLeft: 10
  },
  VersionModalCopyRightArea: {
    flex: 1
  },
  VersionModalCopyRightText: {
    fontSize: 14,
    marginLeft: 10
  },

  countWrapper: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  defaultCountWrapperStyle: {
    width: 23,
    height: 23,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff0000',
    borderRadius: 15
  },
  defaultCountTextStyle: {
    fontFamily: 'Helvetica-Bold',
    fontSize: 15,
    color: '#ffffff',
    letterSpacing: -0.29
  }
})
