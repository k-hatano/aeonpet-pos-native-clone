import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#FE9036',
    flex: 1,
    zIndex: 999
  },
  rightContainer: {
    flex: 1,
    height: 72
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  backgroundImage: {
    width: 147,
    height: 32,
    resizeMode: 'cover',
    marginLeft: 15
  },
  leftContainer: {
    padding: 5,
    alignItems: 'flex-end'
  },
  normalButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  },
  iconButton: {
    backgroundColor: 'transparent',
    marginLeft: 12,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconButtonTextStyle: {
    color: '#fff',
    fontSize: 16
  },
  textButton: {
    color: '#fff',
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10
  },
  staffName: {
    flexDirection: 'row',
    marginRight: 10
  },
  textName: {
    color: '#fff'
  },
  buttonInPopupMore: {
    backgroundColor: 'transparent',
    minWidth: 120,
    borderBottomWidth: 1,
    borderColor: '#D5D4D8',
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10
  },
  popupMoreTooltip: {
    position: 'absolute',
    top: 55,
    right: 5,
    borderColor: '#D5D4D8',
    backgroundColor: '#fff',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 5,
    borderRadius: 2
  },
  absolute: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  backDrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },
  popupVersionContainer: {
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1
  },
  imageIconBack: {
    width: 30,
    height: 32,
    marginLeft: 28
  },
  imageIconHome: {
    width: 33,
    height: 26,
    marginLeft: 31
  },
  headerButtonSeparator: {
    width: 1,
    height: 30,
    backgroundColor: '#ffffff',
    marginRight: 15
  },
  headerLogoWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerLogoTitle: {
    fontSize: 36,
    color: '#fff',
    letterSpacing: -0.58,
    fontFamily: 'Helvetica-Bold'
  }
})
