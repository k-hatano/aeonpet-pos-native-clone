import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    width: 300,
    shadowOpacity: 0.75,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:1, width: 1}
  },
  itemRoot: {
    height: 64,
    justifyContent: 'center',
    padding: 20,
    backgroundColor: 'white'
  },
  separator: {
    height: 1,
    backgroundColor: '#d8d8d8',
    marginHorizontal: 20
  }
})
