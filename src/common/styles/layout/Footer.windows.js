import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  backgroundContainer: {
    flexDirection: 'row',
    backgroundColor: '#4d4d4d',
    flex: 1,
    padding: 5,
    height: 50,
    width: undefined
  },
  leftChilds: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  rightChilds: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
})

export default styles
