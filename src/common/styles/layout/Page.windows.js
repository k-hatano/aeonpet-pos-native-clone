import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  basicLayoutRootStyle: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchCustomer: {
    width: 300,
    height: 25,
    borderColor: '#808080',
    borderWidth: 1,
    marginRight: 5
  },
  container: {
    flexDirection: 'row'
  },
  leftPanel: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  recordItem: {
    height: 40,
    width: 100,
    backgroundColor: '#dddddd',
    marginLeft: 6,
    marginRight: 6,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#979797',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainImage: {
    width: 270
  },
  calendarPanel: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: 400
  },
  bottomToolbar: {
    flex: 1,
    backgroundColor: '#ccc',
    position: 'absolute',
    left: 0,
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'stretch'
  },
  btnShowToolbar: {
    marginRight: 0
  },
  containerButtonToolbar: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    backgroundColor: 'transparent'
  },
  orangeButton: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row'
  },
  tooltipButton: {
    position: 'absolute',
    right: 0,
    borderColor: '#000',
    borderWidth: 1,
    height: 50,
    top: -50,
    width: 200,
    paddingLeft: 10,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  bottomToolbarContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent'
  },
  coverPageContainer: {
    position: 'absolute',
    zIndex: 999,
    width: 0,
    height: 0
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover'
  }
})

export default styles
