export default {
  mainColor: '#ff9024',
  mainButtonColor: '#ff9024',
  subButtonColor: '#44bcb9',
  disabledColor: '#d8d8d8',
  mainBackgroundColor: '#ffffff',
  subBackgroundColor: '#f0f0f0',
  mainTextColor: '#4a4a4a',
  subTextColor: '#9b9b9b',
  borderColor: '#979797',
  separatorColor: '#d8d8d8'
}
