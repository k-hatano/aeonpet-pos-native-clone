import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  recordList: {
    backgroundColor: 'transparent'
  },
  categoriesList: {
    paddingTop: 5,
    paddingBottom: 5,
    padding: 1,
    backgroundColor: '#f7f7f7'
  },
  subListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'stretch',
    justifyContent: 'space-between'
  },
  container: {
    flexDirection: 'row',
    width: '100%'
  },
  productContainer: {
    flex: 3
  },
  categoriesContainer: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 10
  },
  breadcrumbs: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  breadcrumbText: {
    fontSize: 20
  },
  breadcrumbButton: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  categoriesView: {
    flex: 1,
    marginTop: 4,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  categoryItem: {
    width: '100%',
    height: 64,
    marginLeft: 1,
    backgroundColor: '#f7f7f7',
    paddingLeft: 20,
    borderBottomWidth: 1,
    borderColor: '#d8d8d8'
  },
  categoryItemTouch: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  }
})

export default styles
