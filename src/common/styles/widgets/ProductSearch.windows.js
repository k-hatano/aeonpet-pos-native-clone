import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 15
  },
  productListPagerContainer: {
    flexDirection: 'column'
  },
  productListPagerContainerContent: {
    flex: 1,
    flexDirection: 'row'
  },
  searchProductPanel: {
    flex: 1,
    alignItems: 'center'
  },
  searchTitle: {
    fontSize: 18,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 5
  },
  listHeader: {
    width: undefined,
    height: 30,
    marginRight: 10,
    flexDirection: 'row'
  },
  headerBreadcrumbs: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: '#bbb',
    flexDirection: 'row'
  },
  breadcrumbsItem: {
    margin: 5
  },
  currentBreadcrumbsItem: {
    fontWeight: '500'
  },
  headerTitle: {
    marginRight: 0,
    width: 32,
    height: 32
  },
  recordListContainer: {
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  recordItem: {
    height: 90,
    backgroundColor: '#f3f3f3',
    padding: 15,
    borderWidth: 1,
    borderColor: '#bbb',
    flexDirection: 'column',
    marginBottom: 10
  },
  recordItemTitle: {
    backgroundColor: 'transparent'
  },
  recordItemName: {
    fontSize: 20,
    color: '#4a4a4a'
  },
  listViewSeperator: {
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
    height: 2,
    backgroundColor: '#d8d8d8'
  },
  productList: {
    marginTop: 10,
    width: undefined,
    flex: 1
  },
  recordItemRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  recordInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  recordItemStock: {
    color: '#9b9b9b'
  },
  recordItemLeft: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  }
})
