import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  recordItem: {
    height: 95,
    backgroundColor: '#f0f0f0',
    marginBottom: 8,
    marginRight: 2,
    padding: 8,
    borderWidth: 1,
    borderColor: '#979797',
    flexDirection: 'column'
  },
  recordItemTitle: {
    backgroundColor: 'transparent'
  },
  recordItemTitleText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  recordItemInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  recordItemStock: {
    color: '#9b9b9b'
  },
  recordItemWarning: {
    color: '#ff0000'
  }
})

export default styles
