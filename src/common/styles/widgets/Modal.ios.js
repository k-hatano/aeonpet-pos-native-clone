import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    marginBottom: 58
  },
  titleWrapper: {
    flex: 9,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignSelf: 'flex-start'
  },
  emptyLine: {
    flex: 1
  },
  titleLine: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleImage: {
    width: 18,
    height: 21,
    marginRight: 15,
    marginLeft: 32
  },
  titleText: {
    fontFamily: 'HiraginoSans-W6',
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38,
    fontWeight: '500'
  },
  buttonCloseWrapper: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  },
  imageCloseWrapper: {
    width: 48,
    height: 48,
    backgroundColor: '#f0f0f0',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonCloseImage: {
    width: 24,
    height: 24
  },
  modalWrapper: {
    borderWidth: 0,
    backgroundColor: 'transparent'
  },
  modalWrapperShadow: {
    position: 'absolute',
    width: '103%',
    height: '104%',
    top: '-2%',
    left: '-1.5%',
    zIndex: 0
  },
  modalContentWrapper: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    zIndex: 1
  }
})
