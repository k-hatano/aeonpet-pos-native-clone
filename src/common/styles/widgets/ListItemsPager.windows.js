import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  pagerControl: {
    position: 'absolute',
    width: 25,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  pagerControlText: {
    color: '#fff',
    fontSize: 20,
    paddingBottom: 5
  },
  recordList: {
    paddingTop: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  subListContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'stretch'
  }
})

export default styles
