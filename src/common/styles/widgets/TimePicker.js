import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  pickerContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  picker: {
    width: 100,
    justifyContent: 'center'
  }
})
