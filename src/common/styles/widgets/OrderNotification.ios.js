import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 0,
    padding: 0,
    borderWidth: 1,
    borderColor: '#979797',
    borderBottomWidth: 0
  },
  scrollContentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 1
  },
  scrollContainer: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%'
  },
  notify: {
    width: '100%',
    borderBottomWidth: 2,
    borderColor: '#d8d8d8',
    flexDirection: 'row',
    paddingTop: 24,
    paddingBottom: 22,
    backgroundColor: 'transparent'
  },
  notifyName: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  notifyDate: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  headerWrapper: {
    flexDirection: 'row',
    height: 64,
    backgroundColor: '#95989a',
    justifyContent: 'center'
  },
  headerTitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerTitleText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  headerDeleteAllWrapper: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  headerDeleteAllButton: {
    backgroundColor: '#44bcb9',
    borderWidth: 2,
    borderColor: '#ffffff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 16,
    padding: 13,
    paddingTop: 8,
    paddingBottom: 5
  },
  headerDeleteAllButtonText: {
    fontSize: 24,
    color: '#ffffff',
    letterSpacing: -0.38
  },
  listLeftText: {
    fontSize: 18,
    color: '#9b9b9b',
    letterSpacing: -0.29
  },
  listRightText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  }
})
