import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  tabWrapper: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 67,
    marginBottom: 13
  },
  tabHeader: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    height: 67,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    borderBottomWidth: 6,
    borderColor: 'transparent'
  },
  tabName: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  tabContentWrapper: {
    backgroundColor: 'transparent',
    flex: 1,
    overflow: 'hidden'
  },
  tabContent: {
    backgroundColor: 'transparent',
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  defaultActiveBackgroundStyle: {
    borderBottomWidth: 6,
    borderColor: '#ff9024'
  },
  defaultActiveTextStyle: {
    color: '#000000'
  },
  defaultTabStyle: {
    paddingLeft: 0,
    height: 80,
    flex: 1,
    backgroundColor: 'transparent'
  },
  defaultErrorWrapperStyle: {
    width: 23,
    height: 23,
    marginLeft: 3,
    marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff0000',
    borderRadius: 15
  },
  defaultErrorTextStyle: {
    fontFamily: 'Helvetica-Bold',
    fontSize: 15,
    color: '#ffffff',
    letterSpacing: -0.29
  }
})
