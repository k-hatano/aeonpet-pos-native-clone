import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  rowView: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 24,
    paddingRight: 24,
    flex: 1,
    height: 60
  },
  rowTextTitle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  rowTextPrice: {
    width: 30
  },
  rowTextPriceNumber: {
    flex: 2
  },
  rowTextPriceIcon: {
    flex: 1
  },
  separator: {
    width: '97%',
    alignSelf: 'center',
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8e8e8e'
  },
  orderStatus: {
    width: 24,
    height: 24,
    marginRight: 10
  },
  watchControlContainer: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    height: '100%',
    flex: 1
  },
  tabPanelActiveBackgroundStyle: {
    borderBottomWidth: 6,
    borderColor: '#ff9024'
  },
  tabPanelActiveTextStyle: {
    color: '#000000',
    marginTop: 6
  },
  tabPanelTabStyle: {
    paddingLeft: 0,
    height: 80,
    flex: 1,
    backgroundColor: '#ffffff'
  },
  tabPanelTabWrapperStyle: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 67,
    marginBottom: 13
  },
  particularTabStyle: {
    height: 67,
    backgroundColor: 'transparent'
  },
  particularTabNameStyle: {
    fontFamily: 'HiraginoSans-W3',
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  tabErrorLabelStyle: {
    color: '#ff0000'
  },
  tabErrorStyle: {
    flexDirection: 'row'
  },
  errorWrapperStyle: {
    width: 20,
    height: 20,
    marginLeft: 7,
    marginBottom: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff0000',
    borderRadius: 10
  },
  errorTextStyle: {
    fontFamily: 'Helvetica-Bold',
    fontSize: 18,
    color: '#ffffff',
    letterSpacing: -0.29
  },
  activeTextStyle: {
    color: '#ff0000',
    marginTop: 6
  },
  activeErrorWrapperStyle: {
    marginTop: 6
  },
  iconYenCoin: {
    width: 22,
    height: 22,
    marginRight: 14
  },
  iconPaper: {
    width: 18,
    height: 20,
    marginRight: 16
  },
  iconReturn: {
    width: 20,
    height: 18,
    marginRight: 14
  },
  iconYenCash: {
    width: 22,
    height: 22,
    marginRight: 14
  },
  rowTextDate: {
    color: '#9b9b9b',
    fontFamily: 'Helvetica',
    fontSize: 18,
    letterSpacing: -0.29
  },
  rowTextWrapper: {
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
