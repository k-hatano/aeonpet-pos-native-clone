import { StyleSheet } from 'react-native'

export const width = 640
export const height = 302

export default StyleSheet.create({
  textStyle: {
    marginHorizontal: 36
  },
  okButton: {
    width: 312,
    height: 64,
    fontSize: 24,
    fontWeight: 'bold'
  },
  buttonContainer: {
    flexDirection: 'row',
    margin: 32,
    justifyContent: 'center'
  },
  main: {
    width: width,
    height: height,
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:0, width: 0},
    justifyContent: 'center'
  }
})
