import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  layoutRoot: {
    backgroundColor: 'white',
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowColor: '#000000',
    shadowOffset: {height:0, width: 0}

  },
  headerArea: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingBottom: 13
  },
  titleText: {
    flex: 1,
    fontSize: 24,
    fontWeight: 'bold',
    color: baseStyleValues.mainTextColor,
    alignSelf: 'flex-end',
    marginLeft: 32
  },
  closeButton: {
    width: 48,
    height: 48,
    margin: 3,
    backgroundColor: '#f0f0f0'
  },
  closeIcon: {
    width: 24,
    height: 24
  },
  mainArea: {
    flex: 1
  }
})
