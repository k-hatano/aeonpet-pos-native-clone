import { StyleSheet } from 'react-native'
import baseStyleValues from 'common/styles/baseStyleValues'

export default StyleSheet.create({
  root: {
    backgroundColor: baseStyleValues.mainColor,
    borderRadius: 5,
    height: 32,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})