import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  recordItem: {
    backgroundColor: '#ffffff',
    padding: 8,
    flexDirection: 'column'
  },
  recordItemTitle: {
    backgroundColor: 'transparent'
  },
  recordItemTitleText: {
    fontSize: 20,
    color: '#4a4a4a',
    letterSpacing: -0.32
  },
  recordItemInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    width: '100%',
    height: '100%'
  },
  recordItemStock: {
    color: '#9b9b9b'
  },
  recordItemPrice: {
    color: '#9b9b9b'
  },
  recordItemWarning: {
    color: '#ff0000'
  }
})

export default styles
