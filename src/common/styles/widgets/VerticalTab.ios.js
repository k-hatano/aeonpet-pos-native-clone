import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  tabBarContainer: {
    flexDirection: 'column',
    backgroundColor: '#eee'
  },
  tabBarItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    paddingBottom: 10,
    paddingTop: 10
  },
  tabBarItemContainer: {
    flex: 1,
    fontSize: 15
  },
  childContainer: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  childrenContent: {
    flex: 1
  }
})

export default styles
