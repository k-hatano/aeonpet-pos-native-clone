import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    height: 40,
    width: '100%',
    flexDirection: 'row'
  },
  searchBar: {
    flex: 1
  },
  searchButton: {
    marginLeft: 16,
    width: 100
  },
  searchButtonText: {
    fontSize: 24,
    paddingHorizontal: 10
  },
  searchInput: {
    backgroundColor: '#f0f0f0',
    borderWidth: 1,
    borderColor: '#979797',
    height: 38,
    paddingRight: 10,
    paddingLeft: 10
  }
})

export default styles
