import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#88888844',
    height: '100%',
    width: '100%',
    position: 'absolute',
    flex: 1
  },
  loadingBackground: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    position: 'absolute',
    flex: 1
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 180,
    width: 180,
    position: 'absolute',
    flex: 1
  }
})

export default styles
