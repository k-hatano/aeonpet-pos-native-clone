/* eslint-disable max-len */
export default [
  { '20170705041253-create-staffs-table': require('../../migrations/20170705041253-create-staffs-table.js') },
  { '20170705041254-create-staff-roles-table': require('../../migrations/20170705041254-create-staff-roles-table') },
  { '20170705041255-create-staff-role-permissions-table': require('../../migrations/20170705041255-create-staff-role-permissions-table') },
  { '20170705042522-create-payment-methods-table': require('../../migrations/20170705042522-create-payment-methods-table.js') },
  { '20170705043243-create-bundles-table': require('../../migrations/20170705043243-create-bundles-table.js') },
  { '20170705043243-create-bundle-patterns-table': require('../../migrations/20170705043243-create-bundle-patterns-table.js') },
  { '20170705043243-create-product-product-tag-table': require('../../migrations/20170705043243-create-product-product-tag-table.js') },
  { '20170705043243-create-sale-discounts-table': require('../../migrations/20170705043243-create-sale-discounts-table.js') },
  { '20170705061202-create-promotion-presets-table': require('../../migrations/20170705061202-create-promotion-presets-table.js') },
  { '20170705061810-create-warehouses-table': require('../../migrations/20170705061810-create-warehouses-table.js') },
  { '20170705062214-create-shops-table': require('../../migrations/20170705062214-create-shops-table.js') },
  { '20170705063443-create-products-table': require('../../migrations/20170705063443-create-products-table.js') },
  { '20170705064753-create-product-variants-table': require('../../migrations/20170705064753-create-product-variants-table.js') },
  { '20170705065844-create-product-categories-table': require('../../migrations/20170705065844-create-product-categories-table.js') },
  { '20170705070615-create-product-product-categories-table': require('../../migrations/20170705070615-create-product-product-categories-table.js') },
  { '20170705070943-create-product-product-variant-table': require('../../migrations/20170705070943-create-product-product-variant-table.js') },
  { '20170705071454-create-countries-table': require('../../migrations/20170705071454-create-countries-table.js') },
  { '20170705073712-create-closure-types-table': require('../../migrations/20170705073712-create-closure-types-table.js') },
  { '20170705074132-create-pos-order-properties-table': require('../../migrations/20170705074132-create-pos-order-properties-table.js') },
  { '20170705075019-create-deliv-companies-table': require('../../migrations/20170705075019-create-deliv-companies-table.js') },
  { '20170705075223-create-deliv-time-zones-table': require('../../migrations/20170705075223-create-deliv-time-zones-table.js') },
  { '20170705075419-create-cashier-parameter-sets-table': require('../../migrations/20170705075419-create-cashier-parameter-sets-table.js') },
  { '20170705080634-create-cashier-balance-reasons-table': require('../../migrations/20170705080634-create-cashier-balance-reasons-table.js') },
  { '20170705081348-create-discount-reasons-table': require('../../migrations/20170705081348-create-discount-reasons-table.js') },
  { '20170801040428-create-cashiers-table': require('../../migrations/20170801040428-create-cashiers-table.js') },
  { '20170801040429-create-ejournal-table': require('../../migrations/20170801040429-create-ejournal-table.js') },
  { '20170815074501-create-stock-items-table': require('../../migrations/20170815074501-create-stock-items-table.js') },
  { '20170815074501-create-stock-adjustment-reasons-table': require('../../migrations/20170815074501-create-stock-adjustment-reasons-table.js') },
  { '20170901091744-create-operation_logs-table': require('../../migrations/20170901091744-create-operation_logs-table.js') },
  { '20170904223425-create-orders-table': require('../../migrations/20170904223425-create-orders-table.js') },
  { '20170906182700-update-product-table': require('../../migrations/20170906182700-update-product-table.js') },
  { '20170909034021-create-cashier-cash-records-table': require('../../migrations/20170909034021-create-cashier-cash-records-table.js') },
  { '20170911100100-create-stock-taking-task-items-table': require('../../migrations/20170911100100-create-stock-taking-task-items-table.js') },
  { '20170911100121-create-stock-taking-tasks-table': require('../../migrations/20170911100121-create-stock-taking-tasks-table.js') },
  { '20170916065717-rename-column-pos-order-properties': require('../../migrations/20170916065717-rename-column-pos-order-properties.js') },
  { '20170916194100-update-orders-table': require('../../migrations/20170916194100-update-orders-table.js') },
  { '20170918105831-update-cashiers-table.js': require('../../migrations/20170918105831-update-cashiers-table.js') },
  { '20170920025204-add-columns-to-payment-methods-table.js': require('../../migrations/20170920025204-add-columns-to-payment-methods-table.js') },
  { '20170921052852-add-columns-to-shops-table': require('../../migrations/20170921052852-add-columns-to-shops-table.js') },
  { '20171011062652-add-columns-to-cashier-cash-records-table': require('../../migrations/20171011062652-add-columns-to-cashier-cash-records-table.js') },
  { '20171011062653-add-column-to-payment-methods-table': require('../../migrations/20171011062653-add-column-to-payment-methods-table.js') },
  { '20171011062654-rename-column-for-payment-method': require('../../migrations/20171011062654-rename-column-for-payment-method.js') },
  { '20171018012552-create-pended_orders': require('../../migrations/20171018012552-create-pended_orders.js') },
  { '20171128002223-create-cashier-settings-table': require('../../migrations/20171128002223-create-cashier-settings-table.js') },
  { '20171205122221-add-columns-shops-table': require('../../migrations/20171205122221-add-columns-shops-table.js') },
  { '20171204034211-add-index-stock-items-table': require('../../migrations/20171204034211-add-index-stock-items-table.js') },
  { '20171204034242-add-index-product-variant-table': require('../../migrations/20171204034242-add-index-product-variant-table.js') },
  { '20171204041023-add-index-products-table': require('../../migrations/20171204041023-add-index-products-table.js') },
  { '20171220094443-add-aop_domaine-columns-for-product-variants-table': require('../../migrations/20171220094443-add-aop_domaine-columns-for-product-variants-table.js')},
  { '20171226110343-add-is_taxfree-for-shops-table': require('../../migrations/20171226110343-add-is_taxfree-for-shops-table.js')},
  { '20180115122850-rename-column-warehouses-table': require('../../migrations/20180115122850-rename-column-warehouses-table.js')},
  { '20180709182033-add-columns-products-table': require('../../migrations/20180709182033-add-columns-products-table')},
  { '20180711182033-add-columns-products-table': require('../../migrations/20180711182033-add-columns-products-table')},
  { '20180719162511-add-column-status-products-table': require('../../migrations/20180719162511-add-column-status-products-table.js')},
  { '20180802093830-create-regions-table': require('../../migrations/20180802093830-create-regions-table.js')},
  { '20180822153130-add-column-short-name-products-table': require('../../migrations/20180822153130-add-column-short-name-products-table.js')},
  { '20180912094810-add-column-point-rate-shops-table': require('../../migrations/20180912094810-add-column-point-rate-shops-table.js')},
  { '20180922253130-add-column-cost-price-product_variants-table': require('../../migrations/20180922253130-add-column-cost-price-product_variants-table.js')},
  { '20190613031828-change_fund_added_to_cashier_cash_records_ejournal': require('../../migrations/20190613031828-change_fund_added_to_cashier_cash_records_ejournal.js')},
  { '20190626055414-add-column-cashier-cash-records-table': require('../../migrations/20190626055414-add-column-cashier-cash-records-table.js')},
  { '20190701144030-create-multiple_tax_rates-table': require('../../migrations/20190701144030-create-multiple_tax_rates-table.js') },
  { '20190701150030-add-columns-to-products-table': require('../../migrations/20190701150030-add-columns-to-products-table.js') },
  { '20190702093830-create-cash-voucher-table': require('../../migrations/20190702093830-create-cash-voucher-table') },
  { '20190703213030-add-columns-to-shops-table': require('../../migrations/20190703213030-add-columns-to-shops-table.js') },
  { '20190715034441-create-coupon-discounts-table': require('../../migrations/20190715034441-create-coupon-discounts-table.js') },
  { '20190715190000-create-product-point-ratios-table': require('../../migrations/20190715190000-create-product-point-ratios-table.js') },
  { '20190716190000-create-shop-point-ratios-table': require('../../migrations/20190716190000-create-shop-point-ratios-table.js') },
  { '20190717091722-add-discount_rate-for-bundle_patterns-table': require('../../migrations/20190717091722-add-discount_rate-for-bundle_patterns-table.js') },
  { '20190726031832-create-product-groups-table': require('../../migrations/20190726031832-create-product-groups-table.js') },
  { '20190730193030-add-columns-to-shops-table': require('../../migrations/20190730193030-add-columns-to-shops-table.js') },
  { '20190813120000-add-columns-to-staffs-table': require('../../migrations/20190813120000-add-columns-to-staffs-table.js') },
  { '20190925180000-create-promotions-table': require('../../migrations/20190925180000-create-promotions-table.js') },
  { '20190927090209-add-barcode-column-to-bundles-sale_discounts-table': require('../../migrations/20190927090209-add-barcode-column-to-bundles-sale_discounts-table.js') }
]
