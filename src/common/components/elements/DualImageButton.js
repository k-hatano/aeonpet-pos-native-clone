'use strict'

import React, { Component } from 'react'
import { Image, View, Text, TouchableOpacity } from 'react-native'

export default class DualImageButton extends Component {
  render () {
    const {
      disabled,
      title,
      onPress,
      containerStyle,
      imagesWrapperStyle,
      firstIcon,
      firstIconStyle,
      secondIcon,
      secondIconStyle,
      buttonTitleStyle
    } = this.props

    return (
      <TouchableOpacity onPress={() => onPress()} disabled={disabled} style={containerStyle}>
        <View style={imagesWrapperStyle}>
          <Image source={firstIcon} style={firstIconStyle} />
          {secondIcon && <Image source={secondIcon} style={secondIconStyle} />}
        </View>
        {title && <Text style={buttonTitleStyle}> {title} </Text>}
      </TouchableOpacity>
    )
  }
}
