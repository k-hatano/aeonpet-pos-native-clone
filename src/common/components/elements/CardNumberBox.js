import React, { Component } from 'react'
import { TextInput } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/elements/AmountPadModal'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import CustomPropTypes from '../CustomPropTypes'
import CardNumberPad from './CardNumberPad'
import { CUSTOMER_CODE_LENGTH } from 'modules/customer/models'
import Modal from '../widgets/Modal'
import ModalFrame from '../widgets/ModalFrame'

class CardNumberPadModal extends Component {
  static propTypes = {
    maxLength: PropTypes.number,
    onComplete: PropTypes.func
  }

  render () {
    const { onComplete, maxLength } = this.props
    const onCompleteForOverride = async (amount) => {
      if (await onComplete(amount)) {
        Modal.close()
      }
    }

    return (
      <ModalFrame style={styles.layoutRoot} title='' onClose={() => Modal.close()} >
        <CardNumberPad
          onComplete={onCompleteForOverride}
          maxLength={maxLength} />
      </ModalFrame>
    )
  }

  /**
   *
   * @param {AmountPadModalProps} props
   */
  static open (props) {
    Modal.open(CardNumberPadModal, {
      props: props,
      isBackgroundVisible: true
    })
  }
}

export default class CardNumberBox extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    maxLength: PropTypes.number,
    onChange: PropTypes.func,
    style: CustomPropTypes.Style,
    value: PropTypes.string,
  }

  _openNumberPad () {
    CardNumberPadModal.open({
      maxLength: this.props.maxLength ? this.props.maxLength : CUSTOMER_CODE_LENGTH,
      onComplete: async (value) => {
        await this.props.onChange(value)
        return true 
      }
    })
  }

  render () {
    return (
      <TouchableOpacitySfx onPress={() => this._openNumberPad()} disabled={this.props.disabled}>
        <TextInput
          style={this.props.style}
          value={this.props.value}
          placeholder={this.props.placeholder}
          editable={false} />
      </TouchableOpacitySfx>
    )
  }
}
