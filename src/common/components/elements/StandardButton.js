'use strict'

import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import styles from '../../styles/elements/StandardButton'
import PropTypes from 'prop-types'

const buttonStyles = {
  normal: {
    container: styles.standardButtonContainer,
    text: styles.standardButtonText
  },
  borderedButton: {
    container: styles.standardBorderButton,
    text: styles.standardBorderButtonText
  }
}
export default class StandardButton extends Component {
  _disabledButtonStyle () {
    return styles.commandButtonDisabled
  }

  render () {
    const { text, color, textColor, containerStyle, textStyle, onPress, isBorderButton, disabled } = this.props
    const button = !isBorderButton ? buttonStyles.normal : buttonStyles.borderedButton
    const buttonColor = isBorderButton ? { borderColor: color } : { backgroundColor: color }
    return (
      <TouchableOpacitySfx
        style={[button.container, containerStyle, color ? buttonColor : {}, disabled ? { backgroundColor: '#d8d8d8' } : {}]}
        disabled={disabled}
        onPress={onPress}
      >
        {text &&
          <Text style={[button.text, textStyle, textColor ? { color: textColor } : {}]}>
            {text}
          </Text>}
      </TouchableOpacitySfx>
    )
  }
}

class ButtonBase extends Component {
  constructor () {
    super()
    this.measure = (callback) => this.refs.view.measure(callback)
    this.state = {
      isDisabled: false
    }
  }

  static propTypes = {
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object, PropTypes.array]),
    text: PropTypes.string,
    onPress: PropTypes.func,
    disabled: PropTypes.bool,
    dispPulldownIcon: PropTypes.bool
  }

  static defaultProps = {
    text: '',
    fontSize: undefined,
    disabled: false,
    dispPulldownIcon: false
  }

  componentDidMount () {
    this._isMounted = true
  }
  componentWillUnmount () {
    this._isMounted = false
  }

  _buttonStyle () {}
  _disabledButtonStyle () {}
  _textStyle () {}
  _disabledTextStyle () {}

  _onPress = async (callback) => {
    this.setState({isDisabled: true})
    try {
      await callback()
    } finally {
      if (this._isMounted) {
        this.setState({isDisabled: false})
      }
    }
  }

  render () {
    const { fontSize, fontWeight, textAlign, ...baseStyle } = StyleSheet.flatten(this.props.style)
    const fontStyleOverride = { fontSize, fontWeight, textAlign }

    return (
      <TouchableOpacitySfx
        onPress={() => this._onPress(this.props.onPress)}
        disabled={this.props.disabled || this.state.isDisabled}>
        <View
          ref='view'
          style={[this.props.disabled || this.state.isDisabled ? this._disabledButtonStyle() : this._buttonStyle(), baseStyle]} >
          <Text style={[this.props.disabled || this.state.isDisabled ? this._disabledTextStyle() : this._textStyle(), fontStyleOverride]}>
            {this.props.text}
          </Text>
          {this.props.dispPulldownIcon &&
            <Text style={[this.props.disabled || this.state.isDisabled ? this._disabledTextStyle() : this._textStyle(), fontStyleOverride, {marginRight: 0.5}]}>
              ▼
            </Text>
          }
        </View>
      </TouchableOpacitySfx>
    )
  }
}

export class CommandButton extends ButtonBase {
  _textStyle () {
    return styles.commandButtonText
  }

  _disabledTextStyle () {
    return styles.commandButtonTextDisabled
  }

  _buttonStyle () {
    return styles.commandButton
  }

  _disabledButtonStyle () {
    return styles.commandButtonDisabled
  }
}

export class OptionalCommandButton extends ButtonBase {
  _textStyle () {
    return styles.optionalCommandButtonText
  }

  _disabledTextStyle () {
    return styles.optionalCommandButtonTextDisabled
  }

  _buttonStyle () {
    return styles.optionalCommandButton
  }

  _disabledButtonStyle () {
    return styles.optionalCommandButtonDisabled
  }
}

export class ProceedButton extends ButtonBase {
  _textStyle () {
    return styles.proceedButtonText
  }

  _disabledTextStyle () {
    return styles.proceedButtonTextDisabled
  }

  _buttonStyle () {
    return styles.proceedButton
  }

  _disabledButtonStyle () {
    return styles.proceedButtonDisabled
  }
}

export class WeakProceedButton extends ButtonBase {
  _textStyle () {
    return styles.weakProceedButtonText
  }

  _disabledTextStyle () {
    return styles.weakProceedButtonTextDisabled
  }

  _buttonStyle () {
    return styles.weakProceedButton
  }

  _disabledButtonStyle () {
    return styles.weakProceedButtonDisabled
  }
}

export class UnSelectedButton extends ButtonBase {
  _textStyle () {
    return styles.unSelectedButtonText
  }

  _disabledTextStyle () {
    return styles.unSelectedButtonText
  }

  _buttonStyle () {
    return styles.unSelectedButton
  }

  _disabledButtonStyle () {
    return styles.unSelectedButton
  }
}
