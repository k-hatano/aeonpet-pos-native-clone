'use strict'

import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { RadioButtons } from 'react-native-radio-buttons'
import { isEmpty } from '../../utils/validations'
import componentStyles from '../../styles/elements/Inputs'
import ImageButton from './ImageButton'
import { ProceedButton, WeakProceedButton } from 'common/components/elements/StandardButton'

/*
 * USAGE:
 *
 * const radioOptions = [
 *  { label: "Checkout", value: 1, selected: true },
 *  { label: "VOID", value: 2 }
 *  ]
 *
 * <RadioGroup
 *  options={ radioOptions }
 *  onSelectItem={(item) => {console.warn(JSON.stringify(item))}}
 *  />
 *
 * */
class RadioGroup extends Component {
  constructor (props) {
    super(props)

    this.state = {
      item: null
    }
  }

  onSelect = item => {
    const { onSelectItem, disable } = this.props
    if (!disable) {
      this.setState({
        item: item
      })
      onSelectItem && onSelectItem(item)
    }
  }

  renderOption = (option, selected, onSelect, index) => {
    const { signColor, buttonStyle } = this.props
    const signBorder = signColor ? { borderColor: signColor } : {}
    return (
      <TouchableOpacity onPress={onSelect} key={index} style={[componentStyles.radioGroupOption, buttonStyle]}>
        <View style={componentStyles.mainInputWrapper}>
          <View style={[componentStyles.radioGroupOptionSign, signBorder]}>
            {((!(this.state.item) && option.selected) || (this.state.item && (this.state.item.value === option.value)))
              ? <ImageButton
                style={componentStyles.checkedStyle}
                toggle
                onPress={onSelect}
                appearance={{
                  normal: require('../../../assets/images/checked.png'),
                  highlight: require('../../../assets/images/checked.png')
                }}
              />
              : null}
          </View>
          <View>
            {!isEmpty(option.label) && <Text style={componentStyles.radioGroupOptionText}> {option.label} </Text>}
          </View>
        </View>
        <View style={componentStyles.radioGroupOptionSubTextWrapper}>
          {!isEmpty(option.subLabel) &&
            <Text style={componentStyles.radioGroupOptionSubText}> {option.subLabel} </Text>}
        </View>
      </TouchableOpacity>
    )
  }

  renderOptionWithProceedButton = (option, selected, onSelect, index) => {
    return (
      <View key={index}>
        {((!(this.state.item) && option.selected) || (this.state.item && (this.state.item.value === option.value)))
          ? <ProceedButton
              style={componentStyles.proceedButton}
              text={option.label}
              onPress={onSelect} />
          : <WeakProceedButton
              style={componentStyles.proceedButton}
              text={option.label}
              onPress={onSelect} />}
      </View>
    )
  }

  renderContainer = optionNodes => {
    const { vertical, wrap } = this.props
    const horizontalStyle = vertical ? { flexDirection: 'column' } : wrap ? {flexWrap: 'wrap'} : {}
    return <View style={[componentStyles.radioGroupContainer, horizontalStyle]}>{optionNodes}</View>
  }

  render () {
    const { options, vertical, wrap, useProceedButton } = this.props
    const renderOption = useProceedButton ? this.renderOptionWithProceedButton : this.renderOption
    const radioButtons = (
      <RadioButtons
        options={options}
        onSelection={this.onSelect.bind(this)}
        renderOption={renderOption}
        renderContainer={this.renderContainer}
      />
    )

    return wrap ? radioButtons : (
      <ScrollView horizontal={!vertical}>
        {radioButtons}
      </ScrollView>
    )
  }
}

export { RadioGroup }
