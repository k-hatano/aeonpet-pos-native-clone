import React, { Component } from 'react'
import { View, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import componentStyles from '../../styles/elements/SearchInput'

export default class SearchInput extends Component {
  _onIconPress = () => {
    this.refs.searchInput.focus()
  }

  render () {
    const { placeholder, hasIcon, containerStyle, inputStyle, iconSize } = this.props
    const noIconStyle = hasIcon ? {} : componentStyles.noIconSearchInput

    return (
      <View style={[componentStyles.container, containerStyle]}>
        {hasIcon &&
          <Icon
            color='#ccc'
            name='md-search'
            size={iconSize || 24}
            style={componentStyles.searchIcon}
            onPress={() => this._onIconPress()}
          />}
        <View style={componentStyles.searchInputWrapper}>
          <TextInput
            ref='searchInput'
            placeholder={placeholder}
            style={[componentStyles.searchInput, noIconStyle, inputStyle]}
          />
        </View>
      </View>
    )
  }
}
