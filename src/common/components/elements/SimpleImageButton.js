import React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/elements/SimpleImageButton'
import CustomPropTypes from '../CustomPropTypes'
import { playButtonTouch } from '../../utils/sfx'

export default class SimpleImageButton extends React.Component {
  static propTypes = {
    style: CustomPropTypes.Style,
    imageStyle: CustomPropTypes.Style,
    onPress: PropTypes.func,
    isSoundEnabled: PropTypes.bool,
    source: PropTypes.number
  }

  static defaultProps = {
    isSoundEnabled: true
  }

  constructor () {
    super()
    this.measure = (callback) => this.refs.view.measure(callback)
  }

  render () {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        disabled={this.props.disabled}
        onPressIn={() => { if (this.props.isSoundEnabled) { playButtonTouch() } }}>
        <View ref='view' style={[styles.bottonArea, this.props.style, this.props.disabled ? { backgroundColor: '#d8d8d8', borderColor: '#d8d8d8' } : {}]}>
          <Image style={[{}, this.props.imageStyle]} source={this.props.source} resizeMode='stretch' />
        </View>
      </TouchableOpacity>
    )
  }
}
