import React, { Component } from 'react'
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native'
import styles from '../../styles/elements/labels'
import CustomPropTypes from '../CustomPropTypes'

export class NumberInputLabel extends Component {
  static propTypes = {
    style: CustomPropTypes.Style,
    value: React.PropTypes.number,
    onPress: React.PropTypes.func
  }

  render () {
    const { color, fontSize, fontWeight, ...baseStyle } = (StyleSheet.flatten(this.props.style) || {})
    const textStyle_ = { color, fontSize, fontWeight }

    const textStyle = {}
    for (let i in textStyle_) {
      if (textStyle_[i]) textStyle[i] = textStyle_[i]
    }

    return (
      <TouchableOpacity
        onPress={this.props.onPress} >
        <View style={[styles.numberInputLabelBorder, baseStyle]}>
          <Text style={[styles.numberInputLabelText, textStyle]}>{this.props.value}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
