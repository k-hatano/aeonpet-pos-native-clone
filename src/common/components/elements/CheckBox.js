import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableHighlight
} from 'react-native'

const PropTypes = React.PropTypes
const CB_ENABLED_IMAGE = require('../../../assets/images/cb_enabled.png')
const CB_DISABLED_IMAGE = require('../../../assets/images/cb_disabled.png')

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5
  },
  checkbox: {
    width: 32,
    height: 26
  },
  labelContainer: {
    marginLeft: 10,
    marginRight: 10
  },
  label: {
    fontSize: 15,
    color: 'grey'
  }
})

export default class CheckBox extends Component {
  static propTypes = {
    label: PropTypes.string,
    labelBefore: PropTypes.bool,
    labelStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.number]),
    labelLines: PropTypes.number,
    checkboxStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.number]),
    containerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object, PropTypes.number]),
    checked: PropTypes.bool,
    checkedImage: PropTypes.number,
    uncheckedImage: PropTypes.number,
    underlayColor: PropTypes.string,
    onChange: PropTypes.func,
    mutableChecked: PropTypes.bool
  }
  static defaultProps = {
    label: 'Label',
    labelLines: 1,
    labelBefore: false,
    checked: false,
    checkedImage: CB_ENABLED_IMAGE,
    uncheckedImage: CB_DISABLED_IMAGE,
    underlayColor: 'white'
  }
  constructor (props) {
    super(props)

    this.state = {
      checked: props.checked
    }
    this.onChange = this.onChange.bind(this)
  }

  onChange () {
    let checked = this.props.mutableChecked !== undefined ? this.props.mutableChecked : this.state.checked

    if (this.props.onChange) {
      this.props.onChange(!checked)
    }
    this.setState({
      checked: !checked
    })
  }

  render () {
    const checked = this.props.mutableChecked !== undefined ? this.props.mutableChecked : this.state.checked
    let source = checked ? this.props.checkedImage : this.props.uncheckedImage
    let container = (
      <View style={this.props.containerStyle || styles.container}>
        <Image
          style={this.props.checkboxStyle || styles.checkbox}
          source={source} />
        <View style={styles.labelContainer}>
          <Text style={[styles.label, this.props.labelStyle]}>{this.props.label}</Text>
        </View>
      </View>
    )
    if (this.props.labelBefore) {
      container = (
        <View style={this.props.containerStyle || [styles.container, styles.flexContainer]}>
          <View style={styles.labelContainer}>
            <Text numberOfLines={this.props.labelLines} style={[styles.label, this.props.labelStyle]}>{this.props.label}</Text>
          </View>
          <Image
            style={[styles.checkbox, this.props.checkboxStyle]}
            source={source} />
        </View>
      )
    } else {
      container = (
        <View style={[styles.container, this.props.containerStyle]}>
          <Image
            style={[styles.checkbox, this.props.checkboxStyle]}
            source={source} />
          <View style={styles.labelContainer}>
            <Text numberOfLines={this.props.labelLines} style={[styles.label, this.props.labelStyle]}>{this.props.label}</Text>
          </View>
        </View>
      )
    }

    return (
      <TouchableHighlight onPress={this.onChange} underlayColor={this.props.underlayColor} style={styles.flexContainer}>
        {container}
      </TouchableHighlight>
    )
  }
}
