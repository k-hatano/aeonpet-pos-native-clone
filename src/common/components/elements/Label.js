import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/elements/Label'

export default class Label extends Component {
  static propTypes = {
    text: PropTypes.string,
    baseStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    textStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    isVisible: PropTypes.bool
  }

  static defaultProps = {
    isVisible: true
  }

  render () {
    if (this.props.isVisible) {
      return (
        <View style={[styles.background, this.props.baseStyle]}>
          <Text style={this.props.textStyle}>
            {this.props.children}
          </Text>
        </View>
      )
    } else {
      return null
    }
  }
}
