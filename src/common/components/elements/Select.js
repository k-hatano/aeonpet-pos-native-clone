'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text, ListView, StyleSheet } from 'react-native'
import componentStyles from '../../styles/elements/Select'
import PropTypes from 'prop-types'
import Modal from 'common/components/widgets/Modal'
import ModalFrame from 'common/components/widgets/ModalFrame'
import I18n from 'i18n-js'

class selectorModal extends Component {
  static propTypes = {
    displayProperty: PropTypes.string,
    items: PropTypes.array,
    onItemChange: PropTypes.func,
    selectorModalTitle: PropTypes.string,
    placeholder: PropTypes.string,
    hideDefaultValue: PropTypes.bool
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
  }

  _renderSelectorDataItem = (item) => (
    <TouchableOpacity
      onPress={() => this._onItemPress(item)} style={{borderBottomWidth: 0.7, height: 64, justifyContent: 'center'}}>
      <Text style={componentStyles.listValue}>{item[this.props.displayProperty]}</Text>
    </TouchableOpacity>
  )

  _onItemPress = item => {
    this.props.onItemChange(item)
    this.setState({ selectedItem: item })
    Modal.close()
  }

  render () {
    const defaultSelectorLabel = this.props.placeholder ? this.props.placeholder : I18n.t('common.select_one')
    const defaultSelectorDatum = { [this.props.displayProperty]: defaultSelectorLabel }
    const selectorOptions = this.props.hideDefaultValue ? this.props.items : [defaultSelectorDatum, ...this.props.items]
    const width = this.props.width
    const height = this.props.height
    let selectorDataSource = this.dataSource.cloneWithRows(selectorOptions)

    return (
      <View style={[componentStyles.selectDropdown, {width: width, height: height}]}>
        <ModalFrame style={{height: 758, width: 638, backgroundColor: '#fff'}} title={this.props.selectorModalTitle} onClose={() => Modal.close()} >
          <View style={{height: '98%'}}>
            <ListView
              contentContainerStyle={{flexDirection: 'column', marginRight: 52, marginLeft: 52}}
              dataSource={selectorDataSource}
              renderRow={item => this._renderSelectorDataItem(item)}
              enableEmptySections />
          </View>
        </ModalFrame>
      </View>
    )
  }
}

export default class Select extends Component {
  static propTypes = {
    zIndex: PropTypes.number,
    items: PropTypes.array,
    selectedItem: PropTypes.object,
    placeholder: PropTypes.string,
    displayProperty: PropTypes.string,
    onItemChange: PropTypes.func,
    disable: PropTypes.bool,
    hideDefaultValue: PropTypes.bool
  }

  _onPress = () => {
    Modal.open(selectorModal, {
      isBackgroundVisible: false,
      props: {
        displayProperty: this.props.displayProperty,
        items: this.props.items,
        onItemChange: this.props.onItemChange,
        selectorModalTitle: this.props.selectorModalTitle,
        placeholder: this.props.placeholder,
        hideDefaultValue: this.props.hideDefaultValue
      }
    })
  }

  render () {
    const { selectedItem, placeholder, displayProperty, disable, style, selectStyle } = this.props
    return (
      <View style={style}>
        <View style={[componentStyles.selectInput, selectStyle]}>
          <TouchableOpacity style={{ flex: 1, flexDirection: 'row' }} onPress={() => disable ? {} : this._onPress()}>
            <View style={{flex: 9 / 10}}>
              <Text style={componentStyles.selectValue}>{selectedItem ? selectedItem[displayProperty] : placeholder}</Text>
            </View>
            <View style={{flex: 1 / 10, justifyContent: 'center'}}>
              <Text style={componentStyles.selectMark}>{'▼'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
