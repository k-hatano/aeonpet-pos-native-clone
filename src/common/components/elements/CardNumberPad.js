'use strict'

import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import StandardButton from './StandardButton'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import componentStyles from '../../styles/elements/NumberPad'
import PropTypes from 'prop-types'
import { CUSTOMER_CODE_LENGTH } from 'modules/customer/models'

const DEFAULT_RESULT_FONT_SIZE = 36

export default class CardNumberPad extends Component {
  static propTypes = {
    maxLength: PropTypes.number,
    onComplete: PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)
    this.state = {
      result: ''
    }
  }

  validateResultLength = value => {
    const maxLength = this.props.maxLength ? this.props.maxLength : CUSTOMER_CODE_LENGTH
    return value.length <= maxLength
  }

  actionNumberPress = value => {
    let newResult = this.state.result + value

    if (!this.validateResultLength(newResult)) {
      return false
    }
    this.setState({
      result: newResult
    })
    return true
  }

  actionCPress = () => {
    this.setState({
      result: ''
    })
  }

  actionBCPress = () => {
    let newResult = this.state.result.length === 0
      ? ''
      : this.state.result.slice(0, -1)
    this.setState({result: newResult})

  }

  generateFeatureButtons = () => [
    [
      {
        title: '1',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('1')
        }
      },
      {
        title: '2',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('2')
        }
      },
      {
        title: '3',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('3')
        }
      },
      {
        title: 'C',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionCPress()
        }
      }
    ],
    [
      {
        title: '4',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('4')
        }
      },
      {
        title: '5',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('5')
        }
      },
      {
        title: '6',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('6')
        }
      },
      {
        title: 'BC',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionBCPress()
        }
      }
    ],
    [
      {
        title: '7',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('7')
        }
      },
      {
        title: '8',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('8')
        }
      },
      {
        title: '9',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('9')
        }
      },
      {
        flex: 1,
        color: '#fff',
        border: '#fff'
      }
    ],
    [
      {
        title: '0',
        flex: 3,
        onPress: () => {
          this.actionNumberPress('0')
        }
      },
      {
        flex: 1,
        color: '#fff',
        border: '#fff'
      }
    ]
  ]

  render () {
    const listFeatureButton = this.generateFeatureButtons()
    const { result } = this.state
    return (
      <View style={[componentStyles.componentContainer]}>
        <View style={componentStyles.resultSide}>
          <Text style={[componentStyles.resultText, { fontSize: DEFAULT_RESULT_FONT_SIZE }]}>
            {result}
          </Text>
        </View>
        {listFeatureButton.map((listButton, index) =>
          <View key={index} style={componentStyles.featureSide}>
            {listButton.map((button, index) =>
              <View
                key={index}
                style={[
                  componentStyles.featureButton,
                  {
                    flex: button.flex,
                    backgroundColor: button.color || '#f0f0f0',
                    borderColor: button.border || '#979797'
                  }
                ]}
              >
                <TouchableOpacitySfx
                  style={componentStyles.featureButtonTouch}
                  onPress={
                    !button.type
                      ? button.onPress
                      : () => {
                        button.onPress(result)
                      }
                  }
                >
                  <Text style={componentStyles.featureButtonText}>{button.title}</Text>
                </TouchableOpacitySfx>
              </View>
            )}
          </View>
        )}
        <View style={componentStyles.featureSide}>
          <View style={componentStyles.confirm}>
            <StandardButton
              text={I18n.t('order.confirm')}
              color={'#ff9024'}
              textStyle={componentStyles.confirmTouchText}
              onPress={() => {
                this.props.onComplete(this.state.result)
                this.actionCPress()
              }}
              disabled={this.props.disableConfirmButton}
            />
          </View>
        </View>
      </View>
    )
  }
}
