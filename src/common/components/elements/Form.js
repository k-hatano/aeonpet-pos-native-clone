import React from 'react'
import { TextInput as _TextInput, Text, View } from 'react-native'
import { Field as _Field } from 'redux-form'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import PropTypes from 'prop-types'

class TextInput extends React.Component {
  static propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object
  }

  constructor () {
    super()
    this.state = {
      value: ''
    }
  }

  componentDidMount () {
    this.setState({ value: this.props.value })
  }

  render () {
    const { input: { onChange, value, ...inputRest }, meta, ...rest } = this.props

    return (
      <View>
        <_TextInput
          onChange={onChange}
          autoCorrect={false}
          {...inputRest}
          {...rest}
          defaultValue={value}
          value={undefined} />
        {
          meta.touched &&
          meta.error &&
          <Text style={{ color: '#f00', fontStyle: 'italic' }}>
            {meta.error}
          </Text>
        }
      </View>
    )
  }
}

function Field ({ name, ...rest }) {
  return <_Field name={name} {...rest} component={TextInput} />
}

function SubmitButton ({ textStyle, label, onSubmit, buttonStyle }) {
  return (
    <TouchableOpacitySfx style={buttonStyle} onPress={onSubmit}>
      <Text style={textStyle}>{label}</Text>
    </TouchableOpacitySfx>
  )
}

export { Field, SubmitButton }
