'use strict'

import React, { Component } from 'react'
import { View, Text } from 'react-native'
import I18n from 'i18n-js'
import StandardButton from './StandardButton'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import componentStyles from '../../styles/elements/NumberPad'
import PropTypes from 'prop-types'
import { DEFAULT_AMOUNT_MAX_LENGTH } from 'common/models'

export const AMOUNT_PAD_MODE = {
  CASH: 1,
  POINT: 2,
  NUMBER: 3,
  DISCOUNT_PERCENT: 4
}

const CALCULATOR_LABEL = [
  'common.japanese_yen',
  'common.point',
  'common.number',
  'common.percent'
]

export default class AmountPad extends Component {
  static propTypes = {
    maxLength: PropTypes.number,
    format: PropTypes.number,
    maxValue: PropTypes.number,
    justValue: PropTypes.number,
    mode: PropTypes.number,
    onComplete: PropTypes.func.isRequired,
    disableConfirmButton: PropTypes.bool,
    disableCounterSuffix: PropTypes.bool,
    disableClearPoint: PropTypes.bool,
    onClearPoint: PropTypes.func,
    additionalTitles: PropTypes.array,
    useableBasePoints: PropTypes.number,
    maxUsePoint: PropTypes.number
  }

  constructor (props) {
    super(props)
    this.state = {
      additionMode: false,
      resultDisplay: '0',
      highResult: 0,
      result: 0,
      maxLength: this.props.maxLength ? this.props.maxLength : DEFAULT_AMOUNT_MAX_LENGTH,
      currentFontSize: DEFAULT_RESULT_FONT_SIZE
    }
  }

  validateResultLimit = number => {
    return this.state.highResult + parseInt(number, 10) <= this.props.maxValue
  }

  validateResultLength = number => {
    let resultLength = number.toString().length
    return resultLength <= this.state.maxLength
  }

  resetFontSize = result => {
    let currentResultLength = result.toString().length
    if (currentResultLength <= MAX_NUMBER_IN_ROW) {
      this.setState({
        currentFontSize: DEFAULT_RESULT_FONT_SIZE
      })
      return true
    }
    let currentFontStyle = this.state.currentFontSize
    if (currentFontStyle < DEFAULT_RESULT_FONT_SIZE) {
      return false
    }
    this.setState({
      currentFontSize: currentFontStyle - 4
    })
  }

  actionSetResult = number => {
    const newResultDisplay = parseInt(number, 10).toLocaleString('en-US', { minimumFractionDigits: 0 })
    this.setState({
      resultDisplay: newResultDisplay,
      result: parseInt(number, 10)
    })
  }

  /**
   * ポイント利用基底数基準で近似値の下限値を取得
   * @return {Number}
   */
  resultCompleteReplace = () => {
    let { result } = this.state
    if (this.props.mode !== AMOUNT_PAD_MODE.POINT) return result

    // 利用可能ポイントと入力されたポイントの比較
    if (this.props.maxUsePoint < result) result = this.props.maxUsePoint

    const basePoint = this.props.useableBasePoints
      ? this.props.useableBasePoints : 1
    if (basePoint === 1) return result

    // 近似値の下限値計算
    const basePointCoefficient = Math.floor(result / basePoint)
    return basePoint * basePointCoefficient
  }

  actionNumberPress = number => {
    const { onChange } = this.props
    let newResult = `${this.state.result}${number}`
    let success = true
    if (this.props.maxValue !== undefined && !this.validateResultLimit(newResult)) {
      newResult = `${this.props.maxValue}`
      success = false
    }
    if (!this.validateResultLength(newResult)) {
      return false
    }
    let newResultDisplay = (parseInt(newResult, 10) + this.state.highResult).toLocaleString('en-US', { minimumFractionDigits: 0 })
    onChange && onChange(parseInt(newResult, 10) + this.state.highResult)
    this.setState({
      resultDisplay: newResultDisplay,
      result: parseInt(newResult, 10)
    })
    this.resetFontSize(newResult)
    return success
  }

  actionDisitPress = number => {
    const reg = /[1-9]/
    if (this.actionNumberPress(number) && this.state.result) {
      this.setState({highResult: this.state.highResult + parseInt(`${this.state.result}${number}`, 10)})
      let baffer = ''
      for (let i = 0, n = `${this.state.result}${number}`.length; i < n; i++) {
        baffer += `${this.state.result}${number}`[n - i - 1]
      }
      this.setState({maxLength: baffer.search(reg)})
      this.setState({result: 0})
      this.setState({additionMode: true})
    }
  }

  actionCPress = () => {
    const { onChange } = this.props
    onChange && onChange(0)
    this.setState({
      maxLength: this.props.maxLength ? this.props.maxLength : DEFAULT_AMOUNT_MAX_LENGTH,
      resultDisplay: '0',
      result: 0,
      highResult: 0,
      additionMode: false
    })
    this.resetFontSize(0)
  }

  actionBCPress = () => {
    const { onChange } = this.props
    let oldResult = (this.state.highResult || 0) + this.state.result
    this.setState({highResult: 0})
    let newResult = oldResult.toString().length === 1
      ? 0
      : parseInt(oldResult.toString().substr(0, oldResult.toString().length - 1), 10)
    this.setState({result: newResult})
    let newResultDisplay = parseInt(newResult, 10).toLocaleString('en-US')

    onChange && onChange(parseInt(newResult, 10))
    this.setState({
      resultDisplay: newResultDisplay,
      maxLength: this.props.maxLength ? this.props.maxLength : DEFAULT_AMOUNT_MAX_LENGTH
    })
    this.resetFontSize(newResult)
  }

  _renderConfirmButton () {
    return (
      <View style={componentStyles.featureSide}>
        <View style={componentStyles.confirm}>
          <StandardButton
            text={I18n.t('order.confirm')}
            color={'#ff9024'}
            textStyle={componentStyles.confirmTouchText}
            onPress={() => {
              this.props.onComplete(this.state.highResult + this.resultCompleteReplace())
              this.actionCPress()
            }}
            disabled={this.props.disableConfirmButton}
          />
        </View>
      </View>
    )
  }

  _renderJustButton () {
    return (
      <View style={componentStyles.featureSide}>
        <View style={componentStyles.confirm}>
          <StandardButton
            text={I18n.t('order.just')}
            color={'#ff9024'}
            textStyle={componentStyles.confirmTouchText}
            onPress={() => this.setState({
              resultDisplay: parseInt(this.props.justValue, 10).toLocaleString('en-US'),
              result: this.props.justValue
            })}
          />
        </View>
        {this._renderConfirmButton()}
      </View>
    )
  }

  _renderPointButtons () {
    return (
      <View style={componentStyles.featureFunctionSide}>
        <View style={componentStyles.featureSide}>
          <View style={componentStyles.confirm}>
            <StandardButton
              text={I18n.t('order.use_all_points')}
              color={'#ff9024'}
              textStyle={componentStyles.pointConfirmTouchText}
              onPress={() => this.setState({
                resultDisplay: parseInt(this.props.maxValue, 10).toLocaleString('en-US'),
                result: this.props.maxValue
              })}
            />
          </View>
        </View>
        {this._renderJustButton()}
      </View>
    )
  }

  _renderFunctionButtons () {
    switch (this.props.mode) {
      case AMOUNT_PAD_MODE.CASH:
        return this.props.justValue ? this._renderJustButton() : this._renderConfirmButton()
      case AMOUNT_PAD_MODE.POINT:
        return this._renderPointButtons()
      case AMOUNT_PAD_MODE.NUMBER:
        return this._renderConfirmButton()
      case AMOUNT_PAD_MODE.DISCOUNT_PERCENT:
        return this._renderConfirmButton()
    }
  }

  _renderadditionalTitles () {
    let titles = []
    this.props.additionalTitles.map((title, index) => {
      titles.push(
        <View style={componentStyles.additionalTitleContainer} key={index}>
          <Text numberOfLines={1} style={componentStyles.additionalTitleText}>
            {title}
          </Text>
        </View>
      )
    })
    return titles
  }

  _addDisitButtons (buttons) {
    const featureButtons = buttons
    featureButtons[2].push({
      flex: 1,
      color: '#fff',
      border: '#fff'
    })
    featureButtons[3].push({
      flex: 1,
      color: '#fff',
      border: '#fff'
    })
    return featureButtons
  }

  generateFeatureButtons = () => [
    [
      {
        title: '1',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('1')
        }
      },
      {
        title: '2',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('2')
        }
      },
      {
        title: '3',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('3')
        }
      },
      {
        title: 'C',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionCPress()
        }
      }
    ],
    [
      {
        title: '4',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('4')
        }
      },
      {
        title: '5',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('5')
        }
      },
      {
        title: '6',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('6')
        }
      },
      {
        title: 'BC',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionBCPress()
        }
      }
    ],
    [
      {
        title: '7',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('7')
        }
      },
      {
        title: '8',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('8')
        }
      },
      {
        title: '9',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('9')
        }
      }
    ],
    [
      {
        title: '0',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('0')
        }
      },
      {
        title: '00',
        flex: 2,
        onPress: () => {
          if (this.state.highResult) {
            this.actionDisitPress('00')
          } else {
            this.actionNumberPress('00')
          }
        }
      }
    ]
  ]

  render () {
    let listFeatureButton = this._addDisitButtons(this.generateFeatureButtons())
    const { containerStyles } = this.props
    const { resultDisplay, result, currentFontSize } = this.state
    const counterSuffix = this.props.disableCounterSuffix ? '' : I18n.t(CALCULATOR_LABEL[this.props.mode - 1])
    return (
      <View style={[componentStyles.componentContainer, containerStyles]}>
        {this.props.additionalTitles && this._renderadditionalTitles()}
        <View style={componentStyles.resultSide}>
          <Text style={[componentStyles.resultText, { fontSize: currentFontSize }]}>
            {resultDisplay + counterSuffix}
          </Text>
        </View>
        {listFeatureButton.map((listButton, index) =>
          <View key={index} style={componentStyles.featureSide}>
            {listButton.map((button, index) =>
              <View
                key={index}
                style={[
                  componentStyles.featureButton,
                  {
                    flex: button.flex,
                    backgroundColor: button.color || '#f0f0f0',
                    borderColor: button.border || '#979797'
                  }
                ]}
              >
                <TouchableOpacitySfx
                  style={componentStyles.featureButtonTouch}
                  onPress={
                    !button.type
                      ? button.onPress
                      : () => {
                        button.onPress(result)
                      }
                  }
                >
                  <Text style={componentStyles.featureButtonText}>{button.title}</Text>
                </TouchableOpacitySfx>
              </View>
            )}
          </View>
        )}
        {this._renderFunctionButtons()}
      </View>
    )
  }
}

const MAX_NUMBER_IN_ROW = 6
const DEFAULT_RESULT_FONT_SIZE = 44
