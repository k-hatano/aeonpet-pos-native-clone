'use strict'

import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import I18n from 'i18n-js'
import StandardButton from './StandardButton'
import TouchableOpacitySfx from './TouchableOpacitySfx'
import baseStyleValues from 'common/styles/baseStyleValues'

export default class NumberPad extends Component {
  constructor (props) {
    super(props)

    this.state = {
      resultDisplay: '0',
      result: 0,
      currentFontSize: DEFAULT_RESULT_FONT_SIZE
    }
  }

  validateResultLength = number => {
    let resultLength = number.toString().length
    return resultLength <= MAX_RESULT_LENGTH
  }

  resetFontSize = result => {
    let currentResultLength = result.toString().length
    if (currentResultLength <= MAX_NUMBER_IN_ROW) {
      this.setState({
        currentFontSize: DEFAULT_RESULT_FONT_SIZE
      })
      return true
    }
    let currentFontStyle = this.state.currentFontSize
    if (currentFontStyle < DEFAULT_RESULT_FONT_SIZE) {
      return false
    }
    this.setState({
      currentFontSize: currentFontStyle - 4
    })
  }

  actionSetResult = number => {
    const newResultDisplay = parseInt(number, 10).toLocaleString('en-US', { minimumFractionDigits: 0 })
    this.setState({
      resultDisplay: newResultDisplay,
      result: parseInt(number, 10)
    })
  }

  actionNumberPress = number => {
    const { onChange } = this.props
    let newResult = `${this.state.result}${number}`
    if (!this.validateResultLength(newResult)) {
      return false
    }
    let newResultDisplay = parseInt(newResult, 10).toLocaleString('en-US', { minimumFractionDigits: 0 })

    onChange && onChange(parseInt(newResult, 10))

    this.setState({
      resultDisplay: newResultDisplay,
      result: parseInt(newResult, 10)
    })
    this.resetFontSize(newResult)
  }

  actionCPress = () => {
    const { onChange } = this.props
    onChange && onChange(0)
    this.setState({
      resultDisplay: '0',
      result: 0
    })
    this.resetFontSize(0)
  }

  actionBSPress = () => {
    const { onChange } = this.props
    let oldResult = this.state.result
    let newResult = oldResult.toString().length === 1
      ? 0
      : parseInt(oldResult.toString().substr(0, oldResult.toString().length - 1), 10)
    if (!this.validateResultLength(newResult)) {
      return false
    }
    let newResultDisplay = parseInt(newResult, 10).toLocaleString('en-US')

    onChange && onChange(parseInt(newResult, 10))

    this.setState({
      resultDisplay: newResultDisplay,
      result: newResult
    })
    this.resetFontSize(newResult)
  }

  generateFeatureButtons = () => [
    [
      {
        title: '1',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('1')
        }
      },
      {
        title: '2',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('2')
        }
      },
      {
        title: '3',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('3')
        }
      },
      {
        title: 'C',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionCPress()
        }
      }
    ],
    [
      {
        title: '4',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('4')
        }
      },
      {
        title: '5',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('5')
        }
      },
      {
        title: '6',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('6')
        }
      },
      {
        title: 'BS',
        flex: 1,
        color: '#d8d8d8',
        onPress: () => {
          this.actionBSPress()
        }
      }
    ],
    [
      {
        title: '7',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('7')
        }
      },
      {
        title: '8',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('8')
        }
      },
      {
        title: '9',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('9')
        }
      }
    ],
    [
      {
        title: '0',
        flex: 1,
        onPress: () => {
          this.actionNumberPress('0')
        }
      },
      {
        title: '00',
        flex: 2.1,
        onPress: () => {
          this.actionNumberPress('00')
        }
      }
    ]
  ]

  render () {
    let listFeatureButton = this.generateFeatureButtons()
    const { numPadComfirm, containerStyles } = this.props
    const { resultDisplay, result, currentFontSize } = this.state
    return (
      <View style={[componentStyles.componentContainer, containerStyles]}>
        <View style={componentStyles.resultSide}>
          <Text style={[componentStyles.resultText, { fontSize: currentFontSize }]}>
            {resultDisplay}
          </Text>
        </View>
        {listFeatureButton.map((listButton, index) =>
          <View key={index} style={componentStyles.featureSide}>
            {listButton.map((button, index) =>
              <View
                key={index}
                style={[
                  componentStyles.featureButton,
                  {
                    flex: button.flex,
                    backgroundColor: button.color || '#f0f0f0'
                  }
                ]}
              >
                <TouchableOpacitySfx
                  style={componentStyles.featureButtonTouch}
                  onPress={
                    !button.type
                      ? button.onPress
                      : () => {
                        button.onPress(result)
                      }
                  }
                >
                  <Text style={componentStyles.featureButtonText}>{button.title}</Text>
                </TouchableOpacitySfx>
              </View>
            )}
          </View>
        )}
        <View style={componentStyles.confirm}>
          <StandardButton
            text={I18n.t('balance.confirm')}
            color={'#ff9024'}
            textStyle={componentStyles.confirmTouchText}
            onPress={() => numPadComfirm && numPadComfirm(result)}
          />
        </View>
      </View>
    )
  }
}

const MAX_NUMBER_IN_ROW = 6
const MAX_RESULT_LENGTH = 15
const DEFAULT_RESULT_FONT_SIZE = 48

const componentStyles = StyleSheet.create({
  componentContainer: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    padding: 10
  },
  resultSide: {
    backgroundColor: '#4a4a4a',
    borderWidth: 0,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 9,
    flex: 1 / 7,
    margin: 5
  },
  resultText: {
    color: '#ffffff',
    fontSize: 36,
    fontWeight: '600',
    justifyContent: 'center'
  },
  featureSide: {
    flex: 1 / 7,
    flexDirection: 'row'
  },
  featureButton: {
    width: '24%',
    height: '90%',
    borderColor: '#979797',
    borderWidth: 1,
    borderRadius: 10,
    margin: 5
  },
  featureButtonTouch: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  featureButtonText: {
    fontSize: 48,
    letterSpacing: -0.77,
    color: '#4a4a4a',
    fontWeight: '500'
  },
  confirm: {
    flex: 1 / 7,
    margin: 5
  },
  confirmTouchText: {
    fontSize: 48,
    fontFamily: 'Helvetica'
  }
})
