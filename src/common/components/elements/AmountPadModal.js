import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../../styles/elements/AmountPadModal'
import baseStyle from 'common/styles/baseStyle'
import AmountPad from './AmountPad'
import ModalFrame from '../widgets/ModalFrame'
import Modal from '../widgets/Modal'
import AlertView from 'common/components/widgets/AlertView'

/**
 * @typedef {Object} AmountPadModalProps
 * @property {number} maxLength
 * @property {number} format
 * @property {number} maxValue
 * @property {number} justValue
 * @property {number} mode
 * @property {function} onComplete
 * @property {boolean} disableConfirmButton
 * @property {boolean} disableCounterSuffix
 * @property {boolean} disableClearPoint
 * @property {function} onClearPoint
 * @property {string} title
 */

export default class AmountPadModal extends React.Component {
  static propTypes = {
    maxLength: PropTypes.number,
    format: PropTypes.number,
    maxValue: PropTypes.number,
    justValue: PropTypes.number,
    mode: PropTypes.number,
    onComplete: PropTypes.func,
    disableConfirmButton: PropTypes.bool,
    disableCounterSuffix: PropTypes.bool,
    disableClearPoint: PropTypes.bool,
    onClearPoint: PropTypes.func,
    title: PropTypes.string,
    additionalTitles: PropTypes.array,
    useableBasePoints: PropTypes.number,
    maxUsePoint: PropTypes.number
  }

  render () {
    const { title, onComplete, onClearPoint, ...passProps } = this.props
    const onCompleteForOverride = async amount => {
      if (await onComplete(amount)) {
        Modal.close()
      }
    }
    const onClearPointForOverride = async amount => {
      if (await onClearPoint(amount)) {
        Modal.close()
      }
    }

    return (
      <ModalFrame style={styles.layoutRoot} title={title} onClose={() => Modal.close()}>
        <AmountPad
          onComplete={onCompleteForOverride}
          onClearPoint={onClearPointForOverride}
          {...passProps}
          useableBasePoints={this.props.useableBasePoints}
        />
      </ModalFrame>
    )
  }

  /**
   *
   * @param {AmountPadModalProps} props
   */
  static open (props) {
    Modal.open(AmountPadModal, {
      props: props,
      isBackgroundVisible: true
    })
  }
}
