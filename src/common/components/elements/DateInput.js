'use strict'

import React, {Component} from 'React'
import { View, TextInput, TouchableOpacity } from 'react-native'
import Calendar from 'common/components/widgets/Calendar'
import Modal from 'common/components/widgets/Modal'
import TimePicker from 'common/components/widgets/TimePicker'
import I18n from 'i18n-js'
import style from 'common/styles/elements/DateInput'
import PropTypes from 'prop-types'
import { getEndOfToday, getToday } from 'common/utils/dateTime'

export default class DateInput extends Component {
  static propTypes = {
    useTimePicker: PropTypes.bool,
    textInputStyle: PropTypes.number,
    onChangeDateTime: PropTypes.func,
    isAvailableAfterTomorrow: PropTypes.bool,
    isUnavailableYesterdayBefore: PropTypes.bool,
    initialDate: PropTypes.string,
    initialTime: PropTypes.string
  }

  constructor (props) {
    super(props)
    this.state = {
      date: I18n.l('date.formats.year_month_day', new Date()),
      time: '',
      dateTimeLabel: ''
    }
  }

  componentWillMount () {
    if (this.props.useTimePicker) {
      this.setState({time: I18n.l('date.formats.time',
          this.props.fromDateTime ? getToday() : this.props.toDateTime ? getEndOfToday() : new Date())})
      this.setState({dateTimeLabel: ''})
    }
    if (this.props.initialDate) {
      this.setState({date: this.props.initialDate})
    }
    if (this.props.initialTime) {
      this.setState({time: this.props.initialTime})
    }
  }

  componentWillReceiveProps (nextProps) {
    if (!nextProps.compare && nextProps.fromDateTime) {
      this.setState({date: nextProps.fromDateTime.format('YYYY-MM-DD'),
        time: nextProps.fromDateTime.format('HH:mm')})
    } else if (!nextProps.compare && nextProps.toDateTime) {
      this.setState({date: nextProps.toDateTime.format('YYYY-MM-DD'),
        time: nextProps.toDateTime.format('HH:mm')})
    }
  }

  render () {
    const value = this.props.useTimePicker ? this.state.date + ' ' + this.state.time : this.state.date
    return (
      <TouchableOpacity onPress={() => this._openModal(this.refs.DateInput)}>
        <View ref='DateInput'>
          <TextInput
            editable={false}
            style={this.props.textInputStyle ? this.props.textInputStyle : style.textInput}
            value={value} />
        </View>
      </TouchableOpacity>
    )
  }

  _openModal (ref) {
    Modal.openBy(ModalContent, ref, {
      direction: Modal.DIRECTION.BOTTOM,
      isBackgroundVisible: false,
      props: {
        compareDateTime: this.props.compareDateTime,
        date: this.state.date,
        time: this.state.time,
        useTimePicker: this.props.useTimePicker,
        onChangeDateTime: dateTime => this.props.onChangeDateTime(dateTime),
        onSetState: state => this.setState(state),
        isAvailableAfterTomorrow: this.props.isAvailableAfterTomorrow,
        isUnavailableYesterdayBefore: this.props.isUnavailableYesterdayBefore
      }
    })
  }
}

class ModalContent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dateTime: new Date(),
      date: this.props.date,
      time: this.props.time
    }
  }

  componentWillMount () {
    this.props.onSetState({dateTimeLabel: ''})
  }

  componentWillUnmount () {
    if (this.props.useTimePicker) {
      this.props.onChangeDateTime(this.state.date + ' ' + this.state.time)
    } else {
      this.props.onChangeDateTime(this.state.date)
    }
  }

  _renderTimePicker () {
    if (this.props.useTimePicker) {
      return (
        <View style={{left: 400, width: 200}}>
          <TimePicker
            date={this.state.dateTime}
            onChange={times => {
              this.props.onSetState({ time: I18n.l('date.formats.time', times) })
              this.setState({time: I18n.l('date.formats.time', times)})
            }} />
        </View>
      )
    }
  }

  render () {
    const minDate = this.props.isUnavailableYesterdayBefore ? new Date() : null
    const maxDate = !this.props.isAvailableAfterTomorrow ? new Date() : null
    const containerStyle = !this.props.useTimePicker ? { width: 400 } : null
    return (
      <View style={[style.calendarContainer, containerStyle]}>
        <Calendar
          visible={true}
          minDate={minDate}
          maxDate={maxDate}
          style={style.calendar}
          onChange={item => {
            this.props.onSetState({ date: item.format('YYYY-MM-DD') })
            this.setState({date: item.format('YYYY-MM-DD')})
          }} />
        {this._renderTimePicker()}
      </View>
    )
  }
}
