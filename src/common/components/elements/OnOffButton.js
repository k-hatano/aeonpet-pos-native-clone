import React, { Component } from 'react'
import { Text, TouchableOpacity } from 'react-native'
import componentStyles from '../../styles/elements/OnOffButton'
import I18n from 'i18n-js'

export default class OnOffButton extends Component {
  render () {
    return (
      <TouchableOpacity style={[componentStyles.layoutRoot, this.props.style]} onPress={() => this.props.onValueChange(!this.props.isOn)}>
        <Text>{this.props.isOn ? I18n.t('common.on') : I18n.t('common.off')}</Text>
      </TouchableOpacity>
    )
  }
}
