'use strict'

import React, { Component } from 'react'
import { TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Foundation from 'react-native-vector-icons/Foundation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import Zocial from 'react-native-vector-icons/Zocial'
import { isEmpty } from '../../utils/validations'
import componentStyles from '../../styles/layout/Page'

export default class IconButton extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    const { icon, type, text, color, containerStyle, textStyle, iconSize, onPress } = this.props

    return (
      <TouchableOpacity style={[componentStyles.orangeButton, containerStyle]} onPress={onPress}>
        {type === 'ionic' && <Icon name={icon} size={iconSize} color={color} />}
        {type === 'material' && <MIcon name={icon} size={iconSize} color={color} />}
        {type === 'entypo' && <Entypo name={icon} size={iconSize} color={color} />}
        {type === 'evilIcons' && <EvilIcons name={icon} size={iconSize} color={color} />}
        {type === 'fontawesome' && <FontAwesome name={icon} size={iconSize} color={color} />}
        {type === 'foundation' && <Foundation name={icon} size={iconSize} color={color} />}
        {type === 'materialicons' && <MaterialIcons name={icon} size={iconSize} color={color} />}
        {type === 'simpleLineicons' && <SimpleLineIcons name={icon} size={iconSize} color={color} />}
        {type === 'octicons' && <Octicons name={icon} size={iconSize} color={color} />}
        {type === 'zocial' && <Zocial name={icon} size={iconSize} color={color} />}
        {!isEmpty(text) && <Text style={textStyle}>{text}</Text>}
      </TouchableOpacity>
    )
  }
}
