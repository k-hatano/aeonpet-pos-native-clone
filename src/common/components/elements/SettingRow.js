'use strict'

import React, { Component } from 'react'
import { View, Text } from 'react-native'
import settingStyles from '../../../modules/setting/styles/Setting'

export default class SettingRow extends Component {
  render () {
    return (
      <View style={settingStyles.subContainer}>
        <Text style={settingStyles.subContainerHeader}>{this.props.label}</Text>
        <View style={settingStyles.subContainerItemView}>
          {this.props.children}
        </View>
      </View>
    )
  }
}
