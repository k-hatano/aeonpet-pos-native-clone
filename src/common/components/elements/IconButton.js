import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/elements/IconButton'
import CustomPropTypes from '../CustomPropTypes'

import Icon from 'react-native-vector-icons/Ionicons'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Foundation from 'react-native-vector-icons/Foundation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import Zocial from 'react-native-vector-icons/Zocial'

export const ICON_BUTTON_TYPE = {
  IONIC: 'ionic',
  MATERIAL: 'material',
  ENTYPO: 'entypo',
  EVIL_ICONS: 'evilIcons',
  FONTAWESOME: 'fontawesome',
  FOUNDATION: 'foundation',
  MATERIAL_ICONS: 'materialicons',
  SIMPLE_LINE_ICONS: 'simpleLineicons',
  OCTICONS: 'octicons',
  ZOCIAL: 'zocial'
}
export default class IconButton extends Component {
  static propTypes = {
    type: PropTypes.string,
    icon: PropTypes.string,
    style: CustomPropTypes.Style,
    onPress: PropTypes.func
  }

  _selectIconComponent (type) {
    switch (type) {
      case ICON_BUTTON_TYPE.IONIC:
        return Icon

      case ICON_BUTTON_TYPE.MATERIAL:
        return MIcon

      case ICON_BUTTON_TYPE.ENTYPO:
        return Entypo

      case ICON_BUTTON_TYPE.EVIL_ICONS:
        return EvilIcons

      case ICON_BUTTON_TYPE.FONTAWESOME:
        return FontAwesome

      case ICON_BUTTON_TYPE.FOUNDATION:
        return Foundation

      case ICON_BUTTON_TYPE.MATERIAL_ICONS:
        return MaterialIcons

      case ICON_BUTTON_TYPE.SIMPLE_LINE_ICONS:
        return SimpleLineIcons

      case ICON_BUTTON_TYPE.OCTICONS:
        return Octicons

      case ICON_BUTTON_TYPE.ZOCIAL:
        return Zocial

      default:
        console.warn('Invalid icon button type ' + type)
        return null
    }
  }

  render () {
    const { color, fontSize: size, ...baseStyle } = StyleSheet.flatten(this.props.style)
    const IconComponent = this._selectIconComponent(this.props.type)

    return (
      <TouchableOpacity style={[styles.button, baseStyle]} onPress={this.props.onPress}>
        <IconComponent name={this.props.icon} size={size} color={color} />
      </TouchableOpacity>
    )
  }
}
