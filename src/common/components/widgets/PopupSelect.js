import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, TouchableOpacity, ScrollView } from 'react-native'
import Modal from './OldModal'

const screen = Dimensions.get('window')

/**
 * USAGE
 * Add <PopupSelectModal.PopupSelect /> into the most parent component
 * Then put this component :
 *
 *  dataSource structure:
 *  [
 *    { id: i, title:`item${i}` }
 *    { id: i, title:`item${i}` }
 *    { id: i, title:`item${i}` }
 *  ]
 *
 *  <TouchableOpacity
 *    onPress={() => PopupSelectModal.open({
 *      onSelect: (item) => this.setState({ selectedItem: item }),
 *      dataSource,
 *        buttons: [
 *          {
 *            onPress: () => { console.log('on sync') },
 *            style: {},
 *            textStyle: {},
 *            title: 'Sync',
 *            willClose: true
 *          },
 *          {
 *            onPress: () => { console.log('on sort') },
 *            style: {},
 *            textStyle: {},
 *            title: 'Sort'
 *          }
 *        ]
 *      }
 *   )}
 *  >
 *    <Text style={[styles.btnText, { backgroundColor: '#ddd' }]}>{this.state.selectedItem.title || 'Popup'}</Text>
 *  </TouchableOpacity>
 *
 */
class PopupSelect extends Component {
  constructor (props) {
    super(props)

    this.state = {
      selectedItem: {},
      dataSource: []
    }
  }

  componentDidMount () {
    PopupSelect.instance = this
  }

  componentWillUnmount () {
    delete PopupSelect.instance
  }

  open = props => {
    Modal.open('popupSelect')
    this.setState({ ...props })
  }

  close = () => {
    Modal.close('popupSelect')
  }

  onItemChoose = rowData => {
    const { onSelect } = this.state

    this.setState({ selectedItem: rowData })
    if (onSelect) {
      onSelect(rowData)
    }
    Modal.close('popupSelect')
  }

  _renderListData = () => {
    const { dataSource } = this.state

    if (!dataSource) {
      return <View />
    }

    const { highlightBackgroundColor, itemStyle, itemTextStyle } = this.state

    return dataSource.map((data, index) => {
      let backgroundColor = this.state.selectedItem.id && this.state.selectedItem.id === data.id
        ? { backgroundColor: highlightBackgroundColor || '#ccc' }
        : {}

      return (
        <TouchableOpacity
          key={index}
          style={[styles.item, backgroundColor, itemStyle || {}]}
          onPress={() => this.onItemChoose(data, index)}
          underlayColor='#ddd'
        >
          <Text style={itemTextStyle}>{data.title}</Text>
        </TouchableOpacity>
      )
    })
  }

  render () {
    const { buttons, dataSource } = this.state

    return (
      <Modal.InnerModal
        style={[styles.modal]}
        headerStyle={{ marginBottom: 5 }}
        position={'center'}
        modalRef={'popupSelect'}
        backdropOpacity={0}
        animationDuration={0}
      >
        <ScrollView style={styles.itemWrapper}>
          {dataSource && dataSource.length > 0 && this._renderListData()}
        </ScrollView>
        <View style={styles.footer}>
          {buttons &&
            buttons.map((button, id) =>
              <TouchableOpacity
                key={id}
                onPress={() => {
                  button.onPress()
                  if (button.willClose) {
                    Modal.close('popupSelect')
                  }
                }}
                style={[styles.btn, button.style]}
              >
                <Text style={[styles.btnText, button.textStyle]}>{button.title}</Text>
              </TouchableOpacity>
            )}
        </View>
      </Modal.InnerModal>
    )
  }
}

const PopupSelectModal = {
  PopupSelect,
  open: props => {
    if (PopupSelect.instance) {
      PopupSelect.instance.open(props)
    }
  },
  close: () => {
    if (PopupSelect.instance) {
      PopupSelect.instance.close()
    }
  }
}

export default PopupSelectModal

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    paddingTop: 50,
    flex: 1,
    zIndex: 999
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: screen.height / 3,
    width: screen.width * 2 / 3
  },
  btn: {
    width: 100,
    margin: 10,
    padding: 6,
    backgroundColor: '#2d6ec7',
    borderRadius: 4,
    alignItems: 'center'
  },
  btnText: {
    color: '#fff'
  },
  btnModal: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: 'transparent'
  },
  text: {
    color: '#000',
    fontSize: 22
  },
  picker: {
    width: 150,
    borderWidth: 0
  },
  itemWrapper: {
    width: '100%'
  },
  item: {
    width: '100%',
    padding: 10
  },
  footer: {
    flexDirection: 'row',
    width: '100%',
    borderColor: '#ccc',
    borderTopWidth: 1,
    paddingTop: 10,
    justifyContent: 'flex-end'
  }
})
