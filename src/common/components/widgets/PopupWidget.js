'use strict'

import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class PopupWidget extends Component {
  render () {
    const { borderColor, width, height, close } = this.props

    return (
      <View
        style={[
          localStyles.modalPopupWrap,
          {
            borderColor: borderColor || '#007fff',
            width: width,
            height: height
          }
        ]}
      >
        <TouchableOpacity
          style={[
            localStyles.closeButton,
            {
              borderColor: borderColor || '#007fff',
              backgroundColor: borderColor || '#007fff'
            }
          ]}
          onPress={e => {
            close()
          }}
        >
          <Icon name='close' size={25} backgroundColor={borderColor || '#007fff'} color={'#fff'} />
        </TouchableOpacity>
        {this.props.children}
      </View>
    )
  }
}

const localStyles = StyleSheet.create({
  modalPopupWrap: {
    backgroundColor: '#fff',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  closeButton: {
    position: 'absolute',
    top: -2,
    right: -2,
    width: 30,
    height: 30,
    borderWidth: 2
  }
})
