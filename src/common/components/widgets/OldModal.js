import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import ModalBox from 'react-native-modalbox'
import ImageButton from '../elements/ImageButton'
import { playButtonTouch } from '../../utils/sfx'
import componentStyles from '../../styles/widgets/Modal'

class InnerModal extends Component {
  componentDidMount () {
    const { modalRef } = this.props
    Modal.register({ [modalRef]: this.refs.modal })
  }

  componentWillUnmount () {
    const { modalRef } = this.props
    Modal.unRegister(modalRef)
  }

  render () {
    const {
      style,
      title,
      titleIcon,
      onClose,
      header = true,
      headerStyle = { marginBottom: 58 },
      titleStyle = {},
      hasCloseButton = true,
      children,
      ...rest
    } = this.props

    return (
      <ModalBox ref={'modal'} style={[componentStyles.modalWrapper, style]} swipeToClose={false} {...rest}>
        <Image
          style={[componentStyles.modalWrapper, style, componentStyles.modalWrapperShadow]}
          source={require('../../../assets/images/shadow_box.png')}
          resizeMode='stretch'
        />
        <View style={componentStyles.modalContentWrapper}>
          {header &&
            <View style={[componentStyles.headerContainer, headerStyle]}>
              <View style={componentStyles.titleWrapper}>
                <View style={componentStyles.emptyLine} />
                <View style={[componentStyles.titleLine, titleStyle]}>
                  {titleIcon &&
                    <ImageButton
                      toggle
                      style={componentStyles.titleImage}
                      appearance={{
                        normal: titleIcon,
                        highlight: titleIcon
                      }}
                    />}
                  <Text style={componentStyles.titleText}> {title} </Text>
                </View>
              </View>
              {hasCloseButton &&
                <View style={componentStyles.buttonCloseWrapper}>
                  <View style={componentStyles.imageCloseWrapper}>
                    <ImageButton
                      toggle
                      style={componentStyles.buttonCloseImage}
                      onPress={() => {
                        playButtonTouch()
                        if (onClose && typeof onClose === 'function') {
                          onClose()
                        }
                        this.refs.modal.close()
                      }}
                      appearance={{
                        normal: require('../../../assets/images/popup/close.png'),
                        highlight: require('../../../assets/images/popup/close.png')
                      }}
                    />
                  </View>
                </View>
              }
            </View>}
          {children}
        </View>
      </ModalBox>
    )
  }
}

const Modal = {
  InnerModal,

  instance: null,

  register: ref => {
    this.instance = { ...this.instance, ...ref }
  },

  unRegister: modalRef => {
    delete this.instance[modalRef]
  },

  open: modalRef => {
    if (this.instance[modalRef]) {
      this.instance[modalRef].open()
    }
  },

  close: modalRef => {
    if (this.instance[modalRef]) {
      this.instance[modalRef].close()
    }
  }
}

export default Modal
