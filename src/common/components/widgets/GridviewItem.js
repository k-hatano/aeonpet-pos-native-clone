'use strict'

import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import { formatPriceWithCurrency } from '../../utils/formats'
import componentStyles from '../../styles/widgets/GridviewItem'

export default function GridviewItem ({ item, chooseProduct, itemWidth }) {
  return (
    <TouchableOpacity key={item.id} underlayColor={'#bbb'} onPress={() => chooseProduct(item)}>
      <View style={[componentStyles.recordItem, { width: itemWidth }]}>
        <View style={componentStyles.recordItemTitle}>
          <Text style={componentStyles.recordItemTitleText}>
            {I18n.t('product.product_name')}
          </Text>
          <Text style={componentStyles.recordItemTitleText}>
            {item.name}
          </Text>
        </View>
        <View style={componentStyles.recordItemInfo}>
          <Text style={componentStyles.recordItemWarning}>
            {I18n.t('order.out_of_stock')}
          </Text>
          <Text style={componentStyles.recordItemText}>
            {`${formatPriceWithCurrency(item.price)}`}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}
