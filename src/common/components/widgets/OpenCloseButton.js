import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/widgets/OpenCloseButton'
import CustomPropTypes from '../CustomPropTypes'

export default class OpenCloseButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    onIsOpenChanged: PropTypes.func,
    style: CustomPropTypes.Style
  }

  constructor () {
    super()
    this.state = {
      isOpen: false
    }
  }

  _onPress () {
    const current = this.state.isOpen
    this.setState({...this.state, isOpen: !current})
    this.props.onIsOpenChanged(!current)
  }

  render () {
    return (
      <TouchableOpacity onPress={() => this._onPress()}>
        <View style={[styles.root, this.props.style]}>
          <View style={{flexDirection: 'row'}} />
            <Image
              source = {this.props.source} />
            <Text style={{color: 'white', fontSize: 18}}>
              {this.props.text}
            </Text>
            {!this.state.isOpen &&
              <Image
                style={{marginRight: 10}}
                source = {require('../../../assets/images/down_arrow.png')} />
            }
            {this.state.isOpen &&
              <Image
                style={{marginRight: 10}}
                source = {require('../../../assets/images/up_arrow.png')} />
            }
        </View>
      </TouchableOpacity>
    )
  }
}
