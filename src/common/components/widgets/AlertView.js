import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, View, ScrollView, Animated, Easing } from 'react-native'
import baseStyle from '../../styles/baseStyle'
import { CommandButton } from '../elements/StandardButton'
import Modal from './Modal'
import styles from '../../styles/widgets/NoticeView'
import { calculateModalSize } from '../../models'

export default class AlertView extends Component {
  static propTypes = {
    message: PropTypes.string,
    onOk: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      progress: new Animated.Value(0),
    }
  }

  startAnimation(){
    const { progress } = this.state
    progress.setValue(0)
    Animated.timing(
        progress,
      {
        toValue: 1,
        duration: 1500,
        easing: Easing.linear
      }
    ).start(()=>{
      this.startAnimation()
    })
  }

  componentDidMount () {
    this.startAnimation()  
  }

  render () {
    const { progress } = this.state
    
    const rolling = progress.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    const modal = calculateModalSize(this.props.message)

    return (
      <View style={[styles.main, {height: modal.height}]}>
        <MessageView
          message={this.props.message}
          canScroll={modal.canScroll} />
        <View style={styles.buttonContainer}>
          {this.props.loading && <Animated.View style={{
            width: 88,
            height: 88,
            borderRadius: 44,
            borderColor:'#ff9024',
            borderWidth:11,
            borderTopColor: 'transparent',
            transform: [{rotate: rolling}]
          }} />}
          {!this.props.loading && <CommandButton
            style={styles.okButton}
            onPress={this.props.onOk}
            text='OK' />}
        </View>
      </View>
    )
  }

  static show (message, onOk, enableBackgroundCloseFlg = false, closeCurrentModal) {
    class TemporaryNotice extends Component {
      render () {
        return (
          <AlertView
            message={message}
            onOk={() => { if (!onOk || onOk()) { Modal.close() } }} />
        )
      }
    }
    Modal.open(TemporaryNotice, {
      enableBackgroundClose: enableBackgroundCloseFlg,
      isBackgroundVisible: true,
      cannotAbort: true
    }, closeCurrentModal)
  }

  static showWithProgress (message, onOk, workPromise, onError) {
    workPromise().then(()=>{
      onOk()
    }).catch((error)=>{
      onError(error)
    })
    class TemporaryNotice extends Component {
      constructor(props){
        super(props)
      }

      render () {
        return (
          <AlertView
            loading={true}
            message={message}
            onOk={() => { if (!onOk || onOk()) { Modal.close() } }} />
        )
      }
    }
    Modal.open(TemporaryNotice, {
      enableBackgroundClose: false,
      isBackgroundVisible: true,
      cannotAbort: true
    }, true)
  }

  static showAsync (message, onOk) {
    return new Promise((resolve, reject) => {
      class TemporaryNotice extends Component {
        render () {
          return (
            <AlertView
              message={message}
              onOk={() => {
                if (!onOk || onOk()) {
                  Modal.close()
                  resolve()
                }
                return true
              }} />
          )
        }
      }
      Modal.open(TemporaryNotice, {
        enableBackgroundClose: false,
        isBackgroundVisible: true,
        cannotAbort: true
      })
    })
  }
}

export class MessageView extends Component {
  static propTypes = {
    message: PropTypes.string,
    canScroll: PropTypes.bool
  }

  render () {
    if (this.props.canScroll) {
      return (
        <ScrollView>
          <Text style={[baseStyle.mainText, styles.textStyle]}>
            {this.props.message}
          </Text>
        </ScrollView>
      )
    } else {
      return (
        <Text style={[baseStyle.mainText, styles.textStyle]}>
          {this.props.message}
        </Text>
      )
    }
  }
}
