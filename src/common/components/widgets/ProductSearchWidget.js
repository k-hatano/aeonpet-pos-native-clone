import React, { Component } from 'react'
import { View, TextInput } from 'react-native'
import I18n from 'i18n-js'
import StandardButton from '../elements/StandardButton'
import componentStyles from '../../styles/widgets/ProductSearchWidget'

export default class ProductSearchWidget extends Component {
  render () {
    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.searchBar}>
          <TextInput
            style={componentStyles.searchInput}
            placeholder={I18n.t('order.product_search_holder')}
            onChangeText={text => console.log('searching for ', text)}
          />
        </View>
        <View style={componentStyles.searchButton}>
          <StandardButton
            onPress={() => {}}
            text={I18n.t('common.search')}
            textStyle={componentStyles.searchButtonText}
          />
        </View>
      </View>
    )
  }
}
