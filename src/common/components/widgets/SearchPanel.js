'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import I18n from 'i18n-js'
import SearchInput from '../elements/SearchInput'

export default class SearchPanel extends Component {
  render () {
    const { mStyle, containerStyle, placeholder, disabled } = this.props

    return (
      <View style={mStyle}>
        <View style={[styles.container, containerStyle]}>
          <SearchInput icon={'md-search'} placeholder={placeholder} />
          <TouchableOpacity disabled={disabled} style={disabled ? styles.disabledSearchButton : styles.searchButton}>
            <Text style={disabled ? styles.disabledButtonText : styles.buttonText}> {I18n.t('common.search')} </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchButton: {
    backgroundColor: '#000',
    width: 70,
    height: 30,
    margin: 5,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 3
  },
  disabledSearchButton: {
    backgroundColor: '#ccc',
    width: 70,
    height: 30,
    margin: 5,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 3
  },
  buttonText: {
    color: '#ccc'
  },
  disabledButtonText: {
    color: '#000'
  }
})
