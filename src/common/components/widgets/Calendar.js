'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import Dates from 'react-native-dates'
import moment from 'moment'
import styles from '../../styles/layout/Page'

export default class Calendar extends Component {
  render () {
    const { position, visible, onChange, minDate, maxDate, selectStartDate, selectEndDate } = this.props

    const isDateBlocked = date => {
      if (!minDate && !maxDate) {
        return false
      }
      if (minDate && maxDate) {
        return !date.isBetween(moment(minDate), moment(maxDate), 'day', true)
      }
      if (!minDate && maxDate) {
        return date.isAfter(moment(maxDate), 'day')
      }
      if (minDate && !maxDate) {
        return date.isBefore(moment(minDate), 'day')
      }
    }
    if (!visible) {
      return <View />
    }
    return (
      <View style={[styles.calendarPanel, position]}>
        <Dates
          startDate={moment(selectStartDate)}
          endDate={moment(selectEndDate)}
          isDateBlocked={isDateBlocked}
          onDatesChange={({ date }) => {
            if (date && onChange) {
              onChange(date)
            }
          }}
        />
      </View>
    )
  }
}
