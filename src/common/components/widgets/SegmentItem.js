'use strict'

import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import componentStyles from '../../styles/widgets/GridviewItem'

export default function SegmentItem ({ item, chooseProduct, itemWidth }) {
  return (
    <TouchableOpacity key={item.id} underlayColor={'#bbb'} onPress={() => chooseProduct(item)}>
      <View style={[componentStyles.recordItem, { width: itemWidth }]}>
        <View style={componentStyles.recordItemTitle}>
          <Text style={componentStyles.recordItemTitleText}>
            001
          </Text>
          <Text style={componentStyles.recordItemTitleText}>
            {item.name}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}
