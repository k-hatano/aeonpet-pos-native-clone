import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, View, ScrollView } from 'react-native'
import baseStyle from '../../styles/baseStyle'
import { ProceedButton, WeakProceedButton } from '../elements/StandardButton'
import Modal2 from './Modal'
import styles from '../../styles/widgets/AlertView'
import { calculateModalSize } from '../../models'

export default class ConfirmView extends Component {
  static propTypes = {
    message: PropTypes.string,
    onOk: PropTypes.func,
    onCancel: PropTypes.func
  }

  render () {
    const containerStyle = this.props.style && this.props.style.container ? this.props.style.container : {}
    const textStyle = this.props.style && this.props.style.text ? this.props.style.text : {}
    const modal = calculateModalSize(this.props.message)
    return (
      <View style={[styles.main, containerStyle, { height: modal.height }]}>
        <MessageView
          message={this.props.message}
          textStyle={textStyle}
          canScroll={modal.canScroll} />
        <View style={styles.buttonContainer}>
          <ProceedButton
            style={styles.okButton}
            onPress={this.props.onOk}
            text='OK' />
          <WeakProceedButton
            style={styles.cancelButton}
            onPress={this.props.onCancel}
            text='キャンセル' />
        </View>
      </View>
    )
  }

  static show (message, onOk, onCancel, style) {
    class TemporaryAlert extends Component {
      render () {
        return (
          <ConfirmView
            message={message}
            onOk={() => { if (!onOk || onOk() !== false) { Modal2.close() } }}
            onCancel={() => { if (!onCancel || onCancel() !== false) { Modal2.close() } }}
            style={style} />
        )
      }
    }
    Modal2.open(TemporaryAlert, {isBackgroundVisible: true, enableBackgroundClose: false, cannotAbort: true})
  }

  /**
   *
   * @param message
   * @param style
   * @return {Promise.<boolean>}
   */
  static showAsync (message, style) {
    return new Promise((resolve, reject) => {
      ConfirmView.show(message, () => {
        resolve(true)
      }, () => {
        resolve(false)
      }, style)
    })
  }
}

export class MessageView extends Component {
  static propTypes = {
    message: PropTypes.string,
    textStyle: PropTypes.object,
    canScroll: PropTypes.bool
  }

  render () {
    if (this.props.canScroll) {
      return (
        <ScrollView>
          <Text style={[baseStyle.mainText, {marginHorizontal: 36}, this.props.textStyle]}>
            {this.props.message}
          </Text>
        </ScrollView>
      )
    } else {
      return (
        <Text style={[baseStyle.mainText, styles.textStyle, this.props.textStyle]}>
          {this.props.message}
        </Text>
      )
    }
  }
}
