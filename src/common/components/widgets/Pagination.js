'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import I18n from 'i18n-js'

export default class Pagination extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentPage: 1,
      totalPage: 0
    }
  }

  componentWillMount () {
    const { currentPage, totalPage } = this.props
    this.setState({
      currentPage: currentPage,
      totalPage: totalPage
    })
  }

  _goToPage ({ currentPage }) {
    const { goToPage } = this.props
    this.setState({ currentPage: currentPage })
    goToPage({ page: currentPage })
  }

  _goToNextPage = () => {
    const { goToPage } = this.props
    const currentPage = this.state.currentPage + 1
    this.setState({ currentPage: currentPage })
    goToPage({ page: currentPage })
  }

  _goToPreviousPage = () => {
    const { goToPage } = this.props
    const currentPage = this.state.currentPage - 1
    this.setState({ currentPage: currentPage })
    goToPage({ page: currentPage })
  }

  renderNumberButtons () {
    let btnPages = []
    for (let i = 1; i <= this.state.totalPage; i++) {
      let isActive = false
      let buttonStyle = componentStyles.defaultNumberButton
      let textStyle = componentStyles.defaultNumberText
      if (this.state.currentPage === i) {
        isActive = true
        buttonStyle = componentStyles.activeNumberButton
        textStyle = componentStyles.activeNumberText
      }

      btnPages.push(
        <View key={i} style={[componentStyles.defaultNumber, buttonStyle]}>
          <TouchableOpacity
            disabled={isActive}
            onPress={() => this._goToPage({ currentPage: i })}
            style={componentStyles.defaultToggle}
          >
            <View style={componentStyles.defaultToggle}>
              <Text style={textStyle}>{i}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
    return btnPages
  }

  render () {
    let prevDisabled = null
    let nextDisabled = null
    let prevButtonStyle = componentStyles.activeNavButton
    let nextButtonStyle = componentStyles.activeNavButton
    if (this.state.currentPage === 1) {
      prevDisabled = true
      prevButtonStyle = componentStyles.disableNavButton
    }
    if (this.state.currentPage === this.state.totalPage) {
      nextDisabled = true
      nextButtonStyle = componentStyles.disableNavButton
    }

    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.defaultStyle}>
          <TouchableOpacity
            disabled={prevDisabled}
            onPress={this._goToPreviousPage}
            style={componentStyles.defaultToggle}
          >
            <Text style={prevButtonStyle}> {I18n.t('common.previous_page')} </Text>
          </TouchableOpacity>
        </View>
        {this.renderNumberButtons()}
        <View style={componentStyles.defaultStyle}>
          <TouchableOpacity disabled={nextDisabled} onPress={this._goToNextPage} style={componentStyles.defaultToggle}>
            <Text style={nextButtonStyle}> {I18n.t('common.next_page')} </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const componentStyles = StyleSheet.create({
  container: {
    marginTop: 10,
    height: 30,
    flexDirection: 'row',
    borderWidth: 2,
    borderColor: '#ccc',
    borderRadius: 4
  },
  defaultStyle: {
    width: 40,
    height: 26,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  defaultNumber: {
    width: 25,
    height: 25,
    borderWidth: 1,
    borderColor: '#ccc',
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderRightWidth: 0
  },
  defaultToggle: {
    backgroundColor: 'transparent',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  activeNavButton: {
    color: '#3ad800'
  },
  disableNavButton: {
    color: '#c0c0c0'
  },
  activeNumberButton: {
    backgroundColor: '#3ad800'
  },
  activeNumberText: {
    color: '#fff'
  },
  defaultNumberButton: {
    backgroundColor: '#fff'
  },
  defaultNumberText: {
    color: '#000'
  }
})
