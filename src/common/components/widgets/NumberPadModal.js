import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import NumberPad from '../elements/NumberPad'
import Modal from './OldModal'

class NumberPadModalComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      show: false
    }
  }

  componentDidMount () {
    NumberPadModalComponent.instance = this
  }

  componentWillUnmount () {
    delete NumberPadModalComponent.instance
  }

  open = () => {
    Modal.open('syncPopup')
  }

  close = () => {
    Modal.close('syncPopup')
  }

  _onCloseModal = () => {
    const { onCloseModal } = this.props

    onCloseModal && onCloseModal()
  }

  render () {
    const { numPadComfirm } = this.props

    return (
      <Modal.InnerModal
        style={styles.innerModal}
        headerStyle={styles.innerModalHeaderStyle}
        modalRef={'syncPopup'}
        position={'center'}
        animationDuration={0}
        onClose={() => console.log('ON CLOSING')}
        onClosed={() => this._onCloseModal()}
      >
        <View style={styles.popupDisplay}>
          <NumberPad
            containerStyles={styles.numberPadContainer}
            numPadComfirm={value => numPadComfirm && numPadComfirm(value)}
          />
        </View>
      </Modal.InnerModal>
    )
  }
}

/**
 * USAGE:
 * - Add this component (<NumberPadModal.Modal />) to the most outside component
 * - Add NumberPadModal.open() to button or anything else to open it
 * - Add NumberPadModal.close() to button or anything else to close it
 */
const NumberPadModal = {
  NumberPadModalComponent,
  open: () => {
    NumberPadModalComponent.instance && NumberPadModalComponent.instance.open()
  },
  close: () => {
    NumberPadModalComponent.instance && NumberPadModalComponent.instance.close()
  }
}

const styles = StyleSheet.create({
  popupContainer: {
    position: 'absolute',
    zIndex: 999,
    backgroundColor: 'transparent'
  },
  popupDisplay: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    padding: 9
  },
  numberPadContainer: {
    borderColor: '#979797',
    borderWidth: 1,
    width: '100%',
    height: '100%',
    flex: 1
  },
  innerModal: {
    width: 430,
    height: 576
  },
  innerModalHeaderStyle: {
    marginBottom: 5
  }
})

export default NumberPadModal
