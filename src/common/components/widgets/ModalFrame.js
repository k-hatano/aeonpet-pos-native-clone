import React, { Component } from 'react'
import { Text, View } from 'react-native'
// import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../../styles/widgets/ModalFrame'
// import baseStyle from 'common/styles/baseStyle'
import SimpleImageButton from '../elements/SimpleImageButton'
import CustomPropTypes from '../CustomPropTypes'

export default class ModalFrame extends Component {
  static propTypes = {
    title: PropTypes.string,
    onClose: PropTypes.func,
    style: CustomPropTypes.Style,
    disableCloseButton: PropTypes.bool
  }

  render () {
    return (
      <View style={[styles.layoutRoot, this.props.style]}>
        <View style={styles.headerArea}>
          <Text style={styles.titleText} numberOfLines={1}>{this.props.title || ''}</Text>
          {this.props.disableCloseButton ? null : (
            <SimpleImageButton
              style={styles.closeButton}
              imageStyle={styles.closeIcon}
              onPress={this.props.onClose}
              source={require('../../../assets/images/popup/close.png')}
              isSoundEnabled={false}
            />
          )}
        </View>
        <View style={styles.mainArea}>
          {this.props.children}
        </View>
      </View>
    )
  }
}
