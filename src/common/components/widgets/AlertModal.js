import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import I18n from 'i18n-js'
import Modal from './OldModal'
import StandardButton from '../elements/StandardButton'

class AlertModalComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      onOk: null,
      message: '',
      onCancel: null,
      autoClose: true,
      isConfirm: false,
      buttonColor: '#ff9024',
      buttonColorConfirmMode: '#44bcb9'
    }
  }

  componentDidMount () {
    AlertModalComponent.instance = this
  }

  componentWillUnmount () {
    delete AlertModalComponent.instance
  }

  open = (message = '', onOk = null, isConfirm = false, onCancel = null, autoClose = true, color = null) => {
    let { buttonColor, buttonColorConfirmMode } = this.state

    if (color && isConfirm) {
      buttonColorConfirmMode = color
    } else if (color && !isConfirm) {
      buttonColor = color
    }

    this.setState({
      onOk,
      message,
      onCancel,
      isConfirm,
      autoClose,
      buttonColor,
      buttonColorConfirmMode
    })
    // Make sure message changed
    setTimeout(() => Modal.open('commonAlertModal'), 0)
  }

  close = () => {
    Modal.close('commonAlertModal')
  }

  onPressConfirm () {
    const { onOk, autoClose } = this.state
    autoClose && this.close()
    onOk && onOk(this)
  }

  onPressCancel () {
    const { onCancel, autoClose } = this.state
    this.close()
    autoClose && onCancel && onCancel(this)
  }

  render () {
    const { message, isConfirm, buttonColor, buttonColorConfirmMode } = this.state
    return (
      <Modal.InnerModal
        style={styles.innerModal}
        modalRef={'commonAlertModal'}
        position={'center'}
        header={false}
        backdropPressToClose={false}
        animationDuration={0}
      >
        <View style={styles.popupDisplay}>
          <View style={styles.messageContainer}>
            <Text style={styles.messageText}>
              {message}
            </Text>
          </View>
          <View style={styles.buttonsContainer}>
            {!isConfirm
              ? <View style={styles.buttonContainer}>
                <StandardButton
                  text={I18n.t('common.ok')}
                  color={buttonColor}
                  textStyle={styles.confirmTouchText}
                  onPress={() => this.onPressConfirm()}
                />
              </View>
              : <View style={styles.confirmButtonContainer}>
                <View style={styles.buttonConfirmContainer}>
                  <StandardButton
                    text={I18n.t('common.ok')}
                    color={buttonColorConfirmMode}
                    textStyle={styles.confirmTouchText}
                    onPress={() => this.onPressConfirm()}
                  />
                </View>
                <View style={styles.buttonConfirmContainer}>
                  <StandardButton
                    isBorderButton
                    text={I18n.t('common.cancel')}
                    color={buttonColorConfirmMode}
                    textColor={buttonColorConfirmMode}
                    textStyle={styles.confirmTouchText}
                    onPress={() => this.onPressCancel()}
                  />
                </View>
              </View>}
          </View>
        </View>
      </Modal.InnerModal>
    )
  }
}

/**
 *
 * USAGE: add this component (<alertModal.alertModalComponent />) to the most outside component
 * Add AlertModal.show(...) to button or anything else to open alert message with ok button
 * + Message: message want to show
 * + onOk: callback funtion when click oke button
 * + autoClose: auto close popup after click button,
 * + color: button color
 *
 * Add AlertModal.showConfirm(...) to button or anything else to open alert message with oke and cancel button
 * + Message: message want to show
 * + onOk: callback funtion when click oke button
 * + onCancel: callback funtion when click to cancel button
 * + autoClose: auto close popup after click button,
 * + color: button color
 *
 * Add AlertModal.close() to button or anything else to close it
 */
const AlertModal = {
  AlertModalComponent,
  show: (message = '', onOk = null, autoClose = true, color = null) => {
    AlertModalComponent.instance && AlertModalComponent.instance.open(message, onOk, false, null, autoClose, color)
  },
  showConfirm: (message = '', onOk = null, onCancel = null, autoClose = true, color = null) => {
    AlertModalComponent.instance && AlertModalComponent.instance.open(message, onOk, true, onCancel, autoClose, color)
  },
  close: () => {
    AlertModalComponent.instance && AlertModalComponent.instance.close()
  }
}

const styles = StyleSheet.create({
  popupDisplay: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    padding: 32,
    flexDirection: 'column'
  },
  innerModal: {
    width: 640,
    height: 300,
    zIndex: 9999
  },
  confirmTouchText: {
    fontSize: 36,
    fontFamily: 'HiraginoSans-W6',
    letterSpacing: -0.58,
    fontWeight: '500'
  },
  messageContainer: {
    flex: 3,
    width: '100%',
    alignItems: 'center'
  },
  messageText: {
    fontSize: 24,
    color: '#4a4a4a',
    letterSpacing: -0.38
  },
  buttonsContainer: {
    flex: 2,
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  buttonContainer: {
    width: 312,
    height: 64
  },
  confirmButtonContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end'
  },
  buttonConfirmContainer: {
    width: 270,
    height: 64
  }
})

export default AlertModal
