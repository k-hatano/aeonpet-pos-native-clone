'use strict'

import React, { Component } from 'react'
import { Animated, View, Text, TouchableOpacity } from 'react-native'
import componentStyles from '../../styles/widgets/VerticalTab'

const DEFAULT_TAB_WIDTH = 150
const ANIMATION_DURATION = 60
const ANIMATION_PADDING_LEFT = 100

export class VerticalTabChild extends Component {
  constructor (props) {
    super(props)

    this.state = {
      animation: new Animated.Value(ANIMATION_PADDING_LEFT),
      opacity: new Animated.Value(0)
    }
  }

  componentDidMount () {
    Animated.parallel([
      Animated.spring(this.state.animation, {
        toValue: 0,
        duration: ANIMATION_DURATION
      }),
      Animated.timing(this.state.opacity, {
        toValue: 1,
        duration: ANIMATION_DURATION
      })
    ]).start()
  }

  render () {
    const { children } = this.props
    const { opacity, animation } = this.state

    return (
      <Animated.View style={[componentStyles.childrenContent, { opacity: opacity, paddingLeft: animation }]}>
        {children}
      </Animated.View>
    )
  }
}

export class VerticalTab extends Component {
  constructor (props) {
    super(props)

    this.state = {
      selectedTab: 0
    }
  }

  _childrenWithoutFalse () {
    // { (評価式) && ...  }の形式で、条件を満たしていた場合のみ表示することができるように対応
    if (Array.isArray(this.props.children)) {
      const children = this.props.children.filter(e => !!e)
      if (children.length === 1) {
        // タブが1つのみの場合、Objectが渡されるため
        return children[0]
      } else {
        return children
      }
    } else {
      return this.props.children
    }
  }

  _children = (children = this._childrenWithoutFalse()) => React.Children.map(children, child => child)

  _onChangeTab = tabIndex => {
    this.setState({ selectedTab: tabIndex })
  }

  _renderTabBar = ({ normalColor = '#eee', activeColor = '#e1bd9f', tabBarWidth = DEFAULT_TAB_WIDTH }) => {
    const { selectedTab: tabActive } = this.state
    const tabs = this._children().map((tab, tabIndex) =>
      <TouchableOpacity
        key={tabIndex}
        onPress={index => this._onChangeTab(tabIndex)}
        style={[componentStyles.tabBarItem, { backgroundColor: tabActive === tabIndex ? activeColor : normalColor }]}
      >
        <Text style={componentStyles.tabBarItemContainer}>
          {tab.props.tabName}
        </Text>
      </TouchableOpacity>
    )

    return (
      <View style={[componentStyles.tabBarContainer, { width: tabBarWidth }]}>
        {tabs}
      </View>
    )
  }

  _renderChildContent = () => {
    const children = this._childrenWithoutFalse()
    const { selectedTab } = this.state

    if (React.Children.count(children) > 1) {
      return (
        <View key={selectedTab} style={componentStyles.childContainer}>
          {React.cloneElement(children[selectedTab], {})}
        </View>
      )
    } else {
      return (
        <View key={selectedTab} style={componentStyles.childContainer}>
          {React.cloneElement(children, {})}
        </View>
      )
    }
  }

  render () {
    const { normalColor, activeColor, tabBarWidth } = this.props

    return (
      <View style={[componentStyles.container, this.props.style]}>
        {this._renderTabBar({ normalColor, activeColor, tabBarWidth })}
        {this._renderChildContent()}
      </View>
    )
  }
}
