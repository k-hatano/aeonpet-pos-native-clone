'use strict'

import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ListView } from 'react-native'
import ProductSearchWidget from './ProductSearchWidget'
import componentStyles from '../../styles/widgets/ListItemsHandy'

export default class ListItemsHandy extends Component {
  constructor (props) {
    super(props)
    this.scrollView = null
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      total: 0,
      page: 0,
      perPage: 20,
      offset: 0,
      currentCategoryId: 0
    }
  }

  _chooseCategory (item) {
    const { chooseCategory } = this.props
    chooseCategory(item)
    this.setState({
      currentCategoryId: item.id
    })
  }

  _backToParentCategory () {
    const { breadcrumbs, backToParentCategory } = this.props
    let newBreadcumbs = breadcrumbs

    if (breadcrumbs.length > 1) {
      breadcrumbs.pop()
    }
    backToParentCategory(newBreadcumbs)
  }

  _backToTopCategory () {
    const { breadcrumbs, backToParentCategory } = this.props
    const newBreadcrumbs = breadcrumbs.shift()

    backToParentCategory([newBreadcrumbs])
  }

  renderBreadcumbs () {
    const { breadcrumbs } = this.props
    let parentCategory = breadcrumbs[breadcrumbs.length - 1]

    if (breadcrumbs.length === 1) {
      return (
        <View style={componentStyles.breadcrumbs}>
          <Text style={componentStyles.breadcrumbText}>{parentCategory.title}</Text>
        </View>
      )
    }

    if (breadcrumbs.length === 2) {
      return (
        <View style={componentStyles.breadcrumbs}>
          <TouchableOpacity style={componentStyles.breadcrumbButton} onPress={() => this._backToParentCategory()}>
            <Text style={componentStyles.breadcrumbText}>{`<<`}</Text>
          </TouchableOpacity>
          <Text style={componentStyles.breadcrumbText}>{parentCategory.title}</Text>
        </View>
      )
    }

    return (
      <View style={componentStyles.breadcrumbs}>
        <TouchableOpacity style={componentStyles.breadcrumbButton} onPress={() => this._backToTopCategory()}>
          <Text style={componentStyles.breadcrumbText}>{`<<`}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={componentStyles.breadcrumbButton} onPress={() => this._backToParentCategory()}>
          <Text style={componentStyles.breadcrumbText}>{`...<`}</Text>
        </TouchableOpacity>
        <Text style={componentStyles.breadcrumbText}>{parentCategory.title}</Text>
      </View>
    )
  }

  renderCategoryItem (item) {
    return (
      <View style={componentStyles.categoryItem}>
        <TouchableOpacity onPress={() => this._chooseCategory(item)} style={componentStyles.categoryItemTouch}>
          <Text>{item.name}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render () {
    const { isShowingCategory, isShowingSearchWidget, products, renderItem, renderSeparator } = this.props
    let productSource = this.dataSource.cloneWithRows(products.filter(item => item.type === 0))
    let categoriesSource = this.dataSource.cloneWithRows(products.filter(item => item.type === 1))

    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.productContainer}>
          {productSource &&
            <ListView
              renderHeader={() => isShowingSearchWidget && <ProductSearchWidget />}
              style={componentStyles.recordList}
              enableEmptySections
              dataSource={productSource}
              contentContainerStyle={componentStyles.subListContainer}
              renderRow={item => renderItem(item)}
              renderSeparator={(sectionId, rowId) => renderSeparator({ sectionId, rowId })}
              pageSize={50}
            />}
        </View>
        {isShowingCategory &&
          <View style={componentStyles.categoriesContainer}>
            {this.renderBreadcumbs()}
            <View style={componentStyles.categoriesView}>
              {categoriesSource &&
                <ListView
                  style={componentStyles.categoriesList}
                  enableEmptySections
                  dataSource={categoriesSource}
                  renderRow={item => this.renderCategoryItem(item)}
                  pageSize={50}
                />}
            </View>
          </View>}
      </View>
    )
  }
}
