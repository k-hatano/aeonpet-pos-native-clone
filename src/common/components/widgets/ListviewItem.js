'use strict'

import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import I18n from 'i18n-js'
import { formatPriceWithCurrency } from '../../utils/formats'
import componentStyles from '../../styles/widgets/ListviewItem'

export default function GridviewItem ({ item, chooseProduct, itemWidth }) {
  return (
    <TouchableOpacity key={item.id} underlayColor={'#bbb'} onPress={() => chooseProduct(item)}>
      <View style={[componentStyles.recordItem, { width: itemWidth }]}>
        <View style={componentStyles.recordItemLeft}>
          <Text style={componentStyles.recordItemTitleText}>
            {item.name}
          </Text>
        </View>
        <View style={componentStyles.recordItemInfo}>
          <Text style={componentStyles.recordItemStock}>
            {I18n.t('order.stock')}:{I18n.t('common.amount_of_point', { amount: 99 })}
          </Text>
          <Text style={componentStyles.recordItemPrice}>
            {`${formatPriceWithCurrency(item.price)}`}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}
