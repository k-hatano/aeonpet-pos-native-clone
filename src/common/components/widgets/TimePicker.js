'use strict'

import React, {Component} from 'react'
import { View, Picker } from 'react-native'
import PropTypes from 'prop-types'
import styles from '../../styles/widgets/TimePicker'

export default class TimePicker extends Component {
  static propTypes = {
    onChange: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {
      hour: '12',
      minute: '0'
    }
    this.hours = []
    this.minutes = []

    for (let i = 0; i < 24; i++) {
      this.hours.push(String(i))
    }
    for (let i = 0; i < 60; i += 1) {
      this.minutes.push(String(i))
    }
  }

  _updateHour = (hour) => {
    this.setState({hour: hour})
    let date = this.props.date
    date.setHours(hour)
    date.setMinutes(this.state.minute)
    this.props.onChange(date)
  }

  _updateMinute = (minute) => {
    this.setState({minute: minute})
    let date = this.props.date
    date.setMinutes(minute)
    this.props.onChange(this.props.date)
  }

  _hoursPickerItems () {
    return (
      this.hours.map(hour => {
        return <Picker.Item label={hour} value={hour} key={hour} />
      })
    )
  }

  _minutesPickerItems () {
    return (
      this.minutes.map(minute => {
        return <Picker.Item label={minute} value={minute} key={minute} />
      })
    )
  }

  render () {
    return (
      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.hour}
          onValueChange={(hour) => this._updateHour(hour)} >
          {this._hoursPickerItems(this.hours)}
        </Picker>
        <Picker
          style={styles.picker}
          selectedValue={this.state.minute}
          onValueChange={(minute) => this._updateMinute(minute)} >
          {this._minutesPickerItems(this.minutes)}
        </Picker>
      </View>
    )
  }
}
