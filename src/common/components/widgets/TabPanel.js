import React, { Component } from 'react'
import { Animated, ScrollView, View, TouchableOpacity, Text } from 'react-native'
import I18n from 'i18n-js'
import componentStyles from '../../styles/widgets/TabPanel'
import PropTypes from 'prop-types'
import CustomPropTypes from '../CustomPropTypes'

const ANIMATION_FROM_VALUE = 100
const ANIMATION_DURATION = 50
const ANIMATION_DURATION_OPACITY = 400

export default class TabPanel extends Component {
  static propTypes = {
    children: PropTypes.array,
    tabStyle: CustomPropTypes.Style,
    style: CustomPropTypes.Style,
    activeTextStyle: CustomPropTypes.Style,
    activeBackgroundStyle: CustomPropTypes.Style,
    tabWrapperStyle: CustomPropTypes.Style,
    tabContentWrapper: PropTypes.object,
    selectedTab: PropTypes.number,
    onTabClick: PropTypes.func,
    error: PropTypes.string,
    selectTab: PropTypes.number
  }

  constructor (props) {
    super(props)

    this.state = {
      selectedTab: 0,
      animation: new Animated.Value(0),
      opacity: new Animated.Value(1)
    }
  }

  _children = (children = this.props.children) => React.Children.map(children, child => child)

  _isTabActive = tabIndex => this.state.selectedTab === tabIndex

  componentWillReceiveProps (nextProps) {
    if (nextProps.selectedTab !== undefined && nextProps.selectedTab !== this.state.selectedTab) {
      this._runAnimation(nextProps.selectedTab)
    }
  }

  componentWillMount () {
    const { selectTab } = this.props
    if (!(selectTab == null || selectTab.length === 0)) {
      this.setState({ selectedTab: selectTab })
    }
  }

  _runAnimation (tabIndex) {
    if (this._isTabActive(tabIndex)) {
      return false
    }

    this.setState({ selectedTab: tabIndex })
    this.state.animation.setValue(ANIMATION_FROM_VALUE)
    this.state.opacity.setValue(0)

    Animated.parallel([
      Animated.spring(this.state.animation, {
        toValue: 0,
        duration: ANIMATION_DURATION
      }),
      Animated.timing(this.state.opacity, {
        toValue: 1,
        duration: ANIMATION_DURATION_OPACITY
      })
    ]).start()
  }

  _renderTabBars () {
    const {
      tabStyle,
      activeTextStyle,
      activeBackgroundStyle,
      tabWrapperStyle = componentStyles.tabWrapper
    } = this.props
    const tabs = this._children()
    const anyTabHasLabel = tabs.find(tab => tab.props.tabName)

    if (tabs.length <= 1 || !anyTabHasLabel) {
      return false
    }
    return (
      <View style={tabWrapperStyle}>
        <ScrollView horizontal>
          {tabs.map((tabBar, tabIndex) => {
            if (!tabBar.props.tabName) {
              return false
            }
            let activeBackgroundColor = {}
            let activeTextColor = {}
            let activeErrorWrapperStyle = {}
            if (this._isTabActive(tabIndex)) {
              activeBackgroundColor = activeBackgroundStyle || componentStyles.defaultActiveBackgroundStyle || {}
              activeTextColor =
                tabBar.props.activeTextStyle || activeTextStyle || componentStyles.defaultActiveTextStyle || {}
              activeErrorWrapperStyle = tabBar.props.activeErrorWrapperStyle || {}
            }
            return (
              <View key={tabIndex} style={[componentStyles.defaultTabStyle, tabStyle]}>
                <TouchableOpacity
                  style={[componentStyles.tabHeader, tabBar.props.tabStyle, activeBackgroundColor]}
                  onPress={() => {
                    this._runAnimation(tabIndex)
                    tabBar.props.onTabClick && tabBar.props.onTabClick(tabIndex)
                  }}
                >
                  <Text style={[componentStyles.tabName, tabBar.props.tabNameStyle, activeTextColor]}>
                    {tabBar.props.tabName}
                  </Text>
                  {tabBar.props.error &&
                    <View
                      style={[
                        componentStyles.defaultErrorWrapperStyle,
                        tabBar.props.errorWrapperStyle,
                        activeErrorWrapperStyle
                      ]}
                    >
                      <Text style={[componentStyles.defaultErrorTextStyle, tabBar.props.errorTextStyle]}>
                        {tabBar.props.error}
                      </Text>
                    </View>}
                </TouchableOpacity>
              </View>
            )
          })}
        </ScrollView>
      </View>
    )
  }

  _renderTabContent () {
    const { selectedTab } = this.state
    const { tabContentWrapper = componentStyles.tabContentWrapper } = this.props
    const currentTab = this._children()[selectedTab]

    return (
      <View style={tabContentWrapper}>
        <Animated.View
          style={[
            componentStyles.tabContent,
            { marginLeft: this.state.animation, opacity: this.state.opacity },
            currentTab.props.contentStyle
          ]}
        >
          {currentTab}
        </Animated.View>
      </View>
    )
  }

  render () {
    const tabs = this._children()
    if (!tabs || tabs.length === 0) {
      return (
        <View style={[componentStyles.container, this.props.style]}>
          <Text>{I18n.t('common.no_data_message')}</Text>
        </View>
      )
    }

    return (
      <View style={[componentStyles.container, this.props.style]}>
        {this._renderTabBars()}
        {this._renderTabContent()}
      </View>
    )
  }
}
