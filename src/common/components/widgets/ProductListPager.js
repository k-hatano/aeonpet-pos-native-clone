'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import ListItemsHandy from './ListItemsHandy'
import componentStyles from '../../styles/widgets/ProductSearch'

const ITEM_WIDTH = 225
const ITEM_HEIGHT = 100
export default class ProductListPager extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isPrepared: true,
      width: 0,
      height: 0,
      itemWidth: ITEM_WIDTH,
      itemHeight: ITEM_HEIGHT,
      isReadyToShow: false
    }
  }

  componentWillReceiveProps (nextProps) {
    const { isListView: isListViewNew } = nextProps
    const { isListView } = this.props
    const { width: currentWidth } = this.state

    if (isListView !== isListViewNew) {
      if (isListViewNew) {
        const itemWidth = currentWidth
        this.setState({
          itemWidth,
          isReadyToShow: currentWidth >= ITEM_WIDTH
        })
      } else {
        let itemWidth = ITEM_WIDTH
        const width = currentWidth
        switch (true) {
          case currentWidth < ITEM_WIDTH * 3 && currentWidth > ITEM_WIDTH * 2:
            itemWidth = ~~(currentWidth / 2 - 5)
            break
          case currentWidth < ITEM_WIDTH * 2:
            itemWidth = currentWidth - 5
            break
          case currentWidth < ITEM_WIDTH * 4 && currentWidth > ITEM_WIDTH * 3:
            itemWidth = ~~(currentWidth / 3 - 5)
            break
        }

        this.setState({
          itemWidth,
          isReadyToShow: width >= ITEM_WIDTH
        })
      }
    }
  }

  renderItem (item) {
    const { chooseProduct, isListView, gridviewItem, listviewItem } = this.props
    const { itemWidth } = this.state

    if (isListView) {
      return listviewItem({ item, chooseProduct, itemWidth })
    }

    return gridviewItem({ item, chooseProduct, itemWidth })
  }

  renderSeparator ({ sectionId, rowId }) {
    const { isListView } = this.props

    if (isListView) {
      return <View key={`${sectionId}-${rowId}`} style={componentStyles.listViewSeperator} />
    }
  }

  _onLayoutChange (event) {
    const { isListView, isShowingCategory } = this.props
    let { width, height } = event.nativeEvent.layout
    const { height: currentHeight, width: currentWidth } = this.state

    if (isListView) {
      if (width !== currentWidth || height !== currentHeight) {
        const itemWidth = isShowingCategory ? ~~(width * 3 / 4 - 6) : width
        this.setState({
          width: itemWidth,
          height,
          itemWidth,
          isReadyToShow: width >= ITEM_WIDTH
        })
      }
      return true
    }

    if (width !== currentWidth || height !== currentHeight) {
      let itemWidth = ITEM_WIDTH
      width = isShowingCategory ? ~~(width * 3 / 4 - 6) : width

      switch (true) {
        case width < ITEM_WIDTH * 3 && width > ITEM_WIDTH * 2:
          itemWidth = ~~(width / 2 - 5)
          break
        case width < ITEM_WIDTH * 2:
          itemWidth = width - 5
          break
        case width < ITEM_WIDTH * 4 && width > ITEM_WIDTH * 3:
          itemWidth = ~~(width / 3 - 5)
          break
      }

      this.setState({
        width,
        height,
        itemWidth,
        isReadyToShow: width >= ITEM_WIDTH
      })
    }
  }

  renderList () {
    const { width, height, isPrepared, itemWidth, itemHeight, isReadyToShow } = this.state
    const {
      isShowingCategory,
      isShowingSearchWidget,
      products,
      categories,
      breadcrumbs,
      chooseProduct,
      backToParentCategory
    } = this.props

    return (
      <ListItemsHandy
        categories={categories}
        products={products}
        breadcrumbs={breadcrumbs}
        isPrepared={isPrepared}
        renderItem={item => this.renderItem(item)}
        isReadyToShow={isReadyToShow}
        itemWidth={itemWidth}
        itemHeight={itemHeight}
        width={width}
        height={height}
        chooseCategory={item => chooseProduct(item)}
        backToParentCategory={item => backToParentCategory(item)}
        renderSeparator={({ sectionId, rowId }) => this.renderSeparator({ sectionId, rowId })}
        isShowingCategory={isShowingCategory}
        isShowingSearchWidget={isShowingSearchWidget}
      />
    )
  }

  render () {
    const { styles } = this.props
    const { isPrepared } = this.state

    return (
      <View style={[componentStyles.productListPagerContainer, styles]} onLayout={event => this._onLayoutChange(event)}>
        <View style={componentStyles.productListPagerContainerContent}>
          {isPrepared && this.renderList()}
        </View>
      </View>
    )
  }
}
