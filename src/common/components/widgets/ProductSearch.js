'use strict'

import React, { Component } from 'react'
import { Row, Grid } from 'react-native-easy-grid'
import GridviewItem from './GridviewItem'
import ListviewItem from './ListviewItem'
import ProductListPager from './ProductListPager'
import componentStyles from '../../styles/widgets/ProductSearch'

export default class ProductSearch extends Component {
  render () {
    const {
      isListView,
      chooseProduct,
      products,
      categories,
      chooseBreadcrumbs,
      breadcrumbs,
      backToParentCategory
    } = this.props

    return (
      <Grid style={componentStyles.container}>
        <Row>
          <ProductListPager
            chooseProduct={item => chooseProduct(item)}
            chooseBreadcrumbs={item => chooseBreadcrumbs(item)}
            backToParentCategory={item => backToParentCategory(item)}
            isListView={isListView}
            products={products}
            categories={categories}
            breadcrumbs={breadcrumbs}
            gridviewItem={({ item, chooseProduct, itemWidth }) =>
              <GridviewItem item={item} chooseProduct={chooseProduct} itemWidth={itemWidth} />}
            listviewItem={({ item, chooseProduct, itemWidth }) =>
              <ListviewItem item={item} chooseProduct={chooseProduct} itemWidth={itemWidth} />}
          />
        </Row>
      </Grid>
    )
  }
}
