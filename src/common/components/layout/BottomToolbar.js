'use strict'

import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet, LayoutAnimation } from 'react-native'
import IconButton from '../elements/OldIconButton'
import styles from '../../styles/layout/Page'

export default class BottomToolbar extends Component {
  constructor (props) {
    super(props)

    const { fixed } = props
    this.state = {
      show: fixed ? true : props.isShow,
      height: fixed ? 60 : 20
    }
  }

  _onShowToolbar (show) {
    const { fixed } = this.props

    if (fixed) {
      return false
    }

    const height = !show ? 60 : 20

    this.setState({
      show: !show,
      height
    })

    LayoutAnimation.easeInEaseOut()
  }

  render () {
    const { show, height } = this.state
    const { bottomButtons, settingButtons, backgroundColor, backgroundShowButton, containerBackground } = this.props
    const customTooltipTop = settingButtons ? { top: settingButtons.length * DEFAULT_TOP } : {}
    const background = backgroundColor || '#ccc'
    const wrapperBackgroundColor = show ? background : containerBackground || 'transparent'

    return (
      <View style={[styles.bottomToolbarContainer]}>
        <View
          onStartShouldSetResponder={() => true}
          onResponderRelease={() => this._onShowToolbar(show)}
          style={[
            styles.bottomToolbar,
            {
              height: height,
              width: '100%',
              backgroundColor: wrapperBackgroundColor
            }
          ]}
        >
          <View style={styles.containerButtonToolbar}>
            {show &&
              bottomButtons &&
              bottomButtons.map((button, id) =>
                <IconButton
                  key={id}
                  icon={button.icon}
                  type={button.type}
                  text={button.text}
                  iconSize={button.iconSize}
                  containerStyle={button.containerStyle}
                  onPress={button.onPress}
                />
              )}
          </View>
          <TouchableOpacity
            onPress={() => this._onShowToolbar(show)}
            style={[
              styles.btnShowToolbar,
              { backgroundColor: backgroundShowButton || '#ccc', paddingLeft: 10, paddingRight: 15 }
            ]}
          >
            <Text> . . . </Text>
          </TouchableOpacity>
          {show &&
            settingButtons &&
            <View style={[componentStyles.tooltipButtonGroup, customTooltipTop]}>
              {settingButtons.map((button, id) =>
                <View key={id}>
                  <TouchableOpacity style={button.style} onPress={button.onPress}>
                    <Text> {button.title} </Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>}
        </View>
      </View>
    )
  }
}

const DEFAULT_TOP = -45
const componentStyles = StyleSheet.create({
  tooltipButtonGroup: {
    position: 'absolute',
    right: 1,
    minHeight: 50,
    top: DEFAULT_TOP,
    width: 200,
    paddingLeft: 10
  }
})
