'use strict'

import React, { Component } from 'react'
import { Dimensions, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import I18n from 'i18n-js'
import { Actions } from 'react-native-router-flux'
import IconButton from '../elements/OldIconButton'
import ImageButton from '../elements/ImageButton'
import componentStyles from '../../styles/layout/OldHeader'
import Modal from '../widgets/Modal'
import StaffSettings from '../../../common/settings/StaffSettings'

const screen = Dimensions.get('window')
const VERSON_POPUP_WIDTH = 300
const VERSON_POPUP_HEIGHT = 200

export default class Header extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isToggleMoreButton: false,
      isOpenVersonPopup: false,
      selectedStaff: StaffSettings.selectedStaff
    }
  }

  _onItemChoose (staff) {
    this.setState({selectedStaff: staff})
  }

  _openSelectStaffModal () {
  }

  _onToggleMoreButton = () => {
    const { isToggleMoreButton } = this.state
    this.setState({ isToggleMoreButton: !isToggleMoreButton })
  }

  _onVersionClick = () => {
    const { isOpenVersonPopup } = this.state

    this.setState({
      isOpenVersonPopup: !isOpenVersonPopup,
      isToggleMoreButton: false
    })
  }

  _onOpenDrawer = () => {}

  render () {
    const { isToggleMoreButton, isOpenVersonPopup } = this.state

    return (
      <View style={componentStyles.container}>
        <View style={componentStyles.rightContainer}>
          <View style={componentStyles.buttonContainer}>
            <ImageButton
              style={componentStyles.imageIconHome}
              toggle
              appearance={{
                normal: require('../../../assets/images/header/home.png'),
                highlight: require('../../../assets/images/header/home.png')
              }}
              onPress={Actions.home}
            />
          </View>
        </View>
        <View style={componentStyles.headerLogoWrapper}>
          <Text style={componentStyles.headerLogoTitle}>Orange Pos</Text>
        </View>
        <View style={componentStyles.leftContainer}>
          <View style={componentStyles.buttonContainer}>
            <View style={componentStyles.staffName}>
              <Text style={componentStyles.textName}> {I18n.t('common.staff')}: </Text>
              <Text style={componentStyles.textName}>{this.state.selectedStaff ? this.state.selectedStaff.name : ''}</Text>
            </View>
            <TouchableOpacity style={componentStyles.normalButton} onPress={() => this._openSelectStaffModal()}>
              <Text style={componentStyles.textButton}>{I18n.t('common.change_staff')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={componentStyles.normalButton} onPress={() => console.log('')}>
              <Text style={componentStyles.textButton}>{I18n.t('common.preserve_order')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={componentStyles.normalButton} onPress={() => console.log('')}>
              <Text style={componentStyles.textButton}>{I18n.t('common.print_errors')}</Text>
            </TouchableOpacity>
            <View style={componentStyles.headerButtonSeparator} />
            <IconButton
              icon='menu'
              type='material'
              color='#fff'
              iconSize={36}
              containerStyle={[componentStyles.normalButton, { paddingLeft: 5, paddingRight: 5 }]}
              onPress={() => this._onToggleMoreButton()}
            />
          </View>
        </View>
        {isToggleMoreButton &&
          <View style={componentStyles.popupMoreTooltip}>
            <TouchableOpacity style={componentStyles.buttonInPopupMore} onPress={() => this._onOpenDrawer()}>
              <Text>{I18n.t('home.open_drawer')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={componentStyles.buttonInPopupMore} onPress={() => this._onVersionClick()}>
              <Text>{I18n.t('home.version')}</Text>
            </TouchableOpacity>
          </View>}
        {isOpenVersonPopup &&
          <View
            style={[
              componentStyles.absolute,
              {
                width: screen.width,
                height: screen.height
              }
            ]}
          >
            <TouchableWithoutFeedback
              style={[componentStyles.absolute, { width: screen.width, height: screen.height }]}
              onPress={() => this._onVersionClick()}
            >
              <View
                style={[
                  componentStyles.absolute,
                  componentStyles.backDrop,
                  { width: screen.width, height: screen.height }
                ]}
              />
            </TouchableWithoutFeedback>
            <View
              style={[
                componentStyles.popupVersionContainer,
                {
                  width: VERSON_POPUP_WIDTH,
                  height: VERSON_POPUP_HEIGHT,
                  left: (screen.width - VERSON_POPUP_WIDTH) / 2,
                  top: (screen.height - VERSON_POPUP_HEIGHT) / 2
                }
              ]}
            >
              <Text>Orange POS 1.0</Text>
            </View>
          </View>}
      </View>
    )
  }
}
