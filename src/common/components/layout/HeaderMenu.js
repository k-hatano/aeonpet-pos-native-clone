import React from 'react'
import { Text, View, ListView } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../../styles/layout/HeaderMenu'
import Modal from '../widgets/Modal'
import HeaderMenuItemButton from '../../containers/layout/HeaderMenuItemButton'

class HeaderMenuItem extends React.Component {
  static propTypes = {
    menuItem: PropTypes.object,
    onPress: PropTypes.func,
    permissionCode: PropTypes.string
  }

  render () {
    return (
      <HeaderMenuItemButton
        onPress={() => this.props.onPress()}
        label={this.props.menuItem.label}
        permissionCode={this.props.permissionCode}/>
    )
  }
}

export default class HeaderMenu extends React.Component {
  static propTypes = {
    menus: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
  }

  render () {
    const dropListHeight = this.props.menus.length * 64
    return (
      <View style={[styles.container, {height: dropListHeight}]}>
        <ListView
          dataSource={this.dataSource.cloneWithRows(this.props.menus)}
          renderRow={(item) => (
            <HeaderMenuItem 
              menuItem={item}
              onPress={() => item.onPress()}
              permissionCode={item.permissionCode}/>
          )}
          renderSeparator={(sectionId, rowId) => (<View key={rowId} style={styles.separator} />)} />
      </View>
    )
  }

  static openModalBy (menuItems, ref, onPress) {
    class ModalContent extends React.Component {
      render () {
        return (
          <HeaderMenu menus={menuItems} />
        )
      }
    }

    Modal.openBy(ModalContent, ref, {
      direction: Modal.DIRECTION.BOTTOM,
      adjust: Modal.ADJUST.RIGHT,
      isBackgroundVisible: false
    })
  }
}
