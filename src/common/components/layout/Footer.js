'use strict'

import React, { Component } from 'react'
import { View } from 'react-native'
import IconButton from '../elements/OldIconButton'
import componentStyles from '../../styles/layout/Footer'

export default class Footer extends Component {
  render () {
    const { leftButtons, rightButtons } = this.props

    return (
      <View style={componentStyles.backgroundContainer}>
        <View style={componentStyles.leftChilds}>
          {leftButtons &&
            leftButtons.map((button, id) =>
              <IconButton
                key={id}
                icon={button.icon}
                type={button.type}
                color={button.color}
                text={button.text}
                iconSize={24}
                textStyle={{ color: button.color, fontSize: 10, marginTop: -5 }}
                containerStyle={{ backgroundColor: 'transparent', marginLeft: 10 }}
                onPress={button.onPress}
              />
            )}
        </View>
        <View style={componentStyles.rightChilds}>
          {rightButtons &&
            rightButtons.map((button, id) =>
              <IconButton
                key={id}
                icon={button.icon}
                type={button.type}
                color={button.color}
                text={button.text}
                iconSize={24}
                textStyle={{ color: button.color, fontSize: 10, marginTop: -5 }}
                containerStyle={{ backgroundColor: 'transparent', marginLeft: 10 }}
                onPress={button.onPress}
              />
            )}
        </View>
      </View>
    )
  }
}
