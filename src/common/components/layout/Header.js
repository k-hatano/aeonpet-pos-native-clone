import React, { Component } from 'react'
import { Text, Image, View, Clipboard } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../../styles/layout/Header'
import { Actions } from 'react-native-router-flux'
import SimpleImageButton from '../elements/SimpleImageButton'
import HeaderMenu from './HeaderMenu'
import Modal from 'common/components/widgets/Modal'
import ModalFrame from 'common/components/widgets/ModalFrame'
import { showSelectStaffViewModal } from '../../../modules/staff/containers/SelectStaffViewModal'
import { OptionalCommandButton } from '../elements/StandardButton'
import DeviceInfo from 'react-native-device-info'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import PendedOrderModalView from 'modules/order/components/PendedOrderModalView'
import { PERMISSION_CODES } from 'modules/staff/models'
import { waitAsync } from '../../utils/waitAsync'
import { isDevMode } from '../../utils/environment'
import { showDebugBarcodeInput } from '../../../modules/barcodeReader/services'

export default class Header extends React.Component {
  static propTypes = {
    currentStaff: PropTypes.object,
    hideBackButton: PropTypes.bool,
    beforeGoHome: PropTypes.func,
    showPrinterErrorButton: PropTypes.bool,
    showPendedOrderButton: PropTypes.bool,
    showForcedCloseSalesButton: PropTypes.bool,
    onForcedCloseSales: PropTypes.func,
    printerErrorsCount: PropTypes.number,
    pageName: PropTypes.string,
    getDebugMessage: PropTypes.func,
    permissionCode: PropTypes.string,
    showHeaderMenuSetting: PropTypes.bool,
    onSelectSetting: PropTypes.func,
    repositoryContext: PropTypes.string,
    pendedOrders: PropTypes.array,
    restorePendedOrderToCart: PropTypes.func,
    showIncrementalDataSync: PropTypes.bool,
    onSelectDataSync: PropTypes.func,
    showReprintCloseSales: PropTypes.bool,
    onReprintCloseSales: PropTypes.func,
    onOpenDebugBarcodeInput: PropTypes.func,
    usesCautionColor: PropTypes.bool
  }

  constructor (props) {
    super(props)
    if (this.props.showReprintCloseSales) {
      this._menuItems.splice(1, 0, this._reprintCloseSalesButton)
    }
    if (this.props.showForcedCloseSalesButton) {
      this._menuItems.splice(1, 0, this._forcedCloseSalesButton)
    }
    if (this.props.showHeaderMenuSetting) {
      this._menuItems.push(this._headerMenuSettingButton)
    }
    if (this.props.showIncrementalDataSync) {
      this._menuItems.push(this._incrementalDataSyncButton)
    }
  }

  _menuItems = [
    {
      label: I18n.t('home.open_drawer'),
      onPress: async () => {
        Modal.close()
        this.props.onOpenDrawer()
      },
      permissionCode: PERMISSION_CODES.OPEN_DRAWER
    },
    {
      label: I18n.t('home.version'),
      onPress: () => {
        Modal.close()
        this._openVersionModal()
      },
      permissionCode: null
    }
  ]

  _forcedCloseSalesButton = {
    label: I18n.t('home.forced_close_sales'),
    onPress: () => {
      Modal.close()
      this.props.onForcedCloseSales()
    },
    permissionCode: PERMISSION_CODES.FORCE_CLOSE_SALES
  }

  _reprintCloseSalesButton = {
    label: I18n.t('home.reprint_close_sales'),
    onPress: async () => {
      Modal.close()
      await waitAsync(100)
      this.props.onReprintCloseSales()
    },
    permissionCode: PERMISSION_CODES.CLOSE_SALES
  }

  _headerMenuSettingButton = {
    label: I18n.t('home.settings'),
    onPress: () => {
      Modal.close()
      this.props.onSelectSetting()
    },
    permissionCode: PERMISSION_CODES.SETTING
  }

  _incrementalDataSyncButton = {
    label: I18n.t('home.incremental_sync'),
    onPress: () => {
      Modal.close()
      this.props.onSelectDataSync()
    },
    permissionCode: PERMISSION_CODES.SYNC_DATA
  }

  _openVersionModal () {
    Modal.open(ModalContent, {isBackgroundVisible: true})
  }

  _renderPendedOrdersButton () {
    if (this.props.pendedOrders && this.props.pendedOrders.length > 0) {
      return (
        <SimpleImageButton
          ref='pendedButton'
          style={styles.iconImagePendButton}
          imageStyle={styles.pendIcon}
          source={require('../../../assets/images/header/pend.png')}
          onPress={() => PendedOrderModalView.openModalBy(
            this.props.pendedOrders,
            this.refs.pendedButton,
            this.props.restorePendedOrderToCart,
            this.props.beforeGoHome)} />
      )
    } else {
      return null
    }
  }

  _renderPendedOrdersCount () {
    if (this.props.pendedOrders && this.props.pendedOrders.length > 0) {
      return (
        <View style={styles.countWrapper}>
          <View
            style={styles.defaultCountWrapperStyle}>
            <Text style={styles.defaultCountTextStyle}>
              {this.props.pendedOrders.length}
            </Text>
          </View>
        </View>
      )
    } else {
      return null
    }
  }

  _showStaffModal () {
    const { onChangeStaffModalShowing } = this.props
    const permissionCode = this.props.permissionCode ? this.props.permissionCode : null
    if (onChangeStaffModalShowing) {
      onChangeStaffModalShowing(false)
    }
    showSelectStaffViewModal(permissionCode, (staff) => {
      if (onChangeStaffModalShowing) {
        onChangeStaffModalShowing(true)
      }
    })
  }

  render () {
    const printerErrorsCount = this.props.printerErrorsCount
    const isTrainingMode = this.props.repositoryContext === REPOSITORY_CONTEXT.TRAINING
    const menuItems = (!this.props.showOpenDrawer || this.props.isStockManagementMode)
      ? this._menuItems.filter(menu => menu.label !== I18n.t('home.open_drawer')) : this._menuItems
    const showPendedOrderButton = !!this.props.showPendedOrderButton && !this.props.isStockManagementMode
    let styleForMode = null
    if (this.props.usesCautionColor) { styleForMode = styles.layoutRootForCaution }
    if (isTrainingMode) { styleForMode = styles.layoutRootForTraining }
    return (
      <View style={[styles.layoutRoot, styleForMode]}>
        <View style={styles.leftArea}>
          {this.props.canBack ? (
            <SimpleImageButton
              style={styles.iconImageButton}
              imageStyle={styles.homeIcon}
              source={require('../../../assets/images/header/chevron_left.png')}
              onPress={() => {
                Actions.pop()
                if (this.props.onBack != null) this.props.onBack()
              }} />
          ) : null}
          {this.props.hideBackButton ? null : (
            <SimpleImageButton
              style={styles.iconImageButton}
              imageStyle={styles.homeIcon}
              source={require('../../../assets/images/header/home.png')}
              onPress={async () => {
                if (!this.props.beforeGoHome || await this.props.beforeGoHome()) {
                  Actions.redirectPage({redirectTo: 'home'})
                }
              }} />
          )}
        </View>
        <View style={styles.center}>
          <Text style={styles.appName}>
            {isTrainingMode ? I18n.t('page.training_mode') : this.props.pageName ? this.props.pageName : 'Orange Operation'}
          </Text>
        </View>
        {!isDevMode() ? null : (
          <OptionalCommandButton
            text={'Debug'}
            style={{width: 80, height: 40, fontSize: 24}}
            onPress={() => this.props.onOpenDebugBarcodeInput()} />
        )}
        <View style={styles.rightArea}>
          {!this.props.getDebugMessage ? null : (
            <OptionalCommandButton
              text={'Debug'}
              style={{width: 60, height: 36}}
              onPress={() => Clipboard.setString(this.props.getDebugMessage())} />
          )}
          {(this.props.showPrinterErrorButton && printerErrorsCount > 0) ? (<SimpleImageButton
            style={styles.iconImagePrinterButton}
            imageStyle={styles.printerIcon}
            source={require('../../../assets/images/header/printer.png')}
            onPress={() => Actions.printerErrorPage()} />) : null
          }
          {(this.props.showPrinterErrorButton && printerErrorsCount > 0) ? (
            <Image
              style={styles.exclamationIcon}
              source={require('../../../assets/images/header/exclamation_point.png')} />) : null
          }
          {showPendedOrderButton && this._renderPendedOrdersButton()}
          {showPendedOrderButton && this._renderPendedOrdersCount()}
          <SimpleImageButton
            style={styles.iconImageButton}
            imageStyle={styles.userIcon}
            source={require('../../../assets/images/header/user.png')}
            onPress={() => this._showStaffModal()} />
          <Text style={styles.text}>
            {this.props.currentStaff ? this.props.currentStaff.name : ''}
          </Text>
          <View style={styles.separator} />
          <SimpleImageButton
            ref='menuButton'
            style={styles.iconImageButton}
            imageStyle={styles.menuIcon}
            source={require('../../../assets/images/header/menu.png')}
            onPress={() => HeaderMenu.openModalBy(menuItems, this.refs.menuButton)} />
        </View>
      </View>
    )
  }
}

class ModalContent extends Component {
  render () {
    return (
      <ModalFrame style={styles.VersionModal} title='' onClose={() => Modal.close()} >
        <View style={styles.VersionModalDeviceNameArea}>
          <Text style={styles.VersionModalDeviceNameText}>OrangeOperation</Text>
        </View>
        <View style={styles.VersionModalVersionArea}>
          <Text style={styles.VersionModalVersionText}>{I18n.t('common.version')}：{DeviceInfo.getVersion()}</Text>
        </View>
        <View style={styles.VersionModalCopyRightArea}>
          <Text style={styles.VersionModalCopyRightText}>Copyright 2017 S-cubism inc. All rights reserved.</Text>
        </View>
      </ModalFrame>
    )
  }
}
