import React from 'react'
import { Text, View } from 'react-native'
import I18n from 'i18n-js'
import PropTypes from 'prop-types'
import styles from '../../styles/layout/HeaderMenu'
import baseStyle from 'common/styles/baseStyle'
import TouchableOpacitySfx from '../elements/TouchableOpacitySfx'
import CustomPropTypes from 'common/components/CustomPropTypes'
import { REPOSITORY_CONTEXT } from 'common/AppEvents'
import ImageButton from 'common/components/elements/ImageButton'

export default class HomeMainButton extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    onPress: PropTypes.func,
    repositoryContext: PropTypes.string
  }

  render () {
    const { label } = this.props 
    return (
      <TouchableOpacitySfx onPress={() => this.props.onPress()}>
        <View style={styles.itemRoot}>
          <Text style={baseStyle.commonText}>
            {label}
          </Text>
        </View>
      </TouchableOpacitySfx>
    )
  }
}
