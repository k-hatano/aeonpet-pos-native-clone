import React, {Component} from "react"
import { Keyboard, Animated } from 'react-native'

/**
 * 使い方:
 * - 上方へずらしたいコンポーネントの下にこのコンポーネント (<KeyboardSpace />) を設置する
 * - ソフトウェアキーボードを表示した際にキーボードの高さの分、コンポーネントが上方へ移動する
 * - ソフトウェアキーボードが非表示になった際にコンポーネントが元の場所に戻る
 */
export default class KeyboardSpace extends Component {

  constructor(props) {
    super(props)
    this.state = {
      height: new Animated.Value(0)
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardWillShow', (e) => this._keyboardWillShow(e) )
    this.keyboardDidHideListener = Keyboard.addListener('keyboardWillHide', (e) => this._keyboardWillHide(e) )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _keyboardWillShow(e) {
    const height = e.endCoordinates ? e.endCoordinates.height : e.end.height
    Animated.timing(this.state.height, {
      toValue: this.props.toHalf ? height / 2 : height,
      duration: e.duration
    }).start()
  }

  _keyboardWillHide(e) {
    Animated.timing(this.state.height, {
      toValue: 0,
      duration: e.duration
    }).start()
  }

  render() {
    return (
      <Animated.View style={{height: this.state.height}}></Animated.View>
    )
  }
}