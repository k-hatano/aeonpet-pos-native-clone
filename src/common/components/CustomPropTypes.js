import PropTypes from 'prop-types'

export default {
  Style: PropTypes.oneOfType(
    [
      PropTypes.number,
      PropTypes.object,
      PropTypes.array
    ]
  )
}
