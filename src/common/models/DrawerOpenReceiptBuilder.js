import ReceiptBuilder from 'modules/printer/models/ReceiptBuilder'
import I18n from 'i18n-js'
import { makeBaseEJournal, RECEIPT_TYPE } from 'modules/printer/models'
import { loading } from 'common/sideEffects'
import EJournalRepository from 'modules/printer/repositories/EJournalRepository'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'
import SettingKeys from 'modules/setting/models/SettingKeys'

export default class DrawerOpenReceiptBuilder extends ReceiptBuilder {
  buildDrawerOpenReceipt (createdAt) {
    return {
      content: [
        {
          element: 'text',
          align: 'left',
          text: I18n.t('receipt.title.open_drawer')
        },
        this.cashierNameElement(),
        {
          element: 'text',
          align: 'left',
          text: this.formatDateTime(createdAt)
        },
        this.staffElement(),
        this.cutElement()
      ]
    }
  }
}

const makeDrawerOpenEJournal = (receipt, createdAt) => {
  const title = I18n.t('receipt.title.open_drawer')
  const type = RECEIPT_TYPE.OPEN_DRAWER
  const currentStaff = new StoreAccessibleBase().getSetting(SettingKeys.STAFF.CURRENT_STAFF)
  const ejournal = makeBaseEJournal(title, receipt, type, createdAt)
  ejournal.amount = 0
  ejournal.is_print_tax_stamp = false
  ejournal.staff_id = currentStaff.id
  return ejournal
}

export const saveDrawerOpenEJournal = async (dispatch, receipt) => {
  const ejournal = makeDrawerOpenEJournal(receipt, Math.floor(new Date().getTime() / 1000))
  await loading(dispatch, async () => {
    await EJournalRepository.save(ejournal)
    await EJournalRepository.saveIsPrintedById(ejournal.id)
    await EJournalRepository.pushById(ejournal.id)
  })
}
