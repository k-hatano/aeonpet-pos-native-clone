import Validator from 'validator'

export const isEmpty = value => {
  return (
    Validator.isEmpty(value + '') ||
    (value.constructor === Object ? Object.keys(value).length === 0 : false)
  )
}

export const isNullOrEmpty = value => {
  return isNull(value) || isEmpty(value)
}

export const isLength = (value, min, max) => {
  return (
    !isNull(value) &&
    Validator.isLength(value + '', { min: min + '', max: max + '' })
  )
}

export const isHalfWidth = (value, trim = false) => {
  return (
    !isNull(value) &&
    Validator.isHalfWidth(trim ? value.trim() + '' : value + '') &&
    !Validator.isFullWidth(trim ? value.trim() + '' : value + '')
  )
}

export const isHalfWidthNumeric = (value, trim = false) => {
  return (
    !isNull(value) &&
    Validator.isInt(trim ? value.trim() : value) &&
    Validator.isHalfWidth(trim ? value.trim() + '' : value + '')
  )
}

export const isHalfWidthAlpha = (value, trim = false) => {
  return (
    !isNull(value) &&
    Validator.isAlpha(trim ? value.trim() : value) &&
    Validator.isHalfWidth(trim ? value.trim() + '' : value + '') &&
    !Validator.isFullWidth(trim ? value.trim() + '' : value + '')
  )
}

export const isHalfWidthAlphaNumeric = (value, trim = false) => {
  return (
    !isNull(value) &&
    Validator.isAlphanumeric(trim ? value.trim() : value) &&
    Validator.isHalfWidth(trim ? value.trim() + '' : value + '') &&
    !Validator.isFullWidth(trim ? value.trim() + '' : value + '')
  )
}

export const isBetween = (value, min, max) => {
  return (
    !isNull(value) &&
    Validator.isInt(value + '', { min: min + '', max: max + '' })
  )
}

export const isAlpha = (value, trim = false) => {
  return (
    !isNull(value) && Validator.isAlpha(trim ? value.trim() + '' : value + '')
  )
}

export const isNull = value => {
  return value === null
}

export const isHalfChar =  char => {
  return (char >= 0x0 && char < 0x81) ||
    (char === 0xa0) ||
    (char >= 0xa1 && char < 0xdf) ||
    (char >= 0xfd && char < 0xff)
}

export function isSurrogatePair(char, nextChar) {
  return (char >= 0xD800 && char <= 0xDBFF) &&
    (typeof nextChar === 'number') && (nextChar >= 0xDC00 && nextChar <= 0xDFFF);
}