import Decimal from 'decimal.js'
import { ceil, floor, round } from './MoneyAmount'

export function currencyToDecimalPlace (currency) {
  switch (currency.toLowerCase()) {
    case 'jpy':
      return 0

    case 'usd':
      return 2

    default:
      // TODO Error handling
      return 0
  }
}

/**
 * @param {Decimal} amount
 * @param {string} currency
 */
export function ceilByCurrency (amount, currency) {
  return ceil(amount, currencyToDecimalPlace(currency))
}

/**
 * @param {Decimal} amount
 * @param {string} currency
 */
export function floorByCurrency (amount, currency) {
  return floor(amount, currencyToDecimalPlace(currency))
}

/**
 * @param {Decimal} amount
 * @param {string} currency
 */
export function roundByCurrency (amount, currency) {
  return round(amount, currencyToDecimalPlace(currency))
}
