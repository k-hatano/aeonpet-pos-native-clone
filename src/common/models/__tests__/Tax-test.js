import Tax, {TAX_RULE} from '../Tax'
import Decimal from 'decimal.js'

describe('Tax test', () => {
  it('Type: None', () => {
    const tax = new Tax({tax_rate: 0.08, tax_rule: TAX_RULE.NONE})
    expect(tax.calculate(1000.0000, 'jpy').toFixed(4)).toEqual('0.0000')
  })

  it('Type: INCLUDED', () => {
    const tax = new Tax({tax_rate: 0.08, tax_rule: TAX_RULE.INCLUDED_ROUND})
    expect(tax.calculate(1000.0000, 'jpy').toFixed(4)).toEqual('74.0000')
  })

  it('Type: EXCLUDED', () => {
    const tax = new Tax({tax_rate: 0.08, tax_rule: TAX_RULE.EXCLUDED_ROUND})
    expect(tax.calculate(1000.0000, 'jpy').toFixed(4)).toEqual('80.0000')
  })
})
