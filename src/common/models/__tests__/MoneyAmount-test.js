import Decimal from 'decimal.js'
import { ceil, floor, round } from '../MoneyAmount'

describe('MoneyAmount calculation test', () => {
  it('ceil: decimal place = 0', () => {
    const v = ceil(new Decimal(10.123), 0)
    expect(v.toFixed(4)).toBe('11.0000')
  })

  it('floor: decimal place = 0', () => {
    const v = floor(new Decimal(10.123), 0)
    expect(v.toFixed(4)).toBe('10.0000')
  })

  it('round: decimal place = 0', () => {
    const v = round(new Decimal(10.123), 0)
    expect(v.toFixed(4)).toBe('10.0000')
  })

  it('ceil: decimal place = 2', () => {
    const v = ceil(new Decimal(10.123), 2)
    expect(v.toFixed(4)).toBe('10.1300')
  })

  it('floor: decimal place = 2', () => {
    const v = floor(new Decimal(10.123), 2)
    expect(v.toFixed(4)).toBe('10.1200')
  })

  it('round: decimal place = 2', () => {
    const v = round(new Decimal(10.123), 2)
    expect(v.toFixed(4)).toBe('10.1200')
  })
})
