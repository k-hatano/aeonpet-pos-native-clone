import StoreAccessibleBase from './StoreAccessibleBase'
import { NetInfo, AlertIOS } from 'react-native'
import axios from 'axios'
import moment from 'moment'
import I18n from 'i18n-js'
import logger from '../utils/logger'
import { setUserToken } from '../actions'
import SettingKeys from '../../modules/setting/models/SettingKeys'
import { updateSetting } from '../../modules/setting/actions'
import { setLoggedUserType } from '../../modules/login/actions'
import { USER_TYPE } from '../../modules/login/models'
import NetworkError from '../errors/NetworkError'
import nativeEventAbsorber from 'common/models/NativeEventAbsorber'
import { Buffer } from 'buffer'
import { isBasicAuthError } from '../errors'

class Fetcher extends StoreAccessibleBase {
  _getTimeout (options) {
    const timeout = this.getSetting(SettingKeys.NETWORK.ACCESS.REQUEST_TIMEOUT)
    // 設定になければ、デフォルト 15秒
    return options.timeout ? options.timeout : (timeout > 0 ? timeout * 1000 : 15000)
  }

  _isConnected = true

  async post (path, data, options = {}) {
    this._checkConnectionStatus()
    await this.refreshDeviceTokenIfIsNecessary()
    logger.info('Fetcher.post ' + path)

    const config = {
      headers: { ...this._makeBaseHeader(), ...options.headers },
      timeout: this._getTimeout(options)
    }
    const response = await this._client.post(path, data, config)
    logger.info('Fetcher.post ' + path + ' success')
    return response
  }

  _checkConnectionStatus () {
    if (!this._isConnected) {
      throw new NetworkError(NetworkError.TYPE.OFFLINE)
    }
  }

  async get (path, query, options = {}) {
    this._checkConnectionStatus()
    await this.refreshDeviceTokenIfIsNecessary()
    logger.info('Fetcher.get ' + path)
    query = {
      ...{limit: 200000},
      ...query
    }
    const config = {
      headers: { ...this._makeBaseHeader(), ...options.headers },
      timeout: this._getTimeout(options),
      params: query
    }
    const response = await this._client.get(path, config)
    response.pagination = {
      current: Number(response.headers['x-pagination-current-page']),
      pages: Number(response.headers['x-pagination-page-count']),
      limit: Number(response.headers['x-pagination-per-page']),
      total: Number(response.headers['x-pagination-total-count'])
    }
    logger.info('Fetcher.get ' + path + ' success')
    return response
  }

  async put (path, data, options = {}) {
    this._checkConnectionStatus()
    await this.refreshDeviceTokenIfIsNecessary()
    logger.info('Fetcher.put ' + path)
    const config = {
      headers: { ...this._makeBaseHeader(), ...options.headers },
      timeout: this._getTimeout(options)
    }
    const response = await this._client.put(path, data, config)
    logger.info('Fetcher.put ' + path + ' success')
    return response
  }

  async loginByUser (userName, password) {
    this._checkConnectionStatus()
    const config = {
      headers: { ...this._makeBaseHeader() }
    }

    const data = {
      password: password,
      username: userName
    }

    try {
      let response
      try {
        response = await this._client.post('admin-members/login', data, config)
      } catch (error) {
        if (isBasicAuthError(error)) {
          const input = await this._inputBasicAuthorization()
          const basicAuthToken = Buffer.from(`${input.login}:${input.password}`, 'utf-8').toString('base64')
          this.dispatch(updateSetting({ key: SettingKeys.COMMON.BASIC_AUTH_TOKEN, value: basicAuthToken }))
          config.headers['Authorization'] = `Basic ${basicAuthToken}`
          response = await this._client.post('admin-members/login', data, config)
        } else {
          throw error
        }
      }
      this.dispatch(setUserToken(response.data.token))
      this.dispatch(setLoggedUserType(USER_TYPE.ADMIN))
      logger.info('User@admin login success')
      return
    } catch (error) {
      logger.warning('User@admin Login Error : ' + error.message)
    }

    try {
      const response = await this._client.post('shop-members/login', data, config)
      this.dispatch(setUserToken(response.data.token))
      this.dispatch(setLoggedUserType(USER_TYPE.SHOP_MEMBER))
      logger.info('User@shop-member login success')
    } catch (error) {
      logger.warning('User@shop-member Login Error : ' + error.message)
      throw error
    }
  }

  /**
   *
   * @param shopId
   * @return {Promise.<void>}
   */
  async refreshUserToken (shopId = null) {
    this._checkConnectionStatus()
    if (!shopId) {
      shopId = this.getSetting(SettingKeys.COMMON.SHOP_ID)
    }

    const config = {
      headers: { ...this._makeBaseHeader() }
    }

    const data = {
      shop_id: shopId
    }

    try {
      const response = await this._client.put('users/login/refresh', data, config)
      this.dispatch(setUserToken(response.data.token))
      logger.info('Refresh User Token Success')
    } catch (error) {
      logger.warning('Refresh User Token Error : ' + error.message)
      throw error
    }
  }

  async loginByDevice (udid) {
    const config = {
      headers: { ...this._makeBaseHeader() }
    }

    const data = {
      udid
    }

    try {
      const response = await this._client.post('devices/login', data, config)
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_ID, value: response.data.id }))
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: response.data.token }))
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN_TTL, value: response.data.ttl }))
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.LAST_LOGGED_IN_AT, value: moment().unix() }))
      this.dispatch(setUserToken(''))
      logger.info('Device Login Success : ttl=' + response.data.ttl)
      return response.data
    } catch (error) {
      logger.info('Device Login Failure : ' + error.message)
      return null
    }
  }

  async refreshDeviceTokenIfIsNecessary () {
    if (!this.isLoggedIn) {
      // ログイン状態でなければリフレッシュする必要はない。
      return
    }

    const ttl = this.getSetting(SettingKeys.COMMON.DEVICE_TOKEN_TTL) || 0
    const lastLoggedInAt = this.getSetting(SettingKeys.COMMON.LAST_LOGGED_IN_AT) || 0
    const nowTimestamp = moment().unix()
    const refreshSpan = ttl / 10

    const isExpired = nowTimestamp > lastLoggedInAt + ttl
    if (isExpired) {
      // 一応既に有効期限かどうかをチェックするが、
      // デバイスの時計が変更された可能性もあるので、実際にリフレッシュして確かめるので、ログを記録するだけ。
      logger.warning('Device token is expired at local time')
    }

    if (nowTimestamp > lastLoggedInAt + refreshSpan) {
      logger.info('Refresh token required.')
      await this.refreshDeviceToken()
    } else if (nowTimestamp < lastLoggedInAt) {
      // 前回ログイン時間よりも現在時刻が古い = OSの時計をいじられた可能性が高いので、念のためリフレッシュしておく
      logger.info('Refresh token required. Probably the os clock has changed.')
      await this.refreshDeviceToken()
    }
  }

  async refreshDeviceToken () {
    this._checkConnectionStatus()
    const config = {
      headers: { ...this._makeBaseHeader() }
    }

    try {
      const response = await this._client.put('devices/login/refresh', {}, config)
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN, value: response.data.token }))
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.DEVICE_TOKEN_TTL, value: response.data.ttl }))
      this.dispatch(updateSetting({ key: SettingKeys.COMMON.LAST_LOGGED_IN_AT, value: moment().unix() }))
      logger.info('Refresh Device Token Success : ttl=' + response.data.ttl)
    } catch (error) {
      logger.warning('Refresh Device Token Error : ' + error.message)
      throw error
    }
  }

  /**
   *
   * @private
   */
  get _client () {
    return axios.create({
      baseURL: this.getSetting(SettingKeys.NETWORK.ACCESS.SERVER_URL) + '/api/', // TODO
      timeout: 10000
    })
  }

  _makeBaseHeader () {
    const headers = {}
    headers['Accept-Language'] = this._locale
    headers['X-Currency'] = this._currency
    if (this._deviceToken) {
      headers['X-Authorization'] = `Bearer ${this._deviceToken}`
    } else if (this._userToken) {
      headers['X-Authorization'] = `Bearer ${this._userToken}`
    }
    if (this._basicAuthToken) {
      headers['Authorization'] = `Basic ${this._basicAuthToken}`
    }
    return headers
  }

  logout () {
    this.dispatch(setUserToken(''))
    nativeEventAbsorber.removeEvent('HOME_RETURN_FROM_SLEEP')
  }

  /**
   * Basic認証のID/パスワードをiOS標準のアラート上でユーザーに入力させる
   * @return {Promise<{login: string, password: string}>} 入力されたID/パスワード
   * @private
   */
  _inputBasicAuthorization () {
    return new Promise((resolve, reject) => {
      AlertIOS.prompt(
        I18n.t('authentication.basic_auth'),
        I18n.t('authentication.input_basic_auth'),
        [{
          text: 'OK',
          onPress: (input) => resolve(input)
        }],
        'login-password'
      )
    })
  }

  get isLoggedIn () {
    return this.getSetting(SettingKeys.COMMON.IS_LOGGED_IN) && !!this._deviceToken
  }

  get _userToken () {
    return this.getState().common.userToken
  }

  get _deviceToken () {
    return this.getSetting(SettingKeys.COMMON.DEVICE_TOKEN)
  }

  get _basicAuthToken () {
    return this.getSetting(SettingKeys.COMMON.BASIC_AUTH_TOKEN)
  }

  get hasDeviceToken () {
    return !!this._deviceToken
  }

  get _locale () {
    // 今のところ日本語のみ対応
    return 'ja'
  }

  get _currency () {
    // 今のところ日本円のみ対応
    return 'jpy'
  }
}

const fetcher = new Fetcher()

if (NetInfo.isConnected) {
  NetInfo.isConnected.fetch().then().done(() => {
    NetInfo.isConnected.addEventListener(
      'change',
      (isConnected) => {
        fetcher._isConnected = isConnected
        if (logger.initialized) {
          logger.info('Connection Status Changed - ' + (fetcher._isConnected ? 'ONLINE' : 'OFFLINE'))
        }
      }
    )
  })
}

export default fetcher
