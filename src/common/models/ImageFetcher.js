import StoreAccessibleBase from './StoreAccessibleBase'
import SettingKeys from '../../modules/setting/models/SettingKeys'
import RNFetchBlob from 'react-native-fetch-blob'

const STORAGE_PATH = '/storage/'
const IMAGE_DIR = '/images/'

class ImageFetcher extends StoreAccessibleBase {
  async saveImage (imagePath, key) {
    let dirs = RNFetchBlob.fs.dirs
    const url = await this._imageStorageUrl(imagePath)
    const extension = await this._fileExtension(imagePath)
    const response = await RNFetchBlob
      .config({
        path: dirs.DocumentDir + IMAGE_DIR + key + '.' + extension,
        fileCache: true,
        appendExt: extension
      })
      .fetch('GET', url, {
        'Content-Type': 'image/' + extension
      })

    return response.path()
  }

  _imageStorageUrl (imagePath) {
    return this.getSetting(SettingKeys.NETWORK.ACCESS.SERVER_URL) + STORAGE_PATH + imagePath
  }

  _fileExtension (imagePath) {
    const filePathArray = imagePath.split('.')
    return filePathArray.pop()
  }
}

const imageFetcher = new ImageFetcher()
export default imageFetcher
