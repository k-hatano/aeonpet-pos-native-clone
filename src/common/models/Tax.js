import Decimal from 'decimal.js'
import { ceilByCurrency, floorByCurrency, roundByCurrency } from './Currency'
import MultipleTaxRatesRepository from '../../modules/product/repositories/MultipleTaxRatesRepository'

export const TAX_RULE = {
  NONE: 0,
  INCLUDED_FLOOR: 1,
  INCLUDED_CEIL: 2,
  INCLUDED_ROUND: 3,
  EXCLUDED_FLOOR: 4,
  EXCLUDED_CEIL: 5,
  EXCLUDED_ROUND: 6
}

export const TAX_CODE = {
  STANDARD: 1,
  REDUCED: 2
}

/**
 * 免税区分
 * @type {Object}
 */
export const TAXFREE_TYPE = {
  GENERAL: 1, /** 一般品 */
  EXPENDABLE: 2, /** 消耗品 */
  NOT_APPLICABLE: 3 /** 対象外 */
}

/**
 * @name TaxSettingsObject
 * @class
 * @property {float} tax_rate
 * @property {int} tax_rule
 */

/**
 * @class Tax
 * @property {Decimal} taxRate
 * @property {int} taxRule
 */
export default class Tax {
  /**
   * @param {TaxSettingsObject} settings
   */
  constructor ({ tax_rate: taxRate = 0, tax_rule: taxRule = TAX_RULE.EXCLUDED_CEIL } = {}) {
    this.taxRate = new Decimal(taxRate)
    this.taxRule = taxRule
    this.setTaxRates()
  }

  // 標準税率と軽減税率を設定
  setTaxRates () {
    this.standardTaxRate = new Decimal(0.1)
    this.reducedTaxRate = new Decimal(0.08)
    const standardTaxData = MultipleTaxRatesRepository.findTaxWithTaxCode(TAX_CODE.STANDARD)
    const reducedTaxData = MultipleTaxRatesRepository.findTaxWithTaxCode(TAX_CODE.REDUCED)
    standardTaxData.then(data => {
      if (data !== null) {
        this.standardTaxRate = new Decimal(data.tax_rate)
      }
    })
    reducedTaxData.then(data => {
      if (data !== null) {
        this.reducedTaxRate = new Decimal(data.tax_rate)
      }
    })
  }

  /**
   * @param {Decimal|float|string} amount
   * @return {Decimal}
   * @deprecated Use calculateTax
   */
  calculate (amount, currency) {
    if (!(amount instanceof Decimal)) {
      amount = new Decimal(amount)
    }
    switch (this.taxRule) {
      case TAX_RULE.NONE:
        return new Decimal(0)

      case TAX_RULE.INCLUDED_FLOOR:
        return floorByCurrency(amount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.INCLUDED_CEIL:
        return ceilByCurrency(amount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.INCLUDED_ROUND:
        return roundByCurrency(amount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.EXCLUDED_FLOOR:
        return floorByCurrency(amount.mul(this.taxRate), currency)

      case TAX_RULE.EXCLUDED_CEIL:
        return ceilByCurrency(amount.mul(this.taxRate), currency)

      case TAX_RULE.EXCLUDED_ROUND:
        return roundByCurrency(amount.mul(this.taxRate), currency)
    }
  }

  /**
   * 与えられた金額の税額を計算する
   * @param {Decimal|float|string} baseAmount
   * @param {string} currency
   * @return {Decimal}
   */
  calculateTax (baseAmount, currency) {
    if (baseAmount === undefined) {
      baseAmount = Decimal(0)
    } else if (!(baseAmount instanceof Decimal)) {
      baseAmount = Decimal(baseAmount)
    }

    switch (this.taxRule) {
      case TAX_RULE.NONE:
        return new Decimal(0)

      case TAX_RULE.INCLUDED_FLOOR:
        return floorByCurrency(baseAmount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.INCLUDED_CEIL:
        return ceilByCurrency(baseAmount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.INCLUDED_ROUND:
        return roundByCurrency(baseAmount.mul(this.taxRate).div(this.taxRate.add(1)), currency)

      case TAX_RULE.EXCLUDED_FLOOR:
        return floorByCurrency(baseAmount.mul(this.taxRate), currency)

      case TAX_RULE.EXCLUDED_CEIL:
        return ceilByCurrency(baseAmount.mul(this.taxRate), currency)

      case TAX_RULE.EXCLUDED_ROUND:
        return roundByCurrency(baseAmount.mul(this.taxRate), currency)
    }
  }

  // 標準税率計算
  calculateStandardTax (baseAmount, currency) {
    this.taxRate = this.standardTaxRate
    return this.calculateTax(baseAmount, currency)
  }

  // 軽減税率計算
  calculateReducedTax (baseAmount, currency) {
    this.taxRate = this.reducedTaxRate
    return this.calculateTax(baseAmount, currency)
  }

  /**
   * 税抜き額を算出する (外税ならそのまま返す)
   * @param {Decimal|float|string} baseAmount
   * @param {string} currency
   * @return {Decimal}
   */
  calculateTaxless (baseAmount, currency) {
    if (baseAmount === undefined) {
      baseAmount = Decimal(0)
    } else if (!(baseAmount instanceof Decimal)) {
      baseAmount = Decimal(baseAmount)
    }

    switch (this.taxRule) {
      case TAX_RULE.NONE:
      case TAX_RULE.EXCLUDED_FLOOR:
      case TAX_RULE.EXCLUDED_CEIL:
      case TAX_RULE.EXCLUDED_ROUND:
        return baseAmount

      case TAX_RULE.INCLUDED_FLOOR:
      case TAX_RULE.INCLUDED_CEIL:
      case TAX_RULE.INCLUDED_ROUND:
        return baseAmount.sub(this.calculateTax(baseAmount, currency))
    }
  }

  // 標準税率小計の税抜き額
  standardCalculateTaxless (baseAmount, currency) {
    this.taxRate = this.standardTaxRate
    return this.calculateTaxless(baseAmount, currency)
  }

  // 軽減税率小計の税抜き額
  reducedCalculateTaxless (baseAmount, currency) {
    this.taxRate = this.reducedTaxRate
    return this.calculateTaxless(baseAmount, currency)
  }

  /**
   * 内税かどうか
   * @return {boolean}
   */
  get isTaxIncluded () {
    switch (this.taxRule) {
      case TAX_RULE.INCLUDED_FLOOR:
      case TAX_RULE.INCLUDED_CEIL:
      case TAX_RULE.INCLUDED_ROUND:
        return true

      default:
        return false
    }
  }

  /**
   * 外税かどうか
   * @return {boolean}
   */
  get isTaxExcluded () {
    switch (this.taxRule) {
      case TAX_RULE.EXCLUDED_FLOOR:
      case TAX_RULE.EXCLUDED_CEIL:
      case TAX_RULE.EXCLUDED_ROUND:
        return true

      default:
        return false
    }
  }

  /**
   * 非課税かどうか
   * @return {boolean}
   */
  get isNotTaxable () {
    return this.taxRule === TAX_RULE.NONE
  }
}
