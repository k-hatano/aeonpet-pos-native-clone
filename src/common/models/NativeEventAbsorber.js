import * as _ from 'underscore'
import { NativeEventEmitter, NativeModules } from 'react-native'
import StoreAccessibleBase from 'common/models/StoreAccessibleBase'

const EVENT_DATA = [
  {
    titleKey: 'HOME_RETURN_FROM_SLEEP',
    event: (body, getState) => {
      // エノテカでは端末にパスワードが掛かっているため、強制的にホームに戻す処理は不要
      // const cannotAbort = Modal.cannotAbort()
      // const isLoading = getState.common.isLoading
      // const isOpening = getState.common.isOpening
      // if (fetcher.isLoggedIn && !cannotAbort && !isLoading && !isOpening) {
      //   Modal.closeAll()
      //   Actions.home()
      // }
    }
  }
]

class NativeEventAbsorber extends StoreAccessibleBase {
  constructor () {
    super()
    this.events = []
  }
  registEvent (key, event = null) {
    if (!event) {
      event = this.findEvent(key)
    }
    const eventEmitter = new NativeEventEmitter(NativeModules.PosEventEmitter)
    this.events[key] = eventEmitter.addListener(
      key,
      body => {
        event(body, this.getState())
      }
    )
  }

  removeEvent (key) {
    const event = this.events[key]
    if (event) {
      event.remove()
    }
    delete this.events[key]
  }

  findEvent (key) {
    const eventItems = EVENT_DATA.concat()
    const item = _.find(eventItems, function (item) {
      return item.titleKey === key
    })
    return item.event
  }
}

const nativeEventAbsorber = new NativeEventAbsorber()
export default nativeEventAbsorber
