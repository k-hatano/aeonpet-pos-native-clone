import store from '../store'
import { updateSetting } from '../../modules/setting/actions'

export default class StoreAccessibleBase {
  static _store = store

  /**
   * @protected
   */
  getState () {
    return store.getState()
  }

  /**
   * @protected
   */
  get dispatch () {
    return store.dispatch
  }

  getSetting (key) {
    return store.getState().setting.settings[key]
  }

  updateSetting (key, value) {
    this.dispatch(updateSetting({ key, value }))
  }
}
