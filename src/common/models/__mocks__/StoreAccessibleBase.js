import { sampleSettings } from 'modules/setting/samples'
import { updateSetting } from '../../../modules/setting/actions'

export default class StoreAccessibleBase {
  _store = {}

  /**
   * @protected
   */
  getState () {
    return {...this._store}
  }

  /**
   * @protected
   */
  get dispatch () {
    // return store.dispatch
  }

  getSetting (key) {
    return sampleSettings[key]
  }

  updateSetting (key, value) {
    this.dispatch(updateSetting({ key, value }))
  }
}
