import Decimal from 'decimal.js'

// For don't lose information when rounding
Decimal.set({precision: 19 + 4})

/**
 * @param {Decimal} amount
 * @param {int} decimalPlace
 */
export function ceil (amount, decimalPlace) {
  if (decimalPlace === 0) {
    return amount.ceil()
  } else {
    const multiple = Math.pow(10, decimalPlace)
    return amount.mul(multiple).ceil().div(multiple)
  }
}

/**
 * @param {Decimal} amount
 * @param {int} decimalPlace
 */
export function floor (amount, decimalPlace) {
  if (decimalPlace === 0) {
    return amount.floor()
  } else {
    const multiple = Math.pow(10, decimalPlace)
    return amount.mul(multiple).floor().div(multiple)
  }
}

/**
 * @param {Decimal} amount
 * @param {int} decimalPlace
 */
export function round (amount, decimalPlace) {
  if (decimalPlace === 0) {
    return amount.round()
  } else {
    const multiple = Math.pow(10, decimalPlace)
    return amount.mul(multiple).round().div(multiple)
  }
}
