import { AsyncStorage } from 'react-native'
import DeviceInfo from 'react-native-device-info'

export const ENGLISH_LOCALE = 'en'
export const JAPANESE_LOCALE = 'ja'

export const DEFAULT_LANGUAGE = JAPANESE_LOCALE
export const SUPPORTED_LANGUAGES = [JAPANESE_LOCALE, ENGLISH_LOCALE]

export const CURRENCY_USD = 'usd'
export const CURRENCY_JPY = 'jpy'
export const CURRENCY_JPY_SIGN = '円'
export const CURRENCY_USD_SIGN = '$'

export const DEFAULT_AMOUNT_MAX_LENGTH = 15
export const CASH_AMOUNT_MAX_LENGTH = 8

export const ISO_WEEK = {
  MONDAY: 1,
  TUESDAY: 2,
  WEDNESDAY: 3,
  THURSDAY: 4,
  FRIDAY: 5,
  SATURDAY: 6,
  SUNDAY: 7
}

export function getUdid () {
  return DeviceInfo ? DeviceInfo.getUniqueID() : ''
}

export async function getUserLocale () {
  const locale = await AsyncStorage.getItem('user_locale')
  return locale
}

export async function setUserLocale (locale) {
  try {
    await AsyncStorage.setItem('user_locale', locale)
  } catch (error) {
    console.error('AsyncStorage error: ' + error.message)
  }
}

export function getCurrencyLabel (currency) {
  switch (currency) {
    case CURRENCY_JPY:
      return CURRENCY_JPY_SIGN
    default:
      return CURRENCY_USD_SIGN
  }
}

/**
 * AlertViewやConfirmViewのサイズをメッセージの行数から決定し、
 * height及びスクロールするかどうかの真偽値を返す
 * @param message
 * @returns {{height: number, canScroll: boolean}}
 */
export function calculateModalSize (message) {
  // メッセージの行数は改行文字で分割した配列のlengthから取得するものとする
  // ここでの処理で通常サイズ(height: 302)以外になるのは返品時の金種一覧表示の時のみであるとし、
  // かつ、そこでは文字数超過による改行は行われないものと判断したため
  let lineCount = 0
  const minLineCount = 6
  const maxLineCount = 10
  const defaultHeight = 302
  const charaterHeight = 40

  // ','を含む文字列は含まない文字より高さが大きくなる様子？(不明瞭)
  // 体感1.3倍くらい違うので、','が含まれた場合はそのぶん行数カウントを余分に摂る必要がある
  // ','を含むときはカウント+1.3、そうでなければカウント+1とする
  message.split('\n').forEach(line => {
    lineCount = line.indexOf(',') !== -1 ? lineCount + 1.3 : lineCount + 1
  })

  if (lineCount <= minLineCount) {
    // ConfirmViewは6行までであればレイアウト崩れはないため、通常サイズのモーダルを出す
    return { height: defaultHeight, canScroll: false }
  } else if (lineCount <= maxLineCount) {
    // 最大値を10行とする。
    // 最大行に達するまでは柔軟にモーダルのサイズを変更する
    // 6行から超過した行数につき高さを30増加させる(目分量)
    return { height: Math.ceil(defaultHeight + charaterHeight * (lineCount - minLineCount)), canScroll: false }
  } else {
    // 最大とした10行も上回る場合は、10行分の高さで固定とする
    return { height: Math.ceil(defaultHeight + charaterHeight * (maxLineCount - minLineCount)), canScroll: true }
  }
}

export function isNullOrUndefined (value) {
  return value === undefined || value === null
}

export function isNullOrEmpty (value) {
  return isNullOrUndefined(value) || value === ''
}
