// import { AsyncStorage } from 'react-native'

export default class StaffSettings {
  static _isStaffFixed = false
  static _selectedStaff = null;
  static _rolePermissions = null;

  static get isStaffFixed () {
    return this._isStaffFixed
  }

  static get selectedStaff () {
    return this._selectedStaff
  }

  static get rolePermissions () {
    return this._rolePermissions
  }

  static async setIsStaffFixed (value) {
    this._isStaffFixed = value
  }

  static async setSelectedStaff (staff) {
    this._selectedStaff = staff
  }

  static async setRolePermissions (permission) {
    this._rolePermissions = permission
  }

  // TODO??
  static fetchEntity (key) {
  }
}