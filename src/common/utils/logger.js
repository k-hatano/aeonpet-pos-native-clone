import { Platform } from 'react-native'
import FileSystem from 'react-native-filesystem-v1'
import stacktraceParser from 'stacktrace-parser'
import moment from 'moment'
import SettingKeys from 'modules/setting/models/SettingKeys'
import { isDevMode } from './environment'
import StoreAccessibleBase from '../models/StoreAccessibleBase'
import { updateSetting } from '../../modules/setting/actions'
import rejectionTracking from 'promise/setimmediate/rejection-tracking'
import AlertView from 'common/components/widgets/AlertView'
import I18n from 'i18n-js'

export const LogLevel = {
  FATAL: 5,
  ERROR: 4,
  WARNING: 3,
  INFO: 2,
  DEBUG: 1
}

export function getLogLevelLabel (logLevel) {
  switch (logLevel) {
    case LogLevel.FATAL: return 'FATAL'
    case LogLevel.ERROR: return 'ERROR'
    case LogLevel.WARNING: return 'WARNING'
    case LogLevel.INFO: return 'INFO'
    case LogLevel.DEBUG: return 'DEBUG'
  }
  return '--'
}

class Logger extends StoreAccessibleBase {
  async fatal (message) {
    await this._log(LogLevel.FATAL, message)
    if (isDevMode()) {
      console.error('faital  : ' + message)
    }
  }

  async error (message) {
    await this._log(LogLevel.ERROR, message)
    if (isDevMode()) {
      console.log(message)
      console.warn('error : ' + message)
    }
  }

  async warning (message) {
    await this._log(LogLevel.WARNING, message)
    if (isDevMode()) {
      console.warn('warn : ' + message)
    }
  }

  async info (message) {
    await this._log(LogLevel.INFO, message)
    if (isDevMode()) {
      console.log('info : ' + message)
    }
  }

  async debug (message) {
    await this._log(LogLevel.DEBUG, message)
    if (isDevMode()) {
      console.log('debug : ' + message)
    }
  }

  async _log (level, message) {
    if (!this._initialized) {
      console.warn('Logger is not initialized')
      console.log(message)
      return
    }
    if (level >= this._writableLogLevel) {
      const content = `${this.getTimestamp()} ${getLogLevelLabel(level)} ${message}`
      await this._writeLog(content)
    }
  }

  get _writableLogLevel () {
    return this.getSetting(SettingKeys.LOG.LEVEL)
  }

  get _filePathes () {
    return this.getSetting(SettingKeys.LOG.FILE_PATHS)
  }

  get _rotationDays () {
    return this.getSetting(SettingKeys.LOG.ROTATION_DAYS)
  }

  get _logReactNativeError () {
    return this.getSetting(SettingKeys.LOG.REACT_NATIVE_ERROR)
  }

  _initialized = false

  init () {
    // if (this._logReactNativeError) {
    //   this.initGlobalErrorLogging()
    // }
    this.initGlobalErrorLogging()
    this._initialized = true
  }

  get initialized () {
    return this._initialized
  }

  getTimestamp () {
    return moment(new Date()).format('YYYY-MM-DD HH:mm:ssZZ')
  }

  generateFileKeyForNow () {
    return moment(new Date()).format('YYYY-MM-DD')
  }

  async _writeLog (content) {
    if (Platform.OS === 'windows') {
      // TODO Windowsでは排他処理がうまくいかず、エラーを起こすことがあるので暫定的に書き込みしないようにする。
      return
    }
    const currentFileName = 'logs/app_' + this.generateFileKeyForNow() + '.log'
    const logExists = await FileSystem.fileExists(currentFileName)
    if (logExists) {
      await FileSystem.writeToFile(currentFileName, `\n${content}`, true)
    } else {
      await this._rotate(currentFileName)
      await FileSystem.writeToFile(currentFileName, content)
    }
  }

  async _rotate (currentFileName) {
    const pathes = this._filePathes
    const nextPathes = pathes.splice(-this._rotationDays)
    for (const i in pathes) {
      const path = pathes[i]
      try {
        await FileSystem.delete(path)
      } catch (error) {
        console.warn('Delete Expired Files Failure', error)
      }
    }
    nextPathes.push(currentFileName)
    this.dispatch(updateSetting({ key: SettingKeys.LOG.FILE_PATHS, value: nextPathes }))
  }

  async readLogFile (logPath) {
    const logExisted = await FileSystem.fileExists(logPath)
    if (logExisted) {
      return FileSystem.readFile(logPath)
    } else {
      return ''
    }
  }

  parseErrorStack = error => {
    if (!error || !error.stack) {
      return []
    }
    return Array.isArray(error.stack) ? error.stack : stacktraceParser.parse(error.stack)
  }

  initGlobalErrorLogging () {
    if (global.ErrorUtils) {
      const globalHandler = global.ErrorUtils.getGlobalHandler && global.ErrorUtils.getGlobalHandler()
      if (globalHandler) {
        global.ErrorUtils.setGlobalHandler(async (error, isFatal) => {
          await this.writeGlobalLog(isFatal, error.message, this.parseErrorStack(error))
        })
      }
    }
    // 非同期に呼び出さたメソッドの例外ハンドリング
    rejectionTracking.enable({
      allRejections: true,
      onUnhandled: async (id, error) => {
        await this.writeGlobalLog(false, error.message, this.parseErrorStack(error))

        AlertView.show(I18n.t('message.A-00-E001', {error: error.message}))
      }
    })
  }

  async writeGlobalLog (fatal, message, stackTrace) {
    let errorString = `ERROR: ${message} \nSTACKSTRACE:\n`
    if (stackTrace && Array.isArray(stackTrace)) {
      const timestamp = this.getTimestamp()
      const stackMessages = stackTrace.map(stackTraceItem => {
        let file = stackTraceItem.file !== undefined ? stackTraceItem.file : '-'
        if (file.slice(-8) === 'jsbundle') {
          file = 'main.jsbundle'
        }
        const methodName = stackTraceItem.methodName && stackTraceItem.methodName !== '<unknown>'
          ? stackTraceItem.methodName
          : '-'
        const lineNumber = stackTraceItem.lineNumber !== undefined ? stackTraceItem.lineNumber.toString() : '-'
        const column = stackTraceItem.column !== undefined ? stackTraceItem.column.toString() : '-'
        return `${timestamp} Method: ${methodName}, LineNumber: ${lineNumber}, Column: ${column}`
      })
      errorString += stackMessages.join('\n')
    }

    if (isDevMode()) {
      console.warn(errorString)
    }

    if (fatal) {
      return this._writeLog(`${this.getTimestamp()} RNFatal ${errorString}`)
    }
    return this._writeLog(`${this.getTimestamp()} RNError ${errorString}`)
  }
}

const logger = new Logger()
export default logger
