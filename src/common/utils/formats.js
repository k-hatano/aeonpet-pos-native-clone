import { getCurrencyLabel, CURRENCY_JPY } from '../models'
import Decimal from 'decimal.js'

export const formatMoney = (value, option = {}) => {
  return value ? parseFloat(value).toLocaleString('en-US', option) : 0
}

export const formatNumber = (value, option = {}) => {
  return value ? parseFloat(value).toLocaleString('en-US', option) : 0
}

export const formatNumberZeroNull = (value, option = {}) => {
  return value ? parseFloat(value).toLocaleString('en-US', option) : null
}

export const formatNumberZeroNotNull = (value, option = {}) => {
  return value === 0 || value ? parseFloat(value).toLocaleString('en-US', option) : null
}

export const formatPriceWithCurrency = (price, currency = CURRENCY_JPY) => {
  const currencyLabel = getCurrencyLabel(currency)
  const minimumFractionDigits = currency === CURRENCY_JPY ? 0 : 2
  if (price instanceof Decimal) {
    price = price.toNumber()
  }
  price = formatMoney(price, { minimumFractionDigits })
  // HACK I18nを使ってフォーマット
  return `${price}${currencyLabel}`
}

export const formatUnit = (value, unit = '') => {
  return formatNumber(value) + unit
}

export const formatDateFromValueFront = (date) => {
  if (!date) {
    return ''
  }

  if (date.length !== 8) {
    // Invalid date format
    return ''
  }

  const year = date.slice(0,4)
  const month = date.slice(4,6)
  const day = date.slice(6,8)
  return [year, month, day].join('/')
}
