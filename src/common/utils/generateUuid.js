import uuidv4 from 'uuid/v4'

export default () => {
  return uuidv4({}, []).map(v => ('0' + v.toString(16)).slice(-2)).join('')
}
