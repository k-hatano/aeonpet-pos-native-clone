export function encodeCriteria (criteria) {
  const query = {}

  const fields = Object.keys(criteria)
  if (fields.length > 0) {
    const search = []
    fields.forEach((field) => {
      if (field === '_advancedSearch') {
        query.advancedSearch = 1
        return
      }
      const value = criteria[field]
      if (value !== null && value !== undefined && value !== '') {
        search.push(field + ':' + value)
      }
    })
    if (search.length > 0) {
      query.search = search.join(';')
    }
  }

  return query
}

export function getQueryString (query) {
  const keys = Object.keys(query)
  if (keys.length === 0) {
    return ''
  }
  return '?' + keys.map(function (key) {
      return key + '=' + query[key]
    }).join('&')
}
