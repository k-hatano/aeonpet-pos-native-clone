import { NativeModules } from 'react-native'
import logger from 'common/utils/logger'

export async function profileMemory (name, process) {
  const startedAt = (new Date()).getTime()
  const startMemory = await getRamAsync()

  const result = await process()

  const endMemory = await getRamAsync()
  const endedAt = (new Date()).getTime()
  const elapsedTime = endedAt - startedAt
  await logger.info(`Profiling... [${name}] :  Time ${elapsedTime}ms  Memory ${startMemory}MB => ${endMemory}MB`)
  return result
}

const getRamAsync = () => {
  return new Promise((resolve, reject) => {
    NativeModules.PerformanceBridge.getCurrentResidentSize(result => {
      if (result && result.length) {
        resolve(result)
      } else {
        logger.warning('Failed to get current RAM.')
        resolve(null)
      }
    })
  })
}
