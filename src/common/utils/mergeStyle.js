export default function (style1, style2) {
  const merged = {}
  for (const i in {...style1, ...style2}) {
    merged[i] = {...(style1[i] || {}), ...(style2[i] || {})}
  }
  return merged
}
