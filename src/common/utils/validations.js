export const required = (value, field) => {
  if (isEmpty(value)) {
    return `Please enter ${field}`
  }
  return false
}

export const isEmpty = value => {
  return value === undefined || value === null || value === ''
}

export function getValidationMessages (error) {
  let { message } = error
  if (error && error.errors) {
    const fieldErrorMessages = Object.keys(error.errors).map(field => error.errors[field]).join('\n')
    return `${message}\n\n${fieldErrorMessages}`
  }
  return message
}

export const isHalfWidthAndSymbol = value => value.match(/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/)
