/* eslint-disable no-throw-literal */
import Sequelize from 'sequelize'
import { sequelize } from 'common/DB'
import logger from './logger'
import availableMigrations from '../migrations'

function compareByName ({ prevItem, nextItem, isDescending = false }) {
  var prevItemName = Object.keys(prevItem).shift().toUpperCase()
  var nextItemName = Object.keys(nextItem).shift().toUpperCase()
  if (prevItemName < nextItemName) {
    return isDescending ? 1 : -1
  }
  if (prevItemName > nextItemName) {
    return isDescending ? -1 : 1
  }
  return 0
}

class Migrator {
  static MIGRATION_UP = 'up'
  static MIGRATION_DOWN = 'down'
  static MIGRATION_TABLE = 'migrations'
  static DEFAULT_ROLLBACK_STEP = 1
  static DEFAULT_REFRESH_STEP = 1000

  constructor () {
    this.db = sequelize
  }

  async createMigrationTable () {
    const db = this.db.getQueryInterface()
    const migrationTables = await this.db.query(`SELECT 1 FROM sqlite_master WHERE name='${Migrator.MIGRATION_TABLE}'`)
    if (migrationTables.length === 0) {
      await db.createTable(Migrator.MIGRATION_TABLE, {
        id: {
          primaryKey: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        migration: {
          type: Sequelize.STRING
        },
        batch: {
          type: Sequelize.INTEGER
        }
      })
    }
  }

  async init () {
    try {
      // Create table for saving migration history
      await this.createMigrationTable()

      const executedMigrations = await this.db.query(
        `
        SELECT id, migration, batch
        FROM ${Migrator.MIGRATION_TABLE}
        ORDER BY migration
      `,
        {
          type: Sequelize.QueryTypes.SELECT
        }
      )
      return executedMigrations
    } catch (exception) {
      throw `MIGRATION INIT ERROR: ${exception}`
    }
  }

  async up () {
    await this.migrate({ type: Migrator.MIGRATION_UP })
  }

  async down ({ step = Migrator.DEFAULT_ROLLBACK_STEP } = {}) {
    await this.migrate({ type: Migrator.MIGRATION_DOWN, step })
  }

  async refresh ({ step = Migrator.DEFAULT_REFRESH_STEP } = {}) {
    // TODO Make it better
    // Refresh by rollback all & re-migrate
    await this.migrate({ type: Migrator.MIGRATION_DOWN, step })
    await this.migrate({ type: Migrator.MIGRATION_UP })
  }

  async migrate ({ type, step = Migrator.DEFAULT_ROLLBACK_STEP }) {
    try {
      const executedMigrations = (await this.init()) || []
      const executedMigrationSet = new Set(executedMigrations.map(item => item.migration))
      let willRunMigrations = []
      let batch = 0
      await logger.info(`MIGRATION ${type} > START`)

      if (executedMigrations.length > 0) {
        batch = executedMigrations.reduce((value, item) => Math.max(item.batch, value), -Infinity)
      }

      if (type === Migrator.MIGRATION_UP) {
        batch += 1
        // Find migrations which are not executed yet and existed with order by oldest
        willRunMigrations = availableMigrations
          .filter(item => !executedMigrationSet.has(Object.keys(item).shift()))
          .sort((prevItem, nextItem) => compareByName({ prevItem, nextItem, isDescending: false }))
      } else {
        batch = step >= batch ? 0 : (batch - step)
        // Find migrations which are already executed and existed with order by latest
        willRunMigrations = availableMigrations
          .filter(item => {
            const migrationName = Object.keys(item).shift()
            const migration = executedMigrations.find(item => item.migration === migrationName)
            if (executedMigrationSet.has(migrationName) && migration.batch > batch) {
              return item
            }
          })
          .sort((prevItem, nextItem) => compareByName({ prevItem, nextItem, isDescending: true }))
      }

      const length = willRunMigrations.length
      // Make async task work correct
      for (let i = 0; i < length; i++) {
        const migrationData = willRunMigrations[i]
        const name = Object.keys(migrationData).shift()
        const migration = migrationData[name]
        if (migration) {
          await this.execute({ migration, name, type, batch })
          await logger.info(`MIGRATED ${name}`)
        }
      }
    } catch (exception) {
      await logger.fatal(exception)
    } finally {
      await logger.info(`MIGRATION ${type} > END`)
    }
  }

  async done ({ name, type, batch }) {
    try {
      if (type === Migrator.MIGRATION_UP) {
        logger.info('Execute mitrage up ' + name)
        await this.db.query(
          `
          INSERT INTO ${Migrator.MIGRATION_TABLE} (migration, batch)
          VALUES('${name}', ${batch})
        `,
          {
            type: Sequelize.QueryTypes.INSERT
          }
        )
      } else {
        logger.info('Execute mitrage down ' + name)
        await this.db.query(
          `
          DELETE FROM ${Migrator.MIGRATION_TABLE}
          WHERE migration = '${name}'
        `,
          {
            type: Sequelize.QueryTypes.DELETE
          }
        )
      }
    } catch (exception) {
      throw `ERROR WHEN SAVE MIGRATION HISTORY ${exception}`
    }
  }

  async execute ({ migration, name, type, batch }) {
    try {
      await migration[type](this.db.getQueryInterface(), this.db.constructor)
      await this.done({ name, type, batch })
    } catch (exception) {
      throw `MIGRATION INSIDE ${name}: ${exception}`
    }
  }
}

export default new Migrator()
