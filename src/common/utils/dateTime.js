import moment from 'moment'

/**
 * 日付を書式設定します。
 * @param {Date} timestamp
 * @param {string} format
 */
export function formatDate (timestamp, format = 'YYYY-MM-DD') {
  return moment(timestamp).format(format)
}

export function formatDateTime (timestamp, format = 'YYYY-MM-DD HH:mm:ss') {
  return moment(timestamp).format(format)
}

/**
 * 日時オブジェクトからUnixTimestamp形式の日付検索用パラメーターを生成する
 * @param from
 * @param to
 * @returns {{from: (*|number), to: (*|number)}}
 */
export function makeDateRange (from, to) {
  return {
    from: from && Math.floor(from.setHours(0, 0, 0) / 1000),
    to: to && Math.floor(to.setHours(23, 59, 59) / 1000)
  }
}

export function toUnixTimestamp (date) {
  return Math.floor(date.getTime() / 1000)
}

/**
 * 現在日付（時刻なし）を取得します。
 * @param {String} format 書式
 * @returns {Date|String}
 */
export function getToday (format = null) {
  return getStartOfDay(new Date(), format)
}
/**
 * 本日の最終日時（23:59:59）を取得します。
 * @param {String} format 書式
 * @returns {Date|String}
 */
export function getEndOfToday (format = null) {
  return getEndOfDay(new Date(), format)
}

/**
 * 指定した日付の開始日時（0:00:00）を取得します。
 * @param {Date} date 日付
 * @param {string} format 書式
 * @returns {Date|string}
 */
export function getStartOfDay (date, format = null) {
  const value = moment(date).startOf('day')

  return format !== null ? value.format(format) : value._d
}
/**
 * 指定した日付の終了日時（23:59:59）を取得します。
 * @param {Date} date 日付
 * @param {string} format 書式
 * @returns {Date|string}
 */
export function getEndOfDay (date, format = null) {
  const value = moment(date).endOf('day')

  return format !== null ? value.format(format) : value._d
}

/**
 * 日付と日数を指定して、日付に日数を加算した日付を取得します。
 * @param {Date} date 日付
 * @param {Number} days 加算する日数
 * @param {string} format 書式
 * @returns {Date|string}
 */
export function addDays (date, days, format = null) {
  const value = new Date(new Date(date).setDate(date.getDate() + days))

  return format !== null ? formatDate(value, format) : value
}
