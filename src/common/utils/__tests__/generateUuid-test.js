import generateUuid from '../generateUuid'

describe('generateUuid test', () => {
  it('Test length', () => {
    for (let i = 0; i < 16; i++) {
      const uuid = generateUuid()
      expect(uuid.length).toBe(32)
    }
  })
})
