import logger from 'common/utils/logger'
import { NativeModules } from 'react-native'
const SQLite = NativeModules.RNSqlite2

// SQLitePLuginResult
const ERROR = 0
// const INSERTED_ID = 1
const ROWS_AFFECTED = 2
const COLUMNS = 3
const ROWS = 4

class Database {
  static DEBUG_MODE = true

  constructor (file, options, callback) {
    this.debug(`[SQLite3] Database: ${file}, ${options}`)
    this.callback = callback
    this.db = file

    setTimeout(async () => {
      await this.openDatabase({ file })
    }, 1)
  }

  // Try execute a query which will create new database if it doestn't exist
  async openDatabase ({ file }) {
    try {
      await SQLite.exec(file, [['PRAGMA FOREIGN_KEYS=ON', []]], false)
      this.callback()
    } catch (exception) {
      this.debug('[SQLite3] OPEN DATABASE FAILED:', exception)
    }
  }

  // Using for INSERT, UPDATE, DELETE, CREATE TEMPORARY TABLE
  run (sql, args = [], callback) {
    if (typeof args === 'function') {
      callback = args
      args = []
    }
    this.debug('[SQLite3] RUN:', `${sql} - ${args}`)
    this.db && this.query({ sql, args, callback })
  }

  exec (sql, args = [], callback) {
    if (typeof args === 'function') {
      callback = args
      args = []
    }
    this.debug('[SQLite3] EXEC:', `${sql} - ${args}`)
    this.db && this.query({ sql, args, callback })
  }

  // Using for SELECT QUERY
  all (sql, args = [], callback) {
    if (typeof args === 'function') {
      callback = args
      args = []
    }
    this.debug('[SQLite3] ALL:', `${sql} - ${args}`)
    this.db && this.query({ sql, args, callback })
  }

  async query ({ sql, args = [], callback }) {
    if (this.db) {
      try {
        const results = await SQLite.exec(this.db, [[sql, args]], false)
        this.handleResults({ results, callback })
      } catch (exception) {
        this.handleError({ error: exception, sql, callback })
      }
    } else {
      this.handleError({ message: '[SQLite3] DATABASE IS NOT FOUND', sql, callback })
    }
  }

  // Call when transaction is complete
  close () {
    // TODO Handle it
  }

  handleError ({ error, sql, callback }) {
    logger.error('SQLite Query Error ' + error)
    this.debug('[SQLite3] QUERY ERROR:', error)
    // Prepare error response
    const response = {
      code: '',
      message: error,
      sql
    }
    const metadata = {
      changes: 0
    }

    ;(callback || this.callback).bind(metadata)(response, [])
  }

  handleResults ({ results, callback }) {
    const result = results.shift()
    // Query has error
    if (result[ERROR]) {
      throw result[ERROR]
    }

    // Prepare response
    const queryResults = result[ROWS] || []
    const columns = result[COLUMNS] || []
    const resultsAsObjects = queryResults.map(result => {
      let resultObject = {}
      for (const i in columns) {
        resultObject[columns[i]] = result[i]
      }
      return resultObject
    })
    const metadata = {
      changes: result[ROWS_AFFECTED]
    }
    this.debug('[SQLite3] QUERY RESULTS:', resultsAsObjects)

    ;(callback || this.callback).bind(metadata)(null, resultsAsObjects)
  }

  debug (message, params = '') {
    logger.debug(message, params)
  }

  serialize (callback) {
    callback()
  }
}

const sqlite3 = {
  Database
}

export function verbose () {
  return sqlite3
}
