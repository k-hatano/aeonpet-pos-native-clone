import I18n from 'i18n-js'
import logger from 'common/utils/logger'
import NetworkError from './errors/NetworkError'
import { isDevMode } from './utils/environment'

export const REQUEST_ERROR_CODE = {
  DUPLICATED_ENTRY: 'request.duplicated_entry',
  CUSTOMER_NOT_EXIST: 'customer.not_exist',
  CUSTOMER_CODE_DUPLICATE: 'customer.customer_code.duplicate',
  ORDER_CANCEL: 'order.cancel',
  ORDER_CANCEL_IS_RETURNED: 'order.cancel.is_returned',
  ORDER_CANCEL_IS_FINALIZED: 'order.cancel.is_finalized',
  ORDER_CANCEL_HAS_BILL: 'order.cancel.has_bill',
  ORDER_CANCEL_IS_CANCELED: 'order.cancel.is_canceled',
  IN_STOCK_BAD_STATUS: 'stock_movement.in_stock.bad_status',
  OUT_STOCK_DUPLICATE_SLIP_NUMBER: 'stock_movement.out_stock.duplicate_slip_number',
  CANCEL_STOCK_BAD_STATUS: 'stock_movement.cancel_stock.bad_status',
  STOCK_MOVE_PRODUCT_NOT_FOUND: 'stock_movement.move.product_not_found',
  STOCK_MOVE_PRODUCT_STOCK_MODE: 'stock_movement.move.product_stock_mode',
  STOCK_ADJUST_BAD_REASON: 'stock_adjustment.adjust.bad_reason',
  STOCK_ADJUST_BAD_STATUS: 'stock_adjustment.adjust.bad_status',
  STOCK_ADJUST_PRODUCT_NOT_FOUND: 'stock_adjustment.adjust.product_not_found',
  STOCK_ADJUST_PRODUCT_STOCK_MODE: 'stock_adjustment.adjust.product_stock_mode',
  STOCK_TRANSACTION_OVER_MAX_QUANTITY: 'stock_transaction.over_max_quantity',
  STOCK_TRANSACTION_UNDER_MIN_QUANTITY: 'stock_transaction.under_min_quantity'
}

export const handleAxiosError = (error, category = '') => {
  if (error instanceof NetworkError) {
    throw error
  }
  if (error.code === 'ECONNABORTED') {
    logger.warning(`[${category}] : http connection timeouts`)
    throw new NetworkError(NetworkError.TYPE.TIMEOUT)
  }
  if (error.response) {
    if (error.response.status === 403) {
      throw new NetworkError(NetworkError.TYPE.UNAUTHORIZED)
    }
    let info = 'status=' + error.response.status
    logger.warning(`[${category}] : http response error ` + info)
    const statusHead = error.response.status.toString().slice(0, 1)
    if (statusHead === '4') {
      if (isDevMode()) {
        console.log(error)
      }
      if (isBasicAuthError(error)) {
        throw new NetworkError(NetworkError.TYPE.BASIC_AUTH)
      } else {
        throw new NetworkError(NetworkError.TYPE.CLIENT_ERROR, undefined, error)
      }
    } else if (statusHead === '5') {
      throw new NetworkError(NetworkError.TYPE.SERVER_ERROR)
    }
    if (isDevMode()) {
      console.log(error)
    }
    throw new NetworkError(NetworkError.TYPE.UNKNOWN)
  }

  if (error.request) {
    logger.warning(`[${category}] : http network error ` + error.message)
    if (isDevMode()) {
      console.log(error)
    }
    throw new NetworkError(NetworkError.TYPE.INVALID_REQUEST)
  }

  logger.error(`[${category}] : http unexpected error`)
  if (isDevMode()) {
    console.log(error)
  }
  throw new NetworkError(NetworkError.TYPE.UNKNOWN)
}

export const handleFromServerError = (error) => {
  throw new NetworkError(NetworkError.ANY, error.response.data.message)
}

export const handleDatabaseError = (error) => {
  logger.error(`Local Database Error： ${error.message}`)
}

export function getCommonNetworkErrorMessage (error, defaultMessage = undefined) {
  if (error instanceof NetworkError && error.type === NetworkError.TYPE.OFFLINE) {
    return I18n.t('message.A-01-E100')
  } else {
    return defaultMessage === undefined ? error.message : defaultMessage
  }
}

export type ValidationResult = {
  isValid: boolean,
  message?: string
}

export function isBasicAuthError (error) {
  return error &&
    error.response &&
    error.response.headers['www-authenticate'] &&
    error.response.headers['www-authenticate'].startsWith('Basic')
}
