﻿import React from 'react'
import { BackAndroid } from 'react-native'
import { connect, Provider } from 'react-redux'
import { Scene, Router, Actions, ActionConst } from 'react-native-router-flux'
import store from './common/store'
import LoginUserPage from './pages/LoginUserPage'
import LoginSelectShopPage from './pages/LoginSelectShopPage'
import LoginSelectCashierPage from './pages/LoginSelectCashierPage'
import LoginConfirmPage from './pages/LoginConfirmPage'
import SearchOrderPage from './pages/SearchOrderPage'
import Customer from './pages/CustomerPage'
import OrderCompletePage from './pages/OrderCompletePage'
import CashierRecord from './pages/CashierRecordPage'
import CheckSales from './pages/CheckSalesPage'
import CloseSales from './pages/CloseSalesPage'
import CloseSalesAggregate from './pages/CloseSalesAggregatePage'
import Balances from './pages/BalancePage'
import Opening from './pages/Opening'
import Setting from './pages/Setting'
import OpenSales from './pages/OpenSales'
import SyncData from './pages/SyncData'
import TaxFree from './pages/TaxFreePage'
import TaxFreeSellerFormPage from './pages/TaxFreeSellerFormPage'
import TaxFreeConfirmPage from './pages/TaxFreeConfirmPage'
import CartPage from './pages/CartPage'
import InitializePage from './pages/InitializePage'
import StockSearchPage from './pages/StockSearchPage'
import StockMovePage from './pages/StockMovePage'
import StockAdjustmentPage from './pages/StockAdjustmentPage'
import StockAdjustmentSearchPage from './pages/StockAdjustmentSearchPage'
import Debug from './pages/Debug'
import ModalSamplePage from './samplePages/ModalSamplePage'
import ButtonSamplePage from './samplePages/ButtonSamplePage'
import OPN3200SamplePage from './samplePages/OPN3200SamplePage'
import ComponentSamplePage from './samplePages/ComponentSamplePage'
import HomePage from './pages/HomePage'
import StockTakingPage from './pages/StockTakingPage'
import StockTakingCountPage from './pages/StockTakingCountPage'
import PrinterErrorPage from './pages/PrinterErrorPage'
import RedirectPage from './pages/RedirectPage'
import ReturnCompletePage from './pages/ReturnCompletePage'
import AlertViewSamplePage from './samplePages/AlertViewSamplePage'
import StockMoveInPage from './pages/StockMoveInPage'
import StockMoveOutPage from './pages/StockMoveOutPage'
import CustomerRegisterPage from './pages/CustomerRegisterPage'
import HelpPage from './pages/HelpPage'

const RouterWithRedux = connect()(Router)

class App extends React.Component {
  constructor (props) {
    super(props)

    this.handleBackAndroid = () => Actions.pop()
  }

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this.handleBackAndroid)
  }

  componentWillMount () {
    BackAndroid.removeEventListener('hardwareBackPress', this.handleBackAndroid)
  }

  render () {
    return (
      <Provider store={store}>
        <RouterWithRedux duration={0} animation='fade' backAndroidHandler={() => true}>
          <Scene key='root'>
            <Scene key='initialize' type={ActionConst.RESET} component={InitializePage} hideNavBar initial />
            <Scene key='loginUserPage' type={ActionConst.RESET} component={LoginUserPage} hideNavBar />
            <Scene key='loginSelectShopPage' component={LoginSelectShopPage} hideNavBar />
            <Scene key='loginSelectCashierPage' component={LoginSelectCashierPage} hideNavBar />
            <Scene key='loginConfirmPage' component={LoginConfirmPage} hideNavBar />
            <Scene key='home' type={ActionConst.RESET} component={HomePage} hideNavBar />
            <Scene key='stockSearch' type={ActionConst.RESET} component={StockSearchPage} hideNavBar />
            <Scene key='stockMove' type={ActionConst.RESET} component={StockMovePage} hideNavBar />
            <Scene key='orderSearchPage' type={ActionConst.RESET} component={SearchOrderPage} hideNavBar />
            <Scene key='stockAdjustment' type={ActionConst.RESET} component={StockAdjustmentPage} hideNavBar />
            <Scene key='stockAdjustmentSearch' type={ActionConst.RESET} component={StockAdjustmentSearchPage} hideNavBar />
            <Scene key='customer' type={ActionConst.RESET} component={Customer} hideNavBar />
            <Scene key='orderCompletePage' type={ActionConst.RESET} component={OrderCompletePage} hideNavBar />
            <Scene key='returnCompletePage' type={ActionConst.RESET} component={ReturnCompletePage} hideNavBar />
            <Scene key='cashierRecord' component={CashierRecord} hideNavBar />
            <Scene key='checkSales' component={CheckSales} hideNavBar />
            <Scene key='closeSales' component={CloseSales} hideNavBar />
            <Scene key='closeSalesAggregate' type={ActionConst.RESET} component={CloseSalesAggregate} hideNavBar />
            <Scene key='balances' type={ActionConst.RESET} component={Balances} hideNavBar />
            <Scene key='opening' type={ActionConst.RESET} component={Opening} hideNavBar />
            <Scene key='setting' component={Setting} hideNavBar />
            <Scene key='openSales' type={ActionConst.RESET} component={OpenSales} hideNavBar />
            <Scene key='syncData' component={SyncData} hideNavBar />
            <Scene key='taxFree' component={TaxFree} hideNavBar />
            <Scene key='taxFreeSellerForm' component={TaxFreeSellerFormPage} hideNavBar />
            <Scene key='taxFreeConfirm' component={TaxFreeConfirmPage} hideNavBar />
            <Scene key='debug' type={ActionConst.RESET} component={Debug} hideNavBar />
            <Scene key='cartPage' type={ActionConst.RESET} component={CartPage} hideNavBar />
            <Scene key='modalSamplePage' type={ActionConst.RESET} component={ModalSamplePage} hideNavBar />
            <Scene key='buttonSamplePage' type={ActionConst.RESET} component={ButtonSamplePage} hideNavBar />
            <Scene key='OPN3200SamplePage' type={ActionConst.RESET} component={OPN3200SamplePage} hideNavBar />
            <Scene key='componentSamplePage' type={ActionConst.RESET} component={ComponentSamplePage} hideNavBar />
            <Scene key='stockTakingPage' component={StockTakingPage} hideNavBar />
            <Scene key='stockTakingCountPage' component={StockTakingCountPage} hideNavBar />
            <Scene key='printerErrorPage' component={PrinterErrorPage} hideNavBar />
            <Scene key='redirectPage' type={ActionConst.RESET} component={RedirectPage} hideNavBar />
            <Scene key='alertViewSamplePage' type={ActionConst.RESET} component={AlertViewSamplePage} hideNavBar />
            <Scene key='stockMoveInPage' type={ActionConst.RESET} component={StockMoveInPage} hideNavBar />
            <Scene key='stockMoveOutPage' type={ActionConst.RESET} component={StockMoveOutPage} hideNavBar />
            <Scene key='customerRegisterPage' type={ActionConst.RESET} component={CustomerRegisterPage} hideNavBar />
            <Scene key='helpPage' type={ActionConst.RESET} component={HelpPage} hideNavBar />
          </Scene>
        </RouterWithRedux>
      </Provider>
    )
  }
}

export default App
