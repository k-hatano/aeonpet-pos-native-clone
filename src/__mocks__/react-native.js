const values = {}

export const AsyncStorage = {
  setItem: async (key, value) => {
    values[key] = value
  },
  getItem: async (key) => {
    return values[key]
  }
}

export const StyleSheet = {
  create: (arg) => {
    return arg
  }
}

export const NetInfo = {

}

//  {
//   AsyncStorage: {
//     setItem: jest.fn(() => {
//       return new Promise((resolve, reject) => {
//         resolve(null);
//       });
//     }),
//     multiSet:  jest.fn(() => {
//       return new Promise((resolve, reject) => {
//         resolve(null);
//       });
//     }),
//     getItem: jest.fn(() => {
//       return new Promise((resolve, reject) => {
//         resolve(JSON.stringify(getTestData()));
//       });
//     }),
//     multiGet: jest.fn(() => {
//       return new Promise((resolve, reject) => {
//         resolve(multiGetTestData());
//       });
//     }),
//     removeItem: jest.fn(() => {
//       return new Promise((resolve, reject) => {
//         resolve(null);
//       });
//     }),
//     getAllKeys: jest.fn(() => {
//       return new Promise((resolve) => {
//         resolve(['one', 'two', 'three']);
//       });
//     })
//   }
// }
