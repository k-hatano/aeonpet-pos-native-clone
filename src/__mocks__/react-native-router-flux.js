module.exports = {
  ActionConst: {
    FOCUS: 'focus',
    POP: 'pop',
    ANDROID_BACK: 'android_back',
    RESET: 'rest'
  }
}
