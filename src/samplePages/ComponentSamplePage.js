import React, { Component } from 'react'
import { View, ListView, TouchableOpacity, Text } from 'react-native'
import BasePage from '../common/hocs/BasePage'
import baseStyleValues from 'common/styles/baseStyleValues'
import PropTypes from 'prop-types'

class ViewPropertyListItem extends Component {
  static propTypes = {
    property: PropTypes.object,
    onPress: PropTypes.func
  }

  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{height: 60, justifyContent: 'center'}}>
          <Text style={{marginLeft: 40}}>{this.props.property.title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

class ViewFrameListItem extends Component {
  static propTypes = {
    frame: PropTypes.object,
    onPress: PropTypes.func
  }

  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{height: 60, justifyContent: 'center'}}>
          <Text style={{marginLeft: 40}}>{this.props.frame.title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

class SelectComponentView extends Component {
  static propTypes = {
    component: PropTypes.object,
    onSelectComponent: PropTypes.func
  }

  render () {
    return (
      <TouchableOpacity onPress={() => this.props.onSelectComponent(this.props.component)}>
        <View style={{height: 50, width: '100%', justifyContent: 'center'}}>
          <Text style={{marginLeft: 40}}>{this.props.component.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

class SelectComponentHeader extends Component {
  static propTypes = {
    section: PropTypes.string
  }

  render () {
    return (
      <View style={{height: 60, width: '100%', justifyContent: 'center', backgroundColor: 'white'}}>
        <Text style={{marginLeft: 20}}>{this.props.section}</Text>
      </View>
    )
  }
}

export default class ComponentSamplePage extends Component {
  constructor () {
    super()
    this.state = {
      selectedView: null,
      selectedProperty: null,
      selectedFrame: null
    }
    this._dataSource = new ListView.DataSource({
      rowHasChanged: (a, b) => a !== b,
      sectionHeaderHasChanged: (a, b) => a !== b
    })
  }

  _onViewSelected (selectedView) {
    this.setState({
      ...this.state,
      selectedView,
      selectedProperty: selectedView.properties[0],
      selectedFrame: selectedView.frames[0]
    })
  }

  _onPropertySelected (selectedProperty) {
    this.setState({...this.state, selectedProperty})
  }

  _onFrameSelected (selectedFrame) {
    this.setState({...this.state, selectedFrame})
  }

  render () {
    const dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })

    let ContentComponent = () => null
    let properties = []
    let frames = []
    if (this.state.selectedView) {
      ContentComponent = this.state.selectedView.component
      properties = this.state.selectedView.properties
      frames = this.state.selectedView.frames
    }

    let selectedProperty = this.state.selectedProperty || {property: {}}
    let selectedFrame = this.state.selectedFrame || {style: {width: 500, height: 300}}

    return (
      <BasePage>
        <View style={{flex: 1, flexDirection: 'row', alignItems: 'stretch'}}>
          <View style={{flex: 1, alignItems: 'stretch'}}>
            <ListView
              style={{flex: 1}}
              dataSource={this._dataSource.cloneWithRowsAndSections(components)}
              renderRow={(component, rowId) => (
                <SelectComponentView
                  component={component}
                  onSelectComponent={(item) => this._onViewSelected(item)}
                />)}
              renderSectionHeader={(section, sectionId) => (
                <SelectComponentHeader
                  section={sectionId} />
              )}
            />
          </View>
          <View style={{flex: 1, backgroundColor: baseStyleValues.subBackgroundColor}}>
            <View style={{height: 60, justifyContent: 'center'}}>
              <Text style={{margin: 20}}>Properties</Text>
            </View>
            <ListView
              enableEmptySections
              style={{flex: 1}}
              dataSource={dataSource.cloneWithRows(properties)}
              renderRow={(property, rowId) => (
                <ViewPropertyListItem property={property} onPress={() => this._onPropertySelected(property)} />)} />
            <View style={{height: 0.5, backgroundColor: 'gray'}} />
            <View style={{height: 60, justifyContent: 'center'}}>
              <Text style={{margin: 20}}>Frame</Text>
            </View>
            <ListView
              enableEmptySections
              style={{flex: 1}}
              dataSource={dataSource.cloneWithRows(frames)}
              renderRow={(frame, rowId) => (
                <ViewFrameListItem frame={frame} onPress={() => this._onFrameSelected(frame)} />)} />
          </View>
          <View style={{flex: 4, justifyContent: 'center', alignItems: 'center'}}>
            <View {...selectedFrame}>
              <ContentComponent {...selectedProperty.property} />
            </View>
          </View>
        </View>
      </BasePage>
    )
  }
}

const components = {
  Common: [
    require('common/componentSamples/elements/NumberBoxSample').default,
    require('common/componentSamples/layout/HeaderSample').default,
    require('common/componentSamples/layout/HeaderMenuSample').default
  ],
  Home: [
    require('modules/home/componentSamples/HomeMainButtonSample').default
  ],
  OrderCart: [
    require('modules/cart/viewSamples/CartItemViewSample').default,
    require('modules/cart/viewSamples/CartInputNumberFormSample').default,
    require('modules/cart/viewSamples/CartBundledItemViewSample').default,
    require('modules/cart/viewSamples/CartItemListSample').default,
    require('modules/cart/viewSamples/CartHeaderPanelSample').default,
    require('modules/cart/viewSamples/CartSelectProductFooterSample').default,
    require('modules/cart/viewSamples/CartSubtotalViewSample').default,
    require('modules/cart/viewSamples/CartTotalViewSample').default,
    require('modules/cart/viewSamples/CartAmountViewSample').default,
    require('modules/cart/viewSamples/CartPaymentListItemSample').default,
    require('modules/cart/viewSamples/CartCustomerViewSample').default
  ],
  Product: [
    require('modules/product/viewSamples/ProductListItemSample').default,
    require('modules/product/viewSamples/ProductListSample').default,
    require('modules/product/viewSamples/CategoryListSample').default,
    require('modules/product/viewSamples/CategoryProductViewSample').default
  ],
  Promotion: [
    require('modules/promotion/componentSamples/DiscountPadSample').default,
    require('modules/promotion/componentSamples/DiscountOptionsViewSample').default
  ],
  Customer: [
    require('modules/customer/componentSamples/SelectCustomerViewSample').default
  ],
  Order: [
    require('modules/order/componentSamples/OrderdCartItemViewSample').default
  ],
  StockAdjustment: [
    require('modules/stockAdjustment/viewSamples/StockAdjustmentProductViewSample').default
  ],
  Staff: [
    require('modules/staff/viewSamples/SelectStaffViewModalSample').default
  ],
  Debug: [
    require('modules/debug/componentSamples/DebugModalViewSample').default
  ],
  StockTaking: [
    require('modules/stockTaking/viewSamples/StockTakingListSample').default,
    require('modules/stockTaking/viewSamples/StockTakingViewSample').default,
    require('modules/stockTaking/viewSamples/StockTakingTaskStartConfirmViewSample').default,
    require('modules/stockTaking/viewSamples/StockTakingTaskItemListSample').default,
    require('modules/stockTaking/viewSamples/StockTakingCountViewSample').default,
    require('modules/stockTaking/viewSamples/StockTakingSearchProductFormSample').default
  ]
}
