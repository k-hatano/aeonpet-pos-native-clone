import React, { Component } from "react"
import { View } from "react-native"
import BasePage from "../common/hocs/BasePage"
import {
  Button,
  NativeModules,
  Text,
  NativeAppEventEmitter
} from "react-native"

let EventName = "barcode"

export default class OPN3200SamplePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      barcodeText: "ここにバーコードが出力されます。"
    }
  }

  componentWillUnmount() {
    this.eventBarcodeReader.remove()
  }

  componentDidMount() {
    this.eventBarcodeReader = NativeAppEventEmitter.addListener(
      EventName,
      body => {
        this.setState({
          barcodeText: body
        })
      }
    )

    NativeModules.BarcodeReaderBridge.setBarcodeReaderProperty({
      BARCODE_READER_SDK: 0,
      BARCODE_READER_STD: 1,
      BARCODE_READER_IP: "empty"
    })
  }

  render() {
    return (
      <BasePage>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <Text style={{ color: "#000", fontSize: 20 }}>
            {this.state.barcodeText}
          </Text>
        </View>
      </BasePage>
    )
  }
}
