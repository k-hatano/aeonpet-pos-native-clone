import React, { Component } from 'react'
import { View } from 'react-native'
import BasePage from '../common/hocs/BasePage'
import {
  CommandButton, OptionalCommandButton,
  ProceedButton, WeakProceedButton
} from '../common/components/elements/StandardButton'

export default class ButtonSamplePage extends Component {
  render () {
    return (
      <BasePage>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
          <ProceedButton style={{width: 340, height: 40, margin: 20}} text='ProceedButton' />
          <ProceedButton style={{width: 340, height: 40, margin: 20}} text='ProceedButton' disabled />
          <WeakProceedButton style={{width: 340, height: 40, margin: 20}} text='WeakProceedButton' />
          <WeakProceedButton style={{width: 340, height: 40, margin: 20}} text='WeakProceedButton' disabled />
          <OptionalCommandButton style={{width: 340, height: 40, margin: 20}} text='OptionalCommandButton' />
          <OptionalCommandButton style={{width: 340, height: 40, margin: 20}} text='OptionalCommandButton' disabled />
          <CommandButton style={{width: 340, height: 40, margin: 20}} text='CommandButton' />
          <CommandButton style={{width: 340, height: 40, margin: 20}} text='CommandButton' disabled />
        </View>
      </BasePage>
    )
  }
}