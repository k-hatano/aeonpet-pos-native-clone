import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import BasePage from '../common/hocs/BasePage'
import Modal2 from '../common/components/widgets/Modal'
import ConfirmView from '../common/components/widgets/ConfirmView'

class ModalContent extends Component {
  render () {
    return (
      <View style={{backgroundColor: 'red', width: 200, height: 200}} />
    )
  }
}

class ModalButton extends Component {
  constructor () {
    super()

    // For Modal.openBy
    this.measure = (callback) => this.refs.view.measure(callback)
  }

  render () {
    const { color, onClick } = this.props
    return (
      <TouchableOpacity
        onPress={onClick} >
        <View
          ref='view'
          style={{width: 25, height: 25, backgroundColor: color}} />
      </TouchableOpacity>
    )
  }
}

class ButtonBlock extends Component {
  _onClick (refKey, direction, adjust) {
    Modal2.openBy(ModalContent, this.refs[refKey], { direction, adjust })
  }

  render () {
    return (
      <View style={{width: 125, height: 125, margin: 200}}>
        <View style={{height: 25, flexDirection: 'row', justifyContent: 'center'}}>
          <ModalButton
            ref='b_top_left'
            color='red'
            onClick={() => this._onClick('b_top_left', Modal2.DIRECTION.TOP, Modal2.ADJUST.LEFT)} />
          <ModalButton
            ref='b_top_center'
            color='yellow'
            onClick={() => this._onClick('b_top_center', Modal2.DIRECTION.TOP, Modal2.ADJUST.CENTER)} />
          <ModalButton
            ref='b_top_right'
            color='blue'
            onClick={() => this._onClick('b_top_right', Modal2.DIRECTION.TOP, Modal2.ADJUST.RIGHT)} />
        </View>
        <View style={{height: 75, flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <ModalButton
              ref='b_left_top'
              color='red'
              onClick={() => this._onClick('b_left_top', Modal2.DIRECTION.LEFT, Modal2.ADJUST.TOP)} />
            <ModalButton
              ref='b_left_center'
              color='yellow'
              onClick={() => this._onClick('b_left_center', Modal2.DIRECTION.LEFT, Modal2.ADJUST.CENTER)} />
            <TouchableOpacity style={{width: 25, height: 25, backgroundColor: 'red'}} />
          </View>
          <TouchableOpacity
            style={{flex: 3, backgroundColor: 'black', height: 75}}
            onPress={() => ConfirmView.show(
              'test',
              () => { console.log('OK!'); return true },
              () => { console.log('Cancel...'); return true })} />
          <View style={{flex: 1}}>
            <ModalButton
              ref='b_right_top'
              color='red'
              onClick={() => this._onClick('b_right_top', Modal2.DIRECTION.RIGHT, Modal2.ADJUST.TOP)} />
            <ModalButton
              ref='b_right_center'
              color='yellow'
              onClick={() => this._onClick('b_right_center', Modal2.DIRECTION.RIGHT, Modal2.ADJUST.CENTER)} />
            <ModalButton
              ref='b_right_bottom'
              color='blue'
              onClick={() => this._onClick('b_right_bottom', Modal2.DIRECTION.RIGHT, Modal2.ADJUST.BOTTOM)} />
          </View>
        </View>
        <View style={{height: 25, flexDirection: 'row', justifyContent: 'center'}}>
          <ModalButton
            ref='b_bottom_left'
            color='red'
            onClick={() => this._onClick('b_bottom_left', Modal2.DIRECTION.BOTTOM, Modal2.ADJUST.LEFT)} />
          <ModalButton
            ref='b_bottom_center'
            color='yellow'
            onClick={() => this._onClick('b_bottom_center', Modal2.DIRECTION.BOTTOM, Modal2.ADJUST.CENTER)} />
          <ModalButton
            ref='b_bottom_right'
            color='blue'
            onClick={() => this._onClick('b_bottom_right', Modal2.DIRECTION.BOTTOM, Modal2.ADJUST.RIGHT)} />
        </View>
      </View>
    )
  }
}

export default class ModalSamplePage extends Component {
  render () {
    return (
      <BasePage>
        <View style={{flex: 1}}>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <ButtonBlock style={{margin: 50}} />
              <ButtonBlock style={{margin: 50}} />
            </View>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <ButtonBlock style={{margin: 50}} />
              <ButtonBlock style={{margin: 50}} />
            </View>
          </View>
        </View>
      </BasePage>
    )
  }
}
