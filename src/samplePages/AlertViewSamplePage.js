import React, { Component } from 'react'
import BasePage from '../common/hocs/BasePage'
import AlertView from '../common/components/widgets/AlertView'
import ConfirmView from '../common/components/widgets/ConfirmView'
import { View, TextInput } from 'react-native'
import { CommandButton, ProceedButton } from '../common/components/elements/StandardButton'
import Modal from '../common/components/widgets/Modal'

const MODAL_MODE = {
  ALERT: 1,
  CONFIRM: 2
}

export default class AlertViewSamplePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      freeWord: ''
    }
  }

  _openNormalSizeModal (mode) {
    switch (mode) {
      case MODAL_MODE.ALERT:
        AlertView.show('The quick brown fox jumps over the lazy dog.')
        break
      case MODAL_MODE.CONFIRM:
        ConfirmView.show('The quick brown fox jumps over the lazy dog.')
        break
      default:
        break
    }
  }

  _openMaxSizeModal (mode) {
    const message = '返金額：999,999円\n返金方法：金種A\n　　　　　金種B\n　　　　　金種C\n　　　　　金種D\n　　　　　金種E\n　　　　　金種F\n　　　　　金種G\n\n一括返品を実行しますか？'
    switch (mode) {
      case MODAL_MODE.ALERT:
        AlertView.show(message)
        break
      case MODAL_MODE.CONFIRM:
        ConfirmView.show(message)
        break
      default:
        break
    }
  }

  _openScrollModal (mode) {
    const message = '返金額：999,999円\n返金方法：金種A\n　　　　　金種B\n　　　　　金種C\n　　　　　金種D\n　　　　　金種E\n　　　　　金種F\n　　　　　金種G\n　　　　　金種H\n　　　　　金種I\n　　　　　金種J\n　　　　　金種K\n\n一括返品を実行しますか？'
    switch (mode) {
      case MODAL_MODE.ALERT:
        AlertView.show(message)
        break
      case MODAL_MODE.CONFIRM:
        ConfirmView.show(message)
        break
      default:
        break
    }
  }

  _openFreeWordModal (mode, message) {
    switch (mode) {
      case MODAL_MODE.ALERT:
        AlertView.show(message)
        break
      case MODAL_MODE.CONFIRM:
        ConfirmView.show(message)
        break
      default:
        break
    }
  }

  render () {
    return (
      <BasePage>
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}} >
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <CommandButton
              text='NormalAlertView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openNormalSizeModal(MODAL_MODE.ALERT)} />
            <ProceedButton
              text='NormalConfirmView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openNormalSizeModal(MODAL_MODE.CONFIRM)} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <CommandButton
              text='MaxSizeAlertView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openMaxSizeModal(MODAL_MODE.ALERT)} />
            <ProceedButton
              text='MaxSizeConfirmView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openMaxSizeModal(MODAL_MODE.CONFIRM)} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <CommandButton
              text='ScrollAlertView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openScrollModal(MODAL_MODE.ALERT)} />
            <ProceedButton
              text='ScrollConfirmView'
              style={{width: 300, height: 60, margin: 50, fontSize: 24}}
              onPress={() => this._openScrollModal(MODAL_MODE.CONFIRM)} />
          </View>
          <View style={{flexDirection: 'row'}}>
            <TextInput
              style={{width: 300, height: 120, margin: 10, borderColor: 'gray', borderWidth: 1}}
              onChangeText={(freeWord) => this.setState({freeWord})}
              value={this.state.freeWord}
              placeholder='message'
              multiline={true} />
            <View>
              <CommandButton
                text='freeWordAlertView'
                style={{width: 300, height: 60, margin: 10, fontSize: 24}}
                onPress={() => this._openFreeWordModal(MODAL_MODE.ALERT, this.state.freeWord)} />
              <ProceedButton
                text='freeWordConfirmView'
                style={{width: 300, height: 60, margin: 10, fontSize: 24}}
                onPress={() => this._openFreeWordModal(MODAL_MODE.CONFIRM, this.state.freeWord)} />
            </View>
          </View>
        </View>
      </BasePage>
    )
  }
}
